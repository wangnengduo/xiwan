﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.MgGroup
{
    public class MgGroupResp
    {
        #region 获取酒店详细信息
        public class Service_GetHotelDetail
        {
            [XmlElement(ElementName = "GetHotelDetail_Response")]
            public GetHotelDetail_Response GetHotelDetail_Response { get; set; }
        }
        public class GetHotelDetail_Response
        {
            [XmlElement(ElementName = "Error")]
            public Error Error { get; set; }
            [XmlElement(ElementName = "HotelId")]
            public string HotelId { get; set; }
            [XmlElement(ElementName = "HotelName")]
            public string HotelName { get; set; }
            [XmlElement(ElementName = "Rating")]
            public string Rating { get; set; }
            [XmlElement(ElementName = "HotelRooms")]
            public string HotelRooms { get; set; }
            [XmlElement(ElementName = "Address1")]
            public string Address1 { get; set; }
            [XmlElement(ElementName = "Address2")]
            public string Address2 { get; set; }
            [XmlElement(ElementName = "Address3")]
            public string Address3 { get; set; }
            [XmlElement(ElementName = "Continent")]
            public string Continent { get; set; }
            [XmlElement(ElementName = "Region")]
            public string Region { get; set; }
            [XmlElement(ElementName = "Location")]
            public string Location { get; set; }
            [XmlElement(ElementName = "Telephone")]
            public string Telephone { get; set; }
            [XmlElement(ElementName = "Facsimile")]
            public string Facsimile { get; set; }
            [XmlElement(ElementName = "Email")]
            public string Email { get; set; }
            [XmlElement(ElementName = "Website")]
            public string Website { get; set; }
            [XmlElement(ElementName = "Facility")]
            public List<string> Facility { get; set; }
        }
        #endregion

        #region 获取报价
        public class Service_SearchHotel
        {
            [XmlElement(ElementName = "SearchHotel_Response")]
            public SearchHotel_Response SearchHotel_Response { get; set; }
        }
        public class SearchHotel_Response
        {
            [XmlElement(ElementName = "Error")]
            public Error Error { get; set; }
            [XmlElement(ElementName = "hasKey")]
            public string hasKey { get; set; }
            [XmlElement(ElementName = "hotels")]
            public hotels hotels { get; set; }
        }
        public class hotels
        {
            [XmlElement(ElementName = "Hotel")]
            public List<Hotel> Hotel { get; set; }
        }
        public class Hotel
        {
            [XmlAttribute("HotelId")]
            public string HotelId { get; set; }
            [XmlAttribute("HotelName")]
            public string HotelName { get; set; }
            [XmlAttribute("Rating")]
            public string Rating { get; set; }
            [XmlAttribute("Currency")]
            public string Currency { get; set; }
            [XmlAttribute("Latitude")]
            public string Latitude { get; set; }
            [XmlAttribute("Longitude")]
            public string Longitude { get; set; }
            [XmlAttribute("MarketName")]
            public string MarketName { get; set; }
            [XmlAttribute("dtCheckIn")]
            public string dtCheckIn { get; set; }
            [XmlAttribute("dtCheckOut")]
            public string dtCheckOut { get; set; }
            [XmlAttribute("CancelPolicyId")]
            public string CancelPolicyId { get; set; }
            [XmlAttribute("InternalCode")]
            public string InternalCode { get; set; }
            [XmlAttribute("avail")]
            public string avail { get; set; }
            [XmlElement(ElementName = "RoomCateg")]
            public List<RoomCateg> RoomCateg { get; set; }
        }
        public class RoomCateg
        {
            [XmlAttribute("Code")]
            public string Code { get; set; }
            [XmlAttribute("Name")]
            public string Name { get; set; }
            [XmlAttribute("NetPrice")]
            public string NetPrice { get; set; }
            [XmlAttribute("GrossPrice")]
            public string GrossPrice { get; set; }
            [XmlAttribute("CommPrice")]
            public string CommPrice { get; set; }
            [XmlAttribute("Price")]
            public string Price { get; set; }
            [XmlAttribute("BFType")]
            public string BFType { get; set; }

            [XmlElement(ElementName = "RoomType")]
            public List<RoomType> RoomType { get; set; }
        }
        public class RoomType
        {
            [XmlAttribute("TypeName")]
            public string TypeName { get; set; }
            [XmlAttribute("SeqNo")]
            public string SeqNo { get; set; }
            [XmlAttribute("NumRooms")]
            public string NumRooms { get; set; }
            [XmlAttribute("TotalPrice")]
            public string TotalPrice { get; set; }
            [XmlAttribute("avrNightPrice")]
            public string avrNightPrice { get; set; }
            [XmlAttribute("RTGrossPrice")]
            public string RTGrossPrice { get; set; }
            [XmlAttribute("RTCommPrice")]
            public string RTCommPrice { get; set; }
            [XmlAttribute("RTNetPrice")]
            public string RTNetPrice { get; set; }

            [XmlElement(ElementName = "Rate")]
            public List<Rate> Rate { get; set; }
        }
        public class Rate
        {
            [XmlAttribute("offSet")]
            public string offSet { get; set; }
            [XmlAttribute("NightPrice")]
            public string NightPrice { get; set; }

            [XmlElement(ElementName = "RoomRate")]
            public RoomRate RoomRate { get; set; }
            [XmlElement(ElementName = "RoomRateInfo")]
            public RoomRateInfo RoomRateInfo { get; set; }
        }
        public class RoomRate
        {
            [XmlElement(ElementName = "RoomSeq")]
            public RoomSeq RoomSeq { get; set; }
        }
        public class RoomSeq 
        {
            [XmlAttribute("No")]
            public string No { get; set; }
            [XmlAttribute("AdultNum")]
            public string AdultNum { get; set; }
            [XmlAttribute("ChildNum")]
            public string ChildNum { get; set; }
            [XmlAttribute("ChildAge1")]
            public string ChildAge1 { get; set; }
            [XmlAttribute("ChildAge2")]
            public string ChildAge2 { get; set; }
            [XmlAttribute("RoomPrice")]
            public string RoomPrice { get; set; }
            [XmlAttribute("MinstayPrice")]
            public string MinstayPrice { get; set; }
            [XmlAttribute("CompulsoryPrice")]
            public string CompulsoryPrice { get; set; }
            [XmlAttribute("SupplementPrice")]
            public string SupplementPrice { get; set; }
            [XmlAttribute("PromotionBFPrice")]
            public string PromotionBFPrice { get; set; }
            [XmlAttribute("EarlyBirdDiscount")]
            public string EarlyBirdDiscount { get; set; }
            [XmlAttribute("CommissionPrice")]
            public string CommissionPrice { get; set; }
            [XmlAttribute("sRoomType")]
            public string sRoomType { get; set; }
            [XmlAttribute("sCh1")]
            public string sCh1 { get; set; }
            [XmlAttribute("sCh2")]
            public string sCh2 { get; set; }
            [XmlAttribute("NewTypeName")]
            public string NewTypeName { get; set; }
            [XmlAttribute("NewChildNbr")]
            public string NewChildNbr { get; set; }
        }
        public class RoomRateInfo
        {
            [XmlElement(ElementName = "Child")]
            public Child Child { get; set; }
            [XmlElement(ElementName = "Minstay")]
            public Minstay Minstay { get; set; }
            [XmlElement(ElementName = "Compulsory")]
            public Compulsory Compulsory { get; set; }
            [XmlElement(ElementName = "Supplement")]
            public Supplement Supplement { get; set; }
            [XmlElement(ElementName = "Promotion")]
            public Promotion Promotion { get; set; }
            [XmlElement(ElementName = "EarlyBird")]
            public EarlyBird EarlyBird { get; set; }
        }
        public class Child
        {
            [XmlAttribute("MinAge")]
            public string MinAge { get; set; }
            [XmlAttribute("MaxAge")]
            public string MaxAge { get; set; }
            [XmlAttribute("Desc")]
            public string Desc { get; set; }
            [XmlAttribute("Info")]
            public string Info { get; set; }
        }
        public class Minstay
        {
            [XmlAttribute("MSDay")]
            public string MSDay { get; set; }
            [XmlAttribute("MSType")]
            public string MSType { get; set; }
            [XmlAttribute("MSRate")]
            public string MSRate { get; set; }
        }
        public class Compulsory
        {
            [XmlAttribute("Name")]
            public string Name { get; set; }
        }
        public class Supplement
        {
            [XmlAttribute("Name")]
            public string Name { get; set; }
        }
        public class Promotion
        {
            [XmlAttribute("Name")]
            public string Name { get; set; }
            [XmlAttribute("Value")]
            public string Value { get; set; }
            [XmlAttribute("PromoCode")]
            public string PromoCode { get; set; }
        }
        public class EarlyBird
        {
            [XmlAttribute("EBType")]
            public string EBType { get; set; }
            [XmlAttribute("EBRate")]
            public string EBRate { get; set; }
        }
        #endregion
        
        #region 获取退单政策
        public class Service_ViewCancelPolicy
        {
            [XmlElement(ElementName = "ViewCancelPolicy_Response")]
            public ViewCancelPolicy_Response ViewCancelPolicy_Response { get; set; }
        }
        public class ViewCancelPolicy_Response
        {
            [XmlElement(ElementName = "Error")]
            public Error Error { get; set; }
            [XmlElement(ElementName = "HotelId")]
            public string HotelId { get; set; }
            [XmlElement(ElementName = "HotelName")]
            public string HotelName { get; set; }
            [XmlElement(ElementName = "Policies")]
            public Policies Policies { get; set; }
        }
        public class Policies
        {
            [XmlElement(ElementName = "Policy")]
            public List<Policy> Policy { get; set; }
        }
        public class Policy
        {
            [XmlAttribute("FromDate")]
            public string FromDate { get; set; }
            [XmlAttribute("ToDate")]
            public string ToDate { get; set; }
            [XmlElement(ElementName = "RoomCatgCode")]
            public RoomCatgCode RoomCatgCode { get; set; }
            [XmlElement(ElementName = "ExCancelDays")]
            public string ExCancelDays { get; set; }
            [XmlElement(ElementName = "ChargeType")]
            public string ChargeType { get; set; }
            [XmlElement(ElementName = "Currency")]
            public string Currency { get; set; }
            [XmlElement(ElementName = "ChargeRate")]
            public string ChargeRate { get; set; }
            [XmlElement(ElementName = "Description")]
            public string Description { get; set; }
        }
        public class RoomCatgCode
        {
            [XmlAttribute("Name")]
            public string Name { get; set; }
            [XmlAttribute("BFType")]
            public string BFType { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        #endregion

        #region 预订酒店
        public class BookingHotel
        {
            [XmlElement(ElementName = "BookHotel_Response")]
            public BookHotel_Response BookHotel_Response { get; set; }
        }
        public class BookHotel_Response
        {
            [XmlElement(ElementName = "Error")]
            public Error Error { get; set; }
            [XmlElement(ElementName = "hasKey")]
            public string hasKey { get; set; }
            [XmlElement(ElementName = "ResNo")]
            public ResNo ResNo { get; set; }
            [XmlElement(ElementName = "CompleteService")]
            public CompleteService CompleteService { get; set; }
        }
        public class ResNo
        {
            [XmlAttribute("Status")]
            public string Status { get; set; }
            [XmlAttribute("PaxPassport")]
            public string PaxPassport { get; set; }
            [XmlAttribute("OSRefNo")]
            public string OSRefNo { get; set; }
            [XmlAttribute("FinishBook")]
            public string FinishBook { get; set; }
            [XmlAttribute("RPCurrency")]
            public string RPCurrency { get; set; }
            [XmlText]
            public string ResNoValue { get; set; }
        }
        public class CompleteService
        {
            [XmlElement(ElementName = "HBookId")]
            public HBookId HBookId { get; set; }
        }
        public class HBookId
        {
            [XmlAttribute("CanAmend")]
            public string CanAmend { get; set; }
            [XmlAttribute("Id")]
            public string Id { get; set; }
            [XmlAttribute("VoucherNo")]
            public string VoucherNo { get; set; }
            [XmlAttribute("VoucherDt")]
            public string VoucherDt { get; set; }
            [XmlAttribute("Status")]
            public string Status { get; set; }
            [XmlAttribute("InternalCode")]
            public string InternalCode { get; set; }
            [XmlElement(ElementName = "HotelId")]
            public HotelId HotelId { get; set; }
            [XmlElement(ElementName = "Period")]
            public Period Period { get; set; }
            [XmlElement(ElementName = "RoomCatgInfo")]
            public RoomCatgInfo RoomCatgInfo { get; set; }
            [XmlElement(ElementName = "RequestDes")]
            public string RequestDes { get; set; }
        }
        public class HotelId
        {
            [XmlAttribute("HotelName")]
            public string HotelName { get; set; }
            [XmlText]
            public string HotelIdValue { get; set; }
        }
        public class Period
        {
            [XmlAttribute("FromDt")]
            public string FromDt { get; set; }
            [XmlAttribute("ToDt")]
            public string ToDt { get; set; }
        }
        public class RoomCatgInfo
        {
            [XmlElement(ElementName = "RoomCatg")]
            public RoomCatg RoomCatg { get; set; }
        }
        public class RoomCatg
        {
            [XmlAttribute("CatgId")]
            public string CatgId { get; set; }
            [XmlAttribute("CatgName")]
            public string CatgName { get; set; }
            [XmlAttribute("Market")]
            public string Market { get; set; }
            [XmlAttribute("Avail")]
            public string Avail { get; set; }
            [XmlAttribute("BFType")]
            public string BFType { get; set; }
            [XmlElement(ElementName = "Room")]
            public Room Room { get; set; }
        }
        public class Room
        {
            [XmlElement(ElementName = "SeqRoom")]
            public List<SeqRoom> SeqRoom { get; set; }
        }
        public class SeqRoom
        {
            [XmlAttribute("ServiceNo")]
            public string ServiceNo { get; set; }
            [XmlAttribute("RoomType")]
            public string RoomType { get; set; }
            [XmlAttribute("SeqNo")]
            public string SeqNo { get; set; }
            [XmlAttribute("AdultNum")]
            public string AdultNum { get; set; }
            [XmlAttribute("ChildAge1")]
            public string ChildAge1 { get; set; }
            [XmlAttribute("ChildAge2")]
            public string ChildAge2 { get; set; }
            [XmlElement(ElementName = "PriceInfomation")]
            public PriceInfomation PriceInfomation { get; set; }
            [XmlElement(ElementName = "TotalPrice")]
            public string TotalPrice { get; set; }
            [XmlElement(ElementName = "CommissionPrice")]
            public string CommissionPrice { get; set; }
            [XmlElement(ElementName = "NetPrice")]
            public string NetPrice { get; set; }
            [XmlElement(ElementName = "PaxInformation")]
            public PaxInformation PaxInformation { get; set; }
        }
        public class PriceInfomation
        {
            [XmlElement(ElementName = "NightPrice")]
            public List<NightPrice> NightPrice { get; set; }
        }
        public class NightPrice
        {
            [XmlElement(ElementName = "Accom")]
            public Accom Accom { get; set; }
            [XmlElement(ElementName = "Child")]
            public Child Child { get; set; }
            [XmlElement(ElementName = "RatePromotion")]
            public RatePromotion RatePromotion { get; set; }
            [XmlElement(ElementName = "MinStay")]
            public MinStay MinStay { get; set; }
            [XmlElement(ElementName = "Compulsory")]
            public Compulsory Compulsory { get; set; }
            [XmlElement(ElementName = "Supplement")]
            public Supplement Supplement { get; set; }
            [XmlElement(ElementName = "Promotion")]
            public Promotion Promotion { get; set; }
            [XmlElement(ElementName = "EarlyBird")]
            public EarlyBird EarlyBird { get; set; }
            [XmlElement(ElementName = "Commission")]
            public Commission Commission { get; set; }
        }
        public class Accom
        {
            [XmlAttribute("Price")]
            public string Price { get; set; }
        }
        public class RatePromotion
        {
            [XmlAttribute("MINDay")]
            public string MINDay { get; set; }
        }
        public class MinStay
        {
            [XmlAttribute("MSDay")]
            public string MSDay { get; set; }
            [XmlAttribute("MSType")]
            public string MSType { get; set; }
            [XmlAttribute("MSRate")]
            public string MSRate { get; set; }
            [XmlAttribute("MSPrice")]
            public string MSPrice { get; set; }
        }
        public class Commission
        {
            [XmlAttribute("Name")]
            public string Name { get; set; }
            [XmlAttribute("Price")]
            public string Price { get; set; }
        }
        public class PaxInformation
        {
            [XmlElement(ElementName = "GuestName")]
            public List<string> GuestName { get; set; }
        }
        #endregion

        #region 退单
        public class Service_CancelRSVN
        {
            [XmlElement(ElementName = "CancelReservation_Response")]
            public CancelReservation_Response CancelReservation_Response { get; set; }
        }
        public class CancelReservation_Response
        {
            [XmlElement(ElementName = "Error")]
            public Error Error { get; set; }
            [XmlElement(ElementName = "ResNo")]
            public string ResNo { get; set; }
            [XmlElement(ElementName = "OSRefNo")]
            public string OSRefNo { get; set; }
            [XmlElement(ElementName = "Hbooking")]
            public string Hbooking { get; set; }
            [XmlElement(ElementName = "CancelHbooking")]
            public string CancelHbooking { get; set; }
            [XmlElement(ElementName = "CancelResult")]
            public string CancelResult { get; set; }
            [XmlElement(ElementName = "Policies")]
            public CancelPolicies Policies { get; set; }
        }
        public class CancelPolicies
        {
            [XmlElement(ElementName = "RoomCatgCode")]
            public RoomCatgCode RoomCatgCode { get; set; }
            [XmlElement(ElementName = "ExCancelDays")]
            public string ExCancelDays { get; set; }
            [XmlElement(ElementName = "ChargeType")]
            public string ChargeType { get; set; }
            [XmlElement(ElementName = "ChargeRate")]
            public string ChargeRate { get; set; }
            [XmlElement(ElementName = "ChargePrice")]
            public ChargePrice ChargePrice { get; set; }
        }
        public class ChargePrice
        { 
            [XmlAttribute("Price")]
            public string Price { get; set; }
            [XmlAttribute("Currency")]
            public string Currency { get; set; }
        }
        #endregion

        #region 查看订单
        public class Service_GetRSVNInfo
        {
            [XmlElement(ElementName = "GetRSVNInfo_Response")]
            public GetRSVNInfo_Response GetRSVNInfo_Response { get; set; }
        }
        public class GetRSVNInfo_Response
        {
            [XmlElement(ElementName = "Error")]
            public Error Error { get; set; }
            [XmlElement(ElementName = "ResNo")]
            public ResNo ResNo { get; set; }
            [XmlElement(ElementName = "HBookId")]
            public HBookId HBookId { get; set; }
        }
        #endregion

        #region 查看取消费用
        public class Service_GetCancelPolicy
        {
            [XmlElement(ElementName = "GetCancelPolicy_Response")]
            public GetCancelPolicy_Response GetCancelPolicy_Response { get; set; }
        }
        public class GetCancelPolicy_Response
        {
            [XmlElement(ElementName = "Error")]
            public Error Error { get; set; }
            [XmlElement(ElementName = "ResNo")]
            public ResNo ResNo { get; set; }
            [XmlElement(ElementName = "HBookId")]
            public string HBookId { get; set; }
            [XmlElement(ElementName = "HotelId")]
            public string HotelId { get; set; }
            [XmlElement(ElementName = "HotelName")]
            public string HotelName { get; set; }
            [XmlElement(ElementName = "Policies")]
            public Policies Policies { get; set; }
        }
        
        #endregion


        public class Error
        {
            [XmlElement(ElementName = "ErrorDescription")]
            public string ErrorDescription { get; set; }
        }
    }
}