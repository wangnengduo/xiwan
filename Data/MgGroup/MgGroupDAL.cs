﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;

namespace XiWan.Data.MgGroup
{
    public class MgGroupDAL
    {
        string USDtoRMBrate = CCommon.GetWebConfigValue("USDtoRMBrate");//美元转为人民币汇率
        string EURtoRMBrate = CCommon.GetWebConfigValue("EURtoRMBrate");//欧元转为人民币汇率
        string SGDtoRMBrate = CCommon.GetWebConfigValue("SGDtoRMBrate");//新加坡元转为人民币汇率
        string HKDtoRMBrate = CCommon.GetWebConfigValue("HKDtoRMBrate");//港币转为人民币汇率
        string IDRtoRMBrate = CCommon.GetWebConfigValue("IDRtoRMBrate");//印尼卢比转为人民币汇率
        string AgodaTime = CCommon.GetWebConfigValue("AgodaTime");//缓存到数据库的有效期（分钟）
        string SeachUrl = CCommon.GetWebConfigValue("MgGroupSeachUrl");//查询报价
        string HotelDetailUrl = CCommon.GetWebConfigValue("MgGroupHotelDetailUrl");//查询酒店详细信息
        string HotelViewCancelPolicyUrl = CCommon.GetWebConfigValue("MgGroupHotelViewCancelPolicyUrl");//退单政策
        string BookHotelUrl = CCommon.GetWebConfigValue("MgGroupBookHotelUrl");//下订单
        string RSVNInfoUrl = CCommon.GetWebConfigValue("MgGroupRSVNInfoUrl");//
        string CancelPolicyUrl = CCommon.GetWebConfigValue("MgGroupCancelPolicyUrl");//预订后退单罚款
        string CancelReservationUrl = CCommon.GetWebConfigValue("MgGroupCancelReservationUrl");//退单
        string AmendHotelUrl = CCommon.GetWebConfigValue("MgGroupAmendHotelUrl");//
        string AgentID = CCommon.GetWebConfigValue("MgGroupAgentID");//
        string Username = CCommon.GetWebConfigValue("MgGroupUsername");//
        string Password = CCommon.GetWebConfigValue("MgGroupPassword");//
        string AgodaCacheTime = CCommon.GetWebConfigValue("AgodaCacheTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);

        #region 初始化本类
        private static MgGroupDAL _Instance;
        public static MgGroupDAL Instance
        {
            get
            {
                return new MgGroupDAL();
            }
        }
        #endregion

        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetHotels()
        {
            string result = string.Empty;
            string reqData = string.Empty;
            string respData = string.Empty;
            try
            {
                var scoreList = new List<string>();
                scoreList.Add("WSASTHBKK000087");
                scoreList.Add("WSASTHBKK001057");
                scoreList.Add("WSASTHBKK000511");
                scoreList.Add("WSASTHBKK001405");
                scoreList.Add("WSASTHBKK001410");
                scoreList.Add("WSASTHBKK000841");
                scoreList.Add("WSASTHHKT000715");

                for (int i = 0; i < scoreList.Count; i++)
                {

                    reqData = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?>
<Service_GetHotelDetail><AgentLogin><AgentId>{0}</AgentId><LoginName>{1}</LoginName><Password>{2}</Password></AgentLogin>
<GetHotelDetail_Request><HotelId>{3}</HotelId></GetHotelDetail_Request></Service_GetHotelDetail>", AgentID, Username, Password, scoreList[i]);
                    try
                    {
                        respData = Common.Common.PostHttp(HotelDetailUrl, reqData);
                        LogHelper.DoOperateLog(string.Format("Studio：MgGroup从接口获取酒店信息并保存到数据库 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "MgGroup接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:MgGroup从接口获取酒店信息并保存到数据库,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        result = "保存失败";
                        return result;
                    }
                    MgGroupResp.Service_GetHotelDetail objResp = Common.Common.DESerializer<MgGroupResp.Service_GetHotelDetail>(Common.Common.xmlReplace(respData));
                    if (objResp != null)
                    {
                        if (objResp.GetHotelDetail_Response != null)
                        {
                            if (objResp.GetHotelDetail_Response.HotelId != null && objResp.GetHotelDetail_Response.HotelId != "")
                            {
                                MgGroupHotel obj = new MgGroupHotel();
                                obj.HotelId = objResp.GetHotelDetail_Response.HotelId;
                                obj.HotelName = objResp.GetHotelDetail_Response.HotelName ?? "";
                                obj.Rating = objResp.GetHotelDetail_Response.Rating ?? "";
                                obj.Address1 = objResp.GetHotelDetail_Response.Address1 ?? "";
                                obj.Continent = objResp.GetHotelDetail_Response.Continent ?? "";
                                obj.Region = objResp.GetHotelDetail_Response.Region ?? "";
                                obj.Location = objResp.GetHotelDetail_Response.Location ?? "";
                                obj.Telephone = objResp.GetHotelDetail_Response.Telephone ?? "";
                                obj.Email = objResp.GetHotelDetail_Response.Email ?? "";
                                con.Save(obj);
                            }
                        }
                    }
                }
                result = "保存成功";
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup从接口获取酒店信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                result = "保存失败";
            }
            return result;
        }

        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationalityToSql()
        {
            string sql = "select Code as ID,Name as NAME from MgGroupPaxPassport ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestinationToSql(string strName)
        {
            string sql = string.Format(@"SELECT DISTINCT(CityNameEn+','+ CountryNameEn) as name,(CityCode + ',' + CountryCode) as value FROM MgGroupHotel where CityNameEn+','+ CountryNameEn like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        

        /// <summary>
        /// 查询报价
        /// </summary>
        /// <returns></returns>
        public DataTable GetRoomTypePrice(string hotelID, string CheckInDate, string CheckOutDate, List<IntOccupancyInfo> occupancys, int RoomCount, string rateplanId, string nationality)
        {
            string result = string.Empty;
            string reqData = string.Empty;
            string respData = string.Empty;
            string RoomType = string.Empty;
            DataTable dt = new DataTable();
            try
            {
                string roomInfoXml = string.Empty;
                string sqlHt = string.Format(@"select top 1 * from MgGroupHotel WITH(NOLOCK) where HotelID='{0}'", hotelID);
                DataTable dtHt = ControllerFactory.GetController().GetDataTable(sqlHt);
                for (int i = 0; i < occupancys.Count; i++)
                {
                    int Adult = 0;
                    string roomType = string.Empty;
                    if (occupancys[i].adults == 1)
                    {
                        roomType = "Single";
                        Adult = 1;
                    }
                    else if (occupancys[i].adults == 2)
                    {
                        roomType = "Double";
                        Adult = 2;
                    }
                    else if (occupancys[i].adults == 99)
                    {
                        roomType = "Twin";
                        Adult = 2;
                    }
                    else if (occupancys[i].adults == 3)
                    {
                        roomType = "Triple";
                        Adult = 3;
                    }
                    else if (occupancys[i].adults == 4)
                    {
                        roomType = "Quad";
                        Adult = 4;
                    }
                    roomInfoXml += string.Format(@"<RoomInfo><AdultNum RoomType=""{0}"" RQBedChild=""N"">{1}</AdultNum></RoomInfo>",roomType, Adult);
                }

                reqData = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_SearchHotel><AgentLogin><AgentId>{0}</AgentId>
<LoginName>{1}</LoginName><Password>{2}</Password></AgentLogin><SearchHotel_Request><PaxPassport>{3}</PaxPassport><hasKey>{4}</hasKey><RPCurrency>{5}</RPCurrency>
<DestCountry>{6}</DestCountry><DestCity>{7}</DestCity><HotelId>{8}</HotelId><ServiceCode></ServiceCode><Period CheckIn=""{9}"" CheckOut=""{10}"" />{11}<flagAvail>Y</flagAvail>
</SearchHotel_Request></Service_SearchHotel>", AgentID, Username, Password, "WSASCN", "", "MYR", dtHt.Rows[0]["CountryCode"], dtHt.Rows[0]["CityCode"], hotelID, 
                                             CheckInDate,CheckOutDate, roomInfoXml);
                try
                {
                    respData = Common.Common.PostHttp(SeachUrl,reqData);
                    LogHelper.DoOperateLog(string.Format("Studio：MgGroup查询报价 ,请求数据:{0} 返回数据 ：{1} ， Time:{2} ",reqData, respData, DateTime.Now.ToString()), "MgGroup接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:MgGroup查询报价,err:{0} ,请求数据:{1} , Time:{1} ", ex.Message,reqData, DateTime.Now.ToString()), "Err");
                    result = "保存失败";
                    return dt;
                }
                MgGroupResp.Service_SearchHotel objResp = Common.Common.DESerializer<MgGroupResp.Service_SearchHotel>(Common.Common.xmlReplace(respData).Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.SearchHotel_Response != null && objResp.SearchHotel_Response.hotels != null)
                    {
                        HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                        for (int j = 0; j < objResp.SearchHotel_Response.hotels.Hotel.Count; j++)
                        {
                            for (int k = 0; k < objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg.Count; k++)
                            {
                                string cGuid = Common.Common.GuidToLongID();//房型id
                                string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                HotelPrice obj = new HotelPrice();
                                obj.HotelID = objResp.SearchHotel_Response.hotels.Hotel[j].HotelId;
                                obj.Platform = "MgGroup";
                                obj.Status = "1";
                                string EachRoomAdult = string.Empty;
                                string RoomTypeSeqNo = string.Empty;
                                string PriceBreakdownPrice = string.Empty;
                                string ClassName = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].Name + ":";
                                for (int m = 0; m < objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType.Count; m++)
                                {
                                    int NumRooms = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].NumRooms.AsTargetType<int>(0);
                                    for (int t = 0; t < NumRooms; t++)
                                    {
                                        ClassName += objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].TypeName + "+";
                                        EachRoomAdult += objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].Rate[0].RoomRate.RoomSeq.AdultNum + ";";
                                        //RoomTypeSeqNo += objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].SeqNo + ";";
                                        PriceBreakdownPrice += objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].TotalPrice.AsTargetType<decimal>(0) / NumRooms + ";";
                                    }
                                }

                                obj.HotelNo = nationality;// +":" + RoomTypeSeqNo;
                                obj.ClassName = ClassName.TrimEnd('+');
                                obj.ClassNo = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].Code + "," + objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].BFType;
                                
                                decimal ClientPrice = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].NetPrice.AsTargetType<decimal>(0);
                                string toRMBrate = string.Empty;
                                string ClientCurrencyCode = objResp.SearchHotel_Response.hotels.Hotel[j].Currency;
                                string sqlCurrency = string.Format(@"select * from Currency where CurrencyCode='{0}'", ClientCurrencyCode);
                                DataTable dtCurrency = ControllerFactory.GetController().GetDataTable(sqlCurrency);
                                obj.RMBprice = ClientPrice * dtCurrency.Rows[0]["ExchangeRate1"].AsTargetType<decimal>(0);
                                obj.Price = ClientPrice;
                                obj.CurrencyCode = ClientCurrencyCode;
                                int IsBreakfast = 1;
                                string BFType = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].BFType;
                                if (BFType == "RO" || BFType == "ROPROMO" || BFType == "ROSPC")
                                {
                                    IsBreakfast = 0;
                                }
                                obj.IsBreakfast = IsBreakfast;
                                obj.BreakfastName = BFType;
                                obj.ReferenceClient = objResp.SearchHotel_Response.hotels.Hotel[j].CancelPolicyId + "," + objResp.SearchHotel_Response.hotels.Hotel[j].InternalCode;
                                //obj.RoomsLeft = respObj[k].rooms[l].room_classes[m].samples[n].rooms_left;
                                obj.RoomCount = RoomCount;
                                obj.Adults = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[0].Rate[0].RoomRate.RoomSeq.AdultNum.AsTargetType<int>(0);
                                //obj.Childs = children;
                                //obj.ChildsAge = childAges;
                                obj.EachRoomAdult = EachRoomAdult.TrimEnd(';');
                                obj.CheckInDate = CheckInDate.AsTargetType<DateTime>(DateTime.MinValue);
                                obj.CheckOutDate = CheckOutDate.AsTargetType<DateTime>(DateTime.MinValue);
                                obj.SeachID = objResp.SearchHotel_Response.hasKey;
                                obj.CGuID = cGuid;
                                obj.RateplanId = GuidRateplanId;

                                obj.PriceBreakdownDate = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[0].Rate.Count.AsTargetType<string>("0");
                                obj.PriceBreakdownPrice = PriceBreakdownPrice.TrimEnd(';');
                                obj.IsClose = 1;
                                obj.CreateTime = DateTime.Now;
                                obj.UpdateTime = DateTime.Now;
                                obj.EntityState = e_EntityState.Added;
                                objHotelPriceCol.Add(obj);
                            }
                        }
                        string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' and  Platform = 'MgGroup'", hotelID);
                        con.Update(updateHotelPriceSql);
                        con.Save(objHotelPriceCol);
                        string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'MgGroup' and IsClose=1", hotelID);
                        dt = ControllerFactory.GetController().GetDataTable(sql);
                    }
                }
                result = "保存成功";
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup查询报价 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                result = "保存失败";
            }
            return dt;
        }

        /// <summary>
        /// 复查价格
        /// </summary>
        /// <returns></returns>
        public bool RecheckPrice(string hotelID, string CheckInDate, string CheckOutDate, List<IntOccupancyInfo> occupancys, int RoomCount, string rateplanId)
        {
            string result = string.Empty;
            string reqData = string.Empty;
            string respData = string.Empty;
            string RoomType = string.Empty;
            bool isSuccess = false;
            try
            {
                string roomInfoXml = string.Empty;
                string sqlHt = string.Format(@"SELECT * FROM dbo.HotelPrice hp WITH(NOLOCK) INNER JOIN MgGroupHotel mg WITH(NOLOCK) ON mg.HotelId=hp.HotelID
where hp.HotelID='{0}' AND hp.RateplanId='{1}'", hotelID, rateplanId);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sqlHt);
                for (int i = 0; i < occupancys.Count; i++)
                {
                    int Adult = 0;
                    string roomType = string.Empty;
                    if (occupancys[i].adults == 1)
                    {
                        roomType = "Single";
                        Adult = 1;
                    }
                    else if (occupancys[i].adults == 2)
                    {
                        roomType = "Double";
                        Adult = 2;
                    }
                    else if (occupancys[i].adults == 99)
                    {
                        roomType = "Twin";
                        Adult = 2;
                    }
                    else if (occupancys[i].adults == 3)
                    {
                        roomType = "Triple";
                        Adult = 3;
                    }
                    else if (occupancys[i].adults == 4)
                    {
                        roomType = "Quad";
                        Adult = 4;
                    }
                    roomInfoXml += string.Format(@"<RoomInfo><AdultNum RoomType=""{0}"" RQBedChild=""N"">{1}</AdultNum></RoomInfo>", roomType, Adult);
                }

                reqData = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_SearchHotel><AgentLogin><AgentId>{0}</AgentId>
<LoginName>{1}</LoginName><Password>{2}</Password></AgentLogin><SearchHotel_Request><PaxPassport>{3}</PaxPassport><hasKey>{4}</hasKey><RPCurrency>{5}</RPCurrency>
<DestCountry>{6}</DestCountry><DestCity>{7}</DestCity><HotelId>{8}</HotelId><ServiceCode>{9}</ServiceCode><Period CheckIn=""{10}"" CheckOut=""{11}"" />{12}<flagAvail>Y</flagAvail>
</SearchHotel_Request></Service_SearchHotel>", AgentID, Username, Password, dt.Rows[0]["HotelNo"].ToString(), dt.Rows[0]["SeachID"], "USD",
 dt.Rows[0]["CountryCode"], dt.Rows[0]["CityCode"], hotelID, dt.Rows[0]["ClassNo"].ToString().Split(',')[0], CheckInDate, CheckOutDate, roomInfoXml);
                try
                {
                    respData = Common.Common.PostHttp(SeachUrl, reqData);
                    LogHelper.DoOperateLog(string.Format("Studio：MgGroup复查价格 ,请求数据:{0} 返回数据 ：{1} ， Time:{2} ", reqData, respData, DateTime.Now.ToString()), "MgGroup接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:MgGroup复查价格,err:{0} ,请求数据:{1} , Time:{1} ", ex.Message, reqData, DateTime.Now.ToString()), "Err");
                    return false;
                }
                MgGroupResp.Service_SearchHotel objResp = Common.Common.DESerializer<MgGroupResp.Service_SearchHotel>(Common.Common.xmlReplace(respData).Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.SearchHotel_Response != null && objResp.SearchHotel_Response.hotels != null)
                    {
                        HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                        for (int j = 0; j < objResp.SearchHotel_Response.hotels.Hotel.Count; j++)
                        {
                            for(int k=0;k<objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg.Count;k++)
                            {
                                if (objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].BFType == dt.Rows[0]["BreakfastName"].AsTargetType<string>(""))
                                {
                                    decimal ClientPrice = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].NetPrice.AsTargetType<decimal>(0);
                                    string toRMBrate = string.Empty;
                                    string ClientCurrencyCode = objResp.SearchHotel_Response.hotels.Hotel[j].Currency;
                                    string sqlCurrency = string.Format(@"select * from Currency where CurrencyCode='{0}'", ClientCurrencyCode);
                                    DataTable dtCurrency = ControllerFactory.GetController().GetDataTable(sqlCurrency);
                                    decimal RMBprice = ClientPrice * dtCurrency.Rows[0]["ExchangeRate1"].AsTargetType<decimal>(0);
                                    decimal Price = ClientPrice;
                                    string CurrencyCode = ClientCurrencyCode;
                                    string PriceBreakdownPrice = string.Empty;
                                    for (int m = 0; m < objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType.Count; m++)
                                    {
                                        int NumRooms = objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].NumRooms.AsTargetType<int>(0);
                                        for (int t = 0; t < NumRooms; t++)
                                        {
                                            //RoomTypeSeqNo += objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].SeqNo + ";";
                                            PriceBreakdownPrice += objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg[k].RoomType[m].TotalPrice.AsTargetType<decimal>(0) / NumRooms + ";";
                                        }
                                    }
                                    string updateHotelPriceSql = string.Format(@"update HotelPrice  set UpdateTime=GETDATE(),RMBprice={0},Price={1},CurrencyCode='{2}',
CheckID='1',PriceBreakdownPrice='{3}' where HotelID='{4}' AND RateplanId='{5}' AND Platform='MgGroup'", RMBprice, ClientPrice, ClientCurrencyCode,PriceBreakdownPrice, hotelID, rateplanId);
                                    con.Update(updateHotelPriceSql);
                                    isSuccess = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup复查价格 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                isSuccess = false;
            }
            return isSuccess;
        }

        //获取取消政策
        public string GetHotelCancellationPolicy(string HotelID, string RateplanId)
        {
            Respone resp = new Respone();
            string reqData = string.Empty;
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' AND RateplanId='{1}' and  Platform = 'MgGroup'", HotelID, RateplanId);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string[] ArrayClassNames = dt.Rows[0]["ClassName"].ToString().Substring(dt.Rows[0]["ClassName"].ToString().IndexOf(':') + 1).Split('+');
                string[] ArrayEachRoomAdult = dt.Rows[0]["EachRoomAdult"].ToString().Split(';');
                string[] ArrayReferenceClient = dt.Rows[0]["ReferenceClient"].ToString().Split(',');
                //string[] ArrayRoomTypeSeqNo = dt.Rows[0]["HotelNo"].ToString().Substring(dt.Rows[0]["HotelNo"].ToString().IndexOf(':') + 1).Split(';');
                string roomInfoXml=string.Empty;
                for(int i=0;i<ArrayClassNames.Length;i++)
                {
                    roomInfoXml += string.Format(@"<RoomInfo  SeqNo=""{0}""  RoomType=""{1}"" RQBedChild=""N""><AdultNum>{2}</AdultNum></RoomInfo>", i+1, ArrayClassNames[i], ArrayEachRoomAdult[i]);
                }
                reqData = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""UTF-8""?><Service_ViewCancelPolicy><AgentLogin><AgentId>{0}</AgentId>
<LoginName>{1}</LoginName><Password>{2}</Password></AgentLogin><ViewCancelPolicy_Request><PaxPassport>{3}</PaxPassport><HotelId>{4}</HotelId><InternalCode>{5}</InternalCode>
<dtCheckIn>{6}</dtCheckIn><dtCheckOut>{7}</dtCheckOut>{8}<CancelPolicyID>{9}</CancelPolicyID><flagAvail>Y</flagAvail></ViewCancelPolicy_Request></Service_ViewCancelPolicy>",
AgentID, Username, Password, dt.Rows[0]["HotelNo"].ToString(), HotelID,ArrayReferenceClient[1], Convert.ToDateTime(dt.Rows[0]["CheckInDate"]).ToString("yyyy-MM-dd"),
Convert.ToDateTime(dt.Rows[0]["CheckOutDate"]).ToString("yyyy-MM-dd"), roomInfoXml, ArrayReferenceClient[0]);
                try
                {
                    respData = Common.Common.PostHttp(HotelViewCancelPolicyUrl, reqData);
                    LogHelper.DoOperateLog(string.Format("Studio：MgGroup获取取消政策 ,请求数据:{0} 返回数据 ：{1} ， Time:{2} ", reqData, respData, DateTime.Now.ToString()), "MgGroup接口"); 
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:MgGroup获取取消政策,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                }
                MgGroupResp.Service_ViewCancelPolicy objResp = Common.Common.DESerializer<MgGroupResp.Service_ViewCancelPolicy>(Common.Common.xmlReplace(respData));
                if (objResp != null)
                {
                    if (objResp.ViewCancelPolicy_Response != null && objResp.ViewCancelPolicy_Response.Error != null)
                    {
                        resp = new Respone() { code = "99", mes = "获取取消政策失败" + objResp.ViewCancelPolicy_Response.Error.ErrorDescription };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        string result = string.Empty;
                        if (objResp.ViewCancelPolicy_Response.Policies != null)
                        {
                            if (objResp.ViewCancelPolicy_Response.Policies.Policy.Count > 0)
                            {
                                for (int i = 0; i < objResp.ViewCancelPolicy_Response.Policies.Policy.Count; i++)
                                {
                                    string strRoomCatgCodeName=dt.Rows[0]["ClassName"].ToString().Split(':')[0];
                                    string strRoomCatgCodeBFType = dt.Rows[0]["BreakfastName"].AsTargetType<string>("");
                                    string RoomCatgCodeName = objResp.ViewCancelPolicy_Response.Policies.Policy[i].RoomCatgCode.Name;
                                    string RoomCatgCodeBFType = objResp.ViewCancelPolicy_Response.Policies.Policy[i].RoomCatgCode.BFType??"";
                                    if (strRoomCatgCodeName == RoomCatgCodeName)
                                    {
                                        decimal policieFee = 0;
                                        if (RoomCatgCodeBFType != "")
                                        {
                                            if (RoomCatgCodeBFType == strRoomCatgCodeBFType)
                                            {
                                                string ChargeType = objResp.ViewCancelPolicy_Response.Policies.Policy[i].ChargeType;//取消罚款类型
                                                string Description = objResp.ViewCancelPolicy_Response.Policies.Policy[i].Description;//取消罚款备注
                                                string ChargeRate = objResp.ViewCancelPolicy_Response.Policies.Policy[i].ChargeRate??"";//取消罚款金额
                                                int ExCancelDays = objResp.ViewCancelPolicy_Response.Policies.Policy[i].ExCancelDays.AsTargetType<int>(0);//取消天数
                                                DateTime CheckInDate = Convert.ToDateTime(dt.Rows[0]["CheckInDate"]);//入住日期
                                                if (ChargeType == "Percent")
                                                {
                                                    policieFee = dt.Rows[0]["Price"].AsTargetType<decimal>(0) * ChargeRate.AsTargetType<decimal>(0)/100 * dt.Rows[0]["RoomCount"].AsTargetType<decimal>(0);
                                                }
                                                else if (ChargeType == "Amount")
                                                {
                                                    policieFee = ChargeRate.AsTargetType<decimal>(0) * dt.Rows[0]["RoomCount"].AsTargetType<decimal>(0);
                                                }
                                                else if (ChargeType == "Full Charge" || ChargeType == "Night/Room")
                                                {
                                                    policieFee = dt.Rows[0]["Price"].AsTargetType<decimal>(0);
                                                }
                                                string PolicyDate = CheckInDate.AddDays(-ExCancelDays).ToString("yyyy-MM-dd");//取消政策日期
                                                result += "在日期之前：" + PolicyDate + ";罚款类型：" + ChargeType + ";罚款金额:" + policieFee + ";";
                                            }
                                        }
                                        else
                                        {
                                            string ChargeType = objResp.ViewCancelPolicy_Response.Policies.Policy[i].ChargeType;//取消罚款类型
                                            string Description = objResp.ViewCancelPolicy_Response.Policies.Policy[i].Description;//取消罚款备注
                                            string ChargeRate = objResp.ViewCancelPolicy_Response.Policies.Policy[i].ChargeRate;//取消罚款金额
                                            int ExCancelDays = objResp.ViewCancelPolicy_Response.Policies.Policy[i].ExCancelDays.AsTargetType<int>(0);//取消天数
                                            DateTime CheckInDate = Convert.ToDateTime(dt.Rows[0]["CheckInDate"]);//入住日期
                                            string PolicyDate = CheckInDate.AddDays(-ExCancelDays).ToString("yyyy-MM-dd");//取消政策日期
                                            if (ChargeType == "Percent")
                                            {
                                                policieFee = dt.Rows[0]["Price"].AsTargetType<decimal>(0) * ChargeRate.AsTargetType<decimal>(0)/100 * dt.Rows[0]["RoomCount"].AsTargetType<decimal>(0);
                                            }
                                            else if (ChargeType == "Amount")
                                            {
                                                policieFee = ChargeRate.AsTargetType<decimal>(0) * dt.Rows[0]["RoomCount"].AsTargetType<decimal>(0);
                                            }
                                            else if (ChargeType == "Full Charge" || ChargeType == "Night/Room")
                                            {
                                                policieFee = dt.Rows[0]["Price"].AsTargetType<decimal>(0);
                                            }
                                            result += "在日期之前：" + PolicyDate + ";罚款类型：" + ChargeType + ";罚款金额:" + policieFee + ";";
                                        }
                                    }
                                }
                            }
                            else
                            {
                                result = "Non-refundable";
                            }
                        }
                        else
                        {
                            result = "Non-refundable";
                        }
                        resp = new Respone() { code = "00", mes = result };
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：MgGroup获取取消政策 ,Err :{0} ， Time:{1} ", "获取取消政策xml解析出错", DateTime.Now.ToString()), "MgGroup接口");
                    resp = new Respone() { code = "99", mes = "获取取消政策失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup获取取消政策 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询失败" };
                return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //预定
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' AND RateplanId='{1}' and  Platform = 'MgGroup'", HotelID, RateplanId);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string[] ArrayClassNames = dt.Rows[0]["ClassName"].ToString().Substring(dt.Rows[0]["ClassName"].ToString().IndexOf(':') + 1).Split('+');
                    string[] ArrayEachRoomAdult = dt.Rows[0]["EachRoomAdult"].ToString().Split(';');
                    string[] ArrayReferenceClient = dt.Rows[0]["ReferenceClient"].ToString().Split(',');
                    //string[] ArrayRoomTypeSeqNo = dt.Rows[0]["HotelNo"].ToString().Substring(dt.Rows[0]["HotelNo"].ToString().IndexOf(':') + 1).Split(';');
                    string[] ArrayPriceBreakdownPrice = dt.Rows[0]["PriceBreakdownPrice"].ToString().Split(';');
                    string[] ArrayClassNo = dt.Rows[0]["ClassNo"].ToString().Split(',');
                    string ClassName = dt.Rows[0]["ClassName"].ToString().Split(':')[0];
                    string RoomTypeXml=string.Empty;
                    for (int i = 0; i < customers.Count; i++)
                    {
                        int Adults = 0;//输入客户信息时的客户成人数
                        string PaxInfoXml=string.Empty;
                        for (int j = 0; j < customers[i].Customers.Count; j++)
                        {
                            string Salutation = "Mrs.";
                            if (customers[i].Customers[j].sex == 2)
                            {
                                Salutation = "Mrs.";
                            }
                            else if (customers[i].Customers[j].sex == 1)
                            {
                                Salutation = "Mr.";
                            }
                            else if (customers[i].Customers[j].sex == 3)
                            {
                                Salutation = "Miss.";
                            }
                            string strPaxInfo = customers[i].Customers[j].firstName + " " + customers[i].Customers[j].lastName + "," + Salutation;
                            PaxInfoXml += string.Format(@"<PaxInfo Id=""{0}"">{1}</PaxInfo>", j + 1, strPaxInfo);
                        }
                         RoomTypeXml +=string.Format(@"<RoomType Seq=""{0}"" TypeName=""{1}"" RQBedChild =""N"" Price=""{2}"">
<AdultNum>{3}</AdultNum><PaxInformation>{4}</PaxInformation></RoomType>",i+1,
                        ArrayClassNames[i], ArrayPriceBreakdownPrice[i], ArrayEachRoomAdult[i], PaxInfoXml);
                    }
                    TimeSpan ts = DateTime.Now.ToUniversalTime() - new DateTime(1970, 1, 1);
                    //随机数
                    Random ran = new Random();
                    string strRandomResult=ran.Next(1,99).ToString();
                    string OSRefNo = (long)ts.TotalMilliseconds + strRandomResult;//13位当前时间戳 + 随机数，位数小于15位
                    postBoby = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_BookHotel><AgentLogin><AgentId>{0}</AgentId>
<LoginName>{1}</LoginName><Password>{2}</Password></AgentLogin><BookHotel><RefRsvnNo></RefRsvnNo><OSRefNo>{3}</OSRefNo><PaxPassport>{4}</PaxPassport>
<RPCurrency>{5}</RPCurrency><hasKey>{6}</hasKey><FinishBook>Y</FinishBook><HotelList Seq=""{7}"" InternalCode=""{8}"" flagAvail=""Y""><OrgHBId></OrgHBId>
<OrgResId></OrgResId><HotelId>{9}</HotelId><RequestDes></RequestDes><RoomCatg CatgId=""{10}"" CatgName=""{11}"" checkIn=""{12}"" checkOut=""{13}"" BFType=""{14}"">
{15}</RoomCatg></HotelList></BookHotel></Service_BookHotel>", AgentID, Username, Password, OSRefNo, dt.Rows[0]["HotelNo"].ToString(), "USD",
dt.Rows[0]["SeachID"], "", ArrayReferenceClient[1], HotelID, ArrayClassNo[0], ClassName, Convert.ToDateTime(dt.Rows[0]["CheckInDate"]).ToString("yyyy-MM-dd"),
Convert.ToDateTime(dt.Rows[0]["CheckOutDate"]).ToString("yyyy-MM-dd"), ArrayClassNo[1], RoomTypeXml);
                    try
                    {
                        respData = Common.Common.PostHttp(BookHotelUrl, postBoby);
                        LogHelper.DoOperateLog(string.Format("Studio：MgGroup预订 ,请求数据:{0} 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "MgGroup接口"); 
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:MgGroup预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        resp = new Respone() { code = "99", mes = "预订失败失败，" + ex.ToString() };
                    }
                    MgGroupResp.BookingHotel objResp = Common.Common.DESerializer<MgGroupResp.BookingHotel>(Common.Common.xmlReplace(respData));
                    if (objResp != null && objResp.BookHotel_Response != null)
                    {
                        if (objResp.BookHotel_Response.Error != null)
                        {
                            resp = new Respone() { code = "99", mes = "预定失败" + objResp.BookHotel_Response.Error.ErrorDescription };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        
                        Booking booking = new Booking();
                        //保存订单到数据库
                        decimal ClientPrice = 0;
                        string PriceInfomation = string.Empty;
                        for (int i = 0; i < objResp.BookHotel_Response.CompleteService.HBookId.RoomCatgInfo.RoomCatg.Room.SeqRoom.Count; i++)
                        { 
                            ClientPrice += objResp.BookHotel_Response.CompleteService.HBookId.RoomCatgInfo.RoomCatg.Room.SeqRoom[i].NetPrice.AsTargetType<decimal>(0);
                            for (int j = 0; j < objResp.BookHotel_Response.CompleteService.HBookId.RoomCatgInfo.RoomCatg.Room.SeqRoom[i].PriceInfomation.NightPrice.Count; j++)
                            { 
                                string Promotion = objResp.BookHotel_Response.CompleteService.HBookId.RoomCatgInfo.RoomCatg.Room.SeqRoom[i].PriceInfomation.NightPrice[j].Promotion.Name;
                                if (Promotion != "NONE")
                                {
                                    PriceInfomation += Promotion + ";";
                                }
                            }
                        }
                        string toRMBrate = string.Empty;
                        string ClientCurrencyCode = objResp.BookHotel_Response.ResNo.RPCurrency;
                        string sqlCurrency = string.Format(@"select * from Currency where CurrencyCode='{0}'", ClientCurrencyCode);
                        DataTable dtCurrency = ControllerFactory.GetController().GetDataTable(sqlCurrency);
                        decimal RMBprice = ClientPrice * dtCurrency.Rows[0]["ExchangeRate1"].AsTargetType<decimal>(0);
                        decimal Price = ClientPrice;
                        string CurrencyCode = ClientCurrencyCode;
                        booking.BookingCode = objResp.BookHotel_Response.ResNo.ResNoValue;//.BookingReference;
                        booking.HotelNo = objResp.BookHotel_Response.CompleteService.HBookId.Id;
                        booking.InOrderNum = inOrderNum;
                        booking.AgentBookingReference = OSRefNo;
                        booking.RatePlanId = RateplanId;
                        booking.RMBprice = RMBprice;
                        booking.ClientCurrencyCode = ClientCurrencyCode;
                        booking.ClientPartnerAmount = ClientPrice;
                        booking.HotelId = HotelID;
                        //booking.HotelNo = respObj.BookingDetail.Id;
                        booking.Platform = "MgGroup";
                        booking.StatusCode = objResp.BookHotel_Response.ResNo.Status;
                        booking.StatusName = "预订成功";
                        booking.ServiceID = 0;
                        booking.ServiceName = "";
                        booking.LegID = 0;
                        booking.Leg_Status = "";
                        booking.Status = 1;
                        booking.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                        booking.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                        booking.CreatTime = DateTime.Now;
                        booking.UpdateTime = DateTime.Now;
                        booking.EntityState = e_EntityState.Added;
                        con.Save(booking);
                        resp = new Respone() { code = "00", orderNum = OSRefNo, orderTotal = ClientPrice, mes = "预订成功." + PriceInfomation };
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：MgGroup预订 ,Err :{0} ， Time:{1} ", "预订xml解析出错", DateTime.Now.ToString()), "MgGroup接口");
                        resp = new Respone() { code = "99", mes = "预订失败" };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //取消
        public string cancel(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            try
            {
                string sql = string.Format(@"select top 1 * from Booking WITH(NOLOCK) WHERE HotelID='{0}' and  Platform = 'MgGroup' AND BookingCode='{1}'", hotelID, bookingCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                postBoby = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_CancelRSVN><AgentLogin><AgentId>{0}</AgentId><LoginName>{1}</LoginName>
<Password>{2}</Password></AgentLogin><CancelReservation><ResNo>{3}</ResNo><OSRefNo>{4}</OSRefNo><Hbooking>{5}</Hbooking></CancelReservation></Service_CancelRSVN>",
AgentID, Username, Password, bookingCode, "", dt.Rows[0]["HotelNo"]);
                try
                {
                    respData = Common.Common.PostHttp(CancelReservationUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：MgGroup退单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "MgGroup接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:MgGroup退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                MgGroupResp.Service_CancelRSVN objResp = Common.Common.DESerializer<MgGroupResp.Service_CancelRSVN>(Common.Common.xmlReplace(respData));
                if (objResp != null && objResp.CancelReservation_Response != null)
                {
                    if (objResp.CancelReservation_Response.Error != null)
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败" + objResp.CancelReservation_Response.Error.ErrorDescription };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        string strStatus = string.Empty;
                        string StatusName = string.Empty;
                        string Status = objResp.CancelReservation_Response.CancelResult;
                        string mes = string.Empty;
                        if (Status.ToLower() == "true")
                        {
                            strStatus = "CANCELCONF";
                            StatusName = "取消";
                            string updateHotelPriceSql = string.Format(@"update Booking  set UpdateTime=GETDATE(),
        StatusCode='{0}',StatusName='{1}' where HotelID='{2}' AND BookingCode='{3}' AND Platform='MgGroup'", strStatus, StatusName, hotelID, bookingCode);
                            con.Update(updateHotelPriceSql);
                            mes = "取消订单成功";
                            resp = new Respone() { code = "00", mes = mes };
                        }
                        else
                        {
                            mes = "取消订单失败";
                            resp = new Respone() { code = "99", mes = mes };
                        }
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：MgGroup取消订单 ,Err :{0} ， Time:{1} ", "取消订单json解析出错", DateTime.Now.ToString()), "MgGroup接口");
                    resp = new Respone() { code = "99", mes = "取消订单失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //查询订单
        public string CheckingOrder(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string url = string.Empty;
            string postBoby = string.Empty;
            try
            {
                string sql = string.Format(@"select top 1 * from Booking WITH(NOLOCK) WHERE HotelID='{0}' and  Platform = 'MgGroup' AND BookingCode='{1}'", hotelID, bookingCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                postBoby = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_GetRSVNInfo><AgentLogin><AgentId>{0}</AgentId><LoginName>{1}</LoginName>
<Password>{2}</Password></AgentLogin><GetRSVNInfo_Request><ResNo>{3}</ResNo><OSRefNo>{4}</OSRefNo><HBookId>{5}</HBookId></GetRSVNInfo_Request></Service_GetRSVNInfo>",
AgentID, Username, Password, bookingCode, "", dt.Rows[0]["HotelNo"]);
                try
                {
                    respData = Common.Common.PostHttp(RSVNInfoUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：MgGroup查询订单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "MgGroup接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:MgGroup查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                MgGroupResp.Service_GetRSVNInfo objResp = Common.Common.DESerializer<MgGroupResp.Service_GetRSVNInfo>(Common.Common.xmlReplace(respData));
                if (objResp != null && objResp.GetRSVNInfo_Response != null)
                {
                    if (objResp.GetRSVNInfo_Response.Error != null)
                    {
                        resp = new Respone() { code = "99", mes = "预定失败" + objResp.GetRSVNInfo_Response.Error.ErrorDescription };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        string Status = objResp.GetRSVNInfo_Response.ResNo.Status;
                        string StatusName = string.Empty;
                        if (Status == "CONF")
                        {
                            StatusName = "预订成功";
                        }
                        else if (Status == "RESERVED")
                        {
                            StatusName = "根据要求";
                        }
                        else if (Status == "NOTCONF")
                        {
                            StatusName = "不确认";
                        }
                        else if (Status == "ARESERVED")
                        {
                            StatusName = "要求修改";
                        }
                        else if (Status == "AMEND")
                        {
                            StatusName = "要求修改";
                        }
                        else if (Status == "AMENDCONF")
                        {
                            StatusName = "修改确认";
                        }
                        else if (Status == "AMENDNOTCONF")
                        {
                            StatusName = "无法确认修改";
                        }
                        else if (Status == "CANCEL")
                        {
                            StatusName = "要求取消";
                        }
                        else if (Status == "CANCELCONF")
                        {
                            StatusName = "取消确认（免费）";
                        }
                        else if (Status == "CANCELCHARGE")
                        {
                            StatusName = "取消确认收费";
                        }
                        string updateHotelPriceSql = string.Format(@"update Booking  set UpdateTime=GETDATE(),
        StatusCode='{0}',StatusName='{1}' where HotelID='{2}' AND BookingCode='{3}' AND Platform='MgGroup'",Status,StatusName, hotelID, bookingCode);
                        con.Update(updateHotelPriceSql);
                        resp = new Respone() { code = "00", orderNum = bookingCode, orderTotal = dt.Rows[0]["ClientPartnerAmount"].AsTargetType<decimal>(0), mes = "查询订单,该订单状态为:" + StatusName };
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：MgGroup查询订单 ,Err :{0} ， Time:{1} ", "查询订单json解析出错", DateTime.Now.ToString()), "MgGroup接口");
                    resp = new Respone() { code = "99", mes = "查询订单失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //查询订单
        public string BookingDetail(string hotelID, string AgentBookingReference)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string url = string.Empty;
            string postBoby = string.Empty;
            try
            {
                /*url = MgGroupUrl + string.Format(@"index.php?action=booking_detail&username={0}&password={1}&agent_ref_no={2}&gzip=no", MgGroupUsername, MgGroupPassword, AgentBookingReference);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：MgGroup查询订单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "MgGroup接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:MgGroup查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                CheckingOrderResp.CheckingOrder respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<CheckingOrderResp.CheckingOrder>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.Message == "ERROR TYPE")
                    {
                        resp = new Respone() { code = "99", mes = "查询失败" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string strErr = string.Empty;
                    if (respObj.Message == "fail")
                    {
                        strErr = respObj.MessageInfo;
                        LogHelper.DoOperateLog(string.Format("Err：MgGroup查询订单 ,Err :{0} ， Time:{1} ", strErr, DateTime.Now.ToString()), "MgGroup接口");
                        resp = new Respone() { code = "99", mes = "查询订单" + strErr };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string sql = string.Format(@"select top 1 * from Booking WITH(NOLOCK) WHERE HotelID='{0}' AND AgentBookingReference='{1}' and  Platform = 'MgGroup'", hotelID, AgentBookingReference);
                    DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                    resp = new Respone() { code = "00", orderNum = dt.Rows[0]["BookingCode"].AsTargetType<string>(""), orderTotal = dt.Rows[0]["RMBprice"].AsTargetType<decimal>(0), mes = "查询订单,该订单状态为:" + dt.Rows[0]["StatusName"] };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：MgGroup查询订单 ,Err :{0} ， Time:{1} ", "查询订单json解析出错", DateTime.Now.ToString()), "MgGroup接口");
                    resp = new Respone() { code = "99", mes = "查询订单失败" };
                }*/
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //获取取消费用
        public string GetCancellationFee(string HotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            try
            {
                string sql = string.Format(@"select top 1 * from Booking WITH(NOLOCK) WHERE HotelID='{0}' and  Platform = 'MgGroup' AND BookingCode='{1}'", HotelID, bookingCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                postBoby = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_GetCancelPolicy><AgentLogin><AgentId>{0}</AgentId><LoginName>{1}</LoginName>
<Password>{2}</Password></AgentLogin><GetCancelPolicy_Request><ResNo>{3}</ResNo><OSRefNo>{4}</OSRefNo><HBooking>{5}</HBooking><clxDate></clxDate></GetCancelPolicy_Request></Service_GetCancelPolicy>",
AgentID, Username, Password, bookingCode, "", dt.Rows[0]["HotelNo"]);
                try
                {
                    respData = Common.Common.PostHttp(CancelPolicyUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：MgGroup获取取消费用 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "MgGroup接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:MgGroup获取取消费用 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "获取取消费用失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                MgGroupResp.Service_GetCancelPolicy objResp = Common.Common.DESerializer<MgGroupResp.Service_GetCancelPolicy>(Common.Common.xmlReplace(respData));
                 if (objResp != null && objResp.GetCancelPolicy_Response != null)
                 {
                     if (objResp.GetCancelPolicy_Response.Error != null)
                     {
                         resp = new Respone() { code = "99", mes = "获取取消费用失败" + objResp.GetCancelPolicy_Response.Error.ErrorDescription };
                         return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                     }
                     else
                     {
                         if (objResp.GetCancelPolicy_Response.Policies != null)
                         {
                             string result = string.Empty;
                             if (objResp.GetCancelPolicy_Response.Policies != null)
                             {
                                 for (int i = 0; i < objResp.GetCancelPolicy_Response.Policies.Policy.Count; i++)
                                 {
                                     string ChargeType = objResp.GetCancelPolicy_Response.Policies.Policy[i].ChargeType;//取消罚款类型
                                     string Description = objResp.GetCancelPolicy_Response.Policies.Policy[i].Description;//取消罚款备注
                                     string ChargeRate = objResp.GetCancelPolicy_Response.Policies.Policy[i].ChargeRate;//取消罚款金额
                                     int ExCancelDays = objResp.GetCancelPolicy_Response.Policies.Policy[i].ExCancelDays.AsTargetType<int>(0);//取消天数
                                     DateTime CheckInDate = Convert.ToDateTime(dt.Rows[0]["CheckInDate"]);//入住日期
                                     string PolicyDate = CheckInDate.AddDays(-ExCancelDays).ToString("yyyy-MM-dd");//取消政策日期
                                     result += "在日期之前：" + PolicyDate + ";罚款类型：" + ChargeType + ";罚款金额:" + ChargeRate + ";";
                                 }
                                 resp = new Respone() { code = "00", mes = result };
                             }
                         }
                     }
                 }
                 else
                 {
                     LogHelper.DoOperateLog(string.Format("Err：MgGroup获取取消费用 ,Err :{0} ， Time:{1} ", "获取取消费用xml解析出错", DateTime.Now.ToString()), "MgGroup接口");
                     resp = new Respone() { code = "99", mes = "获取取消费用失败" };
                 }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取取消费用 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "获取取消费用失败," + ex.ToString() };
                return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //查询报价
        public string GetSunSeriesRoomTypeOutPrice(string hotelID, string CheckInDate, string CheckOutDate, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            //DataTable dt = new DataTable();
            string respData = string.Empty;
            string postBoby = string.Empty;

            int adults = occupancy.adults;
            int children = occupancy.children;
            string strChildAges = occupancy.childAges ?? "";
            string CheckIn = Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd");
            string Checkout = Convert.ToDateTime(CheckOutDate).ToString("yyyy-MM-dd");
            string StrKey = hotelID + "%" + CheckIn + "%" + Checkout + "%" + adults + "%" + children + "%" + strChildAges + "%" + RoomCount;
            int IsContain = 0;//报价是否存在这条记录
            string roomType = string.Empty;
            if (adults == 1)
            {
                roomType = "Single";
            }
            else if (adults == 2)
            {
                roomType = "Double/Twin";
            }
            else if (adults == 3)
            {
                roomType = "Triple";
            }
            else if (adults == 4)
            {
                roomType = "Quad";
            }
            string roomInfoXml = string.Empty;
            for (int i = 0; i < RoomCount; i++)
            {
                roomInfoXml += string.Format(@"<RoomInfo><AdultNum RoomType=""{0}"" RQBedChild=""N"">{1}</AdultNum></RoomInfo>", roomType, adults);
            }

            try
            {
                string sql = string.Format(@"select * from MgGroupLink_RoomsCache WITH(NOLOCK) where StrKey='{0}';
                select ht.* from TravelIntl.dbo.MgGroupHotel ht WITH(NOLOCK) where HotelID='{1}';", StrKey, hotelID);
                DataSet ds = con.GetDataSet(sql);
                DataTable dt = ds.Tables[0];
                DataTable dtHotel = ds.Tables[1];
                if (dtHotel.Rows.Count == 0)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
                if ((RateplanId == null || RateplanId == "") && dt.Rows.Count == 0 || dt.Rows[0]["IsClose"].AsTargetType<int>(0) == 0)
                {
                    postBoby = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_SearchHotel><AgentLogin><AgentId>{0}</AgentId>
<LoginName>{1}</LoginName><Password>{2}</Password></AgentLogin><SearchHotel_Request><PaxPassport>{3}</PaxPassport><hasKey>{4}</hasKey><RPCurrency>{5}</RPCurrency>
<DestCountry>{6}</DestCountry><DestCity>{7}</DestCity><HotelId>{8}</HotelId><ServiceCode></ServiceCode><Period CheckIn=""{9}"" CheckOut=""{10}"" />{11}<flagAvail>Y</flagAvail>
</SearchHotel_Request></Service_SearchHotel>", AgentID, Username, Password, "国籍编码", "", "USD", dtHotel.Rows[0]["CountryCode"], dtHotel.Rows[0]["CityCode"], hotelID,
                                                 CheckInDate, CheckOutDate, roomInfoXml);
                    try
                    {
                        respData = Common.Common.PostHttp(SeachUrl, postBoby);
                        LogHelper.DoOperateLog(string.Format("Studio：MgGroup查询报价 ,请求数据:{0} 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "MgGroup接口");
                        SunSeriesLink_RoomsCache roomsCache = new SunSeriesLink_RoomsCache();
                        roomsCache.StrKey = StrKey;
                        roomsCache.HotelId = hotelID;
                        roomsCache.Checkin = CheckIn.AsTargetType<DateTime>(DateTime.MinValue);
                        roomsCache.CheckOut = Checkout.AsTargetType<DateTime>(DateTime.MinValue);
                        //ssRoomsCache.Request = postBoby;
                        roomsCache.Response = respData;
                        roomsCache.CreatTime = DateTime.Now;
                        roomsCache.UpdateTime = DateTime.Now;
                        roomsCache.IsClose = 1;
                        roomsCache.EntityState = e_EntityState.Added;
                        if (dt.Rows.Count == 0)
                        {
                            //con2.Save(roomsCache);
                        }
                        else
                        {
                            string updateRoomsCacheSql = string.Format(@"update MgGroupLink_RoomsCache set Response='{0}',UpdateTime=GETDATE(),IsClose=1 where StrKey='{1}'", respData, StrKey);
                            //con2.Update(updateRoomsCacheSql);
                        }

                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:MgGroup查询报价,err:{0} ,请求数据:{1} , Time:{1} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                }
                else
                {
                    postBoby = string.Format(@"requestXML=<?xml version=""1.0"" encoding=""utf-8"" ?><Service_SearchHotel><AgentLogin><AgentId>{0}</AgentId>
<LoginName>{1}</LoginName><Password>{2}</Password></AgentLogin><SearchHotel_Request><PaxPassport>{3}</PaxPassport><hasKey>{4}</hasKey><RPCurrency>{5}</RPCurrency>
<DestCountry>{6}</DestCountry><DestCity>{7}</DestCity><HotelId>{8}</HotelId><ServiceCode>{9}</ServiceCode><Period CheckIn=""{10}"" CheckOut=""{11}"" />{12}<flagAvail>Y</flagAvail>
</SearchHotel_Request></Service_SearchHotel>", AgentID, Username, Password, "国籍信息", RateplanId.Split('_')[0], "USD",
                    dtHotel.Rows[0]["CountryCode"], dtHotel.Rows[0]["CityCode"], hotelID, RateplanId.Split('_')[1], CheckInDate, CheckOutDate, roomInfoXml);
                    try
                    {
                        respData = Common.Common.PostHttp(SeachUrl, postBoby);
                        LogHelper.DoOperateLog(string.Format("Studio：MgGroup复查价格 ,请求数据:{0} 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "MgGroup接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:MgGroup复查价格,err:{0} ,请求数据:{1} , Time:{1} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                }
                 MgGroupResp.Service_SearchHotel objResp = Common.Common.DESerializer<MgGroupResp.Service_SearchHotel>(Common.Common.xmlReplace(respData).Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                 if (objResp != null)
                 {
                     if (objResp.SearchHotel_Response != null && objResp.SearchHotel_Response.hotels != null)
                     {
                         HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                         for (int j = 0; j < objResp.SearchHotel_Response.hotels.Hotel.Count; j++)
                         {
                             for (int k = 0; k < objResp.SearchHotel_Response.hotels.Hotel[j].RoomCateg.Count; k++)
                             {
                             }
                         }
                     }
                 }
                 else
                 {
                     string updateRoomsCacheSql = string.Format(@"update MgGroupLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                     //con2.Update(updateRoomsCacheSql);
                     LogHelper.DoOperateLog(string.Format("Err：MgGroup获取报价 ,Err :{0} ， Time:{1} ", "json解析为空", DateTime.Now.ToString()), "MgGroup接口");
                     return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                 }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:MgGroup获取报价,err:{0} ,请求数据:{1} , Time:{1} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }

        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
    }
}