﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using ServiceStack.Redis;

namespace XiWan.Data.Redis
{
    public class RedisHelper
    {
        private static RedisClient Redis = null;

        static RedisHelper()
        {
            Redis = new RedisClient("127.0.0.1", 6379);
        }

        public static void SetMem<T>(T t, string key, int minute)
        {
            if (minute == 0)
            {
                Redis.Set<T>(key, t);
            }
            else
            {
                DateTime expiryTime = DateTime.Now.AddMinutes(minute);
                Redis.Set<T>(key, t, expiryTime);
            }   
        }

        public static T GetMem<T>(string key)
        {
            return Redis.Get<T>(key);
        }
        /// <summary>
        /// 插入DATASET缓存
        /// </summary>
        /// <param name="key">缓存键</param>
        /// <param name="item">缓存对象</param>
        /// <param name="minute">过期时间（分钟）</param>
        public static void SetMemByDataSet(string key, DataTable dt, int minute)
        {

            DateTime expiryTime = DateTime.Now.AddMinutes(minute);
            System.Runtime.Serialization.IFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();//定义BinaryFormatter以序列化DataSet对象   
            System.IO.MemoryStream ms = new System.IO.MemoryStream();//创建内存流对象   
            formatter.Serialize(ms, dt);//把dt对象序列化到内存流   
            byte[] buffer = ms.ToArray();//把内存流对象写入字节数组   
            ms.Close();//关闭内存流对象   
            ms.Dispose();//释放资源   
            if (minute == 0)
            {
                Redis.Set(key, buffer);
            }
            else
            {
                Redis.Set(key, buffer, expiryTime);
            }
        }
        public static object Get(string key)
        {
            byte[] buffer = Redis.Get(key);

            return GetObjFromBytes(buffer);
        }
        /// <summary>
        /// 从二进制流得到对象（data专用，data数据要序列化为二进制才可保存
        /// /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private static object GetObjFromBytes(byte[] buffer)
        {
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream(buffer))
            {
                stream.Position = 0;
                System.Runtime.Serialization.IFormatter bf = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                Object reobj = bf.Deserialize(stream);
                return reobj;
            }
        }
        /// <summary>
        /// 判断是否失效或没有
        /// </summary>如果为false已失效
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool IsExpired(string key)
        {
            bool result = true;
            byte[] buffer = Redis.Get(key);
            if (buffer == null)
            {
                result = false;
            }
            return result;
        }
        public static DataTable GetMemByDataSet(string key)
        {
            var item = Get(key);
            return (DataTable)item;
        }
        /// <summary>
        /// 清空缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public static bool Remove(string key)
        {
            return Redis.Remove(key);
        }
        /// <summary>
        /// 清空所有缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="t"></param>
        public static void FlushAll()
        {
            Redis.FlushAll();
        }


    }
}
