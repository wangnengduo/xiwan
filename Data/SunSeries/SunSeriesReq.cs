﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.SunSeries
{
    public class SunSeriesReq
    {
        #region 获取token
        public class AuthenticateReqData
        {
            public string email { get; set; }
            public string password { get; set; }
        }
        public class AuthenticateRespData
        {
            public string token { get; set; }
            public string type { get; set; }
        }
        #endregion

        #region 获取国家
        public class SunSeriesCountryRespData
        {
            public string code { get; set; }
            public string name { get; set; }
            public string id { get; set; }
        }
        #endregion

        #region 获取城市
        public class SunSeriesCityRespData
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
            public string country_id { get; set; }
        }
        #endregion

        #region 获取国籍
        public class SunSeriessNationalityRespData
        {
            public string id { get; set; }
            public string name { get; set; }
            public string code { get; set; }
        }
        #endregion

        #region 获取酒店
        public class SunSeriesHotelsRespData
        {
            public string id { get; set; }
            public string name { get; set; }
            public string stars { get; set; }
            public string service_id { get; set; }
            public string city_id { get; set; }
            public string area_id { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
            public string telephone { get; set; }
            public string address { get; set; }
            public List<Images> images { get; set; }
        }
        public class Images
        {
            //public string is_primary { get; set; }
            //public string id { get; set; }
            public string image_url { get; set; }
            //public string cloudinary_name { get; set; }
            //public string thumbnail_url { get; set; }
            //public string public_id { get; set; }
        }
        #endregion

        #region 获取报价
        public class CriteriaReqData
        {
            public criteria criteria { get; set; }
        }
        public class criteria
        {
            public string from { get; set; }
            public string to { get; set; }
            public List<string> hotel_ids { get; set; }
            public string customer_country { get; set; }
            public string city { get; set; }
            public List<rooms> rooms { get; set; }
        }
        public class rooms
        {
            public int adults { get; set; }
            public List<children> children { get; set; }
        }
        public class children
        {
            public int age { get; set; }
        }

        public class CriteriaRespData
        {
            public string id { get; set; }
            public List<roomsResp> rooms { get; set; }
            public room_class_information room_class_information { get; set; }
            public string lowest_price { get; set; }
            public string currency { get; set; }
            public bool valid { get; set; }
            public string description { get; set; }
            public string image { get; set; }
            public string service_id { get; set; }
            public string name { get; set; }
        }
        public class roomsResp
        {
            public string index { get; set; }
            public List<room_classes> room_classes { get; set; }
        }
        public class room_classes
        {
            public string name { get; set; }
            public string id { get; set; }
            public List<configurations> configurations { get; set; }
            //public string name { get; set; }
            //public string name { get; set; }
            //public string name { get; set; }
            public List<samples> samples { get; set; }
        }
        public class configurations
        {
            public string name { get; set; }
            public bool extra_bed { get; set; }
        }
        public class samples
        {
            public string available { get; set; }
            public int rooms_left { get; set; }//剩下房间数
            public string type { get; set; }
            public string  incentive { get; set; }
			public string name { get; set; }
			public string description { get; set; }
            public List<promotions> promotions{get; set;}
		    public string currency { get; set; }
			public string total { get; set; }
			public string cancellation_date { get; set; }
			public string id { get; set; }
			public string rate_code { get; set; }
            public List<available_options> available_options{get;set;}
            public List<nights> nights { get; set; }
        }
        public class promotions
        {
            public string name { get; set; }
			public string description { get; set; }
			public string type { get; set; }
            public bool inflexible { get; set; }
        }
        public class available_options
        {
            public string id { get; set; }
			public string name { get; set; }
			public string rate { get; set; }
            public bool currency { get; set; }
            public bool compulsory { get; set; }
			public string applies_to { get; set; }
            public bool applies_every { get; set; }
        }
        public class nights
        { 
            public string date { get; set; }
			public string rate { get; set; }
			public string currency { get; set; }
            public List<options> options { get; set; }
        }
        public class options
        { 
            public string id { get; set; }
			public string name { get; set; }
			public string rate { get; set; }
			public string currency { get; set; }
			public string compulsory { get; set; }
			public string applies_to { get; set; }
            public string applies_every { get; set; }
        }
        public class room_class_information
        {
            public List<roomsInformation> rooms { get; set; }
            public minimum_night_stay minimum_night_stay { get; set; }
            public rates rates { get; set; }
            public bool is_available { get; set; }
        }
        public class roomsInformation
        {
            public List<room_classesInformation> room_classes { get; set; }
        }
        public class room_classesInformation
        {
            public string id { get; set; }
            public string name { get; set; }
            public bool occupant_support { get; set; }
            public configurationsInformation configurations { get; set; }
        }
        public class configurationsInformation
        {
            public string name { get; set; }
            public bool extra_bed { get; set; }
        }
        public class minimum_night_stay
        {
            public bool selection_available { get; set; }
            public string nights_required { get; set; }
        }
        public class rates
        {
            public bool selection_available { get; set; }
        }
        #endregion

        #region 重新报价
        public class CreatequoteReqData
        {
            public List<CreatequoteRooms> rooms { get; set; }
        }
        public class CreatequoteRooms
        {
            public string sample_id { get; set; }
            public string bed_configuration { get; set; }
            public List<string> option_ids { get; set; }
        }

        public class CreatequoteRespData
        {
            public string id { get; set; }
            public string creation_date { get; set; }
            public string agent_user_id { get; set; }
            public string agent_id { get; set; }
            public string service_id { get; set; }
            public List<string> period { get; set; }
            public string search_id { get; set; }
            public string result_id { get; set; }
            public string provider_id { get; set; }
            public criteriaQuoteResp criteria { get; set; }
            public List<roomsQuoteResp> rooms { get; set; }
            public string cancellation_date { get; set; }  
        }
        public class criteriaQuoteResp
        {
            public List<roomscriteriaQuoteResp> rooms { get; set; }
            public string id { get; set; }
            public string city { get; set; }
            public string from { get; set; }
            public string to { get; set; }
            public string relative_today_date { get; set; }
            public string replace_booking_id { get; set; }
            public string customer_country { get; set; }
            public List<string> hotel_service_ids { get; set; }
            public string room_class_id { get; set; }
            public string price_from { get; set; }
            public string source { get; set; }
            public string agent_id { get; set; }
            public string user_id { get; set; }
            public string providers { get; set; }
            public string price_to { get; set; }
            public bool instant_confirm_only { get; set; }
        }
        public class roomscriteriaQuoteResp
        {
            public List<occupants> occupants { get; set; }
        }
        public class occupants
        {
            public string id { get; set; }
        }
        public class roomsQuoteResp
        {
            public List<occupants> occupants { get; set; }
            public itemization itemization { get; set; }
            public string id { get; set; }
            public string bed_configuration { get; set; }
            public string sample { get; set; }
        }
        public class itemization
        {
            public List<nights> nights { get; set; }
            public string currency { get; set; }
            public string total { get; set; }
            public string incentive { get; set; }
            public room_class room_class { get; set; }
        }
        public class room_class
        {
            public string id { get; set; }
            public string name { get; set; }
        }
        #endregion

        #region 预订
        public class SunSeriesBookReqData
        {
            public List<string> remarks { get; set; }
            public string agent_reference { get; set; }
            public string departure_flight_number { get; set; }
            public string departure_flight_time { get; set; }
            public string arrival_flight_number { get; set; }
            public string arrival_flight_time { get; set; }
            public List<string> notification_recipients { get; set; }
            public List<roomsBookReq> rooms { get; set; }
        }
        public class roomsBookReq
        {
            public List<adultsBookReq> adults { get; set; }
            public List<childrenBookReq> children { get; set; }
        }
        public class adultsBookReq
        {
            public string title { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
        }
        public class childrenBookReq
        {
            public string first_name { get; set; }
            public string last_name { get; set; }
        }

        public class SunSeriesBookRespData
        {
            public string booking_id { get; set; }
            public string state { get; set; }
        }
        #endregion

        #region 查询订单
        public class SunSeriesCheckReqData
        {
            public criteriaCheckReq criteria { get; set; }
        }
        public class criteriaCheckReq
        {
            public creation_date creation_date { get; set; }
            public checkin_date checkin_date { get; set; }
            public List<string> state { get; set; }
            public string user { get; set; }
            public string agent_reference { get; set; }
            public string service_id { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
        }
        public class creation_date
        {
            public string from { get; set; }
            public string to { get; set; }
        }
        public class checkin_date
        {
            public string from { get; set; }
            public string to { get; set; }
        }

        public class SunSeriesCheckRespData
        {
            public string id { get; set; }
            public string reference { get; set; }
            public string current_state { get; set; }
            public string service_id { get; set; }
            public string leading_guest_title { get; set; }
            public string leading_guest_first_name { get; set; }
            public string leading_guest_last_name { get; set; }
            public string from { get; set; }
            public string to { get; set; }
            public string price { get; set; }
            public string currency { get; set; }
            public string date { get; set; }
            public string cancellation_date { get; set; }
            public string agent_reference { get; set; }
            public string hotel_name { get; set; }
            public hotel_image hotel_image { get; set; }
            public string hotel_confirmation_number { get; set; }
        }
        public class hotel_image
        {
            public string is_primary { get; set; }
            public string id { get; set; }
            public string image_url { get; set; }
            public string cloudinary_name { get; set; }
            public string thumbnail_url { get; set; }
            public string public_id { get; set; }
        }
        #endregion

        #region 取消订单
        public class cancelRespData
        {
            public string id { get; set; }
            public string reference { get; set; }
            public string current_state { get; set; }
            public string agent_user_id { get; set; }
            public DateTime date { get; set; }
            public List<Remarks> remarks { get; set; }
            public DateTime cancellation_date { get; set; }
            public List<DateTime> period { get; set; }
            public string invoice_id { get; set; }
            public Total_price total_price { get; set; }
            public Flight_information flight_information { get; set; }
            public string confirm_type { get; set; }
            public List<Rooms> rooms { get; set; }
            public Criteria criteria { get; set; }
            public string city_id { get; set; }
            public string service_id { get; set; }
            public string agent_reference { get; set; }
            public string hotel_confirmation_number { get; set; }
        }
        public class Remarks
        {
            public string id { get; set; }
            public string text { get; set; }
            public string user_id { get; set; }
        }
        public class Total_price
        {
            public string currency { get; set; }
            public int cents { get; set; }
            public string locked_exchange_rates { get; set; }
        }
        public class Departure_flight
        {
            public string number { get; set; }
            public string time { get; set; }
        }

        public class Arrival_flight
        {
            public string number { get; set; }
            public string time { get; set; }
        }

        public class Flight_information
        {
            public Departure_flight departure_flight { get; set; }
            public Arrival_flight arrival_flight { get; set; }
        }

        public class Options
        {
            public string name { get; set; }
            public string rate { get; set; }
            public string currency { get; set; }
        }

        public class Nights
        {
            public DateTime date { get; set; }
            public string rate { get; set; }
            public string currency { get; set; }
            public List<Options> options { get; set; }
        }

        public class Room_class
        {
            public string id { get; set; }
            public string name { get; set; }
        }

        public class Promotions
        {
            public string name { get; set; }
            public string description { get; set; }
            public string type { get; set; }
        }

        public class Itemization
        {
            public List<Nights> nights { get; set; }
            public string currency { get; set; }
            public string total { get; set; }
            public string incentive { get; set; }
            public Room_class room_class { get; set; }
            public List<Promotions> promotions { get; set; }
        }

        public class Occupants
        {
            public string id { get; set; }
            public string title { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
        }

        public class Rooms
        {
            public string bed_configuration { get; set; }
            public Itemization itemization { get; set; }

            public List<Occupants> occupants { get; set; }
        }

        public class CriteriaRooms
        {
            public List<Occupants> occupants { get; set; }
        }

        public class Criteria
        {
            public List<CriteriaRooms> rooms { get; set; }
            public string id { get; set; }
            public string city { get; set; }
            public DateTime from { get; set; }
            public DateTime to { get; set; }
            public string relative_today_date { get; set; }
            public string replace_booking_id { get; set; }
            public string customer_country { get; set; }
            public List<string> hotel_service_ids { get; set; }
            public string room_class_id { get; set; }
            public string price_from { get; set; }
            public string source { get; set; }
            public string agent_id { get; set; }
            public string user_id { get; set; }
            public int providers { get; set; }
            public string price_to { get; set; }
            public bool instant_confirm_only { get; set; }
        }

        #endregion
    }
}