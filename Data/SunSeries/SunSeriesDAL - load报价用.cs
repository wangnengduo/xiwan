﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Data.common;
using XiWan.Model.Entities;

namespace XiWan.Data.SunSeries
{
    public class SunSeriesDAL
    {
        //private static HttpSessionState _session = HttpContext.Current.Session;
        string USDtoRMBrate = CCommon.GetWebConfigValue("USDtoRMBrate");//美元转为人民币汇率
        string EURtoRMBrate = CCommon.GetWebConfigValue("EURtoRMBrate");//欧元转为人民币汇率
        string SGDtoRMBrate = CCommon.GetWebConfigValue("SGDtoRMBrate");//新加坡元转为人民币汇率
        string HKDtoRMBrate = CCommon.GetWebConfigValue("HKDtoRMBrate");//港币转为人民币汇率
        string SunSeriesTime = CCommon.GetWebConfigValue("SunSeriesTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        string url = CCommon.GetWebConfigValue("SunSeriesUrl");//url
        string Username = CCommon.GetWebConfigValue("SunSeriesUsername");//
        string Password = CCommon.GetWebConfigValue("SunSeriesPassword");//
        int SunSeriesTokenCacheTime = CCommon.GetWebConfigValue("SunSeriesTokenCacheTime").AsTargetType<int>(0);//Token有效时长（秒）
        string SunSeriesCacheTime = CCommon.GetWebConfigValue("SunSeriesCacheTime");//缓存到数据库的有效期（分钟）

        #region 初始化本类
        private static SunSeriesDAL _Instance;
        public static SunSeriesDAL Instance
        {
            get
            {
                return new SunSeriesDAL();
            }
        }
        #endregion

        /// <summary>
        /// 获取token
        /// </summary>
        public string GetAuthenticate()
        {
            string result=string.Empty;
            try
            {
                result = CacheHelper.GetCache("SunSeriesToken").AsTargetType<string>("");//先读取缓存
                if (result == null || result == "")//如果没有该缓存
                {
                    string respData = string.Empty;
                    string postBoby = string.Empty;
                    string authenticateUrl = url + "/v1/users/authenticate";
                    SunSeriesReq.AuthenticateReqData req = new SunSeriesReq.AuthenticateReqData() { email = Username, password = Password };
                    postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    try
                    {
                        respData = Common.Common.PostHttp(authenticateUrl, postBoby);
                        LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取token ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:SunSeries获取token ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        return result;
                    }
                    SunSeriesReq.AuthenticateRespData authenticateInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.AuthenticateRespData>(respData, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (authenticateInfo != null && postBoby != "")
                    {
                        CacheHelper.SetCache("SunSeriesToken", authenticateInfo.token, SunSeriesTokenCacheTime);//添加缓存
                        result = authenticateInfo.token;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取token ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return result;
        }
        /// <summary>
        /// 获取国家
        /// </summary>
        public void GetSunSeriesCountries()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getCountriesUrl = url + string.Format(@"/v1/countries?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getCountriesUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取国家 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国家,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriesCountryRespData> hotelsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesCountryRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (hotelsInfo != null)
                {
                    for (int i = 0; i < hotelsInfo.Count; i++)
                    {
                        SunSeriesCountry ssh = new SunSeriesCountry();
                        ssh.CountryCode = hotelsInfo[i].code;
                        ssh.CountryName = string.IsNullOrEmpty(hotelsInfo[i].name) ? "" : hotelsInfo[i].name;
                        ssh.CountryID = string.IsNullOrEmpty(hotelsInfo[i].id) ? "" : hotelsInfo[i].id;
                        con.Save(ssh);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国家 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        /// <summary>
        /// 获取城市
        /// </summary>
        public void GetSunSeriesCities()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getCitiesUrl = url + string.Format(@"/v1/cities?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getCitiesUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取城市 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取城市,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriesCityRespData> cityInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesCityRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (cityInfo != null)
                {
                    for (int i = 0; i < cityInfo.Count; i++)
                    {
                        SunSeriesCity city = new SunSeriesCity();
                        city.CityID = string.IsNullOrEmpty(cityInfo[i].id) ? "" : cityInfo[i].id;
                        city.CityName = string.IsNullOrEmpty(cityInfo[i].name) ? "" : cityInfo[i].name;
                        city.CountryCode = string.IsNullOrEmpty(cityInfo[i].code) ? "" : cityInfo[i].code;
                        city.CountryID = string.IsNullOrEmpty(cityInfo[i].country_id) ? "" : cityInfo[i].country_id;
                        con.Save(city);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取城市 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        /// <summary>
        /// 获取国籍
        /// </summary>
        public void GetSunSeriesNationalities()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getNationalitiesUrl = url + string.Format(@"/v1/nationalities?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getNationalitiesUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取国籍 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国籍,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriessNationalityRespData> nationalityInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriessNationalityRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (nationalityInfo != null)
                {
                    for (int i = 0; i < nationalityInfo.Count; i++)
                    {
                        SunSeriesNationality nationality = new SunSeriesNationality();
                        nationality.CountryID = string.IsNullOrEmpty(nationalityInfo[i].id) ? "" : nationalityInfo[i].id;
                        nationality.CountryName = string.IsNullOrEmpty(nationalityInfo[i].name) ? "" : nationalityInfo[i].name;
                        nationality.CountryCode = string.IsNullOrEmpty(nationalityInfo[i].code) ? "" : nationalityInfo[i].code;
                        con.Save(nationality);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国籍 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        /// <summary>
        /// 获取酒店
        /// </summary>
        public void GetSunSeriesHotels()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getHotelUrl = url + string.Format(@"/v1/hotels?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getHotelUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取酒店 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取酒店,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriesHotelsRespData> hotelsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesHotelsRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (hotelsInfo != null)
                {
                    for (int i = 0; i < hotelsInfo.Count; i++)
                    {
                        SunSeriesHotel ssh = new SunSeriesHotel();
                        ssh.HotelID = hotelsInfo[i].id;
                        ssh.HotelName = string.IsNullOrEmpty(hotelsInfo[i].name) ? "" : hotelsInfo[i].name;
                        ssh.stars = string.IsNullOrEmpty(hotelsInfo[i].stars) ? "" : hotelsInfo[i].stars;
                        ssh.CityId = string.IsNullOrEmpty(hotelsInfo[i].city_id) ? "" : hotelsInfo[i].city_id;
                        ssh.AreaId = string.IsNullOrEmpty(hotelsInfo[i].area_id) ? "" : hotelsInfo[i].area_id;
                        ssh.Address = string.IsNullOrEmpty(hotelsInfo[i].address) ? "" : hotelsInfo[i].address;
                        ssh.Telephone = string.IsNullOrEmpty(hotelsInfo[i].telephone) ? "" : hotelsInfo[i].telephone;
                        ssh.Longitude = string.IsNullOrEmpty(hotelsInfo[i].longitude) ? "" : hotelsInfo[i].longitude;
                        ssh.Latitude = string.IsNullOrEmpty(hotelsInfo[i].latitude) ? "" : hotelsInfo[i].latitude;
                        con.Save(ssh);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取酒店 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationality()
        {
            string sql = "select CountryID as ID,CountryName as NAME from SunSeriesNationality ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }

        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestination(string strName)
        {
            string sql = string.Format(@"SELECT CityName as name,(CityID+','+CountryID) as value FROM SunSeriesCity where CityName like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        //获取报价
        public DataTable GetRoomTypePrice(string hotelID, string beginTime, string endTime, List<IntOccupancyInfo> occupancy, int RoomCount, string RateplanId)
        {
            DataTable dt = new DataTable();
            //List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getPriceUrl = url + string.Format(@"/v1/search_with_results?token={0}", sunSeriesToken);
            try
            {
                string sql1 = string.Format(@"SELECT * FROM SunSeriesHotel");
                DataTable dt1 = ControllerFactory.GetController().GetDataTable(sql1);
                for (int pp = 0; pp < dt1.Rows.Count; pp++)
                {
                    hotelID = dt1.Rows[pp]["hotelID"].AsTargetType<string>("");
                    string sqlHotel = string.Format(@"select ht.*,ssc.CountryID from SunSeriesHotel ht WITH(NOLOCK) LEFT JOIN dbo.SunSeriesCity ssc  WITH(NOLOCK) ON ssc.CityID=ht.CityId where HotelID='{0}' ", hotelID);
                    DataTable dtHotel = ControllerFactory.GetController().GetDataTable(sqlHotel);
                    SunSeriesReq.CriteriaReqData crd = new SunSeriesReq.CriteriaReqData();

                    List<SunSeriesReq.rooms> reqRooms = new List<SunSeriesReq.rooms>();
                    for (int i = 0; i < occupancy.Count; i++)
                    {
                        SunSeriesReq.rooms reqRoom = new SunSeriesReq.rooms();
                        List<SunSeriesReq.children> reqChildrens = new List<SunSeriesReq.children>();

                        if (occupancy[i].childAges != null && occupancy[i].childAges != "")
                        {
                            string[] ArrayAges = occupancy[i].childAges.Split(',');
                            for (int k = 0; k < ArrayAges.Length; k++)
                            {
                                SunSeriesReq.children reqChildren = new SunSeriesReq.children();
                                reqChildren.age = ArrayAges[k].AsTargetType<int>(0);
                                reqChildrens.Add(reqChildren);
                            }
                        }


                        reqRoom.adults = occupancy[i].adults;
                        reqRoom.children = reqChildrens;

                        reqRooms.Add(reqRoom);
                    }

                    List<string> hotel_ids = new List<string>();
                    hotel_ids.Add(hotelID);

                    SunSeriesReq.criteria cr = new SunSeriesReq.criteria();
                    cr.from = beginTime;
                    cr.to = endTime;
                    cr.hotel_ids = hotel_ids;
                    cr.customer_country = dtHotel.Rows[0]["CountryID"].AsTargetType<string>("");
                    cr.city = dtHotel.Rows[0]["CityID"].AsTargetType<string>("");
                    cr.rooms = reqRooms;

                    crd.criteria = cr;
                    postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(crd);
                    try
                    {
                        respData = Common.Common.PostHttp(getPriceUrl, postBoby);
                        LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        return dt;
                    }
                    List<SunSeriesReq.CriteriaRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.CriteriaRespData>>(respData, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (respObj != null)
                    {
                        HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                        for (int k = 0; k < respObj.Count; k++)
                        {
                            for (int l = 0; l < respObj[k].rooms.Count; l++)
                            {
                                for (int m = 0; m < respObj[k].rooms[l].room_classes.Count; m++)
                                {
                                    //string cGuid = Common.Common.GuidToLongID();//房型id
                                    //string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                    HotelPrice obj = new HotelPrice();
                                    obj.HotelID = hotelID;
                                    obj.HotelNo = respObj[k].id;
                                    obj.Platform = "SunSeries";
                                    
                                    obj.ClassName = respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().name;
                                    obj.RMBprice = respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().total.AsTargetType<decimal>(0);
                                    obj.Price = respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().total.AsTargetType<decimal>(0);
                                    obj.CurrencyCode = respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().currency;
                                    int IsBreakfast = 0;
                                    for (int q = 0; q < respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().nights[0].options.Count; q++)
                                    {
                                        if (respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().nights[0].options[q].name == "break_fast")
                                        {
                                            IsBreakfast = IsBreakfast + 1;
                                        }
                                    }
                                    obj.IsBreakfast = IsBreakfast;
                                    obj.ReferenceClient = respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().id;
                                    obj.RoomsLeft = respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().rooms_left;
                                    //obj.Adults = adults;
                                    //obj.Childs = children;
                                    //obj.ChildsAge = childAges;
                                    //obj.RoomAdults = RoomAdults;
                                    //obj.RoomChilds = RoomChilds;
                                    obj.RoomCount = respObj[k].rooms[l].index.AsTargetType<int>(0) + 1;
                                    //obj.ChildsAge = objResp.param.hotls.hot[i].res.pax[j].cod;
                                    obj.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                                    obj.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                                    //obj.CGuID = cGuid;
                                    //obj.RateplanId = GuidRateplanId;
                                    //obj.BedTypeCode = BedTypeCode;

                                    string PriceBreakdownDate = string.Empty;
                                    string PriceBreakdownPrice = string.Empty;
                                    for (int r = 0; r < respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().nights.Count; r++)
                                    {
                                        PriceBreakdownDate += respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().nights[r].date + ",";
                                        PriceBreakdownPrice += respObj[k].rooms[l].room_classes[m].samples.OrderBy(t => t.total).FirstOrDefault().nights[r].rate + ",";
                                    }
                                    obj.PriceBreakdownDate = PriceBreakdownDate;
                                    obj.PriceBreakdownPrice = PriceBreakdownPrice;
                                    obj.IsClose = 1;
                                    obj.CreateTime = DateTime.Now;
                                    obj.UpdateTime = DateTime.Now;
                                    obj.EntityState = e_EntityState.Added;
                                    objHotelPriceCol.Add(obj);
                                    /*for (int n = 0; n < respObj[k].rooms[l].room_classes[m].samples.Count; n++)
                                    {
                                        string cGuid = Common.Common.GuidToLongID();//房型id
                                        string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                        HotelPrice obj = new HotelPrice();
                                        obj.HotelID = hotelID;
                                        obj.HotelNo = respObj[k].id;
                                        obj.Platform = "SunSeries";
                                        string Status = "0";
                                        if (respObj[k].rooms[l].room_classes[m].samples[n].available == "instant")
                                        {
                                            Status = "1";
                                        }
                                        obj.Status = Status;
                                        obj.ClassName = respObj[k].rooms[l].room_classes[m].samples[n].name;
                                        obj.RMBprice = respObj[k].rooms[l].room_classes[m].samples[n].total.AsTargetType<decimal>(0);
                                        obj.Price = respObj[k].rooms[l].room_classes[m].samples[n].total.AsTargetType<decimal>(0);
                                        obj.CurrencyCode = respObj[k].rooms[l].room_classes[m].samples[n].currency;
                                        int IsBreakfast = 0;
                                        for (int q = 0; q < respObj[k].rooms[l].room_classes[m].samples[n].nights[0].options.Count; q++)
                                        {
                                            if (respObj[k].rooms[l].room_classes[m].samples[n].nights[0].options[q].name == "break_fast")
                                            {
                                                IsBreakfast = IsBreakfast + 1;
                                            }
                                        }
                                        obj.IsBreakfast = IsBreakfast;
                                        obj.ReferenceClient = respObj[k].rooms[l].room_classes[m].samples[n].id;
                                        obj.RoomsLeft = respObj[k].rooms[l].room_classes[m].samples[n].rooms_left;
                                        //obj.Adults = adults;
                                        //obj.Childs = children;
                                        //obj.ChildsAge = childAges;
                                        //obj.RoomAdults = RoomAdults;
                                        //obj.RoomChilds = RoomChilds;
                                        obj.RoomCount = respObj[k].rooms[l].index.AsTargetType<int>(0) + 1;
                                        //obj.ChildsAge = objResp.param.hotls.hot[i].res.pax[j].cod;
                                        obj.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                                        obj.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                                        obj.CGuID = cGuid;
                                        obj.RateplanId = GuidRateplanId;
                                        //obj.BedTypeCode = BedTypeCode;

                                        string PriceBreakdownDate = string.Empty;
                                        string PriceBreakdownPrice = string.Empty;
                                        for (int r = 0; r < respObj[k].rooms[l].room_classes[m].samples[n].nights.Count; r++)
                                        {
                                            PriceBreakdownDate += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].date + ",";
                                            PriceBreakdownPrice += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].rate + ",";
                                        }
                                        obj.PriceBreakdownDate = PriceBreakdownDate;
                                        obj.PriceBreakdownPrice = PriceBreakdownPrice;
                                        obj.IsClose = 1;
                                        obj.CreateTime = DateTime.Now;
                                        obj.UpdateTime = DateTime.Now;
                                        obj.EntityState = e_EntityState.Added;
                                        objHotelPriceCol.Add(obj);
                                    }*/
                                }
                            }
                        }
                        string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' and  Platform = 'SunSeries'", hotelID);
                        con.Update(updateHotelPriceSql);
                        con.Save(objHotelPriceCol);
                        //string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and IsClose=1", hotelID);
                        //dt = ControllerFactory.GetController().GetDataTable(sql);
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                return dt;
            }
            //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            return dt;
        }
        //检查报价
        public string createQuote(string hotelID, string rateplanId, string HotelPriceID)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getQuoteUrl = string.Empty;
            try
            {
                string sqlHp = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and id in ({1})", hotelID, HotelPriceID);
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                getQuoteUrl = url + string.Format(@"/v1/search_result/{0}/quote?token={1}",dtHp.Rows[0]["HotelNo"], sunSeriesToken);
                SunSeriesReq.CreatequoteReqData cqr = new SunSeriesReq.CreatequoteReqData();

                List<SunSeriesReq.CreatequoteRooms> reqRooms = new List<SunSeriesReq.CreatequoteRooms>();
                for (int i = 0; i < dtHp.Rows.Count; i++)
                {
                    List<string> option_ids = new List<string>();
                    //option_ids.Add(hotelID);
                    SunSeriesReq.CreatequoteRooms reqRoom = new SunSeriesReq.CreatequoteRooms();
                    reqRoom.sample_id = dtHp.Rows[i]["ReferenceClient"].AsTargetType<string>("");
                    reqRoom.bed_configuration = "single";
                    reqRoom.option_ids = option_ids;
                    reqRooms.Add(reqRoom);
                }
                cqr.rooms = reqRooms;
                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(cqr);
                try
                {
                    respData = Common.Common.PostHttp(getQuoteUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries检查报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries检查报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "检查报价失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                SunSeriesReq.CreatequoteRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.CreatequoteRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    string updateHotelPriceSql = string.Format(@"update HotelPrice set CheckID='{0}',UpdateTime=GETDATE() 
where HotelID='{1}' and  Platform = 'SunSeries' and id in ({2})",respObj.id, hotelID, HotelPriceID);
                    con.Update(updateHotelPriceSql);
                    resp = new Respone() { code = "00", mes = "检查报价成功，" + respObj.cancellation_date };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries检查报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "检查报价失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //预订
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, string HotelPriceID)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string postBookUrl = string.Empty;
            try
            {
                string guID = Common.Common.getGUID();
                string sqlHp = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and id in ({1})", HotelID, HotelPriceID);
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                postBookUrl = url + string.Format(@"/v1/quote/{0}/book?token={1}", dtHp.Rows[0]["CheckID"], sunSeriesToken);
                SunSeriesReq.SunSeriesBookReqData bookReq = new SunSeriesReq.SunSeriesBookReqData();


                List<string> remarks = new List<string>();
                List<SunSeriesReq.roomsBookReq> ssrbs = new List<SunSeriesReq.roomsBookReq>();
                decimal price=0;
                decimal RMBprice=0;
                for (int i = 0; i < dtHp.Rows.Count; i++)
                {
                    RMBprice +=dtHp.Rows[i]["RMBprice"].AsTargetType<decimal>(0);
                    price +=dtHp.Rows[i]["Price"].AsTargetType<decimal>(0);
                    SunSeriesReq.roomsBookReq ssrb = new SunSeriesReq.roomsBookReq();
                    List<SunSeriesReq.adultsBookReq> adultsObj=new List<SunSeriesReq.adultsBookReq>();
                    List<SunSeriesReq.childrenBookReq> childrenObj=new List<SunSeriesReq.childrenBookReq>();
                    for (int j = 0; j < customers[i].Customers.Count; j++)
                    {
                        string title = "MS";
                        SunSeriesReq.adultsBookReq adultObj = new SunSeriesReq.adultsBookReq();
                        SunSeriesReq.childrenBookReq childObj = new SunSeriesReq.childrenBookReq();
                        if (customers[i].Customers[j].age > 12)
                        {
                            if (customers[i].Customers[j].sex == 1)
                            {
                                title = "MR";
                            }
                            adultObj.title = title;
                            adultObj.last_name = customers[i].Customers[j].lastName;
                            adultObj.first_name = customers[i].Customers[j].firstName;
                        }
                        else 
                        {
                            childObj.last_name = customers[i].Customers[j].lastName;
                            childObj.first_name = customers[i].Customers[j].firstName;
                        }
                        adultsObj.Add(adultObj);
                        childrenObj.Add(childObj); 
                        ssrb.adults = adultsObj;
                        ssrb.children = childrenObj;
                    }
                    
                    ssrbs.Add(ssrb);
                }
                bookReq.remarks = remarks;
                bookReq.agent_reference = guID;
                bookReq.departure_flight_number = "";
                bookReq.departure_flight_time = "";
                bookReq.arrival_flight_number = "";
                bookReq.arrival_flight_time = "";
                bookReq.notification_recipients = remarks;
                bookReq.rooms = ssrbs;

                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(bookReq);
                try
                {
                    respData = Common.Common.PostHttp(postBookUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "预订失败失败，" + ex.ToString() };
                }
                SunSeriesReq.SunSeriesBookRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.SunSeriesBookRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    string[] booking_id = respObj.booking_id.Replace("::", ";").Split(';');
                    Booking booking = new Booking();
                    //保存订单到数据库
                    booking.BookingCode = booking_id[1];
                    booking.InOrderNum = inOrderNum;
                    booking.AgentBookingReference = guID;
                    booking.RMBprice = RMBprice;
                    booking.ClientCurrencyCode = dtHp.Rows[0]["CurrencyCode"].AsTargetType<string>("");
                    booking.ClientPartnerAmount = price;
                    booking.HotelId = HotelID;
                    booking.HotelNo = "";
                    booking.Platform = "SunSeries";
                    booking.StatusCode = respObj.state;
                    booking.StatusName = "预订成功";
                    booking.ServiceID = 0;
                    booking.ServiceName = "";
                    booking.LegID = 0;
                    booking.Leg_Status = "";
                    booking.Status = 1;
                    booking.CreatTime = DateTime.Now;
                    booking.UpdateTime = DateTime.Now;
                    booking.EntityState = e_EntityState.Added;
                    con.Save(booking);
                    if (respObj.state == "confirmed")
                    {
                        resp = new Respone() { code = "00", orderNum = booking_id[1], orderTotal = RMBprice, mes = "预订成功" };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "预订失败" };
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //取消订单
        public string cancel(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getCancelUrl = string.Empty;
            try
            {
                getCancelUrl = url + string.Format(@"/v1/bookings/{0}/cancel?token={1}", bookingCode, sunSeriesToken);
                try
                {
                    respData = Common.Common.GetHttp(getCancelUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries退单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", getCancelUrl, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                SunSeriesReq.cancelRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.cancelRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.current_state == "cancelled")
                    {
                        resp = new Respone() { code = "99", mes = "取消订单成功" };
                        string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Status=0,StatusName='订单已取消' where BookingCode='{1}'  and Platform='SunSeries' and hotelID='{2}'"
                            , respObj.current_state, bookingCode, hotelID);
                        con.Update(strUpdateSql);
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败" };
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //查询订单
        public string SunSeriesCheckingOrder(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string postCheckUrl = string.Empty;
            string postBoby = string.Empty;
            try
            {
                postCheckUrl = url + string.Format(@"/v1/bookings/search?token={0}", sunSeriesToken);
                string sql = string.Format(@"select * from Booking WITH(NOLOCK) where  Platform = 'SunSeries' and BookingCode='{0}' and hotelID='{1}'", bookingCode, hotelID);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                SunSeriesReq.SunSeriesCheckReqData sscr = new SunSeriesReq.SunSeriesCheckReqData();
                SunSeriesReq.criteriaCheckReq criteria = new SunSeriesReq.criteriaCheckReq();
                SunSeriesReq.creation_date creation_date = new SunSeriesReq.creation_date();
                criteria.agent_reference = dt.Rows[0]["AgentBookingReference"].AsTargetType<string>("");
                sscr.criteria = criteria;
                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(sscr);
                try
                {
                    respData = Common.Common.PostHttp(postCheckUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries查询订单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                List<SunSeriesReq.SunSeriesCheckRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesCheckRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    int Status = 0;
                    string StatusName = string.Empty;
                    string[] booking_id = respObj[0].id.Replace("::", ";").Split(';');
                    if (respObj[0].current_state == "cancelled")
                    {
                        StatusName = "订单已取消";
                        Status = 0;
                    }
                    else if (respObj[0].current_state == "confirmed")
                    {
                        StatusName = "预订成功";
                        Status = 1;
                    }
                    else
                    {
                        StatusName = respObj[0].current_state;
                        Status = 99;
                    }
                    decimal price = respObj[0].price.AsTargetType<decimal>(0);
                    string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Status='{1}',StatusName='{2}' where BookingCode='{3}'  and Platform='SunSeries' and hotelID='{4}' "
                            , respObj[0].current_state, Status, StatusName, bookingCode, hotelID);
                    con.Update(strUpdateSql);
                    resp = new Respone() { code = "00", orderNum = booking_id[1], orderTotal = price, mes = "查询订单,该订单状态为:" + StatusName };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries查询订单 ,Err :{0} ， Time:{1} ", "查询订单json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", mes = "查询订单失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //查询报价
        public string GetSunSeriesRoomTypeOutPrice(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            DataTable dt = new DataTable();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getPriceUrl = url + string.Format(@"/v1/search_with_results?token={0}", sunSeriesToken);
            try
            {
                int adults = occupancy.adults;
                int children = occupancy.children;
                string childAges = occupancy.childAges ?? "";
                //复查接口时调用，匹配数据是否有该房型
                if (RateplanId != null && RateplanId.Trim() != "")
                {
                    if (getRecheckPrice(hotelID, RateplanId, adults, RoomCount))
                    {
                        string sqlDt = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='SunSeries'  AND RateplanId='{1}'"
                        , hotelID, RateplanId);
                        dt = ControllerFactory.GetController().GetDataTable(sqlDt);
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：SunSeries获取报价 ,Err :{0} ， Time:{1} ", "检查价格是出错", DateTime.Now.ToString()), "SunSeries接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                }
                else
                {
                    string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='SunSeries' AND Adults={1} AND Childs={2}
and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6} AND IsClose=1 AND DATEDIFF( MINUTE, UpdateTime, GETDATE()) < {7} order by ID;
select ht.*,ssc.CountryID from SunSeriesHotel ht WITH(NOLOCK) LEFT JOIN dbo.SunSeriesCity ssc  WITH(NOLOCK) ON ssc.CityID=ht.CityId where HotelID='{0}';",
     hotelID, adults, children, childAges, beginTime, endTime, RoomCount, SunSeriesCacheTime);
                    DataSet ds = con.GetDataSet(sql);
                    dt = ds.Tables[0];
                    DataTable dtHotel = ds.Tables[1];

                    if (dtHotel.Rows.Count > 0 && dt.Rows.Count == 0)
                    {
                        SunSeriesReq.CriteriaReqData crd = new SunSeriesReq.CriteriaReqData();

                        List<SunSeriesReq.rooms> reqRooms = new List<SunSeriesReq.rooms>();
                        for (int i = 0; i < RoomCount; i++)
                        {
                            SunSeriesReq.rooms reqRoom = new SunSeriesReq.rooms();
                            List<SunSeriesReq.children> reqChildrens = new List<SunSeriesReq.children>();
                            if (occupancy.childAges != null && occupancy.childAges != "")
                            {
                                string[] ArrayAges = occupancy.childAges.Split(',');
                                for (int k = 0; k < ArrayAges.Length; k++)
                                {
                                    SunSeriesReq.children reqChildren = new SunSeriesReq.children();
                                    reqChildren.age = ArrayAges[k].AsTargetType<int>(0);
                                    reqChildrens.Add(reqChildren);
                                }
                            }
                            reqRoom.adults = occupancy.adults;
                            reqRoom.children = reqChildrens;

                            reqRooms.Add(reqRoom);
                        }

                        List<string> hotel_ids = new List<string>();
                        hotel_ids.Add(hotelID);

                        SunSeriesReq.criteria cr = new SunSeriesReq.criteria();
                        cr.from = beginTime;
                        cr.to = endTime;
                        cr.hotel_ids = hotel_ids;
                        cr.customer_country = dtHotel.Rows[0]["CountryID"].AsTargetType<string>("");
                        cr.city = dtHotel.Rows[0]["CityID"].AsTargetType<string>("");
                        cr.rooms = reqRooms;

                        crd.criteria = cr;
                        postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(crd);
                        try
                        {
                            respData = Common.Common.PostHttp(getPriceUrl, postBoby);
                            LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                        }
                        catch (Exception ex)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        List<SunSeriesReq.CriteriaRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.CriteriaRespData>>(respData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (respObj != null)
                        {
                            HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                            for (int k = 0; k < respObj.Count; k++)
                            {
                                for (int m = 0; m < respObj[k].rooms[0].room_classes.Count; m++)
                                {
                                    for (int n = 0; n < respObj[k].rooms[0].room_classes[m].samples.Count; n++)
                                    {
                                        int roomsLeft = respObj[k].rooms[0].room_classes[m].samples[n].rooms_left;//可预订数
                                        string roomStats = respObj[k].rooms[0].room_classes[m].samples[n].available;//状态
                                        if (roomStats == "instant" && roomsLeft >= RoomCount)//可预订状态且可预订数大于等于要预订数
                                        {
                                            string cGuid = Common.Common.GuidToLongID();//房型id
                                            string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                            HotelPrice obj = new HotelPrice();
                                            obj.HotelID = hotelID;
                                            obj.HotelNo = respObj[k].id;
                                            obj.Platform = "SunSeries";
                                            obj.Status = "1";
                                            obj.ClassName = respObj[k].rooms[0].room_classes[m].samples[n].name;
                                            obj.RMBprice = respObj[k].rooms[0].room_classes[m].samples[n].total.AsTargetType<decimal>(0) * RoomCount;
                                            obj.Price = respObj[k].rooms[0].room_classes[m].samples[n].total.AsTargetType<decimal>(0) * RoomCount;
                                            obj.CurrencyCode = respObj[k].rooms[0].room_classes[m].samples[n].currency;
                                            int IsBreakfast = 0;
                                            /*for (int p = 0; p < respObj[k].rooms[l].room_classes[m].samples[n].nights.Count; p++)
                                            {
                                            */
                                            for (int q = 0; q < respObj[k].rooms[0].room_classes[m].samples[n].nights[0].options.Count; q++)
                                            {
                                                if (respObj[k].rooms[0].room_classes[m].samples[n].nights[0].options[q].name == "break_fast")
                                                {
                                                    IsBreakfast = IsBreakfast + 1;
                                                }
                                            }
                                            obj.IsBreakfast = IsBreakfast;
                                            obj.ReferenceClient = respObj[k].rooms[0].room_classes[m].samples[n].id;
                                            obj.RoomsLeft = roomsLeft; ;
                                            obj.Adults = adults;
                                            obj.Childs = children;
                                            obj.ChildsAge = childAges;
                                            //obj.RoomAdults = RoomAdults;
                                            //.RoomChilds = RoomChilds;
                                            obj.RoomCount = RoomCount;
                                            obj.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                                            obj.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                                            obj.CGuID = cGuid;
                                            obj.RateplanId = GuidRateplanId;
                                            //obj.BedTypeCode = BedTypeCode;

                                            string PriceBreakdownDate = string.Empty;
                                            string PriceBreakdownPrice = string.Empty;
                                            for (int r = 0; r < respObj[k].rooms[0].room_classes[m].samples[n].nights.Count; r++)
                                            {
                                                PriceBreakdownDate += respObj[k].rooms[0].room_classes[m].samples[n].nights[r].date + ",";
                                                PriceBreakdownPrice += respObj[k].rooms[0].room_classes[m].samples[n].nights[r].rate + ",";
                                            }
                                            obj.PriceBreakdownDate = PriceBreakdownDate.TrimEnd(',');
                                            obj.PriceBreakdownPrice = PriceBreakdownPrice.TrimEnd(',');
                                            obj.IsClose = 1;
                                            obj.CreateTime = DateTime.Now;
                                            obj.UpdateTime = DateTime.Now;
                                            obj.EntityState = e_EntityState.Added;
                                            objHotelPriceCol.Add(obj);
                                        }
                                    }
                                }
                            }
                            string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' and  Platform = 'SunSeries' AND Adults={1} AND Childs={2}   and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6}",
 hotelID, adults, children, childAges, beginTime, endTime, RoomCount);
                            con.Update(updateHotelPriceSql);
                            con.Save(objHotelPriceCol);
                            string sqlHp = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='SunSeries'  AND Adults={1} AND Childs={2}   and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6} AND IsClose=1 order by ID"
                        , hotelID, adults, children, childAges, beginTime, endTime, RoomCount);
                            dt = ControllerFactory.GetController().GetDataTable(sqlHp);
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                RoomOutPrice.Room temp = new RoomOutPrice.Room();
                                temp.ProductSource = "TravPax"; //产品来源 
                                temp.MroomName = temp.XRoomName = temp.RoomName = dt.Rows[j]["ClassName"].AsTargetType<string>("");// 房型名称
                                temp.MroomId = temp.RoomTypeId = dt.Rows[j]["CGuID"].AsTargetType<string>("");//房型ID(由于长度要求，取自己生成后的guid，要是用到下单时转为)

                                //当前请求的房间人数
                                temp.Adults = dt.Rows[j]["Adults"].AsTargetType<int>(0);
                                temp.Children = dt.Rows[j]["Childs"].AsTargetType<int>(0);
                                temp.ChildAges = dt.Rows[j]["ChildsAge"].AsTargetType<string>("").Replace(",", "|");
                                //最大入住人数
                                temp.MaxPerson = dt.Rows[j]["Adults"].AsTargetType<int>(0);

                                temp.RatePlan = new List<RoomOutPrice.RatePlan>();
                                RoomOutPrice.RatePlan thisRatePlan = new RoomOutPrice.RatePlan();
                                string RatePlanName = "有早";
                                thisRatePlan.PayMent = "y";//支付方式。目前全部前台支付
                                int dtIsBreakfast = dt.Rows[j]["IsBreakfast"].AsTargetType<int>(0);
                                if (dtIsBreakfast == 0)
                                {
                                    RatePlanName = "无早";
                                }
                                thisRatePlan.RatePlanName = RatePlanName;  //价格计划名称
                                thisRatePlan.RoomName = RatePlanName;
                                thisRatePlan.XRatePlanName = RatePlanName;
                                thisRatePlan.RatePlanId = dt.Rows[j]["RateplanId"].AsTargetType<string>("");  //价格计划ID

                                thisRatePlan.RoomTypeId = dt.Rows[j]["CGuID"].AsTargetType<string>("");
                                temp.XRoomId = temp.RoomId = thisRatePlan.RoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>("")); //房型Id_价格计划ID
                                //temp.XRoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>(""));
                                thisRatePlan.BedType = "";

                                //售价
                                decimal RMBOutprice = dt.Rows[j]["RMBprice"].AsTargetType<decimal>(0);
                                string[] ArrayPriceBreakdownDate = dt.Rows[j]["PriceBreakdownDate"].AsTargetType<string>("").Split(',');
                                //总的底价 
                                double sMoney = RMBOutprice.AsTargetType<double>(0);
                                bool available = false; // 指示入离日期所有天是否可订 有一天不可订为false
                                List<RoomOutPrice.Rate> rates = new List<RoomOutPrice.Rate>();
                                int days = (Convert.ToDateTime(endTime) - Convert.ToDateTime(beginTime)).Days;
                                for (int x = 0; x < ArrayPriceBreakdownDate.Length; x++)
                                {
                                    RoomOutPrice.Rate rate = new RoomOutPrice.Rate();
                                    rate.Date = Convert.ToDateTime(ArrayPriceBreakdownDate[x]).AddDays(x);
                                    rate.Available = true;  //某天是否可订


                                    rate.HotelID = (RMBOutprice / days).AsTargetType<string>("");  //底价

                                    rate.MemberRate = (RMBOutprice / days);
                                    rate.RetailRate = (RMBOutprice / days);
                                    rates.Add(rate);
                                }


                                //补上没有的日期
                                if (days > rates.Count)
                                {
                                    available = false;
                                    List<RoomOutPrice.Rate> tempR = new List<RoomOutPrice.Rate>();
                                    for (int y = 0; y < days; y++)
                                    {
                                        DateTime date = Convert.ToDateTime(beginTime).AddDays(y).Date;
                                        var one = rates.Find(c => c.Date.Date == date);
                                        if (one != null)
                                        {
                                            tempR.Add(one);
                                        }
                                        else
                                        {
                                            RoomOutPrice.Rate newOne = new RoomOutPrice.Rate() { Available = false, InvStatusCode = "0", MemberRate = 0, Date = date };
                                            tempR.Add(newOne);
                                        }
                                    }
                                    rates = tempR;
                                }

                                thisRatePlan.Rates = rates;
                                thisRatePlan.Available = available;
                                thisRatePlan.SMoney = Convert.ToDecimal(sMoney);

                                decimal avgprice = 0;//均价
                                if (rates.Count > 0)
                                {
                                    avgprice = rates.Average(c => c.MemberRate);
                                    thisRatePlan.AveragePrice = avgprice.ToString("f2");
                                }
                                temp.CurrentAlloment = thisRatePlan.CurrentAlloment = 15; //库存

                                //设置早餐数量
                                int breakfast = -1;
                                thisRatePlan.Breakfast = breakfast + "份早餐";
                                if (dtIsBreakfast == 0)
                                {
                                    thisRatePlan.Breakfast = 0 + "份早餐";
                                }
                                else
                                {
                                    thisRatePlan.Breakfast = dt.Rows[j]["IsBreakfast"].AsTargetType<int>(0) + "份早餐";
                                }
                                string zaocan = thisRatePlan.Breakfast.Replace("份早餐", "");
                                if (!zaocan.Equals("0"))
                                {
                                    switch (zaocan)
                                    {
                                        case "1": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含单早)"; temp.RoomName = temp.RoomName + "(含单早)"; break;
                                        case "2": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含双早)"; temp.RoomName = temp.RoomName + "(含双早)"; break;
                                        case "3": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含三早)"; temp.RoomName = temp.RoomName + "(含三早)"; break;
                                        case "4": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含四早)"; temp.RoomName = temp.RoomName + "(含四早)"; break;
                                    }
                                }
                                thisRatePlan.RoomName = thisRatePlan.RoomName + "[内宾]";

                                temp.RoomPrice = Convert.ToInt32(avgprice);
                                temp.BedType = thisRatePlan.BedType;
                                temp.RatePlan.Add(thisRatePlan);
                                rooms.Add(temp);
                            }
                        }
                        else
                        {
                            LogHelper.DoOperateLog(string.Format("Err：SunSeries获取报价 ,Err :{0} ， Time:{1} ", "json解析为空", DateTime.Now.ToString()), "SunSeries接口");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：SunSeries获取报价 ,Err :{0} ， Time:{1} ", "查询数据库无该酒店信息", DateTime.Now.ToString()), "SunSeries接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }

        //检查报价
        public bool getRecheckPrice(string hotelID, string rateplanId,int adults, int RoomCount)
        {
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getQuoteUrl = string.Empty;
            try
            {
                string bedType = "double";
                if (adults == 1)
                {
                    bedType = "single";
                }
                else if (adults == 2)
                {
                    bedType = "twin";
                }
                string sqlHp = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and RateplanId='{1}'", hotelID, rateplanId);
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                if (dtHp.Rows.Count > 0)
                {
                    getQuoteUrl = url + string.Format(@"/v1/search_result/{0}/quote?token={1}", dtHp.Rows[0]["HotelNo"], sunSeriesToken);
                    SunSeriesReq.CreatequoteReqData cqr = new SunSeriesReq.CreatequoteReqData();

                    List<SunSeriesReq.CreatequoteRooms> reqRooms = new List<SunSeriesReq.CreatequoteRooms>();
                    for (int i = 0; i < dtHp.Rows[0]["RoomCount"].AsTargetType<int>(0); i++)
                    {
                        List<string> option_ids = new List<string>();
                        //option_ids.Add(hotelID);
                        SunSeriesReq.CreatequoteRooms reqRoom = new SunSeriesReq.CreatequoteRooms();
                        reqRoom.sample_id = dtHp.Rows[0]["ReferenceClient"].AsTargetType<string>("");

                        reqRoom.bed_configuration = bedType;
                        reqRoom.option_ids = option_ids;
                        reqRooms.Add(reqRoom);
                    }
                    cqr.rooms = reqRooms;
                    postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(cqr);
                    try
                    {
                        respData = Common.Common.PostHttp(getQuoteUrl, postBoby);
                        LogHelper.DoOperateLog(string.Format("Studio：SunSeries检查报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:SunSeries检查报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        return false;
                    }
                    SunSeriesReq.CreatequoteRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.CreatequoteRespData>(respData, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (respObj != null)
                    {
                        decimal RecheckTotalPrice=0;
                        for(int j=0;j<RoomCount;j++)
                        {
                            RecheckTotalPrice += respObj.rooms[j].itemization.total.AsTargetType<decimal>(0);
                        }
                        decimal RMBprice = RecheckTotalPrice;
                        string RecheckCurrencyCode = respObj.rooms[0].itemization.currency;
                        string updateHotelPriceSql = string.Format(@"update HotelPrice set CheckID='{0}',Price='{1}',CurrencyCode='{2}',RMBprice='{3}',UpdateTime=GETDATE() 
    from HotelPrice hp WITH(NOLOCK) where  hp.HotelID='{4}' AND hp.Platform='SunSeries' and hp.RateplanId='{5}'",
 respObj.id,RecheckTotalPrice, RecheckCurrencyCode, RMBprice, hotelID, rateplanId);
                        con.Update(updateHotelPriceSql);
                        return true;
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：SunSeries检查报价 ,Err :{0} ， Time:{1} ", "检查报价json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                        return false;
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries检查报价 ,Err :{0} ， Time:{1} ", "查询数据库无该计划id报价", DateTime.Now.ToString()), "SunSeries接口");
                    return false;
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                return false;
            }
            return true;
        }
        //创建订单
        public string Create_Order(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string postBookUrl = string.Empty;
            try
            {
                string guID = Common.Common.getGUID();
                string sqlHp = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and RateplanId='{1}'", HotelID, RateplanId);
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                postBookUrl = url + string.Format(@"/v1/quote/{0}/book?token={1}", dtHp.Rows[0]["CheckID"], sunSeriesToken);
                SunSeriesReq.SunSeriesBookReqData bookReq = new SunSeriesReq.SunSeriesBookReqData();


                List<string> remarks = new List<string>();
                List<SunSeriesReq.roomsBookReq> ssrbs = new List<SunSeriesReq.roomsBookReq>();
                decimal price = 0;
                decimal RMBprice = 0;
                for (int i = 0; i < roomNum; i++)
                {
                    RMBprice = dtHp.Rows[0]["RMBprice"].AsTargetType<decimal>(0);
                    price = dtHp.Rows[0]["Price"].AsTargetType<decimal>(0);
                    SunSeriesReq.roomsBookReq ssrb = new SunSeriesReq.roomsBookReq();
                    List<SunSeriesReq.adultsBookReq> adultsObj = new List<SunSeriesReq.adultsBookReq>();
                    List<SunSeriesReq.childrenBookReq> childrenObj = new List<SunSeriesReq.childrenBookReq>();
                    for (int j = 0; j < customers[i].Customers.Count; j++)
                    {
                        string title = "MS";
                        SunSeriesReq.adultsBookReq adultObj = new SunSeriesReq.adultsBookReq();
                        SunSeriesReq.childrenBookReq childObj = new SunSeriesReq.childrenBookReq();
                        if (customers[i].Customers[j].age > 12)
                        {
                            if (customers[i].Customers[j].sex == 1)
                            {
                                title = "MR";
                            }
                            adultObj.title = title;
                            adultObj.last_name = customers[i].Customers[j].lastName;
                            adultObj.first_name = customers[i].Customers[j].firstName;
                        }
                        else
                        {
                            childObj.last_name = customers[i].Customers[j].lastName;
                            childObj.first_name = customers[i].Customers[j].firstName;
                        }
                        adultsObj.Add(adultObj);
                        childrenObj.Add(childObj);
                    }
                    ssrb.adults = adultsObj;
                    ssrb.children = childrenObj;
                    ssrbs.Add(ssrb);
                }
                bookReq.remarks = remarks;
                bookReq.agent_reference = guID;
                bookReq.departure_flight_number = "";
                bookReq.departure_flight_time = "";
                bookReq.arrival_flight_number = "";
                bookReq.arrival_flight_time = "";
                bookReq.notification_recipients = remarks;
                bookReq.rooms = ssrbs;

                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(bookReq);
                try
                {
                    respData = Common.Common.PostHttp(postBookUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "预订失败失败，" + ex.ToString() };
                }
                SunSeriesReq.SunSeriesBookRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.SunSeriesBookRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    string[] booking_id = respObj.booking_id.Replace("::", ";").Split(';');
                    Booking booking = new Booking();
                    //保存订单到数据库
                    booking.BookingCode = booking_id[1];
                    booking.InOrderNum = inOrderNum;
                    booking.AgentBookingReference = guID;
                    booking.RMBprice = RMBprice;
                    booking.ClientCurrencyCode = dtHp.Rows[0]["CurrencyCode"].AsTargetType<string>("");
                    booking.ClientPartnerAmount = price;
                    booking.HotelId = HotelID;
                    booking.HotelNo = "";
                    booking.Platform = "SunSeries";
                    booking.StatusCode = respObj.state;
                    booking.StatusName = "预订成功";
                    booking.ServiceID = 0;
                    booking.ServiceName = "";
                    booking.LegID = 0;
                    booking.Leg_Status = "";
                    booking.Status = 1;
                    booking.CreatTime = DateTime.Now;
                    booking.UpdateTime = DateTime.Now;
                    booking.EntityState = e_EntityState.Added;
                    con.Save(booking);
                    if (respObj.state == "confirmed")
                    {
                        resp = new Respone() { code = "00", orderNum = booking_id[1], orderTotal = RMBprice, mes = "预订成功" };
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订状态不正确", DateTime.Now.ToString()), "SunSeries接口");
                        resp = new Respone() { code = "99", mes = "预订失败" };
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
    }
}