﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Data.common;
using XiWan.Model.Entities;

namespace XiWan.Data.SunSeries
{
    public class SunSeriesDAL
    {
        //private static HttpSessionState _session = HttpContext.Current.Session;
        string USDtoRMBrate = CCommon.GetWebConfigValue("USDtoRMBrate");//美元转为人民币汇率
        string EURtoRMBrate = CCommon.GetWebConfigValue("EURtoRMBrate");//欧元转为人民币汇率
        string SGDtoRMBrate = CCommon.GetWebConfigValue("SGDtoRMBrate");//新加坡元转为人民币汇率
        string HKDtoRMBrate = CCommon.GetWebConfigValue("HKDtoRMBrate");//港币转为人民币汇率
        string SunSeriesTime = CCommon.GetWebConfigValue("SunSeriesTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        ControllerBase con2 = ControllerFactory.GetNewController(e_ConsType.Main2);
        string url = CCommon.GetWebConfigValue("SunSeriesUrl");//url
        string Username = CCommon.GetWebConfigValue("SunSeriesUsername");//
        string Password = CCommon.GetWebConfigValue("SunSeriesPassword");//
        int SunSeriesTokenCacheTime = CCommon.GetWebConfigValue("SunSeriesTokenCacheTime").AsTargetType<int>(0);//Token有效时长（秒）
        string SunSeriesCacheTime = CCommon.GetWebConfigValue("SunSeriesCacheTime");//缓存到数据库的有效期（分钟）

        #region 初始化本类
        private static SunSeriesDAL _Instance;
        public static SunSeriesDAL Instance
        {
            get
            {
                return new SunSeriesDAL();
            }
        }
        #endregion

        /// <summary>
        /// 获取token
        /// </summary>
        public string GetAuthenticate()
        {
            string result=string.Empty;
            try
            {
                result = CacheHelper.GetCache("SunSeriesToken").AsTargetType<string>("");//先读取缓存
                if (result == null || result == "")//如果没有该缓存
                {
                    string respData = string.Empty;
                    string postBoby = string.Empty;
                    string authenticateUrl = url + "/v1/users/authenticate";
                    SunSeriesReq.AuthenticateReqData req = new SunSeriesReq.AuthenticateReqData() { email = Username, password = Password };
                    postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(req);
                    try
                    {
                        respData = Common.Common.PostHttp(authenticateUrl, postBoby);
                        //LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取token ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:SunSeries获取token ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        return result;
                    }
                    SunSeriesReq.AuthenticateRespData authenticateInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.AuthenticateRespData>(respData, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (authenticateInfo != null && postBoby != "")
                    {
                        CacheHelper.SetCache("SunSeriesToken", authenticateInfo.token, SunSeriesTokenCacheTime);//添加缓存
                        result = authenticateInfo.token;
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取token ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return result;
        }
        /// <summary>
        /// 获取国家
        /// </summary>
        public void GetSunSeriesCountries()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getCountriesUrl = url + string.Format(@"/v1/countries?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getCountriesUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取国家 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国家,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriesCountryRespData> hotelsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesCountryRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (hotelsInfo != null)
                {
                    for (int i = 0; i < hotelsInfo.Count; i++)
                    {
                        SunSeriesCountry ssh = new SunSeriesCountry();
                        ssh.CountryCode = hotelsInfo[i].code;
                        ssh.CountryName = string.IsNullOrEmpty(hotelsInfo[i].name) ? "" : hotelsInfo[i].name;
                        ssh.CountryID = string.IsNullOrEmpty(hotelsInfo[i].id) ? "" : hotelsInfo[i].id;
                        con.Save(ssh);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国家 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        /// <summary>
        /// 获取城市
        /// </summary>
        public void GetSunSeriesCities()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getCitiesUrl = url + string.Format(@"/v1/cities?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getCitiesUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取城市 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取城市,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriesCityRespData> cityInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesCityRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (cityInfo != null)
                {
                    for (int i = 0; i < cityInfo.Count; i++)
                    {
                        SunSeriesCity city = new SunSeriesCity();
                        city.CityID = string.IsNullOrEmpty(cityInfo[i].id) ? "" : cityInfo[i].id;
                        city.CityName = string.IsNullOrEmpty(cityInfo[i].name) ? "" : cityInfo[i].name;
                        city.CountryCode = string.IsNullOrEmpty(cityInfo[i].code) ? "" : cityInfo[i].code;
                        city.CountryID = string.IsNullOrEmpty(cityInfo[i].country_id) ? "" : cityInfo[i].country_id;
                        con.Save(city);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取城市 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        /// <summary>
        /// 获取国籍
        /// </summary>
        public void GetSunSeriesNationalities()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getNationalitiesUrl = url + string.Format(@"/v1/nationalities?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getNationalitiesUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取国籍 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国籍,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriessNationalityRespData> nationalityInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriessNationalityRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (nationalityInfo != null)
                {
                    for (int i = 0; i < nationalityInfo.Count; i++)
                    {
                        SunSeriesNationality nationality = new SunSeriesNationality();
                        nationality.CountryID = string.IsNullOrEmpty(nationalityInfo[i].id) ? "" : nationalityInfo[i].id;
                        nationality.CountryName = string.IsNullOrEmpty(nationalityInfo[i].name) ? "" : nationalityInfo[i].name;
                        nationality.CountryCode = string.IsNullOrEmpty(nationalityInfo[i].code) ? "" : nationalityInfo[i].code;
                        con2.Save(nationality);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取国籍 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        /// <summary>
        /// 获取酒店
        /// </summary>
        public void GetSunSeriesHotels()
        {
            string respData = string.Empty;
            //string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getHotelUrl = url + string.Format(@"/v1/hotels?token={0}", sunSeriesToken);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(getHotelUrl);
                    //respData = Common.Common.SplitStringWithComma(respData);
                    //LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取酒店 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries获取酒店,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                List<SunSeriesReq.SunSeriesHotelsRespData> hotelsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesHotelsRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (hotelsInfo != null)
                {
                    for (int i = 0; i < hotelsInfo.Count; i++)
                    {
                        string ImagerUrl = string.Empty;
                        string ThumbnailUrl = string.Empty;
                        for (int j = 0; j < hotelsInfo[i].images.Count; j++)
                        {
                            ImagerUrl += string.IsNullOrEmpty(hotelsInfo[i].images[j].image_url) ? "" : hotelsInfo[i].images[j].image_url + ";";
                            //ThumbnailUrl = string.IsNullOrEmpty(hotelsInfo[i].images[j].thumbnail_url) ? "" : hotelsInfo[i].images[j].thumbnail_url + ";";
                        }
                        SunSeriesHotel ssh = new SunSeriesHotel();
                        ssh.HotelID = hotelsInfo[i].id;
                        ssh.HotelName = string.IsNullOrEmpty(hotelsInfo[i].name) ? "" : hotelsInfo[i].name;
                        ssh.stars = string.IsNullOrEmpty(hotelsInfo[i].stars) ? "" : hotelsInfo[i].stars;
                        ssh.CityId = string.IsNullOrEmpty(hotelsInfo[i].city_id) ? "" : hotelsInfo[i].city_id;
                        ssh.AreaId = string.IsNullOrEmpty(hotelsInfo[i].area_id) ? "" : hotelsInfo[i].area_id;
                        ssh.Address = string.IsNullOrEmpty(hotelsInfo[i].address) ? "" : hotelsInfo[i].address;
                        ssh.Telephone = string.IsNullOrEmpty(hotelsInfo[i].telephone) ? "" : hotelsInfo[i].telephone;
                        ssh.Longitude = string.IsNullOrEmpty(hotelsInfo[i].longitude) ? "" : hotelsInfo[i].longitude;
                        ssh.Latitude = string.IsNullOrEmpty(hotelsInfo[i].latitude) ? "" : hotelsInfo[i].latitude;
                        ssh.ImagerUrl = ImagerUrl.TrimEnd(';');
                        string sqlSunSeriesHotel = string.Format(@"select * from SunSeriesHotel where HotelID='{0}'", hotelsInfo[i].id);
                        DataTable dtSunSeriesRoomHotel = con2.GetDataTable(sqlSunSeriesHotel);
                        if (dtSunSeriesRoomHotel.Rows.Count == 0)
                        {
                            con2.Save(ssh);
                        }
                        //ssh.ThumbnailUrl = ThumbnailUrl.TrimEnd(';');
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取酒店 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationality()
        {
            string sql = "select CountryID as ID,CountryName as NAME from SunSeriesNationality ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }

        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestination(string strName)
        {
            string sql = string.Format(@"SELECT DISTINCT(CityName) as name,(CityID+','+CountryID) as value FROM dbo.SunSeriesHotel where CityName like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }

        //保存房型
        public DataTable GetRoomType(string hotelID, string beginTime, string endTime, List<IntOccupancyInfo> occupancy, int RoomCount, string RateplanId)
        {
            DataTable dt = new DataTable();
            //List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getPriceUrl = url + string.Format(@"/v1/search_with_results?token={0}", sunSeriesToken);
            try
            {
                //string sqlHotel = string.Format(@"select * from SunSeriesHotel ht WITH(NOLOCK) where HotelID='{0}' ", hotelID);
                string sqlHotel = string.Format(@"select * from TravelIntl.dbo.SunSeriesHotel ht WITH(NOLOCK)", hotelID);
                DataTable dtHotel = con2.GetDataTable(sqlHotel);
                for (int f = 0; f < dtHotel.Rows.Count; f++)
                {
                    hotelID = dtHotel.Rows[f]["HotelID"].AsTargetType<string>("");
                    SunSeriesReq.CriteriaReqData crd = new SunSeriesReq.CriteriaReqData();

                    List<SunSeriesReq.rooms> reqRooms = new List<SunSeriesReq.rooms>();
                    for (int i = 0; i < occupancy.Count; i++)
                    {
                        SunSeriesReq.rooms reqRoom = new SunSeriesReq.rooms();
                        List<SunSeriesReq.children> reqChildrens = new List<SunSeriesReq.children>();

                        if (occupancy[i].childAges != null && occupancy[i].childAges != "")
                        {
                            string[] ArrayAges = occupancy[i].childAges.Split(',');
                            for (int k = 0; k < ArrayAges.Length; k++)
                            {
                                SunSeriesReq.children reqChildren = new SunSeriesReq.children();
                                reqChildren.age = ArrayAges[k].AsTargetType<int>(0);
                                reqChildrens.Add(reqChildren);
                            }
                        }


                        reqRoom.adults = occupancy[i].adults;
                        reqRoom.children = reqChildrens;

                        reqRooms.Add(reqRoom);
                    }

                    List<string> hotel_ids = new List<string>();
                    hotel_ids.Add(hotelID);

                    SunSeriesReq.criteria cr = new SunSeriesReq.criteria();
                    cr.from = beginTime;
                    cr.to = endTime;
                    cr.hotel_ids = hotel_ids;
                    cr.customer_country = dtHotel.Rows[f]["CountryID"].AsTargetType<string>("");
                    cr.city = dtHotel.Rows[f]["CityID"].AsTargetType<string>("");
                    cr.rooms = reqRooms;

                    crd.criteria = cr;
                    postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(crd);
                    try
                    {
                        respData = Common.Common.PostHttp(getPriceUrl, postBoby);
                        LogHelper.DoOperateLog(string.Format("Studio：SunSeries保存房型 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:SunSeries保存房型 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        //return dt;
                    }
                    List<SunSeriesReq.CriteriaRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.CriteriaRespData>>(respData, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (respObj != null)
                    {
                        for (int k = 0; k < respObj.Count; k++)
                        {
                            for (int l = 0; l < respObj[k].rooms.Count; l++)
                            {
                                for (int m = 0; m < respObj[k].rooms[l].room_classes.Count; m++)
                                {
                                    for (int n = 0; n < respObj[k].rooms[l].room_classes[m].samples.Count; n++)
                                    {
                                        SunSeriesRoomType obj = new SunSeriesRoomType();
                                        obj.HotelID = hotelID;
                                        obj.RoomTypeName = respObj[k].rooms[l].room_classes[m].name;
                                        obj.RoomTypeID = respObj[k].rooms[l].room_classes[m].id.Replace("backend_hotel::bess-suns", "");
                                        obj.CreateTime = DateTime.Now;
                                        string sqlSunSeriesRoomType = string.Format(@"select * from SunSeriesRoomType where RoomTypeID='{0}' and HotelID='{1}'", respObj[k].rooms[l].room_classes[m].id.Replace("backend_hotel::bess-suns", ""),hotelID);
                                        DataTable dtSunSeriesRoomType = con2.GetDataTable(sqlSunSeriesRoomType);
                                        if (dtSunSeriesRoomType.Rows.Count == 0)
                                        {
                                            con2.Save(obj);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries保存房型 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                //return dt;
            }
            //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            return dt;
        }
        
        //获取报价
        public DataTable GetRoomTypePrice(string hotelID, string beginTime, string endTime, List<IntOccupancyInfo> occupancy, int RoomCount, string RateplanId)
        {
            DataTable dt = new DataTable();
            //List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getPriceUrl = url + string.Format(@"/v1/search_with_results?token={0}", sunSeriesToken);
            try
            {
                int adults = occupancy[0].adults;
                int children = occupancy[0].children;
                string strChildAges = occupancy[0].childAges;
                string childAges = occupancy[0].childAges.AsTargetType<string>("");
                if (strChildAges == null || strChildAges == "")
                {
                    childAges = "";
                }

                string sqlHotel = string.Format(@"select top 1 * from dbo.SunSeriesHotel ht WITH(NOLOCK) where HotelID='{0}' ", hotelID);
                //string sqlHotel = string.Format(@"select * from SunSeriesHotel ht WITH(NOLOCK) ", hotelID);
                DataTable dtHotel = ControllerFactory.GetController().GetDataTable(sqlHotel);
                for (int f = 0; f < dtHotel.Rows.Count; f++)
                {
                    hotelID = dtHotel.Rows[f]["HotelID"].AsTargetType<string>("");
                    SunSeriesReq.CriteriaReqData crd = new SunSeriesReq.CriteriaReqData();

                    List<SunSeriesReq.rooms> reqRooms = new List<SunSeriesReq.rooms>();
                    for (int i = 0; i < occupancy.Count; i++)
                    {
                        SunSeriesReq.rooms reqRoom = new SunSeriesReq.rooms();
                        List<SunSeriesReq.children> reqChildrens = new List<SunSeriesReq.children>();

                        if (occupancy[i].childAges != null && occupancy[i].childAges != "")
                        {
                            string[] ArrayAges = occupancy[i].childAges.Split(',');
                            for (int k = 0; k < ArrayAges.Length; k++)
                            {
                                SunSeriesReq.children reqChildren = new SunSeriesReq.children();
                                reqChildren.age = ArrayAges[k].AsTargetType<int>(0);
                                reqChildrens.Add(reqChildren);
                            }
                        }


                        reqRoom.adults = occupancy[i].adults;
                        reqRoom.children = reqChildrens;

                        reqRooms.Add(reqRoom);
                    }

                    List<string> hotel_ids = new List<string>();
                    hotel_ids.Add(hotelID);

                    SunSeriesReq.criteria cr = new SunSeriesReq.criteria();
                    cr.from = beginTime;
                    cr.to = endTime;
                    cr.hotel_ids = hotel_ids;
                    cr.customer_country = dtHotel.Rows[f]["CountryID"].AsTargetType<string>("");
                    cr.city = dtHotel.Rows[f]["CityID"].AsTargetType<string>("");
                    cr.rooms = reqRooms;

                    crd.criteria = cr;
                    postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(crd);
                    try
                    {

                        TimeSpan spanStartTime = new TimeSpan(DateTime.Now.Ticks);
                        respData = Common.Common.PostHttp(getPriceUrl, postBoby);

                        TimeSpan spanEndTime = new TimeSpan(DateTime.Now.Ticks);
                        TimeSpan ts = spanEndTime.Subtract(spanStartTime).Duration();
 
                        //计算时间间隔，求出调用接口所需要的时间
                        string spanTime = ts.Hours.ToString() + "小时" + ts.Minutes.ToString() + "分" + ts.Seconds.ToString() + "秒" + ts.Milliseconds.ToString();

                        LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ,响应时间：{3}", postBoby, respData, DateTime.Now.ToString(),spanTime), "SunSeries接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        return dt;
                    }
                    List<SunSeriesReq.CriteriaRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.CriteriaRespData>>(respData, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (respObj != null)
                    {
                        HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                        for (int k = 0; k < respObj.Count; k++)
                        {
                            for (int l = 0; l < respObj[k].rooms.Count; l++)
                            {
                                for (int m = 0; m < respObj[k].rooms[l].room_classes.Count; m++)
                                {
                                    for (int n = 0; n < respObj[k].rooms[l].room_classes[m].samples.Count; n++)
                                    {
                                        string cGuid = Common.Common.GuidToLongID();//房型id
                                        string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                        HotelPrice obj = new HotelPrice();
                                        obj.HotelID = hotelID;
                                        obj.SeachID = respObj[k].id;
                                        obj.Platform = "SunSeries";
                                        string Status = "0";
                                        if (respObj[k].rooms[l].room_classes[m].samples[n].available == "instant")
                                        {
                                            Status = "1";
                                        }
                                        obj.Status = Status;
                                        obj.ClassName = respObj[k].rooms[l].room_classes[m].name.Replace("backend_hotel::bess-suns", "");
                                        obj.RoomTypeID = respObj[k].rooms[l].room_classes[m].id;
                                        obj.RMBprice = respObj[k].rooms[l].room_classes[m].samples[n].total.AsTargetType<decimal>(0) * HKDtoRMBrate.AsTargetType<decimal>(0);
                                        obj.Price = respObj[k].rooms[l].room_classes[m].samples[n].total.AsTargetType<decimal>(0);
                                        obj.CurrencyCode = respObj[k].rooms[l].room_classes[m].samples[n].currency;
                                        int IsBreakfast = 0;
                                        /*for (int p = 0; p < respObj[k].rooms[l].room_classes[m].samples[n].nights.Count; p++)
                                        {
                                        */
                                        for (int q = 0; q < respObj[k].rooms[l].room_classes[m].samples[n].nights[0].options.Count; q++)
                                        {
                                            if (respObj[k].rooms[l].room_classes[m].samples[n].nights[0].options[q].name == "break_fast")
                                            {
                                                IsBreakfast = IsBreakfast + 1;
                                            }
                                        }
                                        obj.IsBreakfast = IsBreakfast;
                                        obj.ReferenceClient = respObj[k].rooms[l].room_classes[m].samples[n].id;
                                        obj.RoomsLeft = respObj[k].rooms[l].room_classes[m].samples[n].rooms_left;
                                        obj.Adults = adults;
                                        obj.Childs = children;
                                        obj.ChildsAge = childAges;
                                        /*obj.RoomAdults = RoomAdults;
                                        obj.RoomChilds = RoomChilds;*/
                                        obj.RoomCount = RoomCount;
                                        //obj.ChildsAge = objResp.param.hotls.hot[i].res.pax[j].cod;
                                        obj.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                                        obj.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                                        obj.CGuID = cGuid;
                                        obj.RateplanId = GuidRateplanId;
                                        //obj.BedTypeCode = BedTypeCode;

                                        string PriceBreakdownDate = string.Empty;
                                        string PriceBreakdownPrice = string.Empty;
                                        for (int r = 0; r < respObj[k].rooms[l].room_classes[m].samples[n].nights.Count; r++)
                                        {
                                            PriceBreakdownDate += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].date + ",";
                                            PriceBreakdownPrice += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].rate + ",";
                                        }
                                        obj.PriceBreakdownDate = PriceBreakdownDate;
                                        obj.PriceBreakdownPrice = PriceBreakdownPrice;
                                        obj.IsClose = 1;
                                        obj.CreateTime = DateTime.Now;
                                        obj.UpdateTime = DateTime.Now;
                                        obj.EntityState = e_EntityState.Added;
                                        objHotelPriceCol.Add(obj);
                                    }
                                }
                            }
                        }
                        string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' and  Platform = 'SunSeries'", hotelID);
                        con.Update(updateHotelPriceSql);
                        con.Save(objHotelPriceCol);
                        string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and IsClose=1", hotelID);
                        dt = ControllerFactory.GetController().GetDataTable(sql);
                    }
                    else
                    {
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                return dt;
            }
            //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            return dt;
        }
        //计算预订价格
        public string createQuote(string hotelID, string rateplanId, string HotelPriceID)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getQuoteUrl = string.Empty;
            try
            {
                string sqlHp = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and id in ({1})", hotelID, HotelPriceID);
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                getQuoteUrl = url + string.Format(@"/v1/search_result/{0}/quote?token={1}", dtHp.Rows[0]["SeachID"], sunSeriesToken);
                SunSeriesReq.CreatequoteReqData cqr = new SunSeriesReq.CreatequoteReqData();

                List<SunSeriesReq.CreatequoteRooms> reqRooms = new List<SunSeriesReq.CreatequoteRooms>();
                for (int i = 0; i < dtHp.Rows.Count; i++)
                {
                    List<string> option_ids = new List<string>();
                    //option_ids.Add(hotelID);
                    SunSeriesReq.CreatequoteRooms reqRoom = new SunSeriesReq.CreatequoteRooms();
                    reqRoom.sample_id = dtHp.Rows[i]["ReferenceClient"].AsTargetType<string>("");
                    reqRoom.bed_configuration = "single";
                    reqRoom.option_ids = option_ids;
                    reqRooms.Add(reqRoom);
                }
                cqr.rooms = reqRooms;
                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(cqr);
                try
                {
                    respData = Common.Common.PostHttp(getQuoteUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries检查报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries检查报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "检查报价失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                SunSeriesReq.CreatequoteRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.CreatequoteRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    string updateHotelPriceSql = string.Format(@"update HotelPrice set CheckID='{0}',UpdateTime=GETDATE() 
where HotelID='{1}' and  Platform = 'SunSeries' and id in ({2})",respObj.id, hotelID, HotelPriceID);
                    con.Update(updateHotelPriceSql);
                    resp = new Respone() { code = "00", mes = "检查报价成功，" + respObj.cancellation_date };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries检查报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "检查报价失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //预订
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, string HotelPriceID)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string postBookUrl = string.Empty;
            try
            {
                string guID = Common.Common.getGUID();
                string sqlHp = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and id in ({1})", HotelID, HotelPriceID);
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                postBookUrl = url + string.Format(@"/v1/quote/{0}/book?token={1}", dtHp.Rows[0]["CheckID"], sunSeriesToken);
                SunSeriesReq.SunSeriesBookReqData bookReq = new SunSeriesReq.SunSeriesBookReqData();


                List<string> remarks = new List<string>();
                List<SunSeriesReq.roomsBookReq> ssrbs = new List<SunSeriesReq.roomsBookReq>();
                decimal price=0;
                decimal RMBprice=0;
                for (int i = 0; i < dtHp.Rows.Count; i++)
                {
                    RMBprice += dtHp.Rows[i]["RMBprice"].AsTargetType<decimal>(0) * HKDtoRMBrate.AsTargetType<decimal>(0);
                    price +=dtHp.Rows[i]["Price"].AsTargetType<decimal>(0);
                    SunSeriesReq.roomsBookReq ssrb = new SunSeriesReq.roomsBookReq();
                    List<SunSeriesReq.adultsBookReq> adultsObj=new List<SunSeriesReq.adultsBookReq>();
                    List<SunSeriesReq.childrenBookReq> childrenObj=new List<SunSeriesReq.childrenBookReq>();
                    for (int j = 0; j < customers[i].Customers.Count; j++)
                    {
                        string title = "MS";
                        SunSeriesReq.adultsBookReq adultObj = new SunSeriesReq.adultsBookReq();
                        SunSeriesReq.childrenBookReq childObj = new SunSeriesReq.childrenBookReq();
                        if (customers[i].Customers[j].age > 12)
                        {
                            if (customers[i].Customers[j].sex == 1)
                            {
                                title = "MR";
                            }
                            adultObj.title = title;
                            adultObj.last_name = customers[i].Customers[j].lastName;
                            adultObj.first_name = customers[i].Customers[j].firstName;
                        }
                        else 
                        {
                            childObj.last_name = customers[i].Customers[j].lastName;
                            childObj.first_name = customers[i].Customers[j].firstName;
                        }
                        adultsObj.Add(adultObj);
                        childrenObj.Add(childObj); 
                        ssrb.adults = adultsObj;
                        ssrb.children = childrenObj;
                    }
                    
                    ssrbs.Add(ssrb);
                }
                bookReq.remarks = remarks;
                bookReq.agent_reference = guID;
                bookReq.departure_flight_number = "";
                bookReq.departure_flight_time = "";
                bookReq.arrival_flight_number = "";
                bookReq.arrival_flight_time = "";
                bookReq.notification_recipients = remarks;
                bookReq.rooms = ssrbs;

                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(bookReq);
                try
                {
                    //respData = Common.Common.PostHttp(postBookUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "预订失败失败，" + ex.ToString() };
                }
                SunSeriesReq.SunSeriesBookRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.SunSeriesBookRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    string[] booking_id = respObj.booking_id.Replace("::", ";").Split(';');
                    Booking booking = new Booking();
                    //保存订单到数据库
                    booking.BookingCode = booking_id[1];
                    booking.InOrderNum = inOrderNum;
                    booking.AgentBookingReference = guID;
                    booking.RMBprice = RMBprice;
                    booking.ClientCurrencyCode = dtHp.Rows[0]["CurrencyCode"].AsTargetType<string>("");
                    booking.ClientPartnerAmount = price;
                    booking.HotelId = HotelID;
                    booking.HotelNo = "";
                    booking.Platform = "SunSeries";
                    booking.StatusCode = respObj.state;
                    booking.StatusName = "预订成功";
                    booking.ServiceID = 0;
                    booking.ServiceName = "";
                    booking.LegID = 0;
                    booking.Leg_Status = "";
                    booking.Status = 1;
                    booking.CreatTime = DateTime.Now;
                    booking.UpdateTime = DateTime.Now;
                    booking.EntityState = e_EntityState.Added;
                    con.Save(booking);
                    if (respObj.state == "confirmed")
                    {
                        resp = new Respone() { code = "00", orderNum = booking_id[1], orderTotal = RMBprice, mes = "预订成功" };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "预订失败" };
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //取消订单
        public string cancel(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getCancelUrl = string.Empty;
            try
            {
                getCancelUrl = url + string.Format(@"/v1/bookings/{0}/cancel?token={1}", bookingCode, sunSeriesToken);
                try
                {
                    respData = Common.Common.GetHttp(getCancelUrl);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries退单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", getCancelUrl, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", Status = "取消失败", mes = "取消订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                SunSeriesReq.cancelRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.cancelRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.current_state == "cancelled" || respObj.current_state == "rejected")
                    {
                        resp = new Respone() { code = "00", orderNum = bookingCode, Status = "取消", mes = "取消成功" };
                        string strUpdateSql = string.Format("update SunSeries_Order set StatusCode='{0}',UpdateTime=GETDATE(),Status=0,StatusName='取消' where BookingCode='{1}' and hotelID='{2}'"
                            , respObj.current_state, bookingCode, hotelID);
                        con.Update(strUpdateSql);
                    }
                    else
                    {
                        resp = new Respone() { code = "99", Status = "取消失败", mes = "取消失败" };
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", Status = "取消失败", mes = "取消失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //查询订单
        public string SunSeriesCheckingOrder(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string postCheckUrl = string.Empty;
            string postBoby = string.Empty;
            try
            {
                postCheckUrl = url + string.Format(@"/v1/bookings/search?token={0}", sunSeriesToken);
                string sql = string.Format(@"select * from SunSeries_Order WITH(NOLOCK) where  BookingCode='{0}' and hotelID='{1}'", bookingCode, hotelID);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                SunSeriesReq.SunSeriesCheckReqData sscr = new SunSeriesReq.SunSeriesCheckReqData();
                SunSeriesReq.criteriaCheckReq criteria = new SunSeriesReq.criteriaCheckReq();
                SunSeriesReq.creation_date creation_date = new SunSeriesReq.creation_date();
                criteria.agent_reference = dt.Rows[0]["AgentBookingReference"].AsTargetType<string>("");
                sscr.criteria = criteria;
                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(sscr);
                try
                {
                    respData = Common.Common.PostHttp(postCheckUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries查询订单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                List<SunSeriesReq.SunSeriesCheckRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.SunSeriesCheckRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null && respObj.Count>0)
                {
                    int Status = 0;
                    string reservation_status = string.Empty;
                    string StatusName = string.Empty;
                    string[] booking_id = respObj[0].id.Replace("::", ";").Split(';');
                    if (respObj[0].current_state == "cancelled")
                    {
                        StatusName = "取消";
                        Status = 0;
                        reservation_status = "4";
                    }
                    else if (respObj[0].current_state == "confirmed")
                    {
                        StatusName = "确认";
                        Status = 1;
                        reservation_status = "2";
                    }
                    else if (respObj[0].current_state == "guaranteed")
                    {
                        StatusName = "确认";
                        Status = 1;
                        reservation_status = "2";
                    }
                    else if (respObj[0].current_state == "rejected")
                    {
                        StatusName = "取消";
                        Status = 0;
                        reservation_status = "4";
                    }
                    else
                    {
                        StatusName = respObj[0].current_state;
                        Status = 99;
                    }
                    decimal price = respObj[0].price.AsTargetType<decimal>(0);
                    string strUpdateSql = string.Format("update SunSeries_Order set StatusCode='{0}',UpdateTime=GETDATE(),Status='{1}',StatusName='{2}' where BookingCode='{3}' and hotelID='{4}' "
                            , respObj[0].current_state, Status, StatusName, bookingCode, hotelID);
                    con.Update(strUpdateSql);
                    string orderOriginalPrice = "HK$" + price;
                    decimal RMBprice = price * HKDtoRMBrate.AsTargetType<decimal>(0);
                    Reservation rev = new Reservation();
                    rev.hotel_id = hotelID;
                    rev.aic_reservation_id = booking_id[1];//dt.Rows[0]["BookingConfirmationID"].AsTargetType<string>("");
                    rev.partner_reservation_id = dt.Rows[0]["InOrderNum"].AsTargetType<string>("");
                    rev.reservation_status = respObj[0].current_state;
                    rev.hotel_confirmation_number = "";
                    rev.hotel_cancellation_number = "";
                    rev.special_request = "";
                    rev.check_in_date = dt.Rows[0]["CheckInDate"].AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                    rev.check_out_date = dt.Rows[0]["CheckOutDate"].AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                    rev.adult_number = dt.Rows[0]["Adults"].AsTargetType<string>("");
                    rev.kids_number = dt.Rows[0]["Childs"].AsTargetType<string>("");
                    rev.total_rooms = dt.Rows[0]["RoomCount"].AsTargetType<string>("");
                    rev.total_amount = RMBprice.AsTargetType<string>("");
                    rev.currency = orderOriginalPrice;
                    rev.cancellation_time = dt.Rows[0]["CancellationTime"].AsTargetType<string>("");
                    resp = new Respone() { code = "00", orderNum = booking_id[1], Reservation = rev, orderTotal = RMBprice, orderOriginalPrice = orderOriginalPrice, Status = StatusName, mes = "查询成功" };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries查询订单 ,Err :{0} ， Time:{1} ", "查询订单json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", mes = "查询订单失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //查询报价
        public string GetSunSeriesRoomTypeOutPrice(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            //DataTable dt = new DataTable();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getPriceUrl = url + string.Format(@"/v1/search_with_results?token={0}", sunSeriesToken);

            int adults = occupancy.adults;
            int children = occupancy.children;
            string strChildAges = occupancy.childAges ?? "";
            string CheckIn = Convert.ToDateTime(beginTime).ToString("yyyy-MM-dd");
            string Checkout = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd");
            string StrKey = hotelID + "%" + CheckIn + "%" + Checkout + "%" + adults + "%" + children + "%" + strChildAges + "%" + RoomCount;
            int IsContain = 0;//报价是否存在这条记录
            try
            {
                string sql = string.Format(@"select * from SunSeriesLink_RoomsCache WITH(NOLOCK) where StrKey='{0}';
                select ht.* from dbo.SunSeriesHotel ht WITH(NOLOCK) where HotelID='{1}';", StrKey, hotelID);
                DataSet ds = con.GetDataSet(sql);
                DataTable dt = ds.Tables[0];
                DataTable dtHotel = ds.Tables[1];
                int SeparateMinute = 6000;
                if (dt.Rows.Count > 0)
                {
                    SeparateMinute = DateTime.Now.Subtract(Convert.ToDateTime(dt.Rows[0]["UpdateTime"].AsTargetType<DateTime>(DateTime.MinValue))).Minutes;//计算数据库数据时间与当前时间相差分钟数数
                }
                int SeparateDays = Convert.ToDateTime(beginTime).Subtract(DateTime.Now).Days;//计算预订时间与当前时间相差天数
                if (dtHotel.Rows.Count == 0)
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
                if ((RateplanId != null && RateplanId != "") || dt.Rows.Count == 0 || dt.Rows[0]["IsClose"].AsTargetType<int>(0) == 0 || SeparateDays <= 2 || (SeparateDays > 2 && SeparateDays <= 7 && SeparateMinute > 10) || (SeparateDays > 7 && SeparateDays <= 10 && SeparateMinute > 30) || (SeparateDays > 10 && SeparateDays <= 20 && SeparateMinute > 180) || (SeparateDays > 20  && SeparateMinute > 240))
                {
                    SunSeriesReq.CriteriaReqData crd = new SunSeriesReq.CriteriaReqData();

                    List<SunSeriesReq.rooms> reqRooms = new List<SunSeriesReq.rooms>();
                    for (int i = 0; i < RoomCount; i++)
                    {
                        SunSeriesReq.rooms reqRoom = new SunSeriesReq.rooms();
                        List<SunSeriesReq.children> reqChildrens = new List<SunSeriesReq.children>();
                        if (occupancy.childAges != null && occupancy.childAges != "")
                        {
                            string[] ArrayAges = occupancy.childAges.Split(',');
                            for (int k = 0; k < ArrayAges.Length; k++)
                            {
                                SunSeriesReq.children reqChildren = new SunSeriesReq.children();
                                reqChildren.age = ArrayAges[k].AsTargetType<int>(0);
                                reqChildrens.Add(reqChildren);
                            }
                        }
                        reqRoom.adults = occupancy.adults;
                        reqRoom.children = reqChildrens;

                        reqRooms.Add(reqRoom);
                    }

                    List<string> hotel_ids = new List<string>();
                    hotel_ids.Add(hotelID);

                    SunSeriesReq.criteria cr = new SunSeriesReq.criteria();
                    cr.from = beginTime;
                    cr.to = endTime;
                    cr.hotel_ids = hotel_ids;
                    cr.customer_country = dtHotel.Rows[0]["CountryID"].AsTargetType<string>("");
                    cr.city = dtHotel.Rows[0]["CityID"].AsTargetType<string>("");
                    cr.rooms = reqRooms;

                    crd.criteria = cr;
                    postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(crd);
                    try
                    {
                        respData = Common.Common.PostHttp(getPriceUrl, postBoby);
                        if (RateplanId != null && RateplanId != "")
                        {
                            LogHelper.DoOperateLog(string.Format("Studio：SunSeries获取报价 ,请求数据 :{0} , 返回数据 ：{1} ，RateplanId:{2} , Time:{3} ", postBoby, respData,RateplanId, DateTime.Now.ToString()), "SunSeries接口");
                        }
                        SunSeriesLink_RoomsCache roomsCache = new SunSeriesLink_RoomsCache();
                        roomsCache.StrKey = StrKey;
                        roomsCache.HotelId = hotelID;
                        roomsCache.Checkin = CheckIn.AsTargetType<DateTime>(DateTime.MinValue);
                        roomsCache.CheckOut = Checkout.AsTargetType<DateTime>(DateTime.MinValue);
                        //ssRoomsCache.Request = postBoby;
                        roomsCache.Response = respData;
                        roomsCache.CreatTime = DateTime.Now;
                        roomsCache.UpdateTime = DateTime.Now;
                        roomsCache.IsClose = 1;
                        roomsCache.EntityState = e_EntityState.Added;
                        if (dt.Rows.Count == 0)
                        {
                            con.Save(roomsCache);
                        }
                        else
                        {
                            string updateRoomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set Response='{0}',UpdateTime=GETDATE(),IsClose=1 where StrKey='{1}'", respData.Replace("'", "''"), StrKey);
                            con.Update(updateRoomsCacheSql);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                }
                else
                {
                    respData=dt.Rows[0]["Response"].AsTargetType<string>("");
                }
                List<SunSeriesReq.CriteriaRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.CriteriaRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                    for (int k = 0; k < respObj.Count; k++)
                    {
                        for (int l = 0; l < 1; l++)
                        {
                            if (respObj[k].rooms.Count != 0)
                            {
                                for (int m = 0; m < respObj[k].rooms[l].room_classes.Count; m++)
                                {
                                    string SeachID = respObj[k].id.Replace("search_result::search::", "");
                                    string roomTypeID = respObj[k].rooms[l].room_classes[m].id.Replace("backend_hotel::bess-suns", "");
                                    RoomOutPrice.Room temp = new RoomOutPrice.Room();
                                    temp.ProductSource = "SunSeries"; //产品来源 
                                    temp.RoomName = respObj[k].rooms[l].room_classes[m].name + "[内宾]";
                                    temp.MroomName = temp.XRoomName = temp.MroomNameEn = respObj[k].rooms[l].room_classes[m].name;// 房型名称
                                    temp.MroomId = temp.RoomTypeId = roomTypeID;//房型ID(由于长度要求，取自己生成后的guid，要是用到下单时转为)

                                    //当前请求的房间人数
                                    temp.Adults = adults;
                                    temp.Children = children;
                                    temp.ChildAges = strChildAges.Replace(",", "|");
                                    //最大入住人数
                                    temp.MaxPerson = adults + children;

                                    temp.RatePlan = new List<RoomOutPrice.RatePlan>();
                                    for (int n = 0; n < respObj[k].rooms[l].room_classes[m].samples.Count; n++)
                                    {
                                        int roomsLeft = respObj[k].rooms[l].room_classes[m].samples[n].rooms_left;//可预订数
                                        string roomStats = respObj[k].rooms[l].room_classes[m].samples[n].available;//状态
                                        string strRatePlanId = roomTypeID + "-" + respObj[k].rooms[l].room_classes[m].samples[n].rate_code;//房型id+计划id
                                        decimal recheckPrice = respObj[k].rooms[l].room_classes[m].samples[n].total.AsTargetType<decimal>(0) * HKDtoRMBrate.AsTargetType<decimal>(0) * RoomCount;

                                        int IsBreakfast = 0;
                                        for (int q = 0; q < respObj[k].rooms[l].room_classes[m].samples[n].nights[0].options.Count; q++)
                                        {
                                            if (respObj[k].rooms[l].room_classes[m].samples[n].nights[0].options[q].name == "break_fast")
                                            {
                                                IsBreakfast = IsBreakfast + 1;
                                            }
                                        }
                                        string PriceBreakdownDate = string.Empty;
                                        string PriceBreakdownPrice = string.Empty;
                                        for (int r = 0; r < respObj[k].rooms[l].room_classes[m].samples[n].nights.Count; r++)
                                        {
                                            PriceBreakdownDate += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].date + ",";
                                            PriceBreakdownPrice += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].rate + ",";
                                        }
                                        if (roomStats == "instant" && roomsLeft >= RoomCount)//可预订状态且可预订数大于等于要预订数
                                        {
                                            //复查价格
                                            /*if (RateplanId != null && RateplanId != "" && RateplanId == strRatePlanId)
                                            {
                                                IsContain = 1;
                                                string strRecheckPrice = getRecheckPrice(hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId);
                                                if (strRecheckPrice == "-1")
                                                {
                                                    LogHelper.DoOperateLog(string.Format("Err：SunSeries获取报价 ,Err :{0} ， Time:{1} ", "检查价格出差或检查价格不可预订", DateTime.Now.ToString()), "SunSeries接口");
                                                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                                                }
                                                else if (strRecheckPrice.Contains(","))
                                                {
                                                    recheckPrice = strRecheckPrice.Split(',')[0].AsTargetType<decimal>(0) * HKDtoRMBrate.AsTargetType<decimal>(0) / RoomCount;
                                                }
                                                //strRecheckPrice=getRecheckPrice(hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId).Split(',')[0].AsTargetType<decimal>(0)/RoomCount;
                                            }*/
                                            
                                            RoomOutPrice.RatePlan thisRatePlan = new RoomOutPrice.RatePlan();
                                            string RatePlanName = "有早";
                                            thisRatePlan.PayMent = "y";//支付方式。目前全部前台支付
                                            if (IsBreakfast == 0)
                                            {
                                                RatePlanName = respObj[k].rooms[l].room_classes[m].name + "(无早)" + "[内宾]";
                                            }
                                            thisRatePlan.RatePlanName = RatePlanName;  //价格计划名称
                                            thisRatePlan.RoomName = RatePlanName;
                                            thisRatePlan.XRatePlanName = RatePlanName;
                                            thisRatePlan.RatePlanId = strRatePlanId;  //价格计划ID

                                            thisRatePlan.RoomTypeId = respObj[k].rooms[l].room_classes[m].id;
                                            temp.XRoomId = temp.RoomId = thisRatePlan.RoomId = string.Format("{0}_{1}", roomTypeID, strRatePlanId); //房型Id_价格计划ID
                                            //temp.XRoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>(""));
                                            common.HotelBedType hbt = new common.HotelBedType();
                                            string BedTypeName = hbt.HotelBedTypeToName(respObj[k].rooms[l].room_classes[m].name);
                                            if (BedTypeName == "其他")
                                            {
                                                if (adults == 1)
                                                {
                                                    BedTypeName = "单人床";
                                                }
                                                else if (adults == 2)
                                                {
                                                    BedTypeName = "双床";
                                                }
                                            }
                                            thisRatePlan.BedType = BedTypeName;

                                            //售价
                                            decimal RMBOutprice = recheckPrice / RoomCount;
                                            string[] ArrayPriceBreakdownDate = PriceBreakdownDate.TrimEnd(',').Split(',');
                                            //总的底价 
                                            double sMoney = recheckPrice.AsTargetType<double>(0) / RoomCount;
                                            bool available = true; // 指示入离日期所有天是否可订 有一天不可订为false
                                            List<RoomOutPrice.Rate> rates = new List<RoomOutPrice.Rate>();
                                            int days = Convert.ToDateTime(endTime).Subtract(Convert.ToDateTime(beginTime)).Days; //(Convert.ToDateTime(endTime) - Convert.ToDateTime(beginTime)).Days;
                                            for (int x = 0; x < ArrayPriceBreakdownDate.Length; x++)
                                            {
                                                RoomOutPrice.Rate rate = new RoomOutPrice.Rate();
                                                rate.Date = Convert.ToDateTime(ArrayPriceBreakdownDate[x]).AddDays(x);
                                                rate.Available = true;  //某天是否可订


                                                rate.HotelID = (RMBOutprice / ArrayPriceBreakdownDate.Length).ToString("f2"); //底价

                                                rate.MemberRate = (RMBOutprice / ArrayPriceBreakdownDate.Length).ToString("f2").AsTargetType<decimal>(0);
                                                rate.RetailRate = (RMBOutprice / ArrayPriceBreakdownDate.Length).ToString("f2").AsTargetType<decimal>(0);
                                                rates.Add(rate);
                                            }


                                            //补上没有的日期
                                            if (days > rates.Count)
                                            {
                                                available = false;
                                                List<RoomOutPrice.Rate> tempR = new List<RoomOutPrice.Rate>();
                                                for (int y = 0; y < days; y++)
                                                {
                                                    DateTime date = Convert.ToDateTime(beginTime).AddDays(y).Date;
                                                    var one = rates.Find(c => c.Date.Date == date);
                                                    if (one != null)
                                                    {
                                                        tempR.Add(one);
                                                    }
                                                    else
                                                    {
                                                        RoomOutPrice.Rate newOne = new RoomOutPrice.Rate() { Available = false, InvStatusCode = "0", MemberRate = 0, Date = date };
                                                        tempR.Add(newOne);
                                                    }
                                                }
                                                rates = tempR;
                                            }

                                            thisRatePlan.Rates = rates;
                                            thisRatePlan.Available = available;
                                            thisRatePlan.SMoney = Convert.ToDecimal(sMoney);

                                            thisRatePlan.CurrencyCode = "HKD" + ":" + HKDtoRMBrate;

                                            decimal avgprice = 0;//均价
                                            if (rates.Count > 0)
                                            {
                                                avgprice = rates.Average(c => c.MemberRate);
                                                thisRatePlan.AveragePrice = avgprice.ToString("f2");
                                            }
                                            temp.CurrentAlloment = thisRatePlan.CurrentAlloment = roomsLeft; //库存

                                            //设置早餐数量
                                            int breakfast = -1;
                                            thisRatePlan.Breakfast = breakfast + "份早餐";
                                            if (IsBreakfast == 0)
                                            {
                                                thisRatePlan.Breakfast = 0 + "份早餐";
                                            }
                                            else
                                            {
                                                thisRatePlan.Breakfast = IsBreakfast + "份早餐";
                                            }
                                            string zaocan = thisRatePlan.Breakfast.Replace("份早餐", "");
                                            if (!zaocan.Equals("0"))
                                            {
                                                switch (zaocan)
                                                {
                                                    case "1": thisRatePlan.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = respObj[k].rooms[l].room_classes[m].name + "(含单早)" + "[内宾]"; break;
                                                    case "2": thisRatePlan.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = respObj[k].rooms[l].room_classes[m].name + "(含双早)" + "[内宾]"; break;
                                                    case "3": thisRatePlan.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = respObj[k].rooms[l].room_classes[m].name + "(含三早)" + "[内宾]"; break;
                                                    case "4": thisRatePlan.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = respObj[k].rooms[l].room_classes[m].name + "(含四早)" + "[内宾]"; break;
                                                }
                                            }
                                            //thisRatePlan.RoomName = thisRatePlan.RoomName + "[内宾]";

                                            temp.RoomPrice = Convert.ToInt32(avgprice);
                                            temp.BedType = thisRatePlan.BedType;
                                            if (RateplanId != null && RateplanId != "")
                                            {
                                                if (strRatePlanId == RateplanId)
                                                {
                                                    temp.RatePlan.Add(thisRatePlan);
                                                }
                                            }
                                            else
                                            {
                                                temp.RatePlan.Add(thisRatePlan);
                                            }
                                        }
                                        
                                    }
                                    if (RateplanId != null && RateplanId != "")
                                    {
                                        if (roomTypeID == (RateplanId.Split('-')[0] + "-" + RateplanId.Split('-')[1]))
                                        {
                                            rooms.Add(temp);
                                        }
                                    }
                                    else
                                    {
                                        rooms.Add(temp);
                                    }
                                }
                            }
                            else
                            {
                                string updateRoomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                                con.Update(updateRoomsCacheSql);
                                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                            }
                        }
                    }
                }
                else
                {
                    string updateRoomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                    con.Update(updateRoomsCacheSql);
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries获取报价 ,Err :{0} ，接口返回数据(Resp) ：{1},RateplanId:{2} ， Time:{3} ", "json解析为空",respData, RateplanId, DateTime.Now.ToString()), "SunSeries接口");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
            }
            catch (Exception ex)
            {
                string updateRoomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                con.Update(updateRoomsCacheSql);
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} ,接口返回数据(Resp) ：{2},RateplanId:{3}, Time:{4} ", ex.Message, postBoby,respData, RateplanId, DateTime.Now.ToString()), "Err");
                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }

        //预订前检查是否可预订及搜索id
        public string GetRecheckIsBook(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId, decimal TotalMoney)
        {
            List<roomPrice> listPrice = new List<roomPrice>();
            string result = string.Empty;
            //DataTable dt = new DataTable();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getPriceUrl = url + string.Format(@"/v1/search_with_results?token={0}", sunSeriesToken);

            int IsCan = 0;
            int adults = occupancy.adults;
            int children = occupancy.children;
            string strChildAges = occupancy.childAges ?? "";
            string CheckIn = Convert.ToDateTime(beginTime).ToString("yyyy-MM-dd");
            string Checkout = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd");
            string StrKey = hotelID + "%" + CheckIn + "%" + Checkout + "%" + adults + "%" + children + "%" + strChildAges + "%" + RoomCount;
            try
            {
                string sql = string.Format(@"select * from SunSeriesLink_RoomsCache WITH(NOLOCK) where StrKey='{0}';
                select ht.* from dbo.SunSeriesHotel ht WITH(NOLOCK) where HotelID='{1}';", StrKey, hotelID);
                DataSet ds = con.GetDataSet(sql);
                DataTable dt = ds.Tables[0];
                DataTable dtHotel = ds.Tables[1];
                if (dtHotel.Rows.Count == 0)
                {
                    return "";
                }
                SunSeriesReq.CriteriaReqData crd = new SunSeriesReq.CriteriaReqData();

                List<SunSeriesReq.rooms> reqRooms = new List<SunSeriesReq.rooms>();
                for (int i = 0; i < RoomCount; i++)
                {
                    SunSeriesReq.rooms reqRoom = new SunSeriesReq.rooms();
                    List<SunSeriesReq.children> reqChildrens = new List<SunSeriesReq.children>();
                    if (occupancy.childAges != null && occupancy.childAges != "")
                    {
                        string[] ArrayAges = occupancy.childAges.Split(',');
                        for (int k = 0; k < ArrayAges.Length; k++)
                        {
                            SunSeriesReq.children reqChildren = new SunSeriesReq.children();
                            reqChildren.age = ArrayAges[k].AsTargetType<int>(0);
                            reqChildrens.Add(reqChildren);
                        }
                    }
                    reqRoom.adults = occupancy.adults;
                    reqRoom.children = reqChildrens;

                    reqRooms.Add(reqRoom);
                }

                List<string> hotel_ids = new List<string>();
                hotel_ids.Add(hotelID);

                SunSeriesReq.criteria cr = new SunSeriesReq.criteria();
                cr.from = beginTime;
                cr.to = endTime;
                cr.hotel_ids = hotel_ids;
                cr.customer_country = dtHotel.Rows[0]["CountryID"].AsTargetType<string>("");
                cr.city = dtHotel.Rows[0]["CityID"].AsTargetType<string>("");
                cr.rooms = reqRooms;

                crd.criteria = cr;
                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(crd);
                try
                {
                    respData = Common.Common.PostHttp(getPriceUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries检查是否可预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries检查是否可预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    return "";
                }
                string roomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set Response='{0}',UpdateTime=GETDATE(),IsClose=1 where StrKey='{1}'", respData.Replace("'", "''"), StrKey);
                con.Update(roomsCacheSql);

                List<SunSeriesReq.CriteriaRespData> respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<List<SunSeriesReq.CriteriaRespData>>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                    for (int k = 0; k < respObj.Count; k++)
                    {
                        for (int l = 0; l < 1; l++)
                        {
                            if (respObj[k].rooms.Count != 0)
                            {
                                for (int m = 0; m < respObj[k].rooms[l].room_classes.Count; m++)
                                {
                                    for (int n = 0; n < respObj[k].rooms[l].room_classes[m].samples.Count; n++)
                                    {
                                        string className = respObj[k].rooms[l].room_classes[m].name;
                                        string SeachID = respObj[k].id.Replace("search_result::search::", "");
                                        string roomTypeID = respObj[k].rooms[l].room_classes[m].id.Replace("backend_hotel::bess-suns", "");
                                        int roomsLeft = respObj[k].rooms[l].room_classes[m].samples[n].rooms_left;//可预订数
                                        string roomStats = respObj[k].rooms[l].room_classes[m].samples[n].available;//状态
                                        string strRatePlanId = roomTypeID + "-" + respObj[k].rooms[l].room_classes[m].samples[n].rate_code;//房型id+计划id
                                        decimal recheckPrice = respObj[k].rooms[l].room_classes[m].samples[n].total.AsTargetType<decimal>(0) * HKDtoRMBrate.AsTargetType<decimal>(0);

                                        if (roomStats == "instant" && roomsLeft >= RoomCount && TotalMoney >= recheckPrice)//可预订状态且可预订数大于等于要预订数
                                        {
                                            //检查是否存在该房型
                                            if (RateplanId != null && RateplanId != "" && RateplanId == strRatePlanId)
                                            {
                                                string strResult = SeachID + "_" + respObj[k].rooms[l].room_classes[m].samples[n].id + "_" + className;
                                                roomPrice objPrice = new roomPrice();
                                                objPrice.result = strResult;
                                                objPrice.offerPrice = recheckPrice;
                                                listPrice.Add(objPrice);
                                                IsCan = 1;
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                string updateRoomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                                con.Update(updateRoomsCacheSql);
                                return "";
                            }
                        }
                    }
                }
                else
                {
                    string updateRoomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                    con.Update(updateRoomsCacheSql);
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries检查是否可预订 ,Err :{0} ，接口返回数据(Resp) ：{1}, Time:{2} ", "json解析为空",respData, DateTime.Now.ToString()), "SunSeries接口");
                    return "";
                }
            }
            catch (Exception ex)
            {
                string updateRoomsCacheSql = string.Format(@"update SunSeriesLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                con.Update(updateRoomsCacheSql);
                LogHelper.DoOperateLog(string.Format("Err:SunSeries获取报价 ,err:{0} , 请求数据:{1} , 接口返回数据(Resp) ：{2},Time:{3} ", ex.Message, postBoby,respData, DateTime.Now.ToString()), "Err");
                return "";
            }
            if (IsCan == 1)
            {
                result = listPrice.OrderBy(x => x.offerPrice).Take(1).ToList()[0].result;
            }
            return result;
        }

        //预订计算价格
        public string createQuote(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId, decimal TotalMoney)
        {
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getQuoteUrl = string.Empty;
            try
            {
                string strRecheckIsBook = GetRecheckIsBook(hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId, TotalMoney);
                if (strRecheckIsBook == "")
                {
                    return "-1";
                }
                int adults = occupancy.adults;
                int children = occupancy.children;
                string strChildAges = occupancy.childAges ?? "";
                string bedType = "";
                common.HotelBedType hbt = new common.HotelBedType();

                string BedTypeName = hbt.HotelBedTypeToName(strRecheckIsBook.Replace(strRecheckIsBook.Split('_')[0] + strRecheckIsBook.Split('_')[1], ""));
                if (BedTypeName == "其他")
                {
                    if (adults == 1)
                    {
                        bedType = "single";
                    }
                    else if (adults == 2)
                    {
                        bedType = "twin";
                    }
                }
                else if (BedTypeName == "大床")
                {
                    bedType = "double";
                }
                else if (BedTypeName == "单人床")
                {
                    bedType = "single";
                }
                else if (BedTypeName == "双床")
                {
                    bedType = "twin";
                }
                getQuoteUrl = url + string.Format(@"/v1/search_result/{0}/quote?token={1}", "search_result::search::" + strRecheckIsBook.Split('_')[0], sunSeriesToken);
                SunSeriesReq.CreatequoteReqData cqr = new SunSeriesReq.CreatequoteReqData();

                List<SunSeriesReq.CreatequoteRooms> reqRooms = new List<SunSeriesReq.CreatequoteRooms>();
                for (int i = 0; i < RoomCount; i++)
                {
                    List<string> option_ids = new List<string>();
                    //option_ids.Add(hotelID);
                    SunSeriesReq.CreatequoteRooms reqRoom = new SunSeriesReq.CreatequoteRooms();
                    reqRoom.sample_id = strRecheckIsBook.Split('_')[1].AsTargetType<string>("");

                    reqRoom.bed_configuration = bedType;
                    reqRoom.option_ids = option_ids;
                    reqRooms.Add(reqRoom);
                }
                cqr.rooms = reqRooms;
                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(cqr);
                try
                {
                    respData = Common.Common.PostHttp(getQuoteUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries预订计算价格 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries预订计算价格 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    return "-1";
                } 
                SunSeriesReq.CreatequoteRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.CreatequoteRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    decimal  RecheckTotalPrice = 0;
                    for (int j = 0; j < RoomCount; j++)
                    {
                        int days = Convert.ToDateTime(endTime).Subtract(Convert.ToDateTime(beginTime)).Days;
                        if (respObj.rooms[j].itemization.nights.Count != days)
                        {
                            return "-1";
                        }
                        RecheckTotalPrice += respObj.rooms[j].itemization.total.AsTargetType<decimal>(0);
                    }

                    return RecheckTotalPrice + "," + respObj.id;
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries预订计算价格 ,Err :{0} ， Time:{1} ", "预订计算价格json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    return "-1";
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries预订计算价格 ,err:{0} , 请求数据:{1} ,接口返回数据(Resp) ：{2}, Time:{3} ", ex.Message, postBoby,respData, DateTime.Now.ToString()), "Err");
                return "-1";
            }
            return "-1";
        }
        //创建订单
        public string Create_Order(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string postBookUrl = string.Empty;
            try
            {
                //string guID = Common.Common.getGUID();
                decimal recheckPrice = 0;
                string CheckID = string.Empty;
                //string sqlHp = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and RateplanId='{1}'", HotelID, RateplanId);
                //DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                int Adults = 0;
                int Childs = 0;
                string childAges = string.Empty;
                IntOccupancyInfo occupancy = new IntOccupancyInfo();
                for (int j = 0; j < customers[0].Customers.Count; j++)
                {
                    if (customers[0].Customers[j].age == -1)//成年人输入参数没值默认传-1所以写死为30
                    {
                        Adults = Adults + 1;
                    }
                    else
                    {
                        Childs = Childs + 1;
                        childAges = customers[0].Customers[j].age + ",";
                    }
                }
                occupancy.adults = Adults;
                occupancy.children = Childs;
                occupancy.childAges = childAges.TrimEnd(',');
                //预订前计算价格
                string strRecheckPrice = createQuote(HotelID, beginTime, endTime, occupancy, roomNum, RateplanId, TotalMoney);
                if (strRecheckPrice == "-1")
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries获取报价 ,Err :{0} ， Time:{1} ", "检查价格出差或检查价格不可预订", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败失败，该酒店不可预订" };
                }
                else if (strRecheckPrice.Contains(","))
                {
                    recheckPrice = strRecheckPrice.Split(',')[0].AsTargetType<decimal>(0);
                    CheckID = strRecheckPrice.Split(',')[1].AsTargetType<string>("");
                }
                decimal RMBprice = recheckPrice * HKDtoRMBrate.AsTargetType<decimal>(0);
                decimal price = recheckPrice.AsTargetType<decimal>(0);
                //string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and RateplanId='{1}'", HotelID, RateplanId);
                //DataTable dt = ControllerFactory.GetController().GetDataTable(sql);

                postBookUrl = url + string.Format(@"/v1/quote/{0}/book?token={1}", CheckID, sunSeriesToken);
                SunSeriesReq.SunSeriesBookReqData bookReq = new SunSeriesReq.SunSeriesBookReqData();


                List<string> remarks = new List<string>();
                List<string> recipients = new List<string>();
                List<SunSeriesReq.roomsBookReq> ssrbs = new List<SunSeriesReq.roomsBookReq>();
                
                for (int i = 0; i < roomNum; i++)
                {
                    SunSeriesReq.roomsBookReq ssrb = new SunSeriesReq.roomsBookReq();
                    List<SunSeriesReq.adultsBookReq> adultsObj = new List<SunSeriesReq.adultsBookReq>();
                    List<SunSeriesReq.childrenBookReq> childrenObj = new List<SunSeriesReq.childrenBookReq>();
                    for (int j = 0; j < customers[i].Customers.Count; j++)
                    {
                        string title = "MS";
                        SunSeriesReq.adultsBookReq adultObj = new SunSeriesReq.adultsBookReq();
                        SunSeriesReq.childrenBookReq childObj = new SunSeriesReq.childrenBookReq();
                        if (customers[i].Customers[j].age == -1)//成年人输入参数没值默认传-1所以写死为30
                        {
                            title = "MR";
                            if (customers[i].Customers[j].sex == 1)
                            {
                                title = "MR";
                            }
                            adultObj.title = title;
                            adultObj.last_name = customers[i].Customers[j].lastName;
                            adultObj.first_name = customers[i].Customers[j].firstName;
                        }
                        else
                        {
                            childObj.last_name = customers[i].Customers[j].lastName;
                            childObj.first_name = customers[i].Customers[j].firstName;
                        }
                        adultsObj.Add(adultObj);
                        childrenObj.Add(childObj);
                    }
                    ssrb.adults = adultsObj;
                    ssrb.children = childrenObj;
                    ssrbs.Add(ssrb);
                }
                recipients.Add("wong@xiwantrip.com");
                bookReq.remarks = remarks;
                bookReq.agent_reference = inOrderNum;
                bookReq.departure_flight_number = "";
                bookReq.departure_flight_time = "";
                bookReq.arrival_flight_number = "";
                bookReq.arrival_flight_time = "";
                bookReq.notification_recipients = recipients;
                bookReq.rooms = ssrbs;

                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(bookReq);
                try
                {
                    respData = Common.Common.PostHttp(postBookUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败失败，" + ex.ToString() };
                }
                SunSeriesReq.SunSeriesBookRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.SunSeriesBookRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    string[] booking_id = respObj.booking_id.Replace("::", ";").Split(';');
                    string strUpdateSql = string.Format(@"update SunSeries_Order set StatusCode='{0}',UpdateTime=GETDATE(),BookingConfirmationID='{1}',StatusName='{2}',
ClientPartnerAmount='{3}',RMBprice='{4}',BookingCode='{5}',Status='1',IsClose=1,Adults={6},Childs={7},ChildAges='{8}'  where InOrderNum='{9}' and HotelId='{10}' ",
respObj.state, "", "确认", price, RMBprice, booking_id[1], Adults, Childs, childAges.TrimEnd(','), inOrderNum, HotelID);
                    con.Update(strUpdateSql);

                    if (respObj.state == "confirmed" || respObj.state == "guaranteed")
                    {
                        resp = new Respone() { code = "00", orderNum = booking_id[1], orderTotal = RMBprice, orderOriginalPrice = "HK$" + recheckPrice, Status = "确认", mes = "预订成功" };
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订状态不正确", DateTime.Now.ToString()), "SunSeries接口");
                        resp = new Respone() { code = "99", Status = respObj.state, mes = "预订失败" };
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} ,接口返回数据(Resp) ：{2}, Time:{3} ", ex.Message, postBoby,respData, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //报价接口返回原始数据
        public string GetRoomTypePriceToOriginal(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            //DataTable dt = new DataTable();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string getPriceUrl = url + string.Format(@"/v1/search_with_results?token={0}", sunSeriesToken);

            int adults = occupancy.adults;
            int children = occupancy.children;
            string strChildAges = occupancy.childAges ?? "";
            string CheckIn = Convert.ToDateTime(beginTime).ToString("yyyy-MM-dd");
            string Checkout = Convert.ToDateTime(endTime).ToString("yyyy-MM-dd");
            string StrKey = hotelID + "%" + CheckIn + "%" + Checkout + "%" + adults + "%" + children + "%" + strChildAges + "%" + RoomCount;
            try
            {
                string sql = string.Format(@"select ht.* from TravelIntl.dbo.SunSeriesHotel ht WITH(NOLOCK) where HotelID='{0}';", hotelID);
                DataTable dtHotel = con2.GetDataTable(sql);
                if (dtHotel.Rows.Count == 0)
                {
                    return "不存在该酒店";
                }
                SunSeriesReq.CriteriaReqData crd = new SunSeriesReq.CriteriaReqData();

                List<SunSeriesReq.rooms> reqRooms = new List<SunSeriesReq.rooms>();
                for (int i = 0; i < RoomCount; i++)
                {
                    SunSeriesReq.rooms reqRoom = new SunSeriesReq.rooms();
                    List<SunSeriesReq.children> reqChildrens = new List<SunSeriesReq.children>();
                    if (occupancy.childAges != null && occupancy.childAges != "")
                    {
                        string[] ArrayAges = occupancy.childAges.Split(',');
                        for (int k = 0; k < ArrayAges.Length; k++)
                        {
                            SunSeriesReq.children reqChildren = new SunSeriesReq.children();
                            reqChildren.age = ArrayAges[k].AsTargetType<int>(0);
                            reqChildrens.Add(reqChildren);
                        }
                    }
                    reqRoom.adults = occupancy.adults;
                    reqRoom.children = reqChildrens;

                    reqRooms.Add(reqRoom);
                }

                List<string> hotel_ids = new List<string>();
                hotel_ids.Add(hotelID);

                SunSeriesReq.criteria cr = new SunSeriesReq.criteria();
                cr.from = beginTime;
                cr.to = endTime;
                cr.hotel_ids = hotel_ids;
                cr.customer_country = dtHotel.Rows[0]["CountryID"].AsTargetType<string>("");
                cr.city = dtHotel.Rows[0]["CityID"].AsTargetType<string>("");
                cr.rooms = reqRooms;

                crd.criteria = cr;
                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(crd);
                try
                {
                    respData = Common.Common.PostHttp(getPriceUrl, postBoby);
                }
                catch (Exception ex)
                {
                    return ex.Message;
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return respData;
        }


        //创建订单
        public string Create_OrderTo(string HotelID, string RateplanId, BookingInfo customers, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string sunSeriesToken = GetAuthenticate();//取token
            string postBookUrl = string.Empty;
            try
            {
                //string guID = Common.Common.getGUID();
                decimal recheckPrice = 0;
                string CheckID = string.Empty;
                //string sqlHp = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and RateplanId='{1}'", HotelID, RateplanId);
                //DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);
                int Adults = customers.adults;
                int Childs = 0;
                int Children = 0;
                string childAges = customers.childAge ?? "";
                string roomChildrenAge = string.Empty;
                if (childAges.Trim()!="")
                {
                    Childs = (customers.childAge ?? "").Split(',').Length;
                }
                if (Childs > 0)
                {
                    Children = Math.Ceiling((Childs / roomNum).AsTargetType<double>(0)).AsTargetType<int>(0);
                    for (int i = 0; i < Children; i++)
                    {
                        roomChildrenAge += customers.childAge.Split(',')[i] + ",";
                    }
                    roomChildrenAge = roomChildrenAge.TrimEnd(',');
                }
                
                IntOccupancyInfo occupancy = new IntOccupancyInfo();
                
                occupancy.adults = Math.Ceiling((Adults/roomNum).AsTargetType<double>(0)).AsTargetType<int>(0);
                occupancy.children = Children;
                occupancy.childAges = roomChildrenAge;
                //预订前计算价格
                string strRecheckPrice = createQuote(HotelID, beginTime, endTime, occupancy, roomNum, RateplanId, TotalMoney);
                if (strRecheckPrice == "-1")
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries创建订单 ,Err :{0} ， Time:{1} ", "检查价格出差或检查价格不可预订", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败失败，该酒店不可预订" };
                }
                else if (strRecheckPrice.Contains(","))
                {
                    recheckPrice = strRecheckPrice.Split(',')[0].AsTargetType<decimal>(0);
                    CheckID = strRecheckPrice.Split(',')[1].AsTargetType<string>("");
                }
                decimal RMBprice = recheckPrice * HKDtoRMBrate.AsTargetType<decimal>(0);
                decimal price = recheckPrice.AsTargetType<decimal>(0);
                //string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'SunSeries' and RateplanId='{1}'", HotelID, RateplanId);
                //DataTable dt = ControllerFactory.GetController().GetDataTable(sql);

                postBookUrl = url + string.Format(@"/v1/quote/{0}/book?token={1}", CheckID, sunSeriesToken);
                SunSeriesReq.SunSeriesBookReqData bookReq = new SunSeriesReq.SunSeriesBookReqData();


                List<string> remarks = new List<string>();
                List<string> recipients = new List<string>();
                List<SunSeriesReq.roomsBookReq> ssrbs = new List<SunSeriesReq.roomsBookReq>();

                for (int i = 0; i < roomNum; i++)
                {
                    SunSeriesReq.roomsBookReq ssrb = new SunSeriesReq.roomsBookReq();
                    List<SunSeriesReq.adultsBookReq> adultsObj = new List<SunSeriesReq.adultsBookReq>();
                    List<SunSeriesReq.childrenBookReq> childrenObj = new List<SunSeriesReq.childrenBookReq>();
                    SunSeriesReq.adultsBookReq adultObj = new SunSeriesReq.adultsBookReq();
                    SunSeriesReq.childrenBookReq childObj = new SunSeriesReq.childrenBookReq();

                    adultObj.title = "MR";
                    adultObj.last_name = customers.name.Split(',')[i].Split('/')[0];
                    adultObj.first_name = customers.name.Split(',')[i].Split('/')[1];
                    adultsObj.Add(adultObj);
                    childrenObj.Add(childObj);

                    ssrb.adults = adultsObj;
                    ssrb.children = childrenObj;
                    ssrbs.Add(ssrb);
                }
                recipients.Add("wong@xiwantrip.com");
                bookReq.remarks = remarks;
                bookReq.agent_reference = inOrderNum;
                bookReq.departure_flight_number = "";
                bookReq.departure_flight_time = "";
                bookReq.arrival_flight_number = "";
                bookReq.arrival_flight_time = "";
                bookReq.notification_recipients = recipients;
                bookReq.rooms = ssrbs;

                postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(bookReq);
                try
                {
                    respData = Common.Common.PostHttp(postBookUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "SunSeries接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败失败，" + ex.ToString() };
                }
                SunSeriesReq.SunSeriesBookRespData respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<SunSeriesReq.SunSeriesBookRespData>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    string[] booking_id = respObj.booking_id.Replace("::", ";").Split(';');
                    string strUpdateSql = string.Format(@"update SunSeries_Order set StatusCode='{0}',UpdateTime=GETDATE(),BookingConfirmationID='{1}',StatusName='{2}',
ClientPartnerAmount='{3}',RMBprice='{4}',BookingCode='{5}',Status='1',IsClose=1,Adults={6},Childs={7},ChildAges='{8}'  where InOrderNum='{9}' and HotelId='{10}' ",
respObj.state, "", "确认", price, RMBprice, booking_id[1], Adults, Childs, childAges.TrimEnd(','), inOrderNum, HotelID);
                    con.Update(strUpdateSql);

                    if (respObj.state == "confirmed" || respObj.state == "guaranteed")
                    {
                        resp = new Respone() { code = "00", orderNum = booking_id[1], orderTotal = RMBprice, orderOriginalPrice = "HK$" + recheckPrice, Status = "确认", mes = "预订成功" };
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订状态不正确", DateTime.Now.ToString()), "SunSeries接口");
                        resp = new Respone() { code = "99", Status = respObj.state, mes = "预订失败" };
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：SunSeries预订 ,Err :{0} ， Time:{1} ", "预订json解析出错", DateTime.Now.ToString()), "SunSeries接口");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:SunSeries预订 ,err:{0} , 请求数据:{1} ,接口返回数据(Resp) ：{2}, Time:{3} ", ex.Message, postBoby,respData, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }


        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string orderOriginalPrice { get; set; }//预订原始总额
            public string Status { get; set; }//状态
            public Reservation Reservation { get; set; }
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
        public class roomPrice
        {
            public decimal offerPrice { get; set; }
            public string result { get; set; }
        }
    }
}