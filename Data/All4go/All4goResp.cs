﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.All4go
{
    public class All4goResp
    {
        public class CurrencyResp
        {
            public List<currencies> currencies { get; set; }
            public List<paymentproviders> paymentproviders { get; set; }
        }
        public class currencies
        {
            public string agencycurrency_id { get; set; }
            public string agency_id { get; set; }
            public string currency_code { get; set; }
            public string currency_b2b { get; set; }
            public string currency_b2c { get; set; }
        }
        public class paymentproviders 
        {
            public string paymentprovider2agency_id { get; set; }
            public string paymentprovider_id { get; set; }
            public string agency_id { get; set; }
            public string paymentprovider2agency_markup { get; set; }
            public string paymentprovider_name { get; set; }
            public string paymentprovider_code { get; set; }
            public string paymentprovider_status { get; set; }
            public string paymentprovider_logo { get; set; }
        }

        public class LocationCodeResp
        {
            public string location_id { get; set; }
            public string location_name { get; set; }
            public string country_id { get; set; }
            public string country_name { get; set; }
            public string location_type { get; set; }
            public string location_code { get; set; }
            public string region_name { get; set; }
        }
    }
}