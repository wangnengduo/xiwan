﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.All4go
{
    public class All4goReq
    {
        public class GetCurrencyCodeReq
        {
            public string agency { get; set; }
            public string username { get; set; }
            public string password { get; set; }
        }
    }
}