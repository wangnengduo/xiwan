﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;

namespace XiWan.Data.All4go
{
    public class All4goDAL
    {
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        string url = CCommon.GetWebConfigValue("All4goUrl");//url
        string Agency = CCommon.GetWebConfigValue("All4goAgency");//
        string Username = CCommon.GetWebConfigValue("All4goUsername");
        string Password = CCommon.GetWebConfigValue("All4goPassword");//
        string All4goCacheTime = CCommon.GetWebConfigValue("All4goCacheTime");//缓存到数据库的有效期（分钟）

        #region 初始化本类
        private static All4goDAL _Instance;
        public static All4goDAL Instance
        {
            get
            {
                return new All4goDAL();
            }
        }
        #endregion

        //获取货币类型
        public void GetCurrencyCode()
        {
            string respData = string.Empty;
            string postBoby = string.Empty;
            All4goReq.GetCurrencyCodeReq objReq = new All4goReq.GetCurrencyCodeReq();
            objReq.agency = Agency;
            objReq.username = Username;
            objReq.password = Password;

            postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(objReq);
            try
            {
                try
                {
                    respData = Common.Common.PostHttp(url + "getCurrencies4Root", postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：All4go获取货币类型 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "All4go接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:All4go获取货币类型,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                All4goResp.CurrencyResp currencyInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<All4goResp.CurrencyResp>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (currencyInfo != null)
                {
                    All4goCurrencyCollection objCollection = new All4goCurrencyCollection();
                    for (int i = 0; i < currencyInfo.currencies.Count; i++)
                    {
                        All4goCurrency obj = new All4goCurrency();
                        obj.agencycurrency_id = currencyInfo.currencies[i].agencycurrency_id;
                        obj.agency_id = currencyInfo.currencies[i].agency_id;
                        obj.currency_code = currencyInfo.currencies[i].currency_code;
                        obj.currency_b2b = currencyInfo.currencies[i].currency_b2b;
                        obj.currency_b2c = currencyInfo.currencies[i].currency_b2c;
                        objCollection.Add(obj);
                    }
                    con.Save(objCollection);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:All4go获取货币类型 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        //获取所有地点
        public void GetLocationCode()
        {
            string respData = string.Empty;
            string postBoby = string.Empty;
            All4goReq.GetCurrencyCodeReq objReq = new All4goReq.GetCurrencyCodeReq();
            objReq.agency = Agency;
            objReq.username = Username;
            objReq.password = Password;

            postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(objReq);
            try
            {
                try
                {
                    respData = Common.Common.PostHttp(url + "getLocationsJson", postBoby);
                    //LogHelper.DoOperateLog(string.Format("Studio：All4go获取所有地点 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "All4go接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:All4go获取所有地点,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                if (respData == null || respData == "" || respData == "操作超时")
                {
                    return;
                }
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                serializer.MaxJsonLength = Int32.MaxValue;
                Dictionary<string, object> obj_json = serializer.DeserializeObject(respData) as Dictionary<string, object>;

                List<string> keys = obj_json.Keys.ToList();
                for (int i = 0; i < keys.Count; i++)
                {
                    string value = serializer.Serialize(obj_json[keys[i]]);//取每组key
                    List<All4goResp.LocationCodeResp> objResp = Newtonsoft.Json.JsonConvert.DeserializeObject<List<All4goResp.LocationCodeResp>>(value, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (objResp != null)
                    {
                        All4goLocationCodeCollection objCol = new All4goLocationCodeCollection();
                        for (int j = 0; j < objResp.Count; j++)
                        {
                            All4goLocationCode obj = new All4goLocationCode();
                            obj.location_id = objResp[j].location_id ?? "";
                            obj.location_name = objResp[j].location_name ?? "";
                            obj.country_id = objResp[j].country_id ?? "";
                            obj.country_name = objResp[j].country_name ?? "";
                            obj.location_type = objResp[j].location_type ?? "";
                            obj.location_code = objResp[j].location_code ?? "";
                            obj.region_name = objResp[j].region_name ?? "";
                            con.Save(obj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:All4go获取所有地点 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        //获取打包产品
        public void GetPackages()
        {
            string respData = string.Empty;
            string postBoby = string.Empty;
            All4goReq.GetCurrencyCodeReq objReq = new All4goReq.GetCurrencyCodeReq();
            objReq.agency = Agency;
            objReq.username = Username;
            objReq.password = Password;

            postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(objReq);
            try
            {
                try
                {
                    respData = Common.Common.PostHttp(url + "getPackagesJson", postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：All4go获取打包产品 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "All4go接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:All4go获取打包产品,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                /*All4goResp.CurrencyResp currencyInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<All4goResp.CurrencyResp>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (currencyInfo != null)
                {
                    All4goCurrencyCollection objCollection = new All4goCurrencyCollection();
                    for (int i = 0; i < currencyInfo.currencies.Count; i++)
                    {
                        All4goCurrency obj = new All4goCurrency();
                        obj.agencycurrency_id = currencyInfo.currencies[i].agencycurrency_id;
                        obj.agency_id = currencyInfo.currencies[i].agency_id;
                        obj.currency_code = currencyInfo.currencies[i].currency_code;
                        obj.currency_b2b = currencyInfo.currencies[i].currency_b2b;
                        obj.currency_b2c = currencyInfo.currencies[i].currency_b2c;
                        objCollection.Add(obj);
                    }
                    con.Save(objCollection);
                }*/
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:All4go获取打包产品 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        //获取设施
        public void GetFacilities()
        {
            string respData = string.Empty;
            string postBoby = string.Empty;
            All4goReq.GetCurrencyCodeReq objReq = new All4goReq.GetCurrencyCodeReq();
            objReq.agency = Agency;
            objReq.username = Username;
            objReq.password = Password;

            postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(objReq);
            try
            {
                try
                {
                    respData = Common.Common.PostHttp(url + "getFacilitiesJson", postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：All4go获取设施 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "All4go接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:All4go获取设施,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                /*All4goResp.CurrencyResp currencyInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<All4goResp.CurrencyResp>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (currencyInfo != null)
                {
                    All4goCurrencyCollection objCollection = new All4goCurrencyCollection();
                    for (int i = 0; i < currencyInfo.currencies.Count; i++)
                    {
                        All4goCurrency obj = new All4goCurrency();
                        obj.agencycurrency_id = currencyInfo.currencies[i].agencycurrency_id;
                        obj.agency_id = currencyInfo.currencies[i].agency_id;
                        obj.currency_code = currencyInfo.currencies[i].currency_code;
                        obj.currency_b2b = currencyInfo.currencies[i].currency_b2b;
                        obj.currency_b2c = currencyInfo.currencies[i].currency_b2c;
                        objCollection.Add(obj);
                    }
                    con.Save(objCollection);
                }*/
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:All4go获取设施 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
        //获取Giata酒店地址
        public void GetLocationOfGiataHotel()
        {
            string respData = string.Empty;
            string postBoby = string.Empty;
            All4goReq.GetCurrencyCodeReq objReq = new All4goReq.GetCurrencyCodeReq();
            objReq.agency = Agency;
            objReq.username = Username;
            objReq.password = Password;

            postBoby = Newtonsoft.Json.JsonConvert.SerializeObject(objReq);
            try
            {
                try
                {
                    respData = Common.Common.PostHttp(url + "getLocationOfGiataHotel", postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：All4go获取Giata酒店地址 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "All4go接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:All4go获取Giata酒店地址,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                /*All4goResp.CurrencyResp currencyInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<All4goResp.CurrencyResp>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (currencyInfo != null)
                {
                    All4goCurrencyCollection objCollection = new All4goCurrencyCollection();
                    for (int i = 0; i < currencyInfo.currencies.Count; i++)
                    {
                        All4goCurrency obj = new All4goCurrency();
                        obj.agencycurrency_id = currencyInfo.currencies[i].agencycurrency_id;
                        obj.agency_id = currencyInfo.currencies[i].agency_id;
                        obj.currency_code = currencyInfo.currencies[i].currency_code;
                        obj.currency_b2b = currencyInfo.currencies[i].currency_b2b;
                        obj.currency_b2c = currencyInfo.currencies[i].currency_b2c;
                        objCollection.Add(obj);
                    }
                    con.Save(objCollection);
                }*/
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:All4go获取Giata酒店地址 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }
    }
}