﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelHotelResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "hoteles")]
            public hoteles hoteles { get; set; }
        }
        public class hoteles
        {
            [XmlElement(ElementName = "hotel")]
            public List<hotel> hotel { get; set; }
        }
        public class hotel
        {
            [XmlElement(ElementName = "codigo")]
            public string codigo { get; set; }
            [XmlElement(ElementName = "afiliacion")]
            public string afiliacion { get; set; }
            [XmlElement(ElementName = "nombre_h")]
            public string nombre_h { get; set; }
            [XmlElement(ElementName = "direccion")]
            public string direccion { get; set; }
            [XmlElement(ElementName = "provincia")]
            public string provincia { get; set; }
            [XmlElement(ElementName = "provincia_nombre")]
            public string provincia_nombre { get; set; }
            [XmlElement(ElementName = "poblacion")]
            public string poblacion { get; set; }
            //[XmlElement(ElementName = "descripcion")]
            //public string descripcion { get; set; }
            //[XmlElement(ElementName = "como_llegar")]
            //public string como_llegar { get; set; }
            [XmlElement(ElementName = "categoria")]
            public string categoria { get; set; }
            [XmlElement(ElementName = "foto")]
            public string foto { get; set; }
            [XmlElement(ElementName = "calidad")]
            public string calidad { get; set; }
            [XmlElement(ElementName = "marca")]
            public string marca { get; set; }
            [XmlElement(ElementName = "longitud")]
            public string longitud { get; set; }
            [XmlElement(ElementName = "latitud")]
            public string latitud { get; set; }
            [XmlElement(ElementName = "categoria2")]
            public string categoria2 { get; set; }
            [XmlElement(ElementName = "codigo_cobol")]
            public string codigo_cobol { get; set; }
        }
    }
}