﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class SaleableHotelInformation
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "hotel")]
            public hotel hotel { get; set; }
            [XmlElement(ElementName = "error")]
            public string error { get; set; }
        }
        public class hotel
        {
            [XmlElement(ElementName = "pais")]
            public string pais { get; set; }
            [XmlElement(ElementName = "codigo_hotel")]
            public string codigo_hotel { get; set; }
            [XmlElement(ElementName = "codigo")]
            public string codigo { get; set; }
            [XmlElement(ElementName = "hot_afiliacion")]
            public string hot_afiliacion { get; set; }
            [XmlElement(ElementName = "nombre_h")]
            public string nombre_h { get; set; }
            [XmlElement(ElementName = "direccion")]//Hotel address
            public string direccion { get; set; }
            [XmlElement(ElementName = "codprovincia")]//Province code
            public string codprovincia { get; set; }
            [XmlElement(ElementName = "codpoblacion")]//Town code
            public string codpoblacion { get; set; }
            [XmlElement(ElementName = "provincia")]//Province code
            public string provincia { get; set; }
            [XmlElement(ElementName = "poblacion")]
            public string poblacion { get; set; }//Hotel town
            [XmlElement(ElementName = "cp")]
            public string cp { get; set; }
            [XmlElement(ElementName = "coddup")]
            public string coddup { get; set; }
            [XmlElement(ElementName = "mail")]
            public string mail { get; set; }
            [XmlElement(ElementName = "web")]
            public string web { get; set; }
            [XmlElement(ElementName = "telefono")]
            public string telefono { get; set; }
            [XmlElement(ElementName = "fotos")]
            public fotos fotos { get; set; }

            [XmlElement(ElementName = "categoria")]
            public string categoria { get; set; }//staring

            [XmlElement(ElementName = "plano")]
            public string plano { get; set; }
            [XmlElement(ElementName = "desc_hotel")]
            public string desc_hotel { get; set; }
            [XmlElement(ElementName = "num_habitaciones")]
            public string num_habitaciones { get; set; }
            [XmlElement(ElementName = "longitud")]
            public string longitud { get; set; }//longitud
            [XmlElement(ElementName = "latitud")]
            public string latitud { get; set; }//latitud
            [XmlElement(ElementName = "categoria2")]
            public string categoria2 { get; set; }
            [XmlElement(ElementName = "codigo_cobol")]
            public string codigo_cobol { get; set; }
            [XmlElement(ElementName = "logo_h")]
            public string logo_h { get; set; }
        }
        public class fotos
        {
            [XmlElement(ElementName = "foto")]
            public List<string> foto { get; set; }
        }
    }
}