﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Restel
{
    public class RestelCancellationReq
    {
        public class peticion
        {
            public string tipo { get; set; }

            public string nombre { get; set; }

            public string agencia { get; set; }

            public parametros parametros { get; set; }
        }
        public class parametros
        {
            public List<datos_reserva> datos_reserva { get; set; }
        }
        public class datos_reserva
        {
            public string hotel { get; set; }

            public List<string> lin { get; set; }
        }

    }
}