﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelHotelQuoteResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "param")]
            public param param { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "id")]
            public string id { get; set; }
            [XmlElement(ElementName = "error")]
            public parametrosError error { get; set; }
        }
        public class parametrosError
        {
            [XmlElement(ElementName = "descripcion")]
            public string descripcion { get; set; }
        }
        public class param
        {
            [XmlElement(ElementName = "hotls")]
            public hotls hotls { get; set; }
            [XmlElement(ElementName = "error")]
            public error error { get; set; }
        }
        public class error
        {
            [XmlElement(ElementName = "num_error")]
            public string num_error { get; set; }
        }
        public class hotls
        {
            [XmlAttribute("num")]
            public string num { get; set; }
            [XmlElement(ElementName = "hot")]
            public List<hot> hot { get; set; }
        }
        public class hot
        {
            [XmlElement(ElementName = "cod")]
            public string cod { get; set; }
            [XmlElement(ElementName = "afi")]
            public string afi { get; set; }
            [XmlElement(ElementName = "nom")]
            public string nom { get; set; }
            [XmlElement(ElementName = "pro")]
            public string pro { get; set; }
            [XmlElement(ElementName = "prn")]
            public string prn { get; set; }
            [XmlElement(ElementName = "pob")]
            public string pob { get; set; }
            [XmlElement(ElementName = "cat")]
            public string cat { get; set; }
            [XmlElement(ElementName = "fen")]
            public string fen { get; set; }
            [XmlElement(ElementName = "fsa")]
            public string fsa { get; set; }
            [XmlElement(ElementName = "pdr")]
            public string pdr { get; set; }
            [XmlElement(ElementName = "cal")]
            public string cal { get; set; }
            [XmlElement(ElementName = "mar")]
            public string mar { get; set; }
            [XmlElement(ElementName = "res")]
            public res res { get; set; }
            [XmlElement(ElementName = "pns")]
            public string pns { get; set; }
            [XmlElement(ElementName = "end")]
            public string end { get; set; }
            [XmlElement(ElementName = "enh")]
            public string enh { get; set; }
            [XmlElement(ElementName = "cat2")]
            public string cat2 { get; set; }
            [XmlElement(ElementName = "vafs")]
            public vafs vafs { get; set; }
        }
        public class res
        {
            [XmlElement(ElementName = "pax")]
            public List<pax> pax { get; set; }
        }
        public class pax
        {
            [XmlAttribute("cod")]
            public string cod { get; set; }
            [XmlElement(ElementName = "hab")]
            public List<hab> hab { get; set; }
        }
        public class hab
        {
            [XmlAttribute("cod")]
            public string cod { get; set; }
            [XmlAttribute("desc")]
            public string desc { get; set; }
            [XmlElement(ElementName = "reg")]
            public List<reg> reg { get; set; }
        }
        public class reg
        {
            [XmlAttribute("cod")]
            public string cod { get; set; }
            [XmlAttribute("prr")]
            public string prr { get; set; }
            [XmlAttribute("pvp")]
            public string pvp { get; set; }
            [XmlAttribute("div")]
            public string div { get; set; }
            [XmlAttribute("esr")]
            public string esr { get; set; }

            [XmlElement(ElementName = "lin")]
            public List<string> lin { get; set; }
        }
        public class linItem
        {
            [XmlElement(ElementName = "lin")]
            public string lin { get; set; }
        }
        public class vafs
        {
            [XmlElement(ElementName = "vaf")]
            public string vaf { get; set; }
        }
    }
}