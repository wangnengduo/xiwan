﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelCancellationFeesResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "error")]
            public string error { get; set; }
            [XmlElement(ElementName = "politicaCanc")]
            public List<politicaCanc> politicaCanc { get; set; }
        }
        public class politicaCanc
        {
            [XmlElement(ElementName = "localizador")]
            public string localizador { get; set; }
            [XmlElement(ElementName = "dias_antelacion")]
            public string dias_antelacion { get; set; }
            [XmlElement(ElementName = "horas_antelacion")]
            public string horas_antelacion { get; set; }
            [XmlElement(ElementName = "noches_gasto")]
            public string noches_gasto { get; set; }
            [XmlElement(ElementName = "estCom_gasto")]
            public string estCom_gasto { get; set; }
            [XmlElement(ElementName = "desc")]
            public string desc { get; set; }
        }

    }
}