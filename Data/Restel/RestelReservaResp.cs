﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelReservaResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "error")]
            public string error { get; set; }
            [XmlElement(ElementName = "estado")]
            public string estado { get; set; }
            [XmlElement(ElementName = "n_localizador")]
            public string n_localizador { get; set; }
            [XmlElement(ElementName = "importe_total_reserva")]
            public string importe_total_reserva { get; set; }
            [XmlElement(ElementName = "divisa_total")]
            public string divisa_total { get; set; }
            [XmlElement(ElementName = "n_mensaje")]
            public string n_mensaje { get; set; }
            [XmlElement(ElementName = "n_expediente")]
            public string n_expediente { get; set; }
            [XmlElement(ElementName = "observaciones")]
            public string observaciones { get; set; }
            [XmlElement(ElementName = "datos")]
            public string datos { get; set; }
        }

    }
}