﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelCountryResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "error")]
            public string error { get; set; }
            [XmlElement(ElementName = "paises")]
            public paises paises { get; set; }
        }
        public class paises
        {
            [XmlElement(ElementName = "pais")]
            public List<pais> pais { get; set; }
        }
        public class pais
        {
            [XmlElement(ElementName = "codigo_pais")]
            public string codigo_pais { get; set; }
            [XmlElement(ElementName = "nombre_pais")]
            public string nombre_pais { get; set; }
        }
    }
}