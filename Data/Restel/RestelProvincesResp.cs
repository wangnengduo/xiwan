﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelProvincesResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "provincias")]
            public provincias provincias { get; set; }
        }
        public class provincias
        {
            [XmlElement(ElementName = "provincia")]
            public List<provincia> provincia { get; set; }
        }
        public class provincia
        {
            [XmlElement(ElementName = "codigo_pais")]
            public string codigo_pais { get; set; }
            [XmlElement(ElementName = "codigo_provincia")]
            public string codigo_provincia { get; set; }
            [XmlElement(ElementName = "nombre_provincia")]
            public string nombre_provincia { get; set; }
        }
    }
}