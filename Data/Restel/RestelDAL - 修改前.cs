﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;

namespace XiWan.Data.Restel
{
    public class RestelDAL
    {
        string RestelUrl = CCommon.GetWebConfigValue("RestelUrl");
        string RestelUsername = CCommon.GetWebConfigValue("RestelUsername");
        string RestelPassword = CCommon.GetWebConfigValue("RestelPassword");
        public static string codusu = CCommon.GetWebConfigValue("vCodusu");
        public static string vSecacc = CCommon.GetWebConfigValue("vSecacc");
        public static string afiliacio = CCommon.GetWebConfigValue("vAfiliacio");
        public static string vCodigousu = CCommon.GetWebConfigValue("vCodigousu");
        public static string vClausu = CCommon.GetWebConfigValue("vClausu");
        string USDtoRMBrate = CCommon.GetWebConfigValue("USDtoRMBrate");//美元转为人民币汇率
        string EURtoRMBrate = CCommon.GetWebConfigValue("EURtoRMBrate");//欧元转为人民币汇率
        string SGDtoRMBrate = CCommon.GetWebConfigValue("SGDtoRMBrate");//新加坡元转为人民币汇率
        string HKDtoRMBrate = CCommon.GetWebConfigValue("HKDtoRMBrate");//港币转为人民币汇率
        string RestelCacheTime = CCommon.GetWebConfigValue("RestelCacheTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        ControllerBase con2 = ControllerFactory.GetNewController(e_ConsType.Main2);
        string Identidad = "codusu=" + codusu + "&secacc=" + vSecacc + "&afiliacio=" + afiliacio + "&codigousu=" + vCodigousu + "&clausu=" + vClausu;

        #region 初始化本类
        private static RestelDAL _Instance;
        public static RestelDAL Instance
        {
            get
            {
                return new RestelDAL();
            }
        }
        #endregion

        /// <summary>
        /// 从接口获取国家信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetRestelCountry()
        {
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_paises.dtd'><peticion><nombre></nombre>
<agencia></agencia><tipo>5</tipo><idioma>2</idioma></peticion>");
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                //string fullUrl = RestelUrl + Identidad + Parameter;
                result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取国家信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                RestelCountryResp.respuesta obj = Common.Common.DESerializer<RestelCountryResp.respuesta>(result.Replace("&", ""));
                if (obj.parametros.error == null)
                {
                    RestelCountryCollection objCountryCol = new RestelCountryCollection();
                    for (int i = 0; i < obj.parametros.paises.pais.Count; i++)
                    {
                        RestelCountry objCountry = new RestelCountry();
                        objCountry.CountryCode = obj.parametros.paises.pais[i].codigo_pais;
                        objCountry.CountryName = obj.parametros.paises.pais[i].nombre_pais;
                        objCountryCol.Add(objCountry);
                    }
                    con.Save(objCountryCol);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取国家信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }
        /// <summary>
        /// 从接口获取省信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetProvinces()
        {
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_paises.dtd'><peticion><nombre></nombre>
<agencia></agencia><tipo>6</tipo><idioma>2</idioma></peticion>");
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取省信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                RestelProvincesResp.respuesta obj = Common.Common.DESerializer<RestelProvincesResp.respuesta>(result.Replace("&", ""));
                RestelProvincesCollection objRestelProvincesCol = new RestelProvincesCollection();
                for(int i=0;i<obj.parametros.provincias.provincia.Count;i++)
                {
                    RestelProvinces objProvinces = new RestelProvinces();
                    objProvinces.CountryCode = obj.parametros.provincias.provincia[i].codigo_pais;
                    objProvinces.ProvinceCode = obj.parametros.provincias.provincia[i].codigo_provincia;
                    objProvinces.ProvinceName = obj.parametros.provincias.provincia[i].nombre_provincia;
                    objRestelProvincesCol.Add(objProvinces);
                }
                con.Save(objRestelProvincesCol); 
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取省信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }
        /// <summary>
        /// 从接口获取TOWN信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetTown()
        {
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"SELECT ProvinceCode FROM dbo.RestelProvinces");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);            
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='iso-8859-1'?> <peticion> <tipo>18</tipo> <nombre></nombre> <agencia></agencia> <parametros> <codest>{0}</codest> <codpob></codpob> <marca></marca> </parametros> </peticion>", dt.Rows[j]["ProvinceCode"]);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取TOWN信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    RestelTownCollection objRestelRestelTown = new RestelTownCollection();
                    RestelTownResp.respuesta obj = Common.Common.DESerializer<RestelTownResp.respuesta>(result.Replace("&", ""));
                    for (int i = 0; i < obj.parametros.poblaciones.poblacion.Count; i++)
                    {
                        RestelTown objTown = new RestelTown();
                        objTown.TownCode = obj.parametros.poblaciones.poblacion[i].codpob;
                        objTown.TownName = obj.parametros.poblaciones.poblacion[i].poblidinom1;
                        objTown.TownNameEn = obj.parametros.poblaciones.poblacion[i].poblidinom2;
                        objTown.TownNameFr = obj.parametros.poblaciones.poblacion[i].poblidinom3;
                        objTown.TownNameGe = obj.parametros.poblaciones.poblacion[i].poblidinom4;
                        objTown.TownNameIt = obj.parametros.poblaciones.poblacion[i].poblidinom5;
                        objTown.TownNamePo = obj.parametros.poblaciones.poblacion[i].poblidinom6;
                        objTown.ProvinceCode = obj.parametros.poblaciones.poblacion[i].deppob;
                        objRestelRestelTown.Add(objTown);
                    }
                    con.Save(objRestelRestelTown);
                }    
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取TOWN信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }
        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetHotel()
        {
            try
            {
                string sql = string.Format(@"SELECT ProvinceCode FROM dbo.RestelProvinces");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    string result = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_directorio.dtd'><peticion><tipo>7</tipo><nombre></nombre><agencia></agencia>
<parametros><hotel/><pais>{0}</pais><poblacion/><provincia/><categoria>0</categoria><usuario>{1}</usuario><afiliacion>{2}</afiliacion>
<servhot1/><servhot2/><servhot3/><servhab1/><servhab2/><servhab3/><idioma>2</idioma><radio>1</radio></parametros></peticion>", dt.Rows[j]["ProvinceCode"], codusu,  afiliacio);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    //LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取酒店信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    Regex r = new Regex("(?<=<descripcion>).*?(?=.</descripcion>)", RegexOptions.IgnoreCase);
                    result = r.Replace(result, "");
                    Regex r1 = new Regex("(?<=<descripcion>).*?(?=</descripcion>)", RegexOptions.IgnoreCase);
                    result = r1.Replace(result, "");
                    Regex r2 = new Regex("(?<=<como_llegar>).*?(?=</como_llegar>)", RegexOptions.IgnoreCase);
                    result = r2.Replace(result, "");
                    Regex r3 = new Regex("(?<=<como_llegar>).*?(?=</como_llegar>)", RegexOptions.IgnoreCase);
                    result = r3.Replace(result, "");
                    RestelHotelResp.respuesta obj = Common.Common.DESerializer<RestelHotelResp.respuesta>(result.Replace("<br />", "").Replace("<p>", "").Replace("</p>", "").Replace("<b>", "").Replace("</b>", "").Replace("&", "").Replace("</</", "</"));
                    RestelHotelCollection objRestelHotelCol = new RestelHotelCollection();
                    if (obj != null)
                    {
                        for (int i = 0; i < obj.parametros.hoteles.hotel.Count; i++)
                        {
                            RestelHotel objHotel = new RestelHotel();
                            objHotel.HotelCode = obj.parametros.hoteles.hotel[i].codigo;
                            objHotel.HotelName = obj.parametros.hoteles.hotel[i].nombre_h;
                            objHotel.ProvinceCode = obj.parametros.hoteles.hotel[i].provincia;
                            objHotel.ProvinceName = obj.parametros.hoteles.hotel[i].provincia_nombre;
                            objHotel.TownName = obj.parametros.hoteles.hotel[i].poblacion;
                            objHotel.HotelDescription = "";//obj.parametros.hoteles.hotel[i].descripcion;
                            objHotel.Staring = obj.parametros.hoteles.hotel[i].categoria;
                            objHotel.Calidad = obj.parametros.hoteles.hotel[i].calidad;
                            objHotel.Marca = obj.parametros.hoteles.hotel[i].marca;
                            objHotel.Longitud = obj.parametros.hoteles.hotel[i].longitud;
                            objHotel.Latitud = obj.parametros.hoteles.hotel[i].latitud;
                            objHotel.Categoria2 = obj.parametros.hoteles.hotel[i].categoria2;
                            objHotel.HotelCobol = obj.parametros.hoteles.hotel[i].codigo_cobol;
                            objRestelHotelCol.Add(objHotel);
                        }
                        con.Save(objRestelHotelCol);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取酒店信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }

        /// <summary>
        /// 保存房型
        /// </summary>
        /// <returns></returns>
        public DataTable GetRoomTpe( string hotelCode, string nationality, string room, string CheckInDate, string CheckoutDate,  int Adults, int Childs)
        {
            DataTable reDt = new DataTable();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"SELECT * FROM dbo.RestelHotel rh ", hotelCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    for (int f = 0; f < dt.Rows.Count; f++)
                    {
                        hotelCode = dt.Rows[f]["HotelID"].AsTargetType<string>("");
                        string strhotelCode = hotelCode + "#";//"636739#994214#962243#601022#745388#758213#752553#";
                        string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd'><peticion><tipo>110</tipo><nombre></nombre>
<agencia></agencia><parametros><hotel>{0}</hotel><pais>{1}</pais><provincia>{2}</provincia><poblacion></poblacion><categoria>0</categoria>
<radio>9</radio><fechaentrada>{3}</fechaentrada><fechasalida>{4}</fechasalida><marca></marca><afiliacion>{5}</afiliacion>
<usuario>{6}</usuario>{7}<idioma>2</idioma><duplicidad>1</duplicidad><comprimido>2</comprimido><informacion_hotel>0</informacion_hotel></parametros></peticion>",
        strhotelCode, dt.Rows[f]["CountryCode"], dt.Rows[f]["ProvinceCode"], CheckInDate, CheckoutDate, afiliacio, codusu, room);
                        string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                        try
                        {
                            result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                            LogHelper.DoOperateLog(string.Format("Studio： Service 110 保存房型,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                        }
                        catch (Exception ex)
                        {
                            LogHelper.DoOperateLog(string.Format("Err：Restel从接口获取报价Post数据保存房型 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                            //return reDt;
                        }
                        RestelHotelQuoteResp.respuesta objResp = Common.Common.DESerializer<RestelHotelQuoteResp.respuesta>(result.Replace("&", ""));
                        if (objResp != null)
                        {
                            if (objResp.param.error == null)
                            {
                                if (objResp.param.hotls.num.AsTargetType<int>(0) == 0)
                                {
                                    //return reDt;
                                }
                                else
                                {
                                    for (int i = 0; i < objResp.param.hotls.hot.Count; i++)
                                    {
                                        for (int j = 0; j < objResp.param.hotls.hot[i].res.pax.Count; j++)
                                        {
                                            for (int x = 0; x < objResp.param.hotls.hot[i].res.pax[j].hab.Count; x++)
                                            {
                                                for (int y = 0; y < objResp.param.hotls.hot[i].res.pax[j].hab[x].reg.Count; y++)
                                                {
                                                    string RoomTypeID = Common.Common.Md5Encrypt32(objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim().ToLower());
                                                    RestelRoomType obj = new RestelRoomType();
                                                    obj.HotelID = hotelCode;
                                                    obj.RoomTypeName = objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim();
                                                    obj.RoomTypeID = RoomTypeID;
                                                    obj.CreateTime = DateTime.Now;
                                                    string sqlRestelRoomType = string.Format(@"select * from RestelRoomType where RoomTypeID='{0}'",RoomTypeID);
                                                    DataTable dtRestelRoomType = con2.GetDataTable(sqlRestelRoomType);
                                                    if(dtRestelRoomType.Rows.Count==0)
                                                    {
                                                        con2.Save(obj);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                //return reDt;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口保存房型 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return reDt;
        }


        /// <summary>
        /// 从接口获取报价
        /// </summary>
        /// <returns></returns>
        public DataTable GetRealHotelSQuote(string hotelCode, string nationality, string room, string CheckInDate, string CheckoutDate, int Adults, int Childs)
        {
            DataTable reDt = new DataTable();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"SELECT * FROM dbo.RestelHotel rh where HotelID='{0}' ", hotelCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string strhotelCode = hotelCode + "#";//"636739#994214#962243#601022#745388#758213#752553#";
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd'><peticion><tipo>110</tipo><nombre></nombre>
<agencia></agencia><parametros><hotel>{0}</hotel><pais>{1}</pais><provincia>{2}</provincia><poblacion></poblacion><categoria>0</categoria>
<radio>9</radio><fechaentrada>{3}</fechaentrada><fechasalida>{4}</fechasalida><marca></marca><afiliacion>{5}</afiliacion>
<usuario>{6}</usuario>{7}<idioma>2</idioma><duplicidad>1</duplicidad><comprimido>2</comprimido><informacion_hotel>0</informacion_hotel></parametros></peticion>",
    strhotelCode, dt.Rows[0]["CountryCode"], dt.Rows[0]["ProvinceCode"], CheckInDate, CheckoutDate, afiliacio, codusu, room);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio： Service 110 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err：Restel从接口获取报价Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return reDt;
                    }
                    RestelHotelQuoteResp.respuesta objResp = Common.Common.DESerializer<RestelHotelQuoteResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.param.error == null)
                        {
                            if (objResp.param.hotls.num.AsTargetType<int>(0) == 0)
                            {
                                //return reDt;
                            }
                            else
                            {
                                HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                                for (int i = 0; i < objResp.param.hotls.hot.Count; i++)
                                {
                                    for (int j = 0; j < objResp.param.hotls.hot[i].res.pax.Count; j++)
                                    {
                                        for (int x = 0; x < objResp.param.hotls.hot[i].res.pax[j].hab.Count; x++)
                                        {
                                            for (int y = 0; y < objResp.param.hotls.hot[i].res.pax[j].hab[x].reg.Count; y++)
                                            {
                                                if (objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].esr == "OK")
                                                {
                                                    string lin = string.Empty;
                                                    for (int p = 0; p < objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin.Count; p++)
                                                    {
                                                        lin += "<lin>" + objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin[p] + "</lin>";
                                                    }
                                                    string ClientCurrencyCode = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].div;
                                                    decimal ClientPrice = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].pvp.AsTargetType<decimal>(0);
                                                    string BedTypeCode = objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim();
                                                    string toRMBrate = USDtoRMBrate;
                                                        
                                                    //售价
                                                    decimal RMBprice = ClientPrice * toRMBrate.AsTargetType<decimal>(0);
                                                    HotelPrice obj = new HotelPrice();
                                                    obj.HotelID = objResp.param.hotls.hot[i].cod;
                                                    obj.HotelNo = "";
                                                    obj.Platform = "Restel";
                                                    obj.ClassName = objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim();
                                                    obj.RoomTypeID = Common.Common.Md5Encrypt32(objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim().ToLower());
                                                    obj.RMBprice = RMBprice;
                                                    obj.Price = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].pvp.AsTargetType<decimal>(0);
                                                    obj.CurrencyCode = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].div;
                                                    string Lodging = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].cod;
                                                    if (Lodging == "RO" || Lodging == "OB" || Lodging == "SA")
                                                    {
                                                        obj.IsBreakfast = 0;
                                                    }
                                                    else
                                                    {
                                                        obj.IsBreakfast = 1;
                                                    }
                                                    obj.ReferenceClient = lin;
                                                    string Adults_Childs = objResp.param.hotls.hot[i].res.pax[j].cod;
                                                    int RoomAdults = Adults_Childs.Split('-')[0].AsTargetType<int>(0);
                                                    int RoomChilds = Adults_Childs.Split('-')[1].AsTargetType<int>(0);
                                                    obj.Adults = Adults;
                                                    obj.Childs = Childs;
                                                    obj.RoomAdults = RoomAdults;
                                                    obj.RoomChilds = RoomChilds;
                                                    obj.ChildsAge = objResp.param.hotls.hot[i].res.pax[j].cod;
                                                    obj.CheckInDate = Convert.ToDateTime(CheckInDate);
                                                    obj.CheckOutDate = Convert.ToDateTime(CheckoutDate);
                                                    obj.IsClose = 1;
                                                    obj.CreateTime = DateTime.Now;
                                                    obj.UpdateTime = DateTime.Now;
                                                    obj.EntityState = e_EntityState.Added;
                                                    objHotelPriceCol.Add(obj);
                                                }
                                            }
                                        }
                                    }
                                }
                                string updateHotelPriceSql = string.Format(@"update HotelPrice  set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' AND Platform='Restel' AND Adults={1} AND Childs={2}   AND CheckInDate='{3}' AND CheckOutDate='{4}'",
hotelCode, Adults, Childs, CheckInDate, CheckoutDate);
                                con.Update(updateHotelPriceSql);
                                con.Save(objHotelPriceCol);
                            }
                        }
                        else
                        {
                            return reDt;
                        }
                    }
                    string sqlDt = string.Format(@"select ID,HotelID,ClassName,RMBprice,Price as TotalPrice,CurrencyCode,IsBreakfast,RoomAdults,RoomChilds from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='Restel'  AND Adults={1} AND Childs={2}  AND CheckInDate='{3}' AND CheckOutDate='{4}' AND IsClose=1 order by ID desc"
                , hotelCode, Adults, Childs, CheckInDate, CheckoutDate);
                    reDt = ControllerFactory.GetController().GetDataTable(sqlDt);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取报价 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return reDt;
        }
        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationality()
        {
            string sql = "select CountryCode AS ID,CountryName AS NAME from RestelCountry ORDER BY CountryName";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestination(string strName)
        {
            string sql = string.Format(@" SELECT (rc.CountryName + ',' + rp.ProvinceName) as name,(rp.CountryCode + ',' + rp.ProvinceCode) as value 
FROM dbo.RestelCountry rc INNER JOIN RestelProvinces rp ON rp.CountryCode=rc.CountryCode WHERE (rc.CountryName + ',' + rp.ProvinceName) like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取取消期限
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelPriceIDs"></param>
        /// <returns></returns>
        public string GetCancellationPolicy(string HotelPriceIDs)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM HotelPrice where id in ({0})", HotelPriceIDs.TrimEnd(','));
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string hotelCode = string.Empty;
                string strlin = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    hotelCode = dt.Rows[0]["HotelID"].ToString();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strlin += dt.Rows[i]["ReferenceClient"];
                    }
                }
                string Peticion = string.Format(@"<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_algGastosCanc.dtd'>
<peticion><tipo>144</tipo><nombre></nombre><agencia />
<parametros><datos_reserva><hotel>{0}</hotel>{1}</datos_reserva></parametros></peticion>",
hotelCode, strlin);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio： Service 144 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel获取取消期限Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelCancellationResp.respuesta objResp = Common.Common.DESerializer<RestelCancellationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        string strMes = string.Empty;
                        for (int i = 0; i < objResp.parametros.politicaCanc.Count; i++)
                        {
                            if (objResp.parametros.politicaCanc[i].dias_antelacion == "998" || objResp.parametros.politicaCanc[i].dias_antelacion == "999")
                            {
                                strMes = "预订后不可以取消";
                            }
                            else
                            {
                                strMes += objResp.parametros.politicaCanc[i].entra_en_gastos + "," + objResp.parametros.politicaCanc[i].fecha + ";" + "dias_antelacion:" + objResp.parametros.politicaCanc[i].dias_antelacion + ";";
                            }
                        }
                        resp = new Respone() { code = "00", mes = strMes };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel获取取消期限 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel获取取消期限 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 预订Leg_Status Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认)  99表示失败
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelPriceIDs"></param>
        /// <returns>ServiceName保存接口的名称</returns>
        public string Reserva(string HotelPriceIDs)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM HotelPrice where id in ({0})", HotelPriceIDs.TrimEnd(','));
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string hotelCode = string.Empty;
                string strlin = string.Empty;
                string ocupantes = string.Empty;
                string guid = Common.Common.GuidToLongID();
                string Leg_Status=string.Empty;
                if (dt.Rows.Count > 0)
                {
                    hotelCode = dt.Rows[0]["HotelID"].ToString();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strlin +=  dt.Rows[i]["ReferenceClient"];
                    }
                    ocupantes = "San#Zhang#.#21#@Si#Li#.#21#@";
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_reserva_3.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>202</tipo><parametros><codigo_hotel>{0}</codigo_hotel><nombre_cliente>{1}</nombre_cliente>
<observaciones></observaciones><num_mensaje /><num_expediente>{2}</num_expediente><forma_pago>25</forma_pago><tipo_targeta></tipo_targeta>
<num_targeta></num_targeta><cvv_targeta></cvv_targeta><mes_expiracion_targeta></mes_expiracion_targeta><ano_expiracion_targeta></ano_expiracion_targeta>
<titular_targeta></titular_targeta><ocupantes>{3}</ocupantes><res>{4}</res></parametros></peticion>",
    hotelCode, "San Zhang",guid, ocupantes, strlin);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        //result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio： Service 202 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                        LogHelper.DoOperateLog(string.Format("Err：Restel预订Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    RestelReservaResp.respuesta objResp = Common.Common.DESerializer<RestelReservaResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.parametros.error == null)
                        {
                            string strMes = string.Empty;
                            string StatusName = string.Empty;
                            Booking booking = new Booking();
                            //保存订单到数据库
                            booking.BookingCode = objResp.parametros.n_localizador;
                            booking.AgentBookingReference = guid;
                            booking.ClientCurrencyCode = objResp.parametros.divisa_total;
                            booking.ClientPartnerAmount = objResp.parametros.importe_total_reserva.AsTargetType<decimal>(0);
                            booking.HotelId = dt.Rows[0]["HotelID"].ToString();
                            booking.HotelNo = "";
                            booking.Platform = "Restel";
                            booking.StatusCode = objResp.parametros.estado;
                            if (objResp.parametros.estado == "00")
                            {
                                StatusName = "预订成功";
                                Leg_Status = "11";
                            }
                            else
                            {
                                StatusName = "预订失败";
                                Leg_Status = "99";
                            }
                            booking.StatusName = StatusName;
                            booking.ServiceID = 0;
                            booking.ServiceName = "Reserva";
                            booking.LegID = 0;
                            booking.Leg_Status = Leg_Status;
                            booking.CreatTime = DateTime.Now;
                            booking.UpdateTime = DateTime.Now;
                            booking.EntityState = e_EntityState.Added;
                            con.Save(booking);
                            resp = new Respone() { code = "00", mes = StatusName };
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = objResp.parametros.error };
                            LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        LogHelper.DoOperateLog(string.Format("Studio：Restel预订 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 预订确认 或 取消预订 Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认) 99表示失败
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        ///  <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="ReservationType">AE预订确认  或  AI预订取消</param>
        /// <returns>localizador_corto预订短号保存到HotelNo，ServiceName保存接口的名称，ServiceID保存预订确认长号</returns>
        public string ReservaConfirmation(string BookingNumber, string ReservationType)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string ServiceName = string.Empty;
                string StatusName = string.Empty;
                string Leg_Status = string.Empty;
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_reserva.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>3</tipo><parametros><localizador>{0}</localizador><accion>{1}</accion></parametros></peticion>",
BookingNumber, ReservationType);
                if (ReservationType == "AE")
                {
                    ServiceName = "ReservationConfirmation";
                }
                else if (ReservationType == "AI")
                {
                    ServiceName = "ReservationAnnulment";
                }
                else
                {
                    ServiceName = "";
                }
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    //result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio： Service 3 ({0}) ,请求数据(Req) :{1} , 返回数据(Resp) ：{2} , Time:{3} ",ReservationType, Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelReservaConfirmationResp.respuesta objResp = Common.Common.DESerializer<RestelReservaConfirmationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        if (objResp.parametros.estado == "00" && ReservationType == "AE")
                        {
                            StatusName = "预订已确认";
                            Leg_Status = "21";
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',ServiceID='{2}',BookingConfirmationID='{3}',StatusName='{4}',Leg_Status='{5}' where BookingCode='{6}'  and Platform='Restel' ",
                            objResp.parametros.estado, ServiceName, objResp.parametros.localizador, objResp.parametros.localizador_corto, StatusName, Leg_Status, BookingNumber);
                            con.Update(strUpdateSql);
                        }
                        else if (objResp.parametros.estado == "00" && ReservationType == "AI")
                        {
                            StatusName = "预订已取消";
                            Leg_Status = "10";
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',ServiceID='{2}',BookingConfirmationID='{3}',StatusName='{4}',Leg_Status='{5}' where BookingCode='{6}'  and Platform='Restel' ",
                            objResp.parametros.estado, ServiceName, objResp.parametros.localizador, objResp.parametros.localizador_corto, StatusName, Leg_Status, BookingNumber);
                            con.Update(strUpdateSql);
                        }
                        else
                        {
                            StatusName = "";
                        }
                        resp = new Respone() { code = "00", mes = StatusName };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                        LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 取消订单  Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认) 99表示失败
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="BookingNumberCorto">订单短号</param>
        /// <returns></returns>
        public string ReservaCancellation(string BookingNumber)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM Booking where BookingCode = '{0}'", BookingNumber);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string ServiceName = "ReservaCancellation";
                    string StatusName = string.Empty;
                    string Leg_Status = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_cancelacion.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>401</tipo>
<parametros><localizador_largo>{0}</localizador_largo><localizador_corto>{1}</localizador_corto></parametros></peticion>",
    BookingNumber, dt.Rows[0]["BookingConfirmationID"]);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio： Service 401 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                        LogHelper.DoOperateLog(string.Format("Err：Restel取消订单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    RestelReservaCancellationResp.respuesta objResp = Common.Common.DESerializer<RestelReservaCancellationResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.parametros.error == null)
                        {
                            if (objResp.parametros.estado == "00")
                            {
                                StatusName = "已取消订单";
                                Leg_Status = "20";
                                string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',StatusName='{2}',Leg_Status='{3}' where BookingCode='{4}'  and Platform='Restel' ",
                                objResp.parametros.estado, ServiceName, StatusName,Leg_Status, BookingNumber);
                                con.Update(strUpdateSql);
                            }
                            else
                            {
                                StatusName = "取消订单失败";
                            }
                            resp = new Respone() { code = "00", mes = StatusName };
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = objResp.parametros.error };
                            LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Err");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "无该订单号" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", "无该订单号", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查看订单
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string ReservationInformation(string BookingNumber)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string ServiceName = "ReservaCancellation";
                string StatusName = string.Empty;
                string ResultStatus = string.Empty;
                string Leg_Status = string.Empty;
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_inforeserva.dtd'>
<peticion><tipo>11</tipo><nombre></nombre><agencia></agencia><parametros><Localizador>{1}</Localizador>
<Afiliacion>HA</Afiliacion></parametros></peticion>",
 codusu,BookingNumber);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio： Service 11 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RetelReservationInformationResp.respuesta objResp = Common.Common.DESerializer<RetelReservationInformationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.RESERVA.error == null)
                    {
                        if (objResp.RESERVA.Status == "00")
                        {
                            ResultStatus = "查询成功";
                            if(objResp.RESERVA.Estado=="B")
                            {
                                StatusName = "已取消订单";
                                Leg_Status = "20";
                            }
                            else if (objResp.RESERVA.Estado == "C")
                            {
                                StatusName = "预订已确认";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "F")
                            {
                                StatusName = "开具发票";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "N")
                            {
                                StatusName = "正在进行中";
                                Leg_Status = "11";
                            }
                            string strUpdateSql = string.Format("update Booking set Leg_Status='{0}',UpdateTime=GETDATE() where BookingCode='{1}'  and Platform='Restel' ",
                            Leg_Status, BookingNumber);
                            con.Update(strUpdateSql);
                        }
                        else
                        {
                            ResultStatus = "查询失败";
                        }
                        resp = new Respone() { code = "00", mes = ResultStatus };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.RESERVA.error };
                        LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", objResp.RESERVA.error, DateTime.Now.ToString()), "Restel接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查看订单
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string RestlLastReservations(string seachMode, string BookingNumber, string searchDate, string searchHotelName, string searchCustomerName)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string ServiceName = "ReservaCancellation";
                string StatusName = string.Empty;
                string ResultStatus = string.Empty;
                string Leg_Status = string.Empty;
                string dia=string.Empty;
                string mes=string.Empty;
                string ano=string.Empty;
                string hotelName = string.Empty;
                string CustomerName = string.Empty;
                string BookingCode = string.Empty;
                if (seachMode == "4")
                {
                    string[] ArrayDate = searchDate.Split('-');
                    if (ArrayDate != null)
                    {
                        dia = ArrayDate[2];
                        mes = ArrayDate[1];
                        ano = ArrayDate[0];
                    }
                }
                else if(seachMode=="3")
                {
                    hotelName = searchHotelName;
                }
                else if (seachMode == "1")
                {
                    CustomerName = searchCustomerName;
                }
                else if (seachMode == "2")
                {
                    BookingCode = BookingNumber;
                }
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_ultimasreservas.dtd'>
<peticion><tipo>8</tipo><nombre></nombre> <agencia></agencia><parametros><selector>{0}</selector><dia>{1}</dia>
<mes>{2}</mes><ano>{3}</ano> <usuario>{4}</usuario><hotel>{5}</hotel><cliente>{6}</cliente><localizador>{7}</localizador></parametros>
</peticion>",
 seachMode, dia, mes, ano, codusu, searchHotelName, CustomerName, BookingCode);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio： Service 8 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单Post数据（LastReservations） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RetelReservationInformationResp.respuesta objResp = Common.Common.DESerializer<RetelReservationInformationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.RESERVA.error == null)
                    {
                        if (objResp.RESERVA.Status == "00")
                        {
                            ResultStatus = "查询成功";
                            if (objResp.RESERVA.Estado == "B")
                            {
                                StatusName = "已取消订单";
                                Leg_Status = "20";
                            }
                            else if (objResp.RESERVA.Estado == "C")
                            {
                                StatusName = "预订已确认";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "F")
                            {
                                StatusName = "开具发票";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "N")
                            {
                                StatusName = "正在进行中";
                                Leg_Status = "11";
                            }
                        }
                        else
                        {
                            ResultStatus = "查询失败";
                        }
                        string strUpdateSql = string.Format("update Booking set Leg_Status='{0}',UpdateTime=GETDATE() where BookingCode='{1}'  and Platform='Restel' ",
                            Leg_Status, BookingNumber);
                        con.Update(strUpdateSql);
                        resp = new Respone() { code = "00", mes = ResultStatus };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.RESERVA.error };
                        LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", objResp.RESERVA.error, DateTime.Now.ToString()), "Restel接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查看预订后取消生成的费用
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber"></param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string GetCancellationFees(string BookingNumber)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><peticion><tipo>142</tipo><nombre></nombre><agencia></agencia>
<parametros><usuario>{0}</usuario> <localizador>{1}</localizador> <idioma>2</idioma></parametros></peticion>", codusu, BookingNumber);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio： Service 2 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看预订后取消生成的费用Post数据（LastReservations） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelCancellationFeesResp.respuesta objResp = Common.Common.DESerializer<RestelCancellationFeesResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        string strMes = string.Empty;
                        for (int i = 0; i < objResp.parametros.politicaCanc.Count; i++)
                        {
                            string days = objResp.parametros.politicaCanc[i].dias_antelacion.AsTargetType<string>("0");
                            string incurrs = objResp.parametros.politicaCanc[i].horas_antelacion.AsTargetType<string>("0");
                            string nights = objResp.parametros.politicaCanc[i].noches_gasto.AsTargetType<string>("0");
                            string amount = objResp.parametros.politicaCanc[i].estCom_gasto.AsTargetType<string>("0");

                            strMes += "预订前需要支付取消费用的天数:" + days + "," + "预订中取消费用的一天中的小时:" + incurrs + "," + "将作为取消费用收取的晚数:" + nights + "," + "将作为取消费用收取的预订总金额的百分比:" + amount + ";";
                        }
                        resp = new Respone() { code = "00", mes = strMes };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看预订后取消生成的费用 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查看预订后取消生成的费用 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查询酒店评论
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelCode"></param>
        /// <returns></returns>
        public string GetHotelRemarks(string HotelCode,string CheckInDate, string CheckoutDate)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='iso-8859-1'?> <peticion> <tipo>24</tipo> 
<parametros> <codigo_cobol>{0}</codigo_cobol> <entrada>{1}</entrada> <salida>{2}</salida> </parametros> </peticion>", HotelCode, CheckInDate, CheckoutDate);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio： Service 24 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查询酒店评论Post数据（LastReservations） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelHotelRemarksResp.respuesta objResp = Common.Common.DESerializer<RestelHotelRemarksResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        if (objResp.parametros.hotel.observaciones.num.AsTargetType<int>(0) == 0)
                        {
                            resp = new Respone() { code = "00", mes = "没数据" };
                        }
                        else
                        {
                            string strMes = objResp.parametros.hotel.observaciones.observacion.obs_texto;
                            resp = new Respone() { code = "00", mes = strMes };
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查询酒店评论 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查询酒店评论 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }


        /// <summary>
        /// 获取报价对外接口(提供外部调用接口)
        /// </summary>
        /// <returns></returns>
        public string GetRestelRoomTypeOutPrice(string hotelCode, string CheckInDate, string CheckoutDate, IntOccupancyInfo occupancy, int RoomCount, string rateplanId)
        {
            int adults = occupancy.adults;
            int children = occupancy.children;
            string strChildAges = occupancy.childAges ?? "";
            string CheckIn = Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd");
            string Checkout = Convert.ToDateTime(CheckoutDate).ToString("yyyy-MM-dd");
            string StrKey = hotelCode + "%" + CheckIn + "%" + Checkout + "%" + adults + "%" + children + "%" + strChildAges;

            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            DataTable dt = new DataTable();
            if (RoomCount > 4)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            }
            string result = string.Empty;
            int numhab1 = RoomCount;
            int numhab2 = 0;
            int numhab3 = 0;
            //房型类型  成人数-儿童数  没有为0
            string paxes1 = "0";
            string paxes2 = "0";
            string paxes3 = "0";
            //岁数用逗号分开
            string edades1 = "0";
            string edades2 = "0";
            string edades3 = "0";
            string childAges = occupancy.childAges.AsTargetType<string>("0");
            if (strChildAges == null || strChildAges == "")
            {
                strChildAges = "";
                childAges = "0";
            }
            paxes1 = adults + "-" + children;
            edades1 = childAges;
            string room = string.Format(@"<numhab1>{0}</numhab1><paxes1>{1}</paxes1><edades1>{2}</edades1><numhab2>{3}</numhab2><paxes2>{4}</paxes2><edades2>{5}</edades2>
<numhab3>{6}</numhab3><paxes3>{7}</paxes3><edades3>{8}</edades3>", numhab1, paxes1, edades1, numhab2, paxes2, edades2, numhab3, paxes3, edades3);
            try
            {
                int IsContain = 0;//报价是否存在这条记录
                string sqlWhere = "1=0";
                if(rateplanId != null && rateplanId.Trim() != "")
                {
                    sqlWhere = "1=1";
                }
                /*string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='Restel' AND Adults={1} AND Childs={2}
and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' AND IsClose=1 and RoomCount={6} AND DATEDIFF( MINUTE, UpdateTime, GETDATE()) < {7} order by ID;
SELECT rh.ProvinceCode,rh.CountryCode FROM dbo.RestelHotel rh  WHERE rh.HotelID='{0}';
select * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' and RateplanId='{8}' AND Platform='Restel' and {9}"
               , hotelCode, adults, children, strChildAges, Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(CheckoutDate), RoomCount, RestelCacheTime, rateplanId, sqlWhere);*/
                string sql = string.Format(@"SELECT rh.ProvinceCode,rh.CountryCode FROM dbo.RestelHotel rh  WHERE rh.HotelID='{0}';
select * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' and RateplanId='{8}' AND Platform='Restel' and {9}"
               , hotelCode, adults, children, strChildAges, Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(CheckoutDate), RoomCount, RestelCacheTime, rateplanId, sqlWhere);
                DataSet ds = con.GetDataSet(sql);
                //dt = ds.Tables[0];
                DataTable dtHotel = ds.Tables[0];
                DataTable dtHp = ds.Tables[1];
                if ( dtHotel.Rows.Count > 0 || (rateplanId != null && rateplanId != "" && dtHotel.Rows.Count > 0))
                {
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd'><peticion><tipo>110</tipo><nombre></nombre>
<agencia></agencia><parametros><hotel>{0}</hotel><pais>{1}</pais><provincia>{2}</provincia><poblacion></poblacion><categoria>0</categoria>
<radio>9</radio><fechaentrada>{3}</fechaentrada><fechasalida>{4}</fechasalida><marca></marca><afiliacion>{5}</afiliacion>
<usuario>{6}</usuario>{7}<idioma>2</idioma><duplicidad>1</duplicidad><comprimido>2</comprimido><informacion_hotel>0</informacion_hotel></parametros></peticion>",
    hotelCode + "#", dtHotel.Rows[0]["CountryCode"], dtHotel.Rows[0]["ProvinceCode"], CheckInDate, CheckoutDate, afiliacio, codusu, room);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio：报价(Service 110) ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} ,RateplanId:{2}, Time:{3} ", Peticion, result,rateplanId, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err：Restel从接口获取报价Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    RestelHotelQuoteResp.respuesta objResp = Common.Common.DESerializer<RestelHotelQuoteResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.param.error == null)
                        {
                            if (objResp.param.hotls.num.AsTargetType<int>(0) == 0)
                            {
                                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                            }
                            else
                            {
                                HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                                for (int i = 0; i < objResp.param.hotls.hot.Count; i++)
                                {
                                    for (int j = 0; j < objResp.param.hotls.hot[i].res.pax.Count; j++)
                                    {
                                        for (int x = 0; x < objResp.param.hotls.hot[i].res.pax[j].hab.Count; x++)
                                        {
                                            for (int y = 0; y < objResp.param.hotls.hot[i].res.pax[j].hab[x].reg.Count; y++)
                                            {
                                                if (objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].esr == "OK")
                                                {
                                                    string cGuid = Common.Common.GuidToLongID();
                                                    string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                                    string ClientCurrencyCode = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].div;
                                                    //如果有显示pvp价格的时候，贵司售价不能低于此价格
                                                    decimal ClientPrice = (objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].pvp ?? "").AsTargetType<decimal>(0);//指导价，不一定有值
                                                    decimal BasePrice = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].prr.AsTargetType<decimal>(0);//底价
                                                    string BedTypeCode = objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim();
                                                    string toRMBrate = USDtoRMBrate;
                                                    
                                                    //售价
                                                    if (ClientPrice == 0)
                                                    {
                                                        ClientPrice = BasePrice;
                                                    }
                                                    decimal RMBprice = ClientPrice * toRMBrate.AsTargetType<decimal>(0);
                                                    decimal RMBbasePrice = BasePrice * toRMBrate.AsTargetType<decimal>(0);
                                                    string lin = string.Empty;
                                                    string strLin = string.Empty;//截取组成房型
                                                    for (int p = 0; p < objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin.Count; p++)
                                                    {
                                                        string[] ArrLin = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin[p].TrimEnd('#').Split('#');
                                                        strLin += objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin[p].Replace("#" + ArrLin[1] + "#", "").Replace("#" + ArrLin[3] + "#", "").Replace("#" + ArrLin[13] + "#", "");
                                                        lin += "<lin>" + objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin[p] + "</lin>";
                                                    }
                                                    if (rateplanId != null && rateplanId.Trim() != "")
                                                    {
                                                        string[] ArrDtLin = dtHp.Rows[0]["ReferenceClient"].ToString().Replace("<lin>", "").Replace("</lin>", ";").TrimEnd(';').Split(';');
                                                        string strDtLin = string.Empty;
                                                        for (int w = 0; w < ArrDtLin.Length; w++)
                                                        {
                                                            string[] ArrLin = ArrDtLin[w].Split('#');
                                                            strDtLin += ArrDtLin[w].Replace("#" + ArrLin[1] + "#", "").Replace("#" + ArrLin[3] + "#", "").Replace("#" + ArrLin[13] + "#", "");
                                                        }
                                                        //若之前报价存在这条记录，再次检查价格时匹配
                                                        if (strLin == strDtLin)
                                                        {
                                                            string updateHotelPriceSql = string.Format(@"update HotelPrice  set ReferenceClient='{0}',UpdateTime=GETDATE(),
    RMBprice={1},Price={2},BasePrice={3},RMBbasePrice={4},CurrencyCode='{5}',BedTypeCode='{6}',RoomCount={7} where RateplanId='{8}' AND Platform='Restel'",
lin, RMBprice, ClientPrice, BasePrice, RMBbasePrice,ClientCurrencyCode, BedTypeCode, RoomCount, rateplanId);
                                                            con.Update(updateHotelPriceSql);
                                                            IsContain = 1;
                                                            break;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        HotelPrice obj = new HotelPrice();
                                                        obj.HotelID = objResp.param.hotls.hot[i].cod;
                                                        obj.HotelNo = objResp.param.hotls.hot[i].res.pax[j].hab[x].cod;
                                                        obj.Platform = "Restel";
                                                        obj.ClassName = objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim();
                                                        obj.RoomTypeID = Common.Common.Md5Encrypt32(objResp.param.hotls.hot[i].res.pax[j].hab[x].desc.Trim().ToLower());//房型id
                                                        obj.RMBprice = RMBprice;
                                                        obj.Price = ClientPrice;
                                                        obj.BasePrice = BasePrice;
                                                        obj.RMBbasePrice = RMBbasePrice;
                                                        obj.CurrencyCode = ClientCurrencyCode;
                                                        string Lodging = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].cod;
                                                        if (Lodging == "RO" || Lodging == "OB" || Lodging == "SA")
                                                        {
                                                            obj.IsBreakfast = 0;
                                                        }
                                                        else
                                                        {
                                                            obj.IsBreakfast = 1;
                                                        }
                                                        obj.ReferenceClient = lin;
                                                        string Adults_Childs = objResp.param.hotls.hot[i].res.pax[j].cod;
                                                        int RoomAdults = Adults_Childs.Split('-')[0].AsTargetType<int>(0);
                                                        int RoomChilds = Adults_Childs.Split('-')[1].AsTargetType<int>(0);
                                                        obj.Adults = adults;
                                                        obj.Childs = children;
                                                        obj.ChildsAge = strChildAges;
                                                        obj.RoomAdults = RoomAdults;
                                                        obj.RoomChilds = RoomChilds;
                                                        obj.RoomCount = RoomCount;
                                                        //obj.ChildsAge = objResp.param.hotls.hot[i].res.pax[j].cod;
                                                        obj.CheckInDate = CheckInDate.AsTargetType<DateTime>(DateTime.MinValue);
                                                        obj.CheckOutDate = CheckoutDate.AsTargetType<DateTime>(DateTime.MinValue);
                                                        obj.CGuID = cGuid;
                                                        obj.RateplanId = GuidRateplanId;
                                                        obj.BedTypeCode = BedTypeCode;
                                                        obj.PriceBreakdownDate = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin.Count.AsTargetType<string>("");
                                                        obj.IsClose = 1;
                                                        obj.CreateTime = DateTime.Now;
                                                        obj.UpdateTime = DateTime.Now;
                                                        obj.EntityState = e_EntityState.Added;
                                                        objHotelPriceCol.Add(obj);
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                //复查价格
                                if (rateplanId != null && rateplanId.Trim() != "")
                                {
                                    //是之前有查过报价
                                    if (IsContain == 1)
                                    {
                                        string sqlIsRecord = string.Format(@"select * from HotelPrice WITH(NOLOCK) WHERE RateplanId='{0}' AND Platform='Restel' AND hotelID='{1}' ", rateplanId,hotelCode);
                                        dt = ControllerFactory.GetController().GetDataTable(sqlIsRecord);
                                    }
                                    else
                                    {
                                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                                    }
                                }
                                else
                                {
                                    string updateHotelPriceSql = string.Format(@"update HotelPrice  set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' AND Platform='Restel'  AND Adults={1} AND Childs={2} and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6}",
    hotelCode, adults, children, strChildAges, Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(CheckoutDate).ToString("yyyy-MM-dd"), RoomCount);
                                    con.Update(updateHotelPriceSql);
                                    con.Save(objHotelPriceCol);
                                    string sqlDt = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='Restel'  AND Adults={1} AND Childs={2}   and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6} AND IsClose=1 order by ID"
                    , hotelCode, adults, children, strChildAges, Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(CheckoutDate).ToString("yyyy-MM-dd"), RoomCount);
                                    dt = ControllerFactory.GetController().GetDataTable(sqlDt);
                                }
                            }
                        }
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            RoomOutPrice.Room temp = new RoomOutPrice.Room();
                            temp.ProductSource = "Restel"; //产品来源 
                            temp.MroomName = temp.XRoomName = temp.RoomName = dt.Rows[j]["ClassName"].AsTargetType<string>("");// 房型名称
                            temp.MroomId = temp.RoomTypeId = dt.Rows[j]["RoomTypeID"].AsTargetType<string>("");//房型ID(由于长度要求，取自己生成后的guid，要是用到下单时转为)

                            //当前请求的房间人数
                            temp.Adults = adults;
                            temp.Children = children;
                            temp.ChildAges = childAges.Replace(",", "|");
                            //最大入住人数
                            temp.MaxPerson = adults;

                            temp.RatePlan = new List<RoomOutPrice.RatePlan>();
                            RoomOutPrice.RatePlan thisRatePlan = new RoomOutPrice.RatePlan();
                            string RatePlanName = "有早";
                            thisRatePlan.PayMent = "y";//支付方式。目前全部前台支付
                            int dtIsBreakfast = dt.Rows[j]["IsBreakfast"].AsTargetType<int>(0);
                            if (dtIsBreakfast == 0)
                            {
                                RatePlanName = "无早";
                            }
                            thisRatePlan.RatePlanName = RatePlanName;  //价格计划名称
                            thisRatePlan.RoomName = RatePlanName;
                            thisRatePlan.XRatePlanName = RatePlanName;
                            thisRatePlan.RatePlanId = dt.Rows[j]["RateplanId"].AsTargetType<string>("");  //价格计划ID

                            thisRatePlan.RoomTypeId = dt.Rows[j]["RoomTypeID"].AsTargetType<string>("");
                            temp.XRoomId = temp.RoomId = thisRatePlan.RoomId = string.Format("{0}_{1}", dt.Rows[j]["RoomTypeID"].AsTargetType<string>(""), dt.Rows[j]["RateplanId"].AsTargetType<string>("")); //房型Id_价格计划ID
                            //temp.XRoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>(""));
                            //床型
                            common.HotelBedType hbt = new common.HotelBedType();
                            thisRatePlan.BedType = hbt.HotelBedTypeToName(dt.Rows[j]["BedTypeCode"].AsTargetType<string>(""));

                            //售价
                            decimal price = dt.Rows[j]["RMBbasePrice"].AsTargetType<decimal>(0)/RoomCount;//底总价，供应商收取费用时的价格
                            decimal RMBprice = dt.Rows[j]["RMBprice"].AsTargetType<decimal>(0)/RoomCount;//指导总价，不一定有值，
                            //string[] ArrayPriceBreakdownDate = dt.Rows[j]["PriceBreakdownDate"].AsTargetType<string>("").Split(',');
                            //总的底价 
                            double sMoney = price.AsTargetType<double>(0);
                            bool available = true; // 指示入离日期所有天是否可订 有一天不可订为false
                            List<RoomOutPrice.Rate> rates = new List<RoomOutPrice.Rate>();
                            int days = (Convert.ToDateTime(CheckoutDate) - Convert.ToDateTime(CheckInDate)).Days;
                            for (int x = 0; x < dt.Rows[j]["PriceBreakdownDate"].AsTargetType<int>(0); x++)
                            {
                                RoomOutPrice.Rate rate = new RoomOutPrice.Rate();
                                rate.Date = Convert.ToDateTime(CheckInDate).AddDays(x);
                                rate.Available = true;  //某天是否可订


                                rate.HotelID = (price / days).AsTargetType<string>("");  //底价,供应商收取费用时的价格

                                rate.MemberRate = (RMBprice / days);//指导总价，不一定有值
                                rate.RetailRate = (RMBprice / days);//指导总价，不一定有值
                                rates.Add(rate);
                            }


                            //补上没有的日期
                            if (days > rates.Count)
                            {
                                available = false;
                                List<RoomOutPrice.Rate> tempR = new List<RoomOutPrice.Rate>();
                                for (int y = 0; y < days; y++)
                                {
                                    DateTime date = Convert.ToDateTime(CheckInDate).AddDays(y).Date;
                                    var one = rates.Find(c => c.Date.Date == date);
                                    if (one != null)
                                    {
                                        tempR.Add(one);
                                    }
                                    else
                                    {
                                        RoomOutPrice.Rate newOne = new RoomOutPrice.Rate() { Available = false, InvStatusCode = "0", MemberRate = 0, Date = date };
                                        tempR.Add(newOne);
                                    }
                                }
                                rates = tempR;
                            }

                            thisRatePlan.Rates = rates;
                            thisRatePlan.Available = available;
                            thisRatePlan.SMoney = Convert.ToDecimal(sMoney);

                            thisRatePlan.CurrencyCode = "USD" + ":" + USDtoRMBrate;

                            decimal avgprice = 0;//均价
                            if (rates.Count > 0)
                            {
                                avgprice = rates.Average(c => c.MemberRate);
                                thisRatePlan.AveragePrice = avgprice.ToString("f2");
                            }

                            temp.CurrentAlloment = thisRatePlan.CurrentAlloment = 15; //库存

                            //设置早餐数量
                            int breakfast = -1;
                            thisRatePlan.Breakfast = breakfast + "份早餐";
                            if (dtIsBreakfast == 0)
                            {
                                thisRatePlan.Breakfast = 0 + "份早餐";
                            }
                            else
                            {
                                thisRatePlan.Breakfast = adults + "份早餐";
                            }
                            string zaocan = thisRatePlan.Breakfast.Replace("份早餐", "");
                            if (!zaocan.Equals("0"))
                            {
                                switch (zaocan)
                                {
                                    case "1": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含单早)"; temp.RoomName = temp.RoomName + "(含单早)"; break;
                                    case "2": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含双早)"; temp.RoomName = temp.RoomName + "(含双早)"; break;
                                    case "3": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含三早)"; temp.RoomName = temp.RoomName + "(含三早)"; break;
                                    case "4": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含四早)"; temp.RoomName = temp.RoomName + "(含四早)"; break;
                                }
                            }


                            temp.RoomPrice = Convert.ToInt32(avgprice);
                            temp.BedType = thisRatePlan.BedType;
                            temp.RatePlan.Add(thisRatePlan);
                            rooms.Add(temp);
                        }
                    }
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取报价 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            //LogHelper.DoOperateLog(string.Format("Studio：报价(Service 110) ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} ,RateplanId:{2}, Time:{3} ", Peticion, result, rateplanId, DateTime.Now.ToString()), "Restel接口");
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }
        /// <summary>
        /// 预订确认 或 取消预订 Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认) 99表示失败 预订确认前先预订
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        ///  <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="ReservationType">AE预订确认  或  AI预订取消</param>
        /// <returns>localizador_corto预订短号保存到BookingConfirmationID，ServiceName保存接口的名称，ServiceID保存预订确认长号</returns>
        public string Create_Order(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string BookingNumber = string.Empty;
                string outReservaResult = OutReserva(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum);
                if (outReservaResult.Contains("订单号："))
                {
                    BookingNumber = outReservaResult.Replace("订单号：", "");//订单号
                    string sql = string.Format("SELECT * FROM Booking where BookingCode = '{0}' and HotelID='{1}' and Platform='Restel'", BookingNumber, HotelID);
                    DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                    decimal price = dt.Rows[0]["RMBprice"].AsTargetType<decimal>(0);//预订价格
                    int Adults = dt.Rows[0]["Adults"].AsTargetType<int>(0);
                    int Childs = dt.Rows[0]["Childs"].AsTargetType<int>(0);
                    string ReservationType = "AE";
                    string ServiceName = string.Empty;
                    string StatusName = string.Empty;
                    string Leg_Status = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_reserva.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>3</tipo><parametros><localizador>{0}</localizador><accion>{1}</accion></parametros></peticion>",
    BookingNumber, ReservationType);
                    if (ReservationType == "AE")
                    {
                        ServiceName = "ReservationConfirmation";
                    }
                    else if (ReservationType == "AI")
                    {
                        ServiceName = "ReservationAnnulment";
                    }
                    else
                    {
                        ServiceName = "";
                    }
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio： Restel预订确认Post数据 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        resp = new Respone() { code = "99", mes = "预订失败," + ex.ToString() };
                        LogHelper.DoOperateLog(string.Format("Err：Restel预订确认Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    RestelReservaConfirmationResp.respuesta objResp = Common.Common.DESerializer<RestelReservaConfirmationResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.parametros.error == null)
                        {
                            if (objResp.parametros.estado == "00" && ReservationType == "AE")
                            {
                                StatusName = "确认";
                                Leg_Status = "21";
                                string strUpdateSql = string.Format(@"update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',ServiceID='{2}',BookingConfirmationID='{3}',StatusName='{4}',
Leg_Status='{5}'  where BookingCode='{6}'  and Platform='Restel' and HotelId='{7}' ",
objResp.parametros.estado, ServiceName, objResp.parametros.localizador, objResp.parametros.localizador_corto, StatusName, Leg_Status,BookingNumber, HotelID);
                                con.Update(strUpdateSql);
                                /*Reservation rev = new Reservation();
                                rev.hotel_id = HotelID;
                                rev.aic_reservation_id = objResp.parametros.localizador_corto;
                                rev.partner_reservation_id = BookingNumber;
                                rev.reservation_status="2";
                                rev.hotel_confirmation_number="";
                                rev.hotel_cancellation_number="";
                                rev.special_request="";
                                rev.check_in_date=beginTime;
                                rev.check_out_date=endTime;
                                rev.adult_number = Adults.AsTargetType<string>("");
                                rev.kids_number = Childs.AsTargetType<string>("");
                                rev.total_rooms  = roomNum.AsTargetType<string>("");
                                rev.total_amount  = price.AsTargetType<string>("");
                                rev.currency = "$" + dt.Rows[0]["ClientPartnerAmount"];*/
                                //rev.cancellation_time  = Adults.AsTargetType<string>("");
                                resp = new Respone() { code = "00", orderNum = BookingNumber, orderTotal = price, orderOriginalPrice = "$" + dt.Rows[0]["ClientPartnerAmount"], Status = StatusName, mes = "预订成功" };
                            }
                            else
                            {
                                resp = new Respone() { code = "99", mes = "预订失败" };
                                LogHelper.DoOperateLog(string.Format("Err：Restel预订确认 ,err:{0} , Time:{1} ", "状态不正确", DateTime.Now.ToString()), "Restel接口");
                            }
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = "预订失败" + objResp.parametros.error };
                            LogHelper.DoOperateLog(string.Format("Err：Restel预订确认 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "预订失败" };
                        LogHelper.DoOperateLog(string.Format("Err：Restel预订确认 ,err:{0} ,接口返回值：{1}, Time:{2} ", "无值",result, DateTime.Now.ToString()), "Err");
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Restel预订确认 ,err:{0} , Time:{1} ", outReservaResult, DateTime.Now.ToString()), "Restel接口");
                    resp = new Respone() { code = "99", mes = outReservaResult };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "预订失败," + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel预订确认 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 预订Leg_Status Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认)  99表示失败(相当于复查价格)
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelPriceIDs"></param>
        /// <returns>ServiceName保存接口的名称</returns>
        public string OutReserva(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            string OrderNum = "";
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"SELECT * FROM HotelPrice where HotelID='{0}' and RateplanId='{1}' and Platform='Restel'", HotelID, RateplanId);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string strlin = string.Empty;
                string ocupantes = string.Empty;
                string guid = Common.Common.GuidToLongID();
                string Leg_Status = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    /*if (dt.Rows[0]["RoomCount"].AsTargetType<int>(0) != roomNum)
                    {
                        IntOccupancyInfo occupancy = new IntOccupancyInfo();
                        occupancy.adults = dt.Rows[0]["Adults"].AsTargetType<int>(0);
                        occupancy.childAges = dt.Rows[0]["ChildsAge"].AsTargetType<string>("");
                        occupancy.children = dt.Rows[0]["Childs"].AsTargetType<int>(0);
                        GetRestelRoomTypeOutPrice(HotelID, beginTime, endTime, occupancy, roomNum, RateplanId);
                    }*/
                    //预订前重新查次报价
                    IntOccupancyInfo occupancy = new IntOccupancyInfo();
                    occupancy.adults = dt.Rows[0]["Adults"].AsTargetType<int>(0);
                    occupancy.childAges = dt.Rows[0]["ChildsAge"].AsTargetType<string>("");
                    occupancy.children = dt.Rows[0]["Childs"].AsTargetType<int>(0);
                    GetRestelRoomTypeOutPrice(HotelID, beginTime, endTime, occupancy, roomNum, RateplanId);
                    
                    //拿数据库值与传入值匹配
                    string dtCheckInDate = Convert.ToDateTime(dt.Rows[0]["CheckInDate"]).ToString("yyyy-MM-dd");
                    string dtCheckOutDate = Convert.ToDateTime(dt.Rows[0]["CheckOutDate"]).ToString("yyyy-MM-dd");
                    int dtRoomCount = dt.Rows[0]["RoomCount"].AsTargetType<int>(0);
                    if (dtCheckInDate == beginTime && dtCheckOutDate == endTime && dtRoomCount == roomNum)
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            strlin += dt.Rows[i]["ReferenceClient"];
                        }
                        for (int j = 0; j < customers.Count; j++)
                        {
                            for (int x = 0; x < customers[j].Customers.Count; x++)
                            {
                                int aduiltAge=0;
                                if (customers[j].Customers[x].age == -1)
                                {
                                    aduiltAge = 30;
                                }
                                else
                                {
                                    aduiltAge = customers[j].Customers[x].age;
                                }
                                ocupantes = string.Format(@"{0}#{1}#.#{2}#@", customers[j].Customers[x].firstName, customers[j].Customers[x].lastName, aduiltAge);
                            }      
                        }
                        string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_reserva_3.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>202</tipo><parametros><codigo_hotel>{0}</codigo_hotel><nombre_cliente>{1}</nombre_cliente>
<observaciones></observaciones><num_mensaje /><num_expediente>{2}</num_expediente><forma_pago>25</forma_pago><tipo_targeta></tipo_targeta>
<num_targeta></num_targeta><cvv_targeta></cvv_targeta><mes_expiracion_targeta></mes_expiracion_targeta><ano_expiracion_targeta></ano_expiracion_targeta>
<titular_targeta></titular_targeta><ocupantes>{3}</ocupantes><res>{4}</res></parametros></peticion>",
        HotelID, customers[0].Customers[0].name, guid, ocupantes, strlin);
                        string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                        try
                        {
                            result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                            LogHelper.DoOperateLog(string.Format("Studio： Restel预订 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                        }
                        catch (Exception ex)
                        {
                            LogHelper.DoOperateLog(string.Format("Err：Restel预订Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                            return "预订失败," + ex.ToString();
                        }
                        RestelReservaResp.respuesta objResp = Common.Common.DESerializer<RestelReservaResp.respuesta>(result.Replace("&", ""));
                        if (objResp != null)
                        {
                            if (objResp.parametros.error == null)
                            {
                                if (objResp.parametros.estado == "00")
                                {
                                    decimal ClientPartnerAmount = objResp.parametros.importe_total_reserva.AsTargetType<decimal>(0);
                                    string ClientCurrencyCode = objResp.parametros.divisa_total;
                                    string toRMBrate = USDtoRMBrate;
                                    
                                    //售价
                                    decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                                    int Adults = dt.Rows[0]["Adults"].AsTargetType<int>(0);
                                    int Childs = dt.Rows[0]["Childs"].AsTargetType<int>(0);
                                    Booking booking = new Booking();
                                    //保存订单到数据库
                                    booking.BookingCode = objResp.parametros.n_localizador;
                                    booking.InOrderNum = inOrderNum;
                                    booking.AgentBookingReference = guid;
                                    booking.RMBprice = price;
                                    booking.ClientCurrencyCode = ClientCurrencyCode;
                                    booking.ClientPartnerAmount = ClientPartnerAmount;
                                    booking.HotelId = dt.Rows[0]["HotelID"].ToString();
                                    booking.HotelNo = "";
                                    booking.Platform = "Restel";
                                    booking.StatusCode = objResp.parametros.estado;
                                    booking.StatusName = "确认";
                                    booking.Adults = Adults;
                                    booking.Childs = Childs;
                                    booking.RoomCount = roomNum;
                                    booking.ServiceID = 0;
                                    booking.ServiceName = "Reserva";
                                    booking.LegID = 0;
                                    booking.RatePlanId = RateplanId;
                                    booking.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                                    booking.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                                    booking.Leg_Status = Leg_Status;
                                    booking.CreatTime = DateTime.Now;
                                    booking.UpdateTime = DateTime.Now;
                                    booking.EntityState = e_EntityState.Added;
                                    con.Save(booking);
                                    OrderNum =  "订单号：" + objResp.parametros.n_localizador ;
                                }
                                else
                                {
                                    LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0}  , Time:{1} ", "状态不正确", DateTime.Now.ToString()), "Restel接口");
                                    return "预订失败";
                                }
                            }
                            else
                            {
                                OrderNum = "预订失败," + objResp.parametros.error;
                                LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                            }
                        }
                        else
                        {
                            LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0}  ,接口返回值：{1}, Time:{2} ", "实体类为空",result, DateTime.Now.ToString()), "Err");
                            return "预订失败";
                        }
                    }
                }
                else
                {
                    OrderNum = "预订失败,请重新查询报价";
                    LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0} , Time:{1} ", "预订失败,请重新查询报价,", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return "预订失败," + ex.ToString();
            }
            return OrderNum;
        }
        /// <summary>
        /// 查看订单
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string Get_OrderDetail(string HotelID, string BookingNumber)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM Booking where BookingCode = '{0}' and HotelID='{1}' and Platform='Restel'", BookingNumber, HotelID);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string StatusName = string.Empty;
                    string ResultStatus = string.Empty;
                    string Leg_Status = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_inforeserva.dtd'>
<peticion><tipo>11</tipo><nombre></nombre><agencia></agencia><parametros><Localizador>{1}</Localizador>
<Afiliacion>HA</Afiliacion></parametros></peticion>",
     codusu, BookingNumber);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio： Restel查看订单 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        resp = new Respone() { code = "99", mes = "查看订单失败," + ex.ToString() };
                        LogHelper.DoOperateLog(string.Format("Err：Restel查看订单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    RetelReservationInformationResp.respuesta objResp = Common.Common.DESerializer<RetelReservationInformationResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.RESERVA.error == null)
                        {
                            string reservation_status = string.Empty;
                            if (objResp.RESERVA.Status == "00")
                            {
                                ResultStatus = "查询成功";
                                if (objResp.RESERVA.Estado == "B")
                                {
                                    StatusName = "取消";
                                    Leg_Status = "20";
                                    reservation_status = "4";
                                }
                                else if (objResp.RESERVA.Estado == "C")
                                {
                                    StatusName = "确认";
                                    Leg_Status = "21";
                                    reservation_status = "2";
                                }
                                else if (objResp.RESERVA.Estado == "F")
                                {
                                    //StatusName = "开具发票";
                                    StatusName = "确认";
                                    Leg_Status = "21";
                                    reservation_status = "2";
                                }
                                else if (objResp.RESERVA.Estado == "N")
                                {
                                    StatusName = "正在进行中";
                                    Leg_Status = "11";
                                    reservation_status = "1";
                                }
                                string strUpdateSql = string.Format("update Booking set StatusCode='{0}',Leg_Status='{1}',StatusName='{2}',UpdateTime=GETDATE() where BookingCode='{3}'  and Platform='Restel' and HotelId='{4}' ",
                                objResp.RESERVA.Estado, Leg_Status, StatusName, BookingNumber, HotelID);
                                con.Update(strUpdateSql);
                                decimal ClientPartnerAmount = objResp.RESERVA.PVP.AsTargetType<decimal>(0);
                                string ClientCurrencyCode = string.Empty;
                                if (objResp.RESERVA.Linea != null)
                                {
                                    ClientCurrencyCode = objResp.RESERVA.Linea.Fecha.Divisa;
                                }
                                string toRMBrate = USDtoRMBrate;

                                //售价
                                decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                                string orderOriginalPrice = "$" + ClientPartnerAmount;
                                Reservation rev = new Reservation();
                                rev.hotel_id = HotelID;
                                rev.aic_reservation_id = BookingNumber;//dt.Rows[0]["BookingConfirmationID"].AsTargetType<string>("");
                                rev.partner_reservation_id = dt.Rows[0]["InOrderNum"].AsTargetType<string>("");
                                rev.reservation_status = reservation_status;
                                rev.hotel_confirmation_number = "";
                                rev.hotel_cancellation_number = "";
                                rev.special_request = "";
                                rev.check_in_date = dt.Rows[0]["CheckInDate"].AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                                rev.check_out_date = dt.Rows[0]["CheckOutDate"].AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                                rev.adult_number = dt.Rows[0]["Adults"].AsTargetType<string>("");
                                rev.kids_number = dt.Rows[0]["Childs"].AsTargetType<string>("");
                                rev.total_rooms = dt.Rows[0]["RoomCount"].AsTargetType<string>("");
                                rev.total_amount = price.AsTargetType<string>("");
                                rev.currency = orderOriginalPrice;
                                rev.cancellation_time = dt.Rows[0]["CancellationTime"].AsTargetType<string>("");
                                resp = new Respone() { code = "00", orderNum = BookingNumber, Reservation = rev, orderTotal = price,orderOriginalPrice =orderOriginalPrice, Status = StatusName, mes = "查询成功" };
                            }
                            else
                            {
                                resp = new Respone() { code = "99", mes = "查看订单失败" };
                            }
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = "查看订单失败" + objResp.RESERVA.error };
                            LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", objResp.RESERVA.error, DateTime.Now.ToString()), "Restel接口");
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "查看订单失败" };
                        LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} ,接口返回值：{1}, Time:{2} ", "无值", result, DateTime.Now.ToString()), "Err");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "查看订单失败,无该订单号" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单失败 ,err:{0} , Time:{1} ", "无该订单号", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "查看订单失败," + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 取消订单  Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认) 99表示失败
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="BookingNumberCorto">订单短号</param>
        /// <returns></returns>
        public string Cancel_Order(string HotelID, string BookingNumber)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM Booking where BookingCode = '{0}' and HotelID='{1}' and Platform='Restel'", BookingNumber, HotelID);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string ServiceName = "ReservaCancellation";
                    string StatusName = string.Empty;
                    string Leg_Status = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_cancelacion.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>401</tipo>
<parametros><localizador_largo>{0}</localizador_largo><localizador_corto>{1}</localizador_corto></parametros></peticion>",
    BookingNumber, dt.Rows[0]["BookingConfirmationID"]);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio： Restel取消订单 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        resp = new Respone() { code = "99", mes = "取消失败," + ex.ToString() };
                        LogHelper.DoOperateLog(string.Format("Err：Restel取消订单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    RestelReservaCancellationResp.respuesta objResp = Common.Common.DESerializer<RestelReservaCancellationResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.parametros.error == null)
                        {
                            string code = string.Empty;
                            string reservation_status = string.Empty;
                            string mes = string.Empty;
                            if (objResp.parametros.estado == "00")
                            {
                                StatusName = "取消";
                                mes = "取消";
                                Leg_Status = "20";
                                string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',StatusName='{2}',Leg_Status='{3}',CancellationTime=GETDATE() where BookingCode='{4}'  and Platform='Restel' and HotelID='{5}' ",
                                objResp.parametros.estado, ServiceName, "取消", Leg_Status, BookingNumber, HotelID);
                                con.Update(strUpdateSql);
                                code = "00";
                                reservation_status = "4";
                            }
                            else
                            {
                                StatusName = "取消失败";
                                code = "99";
                                reservation_status = "6";
                                mes = "取消失败;" + objResp.parametros.localizador_baja;
                            }
                            //Reservation rev = new Reservation();
                            //rev.hotel_id = HotelID;
                            //rev.aic_reservation_id = dt.Rows[0]["BookingConfirmationID"].AsTargetType<string>("");
                            //rev.partner_reservation_id = BookingNumber;
                            //rev.reservation_status = reservation_status;
                            //rev.hotel_confirmation_number = "";
                            //rev.hotel_cancellation_number = "";
                            //rev.special_request = "";
                            //rev.check_in_date = dt.Rows[0]["CheckInDate"].AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                            //rev.check_out_date = dt.Rows[0]["CheckOutDate"].AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                            //rev.adult_number = dt.Rows[0]["Adults"].AsTargetType<string>("");
                            //rev.kids_number = dt.Rows[0]["Childs"].AsTargetType<string>("");
                            //rev.total_rooms = dt.Rows[0]["RoomCount"].AsTargetType<string>("");
                            //rev.total_amount = dt.Rows[0]["RMBprice"].AsTargetType<string>("");
                            //rev.currency = "RMB";
                            //rev.cancellation_time = dt.Rows[0]["CancellationTime"].AsTargetType<string>("");
                            //rev.orderOriginalPrice = "$" + dt.Rows[0]["CancellationTime"].AsTargetType<string>("");
                            resp = new Respone() { code = code, orderNum = BookingNumber, Status = StatusName, mes = mes };
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = "取消失败" + objResp.parametros.error };
                            LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败" };
                        LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} ,接口返回值：{1}, Time:{2} ", "无值",result, DateTime.Now.ToString()), "Err");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "取消失败,无该订单号" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", "无该订单号", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "取消订单失败," + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //获取可卖酒店信息
        public void GetSaleableHotel()
        {
            try
            {
                string result = string.Empty;
                string Peticion = string.Format(@"<?xml version='1.0' encoding='iso-8859-1'?> <peticion> <tipo>17</tipo> <nombre></nombre> 
<agencia></agencia> <parametros> </parametros> </peticion>");
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                //LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取可卖酒店信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");

                SaleableHotelResp.respuesta obj = Common.Common.DESerializer<SaleableHotelResp.respuesta>(result.Replace("<br />", "").Replace("<p>", "").Replace("</p>", "").Replace("<b>", "").Replace("</b>", "").Replace("&", "").Replace("</</", "</"));
                RestelSaleableHotelCollection objRestelHotelCol = new RestelSaleableHotelCollection();
                if (obj != null)
                {
                    for (int i = 0; i < obj.parametros.hoteles.hotel.Count; i++)
                    {
                        RestelSaleableHotel objHotel = new RestelSaleableHotel();
                        objHotel.HotelID = obj.parametros.hoteles.hotel[i].hot_codcobol;
                        objHotel.HotelShortID = obj.parametros.hoteles.hotel[i].hot_codigo;
                        //objHotel.HotelName = obj.parametros.hoteles.hotel[i].nombre_h;
                        objHotel.ProvinceCode = obj.parametros.hoteles.hotel[i].codesthot;
                        //objHotel.ProvinceName = obj.parametros.hoteles.hotel[i].provincia_nombre;
                        objHotel.TownCode = obj.parametros.hoteles.hotel[i].codpobhot;
                        //objHotel.Staring = obj.parametros.hoteles.hotel[i].categoria;
                        //objHotel.Longitud = obj.parametros.hoteles.hotel[i].longitud;
                        //objHotel.Latitud = obj.parametros.hoteles.hotel[i].latitud;
                        objHotel.DuplicityCode = obj.parametros.hoteles.hotel[i].hot_coddup;
                        objRestelHotelCol.Add(objHotel);
                    }
                    con.Save(objRestelHotelCol);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取可卖酒店信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }

        //获取补充服务17，补充可卖酒店信息
        public void GetSaleableHotelInformation()
        {
            try
            {
                string sql = string.Format(@"SELECT TOP 2 * from RestelHotel ORDER BY ID desc ");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string hotelID = string.Empty;
                    string result = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><peticion><tipo>15</tipo><nombre></nombre>
<agencia></agencia><parametros><codigo>{0}</codigo><idioma>2</idioma></parametros></peticion>", dt.Rows[i]["HotelID"].ToString().PadLeft(6, '0'));
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取可卖酒店信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    Regex r2 = new Regex("(?<=<fotos>).*?(?=</fotos>)", RegexOptions.IgnoreCase);
                    result = r2.Replace(result, "");
                    SaleableHotelInformation.respuesta obj = Common.Common.DESerializer<SaleableHotelInformation.respuesta>(result.Replace("<br />", "").Replace("<p>", "").Replace("</p>", "").Replace("<b>", "").Replace("</b>", "").Replace("&", "").Replace("</</", "</"));
                    if (obj != null)
                    {
                        if (obj.parametros.error == null)
                        {
                            if (obj.parametros.hotel.codigo_hotel != null && obj.parametros.hotel.codigo_hotel != "")
                            {
                                string HotelName = obj.parametros.hotel.nombre_h.Replace("'", "''");
                                string ProvinceCode = obj.parametros.hotel.codprovincia;
                                string ProvinceName = obj.parametros.hotel.provincia.Replace("'", "''");
                                string TownCode = obj.parametros.hotel.codpoblacion;
                                string TownName = obj.parametros.hotel.poblacion.Replace("'", "''");
                                string Staring = obj.parametros.hotel.categoria;
                                string Address = obj.parametros.hotel.direccion.Replace("'", "''");//.Replace(",","");
                                string Longitud = obj.parametros.hotel.longitud;
                                string Latitud = obj.parametros.hotel.latitud;
                                string Web = obj.parametros.hotel.web.Replace("'", "''");//.Replace(",","");
                                string Telephone = obj.parametros.hotel.telefono.Replace("'", "''");//.Replace(",","");
                                string LogoImage = obj.parametros.hotel.logo_h.Replace("'", "''");//.Replace(",","");
                                string Fotos = string.Empty;
                                if (obj.parametros.hotel.fotos != null)
                                {
                                    for (int j = 0; j < obj.parametros.hotel.fotos.foto.Count; j++)
                                    {
                                        Fotos += obj.parametros.hotel.fotos.foto[j] + ";";
                                    }
                                }
                                //string fotos = obj.parametros.hotel.Fotos.Value.Replace("'", "''");//.Replace(",","");

                                string updateHotelPriceSql = string.Format(@"update RestelHotel set ProvinceCode='{0}',ProvinceName='{1}',
TownCode='{2}',TownName='{3}',Staring='{4}',Address='{5}',Longitud='{6}',Latitud='{7}',Web='{8}',Telephone='{9}',LogoImage='{10}',Fotos='{11}' where HotelID='{12}'",
ProvinceCode, ProvinceName, TownCode, TownName, Staring, Address, Longitud, Latitud,Web,Telephone,LogoImage,Fotos.TrimEnd(';'), dt.Rows[i]["HotelID"]);
                                con.Update(updateHotelPriceSql);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取补充服务17，补充可卖酒店信息 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }

        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string orderOriginalPrice { get; set; }//预订原始总额
            public string Status { get; set; }//状态
            public Reservation Reservation { get; set; }
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
    }
}