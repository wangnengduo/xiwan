﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelTownResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "poblaciones")]
            public poblaciones poblaciones { get; set; }
        }
        public class poblaciones
        {
            [XmlElement(ElementName = "poblacion")]
            public List<poblacion> poblacion { get; set; }
        }
        public class poblacion
        {
            [XmlElement(ElementName = "deppob")]
            public string deppob { get; set; }
            [XmlElement(ElementName = "codpob")]
            public string codpob { get; set; }
            [XmlElement(ElementName = "poblidinom1")]
            public string poblidinom1 { get; set; }
            [XmlElement(ElementName = "poblidinom2")]
            public string poblidinom2 { get; set; }
            [XmlElement(ElementName = "poblidinom3")]
            public string poblidinom3 { get; set; }
            [XmlElement(ElementName = "poblidinom4")]
            public string poblidinom4 { get; set; }
            [XmlElement(ElementName = "poblidinom5")]
            public string poblidinom5 { get; set; }
            [XmlElement(ElementName = "poblidinom6")]
            public string poblidinom6 { get; set; }
        }
    }
}