﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelHotelRemarksResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "error")]
            public string error { get; set; }
            [XmlElement(ElementName = "hotel")]
            public hotel hotel { get; set; }
            [XmlElement(ElementName = "id")]
            public string id { get; set; }
        }
        public class hotel
        {
            [XmlElement(ElementName = "codigo")]
            public string codigo { get; set; }
            [XmlElement(ElementName = "codigo_cobol")]
            public string codigo_cobol { get; set; }
            [XmlElement(ElementName = "observaciones")]
            public observaciones observaciones { get; set; }
        }
        public class observaciones
        {
            [XmlAttribute("num")]
            public string num { get; set; }
            [XmlElement(ElementName = "observacion")]
            public observacion observacion { get; set; }
        }
        public class observacion
        {
            [XmlElement(ElementName = "obs_texto")]
            public string obs_texto { get; set; }
            [XmlElement(ElementName = "obs_desde")]
            public string obs_desde { get; set; }
            [XmlElement(ElementName = "obs_hasta")]
            public string obs_hasta { get; set; }
        }
    }
}