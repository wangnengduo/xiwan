﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RestelReservaCancellationResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "error")]
            public string error { get; set; }
            [XmlElement(ElementName = "estado")]
            public string estado { get; set; }
            [XmlElement(ElementName = "localizador")]
            public string localizador { get; set; }
            [XmlElement(ElementName = "localizador_baja")]
            public string localizador_baja { get; set; }
        }

    }
}