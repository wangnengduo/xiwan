﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class SaleableHotelResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "parametros")]
            public parametros parametros { get; set; }
        }
        public class parametros
        {
            [XmlElement(ElementName = "hoteles")]
            public hoteles hoteles { get; set; }
        }
        public class hoteles
        {
            [XmlElement(ElementName = "hotel")]
            public List<hotel> hotel { get; set; }
        }
        public class hotel
        {
            [XmlElement(ElementName = "codesthot")]
            public string codesthot { get; set; }
            [XmlElement(ElementName = "codpobhot")]
            public string codpobhot { get; set; }
            [XmlElement(ElementName = "hot_codigo")]
            public string hot_codigo { get; set; }
            [XmlElement(ElementName = "hot_codcobol")]
            public string hot_codcobol { get; set; }
            [XmlElement(ElementName = "hot_coddup")]
            public string hot_coddup { get; set; }
            [XmlElement(ElementName = "hot_afiliacion")]
            public string hot_afiliacion { get; set; }
        }
    }
}