﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;

namespace XiWan.Data.Restel
{
    public class RestelDAL
    {
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);

        #region 初始化本类
        private static RestelDAL _Instance;
        public static RestelDAL Instance
        {
            get
            {
                return new RestelDAL();
            }
        }
        #endregion

        /// <summary>
        /// 从接口获取国家信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetRestelCountry(string RestelUrl, string Identidad)
        {
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_paises.dtd'><peticion><nombre></nombre>
<agencia></agencia><tipo>5</tipo><idioma>2</idioma></peticion>");
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                //string fullUrl = RestelUrl + Identidad + Parameter;
                result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取国家信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                RestelCountryResp.respuesta obj = Common.Common.DESerializer<RestelCountryResp.respuesta>(result.Replace("&", ""));
                if (obj.parametros.error == null)
                {
                    RestelCountryCollection objCountryCol = new RestelCountryCollection();
                    for (int i = 0; i < obj.parametros.paises.pais.Count; i++)
                    {
                        RestelCountry objCountry = new RestelCountry();
                        objCountry.CountryCode = obj.parametros.paises.pais[i].codigo_pais;
                        objCountry.CountryName = obj.parametros.paises.pais[i].nombre_pais;
                        objCountryCol.Add(objCountry);
                    }
                    con.Save(objCountryCol);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取国家信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }
        /// <summary>
        /// 从接口获取省信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetProvinces(string RestelUrl, string Identidad)
        {
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_paises.dtd'><peticion><nombre></nombre>
<agencia></agencia><tipo>6</tipo><idioma>2</idioma></peticion>");
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取省信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                RestelProvincesResp.respuesta obj = Common.Common.DESerializer<RestelProvincesResp.respuesta>(result.Replace("&", ""));
                RestelProvincesCollection objRestelProvincesCol = new RestelProvincesCollection();
                for(int i=0;i<obj.parametros.provincias.provincia.Count;i++)
                {
                    RestelProvinces objProvinces = new RestelProvinces();
                    objProvinces.CountryCode = obj.parametros.provincias.provincia[i].codigo_pais;
                    objProvinces.ProvinceCode = obj.parametros.provincias.provincia[i].codigo_provincia;
                    objProvinces.ProvinceName = obj.parametros.provincias.provincia[i].nombre_provincia;
                    objRestelProvincesCol.Add(objProvinces);
                }
                con.Save(objRestelProvincesCol); 
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取省信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }
        /// <summary>
        /// 从接口获取TOWN信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetTown(string RestelUrl, string Identidad)
        {
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"SELECT ProvinceCode FROM dbo.RestelProvinces");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);            
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='iso-8859-1'?> <peticion> <tipo>18</tipo> <nombre></nombre> <agencia></agencia> <parametros> <codest>{0}</codest> <codpob></codpob> <marca></marca> </parametros> </peticion>", dt.Rows[j]["ProvinceCode"]);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取TOWN信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    RestelTownCollection objRestelRestelTown = new RestelTownCollection();
                    RestelTownResp.respuesta obj = Common.Common.DESerializer<RestelTownResp.respuesta>(result.Replace("&", ""));
                    for (int i = 0; i < obj.parametros.poblaciones.poblacion.Count; i++)
                    {
                        RestelTown objTown = new RestelTown();
                        objTown.TownCode = obj.parametros.poblaciones.poblacion[i].codpob;
                        objTown.TownName = obj.parametros.poblaciones.poblacion[i].poblidinom1;
                        objTown.TownNameEn = obj.parametros.poblaciones.poblacion[i].poblidinom2;
                        objTown.TownNameFr = obj.parametros.poblaciones.poblacion[i].poblidinom3;
                        objTown.TownNameGe = obj.parametros.poblaciones.poblacion[i].poblidinom4;
                        objTown.TownNameIt = obj.parametros.poblaciones.poblacion[i].poblidinom5;
                        objTown.TownNamePo = obj.parametros.poblaciones.poblacion[i].poblidinom6;
                        objTown.ProvinceCode = obj.parametros.poblaciones.poblacion[i].deppob;
                        objRestelRestelTown.Add(objTown);
                    }
                    con.Save(objRestelRestelTown);
                }    
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取TOWN信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }
        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetHotel(string RestelUrl, string Identidad, string codusu, string afiliacio)
        {
            try
            {
                string sql = string.Format(@"SELECT ProvinceCode FROM dbo.RestelProvinces");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    string result = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_directorio.dtd'><peticion><tipo>7</tipo><nombre></nombre><agencia></agencia>
<parametros><hotel/><pais>{0}</pais><poblacion/><provincia/><categoria>0</categoria><usuario>{1}</usuario><afiliacion>{2}</afiliacion>
<servhot1/><servhot2/><servhot3/><servhab1/><servhab2/><servhab3/><idioma>2</idioma><radio>1</radio></parametros></peticion>", dt.Rows[j]["ProvinceCode"], codusu,  afiliacio);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取酒店信息并保存到数据库 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    Regex r = new Regex("(?<=<descripcion>).*?(?=.</descripcion>)", RegexOptions.IgnoreCase);
                    result = r.Replace(result, "");
                    Regex r1 = new Regex("(?<=<descripcion>).*?(?=</descripcion>)", RegexOptions.IgnoreCase);
                    result = r1.Replace(result, "");
                    Regex r2 = new Regex("(?<=<como_llegar>).*?(?=</como_llegar>)", RegexOptions.IgnoreCase);
                    result = r2.Replace(result, "");
                    Regex r3 = new Regex("(?<=<como_llegar>).*?(?=</como_llegar>)", RegexOptions.IgnoreCase);
                    result = r3.Replace(result, "");
                    RestelHotelResp.respuesta obj = Common.Common.DESerializer<RestelHotelResp.respuesta>(result.Replace("<br />", "").Replace("<p>", "").Replace("</p>", "").Replace("<b>", "").Replace("</b>", "").Replace("&", "").Replace("</</", "</"));
                    RestelHotelCollection objRestelHotelCol = new RestelHotelCollection();
                    if (obj != null)
                    {
                        for (int i = 0; i < obj.parametros.hoteles.hotel.Count; i++)
                        {
                            RestelHotel objHotel = new RestelHotel();
                            objHotel.HotelCode = obj.parametros.hoteles.hotel[i].codigo;
                            objHotel.HotelName = obj.parametros.hoteles.hotel[i].nombre_h;
                            objHotel.ProvinceCode = obj.parametros.hoteles.hotel[i].provincia;
                            objHotel.ProvinceName = obj.parametros.hoteles.hotel[i].provincia_nombre;
                            objHotel.TownName = obj.parametros.hoteles.hotel[i].poblacion;
                            objHotel.HotelDescription = "";//obj.parametros.hoteles.hotel[i].descripcion;
                            objHotel.Staring = obj.parametros.hoteles.hotel[i].categoria;
                            objHotel.Calidad = obj.parametros.hoteles.hotel[i].calidad;
                            objHotel.Marca = obj.parametros.hoteles.hotel[i].marca;
                            objHotel.Longitud = obj.parametros.hoteles.hotel[i].longitud;
                            objHotel.Latitud = obj.parametros.hoteles.hotel[i].latitud;
                            objHotel.Categoria2 = obj.parametros.hoteles.hotel[i].categoria2;
                            objHotel.HotelCobol = obj.parametros.hoteles.hotel[i].codigo_cobol;
                            objRestelHotelCol.Add(objHotel);
                        }
                        con.Save(objRestelHotelCol);
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取酒店信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }
        /// <summary>
        /// 从接口获取报价
        /// </summary>
        /// <returns></returns>
        public DataTable GetRealHotelSQuote(string RestelUrl, string Identidad, string hotelCode,string nationality,string room, string CheckInDate, string CheckoutDate, string codusu, string afiliacio,int Adults,int Childs)
        {
            DataTable reDt = new DataTable();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"SELECT rh.ProvinceCode,rp.CountryCode FROM dbo.RestelHotel rh INNER JOIN dbo.RestelProvinces rp ON rp.ProvinceCode=rh.ProvinceCode WHERE rh.HotelCobol='{0}'", hotelCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd'><peticion><tipo>110</tipo><nombre></nombre>
<agencia></agencia><parametros><hotel>{0}</hotel><pais>{1}</pais><provincia>{2}</provincia><poblacion></poblacion><categoria>0</categoria>
<radio>9</radio><fechaentrada>{3}</fechaentrada><fechasalida>{4}</fechasalida><marca></marca><afiliacion>{5}</afiliacion>
<usuario>{6}</usuario>{7}<idioma>2</idioma><duplicidad>1</duplicidad><comprimido>2</comprimido><informacion_hotel>0</informacion_hotel></parametros></peticion>",
    hotelCode + "#", dt.Rows[0]["CountryCode"], dt.Rows[0]["ProvinceCode"], CheckInDate, CheckoutDate, afiliacio, codusu, room);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio：Restel从接口获取报价 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err：Restel从接口获取报价Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return reDt;
                    }
                    RestelHotelQuoteResp.respuesta objResp = Common.Common.DESerializer<RestelHotelQuoteResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.param.error == null)
                        {
                            if (objResp.param.hotls.num.AsTargetType<int>(0) == 0)
                            {
                                return reDt;
                            }
                            else
                            {
                                HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                                for (int i = 0; i < objResp.param.hotls.hot.Count; i++)
                                {
                                    for (int j = 0; j < objResp.param.hotls.hot[i].res.pax.Count; j++)
                                    {
                                        for (int x = 0; x < objResp.param.hotls.hot[i].res.pax[j].hab.Count; x++)
                                        {
                                            for (int y = 0; y < objResp.param.hotls.hot[i].res.pax[j].hab[x].reg.Count; y++)
                                            {
                                                if (objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].esr == "OK")
                                                {
                                                    HotelPrice obj = new HotelPrice();
                                                    obj.HotelID = objResp.param.hotls.hot[i].cod;
                                                    obj.HotelNo = "";
                                                    obj.TerminalCode = "Restel";
                                                    obj.ClassName = objResp.param.hotls.hot[i].res.pax[j].hab[x].desc;
                                                    obj.Price = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].pvp.AsTargetType<decimal>(0);
                                                    obj.CurrencyCode = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].div;
                                                    string Lodging = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].cod;
                                                    if (Lodging == "RO" || Lodging == "OB" || Lodging == "SA")
                                                    {
                                                        obj.IsBreakfast = 0;
                                                    }
                                                    else
                                                    {
                                                        obj.IsBreakfast = 1;
                                                    }
                                                    obj.ReferenceClient = objResp.param.hotls.hot[i].res.pax[j].hab[x].reg[y].lin;
                                                    string Adults_Childs = objResp.param.hotls.hot[i].res.pax[j].cod;
                                                    int RoomAdults = Adults_Childs.Split('-')[0].AsTargetType<int>(0);
                                                    int RoomChilds = Adults_Childs.Split('-')[1].AsTargetType<int>(0);
                                                    obj.Adults = Adults;
                                                    obj.Childs = Childs;
                                                    obj.RoomAdults = RoomAdults;
                                                    obj.RoomChilds = RoomChilds;
                                                    obj.ChildsAge = objResp.param.hotls.hot[i].res.pax[j].cod;
                                                    obj.CheckInDate = Convert.ToDateTime(CheckInDate);
                                                    obj.CheckOutDate = Convert.ToDateTime(CheckoutDate);
                                                    obj.IsClose = 1;
                                                    obj.CreateTime = DateTime.Now;
                                                    obj.UpdateTime = DateTime.Now;
                                                    obj.EntityState = e_EntityState.Added;
                                                    objHotelPriceCol.Add(obj);
                                                }
                                            }
                                        }
                                    }
                                }
                                string updateHotelPriceSql = string.Format(@"update HotelPrice  set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' AND TerminalCode='Restel' AND Adults={1} AND Childs={2}   AND CheckInDate='{3}' AND CheckOutDate='{4}'",
hotelCode, Adults, Childs, CheckInDate, CheckoutDate);
                                con.Update(updateHotelPriceSql);
                                con.Save(objHotelPriceCol);
                            }
                        }
                        else
                        {
                            return reDt;
                        }
                    }
                    string sqlDt = string.Format(@"select ID,HotelID,ClassName,Price as TotalPrice,CurrencyCode,IsBreakfast,RoomAdults,RoomChilds from HotelPrice WITH(NOLOCK) where HotelID={0} AND TerminalCode='Restel'  AND Adults={1} AND Childs={2}  AND CheckInDate='{3}' AND CheckOutDate='{4}' AND IsClose=1 order by ID"
                , hotelCode, Adults, Childs, CheckInDate, CheckoutDate);
                    reDt = ControllerFactory.GetController().GetDataTable(sqlDt);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Restel从接口获取报价 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return reDt;
        }
        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationality()
        {
            string sql = "select CountryCode AS ID,CountryName AS NAME from RestelCountry ORDER BY CountryName";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestination(string strName)
        {
            string sql = string.Format(@" SELECT (rc.CountryName + ',' + rp.ProvinceName) as name,(rp.CountryCode + ',' + rp.ProvinceCode) as value 
FROM dbo.RestelCountry rc INNER JOIN RestelProvinces rp ON rp.CountryCode=rc.CountryCode WHERE (rc.CountryName + ',' + rp.ProvinceName) like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取取消期限
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelPriceIDs"></param>
        /// <returns></returns>
        public string GetCancellationPolicy(string RestelUrl, string Identidad, string HotelPriceIDs)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM HotelPrice where id in ({0})", HotelPriceIDs.TrimEnd(','));
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string hotelCode = string.Empty;
                string strlin = string.Empty;
                if (dt.Rows.Count > 0)
                {
                    hotelCode = dt.Rows[0]["HotelID"].ToString();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strlin += string.Format(@"<lin>{0}</lin>", dt.Rows[i]["ReferenceClient"]);
                    }
                }
                string Peticion = string.Format(@"<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_algGastosCanc.dtd'>
<peticion><tipo>144</tipo><nombre></nombre><agencia />
<parametros><datos_reserva><hotel>{0}</hotel>{1}</datos_reserva></parametros></peticion>",
hotelCode, strlin);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel获取取消期限 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel获取取消期限Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelCancellationResp.respuesta objResp = Common.Common.DESerializer<RestelCancellationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        string strMes = string.Empty;
                        for (int i = 0; i < objResp.parametros.politicaCanc.Count; i++)
                        {
                            strMes += objResp.parametros.politicaCanc[i].entra_en_gastos + "," + objResp.parametros.politicaCanc[i].fecha + ";";
                        }
                        resp = new Respone() { code = "00", mes = strMes };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel获取取消期限 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel获取取消期限 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 预订Leg_Status Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认)  99表示失败
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelPriceIDs"></param>
        /// <returns>ServiceName保存接口的名称</returns>
        public string Reserva(string RestelUrl, string Identidad, string HotelPriceIDs)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM HotelPrice where id in ({0})", HotelPriceIDs.TrimEnd(','));
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string hotelCode = string.Empty;
                string strlin = string.Empty;
                string ocupantes = string.Empty;
                string guid = Common.Common.GuidToLongID();
                string Leg_Status=string.Empty;
                if (dt.Rows.Count > 0)
                {
                    hotelCode = dt.Rows[0]["HotelID"].ToString();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        strlin += string.Format(@"<lin>{0}</lin>", dt.Rows[i]["ReferenceClient"]);
                    }
                    ocupantes = "San#Zhang#.#21#@";
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_reserva_3.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>202</tipo><parametros><codigo_hotel>{0}</codigo_hotel><nombre_cliente>{1}</nombre_cliente>
<observaciones></observaciones><num_mensaje /><num_expediente>{2}</num_expediente><forma_pago>25</forma_pago><tipo_targeta></tipo_targeta>
<num_targeta></num_targeta><cvv_targeta></cvv_targeta><mes_expiracion_targeta></mes_expiracion_targeta><ano_expiracion_targeta></ano_expiracion_targeta>
<titular_targeta></titular_targeta><ocupantes>{3}</ocupantes><res>{4}</res></parametros></peticion>",
    hotelCode, "San Zhang",guid, ocupantes, strlin);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio：Restel预订 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                        LogHelper.DoOperateLog(string.Format("Err：Restel预订Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    RestelReservaResp.respuesta objResp = Common.Common.DESerializer<RestelReservaResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.parametros.error == null)
                        {
                            string strMes = string.Empty;
                            string StatusName = string.Empty;
                            Booking booking = new Booking();
                            //保存订单到数据库
                            booking.BookingCode = objResp.parametros.n_localizador;
                            booking.AgentBookingReference = guid;
                            booking.ClientCurrencyCode = objResp.parametros.divisa_total;
                            booking.ClientPartnerAmount = objResp.parametros.importe_total_reserva;
                            booking.HotelId = dt.Rows[0]["HotelID"].ToString();
                            booking.HotelNo = "";
                            booking.Platform = "Restel";
                            booking.StatusCode = objResp.parametros.estado;
                            if (objResp.parametros.estado == "00")
                            {
                                StatusName = "预订成功";
                                Leg_Status = "11";
                            }
                            else
                            {
                                StatusName = "预订失败";
                                Leg_Status = "99";
                            }
                            booking.StatusName = StatusName;
                            booking.ServiceID = 0;
                            booking.ServiceName = "Reserva";
                            booking.LegID = 0;
                            booking.Leg_Status = Leg_Status;
                            booking.CreatTime = DateTime.Now;
                            booking.UpdateTime = DateTime.Now;
                            booking.EntityState = e_EntityState.Added;
                            con.Save(booking);
                            resp = new Respone() { code = "00", mes = StatusName };
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = objResp.parametros.error };
                            LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        LogHelper.DoOperateLog(string.Format("Studio：Restel预订 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel预订 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 预订确认 或 取消预订 Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认) 99表示失败
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        ///  <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="ReservationType">AE预订确认  或  AI预订取消</param>
        /// <returns>localizador_corto预订短号保存到HotelNo，ServiceName保存接口的名称，ServiceID保存预订确认长号</returns>
        public string ReservaConfirmation(string RestelUrl, string Identidad, string BookingNumber, string ReservationType)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string ServiceName = string.Empty;
                string StatusName = string.Empty;
                string Leg_Status = string.Empty;
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_reserva.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>3</tipo><parametros><localizador>{0}</localizador><accion>{1}</accion></parametros></peticion>",
BookingNumber, ReservationType);
                if (ReservationType == "AE")
                {
                    ServiceName = "ReservationConfirmation";
                }
                else if (ReservationType == "AI")
                {
                    ServiceName = "ReservationAnnulment";
                }
                else
                {
                    ServiceName = "";
                }
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel预订确认或取消预订 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelReservaConfirmationResp.respuesta objResp = Common.Common.DESerializer<RestelReservaConfirmationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        if (objResp.parametros.estado == "00" && ReservationType == "AE")
                        {
                            StatusName = "预订已确认";
                            Leg_Status = "21";
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',ServiceID='{2}',HotelNo='{3}',StatusName='{4}',Leg_Status='{5}' where BookingCode='{6}'  and Platform='Restel' ",
                            objResp.parametros.estado, ServiceName, objResp.parametros.localizador, objResp.parametros.localizador_corto, StatusName, Leg_Status, BookingNumber);
                            con.Update(strUpdateSql);
                        }
                        else if (objResp.parametros.estado == "00" && ReservationType == "AI")
                        {
                            StatusName = "预订已取消";
                            Leg_Status = "10";
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',ServiceID='{2}',HotelNo='{3}',StatusName='{4}',Leg_Status='{5}' where BookingCode='{6}'  and Platform='Restel' ",
                            objResp.parametros.estado, ServiceName, objResp.parametros.localizador, objResp.parametros.localizador_corto, StatusName, Leg_Status, BookingNumber);
                            con.Update(strUpdateSql);
                        }
                        else
                        {
                            StatusName = "";
                        }
                        resp = new Respone() { code = "00", mes = StatusName };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                        LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel预订确认或取消预订 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 取消订单  Leg_Status 1开头表示预订，2开头表示预订确认(0表示取消，1表示确认) 99表示失败
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="BookingNumberCorto">订单短号</param>
        /// <returns></returns>
        public string ReservaCancellation(string RestelUrl, string Identidad, string BookingNumber)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format("SELECT * FROM Booking where BookingCode = '{0}'", BookingNumber);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    string ServiceName = "ReservaCancellation";
                    string StatusName = string.Empty;
                    string Leg_Status = string.Empty;
                    string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_cancelacion.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>401</tipo>
<parametros><localizador_largo>{0}</localizador_largo><localizador_corto>{1}</localizador_corto></parametros></peticion>",
    BookingNumber, dt.Rows[0]["HotelNo"]);
                    string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                    try
                    {
                        result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                        LogHelper.DoOperateLog(string.Format("Studio：Restel取消订单 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                    }
                    catch (Exception ex)
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                        LogHelper.DoOperateLog(string.Format("Err：Restel取消订单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    RestelReservaCancellationResp.respuesta objResp = Common.Common.DESerializer<RestelReservaCancellationResp.respuesta>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.parametros.error == null)
                        {
                            if (objResp.parametros.estado == "00")
                            {
                                StatusName = "已取消订单";
                                Leg_Status = "20";
                                string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),ServiceName='{1}',StatusName='{2}',Leg_Status='{3}' where BookingCode='{4}'  and Platform='Restel' ",
                                objResp.parametros.estado, ServiceName, StatusName,Leg_Status, BookingNumber);
                                con.Update(strUpdateSql);
                            }
                            else
                            {
                                StatusName = "取消订单失败";
                            }
                            resp = new Respone() { code = "00", mes = StatusName };
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = objResp.parametros.error };
                            LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", objResp.parametros.error, DateTime.Now.ToString()), "Restel接口");
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Err");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "无该订单号" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", "无该订单号", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel取消订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查看订单
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string ReservationInformation(string RestelUrl, string Identidad, string BookingNumber, string Codusu)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string ServiceName = "ReservaCancellation";
                string StatusName = string.Empty;
                string ResultStatus = string.Empty;
                string Leg_Status = string.Empty;
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_inforeserva.dtd'>
<peticion><tipo>11</tipo><nombre></nombre><agencia></agencia><parametros><Localizador>{1}</Localizador>
<Afiliacion>HA</Afiliacion></parametros></peticion>",
 Codusu,BookingNumber);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel查看订单 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RetelReservationInformationResp.respuesta objResp = Common.Common.DESerializer<RetelReservationInformationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.RESERVA.error == null)
                    {
                        if (objResp.RESERVA.Status == "00")
                        {
                            ResultStatus = "查询成功";
                            if(objResp.RESERVA.Estado=="B")
                            {
                                StatusName = "已取消订单";
                                Leg_Status = "20";
                            }
                            else if (objResp.RESERVA.Estado == "C")
                            {
                                StatusName = "预订已确认";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "F")
                            {
                                StatusName = "开具发票";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "N")
                            {
                                StatusName = "正在进行中";
                                Leg_Status = "11";
                            }
                            string strUpdateSql = string.Format("update Booking set Leg_Status='{0}',UpdateTime=GETDATE() where BookingCode='{1}'  and Platform='Restel' ",
                            Leg_Status, BookingNumber);
                            con.Update(strUpdateSql);
                        }
                        else
                        {
                            ResultStatus = "查询失败";
                        }
                        resp = new Respone() { code = "00", mes = ResultStatus };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.RESERVA.error };
                        LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", objResp.RESERVA.error, DateTime.Now.ToString()), "Restel接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查看订单
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber">Reserva接口返回的订单号（预订）</param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string RestlLastReservations(string RestelUrl, string Identidad, string seachMode, string BookingNumber, string searchDate, string searchHotelName, string searchCustomerName, string Codusu)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string ServiceName = "ReservaCancellation";
                string StatusName = string.Empty;
                string ResultStatus = string.Empty;
                string Leg_Status = string.Empty;
                string dia=string.Empty;
                string mes=string.Empty;
                string ano=string.Empty;
                string hotelName = string.Empty;
                string CustomerName = string.Empty;
                string BookingCode = string.Empty;
                if (seachMode == "4")
                {
                    string[] ArrayDate = searchDate.Split('-');
                    if (ArrayDate != null)
                    {
                        dia = ArrayDate[2];
                        mes = ArrayDate[1];
                        ano = ArrayDate[0];
                    }
                }
                else if(seachMode=="3")
                {
                    hotelName = searchHotelName;
                }
                else if (seachMode == "1")
                {
                    CustomerName = searchCustomerName;
                }
                else if (seachMode == "2")
                {
                    BookingCode = BookingNumber;
                }
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_ultimasreservas.dtd'>
<peticion><tipo>8</tipo><nombre></nombre> <agencia></agencia><parametros><selector>{0}</selector><dia>{1}</dia>
<mes>{2}</mes><ano>{3}</ano> <usuario>{4}</usuario><hotel>{5}</hotel><cliente>{6}</cliente><localizador>{7}</localizador></parametros>
</peticion>",
 seachMode, dia, mes, ano, Codusu, searchHotelName, CustomerName, BookingCode);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel查看订单（LastReservations） ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单Post数据（LastReservations） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RetelReservationInformationResp.respuesta objResp = Common.Common.DESerializer<RetelReservationInformationResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.RESERVA.error == null)
                    {
                        if (objResp.RESERVA.Status == "00")
                        {
                            ResultStatus = "查询成功";
                            if (objResp.RESERVA.Estado == "B")
                            {
                                StatusName = "已取消订单";
                                Leg_Status = "20";
                            }
                            else if (objResp.RESERVA.Estado == "C")
                            {
                                StatusName = "预订已确认";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "F")
                            {
                                StatusName = "开具发票";
                                Leg_Status = "21";
                            }
                            else if (objResp.RESERVA.Estado == "N")
                            {
                                StatusName = "正在进行中";
                                Leg_Status = "11";
                            }
                        }
                        else
                        {
                            ResultStatus = "查询失败";
                        }
                        string strUpdateSql = string.Format("update Booking set Leg_Status='{0}',UpdateTime=GETDATE() where BookingCode='{1}'  and Platform='Restel' ",
                            Leg_Status, BookingNumber);
                        con.Update(strUpdateSql);
                        resp = new Respone() { code = "00", mes = ResultStatus };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.RESERVA.error };
                        LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", objResp.RESERVA.error, DateTime.Now.ToString()), "Restel接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查看订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查看预订后取消生成的费用
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber"></param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string GetCancellationFees(string RestelUrl, string Identidad, string BookingNumber, string Codusu)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='ISO-8859-1' ?><peticion><tipo>142</tipo><nombre></nombre><agencia></agencia>
<parametros><usuario>{0}</usuario> <localizador>{1}</localizador> <idioma>2</idioma></parametros></peticion>", Codusu, BookingNumber);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel查看预订后取消生成的费用 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看预订后取消生成的费用Post数据（LastReservations） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelCancellationFeesResp.respuesta objResp = Common.Common.DESerializer<RestelCancellationFeesResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        string strMes = string.Empty;
                        for (int i = 0; i < objResp.parametros.politicaCanc.Count; i++)
                        {
                            string days = objResp.parametros.politicaCanc[i].dias_antelacion.AsTargetType<string>("0");
                            string incurrs = objResp.parametros.politicaCanc[i].horas_antelacion.AsTargetType<string>("0");
                            string nights = objResp.parametros.politicaCanc[i].noches_gasto.AsTargetType<string>("0");
                            string amount = objResp.parametros.politicaCanc[i].estCom_gasto.AsTargetType<string>("0");

                            strMes += "预订前需要支付取消费用的天数:" + days + "," + "预订中取消费用的一天中的小时:" + incurrs + "," + "将作为取消费用收取的晚数:" + nights + "," + "将作为取消费用收取的预订总金额的百分比:" + amount + ";";
                        }
                        resp = new Respone() { code = "00", mes = strMes };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查看预订后取消生成的费用 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查看预订后取消生成的费用 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查询酒店评论
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelCode"></param>
        /// <returns></returns>
        public string GetHotelRemarks(string RestelUrl, string Identidad, string HotelCode,string CheckInDate, string CheckoutDate)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string Peticion = string.Format(@"<?xml version='1.0' encoding='iso-8859-1'?> <peticion> <tipo>24</tipo> 
<parametros> <codigo_cobol>{0}</codigo_cobol> <entrada>{1}</entrada> <salida>{2}</salida> </parametros> </peticion>", HotelCode, CheckInDate, CheckoutDate);
                string Parameter = "&xml=" + HttpUtility.UrlEncode(Peticion);
                try
                {
                    result = Common.Common.PostHttp(RestelUrl, Identidad + Parameter).Replace("<![CDATA[", "").Replace("]]>", "");
                    LogHelper.DoOperateLog(string.Format("Studio：Restel查询酒店评论 ,请求数据 :{0} , 返回数据 ：{1} , Time:{2} ", Peticion, result, DateTime.Now.ToString()), "Restel接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查询酒店评论Post数据（LastReservations） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                RestelHotelRemarksResp.respuesta objResp = Common.Common.DESerializer<RestelHotelRemarksResp.respuesta>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.parametros.error == null)
                    {
                        if (objResp.parametros.hotel.observaciones.num.AsTargetType<int>(0) == 0)
                        {
                            resp = new Respone() { code = "00", mes = "没数据" };
                        }
                        else
                        {
                            string strMes = objResp.parametros.hotel.observaciones.observacion.obs_texto;
                            resp = new Respone() { code = "00", mes = strMes };
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = objResp.parametros.error };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err：Restel查询酒店评论 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Restel接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Restel查询酒店评论 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }
            public string result { get; set; }
            public string mes { get; set; }
        }
    }
}