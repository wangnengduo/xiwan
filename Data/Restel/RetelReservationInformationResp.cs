﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Restel
{
    public class RetelReservationInformationResp
    {
        public class respuesta
        {
            [XmlElement(ElementName = "tipo")]
            public string tipo { get; set; }
            [XmlElement(ElementName = "nombre")]
            public string nombre { get; set; }
            [XmlElement(ElementName = "agencia")]
            public string agencia { get; set; }
            [XmlElement(ElementName = "RESERVA")]
            public RESERVA RESERVA { get; set; }
        }
        public class RESERVA
        {
            [XmlElement(ElementName = "error")]
            public string error { get; set; }
            [XmlElement(ElementName = "Localizador")]
            public string Localizador { get; set; }
            [XmlElement(ElementName = "Agencia_reserva")]
            public string Agencia_reserva { get; set; }
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "Alta")]
            public string Alta { get; set; }
            [XmlElement(ElementName = "Fecha_Entrada")]
            public string Fecha_Entrada { get; set; }
            [XmlElement(ElementName = "Fecha_Salida")]
            public string Fecha_Salida { get; set; }
            [XmlElement(ElementName = "Cliente")]
            public string Cliente { get; set; }
            [XmlElement(ElementName = "Expediente")]
            public string Expediente { get; set; }
            [XmlElement(ElementName = "Agente")]
            public string Agente { get; set; }
            [XmlElement(ElementName = "Obs_agencia")]
            public string Obs_agencia { get; set; }
            [XmlElement(ElementName = "Estado")]
            public string Estado { get; set; }
            [XmlElement(ElementName = "PVP")]
            public string PVP { get; set; }
            [XmlElement(ElementName = "PVA")]
            public string PVA { get; set; }
            [XmlElement(ElementName = "CVA")]
            public string CVA { get; set; }
            [XmlElement(ElementName = "BAS")]
            public string BAS { get; set; }
            [XmlElement(ElementName = "Linea")]
            public Linea Linea { get; set; }
            [XmlElement(ElementName = "id")]
            public string id { get; set; }
        }
        public class Linea
        {
            [XmlElement(ElementName = "HotPro")]
            public string HotPro { get; set; }
            [XmlElement(ElementName = "Hotel")]
            public string Hotel { get; set; }
            [XmlElement(ElementName = "Afiliacion")]
            public string Afiliacion { get; set; }
            [XmlElement(ElementName = "Nombre_Hotel")]
            public string Nombre_Hotel { get; set; }
            [XmlElement(ElementName = "Pais")]
            public string Pais { get; set; }
            [XmlElement(ElementName = "Provincia")]
            public string Provincia { get; set; }
            [XmlElement(ElementName = "Fecha")]
            public Fecha Fecha { get; set; }
        }
        public class Fecha
        {
            [XmlElement(ElementName = "Entrada")]
            public string Entrada { get; set; }
            [XmlElement(ElementName = "Salida")]
            public string Salida { get; set; }
            [XmlElement(ElementName = "Precio")]
            public string Precio { get; set; }
            [XmlElement(ElementName = "Porcen_iva")]
            public string Porcen_iva { get; set; }
            [XmlElement(ElementName = "Iva_inc")]
            public string Iva_inc { get; set; }
            [XmlElement(ElementName = "Servicio")]
            public string Servicio { get; set; }
            [XmlElement(ElementName = "Divisa")]
            public string Divisa { get; set; }
            [XmlElement(ElementName = "Num_Hab")]
            public string Num_Hab { get; set; }
            [XmlElement(ElementName = "Tipo_Hab")]
            public string Tipo_Hab { get; set; }
            [XmlElement(ElementName = "Forma_pago")]
            public string Forma_pago { get; set; }
        }

    }
}