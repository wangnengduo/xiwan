﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class AsianoverlandReq
    {
        public class AsianoverlandCountry
        {
            public string Code { get; set; }
            public string Name { get; set; }
        }
        public class AsianoverlandCity
        {
            public List<CityInfo> CityInfo { get; set; }
            public string Status { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
        public class CityInfo
        {
            public string CityCode { get; set; }
            public string Name { get; set; }
        }

        public class AsianoverlandNationality
        {
            public List<NationalitiesInfo> NationalitiesInfo { get; set; }
            public string Status { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
        public class NationalitiesInfo
        {
            public string Code { get; set; }
            public string Nationality { get; set; }
            public string IsoCode { get; set; }
        }

        public class AsianoverlandHotels
        {
            public List<LocalHotelList> LocalHotelList { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
        public class LocalHotelList
        {
            /// <summary>
            /// 1994
            /// </summary>
            public string Id { get; set; }
            /// <summary>
            /// Andorra Center
            /// </summary>
            public string Name { get; set; }
        }

        public class AsianoverlandHoteldetails
        {
            public string Name { get; set; }
            public string Rating { get; set; }
            public string Latitude { get; set; }
            public string Longitude { get; set; }
            public string Phone { get; set; }
            public string Address { get; set; }
            public string Fax { get; set; }
            public string Short_desc { get; set; }
            public string Website { get; set; }
            public string Email { get; set; }
            public string MainImage { get; set; }
            public List<string> OptionalImages { get; set; }
            public string LongDesc { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }

        public class roomDetails
        {
            public int numberOfAdults { get; set; }
            public string numberOfChild { get; set; }
            public string ChildAge { get; set; }
        }
    }
}