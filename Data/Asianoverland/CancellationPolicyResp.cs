﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class CancellationPolicyResp
    {
        public class CancellationPolicy
        {
            public string Message { get; set; }
            public string CancellationCurrency { get; set; }
            public string TotalBookingAmount { get; set; }
            public string ContractComment { get; set; }
            public double CancellationHours { get; set; }
            public double AppliedAgentCharges { get; set; }
            public BookingAllowedInfo BookingAllowedInfo { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string MessageInfo { get; set; }
        }
        public class BookingAllowedInfo
        {
            public string Message { get; set; }
            public string SoldOut { get; set; }
            public string MessageInfo { get; set; }
            public string BookingAllowed { get; set; }
        }

        public class CancellationFee
        {
            public string Message { get; set; }
            public string AllowCancel { get; set; }
            public string MessageInfo { get; set; }
            public string CancellationCharge { get; set; }
            public string DisplayCurrencyCode { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
    }
}