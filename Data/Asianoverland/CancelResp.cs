﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class CancelResp
    {
        public string Message { get; set; }
        public string MessageInfo { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }
}