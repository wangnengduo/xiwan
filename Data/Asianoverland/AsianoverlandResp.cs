﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class AsianoverlandResp
    {
        #region 获取报价
        public class RoomTypeOutPriceResp
        {
            public RoomTypePriceResp RoomTypePrice { get; set; }
            public class RoomTypePriceResp
            {
                public int TotalCount { get; set; }
                public string WebServiceVersion { get; set; }
                public string Message { get; set; }
                public List<HotelList> HotelList { get; set; }
                public string SearchUniqueId { get; set; }
                public string StartTime { get; set; }
                public string EndTime { get; set; }
                public string MessageInfo { get; set; }
            }
            public class HotelList
            {
                public string HotelId { get; set; }
                public string HotelName { get; set; }
                public string LocalHotelId { get; set; }
                public string PropertyRating { get; set; }
                public string Available { get; set; }
                public string RateCurrencyCode { get; set; }
                public double TotalCharges { get; set; }
                public List<HotelProperty> HotelProperty { get; set; }
            }
            public class HotelProperty
            {
                public double DisplayRoomRate { get; set; }
                public string Type { get; set; }
                public string SectionUniqueId { get; set; }
                public List<string> RoomDetails { get; set; }
                public List<RoomRates> RoomRates { get; set; }
            }
            public class RoomRates
            {
                public int Available { get; set; }
                public int NumberOfRooms { get; set; }
                public int NumberOfAdults { get; set; }
                public string NumberOfChild { get; set; }
                public double RoomRate { get; set; }
                public string RoomType { get; set; }
                public string RoomCategory { get; set; }
                public string RatePlanCode { get; set; }
                public string available_inventory { get; set; }
                public string inventoryClass { get; set; }
                public string RoomTypeCode { get; set; }
                public string MealBasis { get; set; }
                public string Note { get; set; }
                public string ClassUniqueId { get; set; }
                public List<RateBreakup> RateBreakup { get; set; }
            }
            public class RateBreakup
            {
                public string Date { get; set; }
                public string Day { get; set; }
                public double DisplayNightlyRate { get; set; }
            }
        }
        #endregion

        #region 订单取消政策
        public class CancellationPolicyResp
        {
            public class CancellationPolicy
            {
                public string Message { get; set; }
                public string CancellationCurrency { get; set; }
                public string TotalBookingAmount { get; set; }
                public string ContractComment { get; set; }
                public double CancellationHours { get; set; }
                public double AppliedAgentCharges { get; set; }
                public BookingAllowedInfo BookingAllowedInfo { get; set; }
                public DateTime StartTime { get; set; }
                public DateTime EndTime { get; set; }
                public string MessageInfo { get; set; }
            }
            public class BookingAllowedInfo
            {
                public string Message { get; set; }
                public string SoldOut { get; set; }
                public string MessageInfo { get; set; }
                public string BookingAllowed { get; set; }
            }

            public class CancellationFee
            {
                public string Message { get; set; }
                public string AllowCancel { get; set; }
                public string MessageInfo { get; set; }
                public string CancellationCharge { get; set; }
                public string DisplayCurrencyCode { get; set; }
                public DateTime StartTime { get; set; }
                public DateTime EndTime { get; set; }
            }
        }
        #endregion

        #region 预订
        public class BookingResp
        {
            public class Booking
            {
                public string Message { get; set; }
                public string BookingServiceType { get; set; }
                public BookingDetail BookingDetail { get; set; }
                public DateTime StartTime { get; set; }
                public DateTime EndTime { get; set; }
                public string MessageInfo { get; set; }
            }
            public class BookingDetail
            {
                public string Id { get; set; }
                public string BookingId { get; set; }
                public string AgentId { get; set; }
                public string BookingReference { get; set; }
                public string LeaderTitle { get; set; }
                public string LeaderFirstName { get; set; }
                public string LeaderLastName { get; set; }
                public string TotalCharges { get; set; }
                public string CurrencyCode { get; set; }
                public string HotelName { get; set; }
                public string CountryName { get; set; }
                public string CityId { get; set; }
                public string HotelAddress1 { get; set; }
                public string CurrentStatus { get; set; }
                public DateTime BookingDate { get; set; }
                public DateTime ExpirationDate { get; set; }
                public string NoOfAdult { get; set; }
                public string NoOfChild { get; set; }
                public string TotalRooms { get; set; }
                public string HotelAddress { get; set; }
                public DateTime CheckInDate { get; set; }
                public DateTime CheckOutDate { get; set; }
                public string AgentRate { get; set; }
                public string HotelPhone { get; set; }
                public string HotelRating { get; set; }
                public List<RoomDetail> RoomDetail { get; set; }
            }
            public class RoomDetail
            {
                public string RoomTypeDescription { get; set; }
                public List<Passengers> Passengers { get; set; }
            }
            public class Passengers
            {
                public string Salutations { get; set; }
                public string FirstName { get; set; }
                public string LastName { get; set; }
                public string PassengerType { get; set; }
                public string Age { get; set; }
            }

            public class roomDetails
            {
                public string numberOfChilds { get; set; }
                public string roomClassId { get; set; }
                public List<passangers> passangers { get; set; }
            }
            public class passangers
            {
                public string salutation { get; set; }
                public string first_name { get; set; }
                public string last_name { get; set; }
                public string age { get; set; }
            }
        }
        #endregion

        #region 查询订单
        public class CheckingOrderResp
        {
            public class CheckingOrder
            {
                public string Message { get; set; }
                public string BookingServiceType { get; set; }
                public BookingDetail BookingDetail { get; set; }
                public DateTime StartTime { get; set; }
                public DateTime EndTime { get; set; }
                public string MessageInfo { get; set; }
            }
            public class BookingDetail
            {
                public string Id { get; set; }
                public string BookingReference { get; set; }
                public string LeaderTitle { get; set; }
                public string LeaderFirstName { get; set; }
                public string LeaderLastName { get; set; }
                public string CurrencyCode { get; set; }
                public string LocalHotelId { get; set; }
                public string HotelName { get; set; }
                public string CountryName { get; set; }
                public string CityId { get; set; }
                public string HotelAddress1 { get; set; }
                public string CurrentStatus { get; set; }
                public DateTime ExpirationDate { get; set; }
                public string TotalChildren { get; set; }
                public DateTime CheckInDate { get; set; }
                public DateTime CheckOutDate { get; set; }
                public string AgentRate { get; set; }
                public string AgentRefNo { get; set; }
                public string PassengerPhone { get; set; }
                public string PassengerEmail { get; set; }
                public string TotalAdults { get; set; }
                public string TotalRooms { get; set; }
                public string SelectedNights { get; set; }
                public List<RoomDetail> RoomDetail { get; set; }
            }
            public class RoomDetail
            {
                public string RoomTypeDescription { get; set; }
                public string NumberOfRoom { get; set; }
                public string NumberOfAdults { get; set; }
                public string NumberOfChild { get; set; }
                public List<Passengers> Passengers { get; set; }
            }
            public class Passengers
            {
                public string Salutation { get; set; }
                public string FirstName { get; set; }
                public string LastName { get; set; }
                public string PassengerType { get; set; }
                public string Age { get; set; }
            }
        }
        #endregion

        #region 退单
        public class CancelResp
        {
            public string Message { get; set; }
            public string MessageInfo { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
        }
        #endregion
    }
}