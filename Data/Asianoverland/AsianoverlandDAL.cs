﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;

namespace XiWan.Data.Asianoverland
{
    public class AsianoverlandDAL
    {
        string AsianoverlandUrl = CCommon.GetWebConfigValue("AsianoverlandUrl");
        string AsianoverlandUsername = CCommon.GetWebConfigValue("AsianoverlandUsername");
        string AsianoverlandPassword = CCommon.GetWebConfigValue("AsianoverlandPassword");
        string AsianoverlandCacheTime = CCommon.GetWebConfigValue("AsianoverlandCacheTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        #region 初始化本类
        private static AsianoverlandDAL _Instance;
        public static AsianoverlandDAL Instance
        {
            get
            {
                return new AsianoverlandDAL();
            }
        }
        #endregion

        /// <summary>
        /// 从接口获取国家信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetCountry()
        {
            string respData = string.Empty;
            string url = AsianoverlandUrl + string.Format("index.php?action=get_country&username={0}&password={1}&gzip=no", AsianoverlandUsername, AsianoverlandPassword);
            try
            {
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取国家 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取国家,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                Dictionary<string, object> dic = Common.Common.JsonToDictionary(respData);//将Json数据转成dictionary格式
                foreach (var item in dic)
                {
                    AsianoverlandCountry obj = new AsianoverlandCountry();
                    var subItem = (Dictionary<string, object>)item.Value;
                    foreach (var str in subItem)
                    {
                        string strValue = str.Value.ToString();
                        string strKey = str.Key;
                        if (strKey == "Code")
                        {
                            obj.CountryCode = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        }
                        else if (strKey == "Name")
                        {
                            obj.CountryName = string.IsNullOrEmpty(strValue) ? "" : strValue;
                        }
                    }
                    con.Save(obj);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取国家 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        /// <summary>
        /// 从接口获取市信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetCity()
        {
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                string sql = string.Format(@"select * from AsianoverlandCountry WITH(NOLOCK) ");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        url = AsianoverlandUrl + string.Format("index.php?action=getcity&username={0}&password={1}&country={2}&gzip=no", AsianoverlandUsername, AsianoverlandPassword, dt.Rows[i]["CountryCode"]);
                        try
                        {
                            respData = Common.Common.GetHttp(url);
                            LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取市 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Asianoverland接口");
                        }
                        catch (Exception ex)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取市,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                            return;
                        }
                        AsianoverlandReq.AsianoverlandCity cityInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandReq.AsianoverlandCity>(respData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (cityInfo != null)
                        {
                            for (int j = 0; j < cityInfo.CityInfo.Count; j++)
                            {
                                AsianoverlandCity obj = new AsianoverlandCity();
                                obj.CityCode = string.IsNullOrEmpty(cityInfo.CityInfo[j].CityCode) ? "" : cityInfo.CityInfo[j].CityCode;
                                obj.CityName = string.IsNullOrEmpty(cityInfo.CityInfo[j].Name) ? "" : cityInfo.CityInfo[j].Name;
                                obj.CountryCode = string.IsNullOrEmpty(dt.Rows[i]["CountryCode"].ToString()) ? "" : dt.Rows[i]["CountryCode"].ToString();
                                obj.CountryName = string.IsNullOrEmpty(dt.Rows[i]["CountryName"].ToString()) ? "" : dt.Rows[i]["CountryName"].ToString();
                                con.Save(obj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取市 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        /// <summary>
        /// 从接口获取国籍信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetNationality()
        {
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                url = AsianoverlandUrl + string.Format("index.php?action=get_nationalities&username={0}&password={1}&gzip=no", AsianoverlandUsername, AsianoverlandPassword);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取国籍 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取国籍,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                AsianoverlandReq.AsianoverlandNationality nationalityInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandReq.AsianoverlandNationality>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (nationalityInfo != null)
                {
                    for (int i = 0; i < nationalityInfo.NationalitiesInfo.Count; i++)
                    {
                        AsianoverlandNationality obj = new AsianoverlandNationality();
                        obj.NationalityCode = string.IsNullOrEmpty(nationalityInfo.NationalitiesInfo[i].Code) ? "" : nationalityInfo.NationalitiesInfo[i].Code;
                        obj.NationalityName = string.IsNullOrEmpty(nationalityInfo.NationalitiesInfo[i].Nationality) ? "" : nationalityInfo.NationalitiesInfo[i].Nationality;
                        obj.IsoCode = string.IsNullOrEmpty(nationalityInfo.NationalitiesInfo[i].IsoCode) ? "" : nationalityInfo.NationalitiesInfo[i].IsoCode;
                        con.Save(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取国籍 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetHotels()
        {
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                string sql = string.Format(@"select * from AsianoverlandCity WITH(NOLOCK) ");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        url = AsianoverlandUrl + string.Format(@"index.php?action=get_local_hotels&username={0}&password={1}&sel_country={2}&sel_city={3}&gzip=no",
                            AsianoverlandUsername, AsianoverlandPassword, dt.Rows[i]["CountryCode"], dt.Rows[i]["CityCode"]);
                        try
                        {
                            respData = Common.Common.GetHttp(url);
                            LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取酒店 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Asianoverland接口");
                        }
                        catch (Exception ex)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取酒店,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                            return;
                        }
                        AsianoverlandReq.AsianoverlandHotels hotelsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandReq.AsianoverlandHotels>(respData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (hotelsInfo != null)
                        {
                            for (int j = 0; j < hotelsInfo.LocalHotelList.Count; j++)
                            {
                                AsianoverlandHotel obj = new AsianoverlandHotel();
                                obj.HotelID = string.IsNullOrEmpty(hotelsInfo.LocalHotelList[j].Id) ? "" : hotelsInfo.LocalHotelList[j].Id;
                                obj.HotelName = string.IsNullOrEmpty(hotelsInfo.LocalHotelList[j].Name) ? "" : hotelsInfo.LocalHotelList[j].Name;
                                obj.CityCode = string.IsNullOrEmpty(dt.Rows[i]["CityCode"].ToString()) ? "" : dt.Rows[i]["CityCode"].ToString();
                                obj.CityName = string.IsNullOrEmpty(dt.Rows[i]["CityName"].ToString()) ? "" : dt.Rows[i]["CityName"].ToString();
                                obj.CountryCode = string.IsNullOrEmpty(dt.Rows[i]["CountryCode"].ToString()) ? "" : dt.Rows[i]["CountryCode"].ToString();
                                obj.CountryName = string.IsNullOrEmpty(dt.Rows[i]["CountryName"].ToString()) ? "" : dt.Rows[i]["CountryName"].ToString();
                                con.Save(obj);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取酒店 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        /// <summary>
        /// 从接口获取酒店详细信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetHoteldetails()
        {
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                string sql = string.Format(@"select * from AsianoverlandHotel WITH(NOLOCK) WHERE ID > 47446 ");
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        url = AsianoverlandUrl + string.Format(@"index.php?action=get_local_hotel_details&username={0}&password={1}&id={2}&gzip=no",
                            AsianoverlandUsername, AsianoverlandPassword, dt.Rows[i]["HotelID"]);
                        try
                        {
                            respData = Common.Common.GetHttp(url);
                            LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取酒店详细信息 ,访问地址：{0} 返回数据 ：{1} ， Time:{2} ",url, respData, DateTime.Now.ToString()), "Asianoverland接口");
                        }
                        catch (Exception ex)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取酒店详细信息,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                            return;
                        }
                        AsianoverlandReq.AsianoverlandHoteldetails hoteldetailsInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandReq.AsianoverlandHoteldetails>(respData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (hoteldetailsInfo != null)
                        {
                            string Rating=hoteldetailsInfo.Rating;
                            string Latitude=hoteldetailsInfo.Latitude;
                            string Longitude = hoteldetailsInfo.Longitude;
                            string Phone=hoteldetailsInfo.Phone;
                            string Address=hoteldetailsInfo.Address.Replace("'","''");
                            string Email = hoteldetailsInfo.Email;
                            string updateHotelPriceSql = string.Format(@"update AsianoverlandHotel set Rating='{0}',Latitude='{1}',Longitude='{2}',Phone='{3}',
Address='{4}',Email='{5}' where HotelID='{6}'",Rating, Latitude, Longitude, Phone, Address, Email, dt.Rows[i]["HotelID"].ToString());
                            con.Update(updateHotelPriceSql);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取酒店详细信息 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }

        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationalityToSql()
        {
            string sql = "select NationalityCode as ID,NationalityName as NAME from AsianoverlandNationality ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestinationToSql(string strName)
        {
            string sql = string.Format(@"SELECT DISTINCT(CityName+','+ CountryName) as name,(CityCode + ',' + CountryCode) as value FROM AsianoverlandHotel where CityName+','+ CountryName like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        
        //获取报价
        public DataTable GetHotelQuote(string hotelID, string beginTime, string endTime, List<IntOccupancyInfo> occupancy, int RoomCount, string RateplanId, string nationality, string countryResidence)
        {
            DataTable dt = new DataTable();
            //List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            string respData = string.Empty;
            string postBoby = string.Empty;
            string url = string.Empty;
            try
            {
                
                string sqlHotel = string.Format(@"select top 1 * from AsianoverlandHotel ht WITH(NOLOCK) where HotelID='{0}' ", hotelID);
                DataTable dtHotel = ControllerFactory.GetController().GetDataTable(sqlHotel);

                List<AsianoverlandReq.roomDetails> roomDetails = new List<AsianoverlandReq.roomDetails>();
                for (int i = 0; i < occupancy.Count; i++)
                {
                    AsianoverlandReq.roomDetails roomDetail = new AsianoverlandReq.roomDetails();
                    roomDetail.numberOfAdults = occupancy[i].adults;
                    roomDetail.numberOfChild = occupancy[i].children.AsTargetType<string>("");
                    roomDetail.ChildAge = occupancy[i].childAges;
                    if (occupancy[i].children == 0)
                    {
                        roomDetail.numberOfChild = null;
                    }
                    if (occupancy[i].childAges == "")
                    {
                        roomDetail.ChildAge = null;
                    }
                    roomDetails.Add(roomDetail);
                }
                
                var jsonSetting = new JsonSerializerSettings {NullValueHandling = NullValueHandling.Ignore};
                var roomDetailsJson = JsonConvert.SerializeObject(roomDetails, Formatting.Indented, jsonSetting);

                //string roomDetailsJson = Newtonsoft.Json.JsonConvert.SerializeObject(roomDetails);
                url = AsianoverlandUrl + string.Format(@"index.php");
                postBoby = string.Format(@"action=hotel_search&username={0}&password={1}&checkin_date={2}&checkout_date={3}&sel_country={4}&sel_city={5}&chk_ratings={6}&sel_nationality={7}&country_of_residence={8}&sel_currency={9}&availableonly=1&sel_hotel={10}&timeout=30&static_data=0&number_of_rooms={11}&roomDetails={12}&limit_hotel_room_type=4&gzip=no",
                    AsianoverlandUsername, AsianoverlandPassword, beginTime, endTime, dtHotel.Rows[0]["CountryCode"],dtHotel.Rows[0]["CityCode"],
                    "1.0,2.0,3.0,4.0,5.0", nationality,countryResidence, "CNY", "", RoomCount, roomDetailsJson);
                try
                {
                    respData = Common.Common.PostHttp(url, postBoby);
                    //respData = Common.Common.GetHtmlWithUtf(url + "?" + postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", url + "?" + postBoby, respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    return dt;
                }
                AsianoverlandResp.RoomTypeOutPriceResp.RoomTypePriceResp respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandResp.RoomTypeOutPriceResp.RoomTypePriceResp>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.Message == "ERROR TYPE")
                    {
                        return dt;
                    }
                    string strErr = string.Empty;
                    if (respObj.Message == "fail")
                    {
                        strErr = respObj.MessageInfo;
                        return dt;
                    }
                    HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                    for (int j = 0; j < respObj.HotelList.Count; j++)
                    {
                        for (int k = 0; k < respObj.HotelList[j].HotelProperty.Count; k++)
                        {
                            string cGuid = Common.Common.GuidToLongID();//房型id
                            string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                            HotelPrice obj = new HotelPrice();
                            obj.HotelID = hotelID;
                            obj.HotelNo = respObj.HotelList[j].HotelId;
                            obj.Platform = "Asianoverland";
                            obj.Status = "1";
                            string ClassName = string.Empty;
                            string ClassNo = string.Empty;
                            string EachRoomAdult = string.Empty;
                            string EachRoomChild = string.Empty;
                            for (int m = 0; m < respObj.HotelList[j].HotelProperty[k].RoomRates.Count; m++)
                            {
                                ClassName += respObj.HotelList[j].HotelProperty[k].RoomRates[m].RoomType + ";";
                                ClassNo += respObj.HotelList[j].HotelProperty[k].RoomRates[m].ClassUniqueId + ";";
                                EachRoomAdult += respObj.HotelList[j].HotelProperty[k].RoomRates[m].NumberOfAdults + ";";
                                EachRoomChild += respObj.HotelList[j].HotelProperty[k].RoomRates[m].NumberOfChild + ";";
                            }
                            obj.ClassName = ClassName.TrimEnd(';');
                            obj.ClassNo = ClassNo.TrimEnd(';');
                            obj.RMBprice = respObj.HotelList[j].HotelProperty[k].DisplayRoomRate.AsTargetType<decimal>(0);
                            obj.Price = respObj.HotelList[j].HotelProperty[k].DisplayRoomRate.AsTargetType<decimal>(0);
                            obj.CurrencyCode = respObj.HotelList[j].RateCurrencyCode;
                            int IsBreakfast = 0;

                            if (respObj.HotelList[j].HotelProperty[k].RoomRates[0].MealBasis.ToLower().Contains("breakfast"))
                            {
                                IsBreakfast = 1;
                            }
                            obj.IsBreakfast = IsBreakfast;
                            obj.ReferenceClient = respObj.HotelList[j].HotelProperty[k].SectionUniqueId;
                            //obj.RoomsLeft = respObj[k].rooms[l].room_classes[m].samples[n].rooms_left;
                            obj.RoomCount = RoomCount;
                            /*obj.Adults = adults;
                            obj.Childs = children;
                            obj.ChildsAge = childAges;*/
                            obj.EachRoomAdult = EachRoomAdult.TrimEnd(';');
                            obj.EachRoomChild = EachRoomChild.TrimEnd(';');
                            obj.RoomAdults = respObj.HotelList[j].HotelProperty[k].RoomRates[0].NumberOfAdults;
                            obj.RoomChilds = respObj.HotelList[j].HotelProperty[k].RoomRates[0].NumberOfChild.AsTargetType<int>(0);
                            string[] newArrBeginTime = beginTime.Split('/');
                            string newBeginTime = newArrBeginTime[2] + "-" + newArrBeginTime[1] + "-" + newArrBeginTime[0];
                            string[] newArrEndTime = endTime.Split('/');
                            string newEndTime = newArrEndTime[2] + "-" + newArrEndTime[1] + "-" + newArrEndTime[0];
                            obj.CheckInDate = newBeginTime.AsTargetType<DateTime>(DateTime.MinValue);
                            obj.CheckOutDate = newEndTime.AsTargetType<DateTime>(DateTime.MinValue);
                            obj.SeachID = respObj.SearchUniqueId;
                            obj.CGuID = cGuid;
                            obj.RateplanId = GuidRateplanId;
                            string PriceBreakdownDate = string.Empty;
                            string PriceBreakdownPrice = string.Empty;
                            for (int m = 0; m < respObj.HotelList[j].HotelProperty[k].RoomRates[0].RateBreakup.Count; m++)
                            {
                                string[] arrdate = respObj.HotelList[j].HotelProperty[k].RoomRates[0].RateBreakup[m].Date.Split('-');
                                string newdate = arrdate[2] + "-" + arrdate[1] + "-" + arrdate[0];
                                PriceBreakdownDate += newdate + ",";
                                PriceBreakdownPrice += respObj.HotelList[j].HotelProperty[k].RoomRates[0].RateBreakup[m].DisplayNightlyRate + ",";
                            }
                            obj.PriceBreakdownDate = PriceBreakdownDate.TrimEnd(',');
                            obj.PriceBreakdownPrice = PriceBreakdownPrice.TrimEnd(',');
                            obj.IsClose = 1;
                            obj.CreateTime = DateTime.Now;
                            obj.UpdateTime = DateTime.Now;
                            obj.EntityState = e_EntityState.Added;
                            objHotelPriceCol.Add(obj);
                        }
                    }
                    string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' and  Platform = 'Asianoverland'", hotelID);
                    con.Update(updateHotelPriceSql);
                    con.Save(objHotelPriceCol);
                    string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'Asianoverland' and IsClose=1", hotelID);
                    dt = ControllerFactory.GetController().GetDataTable(sql);
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Asianoverland获取报价 ,Err :{0} ， Time:{1} ", "获取报价json解析出错", DateTime.Now.ToString()), "Asianoverland接口");
                    return dt;
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                return dt;
            }
            //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            return dt;
        }
        //获取取消政策
        public string GetHotelCancellationPolicy(string HotelID,string RateplanId)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' AND RateplanId='{1}' and  Platform = 'Asianoverland'", HotelID, RateplanId);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                url = AsianoverlandUrl + string.Format(@"index.php?action=hotel_cancellation_policy&username={0}&password={1}&hotel_id={2}&unique_id={3}&section_unique_id={4}&gzip=no",
AsianoverlandUsername, AsianoverlandPassword, dt.Rows[0]["HotelNo"], dt.Rows[0]["SeachID"], dt.Rows[0]["ReferenceClient"]);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取取消政策 ,访问地址：{0} 返回数据 ：{1} ， Time:{2} ", url, respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取取消政策,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                }
                AsianoverlandResp.CancellationPolicyResp.CancellationPolicy respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandResp.CancellationPolicyResp.CancellationPolicy>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.Message == "ERROR TYPE")
                    {
                        resp = new Respone() { code = "99", mes = "获取取消政策失败" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string strErr = string.Empty;
                    if (respObj.Message == "fail")
                    {
                        strErr = respObj.MessageInfo;
                        resp = new Respone() { code = "99", mes = "获取取消政策失败" + strErr };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    if (respObj.BookingAllowedInfo.SoldOut.ToLower() == "no" && respObj.BookingAllowedInfo.BookingAllowed.ToLower() == "yes")
                    {
                        string TotalBookingAmount = respObj.TotalBookingAmount;
                        string updateHotelPriceSql = string.Format(@"update HotelPrice set RMBprice='{0}',Price='{0}',UpdateTime=GETDATE(),CheckID=1 
where HotelID='{1}' and  Platform = 'Asianoverland' and RateplanId='{2}'",TotalBookingAmount, HotelID, RateplanId);
                        con.Update(updateHotelPriceSql);
                        string price = respObj.ContractComment;
                        if (respObj.ContractComment == null || respObj.ContractComment == "")
                        {
                            price = "0";
                        }
                        resp = new Respone() { code = "00", mes = "在日期之前：" + Convert.ToDateTime(dt.Rows[0]["CheckInDate"]).AddDays(-Math.Ceiling((respObj.CancellationHours / 24).AsTargetType<double>(0))).ToString("yyyy-MM-dd") + "可退订;" + "退订付款金额为：" + price };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "查询失败,不可以预定" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Asianoverland获取取消政策 ,Err :{0} ， Time:{1} ", "获取取消政策json解析出错", DateTime.Now.ToString()), "Asianoverland接口");
                    resp = new Respone() { code = "99", mes = "查询失败,不可以预定"  };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取取消政策 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询失败,不可以预定" };
                return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //取消
        public string cancel(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                url = AsianoverlandUrl + string.Format(@"index.php?action=cancel_the_booking&username={0}&password={1}&booking_id={2}&gzip=no", AsianoverlandUsername, AsianoverlandPassword, bookingCode);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland退单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", url, respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                AsianoverlandResp.CancelResp respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandResp.CancelResp>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.Message == "ERROR TYPE")
                    {
                        resp = new Respone() { code = "99", mes = "取消失败" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string strErr = string.Empty;
                    if (respObj.Message == "fail")
                    {
                        strErr = respObj.MessageInfo;
                        LogHelper.DoOperateLog(string.Format("Err：Asianoverland退单 ,Err :{0} ， Time:{1} ", strErr, DateTime.Now.ToString()), "Asianoverland接口");
                        resp = new Respone() { code = "99", mes = "退单失败" + strErr };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "取消订单成功" };
                        string strUpdateSql = string.Format("update Booking set UpdateTime=GETDATE(),Status=0,StatusName='订单已取消' where BookingCode='{0}'  and Platform='Asianoverland' and hotelID='{1}'"
                            , bookingCode, hotelID);
                        con.Update(strUpdateSql);
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Asianoverland取消订单 ,Err :{0} ， Time:{1} ", "取消订单json解析出错", DateTime.Now.ToString()), "Asianoverland接口");
                    resp = new Respone() { code = "99", mes = "取消订单失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        //查询订单
        public string AsianoverlandCheckingOrder(string hotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string url = string.Empty;
            string postBoby = string.Empty;
            try
            {
                url = AsianoverlandUrl + string.Format(@"index.php?action=booking_detail&username={0}&password={1}&booking_id={2}&gzip=no", AsianoverlandUsername, AsianoverlandPassword, bookingCode);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland查询订单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", url, respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                AsianoverlandResp.CheckingOrderResp.CheckingOrder respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandResp.CheckingOrderResp.CheckingOrder>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.Message == "ERROR TYPE")
                    {
                        resp = new Respone() { code = "99", mes = "查询失败" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string strErr = string.Empty;
                    if (respObj.Message == "fail")
                    {
                        strErr = respObj.MessageInfo;
                        LogHelper.DoOperateLog(string.Format("Err：Asianoverland查询订单 ,Err :{0} ， Time:{1} ", strErr, DateTime.Now.ToString()), "Asianoverland接口");
                        resp = new Respone() { code = "99", mes = "查询订单" + strErr };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string sql = string.Format(@"select top 1 * from Booking WITH(NOLOCK) WHERE HotelID='{0}' AND BookingCode='{1}' and  Platform = 'Asianoverland'", hotelID, bookingCode);
                    DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                    resp = new Respone() { code = "00", orderNum = bookingCode, orderTotal = dt.Rows[0]["RMBprice"].AsTargetType<decimal>(0), mes = "查询订单,该订单状态为:" + dt.Rows[0]["StatusName"] };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Asianoverland查询订单 ,Err :{0} ， Time:{1} ", "查询订单json解析出错", DateTime.Now.ToString()), "Asianoverland接口");
                    resp = new Respone() { code = "99", mes = "查询订单失败"  };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //查询订单
        public string AsianoverlandBookingDetail(string hotelID, string AgentBookingReference)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string url = string.Empty;
            string postBoby = string.Empty;
            try
            {
                url = AsianoverlandUrl + string.Format(@"index.php?action=booking_detail&username={0}&password={1}&agent_ref_no={2}&gzip=no", AsianoverlandUsername, AsianoverlandPassword, AgentBookingReference);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland查询订单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                AsianoverlandResp.CheckingOrderResp.CheckingOrder respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandResp.CheckingOrderResp.CheckingOrder>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.Message == "ERROR TYPE")
                    {
                        resp = new Respone() { code = "99", mes = "查询失败" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string strErr = string.Empty;
                    if (respObj.Message == "fail")
                    {
                        strErr = respObj.MessageInfo;
                        LogHelper.DoOperateLog(string.Format("Err：Asianoverland查询订单 ,Err :{0} ， Time:{1} ", strErr, DateTime.Now.ToString()), "Asianoverland接口");
                        resp = new Respone() { code = "99", mes = "查询订单" + strErr };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string sql = string.Format(@"select top 1 * from Booking WITH(NOLOCK) WHERE HotelID='{0}' AND AgentBookingReference='{1}' and  Platform = 'Asianoverland'", hotelID, AgentBookingReference);
                    DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                    resp = new Respone() { code = "00", orderNum = dt.Rows[0]["BookingCode"].AsTargetType<string>(""), orderTotal = dt.Rows[0]["RMBprice"].AsTargetType<decimal>(0), mes = "查询订单,该订单状态为:" + dt.Rows[0]["StatusName"] };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Asianoverland查询订单 ,Err :{0} ， Time:{1} ", "查询订单json解析出错", DateTime.Now.ToString()), "Asianoverland接口");
                    resp = new Respone() { code = "99", mes = "查询订单失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland查询订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //预定
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                string guID = Common.Common.getGUID();
                string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' AND RateplanId='{1}' and  Platform = 'Asianoverland'", HotelID, RateplanId);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0 && dt.Rows[0]["CheckID"].AsTargetType<string>("0") == "1")
                {
                    string[] EachRoomAdult = dt.Rows[0]["EachRoomAdult"].ToString().Split(';');
                    string[] EachRoomChild = dt.Rows[0]["EachRoomChild"].ToString().Split(';');
                    string[] roomClassId = dt.Rows[0]["ClassNo"].ToString().Split(';');
                    List<AsianoverlandResp.BookingResp.roomDetails> roomDetails = new List<AsianoverlandResp.BookingResp.roomDetails>();
                    var scoreList = new List<int>();//输入客户对应的排列数
                    for (int i = 0; i < customers.Count; i++)
                    {
                        int Childs = 0;//输入客户信息时的客户儿童数
                        int Adults = 0;//输入客户信息时的客户成人数
                        AsianoverlandResp.BookingResp.roomDetails roomDetail = new AsianoverlandResp.BookingResp.roomDetails();
                        List<AsianoverlandResp.BookingResp.passangers> passangers = new List<AsianoverlandResp.BookingResp.passangers>();
                        for (int j = 0; j < customers[i].Customers.Count; j++)
                        {
                            AsianoverlandResp.BookingResp.passangers passanger = new AsianoverlandResp.BookingResp.passangers();
                            if (customers[i].Customers[j].age < 13 && customers[i].Customers[j].age!=0)
                            {
                                passanger.salutation = "Child";
                                passanger.age = customers[i].Customers[j].age.AsTargetType<string>("0");
                                Childs = Childs + 1;
                            }
                            else
                            {
                                passanger.salutation = "MISS";
                                passanger.age = null;
                                if (customers[i].Customers[j].sex == 1)
                                {
                                    passanger.salutation = "MR";
                                    Adults = Adults + 1;
                                }
                                else if (customers[i].Customers[j].sex == 2)//测试用
                                {
                                    passanger.salutation = "Mrs";
                                    Adults = Adults + 1;
                                }
                                else if (customers[i].Customers[j].sex == 3)//测试用
                                {
                                    passanger.salutation = "Child";
                                    Childs = Childs + 1;
                                }
                                else
                                {
                                    Adults = Adults + 1;
                                }
                            }
                            passanger.first_name = customers[i].Customers[j].firstName;
                            passanger.last_name = customers[i].Customers[j].lastName;
                            passangers.Add(passanger);
                        }
                        int num = 0;//输入客户对应的排列数
                        //int roomAdults = 0;//搜索时房间成人数
                        //int roomChilds = 0;//搜索时房间儿童数
                        for(int j=0;j<EachRoomAdult.Length; j++)
                        {
                            if (EachRoomAdult[j].AsTargetType<int>(0) == Adults)
                            {
                                if (EachRoomChild[j].AsTargetType<int>(0) == Childs)
                                {
                                    if (!scoreList.Exists(o => o == j))
                                    {
                                        scoreList.Add(j);
                                        num = j;
                                        break;
                                    }
                                }
                            }
                        }
                        roomDetail.numberOfChilds = EachRoomChild[num].AsTargetType<string>("0");
                        roomDetail.roomClassId = roomClassId[num].AsTargetType<string>("");
                        roomDetail.passangers = passangers;
                        roomDetails.Add(roomDetail);
                    }
                    var jsonSetting = new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore };
                    var roomDetailsJson = JsonConvert.SerializeObject(roomDetails, Formatting.Indented, jsonSetting);

                    url = AsianoverlandUrl + string.Format(@"index.php?action=hotel_reservation&username={0}&password={1}&hotel_id={2}&unique_id={3}&section_unique_id={4}&agent_ref_no={5}&roomDetails={6}&expected_price={7}&gzip=no",
    AsianoverlandUsername, AsianoverlandPassword, dt.Rows[0]["HotelNo"], dt.Rows[0]["SeachID"], dt.Rows[0]["ReferenceClient"], guID, roomDetailsJson, dt.Rows[0]["RMBprice"]);
                    try
                    {
                        respData = Common.Common.GetHttp(url);
                        LogHelper.DoOperateLog(string.Format("Studio：Asianoverland预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", url, respData, DateTime.Now.ToString()), "Asianoverland接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:Asianoverland预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                        resp = new Respone() { code = "99", mes = "预订失败失败，" + ex.ToString() };
                    }
                    AsianoverlandResp.BookingResp.Booking respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandResp.BookingResp.Booking>(respData, new JsonSerializerSettings
                    {
                        Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                        {
                            args.ErrorContext.Handled = true;
                        }
                    });
                    if (respObj != null)
                    {
                        if (respObj.Message == "ERROR TYPE")
                        {
                            resp = new Respone() { code = "99", mes = "预定失败" };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        string strErr = string.Empty;
                        if (respObj.Message == "fail")
                        {
                            strErr = respObj.MessageInfo;
                            LogHelper.DoOperateLog(string.Format("Err：Asianoverland预定 ,Err :{0} ， Time:{1} ", strErr, DateTime.Now.ToString()), "Asianoverland接口");
                            resp = new Respone() { code = "99", mes = "预定失败" + strErr };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        Booking booking = new Booking();
                        //保存订单到数据库
                        booking.BookingCode = respObj.BookingDetail.Id;//.BookingReference;
                        booking.InOrderNum = inOrderNum;
                        booking.AgentBookingReference = guID;
                        booking.RMBprice = respObj.BookingDetail.TotalCharges.AsTargetType<decimal>(0);
                        booking.ClientCurrencyCode = respObj.BookingDetail.CurrencyCode;
                        booking.ClientPartnerAmount = respObj.BookingDetail.TotalCharges.AsTargetType<decimal>(0); ;
                        booking.HotelId = HotelID;
                        booking.HotelNo = respObj.BookingDetail.Id;
                        booking.Platform = "Asianoverland";
                        booking.StatusCode = "";
                        booking.StatusName = "预订成功";
                        booking.ServiceID = 0;
                        booking.ServiceName = "";
                        booking.LegID = 0;
                        booking.Leg_Status = "";
                        booking.Status = 1;
                        booking.CreatTime = DateTime.Now;
                        booking.UpdateTime = DateTime.Now;
                        booking.EntityState = e_EntityState.Added;
                        con.Save(booking);
                        resp = new Respone() { code = "00", orderNum = respObj.BookingDetail.BookingReference, orderTotal = respObj.BookingDetail.TotalCharges.AsTargetType<decimal>(0), mes = "预订成功" };
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err：Asianoverland预订 ,Err :{0} ， Time:{1} ", "预订json解析出错", DateTime.Now.ToString()), "Asianoverland接口");
                        resp = new Respone() { code = "99", mes = "预订失败" };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "预订失败，请先查看取消日期" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //获取取消费用
        public string GetCancellationFee(string HotelID, string bookingCode)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                url = AsianoverlandUrl + string.Format(@"index.php?action=get_cancellation_charges&username={0}&password={1}&booking_id={2}&gzip=no",
AsianoverlandUsername, AsianoverlandPassword, bookingCode);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取取消费用 ,访问地址：{0} 返回数据 ：{1} ， Time:{2} ", url, respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取取消费用,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                }
                AsianoverlandResp.CancellationPolicyResp.CancellationFee respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandResp.CancellationPolicyResp.CancellationFee>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (respObj != null)
                {
                    if (respObj.Message == "ERROR TYPE")
                    {
                        resp = new Respone() { code = "99", mes = "获取取消费用失败" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string strErr = string.Empty;
                    if (respObj.Message == "fail")
                    {
                        strErr = respObj.MessageInfo;
                        LogHelper.DoOperateLog(string.Format("Err：Asianoverland获取取消费用 ,Err :{0} ， Time:{1} ", strErr, DateTime.Now.ToString()), "Asianoverland接口");
                        resp = new Respone() { code = "99", mes = "获取取消费用失败" + strErr };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    string allowCancel = "不可以取消,";
                    if (respObj.AllowCancel.ToLower() == "yes")
                    {
                        allowCancel = "该订单可以取消,";
                    }
                    resp = new Respone() { code = "00", mes = allowCancel + "取消费用为：" + respObj.CancellationCharge };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Asianoverland获取取消费用 ,Err :{0} ， Time:{1} ", "获取取消费用json解析出错", DateTime.Now.ToString()), "Asianoverland接口");
                    resp = new Respone() { code = "99", mes = "获取取消费用失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取取消费用 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "获取取消费用失败," + ex.ToString()};
                return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
    }
}