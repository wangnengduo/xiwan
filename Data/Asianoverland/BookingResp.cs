﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class BookingResp
    {
        public class Booking
        {
            public string Message { get; set; }
            public string BookingServiceType { get; set; }
            public BookingDetail BookingDetail { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string MessageInfo { get; set; }
        }
        public class BookingDetail
        {
            public string Id { get; set; }
            public string BookingId { get; set; }
            public string AgentId { get; set; }
            public string BookingReference { get; set; }
            public string LeaderTitle { get; set; }
            public string LeaderFirstName { get; set; }
            public string LeaderLastName { get; set; }
            public string TotalCharges { get; set; }
            public string CurrencyCode { get; set; }
            public string HotelName { get; set; }
            public string CountryName { get; set; }
            public string CityId { get; set; }
            public string HotelAddress1 { get; set; }
            public string CurrentStatus { get; set; }
            public DateTime BookingDate { get; set; }
            public DateTime ExpirationDate { get; set; }
            public string NoOfAdult { get; set; }
            public string NoOfChild { get; set; }
            public string TotalRooms { get; set; }
            public string HotelAddress { get; set; }
            public DateTime CheckInDate { get; set; }
            public DateTime CheckOutDate { get; set; }
            public string AgentRate { get; set; }
            public string HotelPhone { get; set; }
            public string HotelRating { get; set; }
            public List<RoomDetail> RoomDetail { get; set; }
        }
        public class RoomDetail
        {
            public string RoomTypeDescription { get; set; }
            public List<Passengers> Passengers { get; set; }
        }
        public class Passengers
        {
            public string Salutations { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PassengerType { get; set; }
            public string Age { get; set; }
        }

        public class roomDetails
        {
            public string numberOfChilds { get; set; }
            public string roomClassId { get; set; }
            public List<passangers> passangers { get; set; }
        }
        public class passangers
        {
            public string salutation { get; set; }
            public string first_name { get; set; }
            public string last_name { get; set; }
            public string age { get; set; }
        }
    }
}