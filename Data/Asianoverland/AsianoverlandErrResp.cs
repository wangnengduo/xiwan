﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class AsianoverlandErrResp
    {
        public string Message { get; set; }
        public string MessageInfo { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
    }
}