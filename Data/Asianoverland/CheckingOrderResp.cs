﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class CheckingOrderResp
    {
        public class CheckingOrder
        {
            public string Message { get; set; }
            public string BookingServiceType { get; set; }
            public BookingDetail BookingDetail { get; set; }
            public DateTime StartTime { get; set; }
            public DateTime EndTime { get; set; }
            public string MessageInfo { get; set; }
        }
        public class BookingDetail
        {
            public string Id { get; set; }
            public string BookingReference { get; set; }
            public string LeaderTitle { get; set; }
            public string LeaderFirstName { get; set; }
            public string LeaderLastName { get; set; }
            public string CurrencyCode { get; set; }
            public string LocalHotelId { get; set; }
            public string HotelName { get; set; }
            public string CountryName { get; set; }
            public string CityId { get; set; }
            public string HotelAddress1 { get; set; }
            public string CurrentStatus { get; set; }
            public DateTime ExpirationDate { get; set; }
            public string TotalChildren { get; set; }
            public DateTime CheckInDate { get; set; }
            public DateTime CheckOutDate { get; set; }
            public string AgentRate { get; set; }
            public string AgentRefNo { get; set; }
            public string PassengerPhone { get; set; }
            public string PassengerEmail { get; set; }
            public string TotalAdults { get; set; }
            public string TotalRooms { get; set; }
            public string SelectedNights { get; set; }
            public List<RoomDetail> RoomDetail { get; set; }
        }
        public class RoomDetail
        {
            public string RoomTypeDescription { get; set; }
            public string NumberOfRoom { get; set; }
            public string NumberOfAdults { get; set; }
            public string NumberOfChild { get; set; }
            public List<Passengers> Passengers { get; set; }
        }
        public class Passengers
        {
            public string Salutation { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string PassengerType { get; set; }
            public string Age { get; set; }
        }
    }
}