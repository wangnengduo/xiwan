﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Asianoverland
{
    public class RoomTypeOutPriceResp
    {
        public RoomTypePriceResp RoomTypePrice { get; set; }
        public class RoomTypePriceResp
        {
            public int TotalCount { get; set; }
            public string WebServiceVersion { get; set; }
            public string Message { get; set; }
            public List<HotelList> HotelList { get; set; }
            public string SearchUniqueId { get; set; }
            public string StartTime { get; set; }
            public string EndTime { get; set; }
            public string MessageInfo { get; set; }
        }
        public class HotelList
        {
            public string HotelId { get; set; }
            public string HotelName { get; set; }
            public string LocalHotelId { get; set; }
            public string PropertyRating { get; set; }
            public string Available { get; set; }
            public string RateCurrencyCode { get; set; }
            public double TotalCharges { get; set; }
            public List<HotelProperty> HotelProperty { get; set; }
        }
        public class HotelProperty
        {
            public double DisplayRoomRate { get; set; }
            public string Type { get; set; }
            public string SectionUniqueId { get; set; }
            public List<string> RoomDetails { get; set; }
            public List<RoomRates> RoomRates { get; set; }
        }
        public class RoomRates
        {
            public int Available { get; set; }
            public int NumberOfRooms { get; set; }
            public int NumberOfAdults { get; set; }
            public string NumberOfChild { get; set; }
            public double RoomRate { get; set; }
            public string RoomType { get; set; }
            public string RoomCategory { get; set; }
            public string RatePlanCode { get; set; }
            public string available_inventory { get; set; }
            public string inventoryClass { get; set; }
            public string RoomTypeCode { get; set; }
            public string MealBasis { get; set; }
            public string Note { get; set; }
            public string ClassUniqueId { get; set; }
            public List<RateBreakup> RateBreakup { get; set; }
        }
        public class RateBreakup
        {
            public string Date { get; set; }
            public string Day { get; set; }
            public double DisplayNightlyRate { get; set; }
        }
    }
}