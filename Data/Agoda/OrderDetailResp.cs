﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Agoda
{
    public class OrderDetailResp
    {
        public class BookingDetailsResponseV2
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "Bookings")]
            public Bookings Bookings { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class ErrorMessages
        {
            [XmlElement(ElementName = "ErrorMessage")]
            public ErrorMessage ErrorMessage { get; set; }
        }
        public class ErrorMessage
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("subid")]
            public string subid { get; set; }
        }
        public class Bookings
        {
            [XmlElement(ElementName = "Booking")]
            public List<Booking> Booking { get; set; }
        }
        public class Booking
        {
            [XmlElement(ElementName = "BookingID")]
            public string BookingID { get; set; }
            [XmlElement(ElementName = "Tag")]
            public string Tag { get; set; }
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "Country")]
            public string Country { get; set; }
            [XmlElement(ElementName = "City")]
            public string City { get; set; }
            [XmlElement(ElementName = "Hotel")]
            public string Hotel { get; set; }
            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }
            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
            [XmlElement(ElementName = "Room")]
            public Room Room { get; set; }
            public TotalRateUSD TotalRateUSD { get; set; }
            [XmlElement(ElementName = "Payment")]
            public Payment Payment { get; set; }
            [XmlElement(ElementName = "GuestDetails")]
            public GuestDetails GuestDetails { get; set; }
            [XmlElement(ElementName = "SelfServiceURL")]
            public string SelfServiceURL { get; set; }
            [XmlElement(ElementName = "Source")]
            public string Source { get; set; }
            [XmlElement(ElementName = "SupplierName")]
            public string SupplierName { get; set; }
            [XmlElement(ElementName = "SupplierReference")]
            public string SupplierReference { get; set; }
            [XmlElement(ElementName = "BookNowPayLaterDate")]
            public string BookNowPayLaterDate { get; set; }
        }
        public class Room
        {
            [XmlElement(ElementName = "RoomType")]
            public string RoomType { get; set; }
            [XmlElement(ElementName = "RoomsBooked")]
            public string RoomsBooked { get; set; }
        }
        public class TotalRateUSD
        {
            [XmlAttribute("rateplan")]
            public string rateplan { get; set; }
            [XmlAttribute("ratetype")]
            public string ratetype { get; set; }
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class Payment
        {
            [XmlElement(ElementName = "PaymentRateInclusive")]
            public List<PaymentRateInclusive> PaymentRateInclusive { get; set; }
        }
        public class PaymentRateInclusive
        {
            [XmlAttribute("currency")]
            public string currency { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class GuestDetails
        {
            [XmlElement(ElementName = "GuestDetail")]
            public List<GuestDetail> GuestDetail { get; set; }
        }
        public class GuestDetail
        {
            [XmlAttribute("Primary")]
            public string Primary { get; set; }
            [XmlElement(ElementName = "Title")]
            public string Title { get; set; }
            [XmlElement(ElementName = "FirstName")]
            public string FirstName { get; set; }
            [XmlElement(ElementName = "LastName")]
            public string LastName { get; set; }
            [XmlElement(ElementName = "CountryOfPassport")]
            public string CountryOfPassport { get; set; } 
        }
    }
}