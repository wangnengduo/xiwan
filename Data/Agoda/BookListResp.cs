﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Agoda
{
    public class BookListResp
    {
        public class BookingListResponseV2
        {
            [XmlElement(ElementName = "Bookings")]
            public Bookings Bookings { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class ErrorMessages
        {
            [XmlElement(ElementName = "ErrorMessage")]
            public ErrorMessage ErrorMessage { get; set; }
        }
        public class ErrorMessage
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("subid")]
            public string subid { get; set; }
        }
        public class Bookings
        {
            [XmlElement(ElementName = "Booking")]
            public List<Booking> Booking { get; set; }
        }
        public class Booking
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlAttribute("tag")]
            public string tag { get; set; }
            [XmlAttribute("hotelid")]
            public string hotelid { get; set; }
            [XmlAttribute("hotelname")]
            public string hotelname { get; set; }
            [XmlAttribute("cityname")]
            public string cityname { get; set; }
            [XmlAttribute("received")]
            public string received { get; set; }
            [XmlAttribute("lastmodified")]
            public string lastmodified { get; set; }
            [XmlAttribute("arrival")]
            public string arrival { get; set; }
            [XmlAttribute("departure")]
            public string departure { get; set; }
            [XmlAttribute("usdamount")]
            public string usdamount { get; set; }
            [XmlAttribute("selfservice")]
            public string selfservice { get; set; }
        }
    }
}