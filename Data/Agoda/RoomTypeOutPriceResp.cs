﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Agoda
{
    public class RoomTypeOutPriceResp
    {
        public class AvailabilityLongResponseV2
        {
            [XmlAttribute("subid")]
            public string subid { get; set; }
            [XmlAttribute("searchid")]
            public string searchid { get; set; }
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "Hotels")]
            public Hotels Hotels { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class ErrorMessages
        {
            [XmlElement(ElementName = "ErrorMessage")]
            public ErrorMessage ErrorMessage { get; set; }
        }
        public class ErrorMessage
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("subid")]
            public string subid { get; set; }
        }
        public class Hotels
        {
            [XmlElement(ElementName = "Hotel")]
            public List<Hotel> Hotel { get; set; }
        }
        public class Hotel
        {
            [XmlElement(ElementName = "Id")]
            public string Id { get; set; }
            [XmlElement(ElementName = "CheapestRoom")]
            public CheapestRoom CheapestRoom { get; set; }
            [XmlElement(ElementName = "Rooms")]
            public Rooms Rooms { get; set; }
        }
        public class CheapestRoom
        {
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class Rooms
        {
            [XmlElement(ElementName = "Room")]
            public List<Room> Room { get; set; }
        }
        public class Room
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("promotionid")]
            public string promotionid { get; set; }
            [XmlAttribute("name")]
            public string name { get; set; }
            [XmlAttribute("lineitemid")]
            public string lineitemid { get; set; }
            [XmlAttribute("rateplan")]
            public string rateplan { get; set; }
            [XmlAttribute("ratetype")]
            public string ratetype { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
            [XmlAttribute("model")]
            public string model { get; set; }
            [XmlAttribute("ratecategoryid")]
            public string ratecategoryid { get; set; }
            [XmlAttribute("blockid")]
            public string blockid { get; set; }
            [XmlAttribute("booknowpaylaterdate")]
            public string booknowpaylaterdate { get; set; }

            [XmlElement(ElementName = "StandardTranslation")]
            public string StandardTranslation { get; set; }
            [XmlElement(ElementName = "Benefits")]
            public Benefits Benefits { get; set; }
            [XmlElement(ElementName = "MaxRoomOccupancy")]
            public MaxRoomOccupancy MaxRoomOccupancy { get; set; }
            [XmlElement(ElementName = "RemainingRooms")]
            public string RemainingRooms { get; set; }
            [XmlElement(ElementName = "RateInfo")]
            public RateInfo RateInfo { get; set; }
            [XmlElement(ElementName = "Cancellation")]
            public Cancellation Cancellation { get; set; }
            [XmlElement(ElementName = "LandingURL")]
            public string LandingURL { get; set; }
        }
        public class Benefits
        {
            [XmlElement(ElementName = "Benefit")]
            public List<Benefit> Benefit { get; set; }
        }
        public class Benefit
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Translation")]
            public string Translation { get; set; }
        }
        public class MaxRoomOccupancy
        {
            [XmlAttribute("normalbedding")]
            public string normalbedding { get; set; }
            [XmlAttribute("extrabeds")]
            public string extrabeds { get; set; }
        }
        public class RateInfo
        {
            [XmlElement(ElementName = "Rate")]
            public Rate Rate { get; set; }
            [XmlElement(ElementName = "Promotion")]
            public Promotion Promotion { get; set; }
            [XmlElement(ElementName = "DailyRates")]
            public DailyRates DailyRates { get; set; }
            [XmlElement(ElementName = "TaxServiceBreakdown")]
            public TaxServiceBreakdown TaxServiceBreakdown { get; set; }
            [XmlElement(ElementName = "Included")]
            public string Included { get; set; }
            [XmlElement(ElementName = "Surcharges")]
            public Surcharges Surcharges { get; set; }
        }
        public class Rate
        {
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class Surcharges
        {
            [XmlElement(ElementName = "Surcharge")]
            public List<Surcharge> Surcharge { get; set; }
        }
        public class Surcharge
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("method")]
            public string method { get; set; }
            [XmlAttribute("charge")]
            public string charge { get; set; }
            [XmlAttribute("margin")]
            public string margin { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Rate")]
            public Rate Rate { get; set; }
        }
        public class Promotion
        {
            [XmlAttribute("savings")]
            public string savings { get; set; }
            [XmlAttribute("text")]
            public string text { get; set; }
        }
        public class DailyRates
        {
            [XmlElement(ElementName = "DailyRateDate")]
            public List<DailyRateDate> DailyRateDate { get; set; }
        }
        public class DailyRateDate
        {
            [XmlAttribute("date")]
            public string date { get; set; }
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class TaxServiceBreakdown
        {
            [XmlElement(ElementName = "TaxFee")]
            public List<TaxFee> TaxFee { get; set; }
        }
        public class TaxFee
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("type")]
            public string type { get; set; }
            [XmlAttribute("description")]
            public string description { get; set; }
            [XmlAttribute("translation")]
            public string translation { get; set; }
            [XmlAttribute("method")]
            public string method { get; set; }
            [XmlAttribute("base")]
            public string baseDesc { get; set; }
            [XmlAttribute("taxable")]
            public string taxable { get; set; }
            [XmlAttribute("percent")]
            public string percent { get; set; }
            [XmlAttribute("amount")]
            public string amount { get; set; }
            [XmlAttribute("total")]
            public string total { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
        }
        public class Cancellation
        {
            [XmlElement(ElementName = "PolicyText")]
            public PolicyText PolicyText { get; set; }
            [XmlElement(ElementName = "PolicyTranslated")]
            public PolicyTranslated PolicyTranslated { get; set; }
            [XmlElement(ElementName = "PolicyParameters")]
            public PolicyParameters PolicyParameters { get; set; }
            [XmlElement(ElementName = "PolicyDates")]
            public PolicyDates PolicyDates { get; set; }
        }
        public class PolicyText
        {
            [XmlAttribute("language")]
            public string language { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class PolicyTranslated
        {
            [XmlAttribute("language")]
            public string language { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class PolicyParameters
        {
            [XmlElement(ElementName = "PolicyParameter")]
            public List<PolicyParameter> PolicyParameter { get; set; }
        }
        public class PolicyParameter
        {
            [XmlAttribute("days")]
            public string days { get; set; }
            [XmlAttribute("charge")]
            public string charge { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class PolicyDates
        {
            [XmlElement(ElementName = "PolicyDate")]
            public List<PolicyDate> PolicyDate { get; set; }
        }
        public class PolicyDate
        {
            [XmlAttribute("before")]
            public string before { get; set; }
            [XmlElement(ElementName = "Rate")]
            public Rate Rate { get; set; }
        }
    }
}