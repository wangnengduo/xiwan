﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Agoda
{
    public class BookingResp
    {
        public class BookingResponseV3
        {
            [XmlElement(ElementName = "BookingDetails")]
            public BookingDetails BookingDetails { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class ErrorMessages
        {
            [XmlElement(ElementName = "ErrorMessage")]
            public ErrorMessage ErrorMessage { get; set; }
        }
        public class ErrorMessage
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("subid")]
            public string subid { get; set; }
        }
        public class BookingDetails
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "Booking")]
            public Booking Booking { get; set; }
        }
        public class Booking
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("ItineraryID")]
            public string ItineraryID { get; set; }
            [XmlAttribute("selfservice")]
            public string selfservice { get; set; }
        }

        public class GuestDetails
        {
            public List<GuestDetail> GuestDetail { get; set; }
        }
        public class GuestDetail
        { 
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string CountryOfPassport { get; set; }
            /*public string Gender { get; set; }
            public int Age { get; set; }*/
        }
    }
}