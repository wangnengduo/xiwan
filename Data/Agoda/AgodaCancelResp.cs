﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Agoda
{
    public class AgodaCancelResp
    {
        public class CancellationResponseV2
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "CancellationSummary")]
            public CancellationSummary CancellationSummary { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class ErrorMessages
        {
            public ErrorMessage ErrorMessage { get; set; }
        }
        public class ErrorMessage
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("subid")]
            public string subid { get; set; }
        }
        public class CancellationSummary
        {
            [XmlElement(ElementName = "BookingID")]
            public string BookingID { get; set; }
            [XmlElement(ElementName = "Reference")]
            public string Reference { get; set; }
            [XmlElement(ElementName = "Cancellation")]
            public Cancellation Cancellation { get; set; }
            [XmlElement(ElementName = "Payment")]
            public Payment Payment { get; set; }
            [XmlElement(ElementName = "Refund")]
            public Refund Refund { get; set; }
        }
        public class Cancellation
        {
            [XmlElement(ElementName = "PolicyText")]
            public List<PolicyText> PolicyText { get; set; }
        }
        public class PolicyText
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("language")]
            public string language { get; set; }
        }
        public class Payment
        {
            [XmlElement(ElementName = "PaymentRateInclusive")]
            public PaymentRateInclusive PaymentRateInclusive { get; set; }
        }
        public class PaymentRateInclusive
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
        }
        public class Refund
        {
            [XmlElement(ElementName = "RefundRateInclusive")]
            public RefundRateInclusive RefundRateInclusive { get; set; }
        }
        public class RefundRateInclusive
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
        }


        public class ConfirmCancellationResponseV2
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
    }
}