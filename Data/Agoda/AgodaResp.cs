﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Agoda
{
    public class AgodaResp
    {
        #region 获取报价
        public class AvailabilityLongResponseV2
        {
            [XmlAttribute("subid")]
            public string subid { get; set; }
            [XmlAttribute("searchid")]
            public string searchid { get; set; }
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "Hotels")]
            public Hotels Hotels { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class ErrorMessages
        {
            [XmlElement(ElementName = "ErrorMessage")]
            public ErrorMessage ErrorMessage { get; set; }
        }
        public class ErrorMessage
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("subid")]
            public string subid { get; set; }
        }
        public class Hotels
        {
            [XmlElement(ElementName = "Hotel")]
            public List<Hotel> Hotel { get; set; }
        }
        public class Hotel
        {
            [XmlElement(ElementName = "Id")]
            public string Id { get; set; }
            [XmlElement(ElementName = "CheapestRoom")]
            public CheapestRoom CheapestRoom { get; set; }
            [XmlElement(ElementName = "Rooms")]
            public Rooms Rooms { get; set; }
        }
        public class CheapestRoom
        {
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class Rooms
        {
            [XmlElement(ElementName = "Room")]
            public List<Room> Room { get; set; }
        }
        public class Room
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("promotionid")]
            public string promotionid { get; set; }
            [XmlAttribute("name")]
            public string name { get; set; }
            [XmlAttribute("lineitemid")]
            public string lineitemid { get; set; }
            [XmlAttribute("rateplan")]
            public string rateplan { get; set; }
            [XmlAttribute("ratetype")]
            public string ratetype { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
            [XmlAttribute("model")]
            public string model { get; set; }
            [XmlAttribute("ratecategoryid")]
            public string ratecategoryid { get; set; }
            [XmlAttribute("blockid")]
            public string blockid { get; set; }
            [XmlAttribute("booknowpaylaterdate")]
            public string booknowpaylaterdate { get; set; }

            [XmlElement(ElementName = "StandardTranslation")]
            public string StandardTranslation { get; set; }
            [XmlElement(ElementName = "ParentRoom")]
            public ParentRoom ParentRoom { get; set; }
            [XmlElement(ElementName = "Benefits")]
            public Benefits Benefits { get; set; }
            [XmlElement(ElementName = "MaxRoomOccupancy")]
            public MaxRoomOccupancy MaxRoomOccupancy { get; set; }
            [XmlElement(ElementName = "RemainingRooms")]
            public string RemainingRooms { get; set; }
            [XmlElement(ElementName = "RateInfo")]
            public RateInfo RateInfo { get; set; }
            [XmlElement(ElementName = "Cancellation")]
            public Cancellation Cancellation { get; set; }
            [XmlElement(ElementName = "LandingURL")]
            public string LandingURL { get; set; }
        }
        public class Benefits
        {
            [XmlElement(ElementName = "Benefit")]
            public List<Benefit> Benefit { get; set; }
        }
        public class Benefit
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Translation")]
            public string Translation { get; set; }
        }
        public class ParentRoom
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "translationname")]
            public string translationname { get; set; }
        }
        public class MaxRoomOccupancy
        {
            [XmlAttribute("normalbedding")]
            public string normalbedding { get; set; }
            [XmlAttribute("extrabeds")]
            public string extrabeds { get; set; }
        }
        public class RateInfo
        {
            [XmlElement(ElementName = "Rate")]
            public Rate Rate { get; set; }
            [XmlElement(ElementName = "Promotion")]
            public Promotion Promotion { get; set; }
            [XmlElement(ElementName = "DailyRates")]
            public DailyRates DailyRates { get; set; }
            [XmlElement(ElementName = "TaxServiceBreakdown")]
            public TaxServiceBreakdown TaxServiceBreakdown { get; set; }
            [XmlElement(ElementName = "Included")]
            public string Included { get; set; }
            [XmlElement(ElementName = "Surcharges")]
            public Surcharges Surcharges { get; set; }
        }
        public class Rate
        {
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class Surcharges
        {
            [XmlElement(ElementName = "Surcharge")]
            public List<Surcharge> Surcharge { get; set; }
        }
        public class Surcharge
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("method")]
            public string method { get; set; }
            [XmlAttribute("charge")]
            public string charge { get; set; }
            [XmlAttribute("margin")]
            public string margin { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "Rate")]
            public Rate Rate { get; set; }
        }
        public class Promotion
        {
            [XmlAttribute("savings")]
            public string savings { get; set; }
            [XmlAttribute("text")]
            public string text { get; set; }
        }
        public class DailyRates
        {
            [XmlElement(ElementName = "DailyRateDate")]
            public List<DailyRateDate> DailyRateDate { get; set; }
        }
        public class DailyRateDate
        {
            [XmlAttribute("date")]
            public string date { get; set; }
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class TaxServiceBreakdown
        {
            [XmlElement(ElementName = "TaxFee")]
            public List<TaxFee> TaxFee { get; set; }
        }
        public class TaxFee
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("type")]
            public string type { get; set; }
            [XmlAttribute("description")]
            public string description { get; set; }
            [XmlAttribute("translation")]
            public string translation { get; set; }
            [XmlAttribute("method")]
            public string method { get; set; }
            [XmlAttribute("base")]
            public string baseDesc { get; set; }
            [XmlAttribute("taxable")]
            public string taxable { get; set; }
            [XmlAttribute("percent")]
            public string percent { get; set; }
            [XmlAttribute("amount")]
            public string amount { get; set; }
            [XmlAttribute("total")]
            public string total { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
        }
        public class Cancellation
        {
            [XmlAttribute("code")]
            public string code { get; set; }
            [XmlElement(ElementName = "PolicyText")]
            public PolicyText PolicyText { get; set; }
            [XmlElement(ElementName = "PolicyTranslated")]
            public PolicyTranslated PolicyTranslated { get; set; }
            [XmlElement(ElementName = "PolicyParameters")]
            public PolicyParameters PolicyParameters { get; set; }
            [XmlElement(ElementName = "PolicyDates")]
            public PolicyDates PolicyDates { get; set; }
        }
        public class PolicyText
        {
            [XmlAttribute("language")]
            public string language { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class PolicyTranslated
        {
            [XmlAttribute("language")]
            public string language { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class PolicyParameters
        {
            [XmlElement(ElementName = "PolicyParameter")]
            public List<PolicyParameter> PolicyParameter { get; set; }
        }
        public class PolicyParameter
        {
            [XmlAttribute("days")]
            public string days { get; set; }
            [XmlAttribute("charge")]
            public string charge { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class PolicyDates
        {
            [XmlElement(ElementName = "PolicyDate")]
            public List<PolicyDate> PolicyDate { get; set; }
        }
        public class PolicyDate
        {
            [XmlAttribute("before")]
            public string before { get; set; }
            [XmlElement(ElementName = "Rate")]
            public Rate Rate { get; set; }
        }
        #endregion


        #region 取消政策
        public class CancellationResponseV2
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "CancellationSummary")]
            public CancellationSummary CancellationSummary { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class CancellationSummary
        {
            [XmlElement(ElementName = "BookingID")]
            public string BookingID { get; set; }
            [XmlElement(ElementName = "Reference")]
            public string Reference { get; set; }
            [XmlElement(ElementName = "Cancellation")]
            public CancellationResponse Cancellation { get; set; }
            [XmlElement(ElementName = "Payment")]
            public Payment Payment { get; set; }
            [XmlElement(ElementName = "Refund")]
            public Refund Refund { get; set; }
        }
        public class CancellationResponse
        {
            [XmlElement(ElementName = "PolicyText")]
            public List<CancellationPolicyText> PolicyText { get; set; }
        }
        public class CancellationPolicyText
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("language")]
            public string language { get; set; }
        }
        public class Payment
        {
            [XmlElement(ElementName = "PaymentRateInclusive")]
            public PaymentRateInclusive PaymentRateInclusive { get; set; }
        }
        public class PaymentRateInclusive
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
        }
        public class Refund
        {
            [XmlElement(ElementName = "RefundRateInclusive")]
            public RefundRateInclusive RefundRateInclusive { get; set; }
        }
        public class RefundRateInclusive
        {
            [XmlText]
            public string Value { get; set; }
            [XmlAttribute("currency")]
            public string currency { get; set; }
        }


        public class ConfirmCancellationResponseV2
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        #endregion

        #region 预订信息
        public class BookingResponseV3
        {
            [XmlElement(ElementName = "BookingDetails")]
            public BookingDetails BookingDetails { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class BookingDetails
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "Booking")]
            public Booking Booking { get; set; }
        }
        public class Booking
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("ItineraryID")]
            public string ItineraryID { get; set; }
            [XmlAttribute("selfservice")]
            public string selfservice { get; set; }
        }

        public class GuestDetails
        {
            public List<GuestDetail> GuestDetail { get; set; }
        }
        public class GuestDetail
        {
            public string Title { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public string CountryOfPassport { get; set; }
            //public int Age { get; set; }
            /*public string Gender { get; set; }
            public int Age { get; set; }*/
        }
        #endregion

        #region 账单信息
        public class BookingListResponseV2
        {
            [XmlElement(ElementName = "Bookings")]
            public BookingsList Bookings { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }
        public class BookingsList
        {
            [XmlElement(ElementName = "Booking")]
            public List<BookingList> Booking { get; set; }
        }
        public class BookingList
        {
            [XmlAttribute("id")]
            public string id { get; set; }
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlAttribute("tag")]
            public string tag { get; set; }
            [XmlAttribute("hotelid")]
            public string hotelid { get; set; }
            [XmlAttribute("hotelname")]
            public string hotelname { get; set; }
            [XmlAttribute("cityname")]
            public string cityname { get; set; }
            [XmlAttribute("received")]
            public string received { get; set; }
            [XmlAttribute("lastmodified")]
            public string lastmodified { get; set; }
            [XmlAttribute("arrival")]
            public string arrival { get; set; }
            [XmlAttribute("departure")]
            public string departure { get; set; }
            [XmlAttribute("usdamount")]
            public string usdamount { get; set; }
            [XmlAttribute("selfservice")]
            public string selfservice { get; set; }
        }
        #endregion

        #region 订单信息
        public class BookingDetailsResponseV2
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlElement(ElementName = "Bookings")]
            public BookingsDetails Bookings { get; set; }
            [XmlElement(ElementName = "ErrorMessages")]
            public ErrorMessages ErrorMessages { get; set; }
        }

        public class BookingsDetails
        {
            [XmlElement(ElementName = "Booking")]
            public List<BookingDetailsResponse> Booking { get; set; }
        }
        public class BookingDetailsResponse
        {
            [XmlElement(ElementName = "BookingID")]
            public string BookingID { get; set; }
            [XmlElement(ElementName = "Tag")]
            public string Tag { get; set; }
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "Country")]
            public string Country { get; set; }
            [XmlElement(ElementName = "City")]
            public string City { get; set; }
            [XmlElement(ElementName = "Hotel")]
            public string Hotel { get; set; }
            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }
            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
            [XmlElement(ElementName = "Room")]
            public BookingRoom Room { get; set; }
            public TotalRateUSD TotalRateUSD { get; set; }
            [XmlElement(ElementName = "Payment")]
            public BookingPayment Payment { get; set; }
            [XmlElement(ElementName = "GuestDetails")]
            public BookingGuestDetails GuestDetails { get; set; }
            [XmlElement(ElementName = "SelfServiceURL")]
            public string SelfServiceURL { get; set; }
            [XmlElement(ElementName = "Source")]
            public string Source { get; set; }
            [XmlElement(ElementName = "SupplierName")]
            public string SupplierName { get; set; }
            [XmlElement(ElementName = "SupplierReference")]
            public string SupplierReference { get; set; }
            [XmlElement(ElementName = "BookNowPayLaterDate")]
            public string BookNowPayLaterDate { get; set; }
        }
        public class BookingRoom
        {
            [XmlElement(ElementName = "RoomType")]
            public string RoomType { get; set; }
            [XmlElement(ElementName = "RoomsBooked")]
            public string RoomsBooked { get; set; }
        }
        public class TotalRateUSD
        {
            [XmlAttribute("rateplan")]
            public string rateplan { get; set; }
            [XmlAttribute("ratetype")]
            public string ratetype { get; set; }
            [XmlAttribute("exclusive")]
            public string exclusive { get; set; }
            [XmlAttribute("tax")]
            public string tax { get; set; }
            [XmlAttribute("fees")]
            public string fees { get; set; }
            [XmlAttribute("inclusive")]
            public string inclusive { get; set; }
        }
        public class BookingPayment
        {
            [XmlElement(ElementName = "PaymentRateInclusive")]
            public List<BookingPaymentRateInclusive> PaymentRateInclusive { get; set; }
        }
        public class BookingPaymentRateInclusive
        {
            [XmlAttribute("currency")]
            public string currency { get; set; }
            [XmlText]
            public string Value { get; set; }
        }
        public class BookingGuestDetails
        {
            [XmlElement(ElementName = "GuestDetail")]
            public List<BookingGuestDetail> GuestDetail { get; set; }
        }
        public class BookingGuestDetail
        {
            [XmlAttribute("Primary")]
            public string Primary { get; set; }
            [XmlElement(ElementName = "Title")]
            public string Title { get; set; }
            [XmlElement(ElementName = "FirstName")]
            public string FirstName { get; set; }
            [XmlElement(ElementName = "LastName")]
            public string LastName { get; set; }
            [XmlElement(ElementName = "CountryOfPassport")]
            public string CountryOfPassport { get; set; }
        }
        #endregion

        #region 试单信息
        public class PrecheckResponse
        {
            [XmlAttribute("status")]
            public string status { get; set; }
            [XmlAttribute("message")]
            public string message { get; set; }
            [XmlElement(ElementName = "errorList")]
            public errorList errorList { get; set; }
        }
        public class errorList
        {
            [XmlAttribute("code")]
            public string code { get; set; }
            [XmlAttribute("hotelId")]
            public string hotelId { get; set; }
            [XmlAttribute("roomId")]
            public string roomId { get; set; }
            [XmlAttribute("uid")]
            public string uid { get; set; }
            [XmlAttribute("message")]
            public string message { get; set; }
        }
        #endregion
    }
}