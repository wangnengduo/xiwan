﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Agoda
{
    public class ContinentResp
    {
        public class Continent_feed
        {
            [XmlElement(ElementName = "continents")]
            public continents continents { get; set; }
        }
        public class continents
        {
            [XmlElement(ElementName = "continent")]
            public List<continent> continent { get; set; }
        }
        public class continent
        {
            [XmlElement(ElementName = "continent_id")]
            public string continent_id { get; set; }
            [XmlElement(ElementName = "continent_name")]
            public string continent_name { get; set; }
            [XmlElement(ElementName = "continent_translated")]
            public string continent_translated { get; set; }
            [XmlElement(ElementName = "active_hotels")]
            public string active_hotels { get; set; }
        }

        public class Country_feed
        {
            [XmlElement(ElementName = "countries")]
            public countries countries { get; set; }
        }
        public class countries
        {
            [XmlElement(ElementName = "country")]
            public List<country> country { get; set; }
        }
        public class country
        {
            [XmlElement(ElementName = "country_id")]
            public string country_id { get; set; }
            [XmlElement(ElementName = "continent_id")]
            public string continent_id { get; set; }
            [XmlElement(ElementName = "country_name")]
            public string country_name { get; set; }
            [XmlElement(ElementName = "country_translated")]
            public string country_translated { get; set; }
            [XmlElement(ElementName = "active_hotels")]
            public string active_hotels { get; set; }
            [XmlElement(ElementName = "country_iso")]
            public string country_iso { get; set; }
            [XmlElement(ElementName = "country_iso2")]
            public string country_iso2 { get; set; }
            [XmlElement(ElementName = "longitude")]
            public string longitude { get; set; }
            [XmlElement(ElementName = "latitude")]
            public string latitude { get; set; }
        }
        public class City_feed
        {
            [XmlElement(ElementName = "cities")]
            public cities cities { get; set; }
        }
        public class cities
        {
            [XmlElement(ElementName = "city")]
            public List<city> city { get; set; }
        }
        public class city
        {
            [XmlElement(ElementName = "city_id")]
            public string city_id { get; set; }
            [XmlElement(ElementName = "country_id")]
            public string country_id { get; set; }
            [XmlElement(ElementName = "city_name")]
            public string city_name { get; set; }
            [XmlElement(ElementName = "city_translated")]
            public string city_translated { get; set; }
            [XmlElement(ElementName = "active_hotels")]
            public string active_hotels { get; set; }
            [XmlElement(ElementName = "longitude")]
            public string longitude { get; set; }
            [XmlElement(ElementName = "latitude")]
            public string latitude { get; set; }
            [XmlElement(ElementName = "no_area")]
            public string no_area { get; set; }
        }

        public class Hotel_feed
        {
            [XmlElement(ElementName = "hotels")]
            public hotels hotels { get; set; }
        }
        public class hotels
        {
            [XmlElement(ElementName = "hotel")]
            public List<hotel> hotel { get; set; }
        }
        public class hotel
        {
            [XmlElement(ElementName = "hotel_id")]
            public string hotel_id { get; set; }
            [XmlElement(ElementName = "hotel_name")]
            public string hotel_name { get; set; }
            [XmlElement(ElementName = "hotel_formerly_name")]
            public string hotel_formerly_name { get; set; }
            [XmlElement(ElementName = "translated_name")]
            public string translated_name { get; set; }
            [XmlElement(ElementName = "star_rating")]
            public string star_rating { get; set; }
            [XmlElement(ElementName = "continent_id")]
            public string continent_id { get; set; }
            [XmlElement(ElementName = "country_id")]
            public string country_id { get; set; }
            [XmlElement(ElementName = "city_id")]
            public string city_id { get; set; }
            [XmlElement(ElementName = "area_id")]
            public string area_id { get; set; }
            [XmlElement(ElementName = "longitude")]
            public string longitude { get; set; }
            [XmlElement(ElementName = "latitude")]
            public string latitude { get; set; }
            [XmlElement(ElementName = "hotel_url")]
            public string hotel_url { get; set; }
            [XmlElement(ElementName = "popularity_score")]
            public string popularity_score { get; set; }
            [XmlElement(ElementName = "remark")]
            public string remark { get; set; }
            [XmlElement(ElementName = "number_of_reviews")]
            public string number_of_reviews { get; set; }
            [XmlElement(ElementName = "rating_average")]
            public string rating_average { get; set; }
            [XmlElement(ElementName = "child_and_extra_bed_policy")]
            public child_and_extra_bed_policy child_and_extra_bed_policy { get; set; }
            [XmlElement(ElementName = "accommodation_type")]
            public string accommodation_type { get; set; }
            [XmlElement(ElementName = "nationality_restrictions")]
            public string nationality_restrictions { get; set; }
        }
        public class child_and_extra_bed_policy
        {
            [XmlElement(ElementName = "infant_age")]
            public string infant_age { get; set; }
            [XmlElement(ElementName = "children_age_from")]
            public string children_age_from { get; set; }
            [XmlElement(ElementName = "children_age_to")]
            public string children_age_to { get; set; }
            [XmlElement(ElementName = "children_stay_free")]
            public string children_stay_free { get; set; }
            [XmlElement(ElementName = "min_guest_age")]
            public string min_guest_age { get; set; }
        }

        //获取房型
        public class Hotel_feed_full
        {
            [XmlElement(ElementName = "addresses")]
            public addresses addresses { get; set; }
            [XmlElement(ElementName = "roomtypes")]
            public roomtypes roomtypes { get; set; }
            [XmlElement(ElementName = "pictures")]
            public pictures pictures { get; set; }
        }
        public class addresses
        {
            [XmlElement(ElementName = "address")]
            public List<address> address { get; set; }
        }
        public class address
        {
            [XmlElement(ElementName = "hotel_id")]
            public string hotel_id { get; set; }
            [XmlElement(ElementName = "address_type")]
            public string address_type { get; set; }
            [XmlElement(ElementName = "address_line_1")]
            public string address_line_1 { get; set; }
            [XmlElement(ElementName = "address_line_2")]
            public string address_line_2 { get; set; }
            [XmlElement(ElementName = "postal_code")]
            public string postal_code { get; set; }
            [XmlElement(ElementName = "state")]
            public string state { get; set; }
            [XmlElement(ElementName = "city")]
            public string city { get; set; }
            [XmlElement(ElementName = "country")]
            public string country { get; set; }
        }
        public class roomtypes
        {
            [XmlElement(ElementName = "roomtype")]
            public List<roomtype> roomtype { get; set; }
        }
        public class roomtype
        {
            [XmlElement(ElementName = "hotel_id")]
            public string hotel_id { get; set; }
            [XmlElement(ElementName = "hotel_room_type_id")]
            public string hotel_room_type_id { get; set; }
            [XmlElement(ElementName = "standard_caption")]
            public string standard_caption { get; set; }
            [XmlElement(ElementName = "standard_caption_translated")]
            public string standard_caption_translated { get; set; }
            //最大可住人数
            [XmlElement(ElementName = "max_occupancy_per_room")]
            public string max_occupancy_per_room { get; set; }
            //酒店总房间数
            [XmlElement(ElementName = "no_of_room")]
            public string no_of_room { get; set; }
            //面积
            [XmlElement(ElementName = "size_of_room")]
            public string size_of_room { get; set; }
            //房间是否带阳台
            [XmlElement(ElementName = "room_size_incl_terrace")]
            public string room_size_incl_terrace { get; set; }
            //景观
            [XmlElement(ElementName = "views")]
            public string views { get; set; }
            //最大加床数
            [XmlElement(ElementName = "max_extrabeds")]
            public string max_extrabeds { get; set; }
            //最大可住婴儿数
            [XmlElement(ElementName = "max_infant_in_room")]
            public string max_infant_in_room { get; set; }
            [XmlElement(ElementName = "hotel_room_type_picture")]
            public string hotel_room_type_picture { get; set; }
            [XmlElement(ElementName = "bed_type")]
            public string bed_type { get; set; }
            //母房型ID
            [XmlElement(ElementName = "hotel_master_room_type_id")]
            public string hotel_master_room_type_id { get; set; }
            [XmlElement(ElementName = "hotel_room_type_alternate_name")]
            public string hotel_room_type_alternate_name { get; set; }
        }

        public class pictures
        {
            [XmlElement(ElementName = "picture")]
            public List<picture> picture { get; set; }
        }
        public class picture
        {
            [XmlElement(ElementName = "hotel_id")]
            public string hotel_id { get; set; }
            [XmlElement(ElementName = "picture_id")]
            public string picture_id { get; set; }
            [XmlElement(ElementName = "caption")]
            public string caption { get; set; }
            [XmlElement(ElementName = "caption_translated")]
            public string caption_translated { get; set; }
            [XmlElement(ElementName = "URL")]
            public string URL { get; set; }
        }
    }
}