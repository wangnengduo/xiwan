﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Web;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;

namespace XiWan.Data.Agoda
{
    public class AgodaDAL
    {
        string USDtoRMBrate = CCommon.GetWebConfigValue("USDtoRMBrate");//美元转为人民币汇率
        string EURtoRMBrate = CCommon.GetWebConfigValue("EURtoRMBrate");//欧元转为人民币汇率
        string SGDtoRMBrate = CCommon.GetWebConfigValue("SGDtoRMBrate");//新加坡元转为人民币汇率
        string HKDtoRMBrate = CCommon.GetWebConfigValue("HKDtoRMBrate");//港币转为人民币汇率
        string AgodaTime = CCommon.GetWebConfigValue("AgodaTime");//缓存到数据库的有效期（分钟）
        string AgodaSeachUrl = CCommon.GetWebConfigValue("AgodaSeachUrl");//报价时用
        string AgodaBookUrl = CCommon.GetWebConfigValue("AgodaBookUrl");//下订单
        string AgodaBookDetailUrl = CCommon.GetWebConfigValue("AgodaBookDetailUrl");//查询订单信息，更新订单状态
        string AgodaBookListUrl = CCommon.GetWebConfigValue("AgodaBookListUrl");//查询账单
        //第一步cancellation，得到数据,再用这些数据调用confirm cancellation确认取消,因为有些有罚金，要你确认罚金的
        string AgodaCancelUrl = CCommon.GetWebConfigValue("AgodaCancelUrl");//取消订单
        string AgodaConfirmCancelUrl = CCommon.GetWebConfigValue("AgodaConfirmCancelUrl");//取消订单
        string Username = CCommon.GetWebConfigValue("AgodaUsername");//
        string Password = CCommon.GetWebConfigValue("AgodaPassword");//
        string AgodaCacheTime = CCommon.GetWebConfigValue("AgodaCacheTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        ControllerBase con2 = ControllerFactory.GetNewController(e_ConsType.Main2);

        #region 初始化本类
        private static AgodaDAL _Instance;
        public static AgodaDAL Instance
        {
            get
            {
                return new AgodaDAL();
            }
        }
        #endregion

        /// <summary>
        /// 从接口获取洲信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetContinent()
        {
            string result = string.Empty;
            string respData = string.Empty;
            string url = string.Format(@"http://affiliatefeed.agoda.com/datafeeds/feed/getfeed?apikey={0}&olanguage_id=8&feed_id=1", Password);
            try
            {
                try
                {
                    respData = Common.Common.GetHtmlWithUtf(url,"");
                    //LogHelper.DoOperateLog(string.Format("Studio：Agoda从接口获取国家信息并保存到数据库 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取国家信息并保存到数据库,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    result = "从接口获取洲信息并保存到数据库失败";
                    return result;
                }
                ContinentResp.Continent_feed objResp = Common.Common.DESerializer<ContinentResp.Continent_feed>(respData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&", "").Replace("<br/>", "").Replace("xmlns='http://www.w3.org/2001/XMLSchema' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'", ""));
                if (objResp != null)
                {
                    for (int i = 0; i < objResp.continents.continent.Count; i++)
                    {
                        AgodaContinent obj = new AgodaContinent();
                        obj.ContinentId = objResp.continents.continent[i].continent_id ?? "";
                        obj.ContinentName = objResp.continents.continent[i].continent_name ?? "";
                        obj.ContinentTranslated = objResp.continents.continent[i].continent_translated ?? "";
                        obj.ActiveHotels = objResp.continents.continent[i].active_hotels ?? "";
                        con.Save(obj);
                    }
                    result = "保存成功";
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取国家信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                result = "从接口获取洲信息并保存到数据库失败";
            }
            return result;
        }

        /// <summary>
        /// 从接口获取国家信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetCountry()
        {
            string result = string.Empty;
            string respData = string.Empty;
            string url = string.Format(@"http://affiliatefeed.agoda.com/datafeeds/feed/getfeed?apikey={0}&olanguage_id=8&feed_id=2", Password);
            try
            {
                try
                {
                    respData = Common.Common.GetHtmlWithUtf(url,"");
                    //LogHelper.DoOperateLog(string.Format("Studio：Agoda从接口获取国家信息并保存到数据库 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取国家信息并保存到数据库,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    result = "从接口获取国家信息并保存到数据库失败";
                    return result;
                }
                ContinentResp.Country_feed objResp = Common.Common.DESerializer<ContinentResp.Country_feed>(respData.Replace("&lt;", "<").Replace("&gt;", ">").Replace("&", "").Replace("<br/>", "").Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    for (int i = 0; i < objResp.countries.country.Count; i++)
                    {
                        AgodaCountry obj = new AgodaCountry();
                        obj.CountryId = objResp.countries.country[i].country_id ?? "";
                        obj.ContinentId = objResp.countries.country[i].continent_id ?? "";
                        obj.CountryName = objResp.countries.country[i].country_name ?? "";
                        obj.CountryTranslated = objResp.countries.country[i].country_translated ?? "";
                        obj.ActiveHotels = objResp.countries.country[i].active_hotels ?? "";
                        obj.CountryIso = objResp.countries.country[i].country_iso ?? "";
                        obj.CountryIso2 = objResp.countries.country[i].country_iso2 ?? "";
                        obj.Longitude = objResp.countries.country[i].longitude ?? "";
                        obj.Latitude = objResp.countries.country[i].latitude ?? "";
                        con.Save(obj);
                    }
                    result = "保存成功";
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取国家信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                result = "从接口获取国家信息并保存到数据库失败";
            }
            return result;
        }

        /// <summary>
        /// 从接口获取市信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetCity()
        {
            string result = string.Empty;
            string respData = string.Empty;
            try
            {
                string sql = "select CountryID from AgodaCountry";
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string url = string.Format(@"http://affiliatefeed.agoda.com/datafeeds/feed/getfeed?apikey={0}&feed_id=3&olanguage_id=8&ocountry_id={1}", Password, dt.Rows[i]["CountryID"]);
                    try
                    {
                        respData = Common.Common.GetHtmlWithUtf(url,"");
                        //LogHelper.DoOperateLog(string.Format("Studio：Agoda从接口获取市信息并保存到数据库 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Agoda接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取市信息并保存到数据库,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        result = "从接口获取市信息并保存到数据库失败";
                        return result;
                    }
                    ContinentResp.City_feed objResp = Common.Common.DESerializer<ContinentResp.City_feed>(respData.Replace("&", "").Replace("&lt;", "<").Replace("&gt;", ">").Replace("<br/>","").Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "").Replace("'", "\'"));
                    if (objResp != null)
                    {
                        for (int j = 0; j < objResp.cities.city.Count; j++)
                        {
                            AgodaCity obj = new AgodaCity();
                            obj.CityId = objResp.cities.city[j].city_id ?? "";
                            obj.CountryId = objResp.cities.city[j].country_id ?? "";
                            obj.CityCame = objResp.cities.city[j].city_name ?? "";
                            obj.CityTranslated = objResp.cities.city[j].city_translated ?? "";
                            obj.ActiveHotels = objResp.cities.city[j].active_hotels ?? "";
                            obj.Longitude = objResp.cities.city[j].longitude ?? "";
                            obj.Latitude = objResp.cities.city[j].latitude ?? "";
                            obj.NoArea = objResp.cities.city[j].no_area ?? "";
                            con.Save(obj);
                        }
                        result = "保存成功";
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取市信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                result = "从接口获取市信息并保存到数据库失败";
            }
            return result;
        }

        /// <summary>
        /// 从接口获取国籍信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        /*public void GetNationality()
        {
            string respData = string.Empty;
            string url = string.Empty;
            try
            {
                url = AsianoverlandUrl + string.Format("index.php?action=get_nationalities&username={0}&password={1}&gzip=no", AsianoverlandUsername, AsianoverlandPassword);
                try
                {
                    respData = Common.Common.GetHttp(url);
                    LogHelper.DoOperateLog(string.Format("Studio：Asianoverland获取国籍 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Asianoverland接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取国籍,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return;
                }
                AsianoverlandReq.AsianoverlandNationality nationalityInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<AsianoverlandReq.AsianoverlandNationality>(respData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (nationalityInfo != null)
                {
                    for (int i = 0; i < nationalityInfo.NationalitiesInfo.Count; i++)
                    {
                        AsianoverlandNationality obj = new AsianoverlandNationality();
                        obj.NationalityCode = string.IsNullOrEmpty(nationalityInfo.NationalitiesInfo[i].Code) ? "" : nationalityInfo.NationalitiesInfo[i].Code;
                        obj.NationalityName = string.IsNullOrEmpty(nationalityInfo.NationalitiesInfo[i].Nationality) ? "" : nationalityInfo.NationalitiesInfo[i].Nationality;
                        obj.IsoCode = string.IsNullOrEmpty(nationalityInfo.NationalitiesInfo[i].IsoCode) ? "" : nationalityInfo.NationalitiesInfo[i].IsoCode;
                        con.Save(obj);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Asianoverland获取国籍 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return;
            }
        }*/


        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetHotels()
        {
            string result = string.Empty;
            string respData = string.Empty;
            try
            {
                string sql = @"select CityId from AgodaCity order by ID";
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string url = string.Format(@"http://affiliatefeed.agoda.com/datafeeds/feed/getfeed?apikey={0}&mcity_id={1}&olanguage_id=8&feed_id=5", Password, dt.Rows[i]["CityId"]);
                    try
                    {
                        respData = Common.Common.GetHtmlWithUtf(url, "");
                        LogHelper.DoOperateLog(string.Format("Studio：Agoda从接口获取酒店信息并保存到数据库 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Agoda接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取酒店信息并保存到数据库,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        result = "保存失败";
                        return result;
                    }
                    ContinentResp.Hotel_feed objResp = Common.Common.DESerializer<ContinentResp.Hotel_feed>(Common.Common.xmlReplace(respData).Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                    if (objResp != null)
                    {
                        if (objResp.hotels != null)
                        {
                            AgodaHotelCollection objCol = new AgodaHotelCollection();
                            for (int j = 0; j < objResp.hotels.hotel.Count; j++)
                            {
                                if (objResp.hotels.hotel[j].hotel_id != null && objResp.hotels.hotel[j].hotel_id != "")
                                {
                                    string HotelId = objResp.hotels.hotel[j].hotel_id;
                                    string HotelName = (objResp.hotels.hotel[j].hotel_name ?? "").Replace("'", "''");
                                    string HotelFormerlyName = (objResp.hotels.hotel[j].hotel_formerly_name ?? "").Replace("'", "''");
                                    string TranslatedName = (objResp.hotels.hotel[j].translated_name ?? "").Replace("'", "''");
                                    string StarRating = objResp.hotels.hotel[j].star_rating ?? "";
                                    string ContinentId = objResp.hotels.hotel[j].continent_id ?? "";
                                    string CountryId = objResp.hotels.hotel[j].country_id ?? "";
                                    string CityId = objResp.hotels.hotel[j].city_id ?? "";
                                    string AreaId = objResp.hotels.hotel[j].area_id ?? "";
                                    string Longitude = objResp.hotels.hotel[j].longitude ?? "";
                                    string Latitude = objResp.hotels.hotel[j].latitude ?? "";
                                    string HotelUrl = objResp.hotels.hotel[j].hotel_url ?? "";
                                    string PopularityScore = objResp.hotels.hotel[j].popularity_score ?? "";
                                    string Remark = (objResp.hotels.hotel[j].remark ?? "").Replace("'", "''");
                                    string NumberOfReviews = (objResp.hotels.hotel[j].number_of_reviews ?? "").Replace("'", "''");
                                    string RatingAverage = objResp.hotels.hotel[j].rating_average ?? "";
                                    string InfantAge = string.Empty;
                                    string ChildrenAgeFrom = string.Empty;
                                    string ChildrenAgeTo = string.Empty;
                                    string ChildrenStayFree = string.Empty;
                                    string MinGuestAge = string.Empty;
                                    string AccommodationType = objResp.hotels.hotel[j].accommodation_type ?? "";
                                    if (objResp.hotels.hotel[j].child_and_extra_bed_policy != null)
                                    {
                                        InfantAge = objResp.hotels.hotel[j].child_and_extra_bed_policy.infant_age ?? "";
                                        ChildrenAgeFrom = objResp.hotels.hotel[j].child_and_extra_bed_policy.children_age_from ?? "";
                                        ChildrenAgeTo = objResp.hotels.hotel[j].child_and_extra_bed_policy.children_age_to ?? "";
                                        ChildrenStayFree = objResp.hotels.hotel[j].child_and_extra_bed_policy.children_stay_free ?? "";
                                        MinGuestAge = objResp.hotels.hotel[j].child_and_extra_bed_policy.min_guest_age ?? "";
                                    }



                                    AgodaHotel obj = new AgodaHotel();
                                    obj.HotelId = HotelId;
                                    obj.HotelName = HotelName;
                                    obj.HotelFormerlyName = HotelFormerlyName;
                                    obj.TranslatedName = TranslatedName;
                                    obj.StarRating = StarRating;
                                    obj.ContinentId = ContinentId;
                                    obj.CountryId = CountryId;
                                    obj.CityId = CityId;
                                    obj.AreaId = AreaId;
                                    obj.Longitude = Longitude;
                                    obj.Latitude = Latitude;
                                    obj.HotelUrl = HotelUrl;
                                    obj.PopularityScore = PopularityScore;
                                    obj.Remark = PopularityScore;
                                    obj.NumberOfReviews = NumberOfReviews;
                                    obj.RatingAverage = RatingAverage;
                                    obj.InfantAge = InfantAge;
                                    obj.ChildrenAgeFrom = ChildrenAgeFrom;
                                    obj.ChildrenAgeTo = ChildrenAgeTo;
                                    obj.ChildrenStayFree = ChildrenStayFree;
                                    obj.MinGuestAge = MinGuestAge;
                                    obj.AccommodationType = objResp.hotels.hotel[j].accommodation_type ?? "";
                                    string sqlAgodaHotel = string.Format(@"select * from AgodaHotel where HotelID='{0}'", HotelId);
                                    DataTable dtAgodaHotel = ControllerFactory.GetController().GetDataTable(sqlAgodaHotel);
                                    if (dtAgodaHotel.Rows.Count > 0)
                                    {
                                        string updateAgodaRoomType = string.Format(@"update AgodaHotel set HotelName='{0}',HotelFormerlyName='{1}',TranslatedName='{2}',
StarRating='{3}',ContinentId='{4}',CountryId='{5}',CityId='{6}',AreaId='{7}',Longitude='{8}',Latitude='{9}',HotelUrl='{10}',PopularityScore='{11}',
Remark='{12}',NumberOfReviews='{13}',RatingAverage='{14}',InfantAge='{15}',ChildrenAgeFrom='{16}',ChildrenAgeTo='{17}',ChildrenStayFree='{18}',MinGuestAge='{19}',
AccommodationType='{20}'where  HotelID='{21}'", HotelName, HotelFormerlyName, TranslatedName, StarRating, ContinentId, CountryId, CityId, AreaId, Longitude, Latitude,
HotelUrl, PopularityScore, Remark, NumberOfReviews, RatingAverage, InfantAge, ChildrenAgeFrom, ChildrenAgeTo, ChildrenStayFree, MinGuestAge, AccommodationType, HotelId);
                                        con.Update(updateAgodaRoomType);//若存在，做修改
                                    }
                                    else
                                    {
                                        objCol.Add(obj);
                                    } 
                                }
                            }
                            con.Save(objCol);
                        }
                    }
                    result = "保存成功";
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取酒店信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                result = "保存失败";
            }
            return result;
        }


        /// <summary>
        /// 从接口获取房型信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetRoomTypeSave()
        {
            string result = string.Empty;
            string respData = string.Empty;
            try
            {
                string sql = @"select HotelID from AgodaHotel where id>147527 order by ID";
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    //Thread.Sleep(1000);
                    string HotelID = dt.Rows[i]["HotelID"].AsTargetType<string>("");
                    string url = string.Format(@"http://affiliatefeed.agoda.com/datafeeds/feed/getfeed?apikey={0}&mhotel_id={1}&olanguage_id=8&feed_id=19", Password, dt.Rows[i]["HotelID"]);
                    try
                    {
                        respData = Common.Common.GetHtmlWithUtf(url, "");
                        //LogHelper.DoOperateLog(string.Format("Studio：Agoda从接口获取房型信息并保存到数据库 , 返回数据 ：{0} ， Time:{1} ", respData, DateTime.Now.ToString()), "Agoda接口");
                    }
                    catch (Exception ex)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取房型信息并保存到数据库,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                        result = "保存失败";
                        return result;
                    }
                    ContinentResp.Hotel_feed_full objResp = Common.Common.DESerializer<ContinentResp.Hotel_feed_full>(Common.Common.xmlReplace(respData).Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                    if (objResp != null)
                    {
                        if (objResp.roomtypes != null)
                        {
                            AgodaRoomTypeCollection objCol = new AgodaRoomTypeCollection();
                            for (int j = 0; j < objResp.roomtypes.roomtype.Count; j++)
                            {
                                if (objResp.roomtypes.roomtype[j].hotel_id != null && objResp.roomtypes.roomtype[j].hotel_id != "")
                                {
                                    string strIsBalcony = objResp.roomtypes.roomtype[j].room_size_incl_terrace ?? "";
                                    int IsBalcony = 0;
                                    if (strIsBalcony.ToLower() == "true")
                                    {
                                        IsBalcony = 1;
                                    }
                                    AgodaRoomType obj = new AgodaRoomType();
                                    string strHotelID = objResp.roomtypes.roomtype[j].hotel_id;
                                    string RoomTypeID = objResp.roomtypes.roomtype[j].hotel_room_type_id ?? "";
                                    string RoomTypeNameEN = (objResp.roomtypes.roomtype[j].standard_caption ?? "").Replace("'", "''");
                                    string RoomTypeNameCN = (objResp.roomtypes.roomtype[j].standard_caption_translated ?? "").Replace("'", "''");
                                    int MaxPerson = (objResp.roomtypes.roomtype[j].max_occupancy_per_room ?? "").AsTargetType<int>(0);
                                    int RoomsTotal = (objResp.roomtypes.roomtype[j].no_of_room ?? "").AsTargetType<int>(0);
                                    string RoomsArea = objResp.roomtypes.roomtype[j].size_of_room ?? "";
                                    string Views = (objResp.roomtypes.roomtype[j].views ?? "").Replace("'", "''");
                                    int MaximumExtraBeds = (objResp.roomtypes.roomtype[j].max_extrabeds ?? "").AsTargetType<int>(0);
                                    int MaximumBabies = (objResp.roomtypes.roomtype[j].max_infant_in_room ?? "").AsTargetType<int>(0);
                                    string RoomTypePicture = objResp.roomtypes.roomtype[j].hotel_room_type_picture ?? "";
                                    string ParentRoomTypeID = objResp.roomtypes.roomtype[j].hotel_master_room_type_id ?? "";
                                    string BedType = objResp.roomtypes.roomtype[j].bed_type ?? "";

                                    obj.HotelID = strHotelID;
                                    obj.RoomTypeID = RoomTypeID;
                                    obj.RoomTypeNameEN = RoomTypeNameEN;
                                    obj.RoomTypeNameCN = RoomTypeNameCN;
                                    obj.MaxPerson = MaxPerson;
                                    obj.RoomsTotal = RoomsTotal;
                                    obj.RoomsArea = RoomsArea;
                                    obj.IsBalcony = IsBalcony;
                                    obj.Views = Views;
                                    obj.MaximumExtraBeds = MaximumExtraBeds;
                                    obj.MaximumBabies = MaximumBabies;
                                    obj.RoomTypePicture = RoomTypePicture;
                                    obj.ParentRoomTypeID = ParentRoomTypeID;
                                    obj.BedType = BedType;
                                    obj.CreateTime = DateTime.Now;
                                    string sqlAgodaRoomType = string.Format(@"select * from AgodaRoomType where RoomTypeID='{0}' AND HotelID='{1}'", RoomTypeID, strHotelID);
                                    DataTable dtAgodaRoomType = ControllerFactory.GetController().GetDataTable(sqlAgodaRoomType);
                                    //con.Save(obj);
                                    if (dtAgodaRoomType.Rows.Count > 0)
                                    {
                                        //Thread.Sleep(500);
                                        string updateAgodaRoomType = string.Format(@"update AgodaRoomType set RoomTypeNameEN='{0}',RoomTypeNameCN='{1}',MaxPerson='{2}',
RoomsTotal='{3}',RoomsArea='{4}',IsBalcony='{5}',Views='{6}',MaximumExtraBeds='{7}',MaximumBabies='{8}',RoomTypePicture='{9}',ParentRoomTypeID='{10}'
,BedType='{11}' where RoomTypeID='{12}' AND  HotelID='{13}'", RoomTypeNameEN, RoomTypeNameCN, MaxPerson, RoomsTotal, RoomsArea, IsBalcony
, Views, MaximumExtraBeds, MaximumBabies, RoomTypePicture, ParentRoomTypeID, BedType, RoomTypeID, strHotelID);
                                        con.Update(updateAgodaRoomType);//若存在，做修改
                                        
                                    }
                                    else
                                    {
                                        objCol.Add(obj);
                                    }
                                }
                            }
                            con.Save(objCol);//保存房型
                            string strPicturesUrl = string.Empty;
                            string AddressEN = string.Empty;//英文名地址
                            string AddressLocal = string.Empty;//当地文字地址
                            for (int j = 0; j < objResp.pictures.picture.Count; j++)
                            {
                                if (objResp.pictures.picture[j].hotel_id != null && objResp.pictures.picture[j].hotel_id != "")
                                {
                                    strPicturesUrl += (objResp.pictures.picture[j].URL ?? "") + ";";
                                }
                            }
                            for (int j = 0; j < objResp.addresses.address.Count; j++)
                            {
                                if (objResp.addresses.address[j].address_type == "English address")
                                {
                                    string address1 = objResp.addresses.address[j].address_line_1 ?? "";
                                    string address2 = objResp.addresses.address[j].address_line_2 ?? "";
                                    AddressEN = address1;
                                    if (address2.Trim() != "")
                                    {
                                        AddressEN = address1 + "," + address2;
                                    }
                                }
                                //else if (objResp.addresses.address[j].address_type == "Local language")
                                //{
                                //    string address1 = objResp.addresses.address[j].address_line_1 ?? "";
                                //    string address2 = objResp.addresses.address[j].address_line_2 ?? "";
                                //    AddressLocal = address1;
                                //    if (address2.Trim() != "")
                                //    {
                                //        AddressLocal = address1 + "," + address2;
                                //    }
                                //}
                            }
                            //Thread.Sleep(500);
                            string updateAgodaHotel = string.Format(@"update AgodaHotel set ImageUrl='{0}',AddressEN='{1}' where HotelID='{2}'",
                                strPicturesUrl.TrimEnd(';').Replace("'", "''"), AddressEN.Replace("'", "''"), HotelID);
                            con.Update(updateAgodaHotel);//保存图片路径
                        }
                    }
                    result = "保存成功";
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda从接口获取房型信息并保存到数据库 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                result = "保存失败";
            }
            return result;
        }

        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationality()
        {
            string sql = "select CountryID as ID,CountryName as NAME from SunSeriesNationality ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }

        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestination(string strName)
        {
            string sql = string.Format(@"SELECT CityTranslated as name,(CityID+','+CountryID) as value FROM AgodaCity where CityTranslated like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }

        //获取报价
        public DataTable GetRtsRoomTypePrice(string hotelID, string CheckInDate, string CheckoutDate, IntOccupancyInfo occupancy, int RoomCount, string rateplanId)
        {
             DataTable dt=new DataTable();
             try
             {
                 string sql1 = string.Format("SELECT  * FROM AgodaHotel where HotelID='{0}'",hotelID);
                 DataTable dt1 = ControllerFactory.GetController().GetDataTable(sql1);
                 for (int p = 0; p < dt1.Rows.Count; p++)
                 {
                     //Thread.Sleep(2000);
                     //Random rNumber = new Random();
                     //int addDays = rNumber.Next(1, 10);
                     //int addAdults = rNumber.Next(1, 4);
                     //RoomCount = rNumber.Next(1, 2);
                     //CheckInDate = GetRandomTime(DateTime.Now, ("2019-10-20").AsTargetType<DateTime>(DateTime.MinValue)).ToString("yyyy-MM-dd");
                     //CheckoutDate = (GetRandomTime(DateTime.Now, ("2019-10-20").AsTargetType<DateTime>(DateTime.MinValue)).AddDays(addDays)).ToString("yyyy-MM-dd");
                     //hotelID = dt1.Rows[p]["HotelId"].AsTargetType<string>("");
                     int BookingDays = Convert.ToDateTime(CheckoutDate).Day - Convert.ToDateTime(CheckInDate).Day;//预订天数
                     string respData = string.Empty;
                     string postBoby = string.Empty;
                     string ChildrenAge = string.Empty;
                     int adults = occupancy.adults; //addAdults;
                     int children = occupancy.children;
                     string childAges = occupancy.childAges ?? "";

                     int Type = 4;//1 = City search (deprecated)   4 = Hotel search  6 = Hotel list 
                     if (children == 0)
                     {
                         ChildrenAge = "";
                     }
                     else
                     {
                         string[] ArrayAges = occupancy.childAges.Split(',');
                         for (int i = 0; i < children; i++)
                         {
                             ChildrenAge += "<Age>" + ArrayAges[i].AsTargetType<int>(0) + "</Age>";
                         }
                         ChildrenAge = "<ChildrenAges>" + ChildrenAge + "</ChildrenAges>";
                     }
                     postBoby = string.Format(@"<?xml version=""1.0"" encoding=""utf-8"" ?> <AvailabilityRequestV2 siteid=""{0}"" apikey=""{1}""
xmlns=""http://xml.agoda.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""> <Type>{2}</Type> <Id>{3}</Id> <CheckIn>{4}</CheckIn> <CheckOut>{5}</CheckOut> 
<Rooms>{6}</Rooms> <Adults>{7}</Adults> <Children>{8}</Children> {9} <Language>zh-cn</Language> <Currency>CNY</Currency> </AvailabilityRequestV2>",
    Username, Password, Type, hotelID, CheckInDate, CheckoutDate, RoomCount, adults, children, ChildrenAge);
                     try
                     {
                         respData = WebReq(AgodaSeachUrl, postBoby);
                         LogHelper.DoOperateLog(string.Format("Studio：Agoda获取报价 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "Agoda接口");
                     }
                     catch (Exception ex)
                     {
                         LogHelper.DoOperateLog(string.Format("Err:Agoda获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                         return dt;
                     }
                     AgodaResp.AvailabilityLongResponseV2 objResp = Common.Common.DESerializer<AgodaResp.AvailabilityLongResponseV2>(respData.Replace("&", "").Replace("xmlns=\"http://xml.agoda.com\"", "").Replace("xmlns:tns=\"http://xml.agoda.com\"", "").Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"", "").Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                     if (objResp != null)
                     {
                         if (objResp.ErrorMessages == null)
                         {
                             HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                             for (int j = 0; j < objResp.Hotels.Hotel.Count; j++)
                             {
                                 for (int k = 0; k < objResp.Hotels.Hotel[j].Rooms.Room.Count; k++)
                                 {
                                     if (objResp.Hotels.Hotel[j].Rooms.Room[k].id != null && objResp.Hotels.Hotel[j].Rooms.Room[k].id != "")
                                     {
                                         //roomID+promotionID+ratecategoryID+所有benefitID+cancellation code
                                         string roomID = objResp.Hotels.Hotel[j].Rooms.Room[k].id;
                                         string promotionID = objResp.Hotels.Hotel[j].Rooms.Room[k].promotionid;
                                         string benefitID = string.Empty;
                                         string cancellationCode = "";
                                         string cGuid = Common.Common.GuidToLongID();//房型id
                                         string GuidRateplanId = Common.Common.getGUID();//生成价格计划id

                                         string strRoom = string.Empty;
                                         string roomid = objResp.Hotels.Hotel[j].Rooms.Room[k].id ?? "";
                                         string roomPromotionid = objResp.Hotels.Hotel[j].Rooms.Room[k].promotionid ?? "";
                                         string roomName = objResp.Hotels.Hotel[j].Rooms.Room[k].name ?? "";
                                         string roomLineitemid = objResp.Hotels.Hotel[j].Rooms.Room[k].lineitemid ?? "";
                                         string roomRateplan = objResp.Hotels.Hotel[j].Rooms.Room[k].rateplan ?? "";
                                         string roomRatetype = objResp.Hotels.Hotel[j].Rooms.Room[k].ratetype ?? "";
                                         string roomCurrency = objResp.Hotels.Hotel[j].Rooms.Room[k].currency ?? "";
                                         string roomModel = objResp.Hotels.Hotel[j].Rooms.Room[k].model ?? "";
                                         string roomRatecategoryid = objResp.Hotels.Hotel[j].Rooms.Room[k].ratecategoryid ?? "";
                                         string roomBlockid = objResp.Hotels.Hotel[j].Rooms.Room[k].blockid ?? "";
                                         string roomBooknowpaylaterdate = objResp.Hotels.Hotel[j].Rooms.Room[k].booknowpaylaterdate ?? "";

                                         string strRate = string.Empty;
                                         string RateExclusive = string.Empty;
                                         string RateTax = string.Empty;
                                         string RateFees = string.Empty;
                                         string RateInclusive = string.Empty;
                                         if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo != null && objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate != null)
                                         {
                                             RateExclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.exclusive ?? "";
                                             RateTax = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.tax ?? "";
                                             RateFees = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.fees ?? "";
                                             RateInclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.inclusive ?? "";
                                         }
                                         strRate = string.Format("<Rate exclusive=\"{0}\" tax=\"{1}\" fees=\"{2}\" inclusive=\"{3}\"/>", RateExclusive, RateTax, RateFees, RateInclusive);

                                         if (roomBooknowpaylaterdate == "" && roomPromotionid == "")
                                         {
                                             strRoom = string.Format("<Room id=\"{0}\" name=\"{1}\" lineitemid=\"{2}\" rateplan=\"{3}\" ratetype=\"{4}\" currency=\"{5}\" model=\"{6}\" ratecategoryid=\"{7}\" blockid=\"{8}\" count=\"{9}\" adults=\"{10}\" children=\"{11}\">"
     , roomid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid, roomBlockid, RoomCount, adults, children);
                                         }
                                         else if (roomBooknowpaylaterdate == "")
                                         {
                                             strRoom = string.Format("<Room id=\"{0}\" promotionid=\"{1}\" name=\"{2}\" lineitemid=\"{3}\" rateplan=\"{4}\" ratetype=\"{5}\" currency=\"{6}\" model=\"{7}\" ratecategoryid=\"{8}\" blockid=\"{9}\" count=\"{10}\" adults=\"{11}\" children=\"{12}\">"
         , roomid, roomPromotionid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid,
         roomBlockid, RoomCount, adults, children);
                                         }
                                         else if (roomPromotionid == "")
                                         {
                                             strRoom = string.Format("<Room id=\"{0}\" name=\"{1}\" lineitemid=\"{2}\" rateplan=\"{3}\" ratetype=\"{4}\" currency=\"{5}\" model=\"{6}\" ratecategoryid=\"{7}\" blockid=\"{8}\" count=\"{9}\" adults=\"{10}\" children=\"{11}\">"
     , roomid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid, roomBlockid, RoomCount, adults, children);
                                         }
                                         else
                                         {
                                             strRoom = string.Format("<Room id=\"{0}\" promotionid=\"{1}\" name=\"{2}\" lineitemid=\"{3}\" rateplan=\"{4}\" ratetype=\"{5}\" currency=\"{6}\" model=\"{7}\" ratecategoryid=\"{8}\" blockid=\"{9}\" count=\"{10}\" adults=\"{11}\" children=\"{12}\">"
     , roomid, roomPromotionid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid, roomBlockid, RoomCount, adults, children);
                                         }
                                         //额外费用 当surcharge中charge=excluded时，这额外费用到店付款，surcharge中charge=mandatory要收，但房价一定得收
                                         decimal SumSurchargeMandatory = 0;//额外费用总和，公司收费
                                         decimal SumSurchargeExcluded = 0;//额外费用总和，个人到酒店付款
                                         string SurchargeXml = "";
                                         string strSurcharge = "";
                                         if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo != null && objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges != null)
                                         {
                                             for (int m = 0; m < objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge.Count; m++)
                                             {
                                                 if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate != null)
                                                 {
                                                     if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].charge == "Mandatory")
                                                     {
                                                         SumSurchargeMandatory += objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive.AsTargetType<decimal>(0);
                                                     }
                                                     else
                                                     {
                                                         SumSurchargeExcluded += objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive.AsTargetType<decimal>(0);
                                                     }
                                                     string surchargeID = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].id ?? "";
                                                     string surchargeExclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.exclusive ?? "";
                                                     string surchargeTax = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.tax ?? "";
                                                     string surchargeFees = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.fees ?? "";
                                                     string surchargeInclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive ?? "";
                                                     strSurcharge += string.Format("<Surcharge id=\"{0}\"><Rate exclusive=\"{1}\" tax=\"{2}\" fees=\"{3}\" inclusive=\"{4}\" /></Surcharge>"
                                                         , surchargeID, surchargeExclusive, surchargeTax, surchargeFees, surchargeInclusive);
                                                 }
                                             }
                                         }
                                         if (strSurcharge != "")
                                         {
                                             SurchargeXml = "<Surcharges>" + strSurcharge + "</Surcharges>";
                                         }


                                         //早餐 BenefitID=20是双早,BenefitID=26是单早,BenefitID=1的情况下，normalbedding=几就是几份早餐。
                                         int IsBreakfast = 0;
                                         if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits != null)
                                         {
                                             for (int m = 0; m < objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit.Count; m++)
                                             {
                                                 benefitID += objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id;
                                                 if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].Name == "Breakfast")
                                                 {
                                                     if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id == "20")
                                                     {
                                                         IsBreakfast = 2;
                                                     }
                                                     else if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id == "26")
                                                     {
                                                         IsBreakfast = 1;
                                                     }
                                                     else if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id == "1")
                                                     {
                                                         IsBreakfast = objResp.Hotels.Hotel[j].Rooms.Room[k].MaxRoomOccupancy.normalbedding.AsTargetType<int>(0);
                                                     }
                                                 }
                                             }
                                         }

                                         HotelPrice obj = new HotelPrice();
                                         obj.HotelID = objResp.Hotels.Hotel[j].Id;
                                         obj.HotelNo = objResp.searchid;
                                         obj.Platform = "Agoda";
                                         obj.Status = "1";
                                         obj.ClassName = objResp.Hotels.Hotel[j].Rooms.Room[k].name;
                                         obj.ReferenceClient = roomID + promotionID + benefitID + cancellationCode;//提供复查价格唯一标识
                                         obj.RMBprice = RateInclusive.AsTargetType<decimal>(0) * RoomCount * BookingDays + SumSurchargeMandatory;
                                         obj.Price = RateInclusive.AsTargetType<decimal>(0) * RoomCount * BookingDays + SumSurchargeMandatory;
                                         obj.Surcharge = SumSurchargeExcluded;//个人到酒店付款附加费
                                         obj.CurrencyCode = objResp.Hotels.Hotel[j].Rooms.Room[k].currency;
                                         obj.IsBreakfast = IsBreakfast;
                                         obj.RoomsLeft = objResp.Hotels.Hotel[j].Rooms.Room[k].RemainingRooms.AsTargetType<int>(0);
                                         obj.Adults = adults;
                                         obj.Childs = children;
                                         obj.ChildsAge = childAges;
                                         obj.RoomCount = RoomCount;
                                         obj.RoomText = strRoom + strRate + ChildrenAge + SurchargeXml;
                                         obj.CheckInDate = CheckInDate.AsTargetType<DateTime>(DateTime.MinValue);
                                         obj.CheckOutDate = CheckoutDate.AsTargetType<DateTime>(DateTime.MinValue);
                                         obj.CGuID = cGuid;
                                         obj.RateplanId = GuidRateplanId;
                                         //obj.BedTypeCode = BedTypeCode;

                                         //string PriceBreakdownDate = string.Empty;
                                         //string PriceBreakdownPrice = string.Empty;
                                         //for (int r = 0; r < respObj[k].rooms[l].room_classes[m].samples[n].nights.Count; r++)
                                         //{
                                         //    PriceBreakdownDate += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].date + ",";
                                         //    PriceBreakdownPrice += respObj[k].rooms[l].room_classes[m].samples[n].nights[r].rate + ",";
                                         //}
                                         //obj.PriceBreakdownDate = PriceBreakdownDate;
                                         //obj.PriceBreakdownPrice = PriceBreakdownPrice;

                                         obj.IsClose = 1;
                                         obj.CreateTime = DateTime.Now;
                                         obj.UpdateTime = DateTime.Now;
                                         obj.EntityState = e_EntityState.Added;
                                         objHotelPriceCol.Add(obj);
                                     }
                                 }

                             }
                             string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' and  Platform = 'Agoda'", hotelID);
                             con.Update(updateHotelPriceSql);
                             con.Save(objHotelPriceCol);
                             string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'Agoda' and IsClose=1", hotelID);
                             dt = ControllerFactory.GetController().GetDataTable(sql);
                         }
                         else
                         {
                             LogHelper.DoOperateLog(string.Format("Err：Agoda获取报价 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                             return dt;
                         }
                     }
                     else
                     {
                         LogHelper.DoOperateLog(string.Format("Err：Agoda获取报价 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Agoda接口");
                         return dt;
                     }
                 }
             }
             catch (Exception ex)
             {
                 LogHelper.DoOperateLog(string.Format("Err:Agoda获取报价 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
             }
             return dt;
        }
        
        //预订
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            try
            {
                string guID = Common.Common.getGUID();
                string sqlHp = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and  Platform = 'Agoda' AND RateplanId='{1}'", HotelID, RateplanId);
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sqlHp);

                AgodaResp.GuestDetails guestDetails = new AgodaResp.GuestDetails();
                List<AgodaResp.GuestDetail> guestDetailList = new List<AgodaResp.GuestDetail>();
                for (int i = 0; i < customers.Count; i++)
                {
                    for (int j = 0; j < customers[i].Customers.Count; j++)
                    {
                        string title = "Ms.";
                        AgodaResp.GuestDetail guestDetail = new AgodaResp.GuestDetail();
                        if (customers[i].Customers[j].sex == 1)
                        {
                            title = "Mr.";
                        }
                        guestDetail.Title = title;
                        guestDetail.FirstName = customers[i].Customers[j].firstName;
                        guestDetail.LastName = customers[i].Customers[j].lastName;
                        guestDetail.CountryOfPassport = "CN";
                        //guestDetail.Gender = "";
                        //guestDetail.Age = customers[i].Customers[j].age;
                        guestDetailList.Add(guestDetail);
                    }
                }
                guestDetails.GuestDetail=guestDetailList;
                string guestDetailsXml = Common.Common.XmlSerializeNoTitle<AgodaResp.GuestDetails>(guestDetails);
                postBoby = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?> <BookingRequestV3 siteid=""{0}"" apikey=""{1}"" xmlns=""http://xml.agoda.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
xsi:schemaLocation=""http://xml.agoda.com BookingRequestV3.xsd""> <BookingDetails searchid=""{2}"" tag=""{3}"" AllowDuplication=""true"" CheckIn=""{4}"" CheckOut=""{5}"">
<Hotel id=""{6}""><Rooms>{7}{8} </Room></Rooms> </Hotel> </BookingDetails> <CustomerDetail><Language>zh-cn</Language>
<Title>Mr.</Title><FirstName>NengDuo</FirstName><LastName>Wang</LastName><Email>wong@xiwantrip.com</Email><Phone><CountryCode></CountryCode><AreaCode></AreaCode>
<Number>15119550317</Number></Phone><Newsletter>true</Newsletter><IpAddress></IpAddress></CustomerDetail> <PaymentDetails> </PaymentDetails> </BookingRequestV3>",
Username, Password, dtHp.Rows[0]["HotelNo"], guID, beginTime, endTime, HotelID, dtHp.Rows[0]["RoomText"],
guestDetailsXml.Replace("<GuestDetails><GuestDetail><GuestDetail>", "<GuestDetails><GuestDetail>").Replace("</GuestDetail></GuestDetail></GuestDetails>", "</GuestDetail></GuestDetails>"));
                try
                {
                    respData = WebReq(AgodaBookUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：Agoda预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Agoda预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "预订失败失败，" + ex.ToString() };
                }
                AgodaResp.BookingResponseV3 objResp = Common.Common.DESerializer<AgodaResp.BookingResponseV3>(respData.Replace("&", "").Replace("xmlns=\"http://xml.agoda.com\" xmlns:tns=\"http://xml.agoda.com\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        Booking booking = new Booking();
                        //保存订单到数据库
                        booking.BookingCode = objResp.BookingDetails.Booking.id;
                        booking.InOrderNum = inOrderNum;
                        booking.AgentBookingReference = guID;
                        booking.RMBprice = 0;
                        booking.ClientCurrencyCode = dtHp.Rows[0]["CurrencyCode"].AsTargetType<string>("");
                        booking.ClientPartnerAmount = dtHp.Rows[0]["Price"].AsTargetType<decimal>(0);
                        booking.RMBprice = dtHp.Rows[0]["Price"].AsTargetType<decimal>(0);
                        booking.Surcharge = dtHp.Rows[0]["Surcharge"].AsTargetType<decimal>(0);
                        booking.HotelId = HotelID;
                        booking.HotelNo = "";
                        booking.Platform = "Agoda";
                        booking.StatusCode = "BookingReceived";
                        booking.StatusName = "订单已提交，但还未确认";
                        booking.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                        booking.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                        //booking.ServiceID = 0;
                        //booking.ServiceName = "";
                        //booking.LegID = 0;
                        //booking.Leg_Status = "";
                        booking.Status = 1;
                        booking.CreatTime = DateTime.Now;
                        booking.UpdateTime = DateTime.Now;
                        booking.EntityState = e_EntityState.Added;
                        con.Save(booking);
                        resp = new Respone() { code = "00", orderNum = objResp.BookingDetails.Booking.id, orderTotal = "0", mes = "订单已提交，但还未确认" };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "预订失败," + objResp.ErrorMessages.ErrorMessage.Value };
                        LogHelper.DoOperateLog(string.Format("Err：Agoda预订 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err：Agoda预订 ,Err :{0} ， Time:{1} ", "预订xml解析出错", DateTime.Now.ToString()), "Agoda接口");
                    resp = new Respone() { code = "99", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        
        //查询账单
        public string GetBookingList(string hotelID, string AgentBookingReference)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string postBoby = string.Empty;
                string StatusName = string.Empty;
                string[] BookingAgentBookingReference = AgentBookingReference.Split(',');
                Tags tags = new Tags();
                List<string> tagList = new List<string>();
                for (int i = 0; i < BookingAgentBookingReference.Length; i++)
                {
                    tagList.Add(BookingAgentBookingReference[i]);
                }
                tags.Tag = tagList;
                string tagXml = Common.Common.XmlSerializeNoTitle<Tags>(tags).Replace("<Tags><Tag>", "<Tags>").Replace("</Tag></Tags>", "</Tags>").Replace("<string>", "<Tag>").Replace("</string>", "</Tag>");
                postBoby = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <BookingListRequestV2 siteid=""{0}"" apikey=""{1}""
xmlns=""http://xml.agoda.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""> {2} </BookingListRequestV2>", Username, Password, tagXml);
                try
                {
                    result = WebReq(AgodaBookListUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio： Agoda查询账单 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", postBoby, result, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "查看订单失败," + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Agoda查询账单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                AgodaResp.BookingListResponseV2 objResp = Common.Common.DESerializer<AgodaResp.BookingListResponseV2>(result.Replace("xmlns=\"http://xml.agoda.com\" xmlns:tns=\"http://xml.agoda.com\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "").Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        for (int j = 0; j < objResp.Bookings.Booking.Count; j++)
                        {
                            if (objResp.Bookings.Booking[j].status == "BookingReceived")
                            {
                                StatusName = "收到预订";
                            }
                            else if (objResp.Bookings.Booking[j].status == "BookingCharged")
                            {
                                StatusName = "预订已确认";
                            }
                            else if (objResp.Bookings.Booking[j].status == "F")
                            {
                                StatusName = "开具发票";
                            }
                            else if (objResp.Bookings.Booking[j].status == "N")
                            {
                                StatusName = "正在进行中";
                            }
                        }
                        resp = new Respone() { code = "99", mes = "查看订单成功" };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "查看订单失败," + objResp.ErrorMessages.ErrorMessage.Value };
                        LogHelper.DoOperateLog(string.Format("Err：Agoda查询账单 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "查看订单失败" };
                    LogHelper.DoOperateLog(string.Format("Err：Agoda查询账单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Agoda接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "查看订单失败," + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Agoda查看订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //查询订单,拿到agoda订单号不等于这个订单已经确认,bookingdetail来查订单状态，直到订单确认为止
        public string Get_OrderDetail(string hotelID, string BookingNumber)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string postBoby = string.Empty;
                string StatusName = string.Empty;
                int status = 0;
                /*
                string[] BookingNumber = BookingNumbers.Split(',');
                string BookingIDXml = string.Empty;
                for (int i = 0; i < BookingNumber.Length; i++)
                {
                    BookingIDXml += string.Format(@"<BookingID>{0}</BookingID>", BookingNumber[i]);
                }*/
                string BookingIDXml = string.Format(@"<BookingID>{0}</BookingID>", BookingNumber);
                postBoby = string.Format(@"<BookingDetailsRequestV2 siteid=""{0}"" apikey=""{1}"" xmlns=""http://xml.agoda.com"" 
xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""> {2} </BookingDetailsRequestV2>",Username, Password, BookingIDXml);
                try
                {
                    result = WebReq(AgodaBookDetailUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio： Agoda查看订单 ,请求数据(Req) :{0} , 返回数据(Resp) ：{1} , Time:{2} ", postBoby, result, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "查看订单失败," + ex.ToString() };
                    LogHelper.DoOperateLog(string.Format("Err：Agoda查看订单Post数据 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                AgodaResp.BookingDetailsResponseV2 objResp = Common.Common.DESerializer<AgodaResp.BookingDetailsResponseV2>(result.Replace("xmlns=\"http://xml.agoda.com\" xmlns:tns=\"http://xml.agoda.com\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", "").Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        for (int j = 0; j < objResp.Bookings.Booking.Count; j++)
                        {
                            if (objResp.Bookings.Booking[j].Status == "BookingReceived")
                            {
                                StatusName = "待确认";
                                status = 2;
                            }
                            else if (objResp.Bookings.Booking[j].Status == "BookingCharged" || objResp.Bookings.Booking[j].Status == "BookingConfirmed")
                            {
                                StatusName = "确认";
                                status = 1;
                            }
                            else if (objResp.Bookings.Booking[j].Status == "BookingCancelledByCustomer")
                            {
                                StatusName = "取消";
                                status = 0;
                            }
                            else if (objResp.Bookings.Booking[j].Status == "Departed")
                            {
                                StatusName = "确认";//"订单已结账";
                                status = 1;
                            }
                            else
                            {
                                StatusName = objResp.Bookings.Booking[j].Status;
                                status = 99;
                            }
                            string confirmation_number = "";
                            if (objResp.Bookings.Booking[j].SupplierReference.ToLower() != "awaiting")
                            {
                                confirmation_number = objResp.Bookings.Booking[j].SupplierReference;
                            }
                            List<Guests> guests = new List<Guests>();
                            Guests guset = new Guests();
                            List<string> adultGivenName = new List<string>();
                            for (int n = 0; n < objResp.Bookings.Booking[j].GuestDetails.GuestDetail.Count; n++)
                            {
                                string LastName=objResp.Bookings.Booking[j].GuestDetails.GuestDetail[n].LastName;//姓
                                string FirstName = objResp.Bookings.Booking[j].GuestDetails.GuestDetail[n].FirstName;//名
                                adultGivenName.Add(FirstName + " " + LastName);
                            }
                            guset.adult_given_name = adultGivenName;
                            guests.Add(guset);
                            //Totalrate是不含surcharge的美金房价,payment是实际支付的含surcharge的支付货币金额(只有mandatory的surcharge)
                            string strUpdateSql = string.Format("update Agoda_Order set StatusCode='{0}',UpdateTime=GETDATE(),Status='{1}',StatusName='{2}',BookingConfirmationID='{3}' where BookingCode='{4}' AND HotelID='{5}'"
                            , objResp.Bookings.Booking[j].Status, status, StatusName, objResp.Bookings.Booking[j].SupplierReference, BookingNumber, hotelID);
                            con.Update(strUpdateSql);
                            string sql = string.Format(@"select top 1 * from Agoda_Order WITH(NOLOCK) where BookingCode='{0}' AND HotelID='{1}'", BookingNumber, hotelID);
                            DataTable dt = con.GetDataTable(sql);
                            string price = objResp.Bookings.Booking[j].Payment.PaymentRateInclusive[0].Value.AsTargetType<string>("");
                            string orderOriginalPrice = "CNY$" + objResp.Bookings.Booking[j].Payment.PaymentRateInclusive[0].Value.AsTargetType<string>("");
                            Reservation rev = new Reservation();
                            rev.hotel_id = hotelID;
                            rev.aic_reservation_id = BookingNumber;//dt.Rows[0]["BookingConfirmationID"].AsTargetType<string>("");
                            rev.partner_reservation_id = objResp.Bookings.Booking[j].Tag.AsTargetType<string>("");
                            rev.reservation_status = objResp.Bookings.Booking[j].Status;
                            rev.hotel_confirmation_number = confirmation_number;
                            rev.hotel_cancellation_number = "";
                            rev.special_request = "";
                            rev.check_in_date = objResp.Bookings.Booking[j].CheckInDate.AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                            rev.check_out_date = objResp.Bookings.Booking[j].CheckOutDate.AsTargetType<DateTime>(DateTime.MinValue).ToString("yyyy-MM-dd");
                            rev.adult_number = dt.Rows[0]["Adults"].AsTargetType<string>("");
                            rev.kids_number = dt.Rows[0]["Childs"].AsTargetType<string>("");
                            rev.total_rooms = dt.Rows[0]["RoomCount"].AsTargetType<string>("");
                            rev.total_amount = price;
                            rev.guests = guests;
                            rev.currency = orderOriginalPrice;
                            rev.cancellation_time = dt.Rows[0]["CancellationTime"].AsTargetType<string>("");
                            resp = new Respone() { code = "00", orderNum = objResp.Bookings.Booking[j].BookingID, Reservation = rev, confirmationNumber = confirmation_number, orderTotal = price, orderOriginalPrice = orderOriginalPrice, Status = StatusName, mes = "查询成功" };
                            //resp = new Respone() { code = "00", orderNum = objResp.Bookings.Booking[j].BookingID, orderTotal = "0", mes = "查询订单,该订单状态为:" + StatusName };
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "查看订单失败," + objResp.ErrorMessages.ErrorMessage.Value };
                        LogHelper.DoOperateLog(string.Format("Err：Agoda查看订单 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "查看订单失败" };
                    LogHelper.DoOperateLog(string.Format("Err：Agoda查看订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Agoda接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "查看订单失败," + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err：Agoda查看订单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //退单
        public string cancelBefore(string hotelID, string BookingNumber)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string postBoby = string.Empty; 
            try
            {
                postBoby = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <CancellationRequestV2 siteid=""{0}"" apikey=""{1}"" 
xmlns=""http://xml.agoda.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""> <BookingID>{2}</BookingID> </CancellationRequestV2>", 
Username, Password, BookingNumber);
                try
                {
                    respData = WebReq(AgodaCancelUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：Agoda退单 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Agoda退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
                    return "";
                }
                AgodaResp.CancellationResponseV2 objResp = Common.Common.DESerializer<AgodaResp.CancellationResponseV2>(respData.Replace("&", "").Replace("xmlns=\"http://xml.agoda.com\" xmlns:tns=\"http://xml.agoda.com\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        string PaymentMoney = objResp.CancellationSummary.Payment.PaymentRateInclusive.Value;
                        string RefundMoney = objResp.CancellationSummary.Refund.RefundRateInclusive.Value;
                        string CancellationNo = objResp.CancellationSummary.Reference;
                        return PaymentMoney + ";" + RefundMoney + ";" + CancellationNo;
                    }
                    else
                    {
                        return "";
                        LogHelper.DoOperateLog(string.Format("Err：Agoda取消订单 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", Status = "取消失败", mes = "取消订单失败" };
                    LogHelper.DoOperateLog(string.Format("Err：Agoda取消订单 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda退单 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", Status = "取消失败", mes = "取消订单失败，" + ex.ToString() };
            }
            return "";
        }

        //退单确认
        public string Cancel_Order(string hotelID, string BookingNumber)
        {
            Respone resp = new Respone();
            string respData = string.Empty;
            string postBoby = string.Empty;
            try
            {
                string strCancelBefore = cancelBefore(hotelID, BookingNumber);
                if (strCancelBefore == "")
                {
                    resp = new Respone() { code = "99", Status = "取消失败", mes = "取消订单失败" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                postBoby = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <ConfirmCancellationRequestV2 siteid=""{0}"" apikey=""{1}"" 
xmlns=""http://xml.agoda.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""> <BookingID>{2}</BookingID> <Reference>{3}</Reference> 
<CancelReason>{4}</CancelReason> <Refund> <RefundRateInclusive currency=""CNY"">{5}</RefundRateInclusive> </Refund> </ConfirmCancellationRequestV2>",
Username, Password, BookingNumber, strCancelBefore.Split(';')[2], 14, strCancelBefore.Split(';')[1]);
                try
                {
                    respData = WebReq(AgodaConfirmCancelUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：Agoda退单确认 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Agoda退单确认 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                AgodaResp.ConfirmCancellationResponseV2 objResp = Common.Common.DESerializer<AgodaResp.ConfirmCancellationResponseV2>(respData.Replace("&", "").Replace("xmlns=\"http://xml.agoda.com\" xmlns:tns=\"http://xml.agoda.com\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        string strUpdateSql = string.Format(@"update Agoda_Order set Status=0,UpdateTime=GETDATE(),CancellationTime=GETDATE(),StatusName='取消',StatusCode='ConfirmCancel',PaymentMoney='{0}',RefundMoney='{1}',
CancellationNo='{2}' where BookingCode='{3}' and hotelID='{4}' ",strCancelBefore.Split(';')[0],strCancelBefore.Split(';')[1],strCancelBefore.Split(';')[2], BookingNumber, hotelID);
                        con.Update(strUpdateSql);
                        resp = new Respone() { code = "00", orderNum = BookingNumber, Status = "取消", mes = "取消成功" };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", Status = "取消失败", mes = "取消订单失败," + objResp.ErrorMessages.ErrorMessage.Value };
                        LogHelper.DoOperateLog(string.Format("Err：Agoda退单确认 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", Status = "取消失败", mes = "取消订单失败" };
                    LogHelper.DoOperateLog(string.Format("Err：Agoda退单确认 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "Err");
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda退单确认 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", Status = "取消失败", mes = "取消订单失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //获取报价(外部接口)   开始日期与结束日期不能间隔14天
        public string GetRoomTypeOutPrice(string hotelID, string CheckInDate, string CheckoutDate, IntOccupancyInfo occupancy, int RoomCount, string rateplanId)
        {
            //int IsHave = 1;//是否有房
            string respData = string.Empty;
            string postBoby = string.Empty;
            int adults = occupancy.adults;
            int children = occupancy.children;
            string ChildrenAge = occupancy.childAges ?? "";
            string CheckIn = Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd");
            string Checkout = Convert.ToDateTime(CheckoutDate).ToString("yyyy-MM-dd");
            string StrKey = hotelID + "%" + CheckIn + "%" + Checkout + "%" + adults + "%" + children + "%" + ChildrenAge + "%" + RoomCount;
            int BookingDays = Convert.ToDateTime(CheckoutDate).Day - Convert.ToDateTime(CheckInDate).Day;//预订天数

            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            //DataTable dt = new DataTable();
            try
            {
                string sql = string.Format(@"select top 1 IsClose,UpdateTime,Response from CacheData.dbo.AgodaLink_RoomsCache WITH(NOLOCK) where StrKey='{0}'", StrKey);
                //DataSet ds = con.GetDataSet(sql);
                DataTable dt = con.GetDataTable(sql);//.Tables[0];
                //DataTable dtHotel = ds.Tables[1];
                int SeparateMinute = 6000;
                if (dt.Rows.Count > 0)
                {
                    SeparateMinute = DateTime.Now.Subtract(Convert.ToDateTime(dt.Rows[0]["UpdateTime"].AsTargetType<DateTime>(DateTime.MinValue))).TotalMinutes.AsTargetType<int>(0);//计算数据库数据时间与当前时间相差分钟数数
                }
                int SeparateDays = Convert.ToDateTime(CheckInDate).Subtract(DateTime.Now).TotalDays.AsTargetType<int>(0);//计算预订时间与当前时间相差天数
                //if (dtHotel.Rows.Count == 0)
                //{
                //    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                //}
                if ((rateplanId != null && rateplanId != "") || dt.Rows.Count == 0 || dt.Rows[0]["IsClose"].AsTargetType<int>(0) == 0 || (SeparateDays <= 2 && SeparateMinute > 5) || (SeparateDays > 2 && SeparateDays <= 7 && SeparateMinute > 15) || (SeparateDays > 7 && SeparateDays <= 10 && SeparateMinute > 30) || (SeparateDays > 10 && SeparateDays <= 20 && SeparateMinute > 60) || (SeparateDays > 20 && SeparateMinute > 240))
                {
                    //string SurchargeXml = "";
                    int Type = 4;//1 = City search (deprecated)   4 = Hotel search  6 = Hotel list 
                    if (occupancy.children == 0)
                    {
                        ChildrenAge = "";
                    }
                    else
                    {
                        string[] ArrayAges = occupancy.childAges.Split(',');
                        for (int t = 0; t < RoomCount; t++)
                        {
                            for (int i = 0; i < ArrayAges.Length; i++)
                            {
                                ChildrenAge += "<Age>" + ArrayAges[i].AsTargetType<int>(0) + "</Age>";
                            }
                        }
                        ChildrenAge = "<ChildrenAges>" + ChildrenAge + "</ChildrenAges>";
                    }
                    postBoby = string.Format(@"<?xml version=""1.0"" encoding=""utf-8"" ?> <AvailabilityRequestV2 siteid=""{0}"" apikey=""{1}""
xmlns=""http://xml.agoda.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""> <Type>{2}</Type> <Id>{3}</Id> <CheckIn>{4}</CheckIn> <CheckOut>{5}</CheckOut> 
<Rooms>{6}</Rooms> <Adults>{7}</Adults> <Children>{8}</Children> {9} <Language>zh-cn</Language> <Currency>CNY</Currency> </AvailabilityRequestV2>",
Username, Password, Type, hotelID, CheckInDate, CheckoutDate, RoomCount, adults * RoomCount, children * RoomCount, ChildrenAge);
                        try
                        {

                            //TimeSpan spanStartTime = new TimeSpan(DateTime.Now.Ticks);
                            respData = WebReq(AgodaSeachUrl, postBoby);

                            //TimeSpan spanEndTime = new TimeSpan(DateTime.Now.Ticks);
                            //TimeSpan ts = spanEndTime.Subtract(spanStartTime).Duration();

                            //计算时间间隔，求出调用接口所需要的时间
                            //string spanTime = ts.Hours.ToString() + "小时" + ts.Minutes.ToString() + "分" + ts.Seconds.ToString() + "秒" + ts.Milliseconds.ToString();

                            
                            if (rateplanId != null && rateplanId != "")
                            {
                                //LogHelper.DoOperateLog(string.Format("Studio：Agoda获取报价 ,请求数据 :{0} , 返回数据 ：{1} ，rateplanId:{2} ,接口花费时间：{3} , Time:{4} ", postBoby, respData,rateplanId,spanTime, DateTime.Now.ToString()), "Agoda接口");
                                LogHelper.DoOperateLog(string.Format("Studio：Agoda获取报价 ,请求数据 :{0} , 返回数据 ：{1} ，rateplanId:{2} , Time:{3} ", postBoby, respData, rateplanId, DateTime.Now.ToString()), "Agoda接口");
                            }
                            AgodaLink_RoomsCache roomsCache = new AgodaLink_RoomsCache();
                            roomsCache.StrKey = StrKey;
                            roomsCache.HotelId = hotelID;
                            roomsCache.Checkin = CheckIn.AsTargetType<DateTime>(DateTime.MinValue);
                            roomsCache.CheckOut = Checkout.AsTargetType<DateTime>(DateTime.MinValue);
                            //ssRoomsCache.Request = postBoby;
                            roomsCache.Response = respData;
                            roomsCache.CreatTime = DateTime.Now;
                            roomsCache.UpdateTime = DateTime.Now;
                            roomsCache.IsClose = 1;
                            roomsCache.EntityState = e_EntityState.Added;
                            if (dt.Rows.Count == 0)
                            {
                                con2.Save(roomsCache);
                            }
                            else
                            {
                                if (SeparateMinute >= 45)
                                {
                                    string updateRoomsCacheSql = string.Format(@"update AgodaLink_RoomsCache set Response='{0}',UpdateTime=GETDATE(),IsClose=1 where StrKey='{1}'", respData.Replace("'", "''"), StrKey);
                                    con2.Update(updateRoomsCacheSql);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:Agoda获取报价 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, respData, DateTime.Now.ToString()), "Err");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                }
                else
                {
                    respData = dt.Rows[0]["Response"].AsTargetType<string>("");
                }
                int IsSucceed = 1;// 0、通过数据库找到房型 1、有对应房型  2、没有对应房型  3、无报价
                rooms = RoomOutPrice(hotelID, CheckInDate, CheckoutDate, adults, children, ChildrenAge, RoomCount, rateplanId, respData);
                if (rateplanId != null && rateplanId != "" && rateplanId != "null" && rooms.Count == 0)
                {
                    IsSucceed = 2;//没有对应房型
                    if (respData.Contains("No search result"))
                    {
                        IsSucceed = 3;//无报价
                    }
                    string sql1 = string.Format(@"select top 1 IsClose,UpdateTime,Response from CacheData.dbo.AgodaLink_RoomsCache WITH(NOLOCK) where StrKey='{0}'", StrKey);
                    DataTable dt1 = con.GetDataTable(sql1);
                    respData = dt1.Rows[0]["Response"].AsTargetType<string>("");
                    rooms = RoomOutPrice(hotelID, CheckInDate, CheckoutDate, adults, children, ChildrenAge, RoomCount, rateplanId, respData);
                    if (rooms.Count > 0)
                    {
                        IsSucceed = 0;//通过数据库找到房型
                    }
                }
                if (rateplanId != null && rateplanId.Trim() != "" && rateplanId != "null")
                {
                    AgodaNoQuote anq = new AgodaNoQuote();
                    anq.HotelID = hotelID;
                    anq.RateplanId = rateplanId;
                    anq.CheckInDate = Convert.ToDateTime(CheckInDate);
                    anq.CheckOutDate = Convert.ToDateTime(CheckoutDate);
                    anq.Adults = adults;
                    anq.Children = children;
                    anq.ChildrenAge = ChildrenAge;
                    anq.RoomCount = RoomCount;
                    anq.CreatDate = DateTime.Now;
                    anq.IsSucceed = IsSucceed;
                    anq.StrKey = StrKey;
                    con.Save(anq);
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda获取报价 ,err:{0}  , 请求数据:{1} ,接口返回数据(Resp) ：{2},RateplanId:{3}, Time:{4} ", ex.Message, postBoby, respData, rateplanId, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }

        //预订前检查是否可订，及输出预订时需要的相关内容
        public string GetRecheckIsBook(string hotelID, string CheckInDate, string CheckoutDate, IntOccupancyInfo occupancy, int RoomCount, string rateplanId, decimal TotalMoney, string inOrderNum)
        {
            int IsCan = 0;
            List<roomPrice> listPrice = new List<roomPrice>();
            string result = string.Empty;
            string respData = string.Empty;
            string postBoby = string.Empty;
            int adults = occupancy.adults;
            int children = occupancy.children;
            string ChildrenAge = occupancy.childAges ?? "";
            string CheckIn = Convert.ToDateTime(CheckInDate).ToString("yyyy-MM-dd");
            string Checkout = Convert.ToDateTime(CheckoutDate).ToString("yyyy-MM-dd");
            string StrKey = hotelID + "%" + CheckIn + "%" + Checkout + "%" + adults + "%" + children + "%" + ChildrenAge + "%" + RoomCount;
            int BookingDays = Convert.ToDateTime(CheckoutDate).Day - Convert.ToDateTime(CheckInDate).Day;//预订天数

            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            //DataTable dt = new DataTable();
            try
            {
                int Type = 4;//1 = City search (deprecated)   4 = Hotel search  6 = Hotel list 
                if (occupancy.children == 0)
                {
                    ChildrenAge = "";
                }
                else
                {
                    string[] ArrayAges = occupancy.childAges.Split(',');
                    for (int t = 0; t < RoomCount; t++)
                    {
                        for (int i = 0; i < ArrayAges.Length; i++)
                        {
                            ChildrenAge += "<Age>" + ArrayAges[i].AsTargetType<int>(0) + "</Age>";
                        }
                    }
                    ChildrenAge = "<ChildrenAges>" + ChildrenAge + "</ChildrenAges>";
                }
                adults = adults * RoomCount;
                children = children * RoomCount;
                postBoby = string.Format(@"<?xml version='1.0' encoding='utf-8' ?> <AvailabilityRequestV2 siteid='{0}' apikey='{1}'
    xmlns='http://xml.agoda.com' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance'> <Type>{2}</Type> <Id>{3}</Id> <CheckIn>{4}</CheckIn> <CheckOut>{5}</CheckOut> 
    <Rooms>{6}</Rooms> <Adults>{7}</Adults> <Children>{8}</Children> <Language>zh-cn</Language> <Currency>CNY</Currency> </AvailabilityRequestV2>",
    Username, Password, Type, hotelID, CheckInDate, CheckoutDate, RoomCount, adults, children, ChildrenAge);
                try
                {
                    respData = WebReq(AgodaSeachUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：Agoda检查是否可订 ,请求数据 :{0} , 返回数据 ：{1} ，rateplanId:{2} ,inOrderNum:{3}, Time:{4} ", postBoby, respData, rateplanId,inOrderNum, DateTime.Now.ToString()), "Agoda接口");
                    //string updateRoomsCacheSql = string.Format(@"update AgodaLink_RoomsCache set Response='{0}',UpdateTime=GETDATE() where StrKey='{1}'", result.Replace("'", "''"), StrKey);
                    //con2.Update(updateRoomsCacheSql);
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Agoda检查是否可订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, respData, DateTime.Now.ToString()), "Err");
                    return "";
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Agoda检查是否可订 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return "";
            }
            result = RecheckIsBook(hotelID, CheckInDate, CheckoutDate, adults, children, ChildrenAge, RoomCount, rateplanId, TotalMoney, inOrderNum, respData);
            if (result == "")
            {
                string sql1 = string.Format(@"select top 1 IsClose,UpdateTime,Response from CacheData.dbo.AgodaLink_RoomsCache WITH(NOLOCK) where StrKey='{0}'", StrKey);
                DataTable dt1 = con.GetDataTable(sql1);
                respData = dt1.Rows[0]["Response"].AsTargetType<string>("");
                result = RecheckIsBook(hotelID, CheckInDate, CheckoutDate, adults, children, ChildrenAge, RoomCount, rateplanId, TotalMoney, inOrderNum, respData);
            }
            return result;
        }

        //预订  预订时，可以只写其中一名入住人信息
        public string Create_Order(string HotelID, string RateplanId, BookingInfo customers, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney)
        {
            Respone resp = new Respone();
            string postBoby = string.Empty;
            string respData = string.Empty;
            try
            {
                int Adults = customers.adults;
                int Childs = 0;
                string childAges = string.Empty;
                if (customers.childAge == null || customers.childAge == "null" || customers.childAge.Trim() == "")
                {
                    childAges = "";
                }
                else
                {
                    childAges = customers.childAge ?? "";
                    if (childAges != "")
                    {
                        Childs = (customers.childAge ?? "").Split(',').Length;
                    }
                }

                IntOccupancyInfo occupancy = new IntOccupancyInfo();
                occupancy.adults = Adults;
                occupancy.children = Childs;
                occupancy.childAges = childAges.TrimEnd(',');
                //判断是否可预订
                string strRecheckIsBook = GetRecheckIsBook(HotelID, beginTime, endTime, occupancy, roomNum, RateplanId, TotalMoney, inOrderNum);
                if (strRecheckIsBook == "")
                {
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败，该房不可预订" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                decimal price = strRecheckIsBook.Split(';')[2].AsTargetType<decimal>(0);
                decimal Surcharge = strRecheckIsBook.Split(';')[3].AsTargetType<decimal>(0);
                //string guID = Common.Common.getGUID();

                AgodaResp.GuestDetails guestDetails = new AgodaResp.GuestDetails();
                List<AgodaResp.GuestDetail> guestDetailList = new List<AgodaResp.GuestDetail>();
                for (int i = 0; i < customers.name.Split(',').Length; i++)
                {
                    AgodaResp.GuestDetail guestDetail = new AgodaResp.GuestDetail();
                    guestDetail.Title = "";
                    guestDetail.FirstName = customers.name.Split(',')[i].Split('/')[1];
                    guestDetail.LastName = customers.name.Split(',')[i].Split('/')[0];
                    guestDetail.CountryOfPassport = "CN";
                    //guestDetail.Gender = "";
                    //guestDetail.Age = customers[i].Customers[j].age;
                    guestDetailList.Add(guestDetail);
                }
                guestDetails.GuestDetail = guestDetailList;
                string guestDetailsXml = Common.Common.XmlSerializeNoTitle<AgodaResp.GuestDetails>(guestDetails);
                postBoby = string.Format(@"<?xml version=""1.0"" encoding=""UTF-8""?> <BookingRequestV3 siteid=""{0}"" apikey=""{1}"" xmlns=""http://xml.agoda.com"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" 
xsi:schemaLocation=""http://xml.agoda.com BookingRequestV3.xsd""> <BookingDetails searchid=""{2}"" tag=""{3}"" AllowDuplication=""true"" CheckIn=""{4}"" CheckOut=""{5}"">
<Hotel id=""{6}""><Rooms>{7}{8} </Room></Rooms> </Hotel> </BookingDetails> <CustomerDetail><Language>zh-cn</Language>
<Title>Mr.</Title><FirstName>Wei</FirstName><LastName>Huang</LastName><Email>jack@xiwantrip.com</Email><Phone><CountryCode></CountryCode><AreaCode></AreaCode>
<Number>13560061046</Number></Phone><Newsletter>true</Newsletter><IpAddress></IpAddress></CustomerDetail> <PaymentDetails> </PaymentDetails> </BookingRequestV3>",
Username, Password, strRecheckIsBook.Split(';')[0], inOrderNum, beginTime, endTime, HotelID, strRecheckIsBook.Split(';')[1],
guestDetailsXml.Replace("<GuestDetails><GuestDetail><GuestDetail>", "<GuestDetails><GuestDetail>").Replace("</GuestDetail></GuestDetail></GuestDetails>", "</GuestDetail></GuestDetails>"));
                try
                {
                    respData = WebReq(AgodaBookUrl, postBoby);
                    LogHelper.DoOperateLog(string.Format("Studio：Agoda预订 ,请求数据 :{0} , 返回数据 ：{1} ， Time:{2} ", postBoby, respData, DateTime.Now.ToString()), "Agoda接口");
                }
                catch (Exception ex)
                {
                    LogHelper.DoOperateLog(string.Format("Err:Agoda预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败失败，" + ex.ToString() };
                }
                AgodaResp.BookingResponseV3 objResp = Common.Common.DESerializer<AgodaResp.BookingResponseV3>(respData.Replace("&", "").Replace("xmlns=\"http://xml.agoda.com\" xmlns:tns=\"http://xml.agoda.com\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        Booking booking = new Booking();
                        //更新订单到数据库
                        string strUpdateSql = string.Format(@"update Agoda_Order set StatusCode='{0}',UpdateTime=GETDATE(),BookingConfirmationID='{1}',StatusName='{2}',
ClientPartnerAmount='{3}',RMBprice='{4}',Surcharge='{5}',BookingCode='{6}',Status='2',IsClose=1,Adults={7},Childs={8},ChildAges='{9}',Response='{10}'  where InOrderNum='{11}' and HotelId='{12}' ",
"BookingReceived", "", "待确认", price, price, Surcharge, objResp.BookingDetails.Booking.id, Adults, Childs, childAges.TrimEnd(','), respData.Replace("'", "''"), inOrderNum, HotelID);
                        con.Update(strUpdateSql);
                        resp = new Respone() { code = "00", orderNum = objResp.BookingDetails.Booking.id, orderTotal = price.ToString("f2"), orderOriginalPrice = "CNY$" + price.ToString("f2"), Status = "待确认", mes = "预订成功" };
                    }
                    else
                    {
                        string strUpdateSql = string.Format(@"update Agoda_Order set UpdateTime=GETDATE(),Status='0',Status = '失败',IsClose=1,Adults={0},Childs={1},ChildAges='{2}',Response='{3}'  where InOrderNum='{4}' and HotelId='{5}' ", Adults, Childs, childAges.TrimEnd(','), respData.Replace("'", "''"), inOrderNum, HotelID);
                        con.Update(strUpdateSql);
                        resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败," + objResp.ErrorMessages.ErrorMessage.Value };
                        LogHelper.DoOperateLog(string.Format("Err：Agoda预订 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                    }
                }
                else
                {
                    string strUpdateSql = string.Format(@"update Agoda_Order set UpdateTime=GETDATE(),Status='0',Status = '失败',IsClose=1,Adults={0},Childs={1},ChildAges='{2}',Response='{3}'  where InOrderNum='{4}' and HotelId='{5}' ", Adults, Childs, childAges.TrimEnd(','), respData.Replace("'", "''"), inOrderNum, HotelID);
                    con.Update(strUpdateSql);
                    LogHelper.DoOperateLog(string.Format("Err：Agoda预订 ,Err :{0} ， Time:{1} ", "预订xml解析出错", DateTime.Now.ToString()), "Agoda接口");
                    resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败" };
                }
            }
            catch (Exception ex)
            {
                string strUpdateSql = string.Format(@"update Agoda_Order set UpdateTime=GETDATE(),Status='0',Status = '失败',IsClose=1,Response='{0}' where InOrderNum='{1}' and HotelId='{2}' ", respData.Replace("'", "''"), inOrderNum, HotelID);
                con.Update(strUpdateSql);
                LogHelper.DoOperateLog(string.Format("Err:Agoda预订 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, postBoby, DateTime.Now.ToString()), "Err");
                resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败，" + ex.ToString() };
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        //报价解析方法
        public List<RoomOutPrice.Room> RoomOutPrice(string hotelID, string CheckInDate, string CheckoutDate, int adults, int children, string ChildrenAge, int RoomCount, string rateplanId, string respData)
        {
            string StrKey = hotelID + "%" + CheckInDate + "%" + CheckoutDate + "%" + adults + "%" + children + "%" + ChildrenAge + "%" + RoomCount;
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            int IsHave = 1;//是否有房
            int BookingDays = Convert.ToDateTime(CheckoutDate).Day - Convert.ToDateTime(CheckInDate).Day;//预订天数
            try
            {
                AgodaResp.AvailabilityLongResponseV2 objResp = Common.Common.DESerializer<AgodaResp.AvailabilityLongResponseV2>(respData.Replace("&", "").Replace("xmlns=\"http://xml.agoda.com\"", "").Replace("xmlns:tns=\"http://xml.agoda.com\"", "").Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"", "").Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        HotelPriceCollection objHotelPriceCol = new HotelPriceCollection();
                        for (int j = 0; j < objResp.Hotels.Hotel.Count; j++)
                        {
                            for (int k = 0; k < objResp.Hotels.Hotel[j].Rooms.Room.Count; k++)
                            {
                                int MaxAdults = adults;
                                int MaxChildren = children;
                                string BedType = "其他";
                                //roomID+promotionID+ratecategoryID+所有benefitID+cancellation code
                                string roomID = objResp.Hotels.Hotel[j].Rooms.Room[k].id;
                                //查看房型相关信息 
                                string sqlRoomTypeInFo = string.Format(@"select top 1 * from agodaRoomType WITH(NOLOCK) where RoomTypeID ='{0}'", roomID);
                                DataTable dtRoomTypeInFo = ControllerFactory.GetController().GetDataTable(sqlRoomTypeInFo);
                                if (dtRoomTypeInFo.Rows.Count > 0)
                                {
                                    MaxAdults = dtRoomTypeInFo.Rows[0]["MaxPerson"].AsTargetType<int>(0);
                                    BedType = dtRoomTypeInFo.Rows[0]["BedType"].AsTargetType<string>("");
                                }
                                string promotionID = objResp.Hotels.Hotel[j].Rooms.Room[k].promotionid ?? "";
                                string ratecategoryid = objResp.Hotels.Hotel[j].Rooms.Room[k].ratecategoryid ?? "";
                                string benefitID = string.Empty;
                                string cancellationCode = string.Empty;
                                string RateInclusive = string.Empty;//房间每一天的单价，当要求总价时：总价=当天单价*天数*房间数+额外费用
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo != null && objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate != null)
                                {
                                    RateInclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.inclusive ?? "";
                                }
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].Cancellation != null)
                                {
                                    cancellationCode = objResp.Hotels.Hotel[j].Rooms.Room[k].Cancellation.code ?? "";
                                }
                                //额外费用 当surcharge中charge=excluded时，这额外费用到店付款，surcharge中charge=mandatory要收，但房价一定得收
                                decimal SumSurchargeMandatory = 0;//额外费用总和，公司代收收费
                                decimal SumSurchargeExcluded = 0;//额外费用总和，个人到酒店付款
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo != null && objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges != null)
                                {
                                    for (int m = 0; m < objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge.Count; m++)
                                    {
                                        if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate != null)
                                        {
                                            if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].charge == "Mandatory")
                                            {
                                                SumSurchargeMandatory += objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive.AsTargetType<decimal>(0);
                                            }
                                            else
                                            {
                                                SumSurchargeExcluded += objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive.AsTargetType<decimal>(0);
                                            }
                                        }
                                    }
                                }

                                //早餐 BenefitID=20是双早,BenefitID=26是单早,BenefitID=1的情况下，normalbedding=几就是几份早餐。
                                int IsBreakfast = 0;
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits != null)
                                {
                                    for (int m = 0; m < objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit.Count; m++)
                                    {
                                        benefitID += objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id;
                                        if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].Name == "Breakfast")
                                        {
                                            if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id == "20")
                                            {
                                                IsBreakfast = 2;
                                            }
                                            else if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id == "26")
                                            {
                                                IsBreakfast = 1;
                                            }
                                            else if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id == "1")
                                            {
                                                IsBreakfast = objResp.Hotels.Hotel[j].Rooms.Room[k].MaxRoomOccupancy.normalbedding.AsTargetType<int>(0);
                                            }
                                        }
                                    }
                                }
                                string parentroomid = objResp.Hotels.Hotel[j].Rooms.Room[k].ParentRoom.id;//母房型ID
                                //判断房型唯一值
                                string rateplanCod = roomID + "-" + promotionID + "-" + ratecategoryid + "-" + benefitID + "-" + cancellationCode;//roomID+promotionID+ratecategoryID+所有benefitID+cancellation code
                                string RoomTypeId = objResp.Hotels.Hotel[j].Rooms.Room[k].id;//房型id
                                string RoomNameEN = objResp.Hotels.Hotel[j].Rooms.Room[k].name;// 房型名称
                                string RoomName = objResp.Hotels.Hotel[j].Rooms.Room[k].name;//中文名称

                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].StandardTranslation != null && objResp.Hotels.Hotel[j].Rooms.Room[k].StandardTranslation != "")
                                {
                                    RoomName = objResp.Hotels.Hotel[j].Rooms.Room[k].StandardTranslation;
                                }

                                decimal ClientPrice = RateInclusive.AsTargetType<decimal>(0) * RoomCount * BookingDays + SumSurchargeMandatory;
                                RoomOutPrice.Room temp = new RoomOutPrice.Room();
                                temp.ProductSource = "Agoda"; //产品来源 
                                temp.MroomNameEn = RoomNameEN;
                                temp.MroomName = temp.XRoomName = temp.RoomName = RoomName;// 房型名称
                                temp.MroomId = temp.RoomTypeId = RoomTypeId;//房型id

                                //当前请求的房间人数
                                temp.Adults = adults;
                                temp.Children = children;
                                temp.ChildAges = ChildrenAge.Replace(",", "|");
                                int PersonCount = adults + children;
                                int MaxPerson = MaxAdults;
                                //如果查询到的最大可入住成人数小于报价时的总人数输出报价时总人数
                                if (PersonCount > MaxAdults)
                                {
                                    MaxPerson = PersonCount;
                                }
                                //最大入住人数
                                temp.MaxPerson = MaxPerson;

                                temp.RatePlan = new List<RoomOutPrice.RatePlan>();
                                RoomOutPrice.RatePlan thisRatePlan = new RoomOutPrice.RatePlan();
                                string RatePlanName = "有早";
                                thisRatePlan.PayMent = "y";//支付方式。目前全部前台支付
                                int dtIsBreakfast = IsBreakfast;
                                if (dtIsBreakfast == 0)
                                {
                                    RatePlanName = RoomName + "(无早)" + "[内宾]";
                                }
                                thisRatePlan.RatePlanName = RatePlanName;  //价格计划名称
                                thisRatePlan.RoomName = RatePlanName;
                                thisRatePlan.XRatePlanName = RatePlanName;
                                thisRatePlan.RatePlanId = rateplanCod;  //价格计划ID

                                thisRatePlan.RoomTypeId = RoomTypeId;
                                temp.XRoomId = temp.RoomId = thisRatePlan.RoomId = string.Format("{0}_{1}", parentroomid, rateplanCod); //房型Id_价格计划ID
                                //temp.XRoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>(""));
                                //床型
                                if (BedType == "其他")
                                {
                                    if (RoomName.Contains("大床") || RoomName.Contains("双人床"))
                                    {
                                        BedType = "大床";
                                    }
                                    else if (RoomName.Contains("双床") || RoomName.Contains("2张单人床") || RoomName.Contains("两张单人床"))
                                    {
                                        BedType = "双床";
                                    }
                                    else if (RoomName.Contains("单人床"))
                                    {
                                        BedType = "单人床";
                                    }
                                    else
                                    {
                                        common.HotelBedType hbt = new common.HotelBedType();
                                        BedType = hbt.HotelBedTypeToName(RoomName);
                                    }
                                }

                                thisRatePlan.BedType = BedType;

                                //售价
                                decimal price = ClientPrice / RoomCount;//底总价，供应商收取费用时的价格
                                decimal RMBprice = ClientPrice / RoomCount;//指导总价，不一定有值，
                                //string[] ArrayPriceBreakdownDate = dt.Rows[j]["PriceBreakdownDate"].AsTargetType<string>("").Split(',');
                                //总的底价 
                                double sMoney = price.AsTargetType<double>(0);
                                bool available = true; // 指示入离日期所有天是否可订 有一天不可订为false
                                List<RoomOutPrice.Rate> rates = new List<RoomOutPrice.Rate>();
                                int days = (Convert.ToDateTime(CheckoutDate) - Convert.ToDateTime(CheckInDate)).Days;
                                for (int t = 0; t < BookingDays; t++)
                                {
                                    RoomOutPrice.Rate rate = new RoomOutPrice.Rate();
                                    rate.Date = Convert.ToDateTime(CheckInDate).AddDays(t);
                                    rate.Available = true;  //某天是否可订


                                    rate.HotelID = (price.ToString("f2").AsTargetType<decimal>(0) / days).AsTargetType<string>("");  //底价,供应商收取费用时的价格

                                    rate.MemberRate = (RMBprice.ToString("f2").AsTargetType<decimal>(0) / days);//指导总价，不一定有值
                                    rate.RetailRate = (RMBprice.ToString("f2").AsTargetType<decimal>(0) / days);//指导总价，不一定有值
                                    rates.Add(rate);
                                }


                                //补上没有的日期
                                if (days > rates.Count)
                                {
                                    available = false;
                                    List<RoomOutPrice.Rate> tempR = new List<RoomOutPrice.Rate>();
                                    for (int p = 0; p < days; p++)
                                    {
                                        DateTime date = Convert.ToDateTime(CheckInDate).AddDays(p).Date;
                                        var one = rates.Find(c => c.Date.Date == date);
                                        if (one != null)
                                        {
                                            tempR.Add(one);
                                        }
                                        else
                                        {
                                            RoomOutPrice.Rate newOne = new RoomOutPrice.Rate() { Available = false, InvStatusCode = "0", MemberRate = 0, Date = date };
                                            tempR.Add(newOne);
                                        }
                                    }
                                    rates = tempR;
                                }

                                thisRatePlan.Rates = rates;
                                thisRatePlan.Available = available;
                                thisRatePlan.SMoney = Convert.ToDecimal(sMoney);

                                thisRatePlan.CurrencyCode = "CNY" + ":" + 1;

                                decimal avgprice = 0;//均价
                                if (rates.Count > 0)
                                {
                                    avgprice = rates.Average(c => c.MemberRate);
                                    thisRatePlan.AveragePrice = avgprice.ToString("f2");
                                }

                                temp.CurrentAlloment = thisRatePlan.CurrentAlloment = objResp.Hotels.Hotel[j].Rooms.Room[k].RemainingRooms.AsTargetType<int>(0); //库存

                                //设置早餐数量
                                int breakfast = -1;
                                thisRatePlan.Breakfast = breakfast + "份早餐";
                                if (dtIsBreakfast == 0)
                                {
                                    thisRatePlan.Breakfast = 0 + "份早餐";
                                }
                                else
                                {
                                    thisRatePlan.Breakfast = adults + "份早餐";
                                }
                                string zaocan = thisRatePlan.Breakfast.Replace("份早餐", "");
                                if (!zaocan.Equals("0"))
                                {
                                    switch (zaocan)
                                    {
                                        case "1": thisRatePlan.RoomName = temp.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = RoomName + "(含单早)" + "[内宾]"; break;
                                        case "2": thisRatePlan.RoomName = temp.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = RoomName + "(含双早)" + "[内宾]"; break;
                                        case "3": thisRatePlan.RoomName = temp.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = RoomName + "(含三早)" + "[内宾]"; break;
                                        case "4": thisRatePlan.RoomName = temp.RoomName = thisRatePlan.RatePlanName = thisRatePlan.XRatePlanName = RoomName + "(含四早)" + "[内宾]"; break;
                                    }
                                }


                                temp.RoomPrice = Convert.ToInt32(avgprice);
                                temp.BedType = thisRatePlan.BedType;
                                temp.RatePlan.Add(thisRatePlan);
                                if (rateplanId != null && rateplanId.Trim() != "" && rateplanId != "null")
                                {
                                    if (rateplanCod == rateplanId)
                                    {
                                        rooms.Add(temp);
                                    }
                                }
                                else
                                {
                                    rooms.Add(temp);
                                }
                            }
                        }
                    }
                    else
                    {
                        if (objResp.ErrorMessages.ErrorMessage.Value == "No search result")
                        {
                            IsHave = 0;
                            string updateRoomsCacheSql = string.Format(@"update AgodaLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                            con2.Update(updateRoomsCacheSql);
                        }
                        //LogHelper.DoOperateLog(string.Format("Err：Agoda获取报价 ,err:{0} , Time:{1} ", objResp.ErrorMessages.ErrorMessage.Value, DateTime.Now.ToString()), "Agoda接口");
                        //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                }
                else
                {
                    //string updateRoomsCacheSql = string.Format(@"update AgodaLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                    //con2.Update(updateRoomsCacheSql);
                    //return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
                
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("ERR：Agoda报价解析方法,respData:{0} , rateplanId：{1}， Time:{2} ",respData, rateplanId, DateTime.Now.ToString()), "ERR");
            }
            return rooms;
        }

        //解析预订前的方法
        public string RecheckIsBook(string hotelID, string CheckInDate, string CheckoutDate, int adults, int children, string ChildrenAge, int RoomCount, string rateplanId, decimal TotalMoney, string inOrderNum, string respData)
        {
            string result = "";
            string StrKey = hotelID + "%" + CheckInDate + "%" + CheckoutDate + "%" + adults + "%" + children + "%" + ChildrenAge + "%" + RoomCount;
            int IsCan = 0;
            List<roomPrice> listPrice = new List<roomPrice>();
            int BookingDays = Convert.ToDateTime(CheckoutDate).Day - Convert.ToDateTime(CheckInDate).Day;//预订天数
            try
            {
                AgodaResp.AvailabilityLongResponseV2 objResp = Common.Common.DESerializer<AgodaResp.AvailabilityLongResponseV2>(respData.Replace("&", "").Replace("xmlns=\"http://xml.agoda.com\"", "").Replace("xmlns:tns=\"http://xml.agoda.com\"", "").Replace("xmlns:xs=\"http://www.w3.org/2001/XMLSchema\"", "").Replace("xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessages == null)
                    {
                        for (int j = 0; j < objResp.Hotels.Hotel.Count; j++)
                        {
                            for (int k = 0; k < objResp.Hotels.Hotel[j].Rooms.Room.Count; k++)
                            {
                                string roomID = objResp.Hotels.Hotel[j].Rooms.Room[k].id;
                                string promotionID = objResp.Hotels.Hotel[j].Rooms.Room[k].promotionid;
                                string ratecategoryid = objResp.Hotels.Hotel[j].Rooms.Room[k].ratecategoryid;
                                string benefitID = string.Empty;
                                string cancellationCode = string.Empty;
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].Cancellation != null)
                                {
                                    cancellationCode = objResp.Hotels.Hotel[j].Rooms.Room[k].Cancellation.code ?? "";
                                }
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits != null)
                                {
                                    for (int m = 0; m < objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit.Count; m++)
                                    {
                                        benefitID += objResp.Hotels.Hotel[j].Rooms.Room[k].Benefits.Benefit[m].id;
                                    }
                                }
                                //额外费用 当surcharge中charge=excluded时，这额外费用到店付款，surcharge中charge=mandatory要收，但房价一定得收
                                decimal SumSurchargeMandatory = 0;//额外费用总和，公司代收收费
                                decimal SumSurchargeExcluded = 0;//额外费用总和，个人到酒店付款
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo != null && objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges != null)
                                {
                                    for (int m = 0; m < objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge.Count; m++)
                                    {
                                        if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate != null)
                                        {
                                            if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].charge == "Mandatory")
                                            {
                                                SumSurchargeMandatory += objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive.AsTargetType<decimal>(0);
                                            }
                                            else
                                            {
                                                SumSurchargeExcluded += objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive.AsTargetType<decimal>(0);
                                            }
                                        }
                                    }
                                }
                                string RateExclusive = string.Empty;
                                string RateTax = string.Empty;
                                string RateFees = string.Empty;
                                string RateInclusive = string.Empty;
                                if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo != null && objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate != null)
                                {
                                    RateExclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.exclusive ?? "";
                                    RateTax = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.tax ?? "";
                                    RateFees = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.fees ?? "";
                                    RateInclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Rate.inclusive ?? "";
                                }
                                decimal ClientPrice = RateInclusive.AsTargetType<decimal>(0) * RoomCount * BookingDays + SumSurchargeMandatory;//公司要收的总价格

                                string rateplanCod = roomID + "-" + promotionID + "-" + ratecategoryid + "-" + benefitID + "-" + cancellationCode;

                                if (rateplanCod == rateplanId && TotalMoney >= ClientPrice)
                                {
                                    string strResult = string.Empty;


                                    string strRoom = string.Empty;
                                    string strRate = string.Empty;
                                    string roomBooknowpaylaterdate = objResp.Hotels.Hotel[j].Rooms.Room[k].booknowpaylaterdate ?? "";
                                    string roomPromotionid = objResp.Hotels.Hotel[j].Rooms.Room[k].promotionid ?? "";
                                    string roomid = objResp.Hotels.Hotel[j].Rooms.Room[k].id ?? "";
                                    string roomName = objResp.Hotels.Hotel[j].Rooms.Room[k].name ?? "";
                                    string roomLineitemid = objResp.Hotels.Hotel[j].Rooms.Room[k].lineitemid ?? "";
                                    string roomRateplan = objResp.Hotels.Hotel[j].Rooms.Room[k].rateplan ?? "";
                                    string roomRatetype = objResp.Hotels.Hotel[j].Rooms.Room[k].ratetype ?? "";
                                    string roomCurrency = objResp.Hotels.Hotel[j].Rooms.Room[k].currency ?? "";
                                    string roomModel = objResp.Hotels.Hotel[j].Rooms.Room[k].model ?? "";
                                    string roomRatecategoryid = objResp.Hotels.Hotel[j].Rooms.Room[k].ratecategoryid ?? "";
                                    string roomBlockid = objResp.Hotels.Hotel[j].Rooms.Room[k].blockid ?? "";


                                    if (objResp.Hotels.Hotel[j].Rooms.Room[k].id != null && objResp.Hotels.Hotel[j].Rooms.Room[k].id != "")
                                    {
                                    }
                                    if (roomBooknowpaylaterdate == "" && roomPromotionid == "")
                                    {
                                        strRoom = string.Format("<Room id=\"{0}\" name=\"{1}\" lineitemid=\"{2}\" rateplan=\"{3}\" ratetype=\"{4}\" currency=\"{5}\" model=\"{6}\" ratecategoryid=\"{7}\" blockid=\"{8}\" count=\"{9}\" adults=\"{10}\" children=\"{11}\">"
    , roomid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid, roomBlockid, RoomCount, adults, children);
                                    }
                                    else if (roomBooknowpaylaterdate == "")
                                    {
                                        strRoom = string.Format("<Room id=\"{0}\" promotionid=\"{1}\" name=\"{2}\" lineitemid=\"{3}\" rateplan=\"{4}\" ratetype=\"{5}\" currency=\"{6}\" model=\"{7}\" ratecategoryid=\"{8}\" blockid=\"{9}\" count=\"{10}\" adults=\"{11}\" children=\"{12}\">"
    , roomid, roomPromotionid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid,
    roomBlockid, RoomCount, adults, children);
                                    }
                                    else if (roomPromotionid == "")
                                    {
                                        strRoom = string.Format("<Room id=\"{0}\" name=\"{1}\" lineitemid=\"{2}\" rateplan=\"{3}\" ratetype=\"{4}\" currency=\"{5}\" model=\"{6}\" ratecategoryid=\"{7}\" blockid=\"{8}\"  count=\"{9}\" adults=\"{10}\" children=\"{11}\">"
    , roomid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid, roomBlockid, RoomCount, adults, children);
                                    }
                                    else
                                    {
                                        strRoom = string.Format("<Room id=\"{0}\" promotionid=\"{1}\" name=\"{2}\" lineitemid=\"{3}\" rateplan=\"{4}\" ratetype=\"{5}\" currency=\"{6}\" model=\"{7}\" ratecategoryid=\"{8}\" blockid=\"{9}\" count=\"{10}\" adults=\"{11}\" children=\"{12}\">"
    , roomid, roomPromotionid, roomName, roomLineitemid, roomRateplan, roomRatetype, roomCurrency, roomModel, roomRatecategoryid, roomBlockid, RoomCount, adults, children);
                                    }

                                    strRate = string.Format("<Rate exclusive=\"{0}\" tax=\"{1}\" fees=\"{2}\" inclusive=\"{3}\"/>", RateExclusive, RateTax, RateFees, RateInclusive);
                                    string SurchargeXml = "";
                                    string strSurcharge = "";
                                    if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo != null && objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges != null)
                                    {
                                        for (int m = 0; m < objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge.Count; m++)
                                        {
                                            if (objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate != null)
                                            {
                                                string surchargeID = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].id ?? "";
                                                string surchargeExclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.exclusive ?? "";
                                                string surchargeTax = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.tax ?? "";
                                                string surchargeFees = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.fees ?? "";
                                                string surchargeInclusive = objResp.Hotels.Hotel[j].Rooms.Room[k].RateInfo.Surcharges.Surcharge[m].Rate.inclusive ?? "";
                                                strSurcharge += string.Format("<Surcharge id=\"{0}\"><Rate exclusive=\"{1}\" tax=\"{2}\" fees=\"{3}\" inclusive=\"{4}\" /></Surcharge>"
                                                    , surchargeID, surchargeExclusive, surchargeTax, surchargeFees, surchargeInclusive);
                                            }
                                        }
                                    }
                                    if (strSurcharge != "")
                                    {
                                        SurchargeXml = "<Surcharges>" + strSurcharge + "</Surcharges>";
                                    }


                                    string searchid = objResp.searchid;
                                    string RoomText = strRoom + strRate + ChildrenAge + SurchargeXml;
                                    strResult = searchid + ";" + RoomText + ";" + ClientPrice + ";" + SumSurchargeExcluded;

                                    roomPrice objPrice = new roomPrice();
                                    objPrice.result = strResult;
                                    objPrice.offerPrice = ClientPrice;
                                    listPrice.Add(objPrice);
                                    IsCan = 1;
                                }
                            }
                        }
                    }
                    else
                    {
                        string updateRoomsCacheSql = string.Format(@"update AgodaLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                        con2.Update(updateRoomsCacheSql);
                        return "";
                    }
                }
                else
                {
                    string updateRoomsCacheSql = string.Format(@"update AgodaLink_RoomsCache set IsClose=0,UpdateTime=GETDATE() where StrKey='{0}'", StrKey);
                    con2.Update(updateRoomsCacheSql);
                    return "";
                }
                if (IsCan == 1)
                {
                    result = listPrice.OrderBy(x => x.offerPrice).Take(1).ToList()[0].result;
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("ERR：Agoda解析预订前的方法，该订单不可预订 , inOrderNum ：{0} ，rateplanId：{1}， Time:{2} ", inOrderNum, rateplanId, DateTime.Now.ToString()), "Agoda接口");
                    return "";
                }
            }
            catch(Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("ERR：Agoda解析预订前的方法,err:{0},respData:{1} , rateplanId：{2}， Time:{3} ", ex.ToString(), respData, rateplanId, DateTime.Now.ToString()), "ERR");
                return "";
            }
            return result;
        }

        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public string orderTotal { get; set; }//预订总额
            public string orderOriginalPrice { get; set; }//预订原始总额
            public string Status { get; set; }//状态
            public Reservation Reservation { get; set; }
            public string mes { get; set; }//成功返回成功，失败返回失败原因
            public string confirmationNumber { get; set; }//确认号
        }

        public class Tags
        {
            public List<string> Tag { get; set; }
        }

        /// <summary>
        /// WebReq函数
        /// </summary>
        /// <param name="url"></param>
        /// <param name="body">请求内容</param>
        /// <returns></returns>
        public static string WebReq(string url, string body)
        {
            string _returnstr = "";
            //发起请求
            //处理HttpWebRequest访问https有安全证书的问题（ 请求被中止: 未能创建 SSL/TLS 安全通道。）
            ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Headers.Add("Authorization", "1811246:49861f3b-1916-4065-a6fe-1f68644948e7");
            webRequest.ContentType = "text/xml; charset=utf-8";
            webRequest.Method = "POST";
            using (Stream requestStream = webRequest.GetRequestStream())
            {
                byte[] paramBytes = Encoding.UTF8.GetBytes(body);
                requestStream.Write(paramBytes, 0, paramBytes.Length);
            }
            //响应
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    _returnstr = myStreamReader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：Agoda(WebReq函数) ,错误信息 :{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                _returnstr = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
            }
            return _returnstr;
        }
        public class roomPrice
        {
            public decimal offerPrice { get; set; }
            public string result { get; set; }
        }

        /// <summary>
        /// 得到随机日期
        /// </summary>
        /// <param name="time1">起始日期</param>
        /// <param name="time2">结束日期</param>
        /// <returns>间隔日期之间的 随机日期</returns>
        public static DateTime GetRandomTime(DateTime time1, DateTime time2)
        {
            Random random = new Random();
            DateTime minTime = new DateTime();
            DateTime maxTime = new DateTime();

            System.TimeSpan ts = new System.TimeSpan(time1.Ticks - time2.Ticks);

            // 获取两个时间相隔的秒数
            double dTotalSecontds = ts.TotalSeconds;
            int iTotalSecontds = 0;

            if (dTotalSecontds > System.Int32.MaxValue)
            {
                iTotalSecontds = System.Int32.MaxValue;
            }
            else if (dTotalSecontds < System.Int32.MinValue)
            {
                iTotalSecontds = System.Int32.MinValue;
            }
            else
            {
                iTotalSecontds = (int)dTotalSecontds;
            }


            if (iTotalSecontds > 0)
            {
                minTime = time2;
                maxTime = time1;
            }
            else if (iTotalSecontds < 0)
            {
                minTime = time1;
                maxTime = time2;
            }
            else
            {
                return time1;
            }

            int maxValue = iTotalSecontds;

            if (iTotalSecontds <= System.Int32.MinValue)
                maxValue = System.Int32.MinValue + 1;

            int i = random.Next(System.Math.Abs(maxValue));

            return minTime.AddSeconds(i);
        }
        /// <summary>
        /// WebReq函数
        /// </summary>
        /// <param name="url"></param>
        /// <param name="body">请求内容</param>
        /// <returns></returns>
        public string WebPost(string url, string body)
        {
            string _returnstr = "";
            //发起请求
            //处理HttpWebRequest访问https有安全证书的问题（ 请求被中止: 未能创建 SSL/TLS 安全通道。）
            ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            WebRequest webRequest = WebRequest.Create(url);
            webRequest.Headers.Add("Authorization", "1811246:49861f3b-1916-4065-a6fe-1f68644948e7");
            webRequest.ContentType = "text/xml; charset=utf-8";
            webRequest.Method = "POST";
            using (Stream requestStream = webRequest.GetRequestStream())
            {
                byte[] paramBytes = Encoding.UTF8.GetBytes(body);
                requestStream.Write(paramBytes, 0, paramBytes.Length);
            }
            //响应
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    _returnstr = myStreamReader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：Agoda(WebReq函数) ,错误信息 :{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                _returnstr = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
            }
            return _returnstr;
        }
    }
}