﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using XiWan.DALFactory;
using System.Text.RegularExpressions;
using System.Collections;
using XiWan.DALFactory.Model;
using XiWan.Data.Redis;
using Newtonsoft.Json;
using System.Xml;
using XiWan.Data.TravPax;
using XiWan.Model.Entities;

namespace XiWan.Data
{
    public class TravPaxDAL
    {
        string USDtoRMBrate = CCommon.GetWebConfigValue("USDtoRMBrate");//美元转为人民币汇率
        string EURtoRMBrate = CCommon.GetWebConfigValue("EURtoRMBrate");//欧元转为人民币汇率
        string SGDtoRMBrate = CCommon.GetWebConfigValue("SGDtoRMBrate");//新加坡元转为人民币汇率
        string HKDtoRMBrate = CCommon.GetWebConfigValue("HKDtoRMBrate");//港币转为人民币汇率
        string TravPaxCacheTime = CCommon.GetWebConfigValue("TravPaxCacheTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);

        #region 初始化本类
        private static TravPaxDAL _Instance;
        public static TravPaxDAL Instance
        {
            get
            {
                return new TravPaxDAL();
            }
        }
        #endregion
        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationality()
        {
            string sql = "select ID,NAME from TravPaxNationality ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestination(string strName)
        {
            string sql = string.Format(@"SELECT (CITYNAME+','+ CITY_DESTINATIONNAME +',' + NAME) as name,(CITY_ID+','+COUNTRY_CODE) as value FROM TravPaxCityDestination where CITYNAME + CITY_DESTINATIONNAME + NAME like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 通过城市查询酒店
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="CountryCode"></param>
        /// <param name="CityID"></param>
        /// <param name="NationalityID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="ddlRoom"></param>
        /// <param name="rooms"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string SearchCity(string url, string travPaxUsername, string travPaxPassword, string CountryCode,string CityID,string NationalityID, string beginTime, string endTime, int ddlRoom, string rooms, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            string result = string.Empty;
            string[] ArrayRoom = rooms.TrimEnd(';').Split(';');
            string[] ArrayChilds;
            string SpendTime = string.Empty;
            try
            {
                TravSearchCityReq.CitySearchReq objReq = new TravSearchCityReq.CitySearchReq();
                objReq.Username = travPaxUsername;
                objReq.Password = travPaxPassword;
                objReq.CheckInDate = beginTime;
                objReq.CheckOutDate = endTime;
                objReq.CountryID = CountryCode;
                objReq.CityID = CityID.AsTargetType<int>(0);
                objReq.NationalityID = NationalityID.AsTargetType<int>(0);

                List<TravSearchCityReq.RoomRequest> roomListReq = new List<TravSearchCityReq.RoomRequest>();
                for (int i = 0; i < ddlRoom; i++)
                {
                    string Age = string.Empty;
                    ArrayChilds = ArrayRoom[i].Split(',');
                    if (ArrayChilds[1] == "1")
                    {
                        Age += string.Format(@"<Age>{0}</Age>", ArrayChilds[2]);
                    }
                    else if (ArrayChilds[1] == "2")
                    {
                        Age += string.Format(@"<Age>{0}</Age><Age>{1}</Age>", ArrayChilds[2], ArrayChilds[3]);
                    }

                    TravSearchCityReq.RoomRequest roomObj = new TravSearchCityReq.RoomRequest();
                    roomObj.Adults = ArrayChilds[0].AsTargetType<int>(0);
                    roomObj.Childs = ArrayChilds[1].AsTargetType<int>(0);
                    roomObj.ChildsAge = Age;

                    roomListReq.Add(roomObj);
                }
                objReq.Rooms = roomListReq;
                string strXml = Common.Common.XmlSerializeNoTitle<TravSearchCityReq.CitySearchReq>(objReq).Replace("&lt;", "<").Replace("&gt;", ">");

                string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                //请求接口
                try
                {
                    DateTime beforDT = System.DateTime.Now;

                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax通过城市查询酒店, 请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax通过城市查询酒店 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    Respone resp = new Respone() { code = "99", mes = "失败" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax通过城市查询酒店 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return SpendTime;
        }
        /// <summary>
        /// 获取报价
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="countryID"></param>
        /// <param name="cityID"></param>
        /// <param name="referenceClient"></param>
        /// <param name="nationalityID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="ddlRoom"></param>
        /// <param name="rooms"></param>
        /// <param name="input_page"></param>
        /// <param name="input_row"></param>
        /// <returns></returns>
        public void GetRealHotelSQuote(string url, string travPaxUsername, string travPaxPassword, string hotelID,string NationalityID, string beginTime, string endTime, int ddlRoom,int Adults,int Childs, string rooms, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            try
            {
                string result = string.Empty;
                string[] ArrayRoom = rooms.TrimEnd(';').Split(';');
                string[] ArrayChilds;

                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0} ", hotelID);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);

                HotelQuoteReq.HotelSearchReq objReq = new HotelQuoteReq.HotelSearchReq();
                objReq.Username = travPaxUsername;
                objReq.Password = travPaxPassword;
                objReq.ReferenceClient = "";
                objReq.CheckInDate = beginTime;
                objReq.CheckOutDate = endTime;
                objReq.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                objReq.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                objReq.HotelID = hotelID.AsTargetType<int>(0);
                objReq.NationalityID = NationalityID.AsTargetType<int>(0);

                List<HotelQuoteReq.RoomRequest> roomListReq = new List<HotelQuoteReq.RoomRequest>();
                for (int i = 0; i < ddlRoom; i++)
                {
                    string Age = string.Empty;
                    ArrayChilds = ArrayRoom[i].Split(',');
                    if (ArrayChilds[1] == "1")
                    {
                        Age += string.Format(@"<Age>{0}</Age>", ArrayChilds[2]);
                    }
                    else if (ArrayChilds[1] == "2")
                    {
                        Age += string.Format(@"<Age>{0}</Age><Age>{1}</Age>", ArrayChilds[2], ArrayChilds[3]);
                    }

                    HotelQuoteReq.RoomRequest roomObj = new HotelQuoteReq.RoomRequest();
                    roomObj.Adults = ArrayChilds[0].AsTargetType<int>(0);
                    roomObj.Childs = ArrayChilds[1].AsTargetType<int>(0);
                    roomObj.ChildsAge = Age;

                    roomListReq.Add(roomObj);
                }
                objReq.Rooms = roomListReq;
                string strXml = Common.Common.XmlSerializeNoTitle<HotelQuoteReq.HotelSearchReq>(objReq).Replace("&lt;", "<").Replace("&gt;", ">");

                string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                //请求接口
                try
                {
                    DateTime beforDT = System.DateTime.Now;

                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio：TravPax获取报价接口 ,请求数据 :{0} , 返回数据 ：{1} ，SpendTime : {2} Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    return ;
                }
                HotelSearchResp objResp = Common.Common.DESerializer<HotelSearchResp>(result);

                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return ;
                    }
                    List<HotelSearchResp.RoomClass> r = Common.Common.DESerializer<HotelSearchResp>(result.Replace("&", "")).Hotels.Hotel.RoomClasses.RoomClass.Where(x => x.Status.Contains("AL")).ToList();
                    HotelPriceCollection objCollection = new HotelPriceCollection();
                    if (r != null)
                    {
                        string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' AND Platform='TravPax' AND Adults={1} AND Childs={2} AND CheckInDate='{3}' AND CheckOutDate='{4}'",
hotelID, Adults, Childs, beginTime, endTime);
                        con.Update(updateHotelPriceSql);
                        for (int i = 0; i < r.Count; i++)
                        {
                            string ClientCurrencyCode = r[i].CurrencyCode;
                            decimal ClientPrice = r[i].TotalPrice;
                            string toRMBrate = string.Empty;
                            if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                            {
                                toRMBrate = USDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "EUR")
                            {
                                toRMBrate = EURtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "SGD")
                            {
                                toRMBrate = SGDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "HKD")
                            {
                                toRMBrate = HKDtoRMBrate;
                            }
                            //售价
                            decimal RMBprice = ClientPrice * toRMBrate.AsTargetType<decimal>(0);
                            HotelPrice obj = new HotelPrice();
                            obj.HotelID = hotelID;
                            obj.HotelNo = "";
                            obj.Platform = "TravPax";
                            obj.ClassName = r[i].ClassName;
                            obj.RMBprice = RMBprice;
                            obj.Price = r[i].TotalPrice;
                            obj.CurrencyCode = r[i].CurrencyCode;
                            if (r[i].Rooms.Room[0].BBCode == "BB")
                            {
                                obj.IsBreakfast = 1;
                            }
                            else
                            {
                                obj.IsBreakfast = 0;
                            }
                            obj.ReferenceClient = objResp.ReferenceClient;
                            obj.RoomCount = ddlRoom;
                            obj.Adults = Adults;
                            obj.Childs = Childs;
                            obj.CheckInDate = Convert.ToDateTime(beginTime);
                            obj.CheckOutDate = Convert.ToDateTime(endTime);
                            obj.IsClose = 1;
                            obj.CreateTime = DateTime.Now;
                            obj.UpdateTime = DateTime.Now;
                            con.Save(obj);

                            HotelPriceDetailCollection hpdl = new HotelPriceDetailCollection();
                            List<HotelSearchResp.Room> roomList = r[i].Rooms.Room;
                            if (roomList != null)
                            {
                                for (int j = 0; j < roomList.Count; j++)
                                {
                                    HotelPriceDetail hpd = new HotelPriceDetail();
                                    hpd.HotelPriceID = obj.ID;
                                    hpd.HotelID = hotelID;
                                    hpd.RoomID = roomList[j].RoomID.ToString();
                                    hpd.RoomName = roomList[j].RoomName;
                                    hpd.Status = roomList[j].Status;
                                    hpd.BBCode = roomList[j].BBCode;
                                    hpd.TotalPrice = roomList[j].TotalPrice;
                                    hpd.RoomCurrencyCode = roomList[j].CurrencyCode;
                                    hpd.AgreementID = roomList[j].AgreementID;
                                    hpd.Adults = roomList[j].Adults;
                                    hpd.Childs = roomList[j].Childs;
                                    string strAge = string.Empty;
                                    for (int k = 0; k < roomList[j].ChildsAge.Age.Count; k++)
                                    {
                                        strAge += roomList[j].ChildsAge.Age[k] + ",";
                                    }
                                    hpd.Age = strAge.TrimEnd(',');
                                    hpd.SupplierRefID = roomList[j].SupplierRef.SupplierRef.ID;
                                    hpd.SupplierRefValue = roomList[j].SupplierRef.SupplierRef.Value;
                                    string PriceBreakdownDate = string.Empty;
                                    string PriceBreakdownPrice = string.Empty;
                                    for (int k = 0; k < roomList[j].RoomPriceBreak.DailyRate.Count; k++)
                                    {
                                        PriceBreakdownDate += roomList[j].RoomPriceBreak.DailyRate[k].DateOfRate.ToString("yyyy-MM-dd") + ",";
                                        PriceBreakdownPrice += roomList[j].RoomPriceBreak.DailyRate[k].DailyRate + ",";
                                    }
                                    hpd.DateOfRate = PriceBreakdownPrice.TrimEnd(',');
                                    hpd.DailyCurrencyCode = roomList[j].RoomPriceBreak.DailyRate[0].CurrencyCode;
                                    hpd.DailyRate = PriceBreakdownDate.TrimEnd(',');
                                    hpd.IsClose = 1;
                                    hpd.CreateTime = DateTime.Now;
                                    hpd.UpdateTime = DateTime.Now;
                                    hpdl.Add(hpd);
                                }
                                con.Save(hpdl);
                            }

                        }
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "r != null", strXml, DateTime.Now.ToString()), "TravPax接口");
                        return;
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                    return;
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
        }

        public DataTable GetRealHotelDetail(string url, string travPaxUsername, string travPaxPassword, string hotelID, string countryID, string cityID, string referenceClient, string nationalityID, string beginTime, string endTime, int ddlRoom, string rooms, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            DataTable dt = new DataTable();
            try
            {
                string result = string.Empty;
                string[] ArrayRoom = rooms.Split(';');
                string[] ArrayChilds;

                string strXml = string.Format(@"<HotelSearchReq> <Username>{0}</Username> <Password>{1}</Password> <ReferenceClient>{2}</ReferenceClient>
<CheckInDate>{3}</CheckInDate> <CheckOutDate>{4}</CheckOutDate> <CountryID>{5}</CountryID> <CityID>{6}</CityID> <HotelID>{7}</HotelID> <NationalityID>{8}</NationalityID> <Rooms>  "
                    , travPaxUsername, travPaxPassword, "", beginTime, endTime, countryID, cityID, hotelID, nationalityID);
                string childXml = string.Empty;
                for (int i = 0; i < ddlRoom; i++)
                {
                    ArrayChilds = ArrayRoom[i].Split(',');
                    if (ArrayChilds[1] == "1")
                    {
                        childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2]);
                    }
                    else if (ArrayChilds[1] == "2")
                    {
                        childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> <Age>{3}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2], ArrayChilds[3]);
                    }
                    else
                    {
                        childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge/> </RoomRequest>", ArrayChilds[0], ArrayChilds[1]);
                    }
                }
                string hotelSearchXml = strXml + childXml + " </Rooms> </HotelSearchReq>";
                string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(hotelSearchXml), btnPost);
                //请求接口
                try
                {
                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", hotelSearchXml, result, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, hotelSearchXml, DateTime.Now.ToString()), "Err");
                }
                if (result.Contains("<ErrorMessage>"))
                {
                    return dt;
                }
                else
                {
                    Regex regex = new Regex("<RoomClasses>");//以RoomClasses分割
                    string[] bit = regex.Split(result);
                    if (bit.Length > 1)
                    {
                        result = bit[1];
                        Regex regexHotel = new Regex("</RoomClasses>");
                        bit = regexHotel.Split(result);
                        result = bit[0];
                        result = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <ArrayOfRoomClass xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" + result + "</ArrayOfRoomClass>");
                        List<HotelSearchResp.RoomClass> r = Common.Common.DESerializer<List<HotelSearchResp.RoomClass>>(result);
                        List<HotelQuoteResp> HotelQuoteList = new List<HotelQuoteResp>();
                        if (r != null)
                        {
                            //RoomClass r3 = r.OrderBy(x => x.TotalPrice).FirstOrDefault();//拿到价格最低的
                            for (int i = 0; i < r.Count; i++)
                            {
                                string Rooms = Newtonsoft.Json.JsonConvert.SerializeObject(r[i].Rooms).Replace("\"", "\'");
                                HotelQuoteResp obj = new HotelQuoteResp();
                                obj.ClassName = r[i].ClassName;
                                obj.Rooms = Rooms;
                                obj.Status = r[i].Status;
                                obj.TotalPrice = r[i].TotalPrice;
                                obj.CurrencyCode = r[i].CurrencyCode;
                                obj.TotalRoom = r[i].TotalRoom;
                                obj.SupplierCode = r[i].SupplierCode;
                                obj.IsOptional = r[i].IsOptional;
                                obj.IsPromotion = r[i].IsPromotion;
                                HotelQuoteList.Add(obj);
                            }
                            dt = CConvert.IListToDataTable<HotelQuoteResp>(HotelQuoteList);
                            //dt = CConvert.DtFromObject<TravPaxRoomClass.RoomClass>(r1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return dt;
        }

        /// <summary>
        /// 获取取消期限
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="referenceClient"></param>
        /// <param name="nationalityID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string GetCancellationPolicy(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID,string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try 
            {
                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};

select hp.Price,hp.CurrencyCode,hp.ReferenceClient,hpd.* from HotelPrice hp WITH(NOLOCK) LEFT JOIN dbo.HotelPriceDetail hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID='{0}' AND hp.Platform='TravPax' AND hp.Adults={1} AND hp.Childs={2} AND hp.CheckInDate='{3}' AND hp.CheckOutDate='{4}' AND hp.IsClose=1 AND hpd.IsClose=1 and hp.ID={5}  order by hp.id desc",
HotelID, Adults, Childs, beginTime, endTime, HotelPriceID);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHotelPriceDetail = ds.Tables[1];
                string childXml=string.Empty;
                TravPaxCancellationPolicyReq.CancellationPolicyReq obj = new TravPaxCancellationPolicyReq.CancellationPolicyReq();
                obj.Username = travPaxUsername;
                obj.Password = travPaxPassword;
                obj.Supplier = "TRAVPAX";
                obj.CheckInDate = beginTime;
                obj.CheckOutDate = endTime;
                obj.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                obj.HotelID = HotelID;
                obj.NationalityID =  NationalityID.AsTargetType<int>(0);
                obj.ReferenceClient = dtHotelPriceDetail.Rows[0]["ReferenceClient"].ToString();

                List<TravPaxCancellationPolicyReq.Room> roomList = new List<TravPaxCancellationPolicyReq.Room>();
                for (int i = 0; i < dtHotelPriceDetail.Rows.Count; i++)
                {
                    string[] ArrayAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                    string strAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("");

                    string Age = string.Empty;
                    if (strAge != "")
                    {
                        List<string> list = new List<string>(strAge.Split(','));
                        if (ArrayAge.Length != 0)
                        {
                            for (int j = 0; j < ArrayAge.Length; j++)
                            {
                                Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                            }
                        }
                    }
                    TravPaxCancellationPolicyReq.Room roomObj = new TravPaxCancellationPolicyReq.Room();
                    roomObj.ProductID = dtHotelPriceDetail.Rows[i]["RoomID"].AsTargetType<int>(0);
                    roomObj.Adults = dtHotelPriceDetail.Rows[i]["Adults"].AsTargetType<int>(0);
                    roomObj.Childs = dtHotelPriceDetail.Rows[i]["Childs"].AsTargetType<int>(0);
                    roomObj.MapRoomID = 0;
                    roomObj.AgreementID = dtHotelPriceDetail.Rows[i]["AgreementID"].AsTargetType<int>(0);
                    roomObj.SupplierCode = "TRAVPAX";
                    roomObj.ChildsAge = Age;

                    TravPaxCancellationPolicyReq.SupplierRefIItem srObj = new TravPaxCancellationPolicyReq.SupplierRefIItem();
                    srObj.ID = dtHotelPriceDetail.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                    srObj.Value = dtHotelPriceDetail.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                    TravPaxCancellationPolicyReq.SupplierRefItem sriObj = new TravPaxCancellationPolicyReq.SupplierRefItem();
                    sriObj.SupplierRef = srObj;

                    roomObj.SupplierRef = sriObj;

                    roomList.Add(roomObj);
                }

                TravPaxCancellationPolicyReq.RoomClass RoomClassObj = new TravPaxCancellationPolicyReq.RoomClass();
                RoomClassObj.Rooms = roomList;
                RoomClassObj.CurrencyCode = dtHotelPriceDetail.Rows[0]["CurrencyCode"].ToString();
                RoomClassObj.TotalPrice = dtHotelPriceDetail.Rows[0]["TotalPrice"].ToString();
                obj.RoomClass = RoomClassObj;
                string strXml = Common.Common.XmlSerializeNoTitle<TravPaxCancellationPolicyReq.CancellationPolicyReq>(obj).Replace("&lt;", "<").Replace("&gt;", ">");
                
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;
                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio: TravPax获取取消政策接口 ,请求数据 :{0} , 返回数据 ：{1} ，SpendTime : {2} , Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取取消政策接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                CancellationPolicyResp objResp = Common.Common.DESerializer<CancellationPolicyResp>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = objResp.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax获取取消政策接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    resp = new Respone() { code = "00", mes = objResp.CancelType + ";" + objResp.CancelDate + ";" + objResp.CancelPrice + ";" + objResp.ErrorMessage };
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员"  };
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax获取取消政策接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取取消政策接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 价格检查
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string GetHotelRecheckPrice(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID, string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};

select hp.Price,hp.CurrencyCode,hp.ReferenceClient,hpd.* from HotelPrice hp WITH(NOLOCK) LEFT JOIN dbo.HotelPriceDetail hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID='{0}' AND hp.Platform='TravPax' AND hp.Adults={1} AND hp.Childs={2} AND hp.CheckInDate='{3}' AND hp.CheckOutDate='{4}' AND hp.IsClose=1 AND hpd.IsClose=1 and hp.ID={5}  order by hp.id desc",
HotelID, Adults,Childs, beginTime, endTime,HotelPriceID);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHotelPriceDetail = ds.Tables[1];
                string childXml = string.Empty;
                TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq obj = new TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq();
                obj.Username = travPaxUsername;
                obj.Password = travPaxPassword;
                obj.CheckInDate = beginTime;
                obj.CheckOutDate = endTime;
                obj.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                obj.HotelID = HotelID;
                obj.NationalityID = NationalityID.AsTargetType<int>(0);
                obj.ReferenceClient = dtHotelPriceDetail.Rows[0]["ReferenceClient"].ToString();

                List<TravPaxHotelRecheckPriceReq.Room> roomList = new List<TravPaxHotelRecheckPriceReq.Room>();
                for (int i = 0; i < dtHotelPriceDetail.Rows.Count; i++)
                {
                    string[] ArrayAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                    string strAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("");

                    string Age = string.Empty;
                    if (strAge != "")
                    {
                        List<string> list = new List<string>(strAge.Split(','));
                        if (ArrayAge.Length != 0)
                        {
                            for (int j = 0; j < ArrayAge.Length; j++)
                            {
                                Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                            }
                        }
                    }
                    TravPaxHotelRecheckPriceReq.Room roomObj = new TravPaxHotelRecheckPriceReq.Room();
                    roomObj.ProductID = dtHotelPriceDetail.Rows[i]["RoomID"].AsTargetType<int>(0);
                    roomObj.Adults = dtHotelPriceDetail.Rows[i]["Adults"].AsTargetType<int>(0);
                    roomObj.Childs = dtHotelPriceDetail.Rows[i]["Childs"].AsTargetType<int>(0);
                    roomObj.MapRoomID = 0;
                    roomObj.AgreementID = dtHotelPriceDetail.Rows[i]["AgreementID"].AsTargetType<int>(0);
                    roomObj.SupplierCode = "SupplierCode";
                    roomObj.ChildsAge = Age;

                    TravPaxHotelRecheckPriceReq.SupplierRefIItem srObj = new TravPaxHotelRecheckPriceReq.SupplierRefIItem();
                    srObj.ID = dtHotelPriceDetail.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                    srObj.Value = dtHotelPriceDetail.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                    TravPaxHotelRecheckPriceReq.SupplierRefItem sriObj = new TravPaxHotelRecheckPriceReq.SupplierRefItem();
                    sriObj.SupplierRef = srObj;

                    roomObj.SupplierRef = sriObj;

                    roomList.Add(roomObj);
                }

                TravPaxHotelRecheckPriceReq.RoomClass RoomClassObj = new TravPaxHotelRecheckPriceReq.RoomClass();
                RoomClassObj.Rooms = roomList;
                RoomClassObj.TotalPrice = dtHotelPriceDetail.Rows[0]["Price"].ToString();
                obj.RoomClass = RoomClassObj;

                string strXml = Common.Common.XmlSerializeNoTitle<TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq>(obj).Replace("&lt;", "<").Replace("&gt;", ">");

                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;

                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口, 请求数据 : {0} , 返回数据 ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax价格检查接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                HotelRecheckPriceResp objResp = Common.Common.DESerializer<HotelRecheckPriceResp>(result.Replace("&", ""));

                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = objResp.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        if (objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room != null)
                        {
                            string updateHotelPriceSql = string.Format(@"update HotelPriceDetail set Status='{0}',TotalPrice='{1}',UpdateTime=GETDATE() 
from HotelPrice hp WITH(NOLOCK)
where  hp.HotelID='{2}' AND hp.Platform='TravPax' AND hp.Adults={3} AND hp.Childs={4} AND hp.CheckInDate='{5}' AND hp.CheckOutDate='{6}' AND hp.IsClose=1  and hp.ID={7};

update HotelRecheckPrice set IsClose=0,UpdateTime=GETDATE() where HotelPriceID='{7}'
",
objResp.Hotels.RoomClasses.RoomClass[0].Status, objResp.Hotels.RoomClasses.RoomClass[0].TotalPrice, HotelID, Adults, Childs, beginTime, endTime, HotelPriceID);
                            con.Update(updateHotelPriceSql);
                            HotelPriceCollection hrdc = new HotelPriceCollection();
                            for (int i = 0; i < objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room.Count; i++)
                            {
                                HotelRecheckPrice hrd = new HotelRecheckPrice();
                                hrd.HotelPriceID = HotelPriceID.AsTargetType<int>(0);
                                hrd.HotelID = HotelID.AsTargetType<string>("");
                                hrd.RoomID = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomID.ToString();
                                hrd.RoomName = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomName;
                                hrd.Status = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].Status;
                                hrd.BBCode = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].BBCode;
                                hrd.TotalPrice = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].TotalPrice;
                                hrd.RoomCurrencyCode = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].CurrencyCode;
                                hrd.AgreementID = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].AgreementID;
                                hrd.Adults = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].Adults;
                                hrd.Childs = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].Childs;
                                string strAge = string.Empty;
                                if (objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].ChildsAge != null)
                                {
                                    for (int k = 0; k < objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].ChildsAge.Age.Count; k++)
                                    {
                                        strAge += objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].ChildsAge.Age[k] + ",";
                                    }
                                }
                                hrd.Age = strAge.TrimEnd(',');
                                hrd.SupplierRefID = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].SupplierRef.SupplierRef.ID;
                                hrd.SupplierRefValue = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].SupplierRef.SupplierRef.Value;
                                hrd.DateOfRate = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomPriceBreak.DailyRate.DateOfRate;
                                hrd.DailyCurrencyCode = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomPriceBreak.DailyRate.CurrencyCode;
                                hrd.DailyRate = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomPriceBreak.DailyRate.DailyRate;
                                hrd.IsClose = 1;
                                hrd.CreateTime = DateTime.Now;
                                hrd.UpdateTime = DateTime.Now;
                                hrdc.Add(hrd);
                            }
                            con.Save(hrdc);
                        }
                    }
                    resp = new Respone() { code = "00", mes =objResp.Hotels.RoomClasses.RoomClass[0].Status + ";" + objResp.Hotels.RoomClasses.RoomClass[0].TotalPrice.AsTargetType<string>("0") }; 
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax价格检查接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string CreatePNR(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID, string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
            string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};
select hp.Price,hp.CurrencyCode,hp.ReferenceClient,hpd.* from HotelPrice hp WITH(NOLOCK) LEFT JOIN dbo.HotelRecheckPrice hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID='{0}' AND hp.Platform='TravPax' AND hp.Adults={1} AND hp.Childs={2} AND hp.CheckInDate='{3}' AND hp.CheckOutDate='{4}' AND hpd.IsClose=1 AND hp.IsClose=1   and hp.ID={5}  order by hp.id desc",
HotelID, Adults,Childs, beginTime, endTime,HotelPriceID);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHotelPriceDetail = ds.Tables[1];

                TravPaxCreatePNRReq.CreatePNRReq obj = new TravPaxCreatePNRReq.CreatePNRReq();
                obj.Username=travPaxUsername;
                obj.Password =travPaxPassword;
                obj.Supplier="TRAVPAX";
                obj.CheckInDate=beginTime;
                obj.CheckOutDate =endTime;
                obj.CountryID =dt.Rows[0]["COUNTRY_CODE"].ToString();
                obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                obj.HotelID =HotelID;
                obj.NationalityID = NationalityID.AsTargetType<int>(0);
                obj.ReferenceClient=dtHotelPriceDetail.Rows[0]["ReferenceClient"].ToString();

                List<TravPaxCreatePNRReq.Room> roomList = new List<TravPaxCreatePNRReq.Room>();
                for (int i = 0; i < dtHotelPriceDetail.Rows.Count; i++)
                {
                    string[] ArrayAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                    string strAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("");

                    string Age = string.Empty;
                    if (strAge != "")
                    {
                        List<string> list = new List<string>(strAge.Split(','));
                        if (ArrayAge.Length != 0)
                        {
                            for (int j = 0; j < ArrayAge.Length; j++)
                            {
                                Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                            }
                        }
                    }
                    TravPaxCreatePNRReq.Room roomObj = new TravPaxCreatePNRReq.Room();
                    roomObj.ProductID = dtHotelPriceDetail.Rows[i]["RoomID"].AsTargetType<int>(0);
                    roomObj.Adults = dtHotelPriceDetail.Rows[i]["Adults"].AsTargetType<int>(0);
                    roomObj.Childs = dtHotelPriceDetail.Rows[i]["Childs"].AsTargetType<int>(0);
                    roomObj.ChildsAge = Age;

                    TravPaxCreatePNRReq.SupplierRefIItem srObj = new TravPaxCreatePNRReq.SupplierRefIItem();
                    srObj.ID = dtHotelPriceDetail.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                    srObj.Value = dtHotelPriceDetail.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                    TravPaxCreatePNRReq.SupplierRefItem sriObj = new TravPaxCreatePNRReq.SupplierRefItem();
                    sriObj.SupplierRef = srObj;
                    roomObj.SupplierRef = sriObj;

                    List<TravPaxCreatePNRReq.PaxInfo> tppObjList = new List<TravPaxCreatePNRReq.PaxInfo>();
                    TravPaxCreatePNRReq.PaxInfo tppObj = new TravPaxCreatePNRReq.PaxInfo();

                    if (i == 0)
                    {
                        TravPaxCreatePNRReq.PaxInfo tppObj1 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Zhang", LastName = "San", Title = "Mr." };
                        TravPaxCreatePNRReq.PaxInfo tppObj2 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Li", LastName = "Si", Title = "Mr." };
                        //TravPaxCreatePNRReq.PaxInfo tppObj3 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Wang", LastName = "Wu", Title = "Mr." };
                        tppObjList.Add(tppObj1);
                        tppObjList.Add(tppObj2);
                        //tppObjList.Add(tppObj3);
                        /*
                        tppObj.FirstName = "Zhang";
                        tppObj.LastName = "San";
                        tppObj.Title = "Mr";
                        tppObjList.Add(tppObj);
                        //roomObj.PaxInfo.Add(tppObj);
                        tppObj.FirstName = "Li";
                        tppObj.LastName = "Si";
                        tppObj.Title = "Mr";
                        tppObjList.Add(tppObj);
                        //roomObj.PaxInfo.Add(tppObj);
                        tppObj.FirstName = "Wang";
                        tppObj.LastName = "Wu";
                        tppObj.Title = "Mr";
                        tppObjList.Add(tppObj);
                        //roomObj.PaxInfo.Add(tppObj);*/
                    }
                    else if (i == 1)
                    {
                        TravPaxCreatePNRReq.PaxInfo tppObj1 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Li", LastName = "Si", Title = "Mr." };
                        TravPaxCreatePNRReq.PaxInfo tppObj2 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Wang", LastName = "Wu", Title = "Mr." };
                        TravPaxCreatePNRReq.PaxInfo tppObj3 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Huang", LastName = "Liu", Title = "Mr." };
                        tppObjList.Add(tppObj1);
                        tppObjList.Add(tppObj2);
                        tppObjList.Add(tppObj3);
                    }
                    else if (i == 2)
                    {
                        TravPaxCreatePNRReq.PaxInfo tppObj1 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Chen", LastName = "Qi", Title = "Mr." };
                        TravPaxCreatePNRReq.PaxInfo tppObj2 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Liang", LastName = "Ba", Title = "Mr." };
                        tppObjList.Add(tppObj1);
                        tppObjList.Add(tppObj2);
                    }

                    roomObj.PaxInfos = tppObjList;

                    roomList.Add(roomObj);   
                }
                TravPaxCreatePNRReq.RoomClass RoomClassObj = new TravPaxCreatePNRReq.RoomClass();
                RoomClassObj.Rooms = roomList;
                RoomClassObj.CurrencyCode = dtHotelPriceDetail.Rows[0]["CurrencyCode"].ToString();
                RoomClassObj.TotalPrice = dtHotelPriceDetail.Rows[0]["Price"].ToString();
                obj.RoomClass = RoomClassObj;

                TravPaxCreatePNRReq.Leading leadObj = new TravPaxCreatePNRReq.Leading();
                leadObj.Nationality = 66;
                leadObj.Title = "Mr";
                leadObj.FirstName = "Zhang";
                leadObj.LastName = "San";

                obj.Leading = leadObj;

                string strXml = Common.Common.XmlSerializeNoTitle<TravPaxCreatePNRReq.CreatePNRReq>(obj).Replace("<PaxInfos>", "").Replace("</PaxInfos>", "").Replace("&lt;", "<").Replace("&gt;", ">");

                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax创建订单接口, 请求数据 : {0} , 返回数据 ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCreatePNRResp.PNRResp objResp = Common.Common.DESerializer<TravPaxCreatePNRResp.PNRResp>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = objResp.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    BookingCollection bookingCollection=new BookingCollection();
                    for (int i = 0; i < objResp.Services.Service.Count; i++)
                    {
                        decimal ClientPartnerAmount = objResp.Services.Service[i].TotalFare;
                        string ClientCurrencyCode = objResp.Services.Service[i].FareCurrency;
                        string toRMBrate = string.Empty;
                        if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                        {
                            toRMBrate = USDtoRMBrate;
                        }
                        else if (ClientCurrencyCode == "EUR")
                        {
                            toRMBrate = EURtoRMBrate;
                        }
                        else if (ClientCurrencyCode == "SGD")
                        {
                            toRMBrate = SGDtoRMBrate;
                        }
                        else if (ClientCurrencyCode == "HKD")
                        {
                            toRMBrate = HKDtoRMBrate;
                        }
                        decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                        Booking booking = new Booking();
                        //保存订单到数据库
                        booking.BookingCode = objResp.PNRNo;
                        booking.AgentBookingReference = objResp.Refer;
                        booking.RMBprice = price;
                        booking.ClientCurrencyCode = objResp.Services.Service[i].FareCurrency;
                        booking.ClientPartnerAmount = objResp.Services.Service[i].TotalFare.AsTargetType<decimal>(0);
                        booking.HotelId = HotelID.ToString();
                        booking.HotelNo = "";
                        booking.Platform = "TravPax";
                        booking.StatusCode = objResp.PNRStatus;
                        booking.StatusName = "";
                        booking.ServiceID = objResp.Services.Service[i].ID;
                        booking.ServiceName = objResp.Services.Service[i].Name;
                        booking.LegID = objResp.Services.Service[i].LegID;
                        booking.Leg_Status = objResp.Services.Service[i].Leg_Status;
                        booking.CreatTime = DateTime.Now;
                        booking.UpdateTime = DateTime.Now;
                        booking.EntityState = e_EntityState.Added;
                        bookingCollection.Add(booking);
                    }
                    con.Save(bookingCollection);
                    resp = new Respone() { code = "00", mes = objResp.PNRStatus };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", "pNRResp != null", DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "创建失败，请联系管理员" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string GetBookingDetail(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string AgentReferenceNumber, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string strXml = string.Format(@"<RetrievePNRReq> <Username>{0}</Username> <Password>{1}</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>{2}</PNRNo> </RetrievePNRReq>"
                , travPaxUsername, travPaxPassword, BookingCode);
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax查询订单接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCreatePNRResp.PNRResp objResp = Common.Common.DESerializer<TravPaxCreatePNRResp.PNRResp>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = objResp.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    for (int i = 0; i < objResp.Services.Service.Count; i++)
                    {
                        string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Leg_Status='{1}' where BookingCode='{2}' and ServiceID={3}  and Platform='TravPax' "
                            , objResp.PNRStatus, objResp.Services.Service[i].Leg_Status, BookingCode, objResp.Services.Service[i].ID);
                        con.Update(strUpdateSql);
                    }
                    resp = new Respone() { code = "00", mes = objResp.Services.Service[0].Leg_Status };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单单接口 ,err:{0} , Time:{1} ", "pNRResp != null", DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "查询失败，请联系管理员" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string BookingCancel(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string AgentReferenceNumber, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string strXml=string.Format(@"<CancelPNRReq> <Username>{0}</Username> <Password>{1}</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>{2}</PNRNo> <ConfirmCode>{3}</ConfirmCode> </CancelPNRReq>"
                , travPaxUsername, travPaxPassword, BookingCode, AgentReferenceNumber);
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax取消订单接口, 请求数据 : {0} , 返回数据 ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCancelPNRResp.CancelPNRResp objResp = Common.Common.DESerializer<TravPaxCancelPNRResp.CancelPNRResp>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = objResp.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    //修改数据库订单状态
                    for (int i = 0; i < objResp.Services.Service.Count; i++)
                    {
                        string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Leg_Status='{1}' where BookingCode='{2}' and ServiceID={3}  and Platform='TravPax'"
                            , objResp.PNRStatus, objResp.Services.Service[i].Leg_Status, BookingCode, objResp.Services.Service[i].ID);
                        con.Update(strUpdateSql);
                    }
                    resp = new Respone() { code = "00", mes = objResp.Services.Service[0].Leg_Status };
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 获取报价（供外部调用）大人数不能大于3，小孩数不能大于2
        /// </summary>ReferenceClient为房型id
        /// <param name="hotelID"></param>
        /// <param name="countryID"></param>
        /// <param name="cityID"></param>
        /// <param name="referenceClient"></param>
        /// <param name="nationalityID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="ddlRoom"></param>
        /// <param name="rooms"></param>
        /// <param name="input_page"></param>
        /// <param name="input_row"></param>
        /// <returns></returns>
        public string GetTravPaxRoomTypeOutPrice(string url, string travPaxUsername, string travPaxPassword, string hotelID, string beginTime, string endTime,IntOccupancyInfo occupancy, int RoomCount,string RateplanId, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            DataTable dt = new DataTable();
            try
            {
                //复查接口时调用，匹配数据是否有该房型
                if (RateplanId != null && RateplanId.Trim() != "")
                {
                    return GetHotelRecheckPriceToOut(url, travPaxUsername, travPaxPassword, hotelID, RateplanId, beginTime, endTime, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                }
                string result = string.Empty;
                int adults = occupancy.adults;
                int children = occupancy.children;
                string childAges = occupancy.childAges ?? "";
                string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID={0} AND Platform='TravPax' AND Adults={1} AND Childs={2}
and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6} AND IsClose=1 AND DATEDIFF( MINUTE, UpdateTime, GETDATE()) < {7} order by ID;
select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0} "
                , hotelID, adults, children, childAges, beginTime, endTime,RoomCount, TravPaxCacheTime);
                DataSet ds = con.GetDataSet(sql);
                dt = ds.Tables[0];
                DataTable dtHotel = ds.Tables[1];

                if ((dt.Rows.Count == 0 && dtHotel.Rows.Count > 0) || (RateplanId != "" && dtHotel.Rows.Count > 0))
                {
                    HotelQuoteReq.HotelSearchReq objReq = new HotelQuoteReq.HotelSearchReq();
                    objReq.Username = travPaxUsername;
                    objReq.Password = travPaxPassword;
                    objReq.ReferenceClient = "";
                    objReq.CheckInDate = beginTime;
                    objReq.CheckOutDate = endTime;
                    objReq.CountryID = dtHotel.Rows[0]["COUNTRY_CODE"].ToString();
                    objReq.CityID = dtHotel.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                    objReq.HotelID = hotelID.AsTargetType<int>(0);
                    objReq.NationalityID = 66;

                    List<HotelQuoteReq.RoomRequest> roomListReq = new List<HotelQuoteReq.RoomRequest>();
                    string Age = string.Empty;
                    string[] ArrayChilds = occupancy.childAges.Split(',');
                    if (children == 1)
                    {
                        Age += string.Format(@"<Age>{0}</Age>", ArrayChilds[0]);
                    }
                    else if (children == 2)
                    {
                        Age += string.Format(@"<Age>{0}</Age><Age>{1}</Age>", ArrayChilds[0], ArrayChilds[1]);
                    }

                    HotelQuoteReq.RoomRequest roomObj = new HotelQuoteReq.RoomRequest();
                    roomObj.Adults = adults;
                    roomObj.Childs = children;
                    roomObj.ChildsAge = Age;
                    //循环相同房型房间数
                    for (int t = 0; t < RoomCount; t++)
                    {
                        roomListReq.Add(roomObj);
                    }

                    objReq.Rooms = roomListReq;
                    string strXml = Common.Common.XmlSerializeNoTitle<HotelQuoteReq.HotelSearchReq>(objReq).Replace("&lt;", "<").Replace("&gt;", ">");

                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //请求接口
                    try
                    {
                        DateTime beforDT = System.DateTime.Now;

                        //发起请求
                        result = Common.Common.PostHttp(url, postBoby);
                        DateTime afterDT = System.DateTime.Now;
                        TimeSpan ts = afterDT.Subtract(beforDT);
                        string SpendTime = ts.Seconds.ToString();
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio：TravPax获取报价接口 ,请求数据 :{0} , 返回数据 ：{1} ，SpendTime : {2} Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                    }
                    catch (Exception ex)
                    {
                        //日记
                        LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    HotelSearchResp objResp = Common.Common.DESerializer<HotelSearchResp>(result);

                    if (objResp != null)
                    {
                        if (objResp.ErrorMessage != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        List<HotelSearchResp.RoomClass> r = Common.Common.DESerializer<HotelSearchResp>(result.Replace("&", "")).Hotels.Hotel.RoomClasses.RoomClass.Where(x => x.Status.Contains("AL")).ToList();
                        HotelPriceCollection objCollection = new HotelPriceCollection();
                        if (r != null)
                        {
                            string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' AND Platform='TravPax' AND Adults={1} AND Childs={2} and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6}",
    hotelID, adults, children, occupancy.childAges, beginTime, endTime, RoomCount);
                            con.Update(updateHotelPriceSql);

                            for (int i = 0; i < r.Count; i++)
                            {
                                HotelPrice obj = new HotelPrice();
                                string ClientCurrencyCode = r[i].CurrencyCode;
                                decimal ClientPrice = r[i].TotalPrice;
                                string toRMBrate = string.Empty;
                                if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                                {
                                    toRMBrate = USDtoRMBrate;
                                }
                                else if (ClientCurrencyCode == "EUR")
                                {
                                    toRMBrate = EURtoRMBrate;
                                }
                                else if (ClientCurrencyCode == "SGD")
                                {
                                    toRMBrate = SGDtoRMBrate;
                                }
                                else if (ClientCurrencyCode == "HKD")
                                {
                                    toRMBrate = HKDtoRMBrate;
                                }
                                //售价
                                decimal RMBprice = ClientPrice * toRMBrate.AsTargetType<decimal>(0);
                                string cGuid = Common.Common.GuidToLongID();//房型id
                                string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                obj.HotelID = hotelID;
                                obj.HotelNo = r[i].Rooms.Room[0].RoomID.AsTargetType<string>("");
                                obj.Platform = "TravPax";
                                obj.RMBprice = RMBprice;
                                obj.ClassName = r[i].ClassName;
                                obj.Price = ClientPrice;
                                obj.CurrencyCode = ClientCurrencyCode;
                                if (r[i].Rooms.Room[0].BBCode == "BB")
                                {
                                    obj.IsBreakfast = 1;
                                }
                                else
                                {
                                    obj.IsBreakfast = 0;
                                }
                                obj.ReferenceClient = objResp.ReferenceClient;
                                obj.RoomCount = RoomCount;
                                obj.Adults = adults;
                                obj.Childs = children;
                                obj.ChildsAge = childAges;
                                obj.RoomCount = RoomCount;
                                obj.CheckInDate = beginTime.AsTargetType<DateTime>(DateTime.MinValue);
                                obj.CheckOutDate = endTime.AsTargetType<DateTime>(DateTime.MinValue);
                                obj.CGuID = cGuid;
                                string PriceBreakdownDate = string.Empty;
                                string PriceBreakdownPrice = string.Empty;
                                for (int k = 0; k < r[i].Rooms.Room[0].RoomPriceBreak.DailyRate.Count; k++)
                                {
                                    PriceBreakdownDate += r[i].Rooms.Room[0].RoomPriceBreak.DailyRate[k].DateOfRate.ToString("yyyy-MM-dd") + ",";
                                    PriceBreakdownPrice += r[i].Rooms.Room[0].RoomPriceBreak.DailyRate[k].DailyRate + ",";
                                }
                                obj.PriceBreakdownDate = PriceBreakdownDate.TrimEnd(',');
                                obj.PriceBreakdownPrice = PriceBreakdownPrice.TrimEnd(',');
                                obj.RateplanId = GuidRateplanId;
                                obj.IsClose = 1;
                                obj.CreateTime = DateTime.Now;
                                obj.UpdateTime = DateTime.Now;
                                con.Save(obj);
                                HotelPriceDetailCollection hpdl = new HotelPriceDetailCollection();
                                List<HotelSearchResp.Room> roomList = r[i].Rooms.Room;
                                if (roomList != null)
                                {
                                    for (int j = 0; j < roomList.Count; j++)
                                    {
                                        HotelPriceDetail hpd = new HotelPriceDetail();
                                        hpd.HotelPriceID = obj.ID;
                                        hpd.HotelID = hotelID;
                                        hpd.RoomID = roomList[j].RoomID.ToString();
                                        hpd.RoomName = roomList[j].RoomName;
                                        hpd.Status = roomList[j].Status;
                                        hpd.BBCode = roomList[j].BBCode;
                                        hpd.TotalPrice = roomList[j].TotalPrice;
                                        hpd.RoomCurrencyCode = roomList[j].CurrencyCode;
                                        hpd.AgreementID = roomList[j].AgreementID;
                                        hpd.Adults = roomList[j].Adults;
                                        hpd.Childs = roomList[j].Childs;
                                        string strAge = string.Empty;
                                        for (int k = 0; k < roomList[j].ChildsAge.Age.Count; k++)
                                        {
                                            strAge += roomList[j].ChildsAge.Age[k] + ",";
                                        }
                                        hpd.Age = strAge.TrimEnd(',');
                                        hpd.SupplierRefID = roomList[j].SupplierRef.SupplierRef.ID;
                                        hpd.SupplierRefValue = roomList[j].SupplierRef.SupplierRef.Value;
                                        hpd.DateOfRate = PriceBreakdownDate.TrimEnd(',');
                                        hpd.DailyCurrencyCode = roomList[j].RoomPriceBreak.DailyRate[0].CurrencyCode;
                                        hpd.DailyRate = PriceBreakdownPrice.TrimEnd(',');
                                        hpd.IsClose = 1;
                                        hpd.CreateTime = DateTime.Now;
                                        hpd.UpdateTime = DateTime.Now;
                                        hpdl.Add(hpd);
                                    }
                                    con.Save(hpdl);
                                }

                            }
                        }
                        else
                        {
                            LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "r != null", strXml, DateTime.Now.ToString()), "TravPax接口");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    string sqlDt = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID={0} AND Platform='TravPax'  AND Adults={1} AND Childs={2}   and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}' and RoomCount={6} AND IsClose=1 order by ID"
                    , hotelID, adults, children, childAges, beginTime, endTime, RoomCount);
                    dt = ControllerFactory.GetController().GetDataTable(sqlDt);
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        RoomOutPrice.Room temp = new RoomOutPrice.Room();
                        temp.ProductSource = "TravPax"; //产品来源 
                        temp.MroomName = temp.XRoomName = temp.RoomName = dt.Rows[j]["ClassName"].AsTargetType<string>("");// 房型名称
                        temp.MroomId = temp.RoomTypeId = dt.Rows[j]["CGuID"].AsTargetType<string>("");//房型ID

                        //当前请求的房间人数
                        temp.Adults = adults;
                        temp.Children = children;
                        temp.ChildAges = childAges.Replace(",", "|");
                        //最大入住人数
                        temp.MaxPerson = adults;

                        temp.RatePlan = new List<RoomOutPrice.RatePlan>();
                        RoomOutPrice.RatePlan thisRatePlan = new RoomOutPrice.RatePlan();
                        string RatePlanName = "有早";
                        thisRatePlan.PayMent = "y";//支付方式。目前全部前台支付
                        int dtIsBreakfast = dt.Rows[j]["IsBreakfast"].AsTargetType<int>(0);
                        if (dtIsBreakfast == 0)
                        {
                            RatePlanName = "无早";
                        }
                        thisRatePlan.RatePlanName = RatePlanName;  //价格计划名称
                        thisRatePlan.RoomName = RatePlanName;
                        thisRatePlan.XRatePlanName = RatePlanName;
                        thisRatePlan.RatePlanId = dt.Rows[j]["RateplanId"].AsTargetType<string>("");  //价格计划ID

                        thisRatePlan.RoomTypeId = dt.Rows[j]["CGuID"].AsTargetType<string>("");
                        temp.XRoomId = temp.RoomId = thisRatePlan.RoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>("")); //房型Id_价格计划ID
                        //temp.XRoomId = temp.RoomId = string.Format("{0}_{1}", dt.Rows[j]["ReferenceClient"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>(""));
                        //床型
                        string sqlRoomName = string.Format(@"select * from HotelPriceDetail WITH(NOLOCK) where HotelPriceID={0} ", dt.Rows[j]["id"]);
                        DataTable dtRoomName = ControllerFactory.GetController().GetDataTable(sqlRoomName);
                        common.HotelBedType hbt = new common.HotelBedType();
                        thisRatePlan.BedType = hbt.HotelBedTypeToName(dtRoomName.Rows[0]["RoomName"].AsTargetType<string>(""));

                        //售价
                        decimal price = dt.Rows[j]["RMBprice"].AsTargetType<decimal>(0);
                        string[] ArrayPriceBreakdownDate = dt.Rows[j]["PriceBreakdownDate"].AsTargetType<string>("").Split(',');
                        //总的底价 
                        double sMoney = price.AsTargetType<double>(0);
                        bool available = true; // 指示入离日期所有天是否可订 有一天不可订为false
                        List<RoomOutPrice.Rate> rates = new List<RoomOutPrice.Rate>();
                        int days = (Convert.ToDateTime(endTime) - Convert.ToDateTime(beginTime)).Days;
                        for (int x = 0; x < ArrayPriceBreakdownDate.Length; x++)
                        {
                            RoomOutPrice.Rate rate = new RoomOutPrice.Rate();
                            rate.Date = Convert.ToDateTime(ArrayPriceBreakdownDate[x]);
                            rate.Available = true;  //某天是否可订


                            rate.HotelID = (price / days).AsTargetType<string>("");  //底价

                            rate.MemberRate = (price / days);
                            rate.RetailRate = (price / days);
                            rates.Add(rate);
                        }


                        //补上没有的日期
                        if (days > rates.Count)
                        {
                            available = false;
                            List<RoomOutPrice.Rate> tempR = new List<RoomOutPrice.Rate>();
                            for (int y = 0; y < days; y++)
                            {
                                DateTime date = Convert.ToDateTime(beginTime).AddDays(y).Date;
                                var one = rates.Find(c => c.Date.Date == date);
                                if (one != null)
                                {
                                    tempR.Add(one);
                                }
                                else
                                {
                                    RoomOutPrice.Rate newOne = new RoomOutPrice.Rate() { Available = false, InvStatusCode = "0", MemberRate = 0, Date = date };
                                    tempR.Add(newOne);
                                }
                            }
                            rates = tempR;
                        }

                        thisRatePlan.Rates = rates;
                        thisRatePlan.Available = available;
                        thisRatePlan.SMoney = Convert.ToDecimal(sMoney);

                        decimal avgprice = 0;//均价
                        if (rates.Count > 0)
                        {
                            avgprice = rates.Average(c => c.MemberRate);
                            thisRatePlan.AveragePrice = avgprice.ToString("f2");
                        }

                        temp.CurrentAlloment = thisRatePlan.CurrentAlloment = 15; //库存

                        //设置早餐数量
                        int breakfast = -1;
                        thisRatePlan.Breakfast = breakfast + "份早餐";
                        if (dtIsBreakfast == 0)
                        {
                            thisRatePlan.Breakfast = 0 + "份早餐";
                        }
                        else
                        {
                            thisRatePlan.Breakfast = adults + "份早餐";
                        }
                        string zaocan = thisRatePlan.Breakfast.Replace("份早餐", "");
                        if (!zaocan.Equals("0"))
                        {
                            switch (zaocan)
                            {
                                case "1": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含单早)"; temp.RoomName = temp.RoomName + "(含单早)"; break;
                                case "2": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含双早)"; temp.RoomName = temp.RoomName + "(含双早)"; break;
                                case "3": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含三早)"; temp.RoomName = temp.RoomName + "(含三早)"; break;
                                case "4": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含四早)"; temp.RoomName = temp.RoomName + "(含四早)"; break;
                            }
                        }
                        thisRatePlan.RoomName = thisRatePlan.RoomName + "[内宾]";

                        temp.RoomPrice = Convert.ToInt32(avgprice);
                        temp.BedType = thisRatePlan.BedType;
                        temp.RatePlan.Add(thisRatePlan);
                        rooms.Add(temp);
                    }
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }

        /// <summary>
        /// 价格检查
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="ratePlanId"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string GetHotelRecheckPriceToOut(string url, string travPaxUsername, string travPaxPassword, string HotelID, string ratePlanId, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            DataTable dtRoom = new DataTable();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};

select hp.id as HotelPriceID,hp.Price,hp.CurrencyCode,hp.ReferenceClient,hp.CheckInDate as beginDate,hp.CheckOutDate as endDate,hpd.* from HotelPrice hp WITH(NOLOCK)
LEFT JOIN dbo.HotelPriceDetail hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID='{0}' AND hp.Platform='TravPax'  AND hpd.IsClose=1 and hp.RateplanId='{1}'  order by hp.id desc",
HotelID, ratePlanId);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHpd = ds.Tables[1];
                if (dt.Rows.Count > 0 && dtHpd.Rows.Count > 0)
                {
                    //汇率
                    string CurrencyCode = dtHpd.Rows[0]["CurrencyCode"].AsTargetType<string>("");
                    string toRMBrate = string.Empty;
                    if (CurrencyCode == "USD" || CurrencyCode == "DO")
                    {
                        toRMBrate = USDtoRMBrate;
                    }
                    else if (CurrencyCode == "EUR")
                    {
                        toRMBrate = EURtoRMBrate;
                    }
                    else if (CurrencyCode == "SGD")
                    {
                        toRMBrate = SGDtoRMBrate;
                    }
                    else if (CurrencyCode == "HKD")
                    {
                        toRMBrate = HKDtoRMBrate;
                    }
                    //报价时售价
                    decimal price = dtHpd.Rows[0]["price"].AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                    string childXml = string.Empty;
                    TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq obj = new TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq();
                    obj.Username = travPaxUsername;
                    obj.Password = travPaxPassword;
                    obj.CheckInDate = Convert.ToDateTime(dtHpd.Rows[0]["beginDate"]).ToString("yyyy-MM-dd");
                    obj.CheckOutDate = Convert.ToDateTime(dtHpd.Rows[0]["endDate"]).ToString("yyyy-MM-dd");
                    obj.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                    obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                    obj.HotelID = HotelID.AsTargetType<int>(0);
                    obj.NationalityID = 66;
                    obj.ReferenceClient = dtHpd.Rows[0]["ReferenceClient"].ToString();

                    List<TravPaxHotelRecheckPriceReq.Room> roomList = new List<TravPaxHotelRecheckPriceReq.Room>();
                    for (int i = 0; i < dtHpd.Rows.Count; i++)
                    {
                        string[] ArrayAge = dtHpd.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                        string strAge = dtHpd.Rows[i]["Age"].AsTargetType<string>("");

                        string Age = string.Empty;
                        if (strAge != "")
                        {
                            List<string> list = new List<string>(strAge.Split(','));
                            if (ArrayAge.Length != 0)
                            {
                                for (int j = 0; j < ArrayAge.Length; j++)
                                {
                                    Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                                }
                            }
                        }
                        TravPaxHotelRecheckPriceReq.Room roomObj = new TravPaxHotelRecheckPriceReq.Room();
                        roomObj.ProductID = dtHpd.Rows[i]["RoomID"].AsTargetType<int>(0);
                        roomObj.Adults = dtHpd.Rows[i]["Adults"].AsTargetType<int>(0);
                        roomObj.Childs = dtHpd.Rows[i]["Childs"].AsTargetType<int>(0);
                        roomObj.MapRoomID = 0;
                        roomObj.AgreementID = dtHpd.Rows[i]["AgreementID"].AsTargetType<int>(0);
                        roomObj.SupplierCode = "SupplierCode";
                        roomObj.ChildsAge = Age;

                        TravPaxHotelRecheckPriceReq.SupplierRefIItem srObj = new TravPaxHotelRecheckPriceReq.SupplierRefIItem();
                        srObj.ID = dtHpd.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                        srObj.Value = dtHpd.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                        TravPaxHotelRecheckPriceReq.SupplierRefItem sriObj = new TravPaxHotelRecheckPriceReq.SupplierRefItem();
                        sriObj.SupplierRef = srObj;

                        roomObj.SupplierRef = sriObj;

                        roomList.Add(roomObj);
                    }

                    TravPaxHotelRecheckPriceReq.RoomClass RoomClassObj = new TravPaxHotelRecheckPriceReq.RoomClass();
                    RoomClassObj.Rooms = roomList;
                    RoomClassObj.TotalPrice = dtHpd.Rows[0]["Price"].ToString();
                    obj.RoomClass = RoomClassObj;

                    string strXml = Common.Common.XmlSerializeNoTitle<TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq>(obj).Replace("&lt;", "<").Replace("&gt;", ">");

                    //请求接口
                    try
                    {
                        string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                        //发起请求
                        DateTime beforDT = System.DateTime.Now;

                        //发起请求
                        result = Common.Common.PostHttp(url, postBoby);
                        DateTime afterDT = System.DateTime.Now;
                        TimeSpan ts = afterDT.Subtract(beforDT);
                        string SpendTime = ts.Seconds.ToString();
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口, 请求数据 : {0} , 返回数据 ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                    }
                    catch (Exception ex)
                    {
                        //日记
                        LogHelper.DoOperateLog(string.Format("Err:TravPax价格检查接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    HotelRecheckPriceResp objResp = Common.Common.DESerializer<HotelRecheckPriceResp>(result.Replace("&", ""));

                    if (objResp != null)
                    {
                        if (objResp.ErrorMessage != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            if (objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room != null && objResp.Hotels.RoomClasses.RoomClass[0].Status == "AL")
                            {
                                string RecheckCurrencyCode = objResp.Hotels.RoomClasses.RoomClass[0].CurrencyCode;
                                decimal RecheckTotalPrice = objResp.Hotels.RoomClasses.RoomClass[0].TotalPrice;
                                string RechecktoRMBrate = string.Empty;
                                if (RecheckCurrencyCode == "USD" || RecheckCurrencyCode == "DO")
                                {
                                    RechecktoRMBrate = USDtoRMBrate;
                                }
                                else if (RecheckCurrencyCode == "EUR")
                                {
                                    RechecktoRMBrate = EURtoRMBrate;
                                }
                                else if (RecheckCurrencyCode == "SGD")
                                {
                                    RechecktoRMBrate = SGDtoRMBrate;
                                }
                                else if (RecheckCurrencyCode == "HKD")
                                {
                                    RechecktoRMBrate = HKDtoRMBrate;
                                }
                                decimal RMBprice = RecheckTotalPrice * RechecktoRMBrate.AsTargetType<decimal>(0);
                                string updateHotelPriceSql = string.Format(@"update HotelPrice set Price='{0}',CurrencyCode='{1}',RMBprice='{2}',UpdateTime=GETDATE() 
    from HotelPrice hp WITH(NOLOCK) where  hp.HotelID='{3}' AND hp.Platform='TravPax' and hp.RateplanId='{4}';
   update HotelRecheckPrice set IsClose=0,UpdateTime=GETDATE() where HotelPriceID='{5}'", RecheckTotalPrice, RecheckCurrencyCode,RMBprice, HotelID, ratePlanId,
     dtHpd.Rows[0]["HotelPriceID"].AsTargetType<int>(0));
                                con.Update(updateHotelPriceSql);
                                
                                HotelPriceCollection hrdc = new HotelPriceCollection();
                                for (int i = 0; i < objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room.Count; i++)
                                {
                                    HotelRecheckPrice hrd = new HotelRecheckPrice();
                                    hrd.HotelPriceID = dtHpd.Rows[0]["HotelPriceID"].AsTargetType<int>(0);
                                    hrd.HotelID = HotelID.AsTargetType<string>("");
                                    hrd.RoomID = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomID.ToString();
                                    hrd.RoomName = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomName;
                                    hrd.Status = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].Status;
                                    hrd.BBCode = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].BBCode;
                                    hrd.TotalPrice = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].TotalPrice;
                                    hrd.RoomCurrencyCode = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].CurrencyCode;
                                    hrd.AgreementID = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].AgreementID;
                                    hrd.Adults = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].Adults;
                                    hrd.Childs = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].Childs;
                                    string strAge = string.Empty;
                                    if (objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].ChildsAge != null)
                                    {
                                        for (int k = 0; k < objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].ChildsAge.Age.Count; k++)
                                        {
                                            strAge += objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].ChildsAge.Age[k] + ",";
                                        }
                                    }
                                    hrd.Age = strAge.TrimEnd(',');
                                    hrd.SupplierRefID = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].SupplierRef.SupplierRef.ID;
                                    hrd.SupplierRefValue = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].SupplierRef.SupplierRef.Value;
                                    hrd.DateOfRate = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomPriceBreak.DailyRate.DateOfRate;
                                    hrd.DailyCurrencyCode = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomPriceBreak.DailyRate.CurrencyCode;
                                    hrd.DailyRate = objResp.Hotels.RoomClasses.RoomClass[0].Rooms.Room[i].RoomPriceBreak.DailyRate.DailyRate;
                                    hrd.IsClose = 1;
                                    hrd.CreateTime = DateTime.Now;
                                    hrd.UpdateTime = DateTime.Now;
                                    hrdc.Add(hrd);
                                }
                                con.Save(hrdc);
                                string sqlDt = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='TravPax'  AND RateplanId='{1}'"
                    , HotelID, ratePlanId);
                                dtRoom = ControllerFactory.GetController().GetDataTable(sqlDt);
                                for (int j = 0; j < dtRoom.Rows.Count; j++)
                                {
                                    RoomOutPrice.Room temp = new RoomOutPrice.Room();
                                    temp.ProductSource = "TravPax"; //产品来源 
                                    temp.MroomName = temp.XRoomName = temp.RoomName = dtRoom.Rows[j]["ClassName"].AsTargetType<string>("");// 房型名称
                                    temp.MroomId = temp.RoomTypeId = dtRoom.Rows[j]["CGuID"].AsTargetType<string>("");//房型ID(由于长度要求，取自己生成后的guid，要是用到下单时转为)

                                    //当前请求的房间人数
                                    temp.Adults = dtRoom.Rows[j]["Adults"].AsTargetType<int>(0);
                                    temp.Children = dtRoom.Rows[j]["Childs"].AsTargetType<int>(0);
                                    temp.ChildAges = dtRoom.Rows[j]["ChildsAge"].AsTargetType<string>("").Replace(",", "|");
                                    //最大入住人数
                                    temp.MaxPerson = dtRoom.Rows[j]["Adults"].AsTargetType<int>(0);

                                    temp.RatePlan = new List<RoomOutPrice.RatePlan>();
                                    RoomOutPrice.RatePlan thisRatePlan = new RoomOutPrice.RatePlan();
                                    string RatePlanName = "有早";
                                    thisRatePlan.PayMent = "y";//支付方式。目前全部前台支付
                                    int dtIsBreakfast = dtRoom.Rows[j]["IsBreakfast"].AsTargetType<int>(0);
                                    if (dtIsBreakfast == 0)
                                    {
                                        RatePlanName = "无早";
                                    }
                                    thisRatePlan.RatePlanName = RatePlanName;  //价格计划名称
                                    thisRatePlan.RoomName = RatePlanName;
                                    thisRatePlan.XRatePlanName = RatePlanName;
                                    thisRatePlan.RatePlanId = dtRoom.Rows[j]["RateplanId"].AsTargetType<string>("");  //价格计划ID

                                    thisRatePlan.RoomTypeId = dtRoom.Rows[j]["CGuID"].AsTargetType<string>("");
                                    temp.XRoomId = temp.RoomId = thisRatePlan.RoomId = string.Format("{0}_{1}", dtRoom.Rows[j]["CGuID"].AsTargetType<string>(""), dtRoom.Rows[j]["HotelNo"].AsTargetType<string>("")); //房型Id_价格计划ID
                                    //temp.XRoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>(""));
                                    thisRatePlan.BedType = "";

                                    //售价
                                    decimal RMBOutprice = dtRoom.Rows[j]["RMBprice"].AsTargetType<decimal>(0);
                                    //string[] ArrayPriceBreakdownDate = dt.Rows[j]["PriceBreakdownDate"].AsTargetType<string>("").Split(',');
                                    //总的底价 
                                    double sMoney = RMBOutprice.AsTargetType<double>(0);
                                    bool available = false; // 指示入离日期所有天是否可订 有一天不可订为false
                                    List<RoomOutPrice.Rate> rates = new List<RoomOutPrice.Rate>();
                                    int days = (Convert.ToDateTime(endTime) - Convert.ToDateTime(beginTime)).Days;
                                    for (int x = 0; x < days; x++)
                                    {
                                        RoomOutPrice.Rate rate = new RoomOutPrice.Rate();
                                        rate.Date = Convert.ToDateTime(beginTime).AddDays(x);
                                        rate.Available = true;  //某天是否可订


                                        rate.HotelID = (RMBOutprice / days).AsTargetType<string>("");  //底价

                                        rate.MemberRate = (RMBOutprice / days);
                                        rate.RetailRate = (RMBOutprice / days);
                                        rates.Add(rate);
                                    }


                                    //补上没有的日期
                                    if (days > rates.Count)
                                    {
                                        available = false;
                                        List<RoomOutPrice.Rate> tempR = new List<RoomOutPrice.Rate>();
                                        for (int y = 0; y < days; y++)
                                        {
                                            DateTime date = Convert.ToDateTime(beginTime).AddDays(y).Date;
                                            var one = rates.Find(c => c.Date.Date == date);
                                            if (one != null)
                                            {
                                                tempR.Add(one);
                                            }
                                            else
                                            {
                                                RoomOutPrice.Rate newOne = new RoomOutPrice.Rate() { Available = false, InvStatusCode = "0", MemberRate = 0, Date = date };
                                                tempR.Add(newOne);
                                            }
                                        }
                                        rates = tempR;
                                    }

                                    thisRatePlan.Rates = rates;
                                    thisRatePlan.Available = available;
                                    thisRatePlan.SMoney = Convert.ToDecimal(sMoney);

                                    decimal avgprice = 0;//均价
                                    if (rates.Count > 0)
                                    {
                                        avgprice = rates.Average(c => c.MemberRate);
                                        thisRatePlan.AveragePrice = avgprice.ToString("f2");
                                    }
                                    temp.CurrentAlloment = thisRatePlan.CurrentAlloment = 15; //库存

                                    //设置早餐数量
                                    int breakfast = -1;
                                    thisRatePlan.Breakfast = breakfast + "份早餐";
                                    if (dtIsBreakfast == 0)
                                    {
                                        thisRatePlan.Breakfast = 0 + "份早餐";
                                    }
                                    else
                                    {
                                        thisRatePlan.Breakfast = dtRoom.Rows[j]["Adults"].AsTargetType<int>(0) + "份早餐";
                                    }
                                    string zaocan = thisRatePlan.Breakfast.Replace("份早餐", "");
                                    if (!zaocan.Equals("0"))
                                    {
                                        switch (zaocan)
                                        {
                                            case "1": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含单早)"; temp.RoomName = temp.RoomName + "(含单早)"; break;
                                            case "2": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含双早)"; temp.RoomName = temp.RoomName + "(含双早)"; break;
                                            case "3": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含三早)"; temp.RoomName = temp.RoomName + "(含三早)"; break;
                                            case "4": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含四早)"; temp.RoomName = temp.RoomName + "(含四早)"; break;
                                        }
                                    }
                                    thisRatePlan.RoomName = thisRatePlan.RoomName + "[内宾]";

                                    temp.RoomPrice = Convert.ToInt32(avgprice);
                                    temp.BedType = thisRatePlan.BedType;
                                    temp.RatePlan.Add(thisRatePlan);
                                    rooms.Add(temp);
                                }
                            }
                            else
                            {
                                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                            }
                        }
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                }
                else
                {
                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax价格检查接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return "";
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }
        /// <summary>
        /// 创建订单（外部调用）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string Create_Order(string url, string travPaxUsername, string travPaxPassword, string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};
select hp.Price,hp.CurrencyCode,hp.ReferenceClient,hp.CheckInDate,hp.CheckOutDate,hp.Adults as hpAdults,hp.Childs as hpChilds,hp.RoomCount,hpd.* from HotelPrice hp WITH(NOLOCK) LEFT JOIN dbo.HotelRecheckPrice hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID='{0}' AND hp.Platform='TravPax' and RateplanId='{1}' and hpd.IsClose=1  order by hp.id desc",
    HotelID, RateplanId);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHotelPriceDetail = ds.Tables[1];
                if (dt.Rows.Count == 0 || dtHotelPriceDetail.Rows.Count == 0)
                {
                    LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", "数据库没值", DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "预订失败" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                //拿数据库值与传入值匹配
                string dtCheckInDate=Convert.ToDateTime(dtHotelPriceDetail.Rows[0]["CheckInDate"]).ToString("yyyy-MM-dd");
                string dtCheckOutDate=Convert.ToDateTime(dtHotelPriceDetail.Rows[0]["CheckOutDate"]).ToString("yyyy-MM-dd");
                int dtRoomCount=dtHotelPriceDetail.Rows[0]["RoomCount"].AsTargetType<int>(0);
                if (dtCheckInDate == beginTime && dtCheckOutDate == endTime && dtRoomCount == roomNum)
                {
                    TravPaxCreatePNRReq.CreatePNRReq obj = new TravPaxCreatePNRReq.CreatePNRReq();
                    obj.Username = travPaxUsername;
                    obj.Password = travPaxPassword;
                    obj.Supplier = "TRAVPAX";
                    obj.CheckInDate = dtCheckInDate;
                    obj.CheckOutDate = dtCheckOutDate;
                    obj.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                    obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                    obj.HotelID = HotelID.AsTargetType<int>(0);
                    obj.NationalityID = 66;
                    obj.ReferenceClient = dtHotelPriceDetail.Rows[0]["ReferenceClient"].ToString();
                    int Adults = dtHotelPriceDetail.Rows[0]["hpAdults"].AsTargetType<int>(0);
                    int Childs = dtHotelPriceDetail.Rows[0]["hpChilds"].AsTargetType<int>(0);

                    List<TravPaxCreatePNRReq.Room> roomList = new List<TravPaxCreatePNRReq.Room>();
                    int k = 0;
                    for (int i = 0; i < dtHotelPriceDetail.Rows.Count; i++)
                    {
                        string[] ArrayAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                        string strAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("");

                        string Age = string.Empty;
                        if (strAge != "")
                        {
                            List<string> list = new List<string>(strAge.Split(','));
                            if (ArrayAge.Length != 0)
                            {
                                for (int j = 0; j < ArrayAge.Length; j++)
                                {
                                    Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                                }
                            }
                        }
                        //每间房间的人数
                        //int Adults = dtHotelPriceDetail.Rows[i]["Adults"].AsTargetType<int>(0);
                        //int Childs = dtHotelPriceDetail.Rows[i]["Childs"].AsTargetType<int>(0);
                        TravPaxCreatePNRReq.Room roomObj = new TravPaxCreatePNRReq.Room();
                        roomObj.ProductID = dtHotelPriceDetail.Rows[i]["RoomID"].AsTargetType<int>(0);
                        roomObj.Adults = Adults;
                        roomObj.Childs = Childs;
                        roomObj.ChildsAge = Age;

                        TravPaxCreatePNRReq.SupplierRefIItem srObj = new TravPaxCreatePNRReq.SupplierRefIItem();
                        srObj.ID = dtHotelPriceDetail.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                        srObj.Value = dtHotelPriceDetail.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                        TravPaxCreatePNRReq.SupplierRefItem sriObj = new TravPaxCreatePNRReq.SupplierRefItem();
                        sriObj.SupplierRef = srObj;
                        roomObj.SupplierRef = sriObj;
                        List<TravPaxCreatePNRReq.PaxInfo> tppObjList = new List<TravPaxCreatePNRReq.PaxInfo>();
                        //TravPaxCreatePNRReq.PaxInfo tppObj = new TravPaxCreatePNRReq.PaxInfo();
                        for (int t = 0; t < customers[i].Customers.Count; t++)
                        {
                            string title = "Mr.";
                            if (customers[i].Customers[t].age < 12)
                            {
                                title = "CHILD";
                            }
                            else
                            {
                                if (customers[i].Customers[t].sex == 1)
                                {
                                    title = "Mr.";
                                }
                                else
                                {
                                    title = "Ms.";
                                }
                            }
                            TravPaxCreatePNRReq.PaxInfo tppObj = new TravPaxCreatePNRReq.PaxInfo() { FirstName = customers[i].Customers[t].firstName, LastName = customers[i].Customers[t].lastName, Title = title };
                            tppObjList.Add(tppObj);

                        }
                        roomObj.PaxInfos = tppObjList;

                        roomList.Add(roomObj);
                    }
                    TravPaxCreatePNRReq.RoomClass RoomClassObj = new TravPaxCreatePNRReq.RoomClass();
                    RoomClassObj.Rooms = roomList;
                    RoomClassObj.CurrencyCode = dtHotelPriceDetail.Rows[0]["CurrencyCode"].ToString();
                    RoomClassObj.TotalPrice = dtHotelPriceDetail.Rows[0]["Price"].ToString();
                    obj.RoomClass = RoomClassObj;

                    TravPaxCreatePNRReq.Leading leadObj = new TravPaxCreatePNRReq.Leading();
                    leadObj.Nationality = 66;
                    string strSex = string.Empty;
                    if (customers[0].Customers[0].sex == 1)
                    {
                        strSex = "Mr.";
                    }
                    else
                    {
                        strSex = "Ms.";
                    }
                    leadObj.Title = strSex;
                    leadObj.FirstName = customers[0].Customers[0].firstName;
                    leadObj.LastName = customers[0].Customers[0].lastName;

                    obj.Leading = leadObj;

                    string strXml = Common.Common.XmlSerializeNoTitle<TravPaxCreatePNRReq.CreatePNRReq>(obj).Replace("<PaxInfos>", "").Replace("</PaxInfos>", "").Replace("&lt;", "<").Replace("&gt;", ">");

                    //请求接口
                    try
                    {
                        string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                        //发起请求
                        DateTime beforDT = System.DateTime.Now;
                        result = Common.Common.PostHttp(url, postBoby);
                        DateTime afterDT = System.DateTime.Now;
                        TimeSpan ts = afterDT.Subtract(beforDT);
                        string SpendTime = ts.Seconds.ToString();
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax创建订单接口, 请求数据 : {0} , 返回数据 ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                    }
                    catch (Exception ex)
                    {
                        //日记
                        LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                        resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    TravPaxCreatePNRResp.PNRResp objResp = Common.Common.DESerializer<TravPaxCreatePNRResp.PNRResp>(result.Replace("&", ""));
                    if (objResp != null)
                    {
                        if (objResp.ErrorMessage != null)
                        {
                            resp = new Respone() { code = "99", mes = "预订失败," + objResp.ErrorMessage };
                            LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        BookingCollection bookingCollection = new BookingCollection();
                        for (int i = 0; i < objResp.Services.Service.Count; i++)
                        {
                            decimal ClientPartnerAmount = objResp.Services.Service[i].TotalFare;
                            string ClientCurrencyCode = objResp.Services.Service[i].FareCurrency;
                            string toRMBrate = string.Empty;
                            if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                            {
                                toRMBrate = USDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "EUR")
                            {
                                toRMBrate = EURtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "SGD")
                            {
                                toRMBrate = SGDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "HKD")
                            {
                                toRMBrate = HKDtoRMBrate;
                            }
                            decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                            Booking booking = new Booking();
                            //保存订单到数据库
                            booking.BookingCode = objResp.PNRNo;
                            booking.InOrderNum = inOrderNum;
                            booking.AgentBookingReference = objResp.Refer;
                            booking.RMBprice = price;
                            booking.ClientCurrencyCode = ClientCurrencyCode;
                            booking.ClientPartnerAmount = ClientPartnerAmount;
                            booking.RatePlanId = RateplanId;
                            booking.HotelId = HotelID.ToString();
                            booking.HotelNo = "";
                            booking.Platform = "TravPax";
                            booking.StatusCode = objResp.PNRStatus;
                            booking.StatusName = "";
                            booking.ServiceID = objResp.Services.Service[i].ID;
                            booking.ServiceName = objResp.Services.Service[i].Name;
                            booking.LegID = objResp.Services.Service[i].LegID;
                            booking.Leg_Status = objResp.Services.Service[i].Leg_Status;
                            booking.CreatTime = DateTime.Now;
                            booking.UpdateTime = DateTime.Now;
                            booking.EntityState = e_EntityState.Added;
                            bookingCollection.Add(booking);
                        }
                        con.Save(bookingCollection);
                        if (objResp.PNRStatus == "VC" || objResp.PNRStatus == "OKX" || objResp.PNRStatus == "OKS" || objResp.PNRStatus == "OK")
                        {
                            decimal ClientPartnerAmount = objResp.Services.Service[0].TotalFare;
                            string ClientCurrencyCode = objResp.Services.Service[0].FareCurrency;
                            string toRMBrate = string.Empty;
                            if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                            {
                                toRMBrate = USDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "EUR")
                            {
                                toRMBrate = EURtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "SGD")
                            {
                                toRMBrate = SGDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "HKD")
                            {
                                toRMBrate = HKDtoRMBrate;
                            }
                            decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                            resp = new Respone() { code = "00", orderNum = objResp.PNRNo, orderTotal = price, mes = "预订成功" };
                        }
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", "pNRResp != null", DateTime.Now.ToString()), "TravPax接口");
                        resp = new Respone() { code = "99", mes = "预订失败" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "预订失败,订单与搜索参数值不匹配" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "预订失败，" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 取消订单（外部调用）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string Cancel_Order(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"SELECT top 1 * FROM dbo.Booking WHERE BookingCode='{0}' and Platform='TravPax' ORDER BY UpdateTime desc", BookingCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string strXml = string.Format(@"<CancelPNRReq> <Username>{0}</Username> <Password>{1}</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>{2}</PNRNo> <ConfirmCode>{3}</ConfirmCode> </CancelPNRReq>"
                , travPaxUsername, travPaxPassword, BookingCode, dt.Rows[0]["AgentBookingReference"]);
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax取消订单接口, 请求数据 : {0} , 返回数据 ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消失败" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCancelPNRResp.CancelPNRResp objResp = Common.Common.DESerializer<TravPaxCancelPNRResp.CancelPNRResp>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = "取消失败" };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    //修改数据库订单状态
                    for (int i = 0; i < objResp.Services.Service.Count; i++)
                    {
                        string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Leg_Status='{1}' where BookingCode='{2}' and ServiceID={3}  and Platform='TravPax'"
                            , objResp.PNRStatus, objResp.Services.Service[i].Leg_Status, BookingCode, objResp.Services.Service[i].ID);
                        con.Update(strUpdateSql);
                    }
                    if (objResp.Services.Service[0].Leg_Status == "XX" || objResp.Services.Service[0].Leg_Status == "XXX" || objResp.Services.Service[0].Leg_Status == "XXS")
                    {
                        resp = new Respone() { code = "00", mes = "取消成功" };
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "取消失败" };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "取消失败" };
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "取消失败" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string Get_OrderDetail(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string strXml = string.Format(@"<RetrievePNRReq> <Username>{0}</Username> <Password>{1}</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>{2}</PNRNo> </RetrievePNRReq>"
                , travPaxUsername, travPaxPassword, BookingCode);
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax查询订单接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "查询订单失败，" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCreatePNRResp.PNRResp objResp = Common.Common.DESerializer<TravPaxCreatePNRResp.PNRResp>(result.Replace("&", ""));
                if (objResp != null)
                {
                    if (objResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = "查询订单失败" };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , Time:{1} ", objResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    for (int i = 0; i < objResp.Services.Service.Count; i++)
                    {
                        string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Leg_Status='{1}' where BookingCode='{2}' and ServiceID={3}  and Platform='TravPax' "
                            , objResp.PNRStatus, objResp.Services.Service[i].Leg_Status, BookingCode, objResp.Services.Service[i].ID);
                        con.Update(strUpdateSql);
                    }
                    string ItemStatusCode = objResp.Services.Service[0].Leg_Status;
                    string StatusName = string.Empty;
                    if (ItemStatusCode == "RQ ")
                    {
                        StatusName = "无法获得预订确认";
                    }
                    else if (ItemStatusCode == "VC" || ItemStatusCode == "OK" || ItemStatusCode == "OKX" || ItemStatusCode == "OKS")
                    {
                        StatusName = "预订成功";
                    }
                    else if (ItemStatusCode == "XX" || ItemStatusCode == "XXX" || ItemStatusCode == "XXS")
                    {
                        StatusName = "已取消";
                    }
                    else
                    {
                        StatusName = "未知状态";
                    }
                    decimal ClientPartnerAmount = objResp.Services.Service[0].TotalFare;
                    string ClientCurrencyCode = objResp.Services.Service[0].FareCurrency;
                    string toRMBrate = string.Empty;
                    if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                    {
                        toRMBrate = USDtoRMBrate;
                    }
                    else if (ClientCurrencyCode == "EUR")
                    {
                        toRMBrate = EURtoRMBrate;
                    }
                    else if (ClientCurrencyCode == "SGD")
                    {
                        toRMBrate = SGDtoRMBrate;
                    }
                    else if (ClientCurrencyCode == "HKD")
                    {
                        toRMBrate = HKDtoRMBrate;
                    }
                    //售价
                    decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                    resp = new Respone() { code = "00", orderNum = BookingCode, orderTotal = price, mes = "查询订单,该订单状态为:" + StatusName };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , Time:{1} ", "pNRResp != null", DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "查询订单失败" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "查询订单失败" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        public class TravPaxCustomers
        {
            public string FirstName { get; set;}//名
            public string LastName {get;set;}//姓
            public string Title { get; set; }//性别（先生：Mr.   女士：Ms.    太太：Mrs.   儿童：CHILD）前三者带英文.
        }
        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
    }
}