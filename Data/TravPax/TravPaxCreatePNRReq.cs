﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.TravPax
{
    public class TravPaxCreatePNRReq
    {
        public class CreatePNRReq
        {
            public string Username { get; set; }

            public string Password { get; set; }

            public string Supplier { get; set; }

            public string CheckInDate { get; set; }

            public string CheckOutDate { get; set; }

            public string CountryID { get; set; }

            public int CityID { get; set; }

            public int HotelID { get; set; }

            public int NationalityID { get; set; }

            public string ReferenceClient { get; set; }

            public string AgentRefer { get; set; }

            public Leading Leading { get; set; }

            public RoomClass RoomClass { get; set; }
        }
        public class Leading
        {
            public int Nationality { get; set; }

            public string Title { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }
        }
        public class RoomClass
        {
            public string TotalPrice { get; set; }

            public string CurrencyCode { get; set; }

            public List<Room> Rooms { get; set; }
        }
        public class Rooms
        {
            public List<Room> Room { get; set; }
        }
        public class Room
        {
            public int ProductID { get; set; }

            public int Adults { get; set; }

            public int Childs { get; set; }

            public string ChildsAge { get; set; }

            public List<PaxInfo> PaxInfos { get; set; }

            public SupplierRefItem SupplierRef { get; set; }
        }
        public class ChildsAge
        {
            public List<string> Age { get; set; }
        }
        public class PaxInfo
        {
            public string Title { get; set; }

            public string FirstName { get; set; }

            public string LastName { get; set; }
        }
        public class SupplierRefItem
        {
            public SupplierRefIItem SupplierRef { get; set; }
        }
        public class SupplierRefIItem
        {
            public int ID { get; set; }

            public string Value { get; set; }
        }
    }
}