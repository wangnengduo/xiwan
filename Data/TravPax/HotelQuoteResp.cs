﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.TravPax
{
    public class HotelQuoteResp
    {
        public string ClassName { get; set; }
        public string Rooms { get; set; }

        public string Status { get; set; }
        public decimal TotalPrice { get; set; }
        public string CurrencyCode { get; set; }
        public string TotalRoom { get; set; }
        public string SupplierCode { get; set; }
        public string IsOptional { get; set; }
        public string IsPromotion { get; set; }
    }
}