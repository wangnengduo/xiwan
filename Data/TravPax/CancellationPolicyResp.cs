﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.TravPax
{
    public class CancellationPolicyResp
    {
        public string ReferenceService { get; set; }
        public string Version { get; set; }
        public string ServiceID { get; set; }
        public string PenaltyCurrency { get; set; }
        public string ErrorMessage { get; set; }
        public string CancelType { get; set; }
        public string CancelDate { get; set; }
        public string CancelPrice { get; set; }
        public string CurrencyCode { get; set; }
    }
}