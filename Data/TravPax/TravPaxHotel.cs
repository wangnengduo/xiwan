﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Data.TravPax
{
    public class TravPaxHotel
    {
        public string ReferenceClient {get; set;}
        public string ReferenceService {get; set;}
        public string Version {get; set;}
        public HotelsItem Hotels { get; set; }
        public class HotelsItem
        {
            public HotelItem Hotel { get; set; }
        }
        public class HotelItem
        {
            public HotelItem Hotel { get; set; }
        }
    }
}