﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.TravPax
{
    [XmlRoot(ElementName = "HotelSearchResp")]
    public class HotelSearchResp
    {
        [XmlElement(ElementName = "ReferenceClient")]
        public string ReferenceClient { get; set; }

        [XmlElement(ElementName = "ReferenceService")]
        public string ReferenceService { get; set; }

        [XmlElement(ElementName = "Version")]
        public string Version { get; set; }
        [XmlElement(ElementName = "ErrorMessage")]
        public string ErrorMessage { get; set; }

        [XmlElement(ElementName = "Hotels")]
        public HotelItem Hotels { get; set; }

        public class HotelItem
        {
            [XmlElement(ElementName = "Hotel")]
            public Hotel Hotel { get; set; }
        }
        public class Hotel
        {
            [XmlElement(ElementName = "HotelID")]
            public int HotelID { get; set; }

            [XmlElement(ElementName = "HotelName")]
            public string HotelName { get; set; }

            [XmlElement(ElementName = "CountryID")]
            public string CountryID { get; set; }

            [XmlElement(ElementName = "CityID")]
            public int CityID { get; set; }

            [XmlElement(ElementName = "StarRating")]
            public string StarRating { get; set; }

            [XmlElement(ElementName = "Address")]
            public string Address { get; set; }

            [XmlElement(ElementName = "Address2")]
            public string Address2 { get; set; }

            [XmlElement(ElementName = "Fax")]
            public string Fax { get; set; }

            [XmlElement(ElementName = "Email")]
            public string Email { get; set; }

            [XmlElement(ElementName = "RoomClasses")]
            public RoomClasses RoomClasses { get; set; }

            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }

            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
        }
        public class RoomClasses
        {
            [XmlElement(ElementName = "RoomClass")]
            public List<RoomClass> RoomClass { get; set; }
        }
        public class RoomClass
        {
            [XmlElement(ElementName = "ClassName")]
            public string ClassName { get; set; }

            [XmlElement(ElementName = "Rooms")]
            public Rooms Rooms { get; set; }

            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }

            [XmlElement(ElementName = "TotalPrice")]
            public decimal TotalPrice { get; set; }

            [XmlElement(ElementName = "CurrencyCode")]
            public string CurrencyCode { get; set; }

            [XmlElement(ElementName = "TotalRoom")]
            public string TotalRoom { get; set; }

            [XmlElement(ElementName = "SupplierCode")]
            public string SupplierCode { get; set; }

            //[XmlElement(ElementName = "HotelOptionals")]
            //public string HotelOptionals { get; set; }

            [XmlElement(ElementName = "IsOptional")]
            public string IsOptional { get; set; }

            [XmlElement(ElementName = "IsPromotion")]
            public string IsPromotion { get; set; }
        }
        public class Rooms
        {
            [XmlElement(ElementName = "Room")]
            public List<Room> Room { get; set; }
        }
        public class Room
        {
            [XmlElement(ElementName = "RoomID")]
            public int RoomID { get; set; }

            [XmlElement(ElementName = "RoomName")]
            public string RoomName { get; set; }

            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }

            [XmlElement(ElementName = "BBCode")]
            public string BBCode { get; set; }

            [XmlElement(ElementName = "TotalPrice")]
            public decimal TotalPrice { get; set; }

            [XmlElement(ElementName = "CurrencyCode")]
            public string CurrencyCode { get; set; }

            [XmlElement(ElementName = "AgreementID")]
            public int AgreementID { get; set; }

            [XmlElement(ElementName = "Adults")]
            public int Adults { get; set; }

            [XmlElement(ElementName = "Childs")]
            public int Childs { get; set; }

            [XmlElement(ElementName = "ChildsAge")]
            public ChildsAge ChildsAge { get; set; }

            [XmlElement(ElementName = "SupplierRef")]
            public SupplierRefItem SupplierRef { get; set; }

            [XmlElement(ElementName = "SupplierCode")]
            public string SupplierCode { get; set; }

            [XmlElement(ElementName = "Supplier")]
            public string Supplier { get; set; }

            [XmlElement(ElementName = "IsPromotion")]
            public bool IsPromotion { get; set; }

            [XmlElement(ElementName = "PromotionDescription")]
            public string PromotionDescription { get; set; }

            [XmlElement(ElementName = "IsOptional")]
            public bool IsOptional { get; set; }

            [XmlElement(ElementName = "Penalty_Date")]
            public string Penalty_Date { get; set; }

            [XmlElement(ElementName = "Penalty_Type")]
            public int Penalty_Type { get; set; }

            [XmlElement(ElementName = "Penalty_Val")]
            public decimal Penalty_Val { get; set; }

            [XmlElement(ElementName = "RoomPriceBreak")]
            public RoomPriceBreak RoomPriceBreak { get; set; }
        }
        public class ChildsAge
        {
            //[XmlArray("Age")]
            [XmlElement(ElementName = "Age")]
            public List<int> Age { get; set; }
        }
        public class SupplierRefItem
        {
            [XmlElement(ElementName = "SupplierRef")]
            public SupplierRefIItem SupplierRef { get; set; }
        }
        public class SupplierRefIItem
        {
            [XmlElement(ElementName = "ID")]
            public int ID { get; set; }

            [XmlElement(ElementName = "Value")]
            public string Value { get; set; }
        }
        public class RoomPriceBreak
        {
            [XmlElement(ElementName = "DailyRate")]
            public List<DailyRateItem> DailyRate { get; set; }
        }
        public class DailyRateItem
        {
            [XmlElement(ElementName = "DateOfRate")]
            public DateTime DateOfRate { get; set; }
            [XmlElement(ElementName = "CurrencyCode")]
            public string CurrencyCode { get; set; }

            [XmlElement(ElementName = "DailyRate")]
            public decimal DailyRate { get; set; }
        }
        
    }
}