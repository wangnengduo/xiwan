﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using XiWan.DALFactory;
using System.Text.RegularExpressions;
using System.Collections;
using XiWan.DALFactory.Model;
using XiWan.Data.Redis;
using Newtonsoft.Json;
using System.Xml;
using XiWan.Data.TravPax;

namespace XiWan.Data
{
    public class TravPaxDAL
    {
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);

        #region 初始化本类
        private static TravPaxDAL _Instance;
        public static TravPaxDAL Instance
        {
            get
            {
                return new TravPaxDAL();
            }
        }
        #endregion
        /// <summary>
        /// 获取国籍
        /// </summary>
        /// <returns></returns>
        public DataTable GetNationality()
        {
            string sql = "select ID,NAME from nationality ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取目的地
        /// </summary>
        /// <param name="strName"></param>
        /// <returns></returns>
        public DataTable GetDestination(string strName)
        {
            string sql = string.Format(@"SELECT (CITYNAME+','+ CITY_DESTINATIONNAME +',' + NAME) as name,(CITY_ID+','+COUNTRY_CODE) as value FROM City_Destination where CITYNAME + CITY_DESTINATIONNAME + NAME like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 通过城市查询酒店
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="CountryCode"></param>
        /// <param name="CityID"></param>
        /// <param name="NationalityID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="ddlRoom"></param>
        /// <param name="rooms"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string SearchCity(string url, string travPaxUsername, string travPaxPassword, string CountryCode,string CityID,string NationalityID, string beginTime, string endTime, int ddlRoom, string rooms, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            string result = string.Empty;
            string[] ArrayRoom = rooms.TrimEnd(';').Split(';');
            string[] ArrayChilds;
            string SpendTime = string.Empty;
            try
            {
                TravSearchCityReq.CitySearchReq objReq = new TravSearchCityReq.CitySearchReq();
                objReq.Username = travPaxUsername;
                objReq.Password = travPaxPassword;
                objReq.CheckInDate = beginTime;
                objReq.CheckOutDate = endTime;
                objReq.CountryID = CountryCode;
                objReq.CityID = CityID.AsTargetType<int>(0);
                objReq.NationalityID = NationalityID.AsTargetType<int>(0);

                List<TravSearchCityReq.RoomRequest> roomListReq = new List<TravSearchCityReq.RoomRequest>();
                for (int i = 0; i < ddlRoom; i++)
                {
                    string Age = string.Empty;
                    ArrayChilds = ArrayRoom[i].Split(',');
                    if (ArrayChilds[1] == "1")
                    {
                        Age += string.Format(@"<Age>{0}</Age>", ArrayChilds[2]);
                    }
                    else if (ArrayChilds[1] == "2")
                    {
                        Age += string.Format(@"<Age>{0}</Age><Age>{1}</Age>", ArrayChilds[2], ArrayChilds[3]);
                    }

                    TravSearchCityReq.RoomRequest roomObj = new TravSearchCityReq.RoomRequest();
                    roomObj.Adults = ArrayChilds[0].AsTargetType<int>(0);
                    roomObj.Childs = ArrayChilds[1].AsTargetType<int>(0);
                    roomObj.ChildsAge = Age;

                    roomListReq.Add(roomObj);
                }
                objReq.Rooms = roomListReq;
                string strXml = Common.Common.XmlSerializeNoTitle<TravSearchCityReq.CitySearchReq>(objReq).Replace("&lt;", "<").Replace("&gt;", ">");

                string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                //请求接口
                try
                {
                    DateTime beforDT = System.DateTime.Now;

                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:CitySearch, Req:{0} , Resq：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "CitySearch");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
            return SpendTime;
        }
        /// <summary>
        /// 获取报价
        /// </summary>
        /// <param name="hotelID"></param>
        /// <param name="countryID"></param>
        /// <param name="cityID"></param>
        /// <param name="referenceClient"></param>
        /// <param name="nationalityID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="ddlRoom"></param>
        /// <param name="rooms"></param>
        /// <param name="input_page"></param>
        /// <param name="input_row"></param>
        /// <returns></returns>
        public void GetRealHotelSQuote(string url, string travPaxUsername, string travPaxPassword, string hotelID,string NationalityID, string beginTime, string endTime, int ddlRoom,int Adults,int Childs, string rooms, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            try
            {
                string result = string.Empty;
                string[] ArrayRoom = rooms.TrimEnd(';').Split(';');
                string[] ArrayChilds;

                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0} ", hotelID);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);

                HotelQuoteReq.HotelSearchReq objReq = new HotelQuoteReq.HotelSearchReq();
                objReq.Username = travPaxUsername;
                objReq.Password = travPaxPassword;
                objReq.ReferenceClient = "";
                objReq.CheckInDate = beginTime;
                objReq.CheckOutDate = endTime;
                objReq.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                objReq.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                objReq.HotelID = hotelID.AsTargetType<int>(0);
                objReq.NationalityID = NationalityID.AsTargetType<int>(0);

                List<HotelQuoteReq.RoomRequest> roomListReq = new List<HotelQuoteReq.RoomRequest>();
                for (int i = 0; i < ddlRoom; i++)
                {
                    string Age = string.Empty;
                    ArrayChilds = ArrayRoom[i].Split(',');
                    if (ArrayChilds[1] == "1")
                    {
                        Age += string.Format(@"<Age>{0}</Age>", ArrayChilds[2]);
                    }
                    else if (ArrayChilds[1] == "2")
                    {
                        Age += string.Format(@"<Age>{0}</Age><Age>{1}</Age>", ArrayChilds[2], ArrayChilds[3]);
                    }

                    HotelQuoteReq.RoomRequest roomObj = new HotelQuoteReq.RoomRequest();
                    roomObj.Adults = ArrayChilds[0].AsTargetType<int>(0);
                    roomObj.Childs = ArrayChilds[1].AsTargetType<int>(0);
                    roomObj.ChildsAge = Age;

                    roomListReq.Add(roomObj);
                }
                objReq.Rooms = roomListReq;
                string strXml = Common.Common.XmlSerializeNoTitle<HotelQuoteReq.HotelSearchReq>(objReq).Replace("&lt;", "<").Replace("&gt;", ">");

                string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                //请求接口
                try
                {
                    DateTime beforDT = System.DateTime.Now;

                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("TravPax：HotelSearchReq ,Req :{0} , Resq ：{1} ，SpendTime : {2} Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "TravPax接口");
                }
                HotelSearchResp hsr = Common.Common.DESerializer<HotelSearchResp>(result);
                
                if (hsr != null)
                {
                    if (hsr.ErrorMessage != null)
                    {
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , Time:{1} ", hsr.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return ;
                    }
                    List<HotelSearchResp.RoomClass> r = Common.Common.DESerializer<HotelSearchResp>(result).Hotels.Hotel.RoomClasses.RoomClass.Where(x => x.Status.Contains("AL")).ToList(); ;
                    HotelPriceCollection objCollection = new HotelPriceCollection();
                    if (r != null)
                    {
                        string updateHotelPriceSql = string.Format(@"update HotelPrice  set IsClose=0,UpdateTime=GETDATE() 
where HotelID={0} AND TerminalCode='TravPax' AND Adults={1} AND Childs={2}   AND CheckInDate='{3}' AND CheckOutDate='{4}'",
hotelID, Adults, Childs, beginTime, endTime);
                        con.Update(updateHotelPriceSql);
                        for (int i = 0; i < r.Count; i++)
                        {
                            HotelPrice obj = new HotelPrice();
                            obj.HotelID = hotelID;
                            obj.HotelNo = "";
                            obj.TerminalCode = "TravPax";
                            obj.ClassName = r[i].ClassName;
                            obj.Price = r[i].TotalPrice;
                            obj.CurrencyCode = r[i].CurrencyCode;
                            if (r[i].Rooms.Room[0].BBCode == "BB")
                            {
                                obj.IsBreakfast = 1;
                            }
                            else
                            {
                                obj.IsBreakfast = 0;
                            }
                            obj.ReferenceClient = hsr.ReferenceClient;
                            obj.RoomCount = ddlRoom;
                            obj.Adults = Adults;
                            obj.Childs = Childs;
                            obj.CheckInDate = Convert.ToDateTime(beginTime);
                            obj.CheckOutDate = Convert.ToDateTime(endTime);
                            obj.IsClose = 1;
                            obj.CreateTime = DateTime.Now;
                            obj.UpdateTime = DateTime.Now;
                            con.Save(obj);

                            HotelPriceDetailCollection hpdl = new HotelPriceDetailCollection();
                            List<HotelSearchResp.Room> roomList = r[i].Rooms.Room;
                            if (roomList != null)
                            {
                                for (int j = 0; j < roomList.Count; j++)
                                {
                                    HotelPriceDetail hpd = new HotelPriceDetail();
                                    hpd.HotelPriceID = obj.ID;
                                    hpd.HotelID = hotelID;
                                    hpd.RoomID = roomList[j].RoomID.ToString();
                                    hpd.RoomName = roomList[j].RoomName;
                                    hpd.Status = roomList[j].Status;
                                    hpd.BBCode = roomList[j].BBCode;
                                    hpd.TotalPrice = roomList[j].TotalPrice;
                                    hpd.RoomCurrencyCode = roomList[j].CurrencyCode;
                                    hpd.AgreementID = roomList[j].AgreementID;
                                    hpd.Adults = roomList[j].Adults;
                                    hpd.Childs = roomList[j].Childs;
                                    string strAge = string.Empty;
                                    for (int k = 0; k < roomList[j].ChildsAge.Age.Count; k++)
                                    {
                                        strAge += roomList[j].ChildsAge.Age[k] + ",";
                                    }
                                    hpd.Age = strAge.TrimEnd(',');
                                    hpd.SupplierRefID = roomList[j].SupplierRef.SupplierRef.ID;
                                    hpd.SupplierRefValue = roomList[j].SupplierRef.SupplierRef.Value;
                                    hpd.DateOfRate = roomList[j].RoomPriceBreak.DailyRate.DateOfRate;
                                    hpd.DailyCurrencyCode = roomList[j].RoomPriceBreak.DailyRate.CurrencyCode;
                                    hpd.DailyRate = roomList[j].RoomPriceBreak.DailyRate.DailyRate;
                                    hpd.IsClose = 1;
                                    hpd.CreateTime = DateTime.Now;
                                    hpd.UpdateTime = DateTime.Now;
                                    hpdl.Add(hpd);
                                }
                                con.Save(hpdl);
                            }

                        }
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "r != null", strXml, DateTime.Now.ToString()), "TravPax接口");
                        return;
                    }
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                    return;
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
        }

        public DataTable GetRealHotelDetail(string url, string travPaxUsername, string travPaxPassword, string hotelID, string countryID, string cityID, string referenceClient, string nationalityID, string beginTime, string endTime, int ddlRoom, string rooms, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            DataTable dt = new DataTable();
            try
            {
                string result = string.Empty;
                string[] ArrayRoom = rooms.Split(';');
                string[] ArrayChilds;

                string strXml = string.Format(@"<HotelSearchReq> <Username>{0}</Username> <Password>{1}</Password> <ReferenceClient>{2}</ReferenceClient>
<CheckInDate>{3}</CheckInDate> <CheckOutDate>{4}</CheckOutDate> <CountryID>{5}</CountryID> <CityID>{6}</CityID> <HotelID>{7}</HotelID> <NationalityID>{8}</NationalityID> <Rooms>  "
                    , travPaxUsername, travPaxPassword, "", beginTime, endTime, countryID, cityID, hotelID, nationalityID);
                string childXml = string.Empty;
                for (int i = 0; i < ddlRoom; i++)
                {
                    ArrayChilds = ArrayRoom[i].Split(',');
                    if (ArrayChilds[1] == "1")
                    {
                        childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2]);
                    }
                    else if (ArrayChilds[1] == "2")
                    {
                        childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> <Age>{3}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2], ArrayChilds[3]);
                    }
                    else
                    {
                        childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge/> </RoomRequest>", ArrayChilds[0], ArrayChilds[1]);
                    }
                }
                string hotelSearchXml = strXml + childXml + " </Rooms> </HotelSearchReq>";
                string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(hotelSearchXml), btnPost);
                //请求接口
                try
                {
                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax获取报价接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", hotelSearchXml, result, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, hotelSearchXml, DateTime.Now.ToString()), "TravPax接口");
                }
                if (result.Contains("<ErrorMessage>"))
                {
                    return dt;
                }
                else
                {
                    Regex regex = new Regex("<RoomClasses>");//以RoomClasses分割
                    string[] bit = regex.Split(result);
                    if (bit.Length > 1)
                    {
                        result = bit[1];
                        Regex regexHotel = new Regex("</RoomClasses>");
                        bit = regexHotel.Split(result);
                        result = bit[0];
                        result = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <ArrayOfRoomClass xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" + result + "</ArrayOfRoomClass>");
                        List<HotelSearchResp.RoomClass> r = Common.Common.DESerializer<List<HotelSearchResp.RoomClass>>(result);
                        List<HotelQuoteResp> HotelQuoteList = new List<HotelQuoteResp>();
                        if (r != null)
                        {
                            //RoomClass r3 = r.OrderBy(x => x.TotalPrice).FirstOrDefault();//拿到价格最低的
                            for (int i = 0; i < r.Count; i++)
                            {
                                string Rooms = Newtonsoft.Json.JsonConvert.SerializeObject(r[i].Rooms).Replace("\"", "\'");
                                HotelQuoteResp obj = new HotelQuoteResp();
                                obj.ClassName = r[i].ClassName;
                                obj.Rooms = Rooms;
                                obj.Status = r[i].Status;
                                obj.TotalPrice = r[i].TotalPrice;
                                obj.CurrencyCode = r[i].CurrencyCode;
                                obj.TotalRoom = r[i].TotalRoom;
                                obj.SupplierCode = r[i].SupplierCode;
                                obj.IsOptional = r[i].IsOptional;
                                obj.IsPromotion = r[i].IsPromotion;
                                HotelQuoteList.Add(obj);
                            }
                            dt = CConvert.IListToDataTable<HotelQuoteResp>(HotelQuoteList);
                            //dt = CConvert.DtFromObject<TravPaxRoomClass.RoomClass>(r1);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
            return dt;
        }

        /// <summary>
        /// 获取取消期限
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="referenceClient"></param>
        /// <param name="nationalityID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string GetCancellationPolicy(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID,string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try 
            {
                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};

select hp.Price,hp.CurrencyCode,hp.ReferenceClient,hpd.* from HotelPrice hp WITH(NOLOCK) LEFT JOIN dbo.HotelPriceDetail hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID={0} AND hp.TerminalCode='TravPax' AND hp.Adults={1} AND hp.Childs={2} AND hp.CheckInDate='{3}' AND hp.CheckOutDate='{4}' AND hp.IsClose=1 AND hpd.IsClose=1 and hp.ID={5}  order by hp.id desc",
HotelID, Adults, Childs, beginTime, endTime, HotelPriceID);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHotelPriceDetail = ds.Tables[1];
                string childXml=string.Empty;
                TravPaxCancellationPolicyReq.CancellationPolicyReq obj = new TravPaxCancellationPolicyReq.CancellationPolicyReq();
                obj.Username = travPaxUsername;
                obj.Password = travPaxPassword;
                obj.Supplier = "TRAVPAX";
                obj.CheckInDate = beginTime;
                obj.CheckOutDate = endTime;
                obj.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                obj.HotelID = HotelID;
                obj.NationalityID =  NationalityID.AsTargetType<int>(0);
                obj.ReferenceClient = dtHotelPriceDetail.Rows[0]["ReferenceClient"].ToString();

                List<TravPaxCancellationPolicyReq.Room> roomList = new List<TravPaxCancellationPolicyReq.Room>();
                for (int i = 0; i < dtHotelPriceDetail.Rows.Count; i++)
                {
                    string[] ArrayAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                    string strAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("");

                    string Age = string.Empty;
                    if (strAge != "")
                    {
                        List<string> list = new List<string>(strAge.Split(','));
                        if (ArrayAge.Length != 0)
                        {
                            for (int j = 0; j < ArrayAge.Length; j++)
                            {
                                Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                            }
                        }
                    }
                    TravPaxCancellationPolicyReq.Room roomObj = new TravPaxCancellationPolicyReq.Room();
                    roomObj.ProductID = dtHotelPriceDetail.Rows[i]["RoomID"].AsTargetType<int>(0);
                    roomObj.Adults = dtHotelPriceDetail.Rows[i]["Adults"].AsTargetType<int>(0);
                    roomObj.Childs = dtHotelPriceDetail.Rows[i]["Childs"].AsTargetType<int>(0);
                    roomObj.MapRoomID = 0;
                    roomObj.AgreementID = dtHotelPriceDetail.Rows[i]["AgreementID"].AsTargetType<int>(0);
                    roomObj.SupplierCode = "TRAVPAX";
                    roomObj.ChildsAge = Age;

                    TravPaxCancellationPolicyReq.SupplierRefIItem srObj = new TravPaxCancellationPolicyReq.SupplierRefIItem();
                    srObj.ID = dtHotelPriceDetail.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                    srObj.Value = dtHotelPriceDetail.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                    TravPaxCancellationPolicyReq.SupplierRefItem sriObj = new TravPaxCancellationPolicyReq.SupplierRefItem();
                    sriObj.SupplierRef = srObj;

                    roomObj.SupplierRef = sriObj;

                    roomList.Add(roomObj);
                }

                TravPaxCancellationPolicyReq.RoomClass RoomClassObj = new TravPaxCancellationPolicyReq.RoomClass();
                RoomClassObj.Rooms = roomList;
                RoomClassObj.CurrencyCode = dtHotelPriceDetail.Rows[0]["CurrencyCode"].ToString();
                RoomClassObj.TotalPrice = dtHotelPriceDetail.Rows[0]["TotalPrice"].ToString();
                obj.RoomClass = RoomClassObj;
                string strXml = Common.Common.XmlSerializeNoTitle<TravPaxCancellationPolicyReq.CancellationPolicyReq>(obj).Replace("&lt;", "<").Replace("&gt;", ">");
                
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;

                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio: CancellationPolicyReq ,Req :{0} , Resq ：{1} ，SpendTime : {2} , Time:{3} ", strXml, result,SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取取消政策接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "TravPax接口");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                CancellationPolicyResp cpr = Common.Common.DESerializer<CancellationPolicyResp>(result);
                if (cpr != null)
                {
                    if (cpr.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = cpr.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax获取取消政策接口 ,err:{0} , Time:{1} ", cpr.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    resp = new Respone() { code = "00", mes = cpr.CancelType + ";" + cpr.CancelDate + ";" + cpr.CancelPrice + ";" + cpr.ErrorMessage };
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员"  };
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax获取取消政策接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax获取取消政策接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 价格检查
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string GetHotelRecheckPrice(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID, string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};

select hp.Price,hp.CurrencyCode,hp.ReferenceClient,hpd.* from HotelPrice hp WITH(NOLOCK) LEFT JOIN dbo.HotelPriceDetail hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID={0} AND hp.TerminalCode='TravPax' AND hp.Adults={1} AND hp.Childs={2} AND hp.CheckInDate='{3}' AND hp.CheckOutDate='{4}' AND hp.IsClose=1 AND hpd.IsClose=1 and hp.ID={5}  order by hp.id desc",
HotelID, Adults,Childs, beginTime, endTime,HotelPriceID);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHotelPriceDetail = ds.Tables[1];
                string childXml = string.Empty;
                TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq obj = new TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq();
                obj.Username = travPaxUsername;
                obj.Password = travPaxPassword;
                obj.CheckInDate = beginTime;
                obj.CheckOutDate = endTime;
                obj.CountryID = dt.Rows[0]["COUNTRY_CODE"].ToString();
                obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                obj.HotelID = HotelID;
                obj.NationalityID = NationalityID.AsTargetType<int>(0);
                obj.ReferenceClient = dtHotelPriceDetail.Rows[0]["ReferenceClient"].ToString();

                List<TravPaxHotelRecheckPriceReq.Room> roomList = new List<TravPaxHotelRecheckPriceReq.Room>();
                for (int i = 0; i < dtHotelPriceDetail.Rows.Count; i++)
                {
                    string[] ArrayAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                    string strAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("");

                    string Age = string.Empty;
                    if (strAge != "")
                    {
                        List<string> list = new List<string>(strAge.Split(','));
                        if (ArrayAge.Length != 0)
                        {
                            for (int j = 0; j < ArrayAge.Length; j++)
                            {
                                Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                            }
                        }
                    }
                    TravPaxHotelRecheckPriceReq.Room roomObj = new TravPaxHotelRecheckPriceReq.Room();
                    roomObj.ProductID = dtHotelPriceDetail.Rows[i]["RoomID"].AsTargetType<int>(0);
                    roomObj.Adults = dtHotelPriceDetail.Rows[i]["Adults"].AsTargetType<int>(0);
                    roomObj.Childs = dtHotelPriceDetail.Rows[i]["Childs"].AsTargetType<int>(0);
                    roomObj.MapRoomID = 0;
                    roomObj.AgreementID = dtHotelPriceDetail.Rows[i]["AgreementID"].AsTargetType<int>(0);
                    roomObj.SupplierCode = "SupplierCode";
                    roomObj.ChildsAge = Age;

                    TravPaxHotelRecheckPriceReq.SupplierRefIItem srObj = new TravPaxHotelRecheckPriceReq.SupplierRefIItem();
                    srObj.ID = dtHotelPriceDetail.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                    srObj.Value = dtHotelPriceDetail.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                    TravPaxHotelRecheckPriceReq.SupplierRefItem sriObj = new TravPaxHotelRecheckPriceReq.SupplierRefItem();
                    sriObj.SupplierRef = srObj;

                    roomObj.SupplierRef = sriObj;

                    roomList.Add(roomObj);
                }

                TravPaxHotelRecheckPriceReq.RoomClass RoomClassObj = new TravPaxHotelRecheckPriceReq.RoomClass();
                RoomClassObj.Rooms = roomList;
                RoomClassObj.TotalPrice = dtHotelPriceDetail.Rows[0]["Price"].ToString();
                obj.RoomClass = RoomClassObj;

                string strXml = Common.Common.XmlSerializeNoTitle<TravPaxHotelRecheckPriceReq.HotelRecheckPriceReq>(obj).Replace("&lt;", "<").Replace("&gt;", ">");

                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;

                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:HotelRecheckPriceReq, Req : {0} , Resq ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result,SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax价格检查接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                HotelRecheckPriceResp hsr = Common.Common.DESerializer<HotelRecheckPriceResp>(result);

                if (hsr != null)
                {
                    if (hsr.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = hsr.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口 ,err:{0} , Time:{1} ", hsr.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    resp = new Respone() { code = "00", result = hsr.Hotels.RoomClasses.RoomClass[0].Status , mes = hsr.Hotels.RoomClasses.RoomClass[0].TotalPrice.AsTargetType<string>("0") }; 
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax价格检查接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax价格检查接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 创建订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string CreatePNR(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID, string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
            string sql = string.Format(@"select * from TravPaxHotel WITH(NOLOCK) where SERVICE_ID={0};
select hp.Price,hp.CurrencyCode,hp.ReferenceClient,hpd.* from HotelPrice hp WITH(NOLOCK) LEFT JOIN dbo.HotelPriceDetail hpd WITH(NOLOCK) ON hpd.HotelPriceID=hp.ID
where  hp.HotelID={0} AND hp.TerminalCode='TravPax' AND hp.Adults={1} AND hp.Childs={2} AND hp.CheckInDate='{3}' AND hp.CheckOutDate='{4}' AND hp.IsClose=1 AND hp.IsClose=1   and hp.ID={5}  order by hp.id desc",
HotelID, Adults,Childs, beginTime, endTime,HotelPriceID);
                DataSet ds = con.GetDataSet(sql);

                DataTable dt = ds.Tables[0];
                DataTable dtHotelPriceDetail = ds.Tables[1];

                TravPaxCreatePNRReq.CreatePNRReq obj = new TravPaxCreatePNRReq.CreatePNRReq();
                obj.Username=travPaxUsername;
                obj.Password =travPaxPassword;
                obj.Supplier="TRAVPAX";
                obj.CheckInDate=beginTime;
                obj.CheckOutDate =endTime;
                obj.CountryID =dt.Rows[0]["COUNTRY_CODE"].ToString();
                obj.CityID = dt.Rows[0]["CITY_ID"].AsTargetType<int>(0);
                obj.HotelID =HotelID;
                obj.NationalityID = NationalityID.AsTargetType<int>(0);
                obj.ReferenceClient=dtHotelPriceDetail.Rows[0]["ReferenceClient"].ToString();

                List<TravPaxCreatePNRReq.Room> roomList = new List<TravPaxCreatePNRReq.Room>();
                for (int i = 0; i < dtHotelPriceDetail.Rows.Count; i++)
                {
                    string[] ArrayAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("").Split(',');

                    string strAge = dtHotelPriceDetail.Rows[i]["Age"].AsTargetType<string>("");

                    string Age = string.Empty;
                    if (strAge != "")
                    {
                        List<string> list = new List<string>(strAge.Split(','));
                        if (ArrayAge.Length != 0)
                        {
                            for (int j = 0; j < ArrayAge.Length; j++)
                            {
                                Age += string.Format(@"<Age>{0}</Age>", ArrayAge[j].AsTargetType<int>(0));
                            }
                        }
                    }
                    TravPaxCreatePNRReq.Room roomObj = new TravPaxCreatePNRReq.Room();
                    roomObj.ProductID = dtHotelPriceDetail.Rows[i]["RoomID"].AsTargetType<int>(0);
                    roomObj.Adults = dtHotelPriceDetail.Rows[i]["Adults"].AsTargetType<int>(0);
                    roomObj.Childs = dtHotelPriceDetail.Rows[i]["Childs"].AsTargetType<int>(0);
                    roomObj.ChildsAge = Age;

                    TravPaxCreatePNRReq.SupplierRefIItem srObj = new TravPaxCreatePNRReq.SupplierRefIItem();
                    srObj.ID = dtHotelPriceDetail.Rows[i]["SupplierRefID"].AsTargetType<int>(0);
                    srObj.Value = dtHotelPriceDetail.Rows[i]["SupplierRefValue"].AsTargetType<string>("");

                    TravPaxCreatePNRReq.SupplierRefItem sriObj = new TravPaxCreatePNRReq.SupplierRefItem();
                    sriObj.SupplierRef = srObj;
                    roomObj.SupplierRef = sriObj;

                    List<TravPaxCreatePNRReq.PaxInfo> tppObjList = new List<TravPaxCreatePNRReq.PaxInfo>();
                    TravPaxCreatePNRReq.PaxInfo tppObj = new TravPaxCreatePNRReq.PaxInfo();

                    if (i == 0)
                    {
                        TravPaxCreatePNRReq.PaxInfo tppObj1 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Zhang", LastName = "San", Title = "Mr." };
                        //TravPaxCreatePNRReq.PaxInfo tppObj2 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Li", LastName = "Si", Title = "Mr." };
                        //TravPaxCreatePNRReq.PaxInfo tppObj3 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Wang", LastName = "Wu", Title = "Mr." };
                        tppObjList.Add(tppObj1);
                        //tppObjList.Add(tppObj2);
                        //tppObjList.Add(tppObj3);
                        /*
                        tppObj.FirstName = "Zhang";
                        tppObj.LastName = "San";
                        tppObj.Title = "Mr";
                        tppObjList.Add(tppObj);
                        //roomObj.PaxInfo.Add(tppObj);
                        tppObj.FirstName = "Li";
                        tppObj.LastName = "Si";
                        tppObj.Title = "Mr";
                        tppObjList.Add(tppObj);
                        //roomObj.PaxInfo.Add(tppObj);
                        tppObj.FirstName = "Wang";
                        tppObj.LastName = "Wu";
                        tppObj.Title = "Mr";
                        tppObjList.Add(tppObj);
                        //roomObj.PaxInfo.Add(tppObj);*/
                    }
                    else if (i == 1)
                    {
                        TravPaxCreatePNRReq.PaxInfo tppObj1 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Li", LastName = "Si", Title = "Mr." };
                        TravPaxCreatePNRReq.PaxInfo tppObj2 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Wang", LastName = "Wu", Title = "Mr." };
                        TravPaxCreatePNRReq.PaxInfo tppObj3 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Huang", LastName = "Liu", Title = "Mr." };
                        tppObjList.Add(tppObj1);
                        tppObjList.Add(tppObj2);
                        tppObjList.Add(tppObj3);
                    }
                    else if (i == 2)
                    {
                        TravPaxCreatePNRReq.PaxInfo tppObj1 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Chen", LastName = "Qi", Title = "Mr." };
                        TravPaxCreatePNRReq.PaxInfo tppObj2 = new TravPaxCreatePNRReq.PaxInfo() { FirstName = "Liang", LastName = "Ba", Title = "Mr." };
                        tppObjList.Add(tppObj1);
                        tppObjList.Add(tppObj2);
                    }

                    roomObj.PaxInfos = tppObjList;

                    roomList.Add(roomObj);   
                }
                TravPaxCreatePNRReq.RoomClass RoomClassObj = new TravPaxCreatePNRReq.RoomClass();
                RoomClassObj.Rooms = roomList;
                RoomClassObj.CurrencyCode = dtHotelPriceDetail.Rows[0]["CurrencyCode"].ToString();
                RoomClassObj.TotalPrice = dtHotelPriceDetail.Rows[0]["Price"].ToString();
                obj.RoomClass = RoomClassObj;

                TravPaxCreatePNRReq.Leading leadObj = new TravPaxCreatePNRReq.Leading();
                leadObj.Nationality = 66;
                leadObj.Title = "Mr";
                leadObj.FirstName = "Zhang";
                leadObj.LastName = "San";

                obj.Leading = leadObj;

                string strXml = Common.Common.XmlSerializeNoTitle<TravPaxCreatePNRReq.CreatePNRReq>(obj).Replace("<PaxInfos>", "").Replace("</PaxInfos>", "").Replace("&lt;", "<").Replace("&gt;", ">");

                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:CreatePNRReq, Req : {0} , Resq ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCreatePNRResp.PNRResp pNRResp = Common.Common.DESerializer<TravPaxCreatePNRResp.PNRResp>(result);
                if (pNRResp != null)
                {
                    if (pNRResp.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = pNRResp.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", pNRResp.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    BookingCollection bookingCollection=new BookingCollection();
                    for (int i = 0; i < pNRResp.Services.Service.Count; i++)
                    {
                        Booking booking = new Booking();
                        //保存订单到数据库
                        booking.BookingCode = pNRResp.PNRNo;
                        booking.AgentBookingReference = pNRResp.Refer;
                        booking.ClientCurrencyCode = pNRResp.Services.Service[i].FareCurrency;
                        booking.ClientPartnerAmount = pNRResp.Services.Service[i].TotalFare.AsTargetType<string>("0");
                        booking.HotelId = HotelID.ToString();
                        booking.HotelNo = "";
                        booking.Platform = "TravPax";
                        booking.StatusCode = pNRResp.PNRStatus;
                        booking.StatusName = "";
                        booking.ServiceID = pNRResp.Services.Service[i].ID;
                        booking.ServiceName = pNRResp.Services.Service[i].Name;
                        booking.LegID = pNRResp.Services.Service[i].LegID;
                        booking.Leg_Status = pNRResp.Services.Service[i].Leg_Status;
                        booking.CreatTime = DateTime.Now;
                        booking.UpdateTime = DateTime.Now;
                        booking.EntityState = e_EntityState.Added;
                        bookingCollection.Add(booking);
                    }
                    con.Save(bookingCollection);
                    resp = new Respone() { code = "00", result = pNRResp.PNRStatus };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err:TravPax获取报价接口 ,err:{0} , Time:{1} ", "pNRResp != null", DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "创建失败，请联系管理员" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax创建订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string GetBookingDetail(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string AgentReferenceNumber, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string strXml = string.Format(@"<RetrievePNRReq> <Username>{0}</Username> <Password>{1}</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>{2}</PNRNo> </RetrievePNRReq>"
                , travPaxUsername, travPaxPassword, BookingCode);
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    result = Common.Common.PostHttp(url, postBoby);
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax查询订单接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCreatePNRResp.PNRResp obj = Common.Common.DESerializer<TravPaxCreatePNRResp.PNRResp>(result);
                if (obj != null)
                {
                    if (obj.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = obj.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , Time:{1} ", obj.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    for (int i = 0; i < obj.Services.Service.Count; i++)
                    {
                        string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Leg_Status='{1}' where BookingCode='{2}' and ServiceID={3}  and Platform='TravPax' ", obj.PNRStatus, obj.Services.Service[i].Leg_Status, BookingCode, obj.Services.Service[i].ID);
                        con.Update(strUpdateSql);
                    }
                    resp = new Respone() { code = "00", result = obj.Services.Service[0].Leg_Status };
                }
                else
                {
                    LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单单接口 ,err:{0} , Time:{1} ", "pNRResp != null", DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "查询失败，请联系管理员" };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax查询订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string BookingCancel(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string AgentReferenceNumber, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            try
            {
                string strXml=string.Format(@"<CancelPNRReq> <Username>{0}</Username> <Password>{1}</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>{2}</PNRNo> <ConfirmCode>{3}</ConfirmCode> </CancelPNRReq>"
                , travPaxUsername, travPaxPassword, BookingCode, AgentReferenceNumber);
                //请求接口
                try
                {
                    string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(strXml), btnPost);
                    //发起请求
                    DateTime beforDT = System.DateTime.Now;
                    result = Common.Common.PostHttp(url, postBoby);
                    DateTime afterDT = System.DateTime.Now;
                    TimeSpan ts = afterDT.Subtract(beforDT);
                    string SpendTime = ts.Seconds.ToString();
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:CancelPNRReq, Req : {0} , Resq ：{1} ，SpendTime : {2} ， Time:{3} ", strXml, result, SpendTime, DateTime.Now.ToString()), "TravPax接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "TravPax接口");
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                TravPaxCancelPNRResp.CancelPNRResp obj = Common.Common.DESerializer<TravPaxCancelPNRResp.CancelPNRResp>(result);
                if (obj != null)
                {
                    if (obj.ErrorMessage != null)
                    {
                        resp = new Respone() { code = "99", mes = obj.ErrorMessage };
                        LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , Time:{1} ", obj.ErrorMessage, DateTime.Now.ToString()), "TravPax接口");
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    //修改数据库订单状态
                    for (int i = 0; i < obj.Services.Service.Count; i++)
                    {
                        string strUpdateSql = string.Format("update Booking set StatusCode='{0}',UpdateTime=GETDATE(),Leg_Status='{1}' where BookingCode='{2}' and ServiceID={3}  and Platform='TravPax'", obj.PNRStatus, obj.Services.Service[i].Leg_Status, BookingCode, obj.Services.Service[i].ID);
                        con.Update(strUpdateSql);
                    }
                    resp = new Respone() { code = "00", result = obj.Services.Service[0].Leg_Status };
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Studio:TravPax取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "转为类时为空", strXml, DateTime.Now.ToString()), "TravPax接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" + ex.ToString() };
                //日记
                LogHelper.DoOperateLog(string.Format("Err:TravPax取消订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "TravPax接口");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 返回结果
        /// </summary>
        public class Respone
        {
            public string code { get; set; }
            public string result { get; set; }
            public string mes { get; set; }
        }
    }
}