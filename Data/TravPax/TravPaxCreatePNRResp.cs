﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.TravPax
{
    public class TravPaxCreatePNRResp
    {
        public class PNRResp
        {
            [XmlElement(ElementName = "ReferenceClient")]
            public string ReferenceService { get; set; }
            [XmlElement(ElementName = "Version")]
            public string Version { get; set; }
            [XmlElement(ElementName = "Supplier")]
            public string Supplier { get; set; }
            [XmlElement(ElementName = "PNRNo")]
            public string PNRNo { get; set; }
            [XmlElement(ElementName = "PNRStatus")]
            public string PNRStatus { get; set; }
            [XmlElement(ElementName = "Services")]
            public Services Services { get; set; }
            [XmlElement(ElementName = "Leading")]
            public Leading Leading { get; set; }
            [XmlElement(ElementName = "TotalPrice")]
            public decimal TotalPrice { get; set; }
            [XmlElement(ElementName = "CreateDate")]
            public string CreateDate { get; set; }
            [XmlElement(ElementName = "Remark")]
            public string Remark { get; set; }
            [XmlElement(ElementName = "Refer")]
            public string Refer { get; set; }
            [XmlElement(ElementName = "AgentRefer")]
            public string AgentRefer { get; set; }
            [XmlElement(ElementName = "ErrorMessage")]
            public string ErrorMessage { get; set; }
        }
        public class Services
        {
            [XmlElement(ElementName = "Service")]
            public List<Service> Service { get; set; }
        }
        public class Service
        {
            [XmlElement(ElementName = "ID")]
            public int ID { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "LegID")]
            public int LegID { get; set; }
            [XmlElement(ElementName = "Leg_Status")]
            public string Leg_Status { get; set; }
            [XmlElement(ElementName = "Rooms")]
            public Rooms Rooms { get; set; }
            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }
            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
            [XmlElement(ElementName = "NumberOfNight")]
            public int NumberOfNight { get; set; }
            [XmlElement(ElementName = "TotalAdults")]
            public int TotalAdults { get; set; }
            [XmlElement(ElementName = "TotalChilds")]
            public int TotalChilds { get; set; }
            [XmlElement(ElementName = "TotalRooms")]
            public int TotalRooms { get; set; }
            [XmlElement(ElementName = "CountryID")]
            public string CountryID { get; set; }
            [XmlElement(ElementName = "CityID")]
            public int CityID { get; set; }
            [XmlElement(ElementName = "TotalFare")]
            public decimal TotalFare { get; set; }
            [XmlElement(ElementName = "FareCurrency")]
            public string FareCurrency { get; set; }
            [XmlElement(ElementName = "IsCanceled")]
            public bool IsCanceled { get; set; }
        }
        public class Rooms
        {
            [XmlElement(ElementName = "Room")]
            public List<Room> Room { get; set; }
        }
        public class Room
        {
            [XmlElement(ElementName = "RoomName")]
            public string RoomName { get; set; }
            [XmlElement(ElementName = "Supplier")]
            public string Supplier { get; set; }
            [XmlElement(ElementName = "BBCode")]
            public string BBCode { get; set; }
            [XmlElement(ElementName = "TotalPrice")]
            public decimal TotalPrice { get; set; }
            [XmlElement(ElementName = "CurrencyCode")]
            public string CurrencyCode { get; set; }
            [XmlElement(ElementName = "Adults")]
            public string Adults { get; set; }
            [XmlElement(ElementName = "Childs")]
            public string Childs { get; set; }
            [XmlElement(ElementName = "IsPromotion")]
            public string IsPromotion { get; set; }
            [XmlElement(ElementName = "IsOptional")]
            public string IsOptional { get; set; }
        }
        public class Leading
        {
            [XmlElement(ElementName = "Nationality")]
            public string Nationality { get; set; }
            [XmlElement(ElementName = "Title")]
            public string Title { get; set; }
            [XmlElement(ElementName = "FirstName")]
            public string FirstName { get; set; }
            [XmlElement(ElementName = "LastName")]
            public string LastName { get; set; }
        }

    }
}