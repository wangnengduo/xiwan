﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.TravPax
{
    public class TravPaxCancelPNRResp
    {
        public class CancelPNRResp
        {
            [XmlElement(ElementName = "ReferenceService")]
            public string ReferenceService { get; set; }
            [XmlElement(ElementName = "Version")]
            public string Version { get; set; }
            [XmlElement(ElementName = "CancelDate")]
            public string CancelDate { get; set; }
            [XmlElement(ElementName = "Supplier")]
            public string Supplier { get; set; }
            [XmlElement(ElementName = "PNRNo")]
            public string PNRNo { get; set; }
            [XmlElement(ElementName = "PNRStatus")]
            public string PNRStatus { get; set; }
            [XmlElement(ElementName = "Services")]
            public Services Services { get; set; }
            [XmlElement(ElementName = "ErrorMessage")]
            public string ErrorMessage { get; set; }
        }
        public class Services
        {
            [XmlElement(ElementName = "Service")]
            public List<Service> Service { get; set; }
        }
        public class Service
        {
            [XmlElement(ElementName = "ID")]
            public int ID { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "LegID")]
            public int LegID { get; set; }
            [XmlElement(ElementName = "Leg_Status")]
            public string Leg_Status { get; set; }
            [XmlElement(ElementName = "Rooms")]
            public Rooms Rooms { get; set; }
            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }
            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
            [XmlElement(ElementName = "NumberOfNight")]
            public string NumberOfNight { get; set; }
            [XmlElement(ElementName = "TotalAdults")]
            public string TotalAdults { get; set; }
            [XmlElement(ElementName = "TotalChilds")]
            public string TotalChilds { get; set; }
            [XmlElement(ElementName = "TotalRooms")]
            public string TotalRooms { get; set; }
            [XmlElement(ElementName = "CityID")]
            public int CityID { get; set; }
            [XmlElement(ElementName = "TotalFare")]
            public string TotalFare { get; set; }
            [XmlElement(ElementName = "FareCurrency")]
            public string FareCurrency { get; set; }
            [XmlElement(ElementName = "IsCanceled")]
            public string IsCanceled { get; set; }
        }
        public class Rooms
        {
            [XmlElement(ElementName = "Room")]
            public List<Room> Room { get; set; }
        }
        public class Room
        {
            [XmlElement(ElementName = "RoomName")]
            public string RoomName { get; set; }
            [XmlElement(ElementName = "BBCode")]
            public string BBCode { get; set; }
            [XmlElement(ElementName = "Adults")]
            public string Adults { get; set; }
            [XmlElement(ElementName = "Childs")]
            public string Childs { get; set; }
            [XmlElement(ElementName = "IsPromotion")]
            public string IsPromotion { get; set; }
            [XmlElement(ElementName = "IsOptional")]
            public string IsOptional { get; set; }
        }
    }
}