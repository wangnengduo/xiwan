﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml;
using XiWan.DALFactory;
using System.Data;
using System.Text;

namespace XiWan.Data
{
    public class TravPaxData
    {
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        public DataTable DisposeData(string reXml)
        {
            DataTable dt=new DataTable();
            var doc = new XmlDocument();
            doc.LoadXml(reXml); ;
            if (doc.SelectNodes("CitySearchResp").Count > 0)
            {
                dt = HotelData(doc);
            }
            else if (doc.SelectNodes("HotelSearchResp").Count > 0)
            {
                dt = HotelDetailData(doc);
            }
            return dt;
        }
        //查询城市酒店数据处理
        private DataTable HotelData(XmlDocument doc)
        {
            //CitySearch citySearch = new CitySearch();
            DataTable dt = new DataTable();
            //添加列
            dt.Columns.Add("HotelID", typeof(int));
            dt.Columns.Add("HotelName", typeof(string));
            dt.Columns.Add("Address", typeof(string));
            dt.Columns.Add("Status", typeof(string));
            dt.Columns.Add("StarRating", typeof(string));
            dt.Columns.Add("TotalPrice", typeof(string));
            dt.Columns.Add("TotalRoom", typeof(string));
            dt.Columns.Add("CountryID", typeof(string));
            dt.Columns.Add("CityID", typeof(string));
            dt.Columns.Add("ReferenceClient", typeof(string));
            dt.Columns.Add("CountryName", typeof(string));
            dt.Columns.Add("StarRatingCount", typeof(string));     
            
            #region
            string ReferenceClient = string.Empty;
            string ResponseTime = string.Empty;
            string ReferenceService = string.Empty;
            string Version = string.Empty;
            string MSG = string.Empty;
            string SessionID = string.Empty;  
            try 
            {
                ReferenceClient = doc.SelectSingleNode("CitySearchResp/ReferenceClient").InnerText;
            }
            catch 
            {
                ReferenceClient = string.Empty;
            }
            try
            {
                ResponseTime = doc.SelectSingleNode("CitySearchResp/ResponseTime").InnerText;
            }
            catch
            {
                ResponseTime = string.Empty;
            }
            try
            {
                ReferenceService = doc.SelectSingleNode("CitySearchResp/ReferenceService").InnerText;
            }
            catch
            {
                ReferenceService = string.Empty;
            }
            try
            {
                Version = doc.SelectSingleNode("CitySearchResp/Version").InnerText;
            }
            catch
            {
                Version = string.Empty;
            }
            try
            {
                MSG = doc.SelectSingleNode("CitySearchResp/MSG").InnerText;
            }
            catch
            {
                MSG = string.Empty;
            }
            try
            {
                SessionID = doc.SelectSingleNode("CitySearchResp/SessionID").InnerText;
            }
            catch
            {
                SessionID = string.Empty;
            }
            //IsClose = false;
            //CreateTime = System.DateTime.Now;
            //con.Save(citySearch);
            //int citySearchID = citySearch.ID;
            #endregion

            //var HotelList = doc.SelectNodes("/CitySearchResp/Hotels");
            var Hotels = doc.SelectNodes("CitySearchResp/Hotels/Hotel");
            if (Hotels != null && Hotels.Count>0)
            {
                foreach (XmlNode rowHotel in Hotels)
                {
                    #region
                    string HotelID = string.Empty;
                    string HotelName = string.Empty;
                    string CountryID = string.Empty;
                    string CountryName = string.Empty;
                    string CityID = string.Empty;
                    string CityName = string.Empty;
                    string StarRating = string.Empty;
                    string StarRatingCount = string.Empty;
                    string Address = string.Empty;
                    string Address2 = string.Empty;
                    string Fax = string.Empty;
                    string Email = string.Empty;   
                    try
                    {
                        HotelID = rowHotel.SelectSingleNode("HotelID").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        HotelName = rowHotel.SelectSingleNode("HotelName").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        CountryID = rowHotel.SelectSingleNode("CountryID").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        CountryName = rowHotel.SelectSingleNode("CountryName").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        CityID = rowHotel.SelectSingleNode("CityID").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        CityName = rowHotel.SelectSingleNode("CityName").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        StringBuilder sb = new StringBuilder();
                        string repayHtml = "<div>";
                        string strHtml = string.Empty;
                        StarRatingCount = rowHotel.SelectSingleNode("StarRating").InnerText;
                        int j = Convert.ToInt16(StarRatingCount);
                        for (int i = 1; i <= j; i++)
                        {
                            strHtml += "<svg width=\"14px\" height=\"14px\" viewBox=\"0 0 512 512\" xml:space=\"preserve\" style=\"margin:0 1px;\"><polygon fill=\"#FFEA00\" stroke=\"#C1AB60\" stroke-width=\"37.6152\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" points=\"259.216,29.942 330.27,173.919 489.16,197.007 374.185,309.08 401.33,467.31 259.216,392.612 117.104,467.31 144.25,309.08 29.274,197.007 188.165,173.919 \"></polygon></svg>";
                        }
                        sb.AppendLine(repayHtml + strHtml +"</div>");
                        //应收
                        //sb.AppendLine("<div><svg width=\"14px\" height=\"14px\" viewBox=\"0 0 512 512\" xml:space=\"preserve\" style=\"margin:0 1px;\"><polygon fill=\"#FFEA00\" stroke=\"#C1AB60\" stroke-width=\"37.6152\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" points=\"259.216,29.942 330.27,173.919 489.16,197.007 374.185,309.08 401.33,467.31 259.216,392.612 117.104,467.31 144.25,309.08 29.274,197.007 188.165,173.919 \"></polygon></svg></div>");

                        StarRating = sb.ToString();
                    }
                    catch
                    { }
                    try
                    {
                        Address = rowHotel.SelectSingleNode("Address").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        Address2 = rowHotel.SelectSingleNode("Address2").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        Fax = rowHotel.SelectSingleNode("Fax").InnerText;
                    }
                    catch
                    { }
                    try
                    {
                        Email = rowHotel.SelectSingleNode("Email").InnerText;
                    }
                    catch
                    { }
                    string CheckInDate = rowHotel.SelectSingleNode("CheckInDate").InnerText;
                    string CheckOutDate = rowHotel.SelectSingleNode("CheckOutDate").InnerText;
                    #endregion

                    var RoomClasses = rowHotel.SelectNodes("RoomClasses/RoomClass");
                    if (RoomClasses != null && RoomClasses.Count > 0)
                    {
                        foreach (XmlNode rowRoom in RoomClasses)
                        {
                            #region
                            string ClassName = string.Empty;
                            string Status = string.Empty;
                            string TotalPrice = string.Empty;
                            string CurrencyCode = string.Empty;
                            string TotalRoom = string.Empty;
                            string SupplierCode = string.Empty;
                            string Supplier = string.Empty;
                            string IsOptional = string.Empty;
                            string IsPromotion = string.Empty;
                            try
                            {
                                ClassName = rowRoom.SelectSingleNode("ClassName").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                Status = rowRoom.SelectSingleNode("Status").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                TotalPrice = rowRoom.SelectSingleNode("TotalPrice").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                CurrencyCode = rowRoom.SelectSingleNode("CurrencyCode").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                TotalRoom = rowRoom.SelectSingleNode("TotalRoom").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                SupplierCode = rowRoom.SelectSingleNode("SupplierCode").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                Supplier = rowRoom.SelectSingleNode("Supplier").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                IsOptional = rowRoom.SelectSingleNode("IsOptional").InnerText;
                            }
                            catch
                            { }
                            try
                            {
                                IsPromotion = rowRoom.SelectSingleNode("IsPromotion").InnerText;
                            }
                            catch
                            { }
                            #endregion
                            
                            //添加行
                            dt.Rows.Add(HotelID, HotelName, Address, Status, StarRating, CurrencyCode + " " + TotalPrice, TotalRoom, CountryID, CityID, ReferenceClient, CountryName, StarRatingCount);
                        }
                    }
                }
            }
            return dt;
        }

        //酒店详情信息处理
        private DataTable HotelDetailData(XmlDocument doc)
        {
            DataTable dtHotelDetail = new DataTable();
            //添加列
            dtHotelDetail.Columns.Add("ClassName", typeof(string));
            dtHotelDetail.Columns.Add("Status", typeof(string));
            dtHotelDetail.Columns.Add("HotelID", typeof(int));
            dtHotelDetail.Columns.Add("RoomName", typeof(string));
            dtHotelDetail.Columns.Add("TotalPrice", typeof(string));
            dtHotelDetail.Columns.Add("CurrencyCode", typeof(string));
            dtHotelDetail.Columns.Add("TotalRoom", typeof(string));

            dtHotelDetail.Columns.Add("BBCode", typeof(string));
            dtHotelDetail.Columns.Add("RoomStatus", typeof(string));
            dtHotelDetail.Columns.Add("RoomTotalPriceDes", typeof(string));
            dtHotelDetail.Columns.Add("Adults", typeof(string));
            dtHotelDetail.Columns.Add("Childs", typeof(string));
            dtHotelDetail.Columns.Add("Penalty", typeof(string));

            dtHotelDetail.Columns.Add("RoomID", typeof(string));
            //dtHotelDetail.Columns.Add("Recognition", typeof(string)); 

            string ReferenceClient = string.Empty;
            string ReferenceService = string.Empty;
            string Supplier = string.Empty;
            string Version = string.Empty; 
            try { ReferenceClient = doc.SelectSingleNode("HotelSearchResp/ReferenceClient").InnerText; } catch { }
            try { ReferenceService = doc.SelectSingleNode("HotelSearchResp/ReferenceService").InnerText; } catch { }
            //try { Supplier = doc.SelectSingleNode("HotelSearchResp/Supplier").InnerText; } catch{ }
            try { Version = doc.SelectSingleNode("HotelSearchResp/Version").InnerText; } catch { }

            var Hotels = doc.SelectNodes("HotelSearchResp/Hotels/Hotel");
            if (Hotels != null && Hotels.Count > 0)
            {
                foreach (XmlNode rowHotel in Hotels)
                {
                    int HotelID = 0;
                    string HotelName = string.Empty;
                    string CountryID = string.Empty;
                    //string CountryName = rowHotel.SelectSingleNode("CountryName").InnerText;
                    int CityID = 0;
                    //string CityName = rowHotel.SelectSingleNode("CityName").InnerText;
                    int StarRating = 0;
                    string Address = string.Empty;
                    string Address2 = string.Empty;
                    string Fax = string.Empty;
                    string Email = string.Empty;
                    string CheckInDate = string.Empty;
                    string CheckOutDate = string.Empty;
                    try { HotelID = rowHotel.SelectSingleNode("HotelID").InnerText.AsTargetType<int>(0); } catch { }
                    try { HotelName = rowHotel.SelectSingleNode("HotelName").InnerText; } catch { }
                    try { CountryID = rowHotel.SelectSingleNode("CountryID").InnerText; } catch { }
                    try { CityID = rowHotel.SelectSingleNode("CityID").InnerText.AsTargetType<int>(0); } catch { }
                    try { StarRating = rowHotel.SelectSingleNode("StarRating").InnerText.AsTargetType<int>(0); } catch { }
                    try { Address = rowHotel.SelectSingleNode("Address").InnerText; } catch { }
                    try { Address2 = rowHotel.SelectSingleNode("Address2").InnerText; } catch { }
                    try { Fax = rowHotel.SelectSingleNode("Fax").InnerText; } catch { }
                    try { Email = rowHotel.SelectSingleNode("Email").InnerText; } catch { }
                    try { CheckInDate = rowHotel.SelectSingleNode("CheckInDate").InnerText; } catch { }
                    try { CheckOutDate = rowHotel.SelectSingleNode("CheckOutDate").InnerText; } catch { }

                    var RoomClasses = rowHotel.SelectNodes("RoomClasses/RoomClass");
                    if (RoomClasses != null && RoomClasses.Count > 0)
                    {
                        foreach (XmlNode roomClass in RoomClasses)
                        {
                            string ClassName = string.Empty;
                            string Status = string.Empty;
                            string TotalPrice = string.Empty;
                            string CurrencyCode = string.Empty;
                            string TotalRoom = string.Empty;
                            string SupplierCode = string.Empty;
                            string IsOptional = string.Empty;
                            string IsPromotion = string.Empty;

                            try { ClassName = roomClass.SelectSingleNode("ClassName").InnerText; }
                            catch { }
                            try { Status = roomClass.SelectSingleNode("Status").InnerText; }
                            catch { }
                            try { TotalPrice = roomClass.SelectSingleNode("TotalPrice").InnerText; }
                            catch { }
                            try { CurrencyCode = roomClass.SelectSingleNode("CurrencyCode").InnerText; }
                            catch { }
                            try { TotalRoom = roomClass.SelectSingleNode("TotalRoom").InnerText; }
                            catch { }
                            try { SupplierCode = roomClass.SelectSingleNode("SupplierCode").InnerText; }
                            catch { }
                            try { IsOptional = roomClass.SelectSingleNode("IsOptional").InnerText; }
                            catch { }
                            try { IsPromotion = roomClass.SelectSingleNode("IsPromotion").InnerText; }
                            catch { }


                            var Rooms = roomClass.SelectNodes("Rooms/Room");
                            
                            if (Rooms != null && Rooms.Count > 0)
                            {
                                string RoomName = string.Empty;
                                string RoomStatus = string.Empty;
                                string BBCode = string.Empty;
                                string RoomTotalPrice = string.Empty;
                                string RoomCurrencyCode = string.Empty;
                                string AgreementID = string.Empty;
                                string Recognition = string.Empty;
                                string RoomID = string.Empty;
                                string Adults = string.Empty;
                                string Childs = string.Empty;
                                string RoomSupplierCode = string.Empty;
                                string RoomSupplier = string.Empty;
                                string RoomIsPromotion = string.Empty;
                                string PromotionDescription = string.Empty;
                                string RoomIsOptional = string.Empty;
                                string Penalty_Date = string.Empty;
                                string Penalty_Type = string.Empty;
                                string Penalty_Val = string.Empty;
                                string RoomTotalPriceDes = string.Empty;
                                string Penalty = string.Empty;
                                string BCode = string.Empty;
                                foreach (XmlNode room in Rooms)
                                {                        
                                    try {
                                        RoomID += room.SelectSingleNode("RoomID").InnerText + ",";
                                    }
                                    catch { }
                                    try { RoomName += room.SelectSingleNode("RoomName").InnerText + "；"; }
                                    catch { }
                                    try { RoomStatus = room.SelectSingleNode("Status").InnerText ; }
                                    catch { }
                                    try { BBCode = room.SelectSingleNode("BBCode").InnerText; }
                                    catch { }
                                    try { RoomTotalPrice = room.SelectSingleNode("TotalPrice").InnerText; }
                                    catch { }
                                    try { RoomCurrencyCode = room.SelectSingleNode("CurrencyCode").InnerText; }
                                    catch { }
                                    try { AgreementID = room.SelectSingleNode("AgreementID").InnerText; }
                                    catch { }
                                    try { Adults += room.SelectSingleNode("Adults").InnerText + ","; }
                                    catch { }
                                    try { Childs += room.SelectSingleNode("Childs").InnerText + ","; }
                                    catch { }
                                    if(BBCode=="BB")
                                    {
                                            BCode += "包含早餐"+ "；";
                                    }
                                    else if (BBCode == "RO") {
                                        BCode += "仅客房"+ "；";
                                    }
                                    else {
                                        BCode +=BBCode + "；";
                                    }
                                    RoomTotalPriceDes +=RoomCurrencyCode +" " + RoomTotalPrice+ "；";


                                    var ChildsAge = room.SelectNodes("ChildsAge/Age");
                                    if (ChildsAge != null && ChildsAge.Count > 0)
                                    {
                                        foreach (XmlNode ages in ChildsAge)
                                        {
                                            int Age = 0;
                                            try { Age = ages.SelectSingleNode("Age").InnerText.AsTargetType<int>(0); }
                                            catch { }
                                        }
                                    }

                                    var SupplierRefs = room.SelectNodes("SupplierRef/SupplierRef");
                                    if (SupplierRefs != null && SupplierRefs.Count > 0)
                                    {
                                        foreach (XmlNode supplierRef in SupplierRefs)
                                        {
                                            int supplierRefID = 0;
                                            try { supplierRefID = supplierRef.SelectSingleNode("ID").InnerText.AsTargetType<int>(0); }
                                            catch { }
                                            string Value = string.Empty;
                                            try { Value = supplierRef.SelectSingleNode("Value").InnerText; }
                                            catch { }
                                        }
                                    }

                                    string strRoom = string.Format(@"<ProductID>0</ProductID> <Adults>2</Adults> <Childs>0</Childs> <ChildsAge/> <MapRoomID>0</MapRoomID> <AgreementID>0</AgreementID> <SupplierCode>TRAVPAX</SupplierCode> <SupplierRef> <SupplierRef>");

                                    try { RoomSupplierCode = room.SelectSingleNode("SupplierCode").InnerText; }
                                    catch { }
                                    try { RoomSupplier = room.SelectSingleNode("Supplier").InnerText; }
                                    catch { }
                                    try { RoomIsPromotion = room.SelectSingleNode("IsPromotion").InnerText; }
                                    catch { }
                                    try { PromotionDescription = room.SelectSingleNode("PromotionDescription").InnerText; }
                                    catch { }
                                    try { RoomIsOptional = room.SelectSingleNode("IsOptional").InnerText; }
                                    catch { }
                                    try { Penalty_Date = room.SelectSingleNode("Penalty_Date").InnerText; }
                                    catch { }
                                    try { Penalty_Type = room.SelectSingleNode("Penalty_Type").InnerText; }
                                    catch { }
                                    try { Penalty_Val = room.SelectSingleNode("Penalty_Val").InnerText; }
                                    catch { }

                                    if (Penalty_Type == "2")
                                    {
                                        if (Penalty_Val == "0")
                                            Penalty += "免费取消" + "；"; 
                                        else
                                            Penalty += "将从" + Convert.ToDateTime(Penalty_Date).ToString("yyyy-MM-dd") + "起,取消罚款：" + RoomCurrencyCode + " " + Penalty_Val + "；";
                                    }
                                    else if (Penalty_Val == "1")
                                    {
                                        Penalty += "将从" + Convert.ToDateTime(Penalty_Date).ToString("yyyy-MM-dd") + "起,取消罚款：百分比" + Penalty_Val + "；"; ;
                                    }
                                    else if (Penalty_Val == "3")
                                    {
                                        Penalty += "一晚上房间费" + "；" ;
                                    }
                                    else
                                    {
                                        Penalty += "" + "；" ;
                                    }

                                    
                                    var DailyRates = room.SelectNodes("RoomPriceBreak/DailyRate");
                                    if (DailyRates != null && DailyRates.Count > 0)
                                    {
                                        foreach (XmlNode DailyRate in DailyRates)
                                        {
                                            int DailyRateDateOfRate = 0;
                                            string DailyRateCurrencyCode = string.Empty;
                                            string DailyRateDailyRate = string.Empty;
                                            try { DailyRateDateOfRate = DailyRate.SelectSingleNode("DateOfRate").InnerText.AsTargetType<int>(0); }
                                            catch { }
                                            try { DailyRateCurrencyCode = DailyRate.SelectSingleNode("CurrencyCode").InnerText; }
                                            catch { }
                                            try { DailyRateDailyRate = DailyRate.SelectSingleNode("DailyRate").InnerText; }
                                            catch { }
                                        }
                                    }
                                }


                                dtHotelDetail.Rows.Add(ClassName, Status, HotelID, RoomName.TrimEnd('；').Replace("；", "<hr size=\"0.1\" width=\"100%\" color=\"#ccc\" noshade=\"noshade\" />"), TotalPrice, CurrencyCode, TotalRoom, BCode.TrimEnd('；').Replace("；", "<hr size=\"0.1\" width=\"100%\" color=\"#ccc\" noshade=\"noshade\" />"), RoomStatus, RoomTotalPriceDes.TrimEnd('；').Replace("；", "<hr size=\"0.1\" width=\"100%\" color=\"#ccc\" noshade=\"noshade\" />"), Adults.TrimEnd(','), Childs.TrimEnd(','), Penalty.TrimEnd('；').Replace("；", "<hr size=\"0.1\" width=\"100%\" color=\"#ccc\" noshade=\"noshade\" />"), RoomID.TrimEnd(','));
                            }
                            var HotelOptionals = roomClass.SelectNodes("HotelOptionals/HotelOptional");

                            if (HotelOptionals != null && HotelOptionals.Count > 0)
                            {
                                string ServiceID = string.Empty;
                                string ProductID = string.Empty;
                                string ProductName = string.Empty;
                                string HotelOptionalSupplierCode = string.Empty;
                                string OptionalType = string.Empty;
                                string MarketID = string.Empty;
                                string HotelOptionalCurrencyCode = string.Empty;
                                string HotelOptionalTotalPrice = string.Empty;
                                string PriceFor = string.Empty;
                                string ConditionFor = string.Empty;
                                string Rate = string.Empty;
                                string HotelOptionalCheckInDate = string.Empty;
                                string HotelOptionalCheckOutDate = string.Empty;
                                string HotelOptionalCityID = string.Empty;
                                string HotelOptionalAdults = string.Empty;
                                string HotelOptionalChilds = string.Empty;
                                string Qty = string.Empty;
                                string HotelOptionalIsPromotion = string.Empty;
                                foreach (XmlNode hotelOptional in HotelOptionals)
                                {
                                    try { ServiceID = hotelOptional.SelectSingleNode("ServiceID").InnerText;}
                                    catch { }
                                    try { ProductID = hotelOptional.SelectSingleNode("ProductID").InnerText; }
                                    catch { }
                                    try { ProductName = hotelOptional.SelectSingleNode("ProductName").InnerText; }
                                    catch { }
                                    try { HotelOptionalSupplierCode = hotelOptional.SelectSingleNode("SupplierCode").InnerText; }
                                    catch { }
                                    try { OptionalType = hotelOptional.SelectSingleNode("OptionalType").InnerText; }
                                    catch { }
                                    try { MarketID = hotelOptional.SelectSingleNode("MarketID").InnerText; }
                                    catch { }
                                    try { HotelOptionalCurrencyCode = hotelOptional.SelectSingleNode("CurrencyCode").InnerText; }
                                    catch { }
                                    try { HotelOptionalTotalPrice += hotelOptional.SelectSingleNode("TotalPrice").InnerText ; }
                                    catch { }
                                    try { PriceFor = hotelOptional.SelectSingleNode("PriceFor").InnerText; }
                                    catch { }
                                    try { ConditionFor = hotelOptional.SelectSingleNode("ConditionFor").InnerText; }
                                    catch { }
                                    try { Rate += hotelOptional.SelectSingleNode("Rate").InnerText; }
                                    catch { }
                                    try { HotelOptionalCheckInDate = hotelOptional.SelectSingleNode("CheckInDate").InnerText; }
                                    catch { }
                                    try { HotelOptionalCheckOutDate = hotelOptional.SelectSingleNode("CheckOutDate").InnerText; }
                                    catch { }
                                    try { HotelOptionalCityID = hotelOptional.SelectSingleNode("CityID").InnerText; }
                                    catch { }
                                    try { HotelOptionalAdults = hotelOptional.SelectSingleNode("Adults").InnerText; }
                                    catch { }
                                    try { HotelOptionalChilds = hotelOptional.SelectSingleNode("Childs").InnerText; }
                                    catch { }
                                    try { Qty = hotelOptional.SelectSingleNode("Qty").InnerText; }
                                    catch { }
                                    try { HotelOptionalIsPromotion = hotelOptional.SelectSingleNode("IsPromotion").InnerText; }
                                    catch { }
                                }
                            }
                        }
                    }
                }
            }
            return dtHotelDetail;
        }
    }
}