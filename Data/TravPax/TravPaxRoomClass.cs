﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    public class TravPaxRoomClass
    {
        public class RoomClass
        {
            public string ClassName { get; set; }
            public List<RoomsItem> Rooms { get; set; }
            public string Status { get; set; }
            public string TotalPrice { get; set; }
            public string CurrencyCode { get; set; }
            public string TotalRoom { get; set; }
            public string SupplierCode { get; set; }
            public string IsOptional { get; set; }
            public string IsPromotion { get; set; }
        }
        public class RoomsItem
        {
            public List<RoomItem> Room { get; set; }
        }
        public class RoomItem
        {
            public int RoomID { get; set; }
            public string RoomName { get; set; }
            public string Status { get; set; }
            public string BBCode { get; set; }
            public string TotalPrice { get; set; }
            public string CurrencyCode { get; set; }
            public string AgreementID { get; set; }
            public int Adults { get; set; }
            public int Childs { get; set; }
            public string ChildsAge { get; set; }
        }
        public class ChildsAgeItem
        {
            public string Age { get; set; }
        }
    }
}