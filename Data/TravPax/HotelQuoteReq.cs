﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.TravPax
{
    public class HotelQuoteReq
    {
        public class HotelSearchReq
        {
            public string Username { get; set; }
            public string Password { get; set; }
            public string ReferenceService { get; set; }
            public string CheckInDate { get; set; }
            public string CheckOutDate { get; set; }
            public string CountryID { get; set; }
            public int CityID { get; set; }
            public int HotelID { get; set; }
            public int NationalityID { get; set; }
            public string ReferenceClient { get; set; }

            public List<RoomRequest> Rooms { get; set; }
        }
        public class RoomRequest
        {
            public int Adults { get; set; }
            public int Childs { get; set; }
            public string ChildsAge { get; set; }
        }
    }
}