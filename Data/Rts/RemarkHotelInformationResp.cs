﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Rts
{
    public class RemarkHotelInformationResp
    {
        public class GetRemarkHotelInformationResponse
        {
            [XmlElement(ElementName = "GetRemarkHotelInformationResult")]
            public GetRemarkHotelInformationResult GetRemarkHotelInformationResult { get; set; }
        }
        public class GetRemarkHotelInformationResult
        {
            [XmlElement(ElementName = "Error")]
            public string Error { get; set; }
            [XmlElement(ElementName = "RemarkHotelInformation")]
            public RemarkHotelInformation RemarkHotelInformation { get; set; }
        }
        public class RemarkHotelInformation
        {
            [XmlElement(ElementName = "ItemCode")]
            public string ItemCode { get; set; }
            [XmlElement(ElementName = "ItemNo")]
            public string ItemNo { get; set; }
            [XmlElement(ElementName = "RoomTypeCode")]
            public string RoomTypeCode { get; set; }
            [XmlElement(ElementName = "Remarks")]
            public Remarks Remarks { get; set; }
        }
        public class Remarks
        {
            [XmlElement(ElementName = "Remark1")]
            public string Remark1 { get; set; }
            [XmlElement(ElementName = "Remark2")]
            public string Remark2 { get; set; }
        }
    }
}