﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.Rts
{
    public class Hotel
    {
        public class PriceInfo
        {
            public string ItemNo { get; set; }
            public string ItemCode { get; set; }
            public string SupplierCompCode { get; set; }
            public string RoomTypeCode { get; set; }
            public string RoomTypeName { get; set; }
            public string BreakfastTypeName { get; set; }
            public string AddBreakfastTypeName { get; set; }
            public string PriceComment { get; set; }
            public string FareRateType { get; set; }
            public string PriceStatus { get; set; }
            public string NetCurrencyCode { get; set; }
            public decimal SellerNetPrice { get; set; }
            public string LocalNetPrice { get; set; }
            public string SellerMarkupPrice { get; set; }
            public string RecommendClientPrice { get; set; }
            public string SellerClientPrice { get; set; }
            public string Comment { get; set; }
            public string DoubleBedYn { get; set; }
        }
        
    }
}