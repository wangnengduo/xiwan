﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using XiWan.DALFactory;
using System.Text.RegularExpressions;
using System.Collections;
using XiWan.DALFactory.Model;
using XiWan.Data.Redis;
using XiWan.Data.Rts;
using XiWan.Model.Entities;

namespace XiWan.Data
{
    public class RtsDAL
    {
        string USDtoRMBrate = CCommon.GetWebConfigValue("USDtoRMBrate");//美元转为人民币汇率
        string EURtoRMBrate = CCommon.GetWebConfigValue("EURtoRMBrate");//欧元转为人民币汇率
        string SGDtoRMBrate = CCommon.GetWebConfigValue("SGDtoRMBrate");//新加坡元转为人民币汇率
        string HKDtoRMBrate = CCommon.GetWebConfigValue("HKDtoRMBrate");//港币转为人民币汇率
        string RtsCacheTime = CCommon.GetWebConfigValue("RtsCacheTime");//缓存到数据库的有效期（分钟）
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        #region 初始化本类
        private static RtsDAL _Instance;
        public static RtsDAL Instance
        {
            get
            {
                return new RtsDAL();
            }
        }
        #endregion
        //国籍
        public DataTable GetRtsNationalityBySql()
        {
            string sql = "select DISTINCT(RTScode) as ID,nationality as NAME from RtsNationalityCode WITH(NOLOCK) ORDER BY nationality";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        //获取目的地
        public DataTable GetRtsDestinationBySql(string strName)
        {
            string sql = string.Format(@"SELECT DISTINCT(CountryEName +',' + CityEName) as name, CityCode as value FROM RtsLocationCode WITH(NOLOCK) where CountryEName + CityEName like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        //区域 
        public DataTable GetRtsLocationCodeBySql(string destination)
        {
            string sql = string.Format(@"select locationcode as CODE,locationname as NAME from RtsLocationCode WITH(NOLOCK) WHERE CityCode='{0}'", destination);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        //判断目的地是否数据库存在
        public DataTable GetRTScityCodeYnBySql(string destination)
        {
            string sql = string.Format(@"SELECT top 1 * FROM RtsLocationCode WITH(NOLOCK) where CityCode='{0}'", destination);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return dt;
        }
        /// <summary>
        /// 获取酒店信息
        /// </summary>
        /// <returns></returns>
        public string GetRTSHotelBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string where, out int totalCount)
        {
            DataTable dt = Common.SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, where, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        /// <summary>
        /// 获取报价
        /// </summary>
        /// <returns></returns>
        public DataTable GetRTSQuote(string Url, string SiteCode, string Password, string BeginTime, string EndTime, int staring, string LocationCode, string SalesCompCode, string ItemName, string ItemCode, string RoomTypeCode1, int Room1Count, int Room1ChildAge1, int Room1ChildAge2, string RoomTypeCode2, int Room2Count, int Room2ChildAge1, int Room2ChildAge2, string RoomTypeCode3, int Room3Count, int Room3ChildAge1, int Room3ChildAge2, string RoomTypeCode4, int Room4Count, int Room4ChildAge1, int Room4ChildAge2, string RoomTypeCode5, int Room5Count, int Room5ChildAge1, int Room5ChildAge2, int input_page, int input_row)
        {
            DataTable dt = new DataTable();
            try
            {
                string result = string.Empty;
                //读取缓存 存在取缓存信息，不存在折调用接口读并做缓存（Redis）
                //RedisHelper.FlushAll();
                //List<Hotel.PriceInfo> priceInfo = RedisHelper.GetMem<List<Hotel.PriceInfo>>(ItemCode + BeginTime + EndTime + RoomTypeCode1);//读取缓存
                //if (priceInfo == null)
                string sql = string.Format("SELECT * FROM dbo.RtsHotel");
                DataTable dtHp = ControllerFactory.GetController().GetDataTable(sql);
                if (dtHp.Rows.Count > 0)
                {
                    for (int t = 0; t <dtHp.Rows.Count; t++)
                    {
                        int Adults = 0;
                        string childXml = string.Empty;
                        string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetHotelSearchList xmlns='http://www.rts.co.kr/'><HotelSearchListNet><LanguageCode>CN</LanguageCode>
<TravelerNationality>{2}</TravelerNationality><CityCode>{3}</CityCode><CheckInDate>{4}</CheckInDate><CheckOutDate>{5}</CheckOutDate>
<StarRating>{6}</StarRating><LocationCode>{7}</LocationCode><SupplierCompCode>{8}</SupplierCompCode><AvailableHotelOnly>false</AvailableHotelOnly>
<RecommendHotelOnly>false</RecommendHotelOnly><ClientCurrencyCode>HKD</ClientCurrencyCode><ItemName>{9}</ItemName><SellerMarkup>*1</SellerMarkup>
<CompareYn>false</CompareYn><SortType></SortType><ItemCodeList><ItemCodeInfo><ItemCode>{10}</ItemCode><ItemNo>0</ItemNo></ItemCodeInfo></ItemCodeList><RoomsList>"
                            , SiteCode, Password, "CN", dtHp.Rows[t]["CityCode"], BeginTime, EndTime, 0, "", "", "", dtHp.Rows[t]["ItemCode"]);
                        if (RoomTypeCode1 != "" && Room1Count != 0)
                        {
                            childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                                , RoomTypeCode1, Room1Count, Room1ChildAge1, Room1ChildAge2);
                        }
                        if (RoomTypeCode2 != "" && Room2Count != 0)
                        {
                            childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                                , RoomTypeCode2, Room2Count, Room2ChildAge1, Room2ChildAge2);
                        }
                        if (RoomTypeCode3 != "" && Room3Count != 0)
                        {
                            childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                                , RoomTypeCode3, Room3Count, Room3ChildAge1, Room3ChildAge2);
                        }
                        if (RoomTypeCode4 != "" && Room4Count != 0)
                        {
                            childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                                , RoomTypeCode4, Room4Count, Room4ChildAge1, Room4ChildAge2);
                        }
                        if (RoomTypeCode5 != "" && Room5Count != 0)
                        {
                            childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                                , RoomTypeCode5, Room5Count, Room5ChildAge1, Room5ChildAge2);
                        }
                        string SeachHotelXml = strXml + childXml + "</RoomsList></HotelSearchListNet></GetHotelSearchList></soap:Body></soap:Envelope>";
                        try
                        {
                            //发起请求
                            result = Common.Common.WebReq(Url, SeachHotelXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                            //日记
                            //LogHelper.DoOperateLog(string.Format("Studio:RTS获取报价接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", SeachHotelXml, result, DateTime.Now.ToString()), "RTS接口");
                        }
                        catch (Exception ex)
                        {
                            //日记
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, SeachHotelXml, DateTime.Now.ToString()), "Err");
                        }
                        if (result.Contains("<soap:Body>"))
                        {
                            Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                            string[] bitBody = regexBody.Split(result);
                            if (bitBody.Length > 1)
                            {
                                result = bitBody[1];
                                Regex reBody = new Regex("</soap:Body>");
                                bitBody = reBody.Split(result);
                                result = bitBody[0];
                            }
                        }
                        HotelSearchResp.GetHotelSearchListResponse objResp = Common.Common.DESerializer<HotelSearchResp.GetHotelSearchListResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                        if (objResp != null)
                        {
                            if (objResp.GetHotelSearchListResult.GetHotelSearchListResponse.Error != null)
                            {
                                LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                                //return dt;
                            }
                            else
                            {
                                if (objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.HotelSearchList.HotelItemInfo != null)
                                {

                                    List<HotelSearchResp.PriceInfo> PriceInfos = objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.HotelSearchList.HotelItemInfo.PriceList.PriceInfo;
                                    //dt = CConvert.IListToDataTable<HotelSearchResp.PriceInfo>(PriceInfo);

                                    //缓存到数据库中
                                    HotelSearchResp.PriceInfo PriceInfo = PriceInfos.OrderBy(x => x.SellerNetPrice).FirstOrDefault();//最低价格
                                    HotelPriceCollection objCollection = new HotelPriceCollection();
                                    if (PriceInfo != null)
                                    {
                                        string ClientCurrencyCode = objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.ClientCurrencyCode;
                                        decimal ClientPrice = PriceInfo.SellerNetPrice.AsTargetType<decimal>(0);
                                        string toRMBrate = string.Empty;
                                        if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                                        {
                                            toRMBrate = USDtoRMBrate;
                                        }
                                        else if (ClientCurrencyCode == "EUR")
                                        {
                                            toRMBrate = EURtoRMBrate;
                                        }
                                        else if (ClientCurrencyCode == "SGD")
                                        {
                                            toRMBrate = SGDtoRMBrate;
                                        }
                                        else if (ClientCurrencyCode == "HKD")
                                        {
                                            toRMBrate = HKDtoRMBrate;
                                        }
                                        //售价
                                        decimal RMBprice = ClientPrice * toRMBrate.AsTargetType<decimal>(0);
                                        string guID = Common.Common.getGUID();//生成房型id
                                        string GuidRateplanId = Common.Common.GuidToLongID();//生成价格计划id
                                        int IsBreakfast = 1;
                                        //缓存到数据库中
                                        HotelPrice obj = new HotelPrice();
                                        obj.HotelID = dtHp.Rows[t]["ItemCode"].AsTargetType<string>("");
                                        obj.HotelNo = PriceInfo.ItemNo;
                                        obj.ClassName = PriceInfo.RoomTypeName;
                                        obj.Platform = "RTS(B2B)";
                                        obj.RMBprice = RMBprice;
                                        obj.Price = ClientPrice;
                                        obj.CurrencyCode = ClientCurrencyCode;
                                        obj.ReferenceClient = PriceInfo.RoomTypeCode.AsTargetType<string>("");//保存房型id   roomtypeid
                                        if (PriceInfo.BreakfastTypeName == "None" || PriceInfo.BreakfastTypeName == "blank")
                                        {
                                            IsBreakfast = 0;
                                        }
                                        obj.IsBreakfast = IsBreakfast;
                                        obj.BreakfastName = PriceInfo.BreakfastTypeName;
                                        //obj.BedTypeCode = RoomTypeCode;
                                        //obj.RoomCount = RoomCount;
                                        obj.Adults = Adults;
                                        //obj.Childs = children;
                                        //obj.ChildsAge = childAges;
                                        obj.CheckInDate = Convert.ToDateTime(BeginTime);
                                        obj.CheckOutDate = Convert.ToDateTime(EndTime);
                                        obj.CGuID = guID;
                                        //obj.RateplanId = GuidRateplanId;
                                        obj.IsClose = 1;
                                        obj.CreateTime = DateTime.Now;
                                        obj.UpdateTime = DateTime.Now;
                                        string PriceBreakdownDate = string.Empty;
                                        string PriceBreakdownPrice = string.Empty;
                                        for (int k = 0; k < PriceInfo.PriceBreakdown.Count; k++)
                                        {
                                            PriceBreakdownDate += PriceInfo.PriceBreakdown[k].Date.AsTargetType<string>("") + ",";
                                            PriceBreakdownPrice += PriceInfo.PriceBreakdown[k].Price.AsTargetType<string>("") + ",";
                                        }
                                        obj.PriceBreakdownDate = PriceBreakdownDate.TrimEnd(',');
                                        obj.PriceBreakdownPrice = PriceBreakdownPrice.TrimEnd(',');
                                        objCollection.Add(obj);
                                    }
                                    con.Save(objCollection);
                                }
                            }
                        }
                        else
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return dt;
        }
        /// <summary>
        /// 获取取消期间
        /// </summary>
        /// <returns></returns>
        public string GetRTSCancelDeadline(string Url, string SiteCode, string Password, string ItemCode, string ItemNo, string RoomTypeCode, string BeginTime, string EndTime, string Nationality, string BedTypeCode)
        {
            Respone resp = new Respone();
            //return Newtonsoft.Json.JsonConvert.SerializeObject(RtsData.GetRTSCancelDeadline(Url, SiteCode, Password, ItemCode, ItemNo, RoomTypeCode, BeginTime, EndTime, Nationality, BedTypeCode));
            string result = string.Empty;
            try
            {
                string childXml = string.Empty;
                string strBedTypeCode = string.Empty;
                string endXml = string.Format(@"</BedTypeCode><LanguageCode>CN</LanguageCode><TravelerNationality>{0}</TravelerNationality><BookingYn>Y</BookingYn><HotelRoomTypeId></HotelRoomTypeId>
    </GetCancelDeadline></GetCancelDeadline></soap:Body></soap:Envelope>", Nationality);
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
    <soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
    <soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
    </BaseInfo></soap:Header><soap:Body><GetCancelDeadline xmlns='http://www.rts.co.kr/'><GetCancelDeadline><ItemCode>{2}</ItemCode><ItemNo>{3}</ItemNo>
    <RoomTypeCode>{4}</RoomTypeCode><CheckInDate>{5}</CheckInDate><CheckOutDate>{6}</CheckOutDate><BedTypeCode>", SiteCode, Password, ItemCode, ItemNo, RoomTypeCode, BeginTime, EndTime);
                string[] BedTypeCodeList = BedTypeCode.TrimEnd(';').Split(';');
                for (int i = 0; i < BedTypeCodeList.Length; i++)
                {
                    string[] roomsList = BedTypeCodeList[i].Split(',');
                    strBedTypeCode = roomsList[0];
                    for (int j = 0; j < roomsList[3].AsTargetType<int>(1); j++)
                    {
                        string age = ",";
                        if (roomsList[1].AsTargetType<int>(0) != 0)
                        {
                            age += roomsList[1] + ",";
                        }
                        if (roomsList[2].AsTargetType<int>(0) != 0)
                        {
                            age += roomsList[2];
                        }
                        childXml += string.Format(@"<String>{0}{1}</String>", strBedTypeCode, age.TrimEnd(','));
                    }
                }
                string SeachHotelXml = strXml + childXml + endXml;
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, SeachHotelXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", SeachHotelXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    resp = new Respone() { code = "99", mes = ex.ToString() };
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, SeachHotelXml, DateTime.Now.ToString()), "Err");
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                CancelDeadlineResp.GetCancelDeadlineResponse objResp = Common.Common.DESerializer<CancelDeadlineResp.GetCancelDeadlineResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.GetCancelDeadlineResult.GetRecommendHotelList != null)
                    {
                        if (objResp.GetCancelDeadlineResult.GetRecommendHotelList.Error != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                            resp = new Respone() { code = "99", mes = objResp.GetCancelDeadlineResult.GetRecommendHotelList.Error };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                    }
                    else
                    {
                        string CancelDeadlineDate = objResp.GetCancelDeadlineResult.GetCancelDeadlineResponse.GetCancelDeadlineResult.CancelDeadlineDate;
                        string TypeCode = objResp.GetCancelDeadlineResult.GetCancelDeadlineResponse.GetCancelDeadlineResult.TypeCode;
                        resp = new Respone() { code = "00", mes = CancelDeadlineDate + ";" + TypeCode };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                LogHelper.DoOperateLog(string.Format("Err:获取取消期间 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 获取提示
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="SiteCode"></param>
        /// <param name="Password"></param>
        /// <param name="Nationality"></param>
        /// <param name="Destination"></param>
        /// <param name="BeginTime"></param>
        /// <param name="EndTime"></param>
        /// <param name="staring"></param>
        /// <param name="LocationCode"></param>
        /// <param name="SalesCompCode"></param>
        /// <param name="ItemCode"></param>
        /// <param name="ItemNo"></param>
        /// <param name="RoomTypeCode"></param>
        /// <param name="RoomTypeCode1"></param>
        /// <param name="Room1Count"></param>
        /// <param name="Room1ChildAge1"></param>
        /// <param name="Room1ChildAge2"></param>
        /// <param name="RoomTypeCode2"></param>
        /// <param name="Room2Count"></param>
        /// <param name="Room2ChildAge1"></param>
        /// <param name="Room2ChildAge2"></param>
        /// <param name="RoomTypeCode3"></param>
        /// <param name="Room3Count"></param>
        /// <param name="Room3ChildAge1"></param>
        /// <param name="Room3ChildAge2"></param>
        /// <param name="RoomTypeCode4"></param>
        /// <param name="Room4Count"></param>
        /// <param name="Room4ChildAge1"></param>
        /// <param name="Room4ChildAge2"></param>
        /// <param name="RoomTypeCode5"></param>
        /// <param name="Room5Count"></param>
        /// <param name="Room5ChildAge1"></param>
        /// <param name="Room5ChildAge2"></param>
        /// <returns></returns>
        public string GetRemarkHotelInformationForCustomerCount(string Url, string SiteCode, string Password, string Nationality, string Destination, string BeginTime, string EndTime, int staring, string LocationCode, string SalesCompCode, string ItemCode, string ItemNo, string RoomTypeCode, string RoomTypeCode1, int Room1Count, int Room1ChildAge1, int Room1ChildAge2, string RoomTypeCode2, int Room2Count, int Room2ChildAge1, int Room2ChildAge2, string RoomTypeCode3, int Room3Count, int Room3ChildAge1, int Room3ChildAge2, string RoomTypeCode4, int Room4Count, int Room4ChildAge1, int Room4ChildAge2, string RoomTypeCode5, int Room5Count, int Room5ChildAge1, int Room5ChildAge2)
        {
            Respone resp = new Respone();
            try
            {
                string result = string.Empty;
                string childXml = string.Empty;
                if (RoomTypeCode1 != "" && Room1Count != 0)
                {
                    childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                        , RoomTypeCode1, Room1Count, Room1ChildAge1, Room1ChildAge2);
                }
                if (RoomTypeCode2 != "" && Room2Count != 0)
                {
                    childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                        , RoomTypeCode2, Room2Count, Room2ChildAge1, Room2ChildAge2);
                }
                if (RoomTypeCode3 != "" && Room3Count != 0)
                {
                    childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                        , RoomTypeCode3, Room3Count, Room3ChildAge1, Room3ChildAge2);
                }
                if (RoomTypeCode4 != "" && Room4Count != 0)
                {
                    childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                        , RoomTypeCode4, Room4Count, Room4ChildAge1, Room4ChildAge2);
                }
                if (RoomTypeCode5 != "" && Room5Count != 0)
                {
                    childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                        , RoomTypeCode5, Room5Count, Room5ChildAge1, Room5ChildAge2);
                }
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetRemarkHotelInformation xmlns='http://www.rts.co.kr/'><HotelSearchListNet><LanguageCode>CN</LanguageCode>
<TravelerNationality>{2}</TravelerNationality><CityCode>{3}</CityCode><CheckInDate>{4}</CheckInDate><CheckOutDate>{5}</CheckOutDate>
<StarRating>{6}</StarRating><LocationCode>{7}</LocationCode><SupplierCompCode>{8}</SupplierCompCode><AvailableHotelOnly>false</AvailableHotelOnly>
<RecommendHotelOnly>false</RecommendHotelOnly><ClientCurrencyCode>HKD</ClientCurrencyCode><ItemName></ItemName><SellerMarkup>*1</SellerMarkup>
<CompareYn>false</CompareYn><SortType></SortType><ItemCodeList><ItemCodeInfo><ItemCode>{9}</ItemCode><ItemNo>{10}</ItemNo></ItemCodeInfo></ItemCodeList>
<RoomsList>{11}</RoomsList></HotelSearchListNet><RoomTypeCode>{12}</RoomTypeCode></GetRemarkHotelInformation></soap:Body></soap:Envelope>"
                    , SiteCode, Password, Nationality, Destination, BeginTime, EndTime, staring, LocationCode, "", ItemCode, ItemNo, childXml, RoomTypeCode);
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS获取提示接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取提示接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                RemarkHotelInformationResp.GetRemarkHotelInformationResponse objResp = Common.Common.DESerializer<RemarkHotelInformationResp.GetRemarkHotelInformationResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.GetRemarkHotelInformationResult.Error != null)
                    {
                        LogHelper.DoOperateLog(string.Format("Err:RTS获取提示接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                        resp = new Respone() { code = "99", mes = objResp.GetRemarkHotelInformationResult.Error };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        string Remark1 = objResp.GetRemarkHotelInformationResult.RemarkHotelInformation.Remarks.Remark1;
                        string Remark2 = objResp.GetRemarkHotelInformationResult.RemarkHotelInformation.Remarks.Remark2;
                        resp = new Respone() { code = "00", mes = Remark1 + ";" + Remark2 };
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取提示接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                LogHelper.DoOperateLog(string.Format("Err:RTS获取提示接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 创建订单
        /// </summary>
        public string CreateSystemBooking(string Url, string SiteCode, string Password, string Nationality, string SalesCompCode, string SalesSiteCode, string SalesUserNo, string ItemCode, string ItemNo, string BeginTime, string EndTime, string RoomTypeCode, string BreakfastTypeName)
        {
            Respone resp = new Respone();
            try
            {
                Booking booking = new Booking();
                string childXml = string.Empty;
                string guID = Common.Common.getGUID();
                string result = string.Empty;
                childXml = string.Format(@"<CustomerInfo><No>1</No><Name>honggildong</Name><LastName>hong</LastName><FirstName>gildong</FirstName>
<Gender>M</Gender><Age>0</Age><Country>CN</Country><Birthday></Birthday><JuminNo></JuminNo><LeadYn>false</LeadYn><PassportNo></PassportNo>
<PassportExpiry></PassportExpiry></CustomerInfo><CustomerInfo><No>2</No><Name>lisi</Name><LastName>si</LastName><FirstName>li</FirstName>
<Gender>M</Gender><Age>0</Age><Country>CN</Country><Birthday></Birthday><JuminNo></JuminNo><LeadYn>false</LeadYn><PassportNo></PassportNo>
<PassportExpiry></PassportExpiry></CustomerInfo>");
                string endXml = string.Format(@"
<RoomAndGuestInfo><RoomNo>1</RoomNo><BedTypeCode>BED01</BedTypeCode><GuestList><GuestInfo>
<GuestNo>1</GuestNo><AgeTypeCode /><ProductId /></GuestInfo></GuestList></RoomAndGuestInfo>
<RoomAndGuestInfo><RoomNo>2</RoomNo><BedTypeCode>BED01</BedTypeCode><GuestList><GuestInfo>
<GuestNo>2</GuestNo><AgeTypeCode /><ProductId /></GuestInfo></GuestList></RoomAndGuestInfo>");
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><CreateSystemBooking xmlns='http://www.rts.co.kr/'><SystemBookingInfoNet><LanguageCode>CN</LanguageCode>
<ClientCurrencyCode>HKD</ClientCurrencyCode><BookingCode></BookingCode><AdminCompCode>RTS32</AdminCompCode><GroupOrFit>F</GroupOrFit>
<NationalityCode>KR</NationalityCode><TravelerNationality>{2}</TravelerNationality><SalesCompCode>{3}</SalesCompCode><SalesSiteCode>{4}</SalesSiteCode>
<SalesUserNo>{5}</SalesUserNo><SalesUserId> </SalesUserId><SalesUserName></SalesUserName><SalesUserGender></SalesUserGender><SalesUserJuminNo></SalesUserJuminNo>
<SalesUserBirthday></SalesUserBirthday><SalesUserHandPhone></SalesUserHandPhone><SalesUserCompPhone></SalesUserCompPhone><SalesUserHomePhone></SalesUserHomePhone>
<SalesUserEamil></SalesUserEamil><SalesEmpNo>{6}</SalesEmpNo><SalesEmpName></SalesEmpName><SalesPayStatusCode></SalesPayStatusCode><NormalRemarks></NormalRemarks>
<SalesRemarks></SalesRemarks><AdminRemarks></AdminRemarks><AdminBlockYn>false</AdminBlockYn><BookingPathCode>PATH01</BookingPathCode><CardPaymentYn>false</CardPaymentYn>
<CardPaymentAmount></CardPaymentAmount><LastWriterUno>{6}</LastWriterUno><CustomerList>{7}</CustomerList><BookingItemList><BookingItemInfo>
<ItemTypeCode>{8}</ItemTypeCode><ItemCode>{9}</ItemCode><ItemNo>{10}</ItemNo><AgentBookingReference>{11}</AgentBookingReference>
<BookerTypeCode>Partner</BookerTypeCode><BookingPathCode>PATH01</BookingPathCode><AppliedFromDate>{12}</AppliedFromDate>
<AppliedToDate>{13}</AppliedToDate><RoomTypeCode>{14}</RoomTypeCode><FreeBreakfastTypeName>{15}</FreeBreakfastTypeName>
<AddBreakfastTypeName></AddBreakfastTypeName><VatSheetYn>false</VatSheetYn><RoundTripYn>false</RoundTripYn><RoomAndGuestList>
{16}</RoomAndGuestList><TicketTimeLimit></TicketTimeLimit><ReceiverZipcode></ReceiverZipcode>
<ReceiverAddress></ReceiverAddress><ReceiverName></ReceiverName></BookingItemInfo></BookingItemList><PriceBookingInfo><PriceBookingInfo><AddBookingCode></AddBookingCode>
<AddTableItemCnt></AddTableItemCnt><AddEurailBookingId></AddEurailBookingId><AddTotalPrice></AddTotalPrice><AddCardPaymentAmount></AddCardPaymentAmount>
<AddResaStatusCode></AddResaStatusCode><AddSupplierStatusCode></AddSupplierStatusCode></PriceBookingInfo></PriceBookingInfo><SellerMarkup>*1</SellerMarkup>
</SystemBookingInfoNet></CreateSystemBooking></soap:Body></soap:Envelope>",
      SiteCode, Password, Nationality, SalesCompCode, SalesSiteCode, SalesUserNo, SalesUserNo, childXml, "ITEM02", ItemCode,
    ItemNo, guID, BeginTime, EndTime, RoomTypeCode, BreakfastTypeName, endXml);
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS创建订单接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                CreateSystemBookingResp.CreateSystemBookingResponse objResp = Common.Common.DESerializer<CreateSystemBookingResp.CreateSystemBookingResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.CreateSystemBookingResult.CreateSystemBooking != null)
                    {
                        if (objResp.CreateSystemBookingResult.CreateSystemBooking.Error != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                            resp = new Respone() { code = "99", mes = objResp.CreateSystemBookingResult.CreateSystemBooking.Error };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        if (objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList != null)
                        {
                            string ItemStatusCode = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusCode;
                            string ItemStatusName = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusName;
                            string BookingCode = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingMaster.BookingCode;
                            string ClientPartnerAmount = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ClientPartnerAmount;
                            string ClientCurrencyCode = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ClientCurrencyCode;
                            string toRMBrate = string.Empty;
                            if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                            {
                                toRMBrate = USDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "EUR")
                            {
                                toRMBrate = EURtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "SGD")
                            {
                                toRMBrate = SGDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "HKD")
                            {
                                toRMBrate = HKDtoRMBrate;
                            }
                            //售价
                            decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                            //保存订单到数据库
                            booking.BookingCode = BookingCode;
                            booking.AgentBookingReference = guID;//创建自定义id
                            booking.RMBprice = price;
                            booking.ClientCurrencyCode = ClientCurrencyCode;
                            booking.ClientPartnerAmount = ClientPartnerAmount.AsTargetType<decimal>(0);
                            booking.HotelId = ItemCode;
                            booking.HotelNo = ItemNo;
                            booking.Platform = "Rts(B2B)";
                            booking.StatusCode = ItemStatusCode;
                            booking.StatusName = ItemStatusName;
                            booking.CreatTime = DateTime.Now;
                            booking.UpdateTime = DateTime.Now;
                            booking.EntityState = e_EntityState.Added;
                            con.Save(booking);
                            resp = new Respone() { code = "00", mes = ItemStatusName };
                        }
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 获取订单明细
        /// </summary>
        public string GetBookingDetail(string Url, string SiteCode, string Password, string BookingCode, string AgentReferenceNumber)
        {
            Respone resp = new Respone();
            //DataTable dt = new DataTable();
            try
            {
                string result = string.Empty;
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetBookingDetail xmlns='http://www.rts.co.kr/'><BookingDetail><LanguageCode>CN</LanguageCode>
<BookingCode>{2}</BookingCode></BookingDetail></GetBookingDetail></soap:Body></soap:Envelope>
", SiteCode, Password, BookingCode);
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS获取订单明细接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消失败" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                GetBookingDetailResp.GetBookingDetailResponse objResp = Common.Common.DESerializer<GetBookingDetailResp.GetBookingDetailResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.GetBookingDetailResult.GetBookingDetail != null)
                    {
                        if (objResp.GetBookingDetailResult.GetBookingDetail.Error != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                            resp = new Respone() { code = "99", mes = objResp.GetBookingDetailResult.GetBookingDetail.Error };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        if (objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList != null)
                        {
                            string ItemStatusCode = objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusCode;
                            string ItemStatusName = objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusName;
                            //修改数据库订单状态
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',StatusName='{1}',UpdateTime=GETDATE()  where BookingCode='{2}'  and Platform='Rts(B2B)' ", ItemStatusCode, ItemStatusName, BookingCode);
                            con.Update(strUpdateSql);
                            resp = new Respone() { code = "00", mes = ItemStatusName };
                        }
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <returns></returns>
        public string BookingCancel(string Url, string SiteCode, string Password, string BookingCode, string ItemNo, string SalesUserNo, string AgentReferenceNumber)
        {
            Respone resp = new Respone();
            try
            {
                string result = string.Empty;
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType></BaseInfo>
</soap:Header><soap:Body><BookingCancel xmlns='http://www.rts.co.kr/'><BookingCancel><LanguageCode>CN</LanguageCode><BookingCode>{2}</BookingCode>
<ItemNo>{3}</ItemNo><CancelReasonCode>CR01</CancelReasonCode><LastWriterUno>{4}</LastWriterUno></BookingCancel></BookingCancel></soap:Body></soap:Envelope>
", SiteCode, Password, BookingCode, 0, SalesUserNo);
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS取消订单接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消失败" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                BookingCancelResp.BookingCancelResponse objResp = Common.Common.DESerializer<BookingCancelResp.BookingCancelResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.BookingCancelResult.BookingCancel != null)
                    {
                        if (objResp.BookingCancelResult.BookingCancel.Error != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                            resp = new Respone() { code = "99", mes = objResp.BookingCancelResult.BookingCancel.Error };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        if (objResp.BookingCancelResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList != null)
                        {
                            string ItemStatusCode = objResp.BookingCancelResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusCode;
                            string ItemStatusName = objResp.BookingCancelResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusName;
                            //修改数据库订单状态
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',StatusName='{1}',UpdateTime=GETDATE()  where BookingCode='{2}'  and Platform='Rts(B2B)'", ItemStatusCode, ItemStatusName, BookingCode);
                            con.Update(strUpdateSql);
                            resp = new Respone() { code = "00", mes = ItemStatusName };
                        }
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 获取订单明细（通过自定义字段）
        /// </summary>
        public string GetAgentReferenceNumber(string Url, string SiteCode, string Password, string AgentReferenceNumber)
        {
            Respone resp = new Respone();
            try
            {
                DataTable dt = new DataTable();
                string result = string.Empty;
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetAgentReferenceNumber xmlns='http://www.rts.co.kr/'><GetAgentReference><LanguageCode>CN</LanguageCode>
<AgentReferenceNumber>{2}</AgentReferenceNumber></GetAgentReference></GetAgentReferenceNumber></soap:Body></soap:Envelope>
", SiteCode, Password, AgentReferenceNumber);
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS获取订单明细（通过自定义字段）,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细（通过自定义字段） ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消失败" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                GetAgentReferenceNumberResp.GetAgentReferenceNumberResponse objResp = Common.Common.DESerializer<GetAgentReferenceNumberResp.GetAgentReferenceNumberResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.GetAgentReferenceNumberResult.GetAgentReferenceNumber != null)
                    {
                        if (objResp.GetAgentReferenceNumberResult.GetAgentReferenceNumber.Error != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细（通过自定义字段） ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                            resp = new Respone() { code = "99", mes = objResp.GetAgentReferenceNumberResult.GetAgentReferenceNumber.Error };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                        if (objResp.GetAgentReferenceNumberResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList != null)
                        {
                            string ItemStatusCode = objResp.GetAgentReferenceNumberResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusCode;
                            string ItemStatusName = objResp.GetAgentReferenceNumberResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusName;
                            //修改数据库订单状态
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',StatusName='{1}',UpdateTime=GETDATE()  where AgentBookingReference='{2}'  and Platform='Rts(B2B)'", ItemStatusCode, ItemStatusName, AgentReferenceNumber);
                            con.Update(strUpdateSql);
                            resp = new Respone() { code = "00", mes = ItemStatusName };
                        }
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细（通过自定义字段）,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取失败，请联系管理员" };
                LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细（通过自定义字段） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public DataTable GetRTSbookingBySql()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select * from Booking WITH(NOLOCK) where Platform='Rts(B2B)'";
                dt = ControllerFactory.GetController().GetDataTable(sql);
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:RTS从数据库查询订单信息 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return dt;
        }
        
        
        /// <summary>
        /// 获取报价对外接口(提供外部调用接口)
        /// </summary>
        /// <returns></returns>
        public string GetRtsRoomTypeOutPrice(string Url, string SiteCode, string Password, string BeginTime, string EndTime, string ItemCode, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            HotelPriceCollection objCollection = new HotelPriceCollection();
            DataTable dt=new DataTable();
            try
            {
                if (RoomCount <= 5)
                {
                    int Adults = occupancy.adults;
                    int children = occupancy.children;
                    string childAges = occupancy.childAges ?? "";
                    string strRoomTpye = string.Empty;
                    string RoomTypeCode = string.Empty;
                    int ChildAge1 = 0;
                    int ChildAge2 = 0;
                    string bed = string.Empty;
                    if ((RoomCount > 1 && children > 0) || Adults > 4)
                    {
                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    string sql = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='RTS(B2B)' AND Adults={1} AND Childs={2}
and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}'  and RoomCount={6} AND IsClose=1 AND DATEDIFF( MINUTE, UpdateTime, GETDATE()) < {7} order by ID;
SELECT * FROM dbo.RtsHotel where ItemCode ='{0}';"
                , ItemCode, Adults, children,childAges,BeginTime , EndTime,RoomCount ,RtsCacheTime);
                    DataSet ds = con.GetDataSet(sql);

                    dt = ds.Tables[0];
                    DataTable dtHp = ds.Tables[1];
                    //DataTable dtHp = ControllerFactory.GetController().GetDataTable(sql);
                    //dt = ControllerFactory.GetController().GetDataTable(sql);
                    if ((dt.Rows.Count == 0 && dtHp.Rows.Count > 0) || (RateplanId != "" && dtHp.Rows.Count > 0))
                    {
                        if (children > 0)
                        {
                            if (childAges != "" || childAges != null)
                            {
                                string[] childAgesList = childAges.Split(',');
                                if (childAgesList.Length > 2)
                                {
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                                }
                                else if (childAgesList.Length == 2)
                                {
                                    ChildAge1 = childAgesList[0].AsTargetType<int>(0);
                                    ChildAge2 = childAgesList[1].AsTargetType<int>(0);
                                }
                                else if (childAgesList.Length == 1)
                                {
                                    ChildAge1 = childAgesList[0].AsTargetType<int>(0);
                                }
                            }
                            if (Adults == 1)
                            {
                                RoomTypeCode = "BED10";
                            }
                            else if (Adults == 2)
                            {
                                RoomTypeCode = "BED11";
                            }
                            else if (Adults == 3)
                            {
                                RoomTypeCode = "BED13";
                            }
                            else if (Adults == 4)
                            {
                                RoomTypeCode = "BED14";
                            }
                        }
                        else
                        {
                            if (Adults == 1)
                            {
                                RoomTypeCode = "BED01";
                            }
                            else if (Adults == 2)
                            {
                                RoomTypeCode = "BED02";
                            }
                            else if (Adults == 3)
                            {
                                RoomTypeCode = "BED04";
                            }
                            else if (Adults == 4)
                            {
                                RoomTypeCode = "BED05";
                            }
                        }
                        strRoomTpye = string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>",
                            RoomTypeCode, RoomCount, ChildAge1, ChildAge2);
                        string result = string.Empty;
                        string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
            <soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
            <soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
            </BaseInfo></soap:Header><soap:Body><GetHotelSearchList xmlns='http://www.rts.co.kr/'><HotelSearchListNet><LanguageCode>CN</LanguageCode>
            <TravelerNationality>{2}</TravelerNationality><CityCode>{3}</CityCode><CheckInDate>{4}</CheckInDate><CheckOutDate>{5}</CheckOutDate>
            <StarRating>{6}</StarRating><LocationCode>{7}</LocationCode><SupplierCompCode>{8}</SupplierCompCode><AvailableHotelOnly>false</AvailableHotelOnly>
            <RecommendHotelOnly>false</RecommendHotelOnly><ClientCurrencyCode>HKD</ClientCurrencyCode><ItemName>{9}</ItemName><SellerMarkup>*1</SellerMarkup>
            <CompareYn>false</CompareYn><SortType></SortType><ItemCodeList><ItemCodeInfo><ItemCode>{10}</ItemCode><ItemNo>0</ItemNo></ItemCodeInfo>
            </ItemCodeList><RoomsList>{11}</RoomsList></HotelSearchListNet></GetHotelSearchList></soap:Body></soap:Envelope>"
                    , SiteCode, Password, "CN", dtHp.Rows[0]["CityCode"], BeginTime, EndTime, 0, "", "", "", ItemCode, strRoomTpye);
                        try
                        {
                            //发起请求
                            result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                            //日记
                            LogHelper.DoOperateLog(string.Format("Studio:RTS获取报价接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                        }
                        catch (Exception ex)
                        {
                            //日记
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        if (result.Contains("<soap:Body>"))
                        {
                            Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                            string[] bitBody = regexBody.Split(result);
                            if (bitBody.Length > 1)
                            {
                                result = bitBody[1];
                                if (result.Contains("</soap:Body>"))
                                {
                                    Regex reBody = new Regex("</soap:Body>");
                                    bitBody = reBody.Split(result);
                                    result = bitBody[0];
                                }
                            }
                        }
                        else
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , Time:{1} ", "以<soap:Body>分割", DateTime.Now.ToString()), "RTS接口");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        HotelSearchResp.GetHotelSearchListResponse objResp = Common.Common.DESerializer<HotelSearchResp.GetHotelSearchListResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                        if (objResp == null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口,Err:{0} , Time:{1} ", "objResp为空", DateTime.Now.ToString()), "RTS接口");
                            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            if (objResp.GetHotelSearchListResult.GetHotelSearchListResponse.Error != null)
                            {
                                LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                            }
                            if (objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.HotelSearchList.HotelItemInfo==null)
                            {
                                LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , Time:{1} ", "无值", DateTime.Now.ToString()), "RTS接口");
                                return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                            }
                            else
                            {
                                List<HotelSearchResp.PriceInfo> PriceInfo;
                                //复查接口时调用，匹配数据是否有该房型
                                if (RateplanId != null && RateplanId != "")
                                {
                                    string sqlHotelPrice = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' and RateplanId='{1}' AND Platform='RTS(B2B)'", ItemCode, RateplanId);
                                    DataTable dtHotelPrice = ControllerFactory.GetController().GetDataTable(sqlHotelPrice);
                                    PriceInfo = objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.HotelSearchList.HotelItemInfo.PriceList.PriceInfo.Where(x => x.PriceStatus == "Available" && x.RoomTypeName == dtHotelPrice.Rows[0]["ClassName"].AsTargetType<string>("") && x.BreakfastTypeName == dtHotelPrice.Rows[0]["BreakfastName"].AsTargetType<string>("")).ToList();
                                    if (PriceInfo != null)
                                    {
                                        string ClientCurrencyCode = objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.ClientCurrencyCode;
                                        decimal ClientPrice = PriceInfo[0].SellerNetPrice.AsTargetType<decimal>(0);
                                        string toRMBrate = string.Empty;
                                        if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                                        {
                                            toRMBrate = USDtoRMBrate;
                                        }
                                        else if (ClientCurrencyCode == "EUR")
                                        {
                                            toRMBrate = EURtoRMBrate;
                                        }
                                        else if (ClientCurrencyCode == "SGD")
                                        {
                                            toRMBrate = SGDtoRMBrate;
                                        }
                                        else if (ClientCurrencyCode == "HKD")
                                        {
                                            toRMBrate = HKDtoRMBrate;
                                        }
                                        //售价
                                        decimal RMBprice = ClientPrice * toRMBrate.AsTargetType<decimal>(0);
                                        string strRoomTypeCode = PriceInfo[0].RoomTypeCode;
                                        string updateHotelPriceSql = string.Format(@"update HotelPrice set RMBprice='{0}',Price='{1}',CurrencyCode='{2}',
ReferenceClient='{3}',UpdateTime=GETDATE() where HotelID='{4}' AND Platform='RTS(B2B)' AND RateplanId='{5}'",
    RMBprice, ClientPrice, ClientCurrencyCode, strRoomTypeCode, ItemCode, RateplanId);
                                        con.Update(updateHotelPriceSql);
                                        string sqlDt = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='RTS(B2B)'  AND RateplanId='{1}'"
                    , ItemCode, RateplanId);
                                        dt = ControllerFactory.GetController().GetDataTable(sqlDt);
                                    }
                                    else
                                    {
                                        LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , Time:{1} ", "复查价格时无数据", DateTime.Now.ToString()), "RTS接口");
                                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                                    }
                                }
                                else
                                {
                                    PriceInfo = objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.HotelSearchList.HotelItemInfo.PriceList.PriceInfo.Where(x => x.PriceStatus == "Available").ToList();
                                    if (PriceInfo != null)
                                    {
                                        for (int i = 0; i < PriceInfo.Count; i++)
                                        {
                                            string ClientCurrencyCode = objResp.GetHotelSearchListResult.GetHotelSearchListResponse.GetHotelSearchListResult.ClientCurrencyCode;
                                            decimal ClientPrice = PriceInfo[i].SellerNetPrice.AsTargetType<decimal>(0);
                                            string toRMBrate = string.Empty;
                                            if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                                            {
                                                toRMBrate = USDtoRMBrate;
                                            }
                                            else if (ClientCurrencyCode == "EUR")
                                            {
                                                toRMBrate = EURtoRMBrate;
                                            }
                                            else if (ClientCurrencyCode == "SGD")
                                            {
                                                toRMBrate = SGDtoRMBrate;
                                            }
                                            else if (ClientCurrencyCode == "HKD")
                                            {
                                                toRMBrate = HKDtoRMBrate;
                                            }
                                            //售价
                                            decimal RMBprice = ClientPrice * toRMBrate.AsTargetType<decimal>(0);
                                            string guID = Common.Common.GuidToLongID();//房型id
                                            string GuidRateplanId = Common.Common.getGUID();//生成价格计划id
                                            int IsBreakfast = 1;
                                            //缓存到数据库中
                                            HotelPrice obj = new HotelPrice();
                                            obj.HotelID = ItemCode;
                                            obj.HotelNo = PriceInfo[i].ItemNo;
                                            obj.ClassName = PriceInfo[i].RoomTypeName;
                                            obj.Platform = "RTS(B2B)";
                                            obj.RMBprice = RMBprice;
                                            obj.Price = ClientPrice;
                                            obj.CurrencyCode = ClientCurrencyCode;
                                            obj.ReferenceClient = PriceInfo[i].RoomTypeCode.AsTargetType<string>("");//保存房型id   roomtypeid
                                            if (PriceInfo[i].BreakfastTypeName == "None" || PriceInfo[i].BreakfastTypeName == "blank")
                                            {
                                                IsBreakfast = 0;
                                            }
                                            obj.IsBreakfast = IsBreakfast;
                                            obj.BreakfastName = PriceInfo[i].BreakfastTypeName;
                                            obj.BedTypeCode = RoomTypeCode;
                                            obj.RoomCount = RoomCount;
                                            obj.Adults = Adults;
                                            obj.Childs = children;
                                            obj.ChildsAge = childAges;
                                            obj.CheckInDate = BeginTime.AsTargetType<DateTime>(DateTime.MinValue);
                                            obj.CheckOutDate = EndTime.AsTargetType<DateTime>(DateTime.MinValue);
                                            obj.CGuID = guID;
                                            obj.RateplanId = GuidRateplanId;
                                            obj.IsClose = 1;
                                            obj.CreateTime = DateTime.Now;
                                            obj.UpdateTime = DateTime.Now;
                                            string PriceBreakdownDate = string.Empty;
                                            string PriceBreakdownPrice = string.Empty;
                                            for (int k = 0; k < PriceInfo[i].PriceBreakdown.Count; k++)
                                            {
                                                PriceBreakdownDate += PriceInfo[i].PriceBreakdown[k].Date.AsTargetType<string>("") + ",";
                                                PriceBreakdownPrice += PriceInfo[i].PriceBreakdown[k].Price.AsTargetType<string>("") + ",";
                                            }
                                            obj.PriceBreakdownDate = PriceBreakdownDate.TrimEnd(',');
                                            obj.PriceBreakdownPrice = PriceBreakdownPrice.TrimEnd(',');
                                            objCollection.Add(obj);
                                        }
                                        string updateHotelPriceSql = string.Format(@"update HotelPrice set IsClose=0,UpdateTime=GETDATE() 
where HotelID='{0}' AND Platform='RTS(B2B)' AND Adults={1} AND Childs={2} and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}'  and RoomCount={6}",
    ItemCode, Adults, children, childAges, BeginTime, EndTime, RoomCount);
                                        con.Update(updateHotelPriceSql);
                                        con.Save(objCollection);
                                        string sqlDt = string.Format(@"select * from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='RTS(B2B)'  AND Adults={1} AND Childs={2}   and ChildsAge='{3}' AND CheckInDate='{4}' AND CheckOutDate='{5}'  and RoomCount={6} AND IsClose=1 order by ID"
                    , ItemCode, Adults, children, childAges, BeginTime, EndTime, RoomCount);
                                        dt = ControllerFactory.GetController().GetDataTable(sqlDt);
                                    }
                                    else
                                    {
                                        LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                                        return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                                    }
                                }
                            }
                        }
                    }
                    for(int j=0;j<dt.Rows.Count;j++)
                    {
                        RoomOutPrice.Room temp = new RoomOutPrice.Room();
                        temp.ProductSource = "RtsB2B"; //产品来源 
                        temp.MroomName = temp.XRoomName = temp.RoomName = dt.Rows[j]["ClassName"].AsTargetType<string>("");// 房型名称
                        temp.MroomId = temp.RoomTypeId = dt.Rows[j]["CGuID"].AsTargetType<string>("");//房型ID(由于长度要求，取自己生成后的guid，要是用到下单时转为)

                        //当前请求的房间人数
                        temp.Adults = Adults;
                        temp.Children = children;
                        temp.ChildAges = childAges.Replace(",", "|");
                        //最大入住人数
                        temp.MaxPerson = Adults;

                        temp.RatePlan = new List<RoomOutPrice.RatePlan>();
                        RoomOutPrice.RatePlan thisRatePlan = new RoomOutPrice.RatePlan();
                        string RatePlanName = "有早";
                        thisRatePlan.PayMent = "y";//支付方式。目前全部前台支付
                        int dtIsBreakfast = dt.Rows[j]["IsBreakfast"].AsTargetType<int>(0);
                        if (dtIsBreakfast == 0)
                        {
                            RatePlanName = "无早";
                        }
                        thisRatePlan.RatePlanName = RatePlanName;  //价格计划名称
                        thisRatePlan.RoomName = RatePlanName;
                        thisRatePlan.XRatePlanName = RatePlanName;
                        thisRatePlan.RatePlanId = dt.Rows[j]["RatePlanId"].AsTargetType<string>("");  //价格计划ID

                        thisRatePlan.RoomTypeId = dt.Rows[j]["CGuID"].AsTargetType<string>("");
                        temp.XRoomId = temp.RoomId = thisRatePlan.RoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>("")); //房型Id_价格计划ID
                        //temp.XRoomId = string.Format("{0}_{1}", dt.Rows[j]["CGuID"].AsTargetType<string>(""), dt.Rows[j]["HotelNo"].AsTargetType<string>(""));
                        //床型
                        common.HotelBedType hbt = new common.HotelBedType();
                        thisRatePlan.BedType = hbt.HotelBedTypeToName(dt.Rows[j]["ClassName"].AsTargetType<string>(""));

                        //售价
                        decimal price = dt.Rows[j]["RMBprice"].AsTargetType<decimal>(0);
                        //string[] ArrayPriceBreakdownDate = dt.Rows[j]["PriceBreakdownDate"].AsTargetType<string>("").Split(',');
                        //总的底价 
                        double sMoney = price.AsTargetType<double>(0);
                        bool available = false; // 指示入离日期所有天是否可订 有一天不可订为false
                        List<RoomOutPrice.Rate> rates = new List<RoomOutPrice.Rate>();
                        int days = (Convert.ToDateTime(EndTime) - Convert.ToDateTime(BeginTime)).Days;
                        for (int x = 0; x < days; x++)
                        {
                            RoomOutPrice.Rate rate = new RoomOutPrice.Rate();
                            rate.Date = Convert.ToDateTime(BeginTime).AddDays(x);
                            rate.Available = true;  //某天是否可订


                            rate.HotelID = (price / days).AsTargetType<string>("");  //底价

                            rate.MemberRate = (price / days);
                            rate.RetailRate = (price / days);
                            rates.Add(rate);
                        }


                        //补上没有的日期
                        if (days > rates.Count)
                        {
                            available = false;
                            List<RoomOutPrice.Rate> tempR = new List<RoomOutPrice.Rate>();
                            for (int y = 0; y < days; y++)
                            {
                                DateTime date = Convert.ToDateTime(BeginTime).AddDays(y).Date;
                                var one = rates.Find(c => c.Date.Date == date);
                                if (one != null)
                                {
                                    tempR.Add(one);
                                }
                                else
                                {
                                    RoomOutPrice.Rate newOne = new RoomOutPrice.Rate() { Available = false, InvStatusCode = "0", MemberRate = 0, Date = date };
                                    tempR.Add(newOne);
                                }
                            }
                            rates = tempR;
                        }

                        thisRatePlan.Rates = rates;
                        thisRatePlan.Available = available;
                        thisRatePlan.SMoney = Convert.ToDecimal(sMoney);

                        decimal avgprice = 0;//均价
                        if (rates.Count > 0)
                        {
                            avgprice = rates.Average(c => c.MemberRate);
                            thisRatePlan.AveragePrice = avgprice.ToString("f2");
                        }

                        temp.CurrentAlloment = thisRatePlan.CurrentAlloment = 15; //库存

                        //设置早餐数量
                        int breakfast = -1;
                        thisRatePlan.Breakfast = breakfast + "份早餐";
                        if (dtIsBreakfast == 0)
                        {
                            thisRatePlan.Breakfast = 0 + "份早餐";
                        }
                        else
                        {
                            thisRatePlan.Breakfast = Adults + "份早餐";
                        }
                        string zaocan = thisRatePlan.Breakfast.Replace("份早餐", "");
                        if (!zaocan.Equals("0"))
                        {
                            switch (zaocan)
                            {
                                case "1": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含单早)"; temp.RoomName = temp.RoomName + "(含单早)"; break;
                                case "2": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含双早)"; temp.RoomName = temp.RoomName + "(含双早)"; break;
                                case "3": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含三早)"; temp.RoomName = temp.RoomName + "(含三早)"; break;
                                case "4": thisRatePlan.RoomName = thisRatePlan.RoomName + "(含四早)"; temp.RoomName = temp.RoomName + "(含四早)"; break;
                            }
                        }
                        thisRatePlan.RoomName = thisRatePlan.RoomName + "[内宾]";

                        temp.RoomPrice = Convert.ToInt32(avgprice);
                        temp.BedType = thisRatePlan.BedType;
                        temp.RatePlan.Add(thisRatePlan);
                        rooms.Add(temp);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:RTS获取报价接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
        }

        /// <summary>
        /// 创建订单
        /// </summary>
        public string Create_Order(string Url, string SiteCode, string Password, string SalesCompCode, string SalesSiteCode, string SalesUserNo, string roomTypeId, string ratePlanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            Respone resp = new Respone();
            try
            {
                string sql = string.Format(@"SELECT top 1 * FROM dbo.HotelPrice WHERE CGuID='{0}' and RateplanId='{1}' and Platform='RTS(B2B)' ORDER BY UpdateTime desc", roomTypeId, ratePlanId);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                if (dt.Rows.Count > 0)
                {
                    //拿数据库值与传入值匹配
                    string dtCheckInDate = Convert.ToDateTime(dt.Rows[0]["CheckInDate"]).ToString("yyyy-MM-dd");
                    string dtCheckOutDate = Convert.ToDateTime(dt.Rows[0]["CheckOutDate"]).ToString("yyyy-MM-dd");
                    int dtRoomCount = dt.Rows[0]["RoomCount"].AsTargetType<int>(0);
                    if (dtCheckInDate == beginTime && dtCheckOutDate == endTime && dtRoomCount == roomNum)
                    {
                        string childXml = string.Empty;
                        string guID = Common.Common.getGUID();
                        string result = string.Empty;
                        string endXml = string.Empty;
                        int roomCount = dt.Rows[0]["RoomCount"].AsTargetType<int>(0);//房间数
                        int adults = dt.Rows[0]["Adults"].AsTargetType<int>(0);//1间房间成年数
                        int childs = dt.Rows[0]["Childs"].AsTargetType<int>(0);//1间房间儿童数
                        //当查询人数多于预订人数需补人数
                        /*if (childs == 0 && (adults * roomCount) > customers.Count)
                        {
                            for (int k = 0; k < (adults * roomCount) - customers.Count; k++)
                            {
                                if (k == 0)
                                {
                                    Customer customer = new Customer();
                                    customer.name = "San Zhang";
                                    customer.lastName = "Zhang";
                                    customer.firstName = "San";
                                    customer.age = 0;
                                    customers.Add(customer);
                                }
                                else if (k == 1)
                                {
                                    Customer customer = new Customer();
                                    customer.name = "Si Li";
                                    customer.lastName = "Li";
                                    customer.firstName = "Si";
                                    customer.age = 0;
                                    customers.Add(customer);
                                }
                                if (k == 2)
                                {
                                    Customer customer = new Customer();
                                    customer.name = "Wu Wang";
                                    customer.lastName = "Wang";
                                    customer.firstName = "Wu";
                                    customer.age = 0;
                                    customers.Add(customer);
                                }
                                if (k == 3)
                                {
                                    Customer customer = new Customer();
                                    customer.name = "Liu Chen";
                                    customer.lastName = "Chen";
                                    customer.firstName = "Liu";
                                    customer.age = 0;
                                    customers.Add(customer);
                                }
                            }
                        }*/
                        //入住人员
                        int p = 0;
                        for (int i = 0; i < customers.Count; i++)
                        {
                            string Name = string.Empty;
                            string LastName = string.Empty;
                            string FirstName = string.Empty;
                            int age = 0;
                            string Gender = "F";
                            for (int x = 0; x < customers[i].Customers.Count; x++)
                            {
                                p = p + 1;
                                Name = customers[i].Customers[x].name;
                                LastName = customers[i].Customers[x].lastName;
                                FirstName = customers[i].Customers[x].firstName;
                                age = customers[i].Customers[x].age;
                                if (age > 12)
                                {
                                    age = 0;
                                }
                                if (customers[i].Customers[x].sex == 1)
                                {
                                    Gender = "M";
                                }
                                childXml += string.Format(@"<CustomerInfo><No>{0}</No><Name>{1}</Name><LastName>{2}</LastName><FirstName>{3}</FirstName>
<Gender>{4}</Gender><Age>{5}</Age><Country>CN</Country><Birthday></Birthday><JuminNo></JuminNo><LeadYn>false</LeadYn><PassportNo></PassportNo>
<PassportExpiry></PassportExpiry></CustomerInfo>", p, Name, LastName, FirstName, Gender, age);
                            }
                            
                        }
                        //人员分配
                        int t = 0;
                        for (int j = 0; j < dt.Rows[0]["RoomCount"].AsTargetType<int>(0); j++)
                        {
                            string GuestList = string.Empty;

                            for (int k = 0; k < adults + childs; k++)
                            {
                                t = t + 1;
                                GuestList += string.Format(@"<GuestInfo><GuestNo>{0}</GuestNo><AgeTypeCode /><ProductId /></GuestInfo>", t);
                            }
                            endXml += string.Format(@"<RoomAndGuestInfo><RoomNo>{0}</RoomNo><BedTypeCode>{1}</BedTypeCode><GuestList>{2}</GuestList></RoomAndGuestInfo>", j + 1, dt.Rows[0]["BedTypeCode"], GuestList);
                        }
                        string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><CreateSystemBooking xmlns='http://www.rts.co.kr/'><SystemBookingInfoNet><LanguageCode>CN</LanguageCode>
<ClientCurrencyCode>HKD</ClientCurrencyCode><BookingCode></BookingCode><AdminCompCode>RTS32</AdminCompCode><GroupOrFit>F</GroupOrFit>
<NationalityCode>KR</NationalityCode><TravelerNationality>{2}</TravelerNationality><SalesCompCode>{3}</SalesCompCode><SalesSiteCode>{4}</SalesSiteCode>
<SalesUserNo>{5}</SalesUserNo><SalesUserId> </SalesUserId><SalesUserName></SalesUserName><SalesUserGender></SalesUserGender><SalesUserJuminNo></SalesUserJuminNo>
<SalesUserBirthday></SalesUserBirthday><SalesUserHandPhone></SalesUserHandPhone><SalesUserCompPhone></SalesUserCompPhone><SalesUserHomePhone></SalesUserHomePhone>
<SalesUserEamil></SalesUserEamil><SalesEmpNo>{6}</SalesEmpNo><SalesEmpName></SalesEmpName><SalesPayStatusCode></SalesPayStatusCode><NormalRemarks></NormalRemarks>
<SalesRemarks></SalesRemarks><AdminRemarks></AdminRemarks><AdminBlockYn>false</AdminBlockYn><BookingPathCode>PATH01</BookingPathCode><CardPaymentYn>false</CardPaymentYn>
<CardPaymentAmount></CardPaymentAmount><LastWriterUno>{6}</LastWriterUno><CustomerList>{7}</CustomerList><BookingItemList><BookingItemInfo>
<ItemTypeCode>{8}</ItemTypeCode><ItemCode>{9}</ItemCode><ItemNo>{10}</ItemNo><AgentBookingReference>{11}</AgentBookingReference>
<BookerTypeCode>Partner</BookerTypeCode><BookingPathCode>PATH01</BookingPathCode><AppliedFromDate>{12}</AppliedFromDate>
<AppliedToDate>{13}</AppliedToDate><RoomTypeCode>{14}</RoomTypeCode><FreeBreakfastTypeName>{15}</FreeBreakfastTypeName>
<AddBreakfastTypeName></AddBreakfastTypeName><VatSheetYn>false</VatSheetYn><RoundTripYn>false</RoundTripYn><RoomAndGuestList>
{16}</RoomAndGuestList><TicketTimeLimit></TicketTimeLimit><ReceiverZipcode></ReceiverZipcode>
<ReceiverAddress></ReceiverAddress><ReceiverName></ReceiverName></BookingItemInfo></BookingItemList><PriceBookingInfo><PriceBookingInfo><AddBookingCode></AddBookingCode>
<AddTableItemCnt></AddTableItemCnt><AddEurailBookingId></AddEurailBookingId><AddTotalPrice></AddTotalPrice><AddCardPaymentAmount></AddCardPaymentAmount>
<AddResaStatusCode></AddResaStatusCode><AddSupplierStatusCode></AddSupplierStatusCode></PriceBookingInfo></PriceBookingInfo><SellerMarkup>*1</SellerMarkup>
</SystemBookingInfoNet></CreateSystemBooking></soap:Body></soap:Envelope>",
              SiteCode, Password, "CN", SalesCompCode, SalesSiteCode, SalesUserNo, SalesUserNo, childXml, "ITEM02", dt.Rows[0]["HotelID"], dt.Rows[0]["HotelNo"],
            guID, Convert.ToDateTime(dt.Rows[0]["CheckInDate"]).ToString("yyyy-MM-dd"), Convert.ToDateTime(dt.Rows[0]["CheckOutDate"]).ToString("yyyy-MM-dd"),
            dt.Rows[0]["ReferenceClient"], dt.Rows[0]["BreakfastName"], endXml);
                        try
                        {
                            //发起请求
                            result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                            //日记
                            LogHelper.DoOperateLog(string.Format("Studio:RTS创建订单接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                        }
                        catch (Exception ex)
                        {
                            //日记
                            LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                            resp = new Respone() { code = "99", mes = "预订失败," + ex.ToString() };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        if (result.Contains("<soap:Body>"))
                        {
                            Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                            string[] bitBody = regexBody.Split(result);
                            if (bitBody.Length > 1)
                            {
                                result = bitBody[1];
                                Regex reBody = new Regex("</soap:Body>");
                                bitBody = reBody.Split(result);
                                result = bitBody[0];
                            }
                        }
                        CreateSystemBookingResp.CreateSystemBookingResponse objResp = Common.Common.DESerializer<CreateSystemBookingResp.CreateSystemBookingResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                        if (objResp != null)
                        {
                            if (objResp.CreateSystemBookingResult.CreateSystemBooking != null)
                            {
                                if (objResp.CreateSystemBookingResult.CreateSystemBooking.Error != null)
                                {
                                    LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                                    resp = new Respone() { code = "99", mes = "预订失败," + objResp.CreateSystemBookingResult.CreateSystemBooking.Error };
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                                }
                                else
                                {
                                    LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                                    resp = new Respone() { code = "99", mes = "预订失败" };
                                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                                }
                            }
                            else
                            {
                                resp = new Respone() { code = "99", mes = "预订失败" };
                                if (objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList != null)
                                {
                                    string ItemStatusCode = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusCode;
                                    string ItemStatusName = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusName;
                                    string BookingCode = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingMaster.BookingCode;
                                    string ClientPartnerAmount = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ClientPartnerAmount;
                                    string ClientCurrencyCode = objResp.CreateSystemBookingResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ClientCurrencyCode;
                                    string toRMBrate = string.Empty;
                                    if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                                    {
                                        toRMBrate = USDtoRMBrate;
                                    }
                                    else if (ClientCurrencyCode == "EUR")
                                    {
                                        toRMBrate = EURtoRMBrate;
                                    }
                                    else if (ClientCurrencyCode == "SGD")
                                    {
                                        toRMBrate = SGDtoRMBrate;
                                    }
                                    else if (ClientCurrencyCode == "HKD")
                                    {
                                        toRMBrate = HKDtoRMBrate;
                                    }
                                    //售价
                                    decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                                    Booking booking = new Booking();
                                    //保存订单到数据库
                                    booking.BookingCode = BookingCode;
                                    booking.InOrderNum = inOrderNum;
                                    booking.AgentBookingReference = guID;//创建自定义id
                                    booking.RMBprice = price;
                                    booking.ClientCurrencyCode = ClientCurrencyCode;
                                    booking.ClientPartnerAmount = ClientPartnerAmount.AsTargetType<decimal>(0);
                                    booking.RatePlanId = ratePlanId;
                                    booking.HotelId = dt.Rows[0]["HotelID"].ToString();
                                    booking.HotelNo = dt.Rows[0]["HotelNo"].ToString();
                                    booking.Platform = "Rts(B2B)";
                                    booking.StatusCode = ItemStatusCode;
                                    booking.StatusName = ItemStatusName;
                                    booking.CreatTime = DateTime.Now;
                                    booking.UpdateTime = DateTime.Now;
                                    booking.EntityState = e_EntityState.Added;
                                    con.Save(booking);
                                    if (ItemStatusCode == "BS02")
                                    {
                                        resp = new Respone() { code = "00", orderNum = BookingCode, orderTotal = price, mes = "预订成功" };
                                    }
                                    else
                                    {
                                        LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口,Err:{0} , Time:{1} ", "状态不正确" + ItemStatusCode, DateTime.Now.ToString()), "RTS接口");
                                        resp = new Respone() { code = "99", mes = "预订失败" };
                                    }
                                }
                            }
                        }
                        else
                        {
                            resp = new Respone() { code = "99", mes = "预订失败" };
                            LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                        }
                    }
                    else
                    {
                        LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口,Err:{0} , Time:{1} ", "订单与搜索参数值不匹配", DateTime.Now.ToString()), "RTS接口");
                        resp = new Respone() { code = "99", mes = "预订失败,订单与搜索参数值不匹配" };
                        return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "预订失败" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err:RTS创建订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 取消订单
        /// </summary>
        /// <returns></returns>
        public string Cancel_Order(string Url, string SiteCode, string Password, string BookingCode, string SalesUserNo)
        {
            Respone resp = new Respone();
            try
            {
                string result = string.Empty;
                string sql = string.Format(@"SELECT top 1 * FROM dbo.Booking WHERE BookingCode='{0}' and Platform='Rts(B2B)'", BookingCode);
                DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType></BaseInfo>
</soap:Header><soap:Body><BookingCancel xmlns='http://www.rts.co.kr/'><BookingCancel><LanguageCode>CN</LanguageCode><BookingCode>{2}</BookingCode>
<ItemNo>{3}</ItemNo><CancelReasonCode>CR01</CancelReasonCode><LastWriterUno>{4}</LastWriterUno></BookingCancel></BookingCancel></soap:Body></soap:Envelope>
", SiteCode, Password, BookingCode, 0, SalesUserNo);
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS取消订单接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "取消订单失败" + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                BookingCancelResp.BookingCancelResponse objResp = Common.Common.DESerializer<BookingCancelResp.BookingCancelResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.BookingCancelResult.BookingCancel != null)
                    {
                        if (objResp.BookingCancelResult.BookingCancel.Error != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                            resp = new Respone() { code = "99", mes = "取消订单失败," + objResp.BookingCancelResult.BookingCancel.Error };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败" };
                        if (objResp.BookingCancelResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList != null)
                        {
                            string ItemStatusCode = objResp.BookingCancelResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusCode;
                            string ItemStatusName = objResp.BookingCancelResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusName;
                            if (ItemStatusCode == "BS05")
                            {
                                //修改数据库订单状态
                                string strUpdateSql = string.Format("update Booking set StatusCode='{0}',StatusName='{1}',UpdateTime=GETDATE()  where BookingCode='{2}'  and Platform='Rts(B2B)'", ItemStatusCode, ItemStatusName, BookingCode);
                                con.Update(strUpdateSql);
                                resp = new Respone() { code = "00", mes = "取消成功" };
                            }
                            else
                            {
                                resp = new Respone() { code = "99", mes = "取消订单失败" };
                            }
                            
                        }
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "取消订单失败" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "取消订单失败"+ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err:RTS取消订单接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }

        /// <summary>
        /// 获取订单明细
        /// </summary>
        public string Get_OrderDetail(string Url, string SiteCode, string Password, string BookingCode)
        {
            Respone resp = new Respone();
            //DataTable dt = new DataTable();
            try
            {
                string result = string.Empty;
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetBookingDetail xmlns='http://www.rts.co.kr/'><BookingDetail><LanguageCode>CN</LanguageCode>
<BookingCode>{2}</BookingCode></BookingDetail></GetBookingDetail></soap:Body></soap:Envelope>
", SiteCode, Password, BookingCode);
                try
                {
                    //发起请求
                    result = Common.Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("RTS：获取订单明细 ,Req:{0} , Resp：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "Err");
                    resp = new Respone() { code = "99", mes = "获取订单明细失败," + ex.ToString() };
                    return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                }
                if (result.Contains("<soap:Body>"))
                {
                    Regex regexBody = new Regex("<soap:Body>");//以<soap:Body>分割
                    string[] bitBody = regexBody.Split(result);
                    if (bitBody.Length > 1)
                    {
                        result = bitBody[1];
                        Regex reBody = new Regex("</soap:Body>");
                        bitBody = reBody.Split(result);
                        result = bitBody[0];
                    }
                }
                GetBookingDetailResp.GetBookingDetailResponse objResp = Common.Common.DESerializer<GetBookingDetailResp.GetBookingDetailResponse>(result.Replace("&", "").Replace("xmlns=\"http://www.rts.co.kr/\"", ""));
                if (objResp != null)
                {
                    if (objResp.GetBookingDetailResult.GetBookingDetail != null)
                    {
                        if (objResp.GetBookingDetailResult.GetBookingDetail.Error != null)
                        {
                            LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口 ,err:{0} , Time:{1} ", "<Error>", DateTime.Now.ToString()), "RTS接口");
                            resp = new Respone() { code = "99", mes = "获取订单明细失败," + objResp.GetBookingDetailResult.GetBookingDetail.Error };
                            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                    }
                    else
                    {
                        resp = new Respone() { code = "99", mes = "获取订单明细失败" };
                        if (objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList != null)
                        {
                            string StatusName = string.Empty;
                            string ItemStatusCode = objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusCode;
                            string ItemStatusName = objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ItemStatusName;
                            //修改数据库订单状态
                            string strUpdateSql = string.Format("update Booking set StatusCode='{0}',StatusName='{1}',UpdateTime=GETDATE()  where BookingCode='{2}'  and Platform='Rts(B2B)' ", ItemStatusCode, ItemStatusName, BookingCode);
                            con.Update(strUpdateSql);
                            if (ItemStatusCode == "BS01")
                            {
                                StatusName = "待定";
                            }
                            else if (ItemStatusCode == "BS02")
                            {
                                StatusName = "预订成功";
                            }
                            else if (ItemStatusCode == "BS05")
                            {
                                StatusName = "已取消";
                            }
                            else if (ItemStatusCode == "BS06")
                            {
                                StatusName = "不可用";
                            }
                            else if (ItemStatusCode == "BS07" || ItemStatusCode == "BS08")
                            {
                                StatusName = "未发送";
                            }
                            else
                            {
                                StatusName = "未知状态";
                            }
                            string ClientPartnerAmount = objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ClientPartnerAmount;
                            string ClientCurrencyCode = objResp.GetBookingDetailResult.GetBookingDetailResponse.GetBookingDetailResult.BookingItemList.BookingItemInfo.ClientCurrencyCode;
                            string toRMBrate = string.Empty;
                            if (ClientCurrencyCode == "USD" || ClientCurrencyCode == "DO")
                            {
                                toRMBrate = USDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "EUR")
                            {
                                toRMBrate = EURtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "SGD")
                            {
                                toRMBrate = SGDtoRMBrate;
                            }
                            else if (ClientCurrencyCode == "HKD")
                            {
                                toRMBrate = HKDtoRMBrate;
                            }
                            //售价
                            decimal price = ClientPartnerAmount.AsTargetType<decimal>(0) * toRMBrate.AsTargetType<decimal>(0);
                            resp = new Respone() { code = "00", orderNum = BookingCode, orderTotal = price, mes = "获取订单明细成功" + "该订单状态为:" + StatusName };
                        }
                    }
                }
                else
                {
                    resp = new Respone() { code = "99", mes = "获取订单明细失败" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口,Err:{0} , Time:{1} ", "实体类为空", DateTime.Now.ToString()), "RTS接口");
                }
            }
            catch (Exception ex)
            {
                resp = new Respone() { code = "99", mes = "获取订单明细失败" + ex.ToString() };
                LogHelper.DoOperateLog(string.Format("Err:RTS获取订单明细接口 ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
    }
}