﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Rts
{
    public class CancelDeadlineResp
    {
        public class GetCancelDeadlineResponse
        {
            [XmlElement(ElementName = "GetCancelDeadlineResult")]
            public GetCancelDeadlineResult GetCancelDeadlineResult { get; set; }
        }
        public class GetCancelDeadlineResult
        {
            [XmlElement(ElementName = "GetRecommendHotelList")]
            public GetRecommendHotelList GetRecommendHotelList { get; set; }
            [XmlElement(ElementName = "GetCancelDeadlineResponse")]
            public GetCancelDeadlineResponse2 GetCancelDeadlineResponse { get; set; }
        }
        public class GetCancelDeadlineResponse2
        {
            [XmlElement(ElementName = "GetCancelDeadlineResult")]
            public GetCancelDeadlineResult2 GetCancelDeadlineResult { get; set; }
        }
        public class GetRecommendHotelList
        {
            [XmlElement(ElementName = "Error")]
            public string Error { get; set; }
        }
        public class GetCancelDeadlineResult2
        {
            [XmlElement(ElementName = "CancelDeadlineDate")]
            public string CancelDeadlineDate { get; set; }
            [XmlElement(ElementName = "TypeCode")]
            public string TypeCode { get; set; }
        }
    }
}