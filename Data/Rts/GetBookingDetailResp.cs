﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Rts
{
    public class GetBookingDetailResp
    {
        public class GetBookingDetailResponse
        {
            [XmlElement(ElementName = "GetBookingDetailResult")]
            public GetBookingDetailResult GetBookingDetailResult { get; set; }
        }
        public class GetBookingDetailResult
        {
            [XmlElement(ElementName = "GetBookingDetail")]
            public GetBookingDetail GetBookingDetail { get; set; }
            [XmlElement(ElementName = "GetBookingDetailResponse")]
            public GetBookingDetailResponse2 GetBookingDetailResponse { get; set; }
        }
        public class GetBookingDetail
        {
            [XmlElement(ElementName = "Error")]
            public string Error { get; set; }
        }
        public class GetBookingDetailResponse2
        {
            [XmlElement(ElementName = "GetBookingDetailResult")]
            public GetBookingDetailResult2 GetBookingDetailResult { get; set; }
        }
        public class GetBookingDetailResult2
        {
            [XmlElement(ElementName = "LanguageCode")]
            public string LanguageCode { get; set; }
            [XmlElement(ElementName = "LanguageName")]
            public string LanguageName { get; set; }
            [XmlElement(ElementName = "BookingMaster")]
            public BookingMaster BookingMaster { get; set; }
            [XmlElement(ElementName = "BookingItemList")]
            public BookingItemList BookingItemList { get; set; }
        }
        public class BookingMaster
        {
            [XmlElement(ElementName = "BookingCode")]
            public string BookingCode { get; set; }
            [XmlElement(ElementName = "ProductTypeCode")]
            public string ProductTypeCode { get; set; }
            [XmlElement(ElementName = "ProductTypeName")]
            public string ProductTypeName { get; set; }
            [XmlElement(ElementName = "GroupOrFit")]
            public string GroupOrFit { get; set; }
            [XmlElement(ElementName = "NationalityCode")]
            public string NationalityCode { get; set; }
            [XmlElement(ElementName = "NationalityName")]
            public string NationalityName { get; set; }
            [XmlElement(ElementName = "BookingName")]
            public string BookingName { get; set; }
            [XmlElement(ElementName = "SalesCompCode")]
            public string SalesCompCode { get; set; }
            [XmlElement(ElementName = "SalesCompName")]
            public string SalesCompName { get; set; }
            [XmlElement(ElementName = "SalesSiteCode")]
            public string SalesSiteCode { get; set; }
            [XmlElement(ElementName = "SalesSiteDomain")]
            public string SalesSiteDomain { get; set; }
            [XmlElement(ElementName = "SalesUserNo")]
            public string SalesUserNo { get; set; }
            [XmlElement(ElementName = "SalesUserId")]
            public string SalesUserId { get; set; }
            [XmlElement(ElementName = "SalesUserName")]
            public string SalesUserName { get; set; }
            [XmlElement(ElementName = "SalesUserGender")]
            public string SalesUserGender { get; set; }
            [XmlElement(ElementName = "SalesUserJuminNo")]
            public string SalesUserJuminNo { get; set; }
            [XmlElement(ElementName = "SalesUserBirthday")]
            public string SalesUserBirthday { get; set; }
            [XmlElement(ElementName = "SalesUserLastName")]
            public string SalesUserLastName { get; set; }
            [XmlElement(ElementName = "BookingItemList")]
            public string SalesUserFirstName { get; set; }
            [XmlElement(ElementName = "SalesUserHandPhone")]
            public string SalesUserHandPhone { get; set; }
            [XmlElement(ElementName = "SalesUserCompPhone")]
            public string SalesUserCompPhone { get; set; }
            [XmlElement(ElementName = "SalesUserHomePhone")]
            public string SalesUserHomePhone { get; set; }
            [XmlElement(ElementName = "SalesUserEmail")]
            public string SalesUserEmail { get; set; }
            [XmlElement(ElementName = "SalesEmpUno")]
            public string SalesEmpUno { get; set; }
            [XmlElement(ElementName = "SalesEmpId")]
            public string SalesEmpId { get; set; }
            [XmlElement(ElementName = "SalesEmpName")]
            public string SalesEmpName { get; set; }
            [XmlElement(ElementName = "SalesEmpPosition")]
            public string SalesEmpPosition { get; set; }
            [XmlElement(ElementName = "SalesEmpCompPhone")]
            public string SalesEmpCompPhone { get; set; }
            [XmlElement(ElementName = "SalesEmpEmail")]
            public string SalesEmpEmail { get; set; }
            [XmlElement(ElementName = "SalesmanUno")]
            public string SalesmanUno { get; set; }
            [XmlElement(ElementName = "SalesmanId")]
            public string SalesmanId { get; set; }
            [XmlElement(ElementName = "SalesmanName")]
            public string SalesmanName { get; set; }
            [XmlElement(ElementName = "SalesmanPosition")]
            public string SalesmanPosition { get; set; }
            [XmlElement(ElementName = "SalesmanHandPhone")]
            public string SalesmanHandPhone { get; set; }
            [XmlElement(ElementName = "SalesmanCompPhone")]
            public string SalesmanCompPhone { get; set; }
            [XmlElement(ElementName = "SalesmanEmail")]
            public string SalesmanEmail { get; set; }
            [XmlElement(ElementName = "OperatorUno")]
            public string OperatorUno { get; set; }
            [XmlElement(ElementName = "OperatorId")]
            public string OperatorId { get; set; }
            [XmlElement(ElementName = "OperatorName")]
            public string OperatorName { get; set; }
            [XmlElement(ElementName = "OperatorPosition")]
            public string OperatorPosition { get; set; }
            [XmlElement(ElementName = "OperatorCompPhone")]
            public string OperatorCompPhone { get; set; }
            [XmlElement(ElementName = "OperatorEmail")]
            public string OperatorEmail { get; set; }
            [XmlElement(ElementName = "BookingStatusCode")]
            public string BookingStatusCode { get; set; }
            [XmlElement(ElementName = "BookingStatusName")]
            public string BookingStatusName { get; set; }
            [XmlElement(ElementName = "TicketStatusCode")]
            public string TicketStatusCode { get; set; }
            [XmlElement(ElementName = "TicketStatusName")]
            public string TicketStatusName { get; set; }
            [XmlElement(ElementName = "SalesPayStatusCode")]
            public string SalesPayStatusCode { get; set; }
            [XmlElement(ElementName = "SalesPayStatusName")]
            public string SalesPayStatusName { get; set; }
            [XmlElement(ElementName = "InsidePayStatusCode")]
            public string InsidePayStatusCode { get; set; }
            [XmlElement(ElementName = "InsidePayStatusName")]
            public string InsidePayStatusName { get; set; }
            [XmlElement(ElementName = "FullCancelReasonCode")]
            public string FullCancelReasonCode { get; set; }
            [XmlElement(ElementName = "FullCancelReasonName")]
            public string FullCancelReasonName { get; set; }
            [XmlElement(ElementName = "AccountCloseYn")]
            public string AccountCloseYn { get; set; }
            [XmlElement(ElementName = "AccountCloseTime")]
            public string AccountCloseTime { get; set; }
            [XmlElement(ElementName = "NormalRemarks")]
            public string NormalRemarks { get; set; }
            [XmlElement(ElementName = "SalesRemarks")]
            public string SalesRemarks { get; set; }
            [XmlElement(ElementName = "BookingTime")]
            public string BookingTime { get; set; }
            [XmlElement(ElementName = "DepartureDate")]
            public string DepartureDate { get; set; }
            [XmlElement(ElementName = "DepartureWeekday")]
            public string DepartureWeekday { get; set; }
            [XmlElement(ElementName = "DepartureLeftDays")]
            public string DepartureLeftDays { get; set; }
            [XmlElement(ElementName = "CancelDeadLine")]
            public string CancelDeadLine { get; set; }
            [XmlElement(ElementName = "DeadLineWeekday")]
            public string DeadLineWeekday { get; set; }
            [XmlElement(ElementName = "DeadLineLeftDays")]
            public string DeadLineLeftDays { get; set; }
            [XmlElement(ElementName = "LastWriteTime")]
            public string LastWriteTime { get; set; }
            [XmlElement(ElementName = "CustomerList")]
            public CustomerList CustomerList { get; set; }
        }
        public class CustomerList
        {
            [XmlElement(ElementName = "CustomerInfo")]
            public List<CustomerInfo> CustomerInfo { get; set; }
        }
        public class CustomerInfo
        {
            [XmlElement(ElementName = "No")]
            public string No { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "LastName")]
            public string LastName { get; set; }
            [XmlElement(ElementName = "FirstName")]
            public string FirstName { get; set; }
            [XmlElement(ElementName = "Gender")]
            public string Gender { get; set; }
            [XmlElement(ElementName = "Age")]
            public string Age { get; set; }
            [XmlElement(ElementName = "Birthday")]
            public string Birthday { get; set; }
            [XmlElement(ElementName = "JuminNo")]
            public string JuminNo { get; set; }
            [XmlElement(ElementName = "PassportNo")]
            public string PassportNo { get; set; }
            [XmlElement(ElementName = "PassportExpiryDate")]
            public string PassportExpiryDate { get; set; }
        }
        public class BookingItemList
        {
            [XmlElement(ElementName = "BookingItemInfo")]
            public BookingItemInfo BookingItemInfo { get; set; }
        }
        public class BookingItemInfo
        {
            [XmlElement(ElementName = "ItemNo")]
            public string ItemNo { get; set; }
            [XmlElement(ElementName = "ItemTypeCode")]
            public string ItemTypeCode { get; set; }
            [XmlElement(ElementName = "NormalReceiveYn")]
            public string NormalReceiveYn { get; set; }
            [XmlElement(ElementName = "BookingReference")]
            public string BookingReference { get; set; }
            [XmlElement(ElementName = "ItemReference")]
            public string ItemReference { get; set; }
            [XmlElement(ElementName = "CountryCode")]
            public string CountryCode { get; set; }
            [XmlElement(ElementName = "CountryEname")]
            public string CountryEname { get; set; }
            [XmlElement(ElementName = "CountryName")]
            public string CountryName { get; set; }
            [XmlElement(ElementName = "CityCode")]
            public string CityCode { get; set; }
            [XmlElement(ElementName = "CityEname")]
            public string CityEname { get; set; }
            [XmlElement(ElementName = "CtiyName")]
            public string CtiyName { get; set; }
            [XmlElement(ElementName = "ItemCode")]
            public string ItemCode { get; set; }
            [XmlElement(ElementName = "ItemName")]
            public string ItemName { get; set; }
            [XmlElement(ElementName = "BookerTypeCode")]
            public string BookerTypeCode { get; set; }
            [XmlElement(ElementName = "BookerTypeName")]
            public string BookerTypeName { get; set; }
            [XmlElement(ElementName = "BookingPathCode")]
            public string BookingPathCode { get; set; }
            [XmlElement(ElementName = "BookingPathName")]
            public string BookingPathName { get; set; }
            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }
            [XmlElement(ElementName = "CheckInWeekday")]
            public string CheckInWeekday { get; set; }
            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
            [XmlElement(ElementName = "CheckOutWeekday")]
            public string CheckOutWeekday { get; set; }
            [XmlElement(ElementName = "Duration")]
            public string Duration { get; set; }
            [XmlElement(ElementName = "CheckInLeftDays")]
            public string CheckInLeftDays { get; set; }
            [XmlElement(ElementName = "CancelDeadLine")]
            public string CancelDeadLine { get; set; }
            [XmlElement(ElementName = "DeadLineWeekday")]
            public string DeadLineWeekday { get; set; }
            [XmlElement(ElementName = "DeadLineLeftDays")]
            public string DeadLineLeftDays { get; set; }
            [XmlElement(ElementName = "FreeBreakfastCode")]
            public string FreeBreakfastCode { get; set; }
            [XmlElement(ElementName = "FreeBreakfastName")]
            public string FreeBreakfastName { get; set; }
            [XmlElement(ElementName = "AddBreakfastCode")]
            public string AddBreakfastCode { get; set; }
            [XmlElement(ElementName = "AddBreakfastName")]
            public string AddBreakfastName { get; set; }
            [XmlElement(ElementName = "ExchangeConvertDate")]
            public string ExchangeConvertDate { get; set; }
            [XmlElement(ElementName = "ExchangeConvertWeekday")]
            public string ExchangeConvertWeekday { get; set; }
            [XmlElement(ElementName = "SellingCurrencyCode")]
            public string SellingCurrencyCode { get; set; }
            [XmlElement(ElementName = "SellingConvertRate")]
            public string SellingConvertRate { get; set; }
            [XmlElement(ElementName = "ClientCurrencyCode")]
            public string ClientCurrencyCode { get; set; }
            [XmlElement(ElementName = "LocalProductAmount")]
            public string LocalProductAmount { get; set; }
            [XmlElement(ElementName = "LocalCommAmount")]
            public string LocalCommAmount { get; set; }
            [XmlElement(ElementName = "LocalTaxAmount")]
            public string LocalTaxAmount { get; set; }
            [XmlElement(ElementName = "LocalChangedAmount")]
            public string LocalChangedAmount { get; set; }
            [XmlElement(ElementName = "LocalCancelCharge")]
            public string LocalCancelCharge { get; set; }
            [XmlElement(ElementName = "LocalSellingAmount")]
            public string LocalSellingAmount { get; set; }
            [XmlElement(ElementName = "LocalPartnerAmount")]
            public string LocalPartnerAmount { get; set; }
            [XmlElement(ElementName = "ClientProductAmount")]
            public string ClientProductAmount { get; set; }
            [XmlElement(ElementName = "ClientCommAmount")]
            public string ClientCommAmount { get; set; }
            [XmlElement(ElementName = "ClientCancelCharge")]
            public string ClientCancelCharge { get; set; }
            [XmlElement(ElementName = "ClientPartnerAmount")]
            public string ClientPartnerAmount { get; set; }
            [XmlElement(ElementName = "ClientSellingAmount")]
            public string ClientSellingAmount { get; set; }
            [XmlElement(ElementName = "ItemStatusCode")]
            public string ItemStatusCode { get; set; }
            [XmlElement(ElementName = "ItemStatusName")]
            public string ItemStatusName { get; set; }
            [XmlElement(ElementName = "ItemConfirmationNo")]
            public string ItemConfirmationNo { get; set; }
            [XmlElement(ElementName = "ItemConfrimedTime")]
            public string ItemConfrimedTime { get; set; }
            [XmlElement(ElementName = "TaxSheetStatusCode")]
            public string TaxSheetStatusCode { get; set; }
            [XmlElement(ElementName = "TaxSheetStatusName")]
            public string TaxSheetStatusName { get; set; }
            [XmlElement(ElementName = "ItemCancelReasonCode")]
            public string ItemCancelReasonCode { get; set; }
            [XmlElement(ElementName = "ItemCancelReasonName")]
            public string ItemCancelReasonName { get; set; }
            [XmlElement(ElementName = "VoucherReferenceName")]
            public string VoucherReferenceName { get; set; }
            [XmlElement(ElementName = "PorcessingComment")]
            public string PorcessingComment { get; set; }
            [XmlElement(ElementName = "RoomTypeCode")]
            public string RoomTypeCode { get; set; }
            [XmlElement(ElementName = "RoomTypeName")]
            public string RoomTypeName { get; set; }
            [XmlElement(ElementName = "ModifyAble")]
            public string ModifyAble { get; set; }
            [XmlElement(ElementName = "DeleteAble")]
            public string DeleteAble { get; set; }
            [XmlElement(ElementName = "LastWriteTime")]
            public string LastWriteTime { get; set; }
            [XmlElement(ElementName = "VatSheetYn")]
            public string VatSheetYn { get; set; }
            [XmlElement(ElementName = "BookingDate")]
            public string BookingDate { get; set; }
            [XmlElement(ElementName = "BookingWeek")]
            public string BookingWeek { get; set; }
            [XmlElement(ElementName = "UserKey")]
            public string UserKey { get; set; }
            [XmlElement(ElementName = "AgentBookingReference")]
            public string AgentBookingReference { get; set; }
            [XmlElement(ElementName = "OPStatusCode")]
            public string OPStatusCode { get; set; }
            [XmlElement(ElementName = "OPStatusName")]
            public string OPStatusName { get; set; }
            [XmlElement(ElementName = "PromotionCode")]
            public string PromotionCode { get; set; }
            [XmlElement(ElementName = "AdminRemarks")]
            public string AdminRemarks { get; set; }
            [XmlElement(ElementName = "NavisionUploadYn")]
            public string NavisionUploadYn { get; set; }
            [XmlElement(ElementName = "NavisionPostingYn")]
            public string NavisionPostingYn { get; set; }
            [XmlElement(ElementName = "RoomsAndGuestList")]
            public RoomsAndGuestList RoomsAndGuestList { get; set; }
            [XmlElement(ElementName = "BookMailCount")]
            public string BookMailCount { get; set; }
            [XmlElement(ElementName = "CancelMailCount")]
            public string CancelMailCount { get; set; }
            [XmlElement(ElementName = "ModifyMailCount")]
            public string ModifyMailCount { get; set; }
            [XmlElement(ElementName = "VoucherMailCount")]
            public string VoucherMailCount { get; set; }
            [XmlElement(ElementName = "VoucherRemarks")]
            public string VoucherRemarks { get; set; }
            [XmlElement(ElementName = "SpecialRemarks")]
            public string SpecialRemarks { get; set; }
            [XmlElement(ElementName = "TravelerNationality")]
            public string TravelerNationality { get; set; }
            [XmlElement(ElementName = "CancelNotice")]
            public string CancelNotice { get; set; }
        }
        public class RoomsAndGuestList
        {
            [XmlElement(ElementName = "RoomsAndGuestInfo")]
            public List<RoomsAndGuestInfo> RoomsAndGuestInfo { get; set; }
        }
        public class RoomsAndGuestInfo
        {
            [XmlElement(ElementName = "RoomNo")]
            public string RoomNo { get; set; }
            [XmlElement(ElementName = "BedTypeCode")]
            public string BedTypeCode { get; set; }
            [XmlElement(ElementName = "BedTypeName")]
            public string BedTypeName { get; set; }
            [XmlElement(ElementName = "LocalAddedAmount")]
            public string LocalAddedAmount { get; set; }
            [XmlElement(ElementName = "VouchereComment")]
            public string VouchereComment { get; set; }
            [XmlElement(ElementName = "GuestList")]
            public GuestList GuestList { get; set; }
        }
        public class GuestList
        {
            [XmlElement(ElementName = "GuestInfo")]
            public List<GuestInfo> GuestInfo { get; set; }
        }
        public class GuestInfo
        {
            [XmlElement(ElementName = "GuestNo")]
            public string GuestNo { get; set; }
            [XmlElement(ElementName = "GuestName")]
            public string GuestName { get; set; }
        }
    }
}