﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace XiWan.Data.Rts
{
    public class HotelSearchResp
    {
        public class GetHotelSearchListResponse
        {
            [XmlElement(ElementName = "GetHotelSearchListResult")]
            public GetHotelSearchListResult GetHotelSearchListResult { get; set; }
        }
        public class GetHotelSearchListResult
        {
            [XmlElement(ElementName = "GetHotelSearchListResponse")]
            public GetHotelSearchListResponse2 GetHotelSearchListResponse { get; set; }
        }
        public class GetHotelSearchListResponse2
        {
            [XmlElement(ElementName = "Error")]
            public string Error { get; set; }
            [XmlElement(ElementName = "GetHotelSearchListResult")]
            public GetHotelSearchListResult2 GetHotelSearchListResult { get; set; }
        }
        public class GetHotelSearchListResult2
        {
            [XmlElement(ElementName = "LanguageCode")]
            public string LanguageCode { get; set; }
            [XmlElement(ElementName = "LanguageName")]
            public string LanguageName { get; set; }
            [XmlElement(ElementName = "NationalityCode")]
            public string NationalityCode { get; set; }
            [XmlElement(ElementName = "NationalityName")]
            public string NationalityName { get; set; }
            [XmlElement(ElementName = "ContinentCode")]
            public string ContinentCode { get; set; }
            [XmlElement(ElementName = "CityCode")]
            public string CityCode { get; set; }
            [XmlElement(ElementName = "CityEname")]
            public string CityEname { get; set; }
            [XmlElement(ElementName = "CityName")]
            public string CityName { get; set; }
            [XmlElement(ElementName = "CountryCode")]
            public string CountryCode { get; set; }
            [XmlElement(ElementName = "CountryEname")]
            public string CountryEname { get; set; }
            [XmlElement(ElementName = "CountryName")]
            public string CountryName { get; set; }
            [XmlElement(ElementName = "StateCode")]
            public string StateCode { get; set; }
            [XmlElement(ElementName = "StateEname")]
            public string StateEname { get; set; }
            [XmlElement(ElementName = "StateName")]
            public string StateName { get; set; }
            [XmlElement(ElementName = "CheckInDate")]
            public string CheckInDate { get; set; }
            [XmlElement(ElementName = "CheckInWeekday")]
            public string CheckInWeekday { get; set; }
            [XmlElement(ElementName = "CheckOutDate")]
            public string CheckOutDate { get; set; }
            [XmlElement(ElementName = "CheckOutWeekday")]
            public string CheckOutWeekday { get; set; }
            [XmlElement(ElementName = "Duration")]
            public string Duration { get; set; }
            [XmlElement(ElementName = "CheckInLeftDays")]
            public string CheckInLeftDays { get; set; }
            [XmlElement(ElementName = "ItemName")]
            public string ItemName { get; set; }
            [XmlElement(ElementName = "ItemCode")]
            public string ItemCode { get; set; }
            [XmlElement(ElementName = "ItemNo")]
            public string ItemNo { get; set; }
            [XmlElement(ElementName = "StarRating")]
            public string StarRating { get; set; }
            [XmlElement(ElementName = "LocationCode")]
            public string LocationCode { get; set; }
            [XmlElement(ElementName = "AvailableHotelOnly")]
            public string AvailableHotelOnly { get; set; }
            [XmlElement(ElementName = "RecommendHotelOnly")]
            public string RecommendHotelOnly { get; set; }
            [XmlElement(ElementName = "TotalResultCount")]
            public string TotalResultCount { get; set; }
            [XmlElement(ElementName = "ExchangeConvertDate")]
            public string ExchangeConvertDate { get; set; }
            [XmlElement(ElementName = "SellingCurrencyCode")]
            public string SellingCurrencyCode { get; set; }
            [XmlElement(ElementName = "ClientCurrencyCode")]
            public string ClientCurrencyCode { get; set; }
            [XmlElement(ElementName = "SellingConvertRate")]
            public string SellingConvertRate { get; set; }
            [XmlElement(ElementName = "CityEventList")]
            public string CityEventList { get; set; }
            [XmlElement(ElementName = "RoomList")]
            public RoomList RoomList { get; set; }
            [XmlElement(ElementName = "HotelSearchList")]
            public HotelSearchList HotelSearchList { get; set; }
            [XmlElement(ElementName = "Errors")]
            public string Errors { get; set; }
        }
        public class RoomList
        {
            [XmlElement(ElementName = "RoomInfo")]
            public List<RoomInfo> RoomInfo { get; set; }
        }
        public class RoomInfo
        {
            [XmlElement(ElementName = "BedTypeCode")]
            public string BedTypeCode { get; set; }
            [XmlElement(ElementName = "RoomCount")]
            public string RoomCount { get; set; }
            [XmlElement(ElementName = "ChlidAge1")]
            public int ChlidAge1 { get; set; }
            [XmlElement(ElementName = "ChlidAge2")]
            public int ChlidAge2 { get; set; }
        }
        public class HotelSearchList
        {
            [XmlElement(ElementName = "HotelItemInfo")]
            public HotelItemInfo HotelItemInfo { get; set; }
        }
        public class HotelItemInfo
        {
            [XmlElement(ElementName = "ItemCode")]
            public string ItemCode { get; set; }
            [XmlElement(ElementName = "ItemName")]
            public string ItemName { get; set; }
            [XmlElement(ElementName = "StarRating")]
            public string StarRating { get; set; }
            [XmlElement(ElementName = "RecommendYn")]
            public string RecommendYn { get; set; }
            [XmlElement(ElementName = "ExpertReportYn")]
            public string ExpertReportYn { get; set; }
            [XmlElement(ElementName = "FirstImageFileName")]
            public string FirstImageFileName { get; set; }
            [XmlElement(ElementName = "HotelDescription")]
            public string HotelDescription { get; set; }
            [XmlElement(ElementName = "BackpackYn")]
            public string BackpackYn { get; set; }
            [XmlElement(ElementName = "BusinessYn")]
            public string BusinessYn { get; set; }
            [XmlElement(ElementName = "HoneymoonYn")]
            public string HoneymoonYn { get; set; }
            [XmlElement(ElementName = "FairYn")]
            public string FairYn { get; set; }
            [XmlElement(ElementName = "AirPackYn")]
            public string AirPackYn { get; set; }
            [XmlElement(ElementName = "BookingCount")]
            public string BookingCount { get; set; }
            [XmlElement(ElementName = "GeoCode")]
            public GeoCode GeoCode { get; set; }
            [XmlElement(ElementName = "LocationList")]
            public LocationList LocationList { get; set; }
            [XmlElement(ElementName = "PriceList")]
            public PriceList PriceList { get; set; }
        }
        public class GeoCode
        {
            [XmlElement(ElementName = "Latitude")]
            public string Latitude { get; set; }
            [XmlElement(ElementName = "Longitude")]
            public string Longitude { get; set; }
            [XmlElement(ElementName = "PhoneNo")]
            public string PhoneNo { get; set; }
            [XmlElement(ElementName = "FaxNo")]
            public string FaxNo { get; set; }
            [XmlElement(ElementName = "Email")]
            public string Email { get; set; }
            [XmlElement(ElementName = "Address")]
            public string Address { get; set; }
            [XmlElement(ElementName = "HomepageUrl")]
            public string HomepageUrl { get; set; }
        }
        public class LocationList
        {
            [XmlElement(ElementName = "Location")]
            public List<Location> Location { get; set; }
        }
        public class Location
        {
            [XmlElement(ElementName = "LocationCode")]
            public string LocationCode { get; set; }
            [XmlElement(ElementName = "LocationName")]
            public string LocationName { get; set; }
            [XmlElement(ElementName = "GeneralLocationYn")]
            public string GeneralLocationYn { get; set; }
        }
        public class PriceList
        {
            [XmlElement(ElementName = "PriceInfo")]
            public List<PriceInfo> PriceInfo { get; set; }
        }
        public class PriceInfo
        {
            [XmlElement(ElementName = "ItemNo")]
            public string ItemNo { get; set; }
            [XmlElement(ElementName = "ItemCode")]
            public string ItemCode { get; set; }
            [XmlElement(ElementName = "SupplierCompCode")]
            public string SupplierCompCode { get; set; }
            [XmlElement(ElementName = "RoomTypeCode")]
            public string RoomTypeCode { get; set; }
            [XmlElement(ElementName = "RoomTypeName")]
            public string RoomTypeName { get; set; }
            [XmlElement(ElementName = "BreakfastTypeName")]
            public string BreakfastTypeName { get; set; }
            [XmlElement(ElementName = "AddBreakfastTypeName")]
            public string AddBreakfastTypeName { get; set; }
            [XmlElement(ElementName = "PriceComment")]
            public string PriceComment { get; set; }
            [XmlElement(ElementName = "FareRateType")]
            public string FareRateType { get; set; }
            [XmlElement(ElementName = "PriceStatus")]
            public string PriceStatus { get; set; }
            [XmlElement(ElementName = "NetCurrencyCode")]
            public string NetCurrencyCode { get; set; }
            [XmlElement(ElementName = "NetConvertRate")]
            public string NetConvertRate { get; set; }
            [XmlElement(ElementName = "SellerNetPrice")]
            public string SellerNetPrice { get; set; }
            [XmlElement(ElementName = "LocalNetPrice")]
            public string LocalNetPrice { get; set; }
            [XmlElement(ElementName = "SellerMarkupPrice")]
            public string SellerMarkupPrice { get; set; }
            [XmlElement(ElementName = "RecommendClientPrice")]
            public string RecommendClientPrice { get; set; }
            [XmlElement(ElementName = "SellerClientPrice")]
            public string SellerClientPrice { get; set; }
            [XmlElement(ElementName = "Comment")]
            public string Comment { get; set; }
            [XmlElement(ElementName = "DoubleBedYn")]
            public string DoubleBedYn { get; set; }
            [XmlElement(ElementName = "PriceBreakdown")]
            public List<PriceBreakdown> PriceBreakdown { get; set; }
        }
        public class PriceBreakdown
        {
            [XmlElement(ElementName = "Date")]
            public string Date { get; set; }
            [XmlElement(ElementName = "Price")]
            public string Price { get; set; }
        }

    }
}