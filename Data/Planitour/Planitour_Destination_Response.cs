﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    public class Planitour_Destination_Response
    {
        public List<DataItem> Data { get; set; }
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public int StatusCode { get; set; }
        public string ReasonPhrase { get; set; }
        public class DataItem
        {
            /// <summary>
            /// DestinationId
            /// </summary>
            public int DestinationId { get; set; }
            /// <summary>
            /// PARIS
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// ParentDestinationId
            /// </summary>
            public int ParentDestinationId { get; set; }
            /// <summary>
            /// Children
            /// </summary>
            public List<ChildrenItem> Children { get; set; }
        }
        public class ChildrenItem
        {
            /// <summary>
            /// DestinationId
            /// </summary>
            public int DestinationId { get; set; }
            /// <summary>
            /// Test
            /// </summary>
            public string Name { get; set; }
            /// <summary>
            /// ParentDestinationId
            /// </summary>
            public int ParentDestinationId { get; set; }
            /// <summary>
            /// Children
            /// </summary>
            public List<ChildrenItem> Children { get; set; }
        }
    }
}