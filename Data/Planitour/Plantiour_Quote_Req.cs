﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    public class Plantiour_Quote_Req
    {
        public string SessionID {get;set;}
        public int DestinationId {get;set;}
        public string CheckInDate {get;set;}
        public string CheckOutDate {get;set;}
        public string CurrencyCode {get;set;}
        public string NationalityCode {get;set;}
        public List<RoomItem> Rooms
        {
            set;
            get;
        }
        public class RoomItem
        {
            public int RoomID { set; get; }
            public int AdultsNo { set; get; }
            public int ChildrenNo { set; get; }
            public List<ChildrenAgesItem> ChildrenAges
            {
                set;
                get;
            }
        }
        public class ChildrenAgesItem
        {
            public int Ages { set; get; }
        }
    }
}