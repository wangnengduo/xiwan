﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    /// <summary>
    /// 请求取消策略条件和价格细分实体类
    /// </summary>
    public class Planitour_Booking_Req
    {
        public string SessionID { get; set; }
        public string TID { get; set; }
        /// <summary>
        /// Plantiour_Quote_Resp回复的HotelCode
        /// </summary>
        public string HotelCode { get; set; }
    }
}