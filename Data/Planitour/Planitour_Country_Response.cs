﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    public class Planitour_Country_Response
    {
        public List<DataItem> Data { get; set; }
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public int StatusCode { get; set; }
        public string ReasonPhrase { get; set; }
        public class DataItem
        {
            public int CountryId { get; set; }
            public string ISO { get; set; }
            public string Name { get; set; }
        }
    }
}