﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using XiWan.DALFactory;
using System.Text.RegularExpressions;
using System.Collections;
using XiWan.DALFactory.Model;
using XiWan.Data.Redis;
using Newtonsoft.Json;

namespace XiWan.Data
{
    public class PlanitourDAL
    {
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        #region 初始化本类
        private static PlanitourDAL _Instance;
        public static PlanitourDAL Instance
        {
            get
            {
                return new PlanitourDAL();
            }
        }
        #endregion

        /// <summary>
        /// 获取国家信息（从对方系统拿数据）
        /// </summary>
        /// <param name="url"></param>
        public bool GetCountries(string url)
        {
            bool result =true;
            try
            {
                string resultToGet = Common.Common.GetHttp(url);
                //resultToGet = "{\"Data\": [{\"CountryId\": 6069,\"ISO\": \"AF\",\"Name\": \"AFGHANISTAN\"},{\"CountryId\": 7718,\"ISO\": \"AT\",\"Name\": \"AUSTRIA\"}],\"Success\": true,\"Messages\": [\"OK\"],\"StatusCode\": 200,\"ReasonPhrase\": \"OK\"}";
                Planitour_Country_Response respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_Country_Response>(resultToGet);
                Planitour_CountryCollection objCollection = new Planitour_CountryCollection();
                if (respObj.Success == true)
                {
                    List<Planitour_Country_Response.DataItem> objItem = new List<Planitour_Country_Response.DataItem>(respObj.Data);
                    for (int i = 0; i < objItem.Count; i++)
                    {
                        Planitour_Country obj = new Planitour_Country();
                        obj.CountryId = objItem[i].CountryId;
                        obj.ISO = objItem[i].ISO;
                        obj.Name = objItem[i].Name;
                        obj.EntityState = e_EntityState.Added;
                        objCollection.Add(obj);
                    }
                    con.Save(objCollection);
                } 
            }
            catch (Exception ex)
            {
                result = false;
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取国家信息（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取目的地（从对方系统拿数据）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool GetDestinations(string url,int countryId)
        {
            bool result = true;
            try
            {
                string resultToGet = Common.Common.GetHttp(url);
                //resultToGet = "{\"Data\": [{\"DestinationId\": 312495,\"Name\": \"PARIS\",\"ParentDestinationId\": null,\"Children\": [{\"DestinationId\": 497734,\"Name\": \"Test\",\"ParentDestinationId\": 312495,\"Children\": []},{\"DestinationId\": 497735,\"Name\": \"Test2\",\"ParentDestinationId\": 312495,\"Children\": [{\"DestinationId\": 497736,\"Name\": \"Test3\",\"ParentDestinationId\": 497735,\"Children\": []}]}]},{\"DestinationId\": 241858,\"Name\": \"NICE\",\"ParentDestinationId\": null,\"Children\": []},{\"DestinationId\": 242693,\"Name\": \"LYON\",\"ParentDestinationId\": null,\"Children\": []}],\"Success\": true,\"Messages\": [\"OK\"],\"StatusCode\": 200,\"ReasonPhrase\": \"OK\"}";
                Planitour_Destination_Response respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_Destination_Response>(resultToGet);
                Planitour_DestinationsCollection objCollection = new Planitour_DestinationsCollection();
                if (respObj.Success == true)
                {
                    List<Planitour_Destination_Response.DataItem> objItem = new List<Planitour_Destination_Response.DataItem>(respObj.Data);
                    for (int i = 0; i < objItem.Count; i++)
                    {
                        Planitour_Destinations obj = new Planitour_Destinations();
                        obj.DestinationId = objItem[i].DestinationId;
                        obj.Name = objItem[i].Name;
                        obj.ParentDestinationId = objItem[i].ParentDestinationId;
                        obj.countryId = countryId;
                        obj.EntityState = e_EntityState.Added;
                        objCollection.Add(obj);
                        List<Planitour_Destination_Response.ChildrenItem> childrenItem = new List<Planitour_Destination_Response.ChildrenItem>(objItem[i].Children);
                        for (int j = 0; j < childrenItem.Count; j++)
                        {
                            obj.DestinationId = childrenItem[j].DestinationId;
                            obj.Name = childrenItem[j].Name;
                            obj.ParentDestinationId = childrenItem[j].ParentDestinationId;
                            obj.countryId = countryId;
                            obj.EntityState = e_EntityState.Added;
                            objCollection.Add(obj);
                            objCollection.Add(obj);
                            List<Planitour_Destination_Response.ChildrenItem> childrenItem2 = new List<Planitour_Destination_Response.ChildrenItem>(childrenItem[j].Children);
                            for (int y = 0; y < childrenItem2.Count; j++)
                            {
                                obj.DestinationId = childrenItem2[y].DestinationId;
                                obj.Name = childrenItem2[y].Name;
                                obj.ParentDestinationId = childrenItem2[y].ParentDestinationId;
                                obj.countryId = countryId;
                                obj.EntityState = e_EntityState.Added;
                                objCollection.Add(obj);
                            }
                        }
                    }
                    con.Save(objCollection);
                }
            }
            catch (Exception ex)
            {
                result = false;
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取目的地（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取酒店
        /// </summary>
        /// <param name="url"></param>
        /// <param name="destinationId"></param>
        /// <returns></returns>
        public bool GetHotel(string url, int destinationId)
        {
            bool result = true;
            try
            { 
                string resultToGet = Common.Common.GetHttp(url);
                //resultToGet = "{\"Data\": [{\"DestinationId\": 312495,\"Name\": \"PARIS\",\"ParentDestinationId\": null,\"Children\": [{\"DestinationId\": 497734,\"Name\": \"Test\",\"ParentDestinationId\": 312495,\"Children\": []},{\"DestinationId\": 497735,\"Name\": \"Test2\",\"ParentDestinationId\": 312495,\"Children\": [{\"DestinationId\": 497736,\"Name\": \"Test3\",\"ParentDestinationId\": 497735,\"Children\": []}]}]},{\"DestinationId\": 241858,\"Name\": \"NICE\",\"ParentDestinationId\": null,\"Children\": []},{\"DestinationId\": 242693,\"Name\": \"LYON\",\"ParentDestinationId\": null,\"Children\": []}],\"Success\": true,\"Messages\": [\"OK\"],\"StatusCode\": 200,\"ReasonPhrase\": \"OK\"}";
                Planitour_Hotel_Response respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_Hotel_Response>(resultToGet);
                Planitour_HotelCollection objCollection = new Planitour_HotelCollection();
                if (respObj.Success == true)
                {
                    List<Planitour_Hotel_Response.DataItem> objItem = new List<Planitour_Hotel_Response.DataItem>(respObj.Data);
                    for (int i = 0; i < objItem.Count; i++)
                    {
                        Planitour_Hotel obj = new Planitour_Hotel();
                        obj.HotelCode = objItem[i].HotelCode;
                        obj.Name = objItem[i].Name;
                        obj.Category = objItem[i].Category;
                        obj.StarRating = objItem[i].StarRating;
                        obj.PropertyType = objItem[i].PropertyType;
                        obj.DestinationId = destinationId;
                        obj.EntityState = e_EntityState.Added;
                        objCollection.Add(obj);
                    }
                    con.Save(objCollection);
                }   
            }
            catch(Exception ex)
            {
                result = false;
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取酒店（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取酒店明细信息
        /// </summary>
        /// <param name="url"></param>
        /// <param name="hotelCode"></param>
        /// <returns></returns>
        public bool GetHotelDetail(string url, int hotelCode)
        {
            bool result = true;
            try
            {
                string resultToGet = Common.Common.GetHttp(url);
                Planitour_HotelDetail_Resp respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_HotelDetail_Resp>(resultToGet);
                if (respObj.Success == true)
                {
                    Planitour_HotelDetail_Resp.DataItem respObjData = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_HotelDetail_Resp.DataItem>(resultToGet);
                    Planitour_HotelDetail obj = new Planitour_HotelDetail();
                    obj.HotelCode =respObjData.HotelCode;
                    obj.Name =respObjData.HotelCode;
                    obj.Description = respObjData.Description;
                    obj.Latitude = respObjData.Latitude;
                    obj.Longitude = respObjData.Longitude;
                    obj.StarRating = respObjData.StarRating;
                    obj.Category = respObjData.Category;
                    obj.Location = respObjData.Location;
                    obj.Email = respObjData.Email;
                    obj.Address = respObjData.Address;
                    obj.Phone = respObjData.Phone;
                    //当存在时做更新
                    string sql = "select top 1 * from Planitour_HotelDetail WITH(NOLOCK) where HotelCode='{0}'";
                    int ID = ControllerFactory.GetController().GetDataTable(sql).Rows[0]["ID"].AsTargetType<int>(0);
                    if (ID != 0)
                    {
                        obj.ID = ID;
                    }
                    con.Save(obj);
                }
            }
            catch (Exception ex)
            {
                result = false;
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取酒店明细信息（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取可用语言
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool GetLanguages(string url)
        {
            bool result = true;
            try
            { 
                string resultToGet = Common.Common.GetHttp(url);
                Planitour_Languages_Resp respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_Languages_Resp>(resultToGet);
                if (respObj.Success == true)
                {
                    Planitour_Languages_Resp.DataItem respObjData = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_Languages_Resp.DataItem>(resultToGet);
                    Planitour_Languages obj = new Planitour_Languages();
                    obj.Code = respObjData.Code;
                    obj.Name = respObjData.Name;
                    con.Save(obj);
                }
            }
            catch (Exception ex)
            {
                result = false;
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取可用语言（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取可用货币
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool GetCurrencies(string url)
        {
            bool result = true;
            try
            {
                string resultToGet = Common.Common.GetHttp(url);
                Planitour_Currencies_Resp respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_Currencies_Resp>(resultToGet);
                if (respObj.Success == true)
                {
                    Planitour_Currencies_Resp.DataItem respObjData = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_Currencies_Resp.DataItem>(resultToGet);
                    Planitour_Currencies obj = new Planitour_Currencies();
                    obj.Code = respObjData.Code;
                    con.Save(obj);
                }
            }
            catch (Exception ex)
            {
                result = false;
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取可用货币（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取可用国籍
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public bool GetNationalities(string url)
        {
            bool result = true;
            try
            {
                string resultToGet = Common.Common.GetHttp(url);
                Plantiour_Nationalities_Resp respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Plantiour_Nationalities_Resp>(resultToGet);
                if (respObj.Success == true)
                {
                    Plantiour_Nationalities_Resp.DataItem respObjData = Newtonsoft.Json.JsonConvert.DeserializeObject<Plantiour_Nationalities_Resp.DataItem>(resultToGet);
                    Plantiour_Nationalities obj = new Plantiour_Nationalities();
                    obj.Code = respObjData.Code;
                    obj.Name = respObjData.Name;
                    con.Save(obj);
                }
            }
            catch (Exception ex)
            {
                result = false;
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取可用国籍（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取SessionID
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetSessionID(string url)
        {
            string result = string.Empty;
            try
            {
                string resultToPost = Common.Common.PostHttp(url,"");
                Planitour_SessionID_Resp respObj = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_SessionID_Resp>(resultToPost);
                if (respObj.Success == true)
                {
                    Planitour_SessionID_Resp.DataItem respObjData = Newtonsoft.Json.JsonConvert.DeserializeObject<Planitour_SessionID_Resp.DataItem>(resultToPost);
                    result = respObjData.SessionID;
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取SessionID（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
        /// <summary>
        /// 获取报价
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetHotelQuote(string url, string SessionID, int DestinationId, string CheckInDate, string CheckOutDate, string CurrencyCode, string NationalityCode)
        {
            string result = string.Empty;
            try
            {
                Plantiour_Quote_Req obj = new Plantiour_Quote_Req();
                obj.SessionID = SessionID;
                obj.DestinationId = DestinationId;
                obj.CheckInDate = CheckInDate;
                obj.CheckOutDate = CheckOutDate;
                obj.CurrencyCode = CurrencyCode;
                obj.NationalityCode = NationalityCode;
                List<Plantiour_Quote_Req.RoomItem> RelList = new List<Plantiour_Quote_Req.RoomItem>();
                obj.Rooms = RelList;
                //返回接口
                string resultToPost = Common.Common.PostHttp(url, JsonConvert.SerializeObject(obj));
                LogHelper.DoOperateLog(string.Format("Studio:Planitour获取可用国籍（从对方系统拿数据） ,请求数据:{0} ,接口回复数据：{1} , Time:{2} ", JsonConvert.SerializeObject(obj),resultToPost, DateTime.Now.ToString()), "Planitour接口");
                //解析json获得实体
                Plantiour_Quote_Resp Quote = Newtonsoft.Json.JsonConvert.DeserializeObject<Plantiour_Quote_Resp>(resultToPost);
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err:Planitour获取可用国籍（从对方系统拿数据） ,err:{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Planitour接口");
            }
            return result;
        }
    }
}