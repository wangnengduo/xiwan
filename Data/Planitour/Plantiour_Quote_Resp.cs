﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    public class Plantiour_Quote_Resp
    {
        public DataItem Data { get; set; }
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public int StatusCode { get; set; }
        public string ReasonPhrase { get; set; }
        public class DataItem
        {
            public List<HotelItem> Hotels
            {
                set;
                get;
            }
        }
        public class HotelItem
        {
            public string HotelCode { get; set; }
            public string HotelName { get; set; }
            public string DestinationName { get; set; }
            public int DestinationId { get; set; }
            public List<OptionItem> Options
            {
                set;
                get;
            }
        }
        public class OptionItem
        { 
            public string TID { get; set; }
            public bool OnRequest { get; set; }
            public decimal TotalPrice { get; set; }
            public string Currency { get; set; }
            public string MealType { get; set; }
            public string MealName { get; set; }
            public List<RoomItem> Rooms
            {
                set;
                get;
            }
        }
        public class RoomItem
        {
            public int RoomID { get; set; }
            public string Description { get; set; }
        }
    }
}