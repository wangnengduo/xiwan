﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    public class Planitour_HotelDetail_Resp
    {
        public DataItem Data { get; set; }
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public int StatusCode { get; set; }
        public string ReasonPhrase { get; set; }
        public class DataItem
        {
            public string HotelCode { get; set; }
            public string Name { get; set; }
            public string Description { get; set; }
            public decimal Latitude { get; set; }
            public decimal Longitude { get; set; }
            public int StarRating { get; set; }
            public string Category { get; set; }
            public string Location { get; set; }
            public string Email { get; set; }
            public string Address { get; set; }
            public string Phone { get; set; }
        }
    }
}