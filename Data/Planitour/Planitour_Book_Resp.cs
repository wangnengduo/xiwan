﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    /// <summary>
    /// 预订返回实体类
    /// </summary>
    public class Planitour_Book_Resp
    {
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public DataItem Data { get; set; }
        public int StatusCode { get; set; }
        public string ReasonPhrase { get; set; }
    }
    public class DataItem
    {
        public string HotelCode { get; set; }
        public string HotelName { get; set; }
        public int DestinationId { get; set; }
        public string DestinationName { get; set; }
        public DateTime CheckInDate { get; set; }
        public DateTime CheckOutDate { get; set; }
        public string BookingReference { get; set; }
        public string BookingStatus { get; set; }
        public int PaymentType { get; set; }
        public bool IsPaid { get; set; }
        public DateTime CancellationDeadline { get; set; }
        public bool NonRefundable { get; set; }
        public string CancellationConditions { get; set; }
        public decimal Price { get; set; }
        public string Currency { get; set; }
        public List<RoomItem> Rooms { get; set; }
        public List<VoucherDataItem> VoucherData { get; set; }
    }
    public class RoomItem
    {
        public int RoomId { get; set; }       
        public string RoomDescription { get; set; }
        public string MealType { get; set; }
        public string MealName { get; set; }
        public decimal Price { get; set; }
        public int AdultsNo { get; set; }
        public int ChildrenNo { get; set; }
        public List<PassengerItem> Passengers { get; set; }
    }
    public class PassengerItem
    {
        public string Salutation { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsChild { get; set; }
        public int Age { get; set; }
    }
    public class VoucherDataItem
    {
        public string Leader { get; set; }
        public string References { get; set; }
        public string HotelConfirmationNumber { get; set; }
        public DateTime DateOfIssue { get; set; }
        public string EmergencyInfo { get; set; }
        public string Remarks { get; set; }
        public string ProviderRemarks { get; set; }
    }
}