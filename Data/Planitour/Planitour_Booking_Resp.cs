﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    /// <summary>
    /// 返回取消策略条件和价格细分实体类
    /// </summary>
    public class Planitour_Booking_Resp
    {
        public DataItem Data { get; set; }
        public bool Success { get; set; }
        public List<string> Messages { get; set; }
        public int StatusCode { get; set; }
        public string ReasonPhrase { get; set; }
        public class DataItem
        {
            public CxlPolicyItem CxlPolicy
            {
                get;
                set;
            }
            public DayPricesItem DayPrices
            {
                get;
                set;
            }
        }
        public class CxlPolicyItem
        { 
            public bool Success {get; set;}
            public DateTime ToDate {get; set;}
            public bool NonRefundable {get; set;}
            public List<ConditionItem> Conditions
            {
                set;
                get;
            }
            public string Comment {get; set;}
            public List<string> Remarks
            {
                set;
                get;
            }
        }
        public class ConditionItem
        {
            public DateTime FromDate {get; set;}
            public DateTime ToDate {get; set;}
            public decimal Amount {get; set;}
            public string Currency {get; set;}
            public string Text {get; set;}
        }

        public class DayPricesItem
        {
            public bool Success { get; set; }
            public string Currency { get; set; }
            public string EssentialInfo { get; set; }
            public string Comment { get; set; }
            public decimal TotalPrice { get; set; }
            public List<SupplementItem> Supplements { get; set; }
            public List<string> SpecialOffers { get; set; }
            public List<RoomItem> Rooms { get; set; }
        }
        public class SupplementItem
        {
            public DateTime DateFrom { get; set; }
            public DateTime DateTo { get; set; }
            public int PaxesNumber { get; set; }
            public decimal Price { get; set; }
            public string Text { get; set; }
            //public string Type { get; set; }
        } 

        public class RoomItem
        {
            public int RoomId { get; set; }
            public int AdultsNo { get; set; }
            public int ChildrenNo { get; set; }
            public List<string> ChildrenAges { get; set; }
            public string Description { get; set; }
            public List<PricesItem> Prices { get; set; }
        }
        public class PricesItem
        {
            public DateTime Date { get; set; }
            public double Price { get; set; }
        }
    }
}