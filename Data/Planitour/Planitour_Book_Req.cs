﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data
{
    /// <summary>
    /// 预订请求实体类
    /// </summary>
    public class Planitour_Book_Req
    {
        public string SessionID { get; set; }
        public string TID { get; set; }
        public string HotelCode { get; set; }
        public int PaymentOption { get; set; }
        public string AgencyReference { get; set; }
        public string CarbonCopyMail { get; set; }
        public List<RoomItem> Rooms { get; set; }
        public class RoomItem
        {
            public int RoomId { get; set; }
            public int AdultsNo { get; set; }
            public int ChildrenNo { get; set; }
            public List<PassengerItem> Passengers { get; set; }
        }
        public class PassengerItem
        {
            public string Salutation { get; set; }
            public string Age { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public bool IsChild { get; set; }
        }
    }
}