﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Data.common
{
    public class HotelBedType
    {
        /// <summary>
        /// 输入房型名称，判断床型
        /// </summary>
        /// <param name="className"></param>
        /// <returns></returns>
        public string HotelBedTypeToName(string className)
        {
            string BedTypeName = "其他";
            if (className.ToLower().Contains("twin") && className.ToLower().Contains("double"))
            {
                BedTypeName = "大/双";
            }
            else if (className.ToLower().Contains("twin") && className.ToLower().Contains("queen"))
            {
                BedTypeName = "大/双";
            }
            else if (className.ToLower().Contains("twin"))
            {
                BedTypeName = "双床 ";
            }
            else if (className.ToLower().Contains("queen") || className.ToLower().Contains("double") || className.ToLower().Contains("big") || className.ToLower().Contains("king"))
            {
                BedTypeName = "大床 ";
            }
            else if (className.ToLower().Contains("single") || className.ToLower().Contains("individual"))
            {
                BedTypeName = "单人床";
            }
            return BedTypeName;
        }
    }
}