﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Web;
using System.Net;
using System.IO;
using System.Data;
using System.Reflection;
using System.Xml.Serialization;
using System.Security.Cryptography;
using System.Xml;
using XiWan.DALFactory;
using System.Web.Script.Serialization;
using System.IO.Compression;
using XiWan.DALFactory.Model;

namespace XiWan.Data.Common
{
    public class Common
    {
        /// <summary>
        /// Get函数
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetHttp(string url)
        {
            string content = "";
            try
            {
                //处理HttpWebRequest访问https有安全证书的问题（ 请求被中止: 未能创建 SSL/TLS 安全通道。）
                ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;

                System.Net.HttpWebRequest myRequest = (System.Net.HttpWebRequest)WebRequest.Create(url);

                myRequest.Method = "GET";
                //myRequest.Headers.Add("Accept-Encoding", "gzip, deflate");
                //myRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                System.IO.StreamReader reader = new System.IO.StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                content = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：Get函数 ,错误信息 :{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return ex.Message;
            }
            return content;
        }

        /// <summary>
        /// Post函数
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string PostHttp(string url, string body)
        {
            string responseContent = "";
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
                //httpWebRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 60000;

                byte[] btBodys = Encoding.UTF8.GetBytes(body);
                httpWebRequest.ContentLength = btBodys.Length;
                //处理HttpWebRequest访问https有安全证书的问题（ 请求被中止: 未能创建 SSL/TLS 安全通道。）
                ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                httpWebRequest.GetRequestStream().Write(btBodys, 0, btBodys.Length);
                
                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                //httpWebResponse.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
                StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
                responseContent = streamReader.ReadToEnd();

                httpWebResponse.Close();
                streamReader.Close();
                httpWebRequest.Abort();
                httpWebResponse.Close();
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：PostHttp ,错误信息 :{0} , 请求url：{1}，请求数据：{2}， Time:{3} ", ex.Message,url,body, DateTime.Now.ToString()), "Err");
                return ex.Message;
            }
            return responseContent;
        }

        /// <summary>
        /// WebReq函数
        /// </summary>
        /// <param name="url"></param>
        /// <param name="body">请求内容</param>
        /// <returns></returns>
        public static string WebReq(string url, string body)
        {
            string _returnstr = "";
            //发起请求
            //处理HttpWebRequest访问https有安全证书的问题（ 请求被中止: 未能创建 SSL/TLS 安全通道。）
            //ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
            //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            WebRequest webRequest = WebRequest.Create(url);
            //webRequest.Headers.Add(HttpRequestHeader.AcceptEncoding, "gzip,deflate");
            webRequest.ContentType = "text/xml; charset=utf-8";
            webRequest.Method = "POST";
            using (Stream requestStream = webRequest.GetRequestStream())
            {
                byte[] paramBytes = Encoding.UTF8.GetBytes(body);
                requestStream.Write(paramBytes, 0, paramBytes.Length);
            }
            //响应
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    _returnstr = myStreamReader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：WebReq函数 ,错误信息 :{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                _returnstr = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
            }
            return _returnstr;
        }

        //table转为list
        public class ModelConvertHelper<T> where T : new()
        {
            public static List<T> ConvertToModel(DataTable dt)
            {
                // 定义集合    
                List<T> ts = new List<T>();
                // 获得此模型的类型   
                Type type = typeof(T);
                string tempName = "";
                foreach (DataRow dr in dt.Rows)
                {
                    T t = new T();
                    // 获得此模型的公共属性      
                    PropertyInfo[] propertys = t.GetType().GetProperties();
                    foreach (PropertyInfo pi in propertys)
                    {
                        tempName = pi.Name;  // 检查DataTable是否包含此列    
                        if (dt.Columns.Contains(tempName))
                        {
                            // 判断此属性是否有Setter      
                            if (!pi.CanWrite) continue;
                            object value = dr[tempName];
                            if (value != DBNull.Value)
                            {
                                pi.SetValue(t, value, null);
                            }
                        }
                    }
                    ts.Add(t);
                }
                return ts;
            }
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="dt">总数据</param>
        /// <param name="PageIndex">第几页</param>
        /// <param name="PageSize">每页的记录数</param>
        /// <returns></returns>
        public static DataTable GetPagedTable(DataTable dt, int PageIndex, int PageSize)//PageIndex表示第几页，PageSize表示每页的记录数
        {
            if (PageIndex == 0)
                return dt;//0页代表每页数据，直接返回

            DataTable newdt = dt.Copy();
            newdt.Clear();//copy dt的框架

            int rowbegin = (PageIndex - 1) * PageSize;
            int rowend = PageIndex * PageSize;

            if (rowbegin >= dt.Rows.Count)
                return newdt;//源数据记录数小于等于要显示的记录，直接返回dt

            if (rowend > dt.Rows.Count)
                rowend = dt.Rows.Count;
            for (int i = rowbegin; i <= rowend - 1; i++)
            {
                DataRow newdr = newdt.NewRow();
                DataRow dr = dt.Rows[i];
                foreach (DataColumn column in dt.Columns)
                {
                    newdr[column.ColumnName] = dr[column.ColumnName];
                }
                newdt.Rows.Add(newdr);
            }
            return newdt;
        }
        //把xml转为实体
        public static string XmlSerialize<T>(T obj)
        {
            using (StringWriter sw = new StringWriter())
            {
                Type t = obj.GetType();
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(sw, obj);
                sw.Close();
                return sw.ToString();
            }
        }

        /// <summary>
        /// 把实体转为xml(不带表头)
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string XmlSerializeNoTitle<T>(T obj)
        {
            string result = string.Empty;
            try
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                //去除xml声明
                settings.OmitXmlDeclaration = true;
                settings.Encoding = Encoding.Default;
                System.IO.MemoryStream mem = new MemoryStream();
                using (XmlWriter writer = XmlWriter.Create(mem, settings))
                {
                    //去除默认命名空间xmlns:xsd和xmlns:xsi
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("", "");
                    XmlSerializer formatter = new XmlSerializer(obj.GetType());
                    formatter.Serialize(writer, obj, ns);
                }
                result = Encoding.Default.GetString(mem.ToArray());
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：把实体转为xml(不带表头) ,错误信息 :{0} , Time:{1} ", ex.Message, DateTime.Now.ToString()), "Err");
                return null;
            }
            return result;
        }
        /// <summary>
        /// Xml解析为实例类
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="strXML"></param>
        /// <returns></returns>
        public static T DESerializer<T>(string strXML) where T : class
        {
            try
            {
                using (StringReader sr = new StringReader(strXML))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    return serializer.Deserialize(sr) as T;
                }
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(string.Format("Err：Xml解析为实例类 ,错误信息 :{0 }, 请求数据：{1} , Time:{2} ", ex.Message, strXML, DateTime.Now.ToString()), "Err");
                return null;
            }
        }

        /// <summary>
        /// MD5 32位 加密(小写)
        /// </summary>
        /// <param name="input">需要加密的字符串</param>
        /// <returns></returns>
        public static string Md5Encrypt32(string input)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] t = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < t.Length; i++)
                sb.Append(t[i].ToString("x").PadLeft(2, '0'));
            return sb.ToString();
        }

        /// <summary>
        ///  MD5 16位 加密(大写)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Md5Encrypt16(string input)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            string result = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(input)), 4, 8);
            result = result.Replace("-", "");
            return result;
        }

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="key">秘钥</param>
        /// <param name="encryptString">>需要加密的明文</param>
        /// <returns>返回明文</returns>
        public static string Encrypt(string key, string encryptString)
        {
            string str = string.Empty;
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);

                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                str = Convert.ToBase64String(ms.ToArray());
            }
            catch (System.Exception error)
            {
                str = error.Message;
            }

            return str;
        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="key">秘钥</param>
        /// <param name="decryptString">加密后的密文</param>
        /// <returns></returns>
        public static string Decrypt(string key, string decryptString)
        {
            string str = string.Empty;
            try
            {
                //key = Md5Encrypt16(key).Substring(0, 8);//由于秘钥只能是8位，所以用MD5加密后再取8位
                key = key.Substring(0, 8);
                byte[] keyBytes = Encoding.UTF8.GetBytes(key);
                byte[] keyIV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, provider.CreateDecryptor(keyBytes, keyIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                str = Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch (System.Exception error)
            {
                str = "0";
            }
            return str;
        }

        /// <summary>
        /// 生成GUID
        /// </summary>
        /// <returns></returns>
        public static string getGUID()
        {
            System.Guid guid = new Guid();
            guid = Guid.NewGuid();
            string str = guid.ToString();
            return str;
        }
        /// <summary>  
        /// 根据GUID获取19位的唯一数字序列  
        /// </summary>  
        /// <returns></returns>  
        public static string GuidToLongID()
        {
            byte[] buffer = Guid.NewGuid().ToByteArray();
            return BitConverter.ToInt64(buffer, 0).AsTargetType<string>("0");
        }
        public static DateTime ConvertStringToDateTime(string timeStamp)
        {
            DateTime dtStart = TimeZone.CurrentTimeZone.ToLocalTime(new DateTime(1970, 1, 1));
            long lTime = long.Parse(timeStamp + "0000");
            TimeSpan toNow = new TimeSpan(lTime);
            return dtStart.Add(toNow);
        }
        
        /// <summary>
        /// 将json数据反序列化为Dictionary
        /// </summary>
        /// <param name="jsonData">json数据</param>
        /// <returns></returns>
        public static Dictionary<string, object> JsonToDictionary(string jsonData)
        {
            //实例化JavaScriptSerializer类的新实例
            JavaScriptSerializer jss = new JavaScriptSerializer();
            jss.MaxJsonLength = Int32.MaxValue;
            try
            {
                //将指定的 JSON 字符串转换为 Dictionary<string, object> 类型的对象
                return jss.Deserialize<Dictionary<string, object>>(jsonData);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        //格式化xml
        public static string FormatXml(string sUnformattedXml)
        {
            XmlDocument xd = new XmlDocument();
            xd.LoadXml(sUnformattedXml);
            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            XmlTextWriter xtw = null;
            try
            {
                xtw = new XmlTextWriter(sw);
                xtw.Formatting = Formatting.Indented;
                xtw.Indentation = 1;
                xtw.IndentChar = '\t';
                xd.WriteTo(xtw);
            }
            finally
            {
                if (xtw != null)
                    xtw.Close();
            }
            return sb.ToString();
        }

        /// <summary>
        /// C#使用GZIP解压缩完整读取网页内容
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string GetHtmlWithUtf(string url,string id)
        {
            string sHTML = string.Empty;
            //string ProxyStr = "http://192.252.223.44";
            try
            {
                if (!(url.Contains("http://") || url.Contains("https://")))
                {
                    url = "http://" + url;
                }
                HttpWebRequest req = (HttpWebRequest)HttpWebRequest.Create(url);
                /*if (ProxyStr != "" && ProxyStr != null)
                {
                    //设置代理
                    WebProxy proxy = new WebProxy();
                    proxy.Address = new Uri(ProxyStr);
                    req.UseDefaultCredentials = true;
                    req.Proxy = proxy;
                }*/
                req.UserAgent = "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)";
                req.Accept = "*/*";
                req.Headers.Add("Accept-Language", "zh-cn,en-us;q=0.5");
                req.ContentType = "text/xml";

                using (HttpWebResponse response = (HttpWebResponse)req.GetResponse())
                {
                    if (response.ContentEncoding.ToLower().Contains("gzip"))
                    {
                        //处理HttpWebRequest访问https有安全证书的问题（ 请求被中止: 未能创建 SSL/TLS 安全通道。）
                        //ServicePointManager.ServerCertificateValidationCallback += (s, cert, chain, sslPolicyErrors) => true;
                        //ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                        using (GZipStream stream = new GZipStream(response.GetResponseStream(), CompressionMode.Decompress))
                        {
                            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                            {
                                sHTML = reader.ReadToEnd();
                            }
                        }
                    }
                    else if (response.ContentEncoding.ToLower().Contains("deflate"))
                    {
                        using (DeflateStream stream = new DeflateStream(response.GetResponseStream(), CompressionMode.Decompress))
                        {
                            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                            {
                                sHTML = reader.ReadToEnd();
                            }
                        }
                    }
                    else
                    {
                        using (Stream stream = response.GetResponseStream())
                        {
                            using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
                            {
                                sHTML = reader.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                sHTML = ex.ToString();
                LogHelper.DoOperateLog(string.Format("Err：使用GZIP解压缩完整读取网页内容 ,错误信息 :{0 }, 请求数据：{1} , Time:{2} ", ex.Message, url, DateTime.Now.ToString()), "Err");
            }
            return sHTML;
        }

        public static bool GetValue(string json, string key, out string value)
        {
            //解析失败的默认返回值
            value = "";
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                Dictionary<string, object> obj_json = serializer.DeserializeObject(json) as Dictionary<string, object>;
                if (obj_json.ContainsKey(key))
                {
                    //加上这个类型判断即可
                    if (obj_json[key] is System.String)
                    {
                        value = obj_json[key].ToString();
                        return true;
                    }
                    value = serializer.Serialize(obj_json[key]);
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 截取指定字节长度的字符串
        /// </summary>
        /// <param name="str">原字符串</param>
        /// <param name="len">截取字节长度</param>
        /// <returns></returns>
        public static string StringTruncat(string str, int len)
        {
            string result = string.Empty;// 最终返回的结果
            if (string.IsNullOrEmpty(str)) { return result; }
            int byteLen = System.Text.Encoding.Default.GetByteCount(str);// 单字节字符长度
            int charLen = str.Length;// 把字符平等对待时的字符串长度
            int byteCount = 0;// 记录读取进度
            int pos = 0;// 记录截取位置
            if (byteLen > len)
            {
                for (int i = 0; i < charLen; i++)
                {
                    if (Convert.ToInt32(str.ToCharArray()[i]) > 255)// 按中文字符计算加2
                    { byteCount += 2; }
                    else// 按英文字符计算加1
                    { byteCount += 1; }
                    if (byteCount > len)// 超出时只记下上一个有效位置
                    {
                        pos = i;
                        break;
                    }
                    else if (byteCount == len)// 记下当前位置
                    {
                        pos = i + 1;
                        break;
                    }
                }
                if (pos >= 0)
                { result = str.Substring(0, pos); }
            }
            else
            { result = str; }
            return result;
        }

        public static string xmlReplace(string strXml)
        {
            return strXml.Replace("<br/>", "").Replace("<br>", "").Replace("<BR>", "").Replace("<BR/>", "").Replace("<Br>", "").Replace("<Br/>", "").Replace("<bR>", "").Replace("<bR/>", "").Replace("<b>", "").Replace("</b>", "").Replace("<B>", "").Replace("</B>", "").Replace("<ul>", "").Replace("</ul>", "").Replace("<li>", "").Replace("<u>", "").Replace("</u>", "").Replace("<U>", "").Replace("</U>", "").Replace("<i>", "").Replace("</i>", "").Replace("<I>", "").Replace("</I>", "");
        }

        /// <summary>
        /// 以逗号拆分字符串
        /// 若字段中包含逗号(备注：包含逗号的字段必须有双引号引用)则对其进行拼接处理
        /// 最后在去除其字段的双引号
        /// </summary>
        /// <param name="splitStr"></param>
        /// <returns></returns>
        public static string SplitStringWithComma(string splitStr)
        {
            var newstr = string.Empty;

            bool isSplice = false;
            string[] array = splitStr.Split(new char[] { ',' });
            foreach (var str in array)
            {
                if (!string.IsNullOrEmpty(str) && str.IndexOf('"') > -1)
                {
                    var firstchar = str.Substring(0, 1);
                    var lastchar = string.Empty;
                    if (str.Length > 0)
                    {
                        lastchar = str.Substring(str.Length - 1, 1);
                    }
                    if (firstchar.Equals("\"") && !lastchar.Equals("\""))
                    {
                        isSplice = true;
                    }
                    if (lastchar.Equals("\""))
                    {
                        if (!isSplice)
                            newstr += str;
                        else
                            newstr = newstr + "," + str;

                        isSplice = false;
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(newstr))
                        newstr += str;
                }

                if (isSplice)
                {
                    //添加因拆分时丢失的逗号
                    if (string.IsNullOrEmpty(newstr))
                        newstr += str;
                    else
                        newstr = newstr + "," + str;
                }
                else
                {
                    newstr.Replace("\"", "").Trim();//去除字符中的双引号和首尾空格
                }
            }
            return newstr;
        }
    }
}
