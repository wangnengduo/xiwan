﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UserList.aspx.cs" Inherits="Survey.UserList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        body
        {
            width: 98%;
            font-size: 12px;
        }
        .btabs
        {
            border: 1px solid #8DB2E3;
            font-size: 12px;
            height: 26px;
            list-style-type: none;
            margin: 0;
            padding: 4px 0 0 4px;
            width: 99.5%;
            background-color: #E0ECFF;
        }
    </style>
    <link href="js/Treetable_files/jqtreetable.css" rel="stylesheet" type="text/css" />
    <link href="Css/default.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="js/jquery-easyui-1.2.4/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="js/jquery-easyui-1.2.4/themes/icon.css" />
    <script type="text/javascript" src="js/jquery-easyui-1.2.4/jquery-1.6.min.js"></script>
    <script type="text/javascript" src="js/jquery-easyui-1.2.4/jquery.easyui.min.js"></script>
    <script type="text/javascript" src="js/Treetable_files/jqtreetable.js"></script>
    <script type="text/javascript" src="js/btns.js"></script>
    <script type="text/javascript">
        window.onload = windowHeight; //页面载入完毕执行函数
        function windowHeight() {
            var h = document.documentElement.clientHeight;
            var bodyHeight = document.getElementById("content");
            if (h < 598) {
                h = 598;
                bodyHeight.style.height = (h - 130) + "px";
            }
            else bodyHeight.style.height = (h - 130) + "px";
        }
        setInterval(windowHeight, 500)//每半秒执行一次windowHeight函数

        //弹出信息窗口 title:标题 msgString:提示信息 msgType:信息类型 [error,info,question,warning]
        function msgShow(title, msgString, msgType) {
            $.messager.alert(title, msgString, msgType);
        }
        $(function () {
            $('#dd').dialog({
                closed: true,
                modal: true,
                title: '用户管理'
            });
            $('#dd2').dialog({
                closed: true,
                modal: true,
                title: '修改密码'
            });
            $('#dd3').dialog({
                closed: true,
                modal: true,
                title: '个人信息'
            });
            $('#dd4').dialog({
                closed: true,
                modal: true,
                title: '分配角色'
            });
            $('#tt2').tree({
                url: 'ashx/OrglistHandler.ashx',
                onClick: function (node) {
                    $(this).tree('toggle', node.id);
                    databind(node.id);
                    $('#cc').combotree('setValue', node.id);
                },
                onContextMenu: function (e, node) {
                    e.preventDefault();
                    $('#tt2').tree('select', node.target);
                    $('#mm').menu('show', {
                        left: e.pageX,
                        top: e.pageY
                    });
                }
            });
            $('#tt').datagrid({
                width: document.getElementById('div1').width,
                height: document.getElementById('div1').heigh,
                nowrap: false,
                striped: true,
                collapsible: true,
                remoteSort: false,
                singleSelect: true,
                sortName: 'Userid',
                sortOrder: 'desc',
                idField: 'Id',
                url: 'ashx/UserListHandler.ashx',
                columns: [[
                { field: 'LoginName', title: '用户名', width: $(this).width() * 0.2 },
                { field: 'UserRealName', title: '真实姓名', width: $(this).width() * 0.15 },
                { field: 'OrgId', title: '所属部门', width: $(this).width() * 0.15 },
                { field: 'Email', title: 'Email', width: $(this).width() * 0.15 },
                { field: 'Status', title: '状态', width: $(this).width() * 0.15 }
                ]],
                pagination: true
            });
            $('#Role').combogrid({
                panelWidth: 280,
                multiple: true,
                idField: 'Id',
                textField: 'Name',
                url: 'ashx/RoleList.ashx',
                columns: [[
					{ checkbox: 'true' },
					{ field: 'Name', title: '角色名称', width: 100 },
                    { field: 'Remark', title: '角色说明', width: 130 }
				]]
            });
        });
        function close1() {
            $('#dd').dialog('close');
        }
        function close2() {
            $('#dd2').dialog('close');
        }
        function close4() {
            $('#dd4').dialog('close');
        }
        function databind(orgId) {
            $('#tt').datagrid('reload', { 'OrgId': orgId });
        }
        function add() {
            $('#txtLogin').val('');
            $('#txtPass').val('');
            $('#txtPass2').val('');
            $('#txtName').val('');
            $('#Radio1').attr('checked', 'checked');
            $('#txtMail').val('');
            $('#txtTel').val('');
            $('#txtMobile').val('');
            $('#Radio3').attr('checked', 'checked');
            $('#dd').dialog('open');
            $('#hdId').val('');
        }
        function edit() {
            var node = $('#tt').datagrid('getSelected');
            if (node) {
                $('#hdId').val(node.Userid);
                $('#txtLogin').val(node.LoginName);
                $('#txtPass').val(node.Pass);
                $('#txtPass2').val(node.Pass);
                $('#txtName').val(node.UserRealName);
                if (node.Sex == '1')
                    $('#Radio1').attr('checked', 'checked');
                else
                    $('#Radio2').attr('checked', 'checked');
                $('#cc').combotree('setValue', node.OrgId);
                $('#txtMail').val(node.Mail);
                $('#txtTel').val(node.Tel);
                $('#txtMobile').val(node.Mobile);
                if (node.Status == '未启用') {
                    $('#Radio4').attr('checked', 'checked');
                } else if (node.Status == '正常') {
                    $('#Radio3').attr('checked', 'checked');
                } else {
                    $('#Radio5').attr('checked', 'checked');
                }
                $.post('ashx/UsersHandler.ashx?type=edit&Id=' + node.Userid, function (msg) {
                    var str = msg.split(',');
                    for (var i = 0; i < str.length; i++) {
                        $('#Role').combogrid('setValue', str[i]);
                    }
                });
                $('#dd').dialog('open');
            } else {
                $.messager.alert('系统提示', '请选择要编辑的用户', 'error');
            }
        }
        function del() {
            var selected = $('#tt').datagrid('getSelected');
            if (selected) {
                $.messager.confirm('系统提示', '删除后不可恢复，您确定要删除吗?', function (r) {
                    if (r) {
                        $.post('ashx/UsersHandler.ashx?type=del&Id=' + selected.Userid, function (msg) {
                            if (msg == 'true') {
                                $.messager.alert('系统提示', '删除成功', 'info');
                                databind(orgId);
                            } else {
                                $.messager.alert('系统提示', '删除失败，请稍后重试', 'info');
                            }
                        });
                    }
                });

            } else {
                $.messager.alert('系统提示', '请选择要删除的用户', 'error');
            }
        }
        function Save() {
            var login = $('#txtLogin').val();
            var Pass = $('#txtPass').val();
            var Pass2 = $('#txtPass2').val();
            var name = $('#txtName').val();
            var sex = $("input[name='rd']").val();
            var email = $('#txtMail').val();
            var tel = $('#txtTel').val();
            var mobile = $('#txtMobile').val();
            var status = $("input[name='rdStatus']").val();
            var org = $('#cc').combotree('getValue');
            if (login == '') {
                $.messager.alert('系统提示', '请输入账号', 'error');
                return false;
            } else if (Pass == '') {
                $.messager.alert('系统提示', '请输入密码', 'error');
                return false;
            } else if (Pass != Pass2) {
                $.messager.alert('系统提示', '两次秘密输入不一致，请重新输入', 'error');
                $('#txtPass2').val('');
                $('#txtPass').val('');
                return false;
            } else if (name == '') {
                $.messager.alert('系统提示', '请输入姓名', 'error');
                return false;
            }
            else if (org == null) {
                $.messager.alert('系统提示', '请选择所属部门', 'error');
                return false;
            }
            else {
                var Role = '';
                var nodes = $('#Role').combogrid('getValues');
                for (var i = 0; i < nodes.length; i++) {
                    if (Role != '')
                        Role += ",";
                    Role += nodes[i];
                }
                if (Role != '') {
                    var userid = "";
                    var node = $('#tt').datagrid('getSelected');
                    if (node != null)
                        userid = node.Userid;
                    $.post('ashx/UsersHandler.ashx?type=save&Userid=' + userid + '&login=' + encodeURI(login) + "&pass=" + Pass + "&name=" + encodeURI(name) + "&sex=" + sex + "&org=" +org + "&email=" + email + "&tel=" + tel + "&mobile=" + mobile + "&status=" + status + '&role=' + Role, function (msg) {
                        if (userid != null) {
                            $.messager.alert('系统提示', '修改成功', 'info');
                            $('#dd').dialog('close');
                            $('#tt').datagrid('reload');
                        } else {
                            if (msg == 'false') {
                                $.messager.alert('系统提示', '账号已存在，请重新输入账号', 'error');
                                $('#txtLogin').val('');
                            } else {
                                $.messager.alert('系统提示', '添加成功', 'info');
                                $('#dd').dialog('close');
                                $('#tt').datagrid('reload');
                            }
                        }
                    });
                } else {
                    $.messager.alert('系统提示', '请选用户角色', 'error');
                    return false;
                }
            }
        }
        function browse() {
            var node = $('#tt').datagrid('getSelected');
            if (node) {
                $('#lblLogin').html(node.LoginName);
                $('#lblPass').html(node.Pass);
                $('#lblName').html(node.UserRealName);
                $('#lblOrg').html(node.OrgId);
                $('#lblMail').html(node.Mail);
                $('#lblTel').html(node.Tel);
                $('#lblMobile').html(node.Mobile);
                if (node.Sex == '1') {
                    $('#lblSex').html('男');
                } else {
                    $('#lblSex').html('女');
                }
                $('#lblStatus').html(node.Status);
                $.post('ashx/UsersHandler.ashx?type=sel&Id=' + node.Userid, function (msg) {
                    $('#lblRole').html(msg);
                });
                $('#dd3').dialog('open');
            } else {
                $.messager.alert('系统提示', '请选择要浏览的用户', 'error');
            }
        }
        function Upd1() {
            var node = $('#tt').datagrid('getSelected');
            $('#newPass').val('');
            if (node) {
                $('#hdId').val(node.Userid);
                $('#oldPass').val(node.Pass);
                $('#dd2').dialog('open');
            } else {
                $.messager.alert('系统提示', '请选择要编辑的用户', 'error');
            }
        }
        function Role1() {
            var selected = $('#tt').datagrid('getSelected');
            if (selected) {
                $('#hdId').val(selected.Userid);
                $('#tt3').tree({
                    checkbox: true,
                    url: 'ashx/treeHandler.ashx?Id=' + selected.Userid,
                    onClick: function (node) {
                        $(this).tree('toggle', node.target);
                    },
                    onContextMenu: function (e, node) {
                        e.preventDefault();
                        $('#tt3').tree('select', node.target);
                        $('#mm').menu('show', {
                            left: e.pageX,
                            top: e.pageY
                        });
                    }
                });
                $('#dd4').dialog('open');
            } else {
                $.messager.alert('系统提示', '请选择用户', 'error');
            }
        }
        function saveRole() {
            var nodes = $('#tt3').tree('getChecked');
            var s = '';
            for (var i = 0; i < nodes.length; i++) {
                if (s != '') s += ',';
                s += nodes[i].id;
            }
            var selected = $('#tt').datagrid('getSelected');
            $.post('ashx/UsersHandler.ashx?type=role&UserId=' + selected.Userid + '&roleId=' + s, function (msg) {
                $.messager.alert('系统提示', '分配角色成功', 'info');
                close4();
            });
        }
        function savePass() {
            var selected = $('#tt').datagrid('getSelected');
            var pass = $('#newPass').val();
            var pass2 = $('#newPass2').val();
            if (pass == '') {
                $.messager.alert('系统提示', '请输入新密码', 'error');
            } else {
                if (pass == pass2) {
                    $.post('ashx/UsersHandler.ashx?type=pass&UserId=' + selected.Userid + '&pass=' + pass, function (msg) {
                        if (msg == "true") {
                            $.messager.alert('系统提示', '密码修改成功,新密码为:' + pass, 'info');
                            close2();
                        } else {
                            $.messager.alert('系统提示', '密码修改失败，请稍后重试', 'info');
                            close2();
                        }
                    });
                } else {
                    $.messager.alert('系统提示', '两次输入密码不一致，请重新输入', 'error');
                    $('#newPass2').val('');
                    $('#newPass').val('');
                }
            }
        }
    </script>
</head>
<body style="background-color: White;">
    <input id="hdId" type="hidden" />
    <div style="height: 100%">
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td width="204px" valign="top" height="100%">
                    <table id="tableRole" cellpadding="0" cellspacing="1px" border="0" style="width: 200px;
                        height: 100%;" bgcolor="b5d6e6">
                        <tr style="height: 26px; background-color: #E0ECFF;">
                            <td style="padding-left: 5px">
                                <span class="icon icon-role" onclick="ss(this)">&nbsp;</span>组织机构
                            </td>
                        </tr>
                        <tr style="background-color: white">
                            <td valign="top" style="padding: 5px">
                                <div id="content">
                                    <ul id="tt2">
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <div class="btabs">
                        <a href="javascript:void(0)" onclick="Upd1()"><span class="icon icon-Pass">&nbsp;</span>修改密码</a>
                    </div>
                    <div style="height: 2px">
                    </div>
                    <div id="div1" style="width: 100%;">
                        <table id="tt">
                        </table>
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <form action="ashx/UsersHandler.ashx?type=save" id="form1">
    <div id="dd" icon="icon-save" style="padding: 5px; width: 360px; height: 460px;">
        <table cellpadding="0" cellspacing="1px" border="0" style="width: 100%;" bgcolor="b5d6e6">
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    账号:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="txtLogin" type="text" style="border: 1px solid #8DB2E3; width: 200px;
                        height: 20px" />*
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    密码:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="txtPass" type="password" style="border: 1px solid #8DB2E3; width: 200px;
                        height: 20px" />*
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    密码确认:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="txtPass2" type="password" style="border: 1px solid #8DB2E3; width: 200px;
                        height: 20px" />*
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    姓名:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="txtName" type="text" style="border: 1px solid #8DB2E3; width: 200px; height: 20px" />*
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right">
                    性别:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="Radio1" name="rd" type="radio" value="1" />男&nbsp;<input id="Radio2" name="rd"
                        type="radio" value="2" />女
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    所属部门:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="cc" class="easyui-combotree" url="ashx/OrglistHandler.ashx" required="true" value="1"
                        style="width: 200px;">*
                </td>
            </tr>
             <tr style="background-color: White; height: 26px;">
                <td align="right">
                    角色:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                     <select id="Role" name="dept" style="width: 200px;"></select>*
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    Email:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="txtMail" type="text" style="border: 1px solid #8DB2E3; width: 200px; height: 20px" />
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    电话:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="txtTel" type="text" style="border: 1px solid #8DB2E3; width: 200px; height: 20px" />
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    手机:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="txtMobile" type="text" style="border: 1px solid #8DB2E3; width: 200px;
                        height: 20px" />
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    状态:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="Radio3" name="rdStatus" type="radio" value="1" />启用&nbsp;
                    <input id="Radio4" name="rdStatus" type="radio" value="0" />禁用
                    <input id="Radio5" name="rdStatus" type="radio" value="2" />禁止登录
                </td>
            </tr>
        </table>
        <div region="south" border="false" style="text-align: center; height: 30px; line-height: 30px;">
            <a id="A1" class="easyui-linkbutton" onclick="Save()" icon="icon-ok" href="javascript:void(0)">
                确定</a> <a id="A2" class="easyui-linkbutton" onclick="close1()" icon="icon-cancel"
                    href="javascript:void(0)">取消</a>
        </div>
    </div>
    </form>
    <div id="dd3" icon="icon-save" style="padding: 5px; width: 360px; height: 350px;">
        <table cellpadding="0" cellspacing="1px" border="0" style="width: 100%;" bgcolor="b5d6e6">
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    账号:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblLogin">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    密码:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblPass">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    姓名:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblName">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right">
                    性别:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblSex">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    所属部门:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblOrg">
                    </label>
                </td>
            </tr>
              <tr style="background-color: White; height: 26px;">
                <td align="right">
                    角色:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblRole">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    Email:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblMail">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    电话:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblTel">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    手机:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblMobile">
                    </label>
                </td>
            </tr>
            <tr style="background-color: White; height: 26px;">
                <td align="right">
                    状态:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <label id="lblStatus">
                    </label>
                </td>
            </tr>
        </table>
    </div>
    <div id="dd2" icon="icon-save" style="padding: 5px; width: 360px; height: 177px;">
        <table cellpadding="0" cellspacing="1px" border="0" style="width: 100%;" bgcolor="b5d6e6">
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    旧密码:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="oldPass" type="text" readonly="readonly" style="border: 1px solid #8DB2E3;
                        width: 200px; height: 20px" />
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    新密码:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="newPass" type="password" style="border: 1px solid #8DB2E3; width: 200px;
                        height: 20px" />
                </td>
            </tr>
            <tr style="background-color: White; height: 32px;">
                <td align="right" style="width: 80px;">
                    密码确认:&nbsp;&nbsp;
                </td>
                <td style="padding: 5px">
                    <input id="newPass2" type="password" style="border: 1px solid #8DB2E3; width: 200px;
                        height: 20px" />
                </td>
            </tr>
        </table>
        <div region="south" border="false" style="text-align: center; height: 30px; line-height: 30px;">
            <a id="A3" class="easyui-linkbutton" onclick="savePass()" icon="icon-ok" href="javascript:void(0)">
                确定</a> <a id="A4" class="easyui-linkbutton" onclick="close2()" icon="icon-cancel"
                    href="javascript:void(0)">取消</a>
        </div>
    </div>
    <div id="dd4" icon="icon-save" style="padding: 5px; width: 260px; height: 250px;">
        <div style="height: 180px; overflow: hidden; width: 200px">
            <ul id="tt3">
            </ul>
        </div>
        <div region="south" border="false" style="text-align: center; height: 30px; line-height: 30px;">
            <a id="A5" class="easyui-linkbutton" onclick="saveRole()" icon="icon-ok" href="javascript:void(0)">
                确定</a> <a id="A6" class="easyui-linkbutton" onclick="close4()" icon="icon-cancel"
                    href="javascript:void(0)">取消</a>
        </div>
    </div>
</body>
</html>
