﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL.Restel;
using XiWan.DALFactory;
using XiWan.Model.Entities;

namespace XiWan.Restel
{
    /// <summary>
    /// Restel报价接口
    /// </summary>
    public class GetRestelRoomTypePrice : IHttpHandler
    {
        List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
        string RestelUrl = CCommon.GetWebConfigValue("RestelUrl");
        string RestelUsername = CCommon.GetWebConfigValue("RestelUsername");
        string RestelPassword = CCommon.GetWebConfigValue("RestelPassword");
        string vCodusu = CCommon.GetWebConfigValue("vCodusu");
        string vSecacc = CCommon.GetWebConfigValue("vSecacc");
        string vAfiliacio = CCommon.GetWebConfigValue("vAfiliacio");
        string vCodigousu = CCommon.GetWebConfigValue("vCodigousu");
        string vClausu = CCommon.GetWebConfigValue("vClausu");
        string RestelCacheTime = CCommon.GetWebConfigValue("RestelCacheTime");
        public void ProcessRequest(HttpContext context)
        {
            string result = string.Empty;
            string reqData = context.Request["data"] ?? "";
            context.Response.ContentType = "application/json";
            string Identidad = "codusu=" + vCodusu + "&secacc=" + vSecacc + "&afiliacio=" + vAfiliacio + "&codigousu=" + vCodigousu + "&clausu=" + vClausu;
            /*string hotelCode = context.Request["hotelId"] ?? "";
            string CheckInDate = context.Request["arrivalDate"] ?? "";
            string CheckOutDate = context.Request["departureDate"] ?? "";
            string postOccupancy = context.Request["occupancy"] ?? "";
            int RoomCount = (context.Request["roomNum"] ?? "").AsTargetType<int>(1);
            //解析json
            IntOccupancyInfo occupancy = new IntOccupancyInfo();
            occupancy = Newtonsoft.Json.JsonConvert.DeserializeObject<IntOccupancyInfo>(postOccupancy, new JsonSerializerSettings
            {
                Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                {
                    args.ErrorContext.Handled = true;
                }
            });
            string RateplanId = context.Request["RateplanId"] ?? "";*/
            if (reqData == "")
            {
                result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            }
            else
            {
                Request request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(reqData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (request == null)
                {
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
                else
                {
                    result = RestelBLL.Instance.GetRestelRoomTypeOutPrice(request.hotelId, Convert.ToDateTime(request.arrivalDate).ToString("MM/dd/yyyy"), Convert.ToDateTime(request.departureDate).ToString("MM/dd/yyyy"), request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                }
            } 
            context.Response.Write(result);     
        }
        public class Request
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}