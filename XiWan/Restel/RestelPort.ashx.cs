﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.DALFactory;
using XiWan.BLL;
using XiWan.BLL.Restel;
using XiWan.Model.Entities;

namespace XiWan.Restel
{
    /// <summary>
    /// RestelPort 的摘要说明
    /// </summary>
    public class RestelPort : IHttpHandler
    {
        string RestelUrl = CCommon.GetWebConfigValue("RestelUrl");
        string RestelUsername = CCommon.GetWebConfigValue("RestelUsername");
        string RestelPassword = CCommon.GetWebConfigValue("RestelPassword");
        string vCodusu = CCommon.GetWebConfigValue("vCodusu");
        string vSecacc = CCommon.GetWebConfigValue("vSecacc");
        string vAfiliacio = CCommon.GetWebConfigValue("vAfiliacio");
        string vCodigousu = CCommon.GetWebConfigValue("vCodigousu");
        string vClausu = CCommon.GetWebConfigValue("vClausu");
        string RestelCacheTime = CCommon.GetWebConfigValue("RestelCacheTime");
        string BookingNumber = string.Empty;
        public void ProcessRequest(HttpContext context)
        {
            string Identidad = "codusu=" + vCodusu + "&secacc=" + vSecacc + "&afiliacio=" + vAfiliacio + "&codigousu=" + vCodigousu + "&clausu=" + vClausu;
            string result = string.Empty;
            int input_page = Convert.ToInt16(context.Request["page"] ?? "0");
            int input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
            string countryCode = context.Request["countryCode"] ?? "";
            string ProvinceCode = context.Request["ProvinceCode"] ?? "";
            context.Response.ContentType = "application/json";
            if (!string.IsNullOrEmpty(context.Request["type"]))
            {
                switch (context.Request["type"])
                {
                    //获取国家
                    case "GetRestelCountry":
                        RestelBLL.Instance.GetRestelCountry();
                        context.Response.Write("成功");
                        break;
                    //获取省份
                    case "GetProvinces":
                        RestelBLL.Instance.GetProvinces();
                        context.Response.Write("成功");
                        break;
                    //获取省份
                    case "GetTown":
                        RestelBLL.Instance.GetTown();
                        context.Response.Write("成功");
                        break;
                    //获取酒店（从接口）
                    case "GetRestelHotel":
                        RestelBLL.Instance.GetHotel();
                        context.Response.Write("成功");
                        break;
                    //获取国籍
                    case "GetRestelNationality":
                        context.Response.ContentType = "application/json";
                        result = RestelBLL.Instance.GetRestelnationality();
                        context.Response.Write(result);
                        break;
                    //获取目的地
                    case "GetRestelDestination":
                        context.Response.ContentType = "application/json";
                        string strName = context.Request["paramName"].ToString();
                        if (strName != "")
                            result = RestelBLL.Instance.GetResteldestination(strName);
                        context.Response.Write(result);
                        break;
                    //酒店信息
                    case "Get_HotelInformation":
                        string _HotelName = context.Request["hotelName"] ?? "";
                        string _ddlGreadeCode = context.Request["ddlGreadeCode"] ?? "0";
                        string destination = context.Request["destination"] ?? "";
                        if (destination != "")
                        {
                            string[] ArrayDestination = destination.Split(',');
                            ProvinceCode = ArrayDestination[1];
                        }
                        string sqlWhere = " 1=1 ";
                        if ( ProvinceCode != "")
                        {
                            if (ProvinceCode != "")
                                sqlWhere += string.Format(@" and ProvinceCode='{0}'", ProvinceCode);
                        }
                        else
                        {
                            sqlWhere += "and 1=0";
                        }
                        if (_HotelName != "")
                        {
                            sqlWhere += string.Format(@" and HotelName like '%{0}%'", _HotelName);
                        }
                        if (_ddlGreadeCode != "0")
                        {
                            sqlWhere += string.Format(@" and Staring={0}", _ddlGreadeCode);
                        }
                        int totalCount = 0;
                        result = RestelBLL.Instance.GetHotelBySql("RestelHotel", "*", "HotelName", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                        break;
                    //获取报价
                    case "GetRealHotelSQuote":
                        context.Response.ContentType = "application/json";
                        string hotelCode = context.Request["hotelCode"] ?? "";
                        string CheckInDate = context.Request["CheckInDate"] ?? "";
                        string CheckOutDate = context.Request["CheckOutDate"] ?? "";
                        string Rooms = context.Request["Rooms"] ?? "";
                        string nationality = context.Request["nationality"] ?? "";
                        //GetRealHotelSQuote 查询报价  GetRoomType 获取房型
                        result = RestelBLL.Instance.GetRoomType(hotelCode, nationality, Rooms, Convert.ToDateTime(CheckInDate).ToString("MM/dd/yyyy"), Convert.ToDateTime(CheckOutDate).ToString("MM/dd/yyyy"), RestelCacheTime);
                        context.Response.Write(result);
                        break;
                    //获取取消期限
                    case "Get_CancellationPolicy":
                        context.Response.ContentType = "application/json";
                        string HotelPriceIDs = context.Request["HotelPriceID"] ?? "";
                        result = RestelBLL.Instance.GetCancellationPolicy(HotelPriceIDs);
                        context.Response.Write(result);
                        break;
                    //预订
                    case "RestelReserva":
                        context.Response.ContentType = "application/json";
                        HotelPriceIDs = context.Request["HotelPriceID"] ?? "";
                        result = RestelBLL.Instance.Reserva(HotelPriceIDs);
                        context.Response.Write(result);
                        break;
                    //预订确认 或 取消预订
                    case "RestelReservaConfirmation":
                        context.Response.ContentType = "application/json";
                        BookingNumber = context.Request["BookingCode"] ?? "";
                        string ReservationType = context.Request["ReservationType"] ?? "";
                        result = RestelBLL.Instance.ReservaConfirmation(BookingNumber, ReservationType);
                        context.Response.Write(result);
                        break;
                    //取消订单
                    case "RestelCancellation":
                        context.Response.ContentType = "application/json";
                        BookingNumber = context.Request["BookingCode"] ?? "";
                        result = RestelBLL.Instance.ReservaCancellation( BookingNumber);
                        context.Response.Write(result);
                        break;
                    //查看订单
                    case "RestelCheckingOrder":
                        context.Response.ContentType = "application/json";
                        BookingNumber = context.Request["BookingCode"] ?? "";
                        result = RestelBLL.Instance.ReservationInformation(BookingNumber);
                        context.Response.Write(result);
                        break;
                    //查看订单（本地数据库）
                    case "RestelBooking":
                        context.Response.ContentType = "application/json";
                        totalCount = 0;
                        sqlWhere = " 1=1 ";
                        string _BookingCode = context.Request["BookingNumber"] ?? "";
                        string InDate = context.Request["InDate"] ?? "";
                        string InNum = context.Request["InNum"] ?? "";
                        string Status = context.Request["Status"] ?? "";
                        if (_BookingCode != "")
                        {
                            sqlWhere = sqlWhere + string.Format("AND BookingCode='{0}' ", _BookingCode);
                        }
                        if (InNum != "")
                        {
                            sqlWhere = sqlWhere + string.Format("AND InOrderNum='{0}' ", InNum);
                        }
                        if (InDate != "")
                        {
                            sqlWhere = sqlWhere + string.Format("AND CONVERT(varchar(100), CreatTime, 23)='{0}' ", InDate);
                        }
                        if (Status != "0")
                        {
                            if (Status == "1")
                            {
                                sqlWhere = sqlWhere + "AND LegID <> 99 ";
                            }
                            else if (Status == "2")
                            {
                                sqlWhere = sqlWhere + "AND LegID='99' ";
                            }
                        }
                        result = RestelBLL.Instance.GetBookingBySql("Restel_Order ", " * ", " Id ", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                        break;
                    //查看订单
                    case "RestlLastReservations":
                        context.Response.ContentType = "application/json";
                        string seachMode = context.Request["seachMode"] ?? "";
                        string searchDate = context.Request["searchDate"] ?? "";
                        string searchHotelName = context.Request["searchHotelName"] ?? "";
                        string searchCustomerName = context.Request["searchCustomerName"] ?? "";
                        BookingNumber = context.Request["BookingNumber"] ?? "";
                        result = RestelBLL.Instance.RestlLastReservations(seachMode, BookingNumber,searchDate,searchHotelName,searchCustomerName);
                        context.Response.Write(result);
                        break;
                    //查看预订后取消生成的费用
                    case "GetCancellationFees":
                        context.Response.ContentType = "application/json";
                        BookingNumber = context.Request["BookingCode"] ?? "";
                        result = RestelBLL.Instance.GetCancellationFees(BookingNumber);
                        context.Response.Write(result);
                        break;
                    //查询酒店评论
                    case "GetHotelRemarks":
                        context.Response.ContentType = "application/json";
                        string HotelCode = context.Request["HotelCode"] ?? "";
                        CheckInDate = context.Request["CheckInDate"] ?? "";
                        CheckOutDate = context.Request["CheckOutDate"] ?? "";
                        result = RestelBLL.Instance.GetHotelRemarks(HotelCode, Convert.ToDateTime(CheckInDate).ToString("dd-MM-yyyy"), Convert.ToDateTime(CheckOutDate).ToString("dd-MM-yyyy"));
                        context.Response.Write(result);
                        break;
                    //获取报价（供外部调用）
                    case "GetRestelRoomTypeOutPrice":
                        context.Response.ContentType = "application/json";
                        hotelCode = context.Request["hotelCode"] ?? "";
                        CheckInDate = context.Request["CheckInDate"] ?? "";
                        CheckOutDate = context.Request["CheckOutDate"] ?? "";
                        Rooms = context.Request["Rooms"] ?? "";
                        string[] ArrayChilds;
                        IntOccupancyInfo occupancy = new IntOccupancyInfo();
                        string Age = "0";
                        ArrayChilds = Rooms.Split(';')[0].Split(',');
                        if (ArrayChilds[1] == "1")
                        {
                            Age += string.Format(@"{0}", ArrayChilds[2]);
                        }
                        else if (ArrayChilds[1] == "2")
                        {
                            Age += string.Format(@"{0},{1}", ArrayChilds[2], ArrayChilds[3]);
                        }
                        occupancy.adults = ArrayChilds[0].AsTargetType<int>(0);
                        occupancy.children = ArrayChilds[1].AsTargetType<int>(0);
                        occupancy.childAges = Age;
                        int RoomCount=ArrayChilds[4].AsTargetType<int>(0);
                        result = RestelBLL.Instance.GetRestelRoomTypeOutPrice( hotelCode, Convert.ToDateTime(CheckInDate).ToString("MM/dd/yyyy"), Convert.ToDateTime(CheckOutDate).ToString("MM/dd/yyyy"), occupancy, RoomCount,"");
                        context.Response.Write(result);
                        break;   
                    //获取可卖酒店
                    case "GetSaleableHotel":
                        RestelBLL.Instance.GetSaleableHotel();
                        context.Response.Write("成功");
                    break;
                    //获取补充服务17，补充可卖酒店信息
                    case "GetSaleableHotelInformation":
                        RestelBLL.Instance.GetSaleableHotelInformation();
                        context.Response.Write("成功");
                    break;
                }
            }
            context.Response.End();
            
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}