﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.BLL.Restel;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;

namespace XiWan.Restel
{
    /// <summary>
    /// Restel 的摘要说明
    /// </summary>
    public class Restel : IHttpHandler
    {
        List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
        
        string BookingNumber = string.Empty;
        string OrderNum = string.Empty;//订单号
        string HotelID = string.Empty;
        public void ProcessRequest(HttpContext context)
        {
            Respone resp = new Respone();
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string reqData = context.Request["data"] ?? "";
            string result = string.Empty;
            ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
            //LogHelper.DoOperateLog(string.Format("Studio：Restel(平台请求) ,请求数据 :{0} ,  Time:{1} ", "type:" + type + ";data:" + reqData, DateTime.Now.ToString()), "Restel接口(平台请求)");
            switch (type)
            {
                //获取报价
                case "GetRoomTypePrice":
                    string strRateplanId = string.Empty;
                    if (reqData == "")
                    {
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    else
                    {
                        PriceRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            strRateplanId = request.rateplanId.Trim();
                            result = RestelBLL.Instance.GetRestelRoomTypeOutPrice(request.hotelId, Convert.ToDateTime(request.arrivalDate).ToString("MM/dd/yyyy"), Convert.ToDateTime(request.departureDate).ToString("MM/dd/yyyy"), request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                        }
                    }
                    if (strRateplanId != null && strRateplanId != "")
                    {
                        //string HostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;
                        LogHelper.DoOperateLog(string.Format("Studio：Restel(平台请求)获取报价 ,请求数据 :{0} , 返回数据：{1} ，Time:{2} ", type + reqData, result, DateTime.Now.ToString()), "Restel接口(平台请求)");
                    }
                    context.Response.Write(result);
                    break;
                //创建订单
                case "Create_Order":
                    if (reqData == "")
                    {
                        resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        CreateOrderRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateOrderRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else if (request.inOrderNum=="")
                        {
                            resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败,订单号不能为空" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else
                        {
                            int IsClose = 1;//预订成功时不能重复提交
                            string sql = string.Format("SELECT * FROM Restel_Order where InOrderNum = '{0}' and HotelID='{1}'", request.inOrderNum, request.hotelId);
                            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                            if (dt.Rows.Count > 0)
                            {
                                IsClose = dt.Rows[0]["IsClose"].AsTargetType<int>(0);
                            }
                            if (dt.Rows.Count > 0 && IsClose == 1)
                            {
                                resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败,该订单号已存在" };
                                result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                            }
                            else
                            {
                                string userHostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                                string BookingReference = CCommon.GuidToLongID();//生成订单id
                                Restel_Order booking = new Restel_Order();
                                //保存订单到数据库
                                booking.InOrderNum = request.inOrderNum;
                                booking.AgentBookingReference = request.inOrderNum;
                                booking.Request = reqData;
                                booking.HotelId = request.hotelId;
                                booking.Platform = "Restel";
                                booking.RoomCount = request.roomNum;
                                booking.SellingPrice = request.TotalMoney;
                                booking.RatePlanId = request.ratePlanId;
                                booking.CheckInDate = request.arrivalDate.AsTargetType<DateTime>(DateTime.MinValue);
                                booking.CheckOutDate = request.departureDate.AsTargetType<DateTime>(DateTime.MinValue);
                                booking.IsClose = 1;
                                booking.CreatTime = DateTime.Now;
                                booking.CreatTime = DateTime.Now;
                                booking.UpdateTime = DateTime.Now;
                                booking.CreatIP = userHostAddress;
                                booking.EntityState = e_EntityState.Added;
                                if (dt.Rows.Count == 0)
                                {
                                    con.Save(booking);
                                }
                                else 
                                {
                                    string strUpdateSql = string.Format(@"update Restel_Order set IsClose=1,UpdateTime=GETDATE()  where InOrderNum='{0}' and HotelId='{1}' ", request.inOrderNum, request.hotelId);
                                    con.Update(strUpdateSql);
                                }
                                result = RestelBLL.Instance.Create_Order(request.hotelId, request.ratePlanId, request.customers, request.inOrderNum, Convert.ToDateTime(request.arrivalDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(request.departureDate).ToString("yyyy-MM-dd"), request.roomNum, request.TotalMoney, BookingReference);
                            }
                        }
                    }
                    //string userHostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                    LogHelper.DoOperateLog(string.Format("Studio：Restel(平台请求)创建订单,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + reqData, result, DateTime.Now.ToString()), "Restel接口(平台请求预订)");
                    context.Response.Write(result);
                    break;
                //取消订单
                case "Cancel_Order":
                    HotelID = context.Request["hotelID"] ?? "";
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "" || OrderNum == "null")
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = RestelBLL.Instance.Cancel_Order(HotelID, OrderNum);
                    }
                    string hostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                    LogHelper.DoOperateLog(string.Format("Studio：Restel(平台请求)取消订单 ,请求数据 :{0} , 返回数据：{1}，取消订单ip：{2} ， Time:{3} ", type + ",hotelID:" + HotelID + ",OrderNum:" + OrderNum, result, hostAddress, DateTime.Now.ToString()), "Restel接口(平台请求预订)");
                    context.Response.Write(result);
                    break;
                //获取订单明细
                case "Get_OrderDetail":
                    HotelID = context.Request["hotelID"] ?? "";
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "" || OrderNum == "null")
                    {
                        resp = new Respone() { code = "99", mes = "获取订单明细失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = RestelBLL.Instance.Get_OrderDetail(HotelID, OrderNum);
                    }
                    LogHelper.DoOperateLog(string.Format("Studio：Restel(平台请求)获取订单明细 ,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + ",hotelID:" + HotelID + ",OrderNum:" + OrderNum, result, DateTime.Now.ToString()), "Restel接口(平台请求)");
                    context.Response.Write(result);
                    break;
            }
        }
        public class PriceRequest
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public class CreateOrderRequest
        {
            public string inOrderNum { get; set; }//传入订单号
            public string hotelId { get; set; }//酒店id
            public string roomTypeId { get; set; }//房型id
            public string ratePlanId { get; set; }//房型价格计划ID
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public int roomNum { get; set; }//房间数
            public List<roomCustomers> customers { get; set; }//每个房间入住客户信息
            public decimal TotalMoney { get; set; }//订单总金额卖价

        }
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string Status { get; set; }
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}