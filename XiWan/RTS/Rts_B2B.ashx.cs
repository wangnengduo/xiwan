﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL;
using XiWan.DALFactory;
using XiWan.Model.Entities;

namespace XiWan.RTS
{
    /// <summary>
    /// Rts_B2B 的摘要说明
    /// </summary>
    public class Rts_B2B : IHttpHandler
    {

        List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelUrl");
        string RtsBookingsUrl = CCommon.GetWebConfigValue("RtsBookingsUrl");
        string RtsBookingDetailsUrl = CCommon.GetWebConfigValue("RtsBookingDetailsUrl");
        string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCode");
        string RtsPassword = CCommon.GetWebConfigValue("RtsPassword");
        string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCode");
        string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCode");
        string RtsSalesUserNo = CCommon.GetWebConfigValue("RtsSalesUserNo");
        string OrderNum = string.Empty;//订单号
        public void ProcessRequest(HttpContext context)
        {
            Respone resp = new Respone();
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string reqData = context.Request["data"] ?? "";
            string result = string.Empty;
            switch (type)
            {
                //获取报价
                case "GetRtsRoomTypePrice":
                    IntOccupancyInfo ioi=new Model.Entities.IntOccupancyInfo();
                    ioi.adults = 1;
                    ioi.children = 0;
                    ioi.childAges = "";
                    PriceRequest req = new PriceRequest();
                    req.hotelId = context.Request["ItemCode"];
                    req.arrivalDate = context.Request["beginTime"];
                    req.departureDate = context.Request["endTime"];
                    req.IntOccupancyInfo = ioi;
                    req.rateplanId = "725e90b7-dd1b-4151-a4e0-76da8b21c7d8";
                    req.roomNum = context.Request["Room1Count"].AsTargetType<int>(0);
                    result = RtsBLL.Instance.GetRtsRoomTypeOutPrice(RtsHotelUrl, RtsSiteCode, RtsPassword, req.arrivalDate, req.departureDate, req.hotelId, req.IntOccupancyInfo, req.roomNum, req.rateplanId);
                    
                    /*if (reqData == "")
                    {
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    else
                    {
                        PriceRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            result = RtsBLL.Instance.GetRtsRoomTypeOutPrice(RtsHotelUrl, RtsSiteCode, RtsPassword, request.arrivalDate, request.departureDate, request.hotelId, request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                        }
                    }*/
                    context.Response.Write(result);
                    break;
                //创建订单
                case "Create_RtsOrder":
                    List<roomCustomers> roomCustomers = new List<roomCustomers>();

                    roomCustomers roomCustomer = new roomCustomers();
                    List<Customer> customers = new List<Customer>();
                    Customer tppObj1 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18, sex = 1 };
                    //Customer tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                    //Customer tppObj3 = new Customer() { firstName = "Ba", lastName = "Liang", name = "Ba Liang", age = 20, sex = 1 };

                    //Customer tppObj3 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 10, sex = 1 };
                    //Customer tppObj4 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 12, sex = 1 };
                    customers.Add(tppObj1);
                    //customers.Add(tppObj2);
                    //customers.Add(tppObj3);

                    //customers.Add(tppObj3);
                    //customers.Add(tppObj4);
                    roomCustomer.Customers = customers;
                    roomCustomers.Add(roomCustomer);

                    roomCustomers roomCustomer2 = new roomCustomers();
                    List<Customer> customers2 = new List<Customer>();
                    // tppObj12 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18, sex = 1 };
                    //Customer tppObj22 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                    //Customer tppObj32 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };

                    Customer tppObj32 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };
                    //Customer tppObj42 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 21, sex = 1 };
                    // tppObj52 = new Customer() { firstName = "Jiu", lastName = "Liu", name = "Jiu Liu", age = 20, sex = 1 };
                    //.Add(tppObj12);
                    //customers.Add(tppObj22);
                    //customers.Add(tppObj32);

                    customers2.Add(tppObj32);
                    //customers2.Add(tppObj42);
                    //customers2.Add(tppObj52);
                    roomCustomer2.Customers = customers2;
                    roomCustomers.Add(roomCustomer2);
                    result = RtsBLL.Instance.Create_Order(RtsBookingsUrl, RtsSiteCode, RtsPassword, RtsSalesCompCode, RtsSalesSiteCode, RtsSalesUserNo, "5366793651480670732", "725e90b7-dd1b-4151-a4e0-76da8b21c7d8", roomCustomers, "", "", "", 0);
                    /*
                    if (reqData == "")
                    {
                        resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        CreateOrderRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateOrderRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else
                        {
                            result = RtsBLL.Instance.Create_Order(RtsBookingsUrl, RtsSiteCode, RtsPassword, RtsSalesCompCode, RtsSalesSiteCode, RtsSalesUserNo, request.roomTypeId, request.ratePlanId, request.customers,request.inOrderNum, Convert.ToDateTime(request.arrivalDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(request.departureDate).ToString("yyyy-MM-dd"), request.roomNum);
                        }
                    }*/
                    context.Response.Write(result);
                    break;
                //取消订单
                case "Cancel_RtsOrder":
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "")
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = RtsBLL.Instance.Cancel_Order(RtsBookingsUrl, RtsSiteCode, RtsPassword, OrderNum, RtsSalesUserNo);
                    }
                    context.Response.Write(result);
                    break;
                //获取订单明细
                case "Get_RtsOrderDetail":
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if ( OrderNum == "")
                    {
                        resp = new Respone() { code = "99", mes = "获取订单明细失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = RtsBLL.Instance.Get_OrderDetail(RtsBookingDetailsUrl, RtsSiteCode, RtsPassword, OrderNum);                   
                    }
                    context.Response.Write(result);
                    break;
            }
        }
        public class PriceRequest
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public class CreateOrderRequest
        {
            public string inOrderNum { get; set; }//传入订单号
            public string hotelId { get; set; }//酒店id
            public string roomTypeId { get; set; }//房型id
            public string ratePlanId { get; set; }//房型价格计划ID
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public int roomNum { get; set; }//房间数
            public List<roomCustomers> customers { get; set; }//入住客户信息
        }
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}