﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL;
using XiWan.DALFactory;
using XiWan.Model.Entities;

namespace XiWan.RTS_B2C
{
    /// <summary>
    /// Rts(B2C)报价接口
    /// </summary>
    public class GetRtsB2CRoomTypePrice : IHttpHandler
    {
        List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
        string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelB2CUrl");
        string RtsBookingsUrl = CCommon.GetWebConfigValue("RtsBookingsB2CUrl");
        string RtsBookingDetailsUrl = CCommon.GetWebConfigValue("RtsBookingDetailsB2CUrl");
        string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCodeB2C");
        string RtsPassword = CCommon.GetWebConfigValue("RtsPasswordB2C");
        string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCodeB2C");
        string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCodeB2C");
        string RtsSalesUserNo = CCommon.GetWebConfigValue("RtsSalesUserNoB2C");
        public void ProcessRequest(HttpContext context)
        {
            /*IntOccupancyInfo occupancy = new IntOccupancyInfo();
            string _ItemCode = context.Request["hotelId"] ?? "";
            string _beginTime = context.Request["arrivalDate"] ?? "";
            string _endTime = context.Request["departureDate"] ?? "";
            string postOccupancy = context.Request["occupancy"] ?? "";
            int RoomCount = (context.Request["roomNum"] ?? "").AsTargetType<int>(1);
            //解析json
            occupancy = Newtonsoft.Json.JsonConvert.DeserializeObject<IntOccupancyInfo>(postOccupancy, new JsonSerializerSettings
            {
                Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                {
                    args.ErrorContext.Handled = true;
                }
            });
            string RateplanId = context.Request["RateplanId"] ?? "";*/
            string reqData = context.Request["data"] ?? "";
            string result = string.Empty;
            context.Response.ContentType = "application/json";
            if (reqData == "")
            {
                result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            }
            else
            {
                Request request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(reqData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (request == null)
                {
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
                else
                {
                    result = Rts_B2CBLL.Instance.GetRtsRoomTypeOutPrice(RtsHotelUrl, RtsSiteCode, RtsPassword, request.arrivalDate, request.departureDate, request.hotelId, request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                }
            }
            context.Response.Write(result);     
        }

        public class Request
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}