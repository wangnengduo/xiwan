﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL;
using XiWan.DALFactory;
using XiWan.Model.Entities;

namespace XiWan.RTS_B2C
{
    /// <summary>
    /// Rts_B2C 的摘要说明
    /// </summary>
    public class Rts_B2C : IHttpHandler
    {
        List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelB2CUrl");
        string RtsBookingsUrl = CCommon.GetWebConfigValue("RtsBookingsB2CUrl");
        string RtsBookingDetailsUrl = CCommon.GetWebConfigValue("RtsBookingDetailsB2CUrl");
        string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCodeB2C");
        string RtsPassword = CCommon.GetWebConfigValue("RtsPasswordB2C");
        string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCodeB2C");
        string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCodeB2C");
        string RtsSalesUserNo = CCommon.GetWebConfigValue("RtsSalesUserNoB2C");
        string OrderNum = string.Empty;
        public void ProcessRequest(HttpContext context)
        {
            Respone resp = new Respone();
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string reqData = context.Request["data"] ?? "";
            string result = string.Empty;
            switch (type)
            {
                //获取报价
                case "GetRtsRoomTypePrice":
                    /*IntOccupancyInfo ioi = new IntOccupancyInfo();
                    ioi.adults = 1;
                    ioi.children = 0;
                    ioi.childAges = "";
                    PriceRequest req = new PriceRequest();
                    req.hotelId = context.Request["ItemCode"];
                    req.arrivalDate = context.Request["beginTime"];
                    req.departureDate = context.Request["endTime"];
                    req.IntOccupancyInfo = ioi;
                    req.rateplanId = "5486774348528344582";
                    req.roomNum = context.Request["Room1Count"].AsTargetType<int>(0);
                    result = Rts_B2CBLL.Instance.GetRtsRoomTypeOutPrice(RtsHotelUrl, RtsSiteCode, RtsPassword, req.arrivalDate, req.departureDate, req.hotelId, req.IntOccupancyInfo, req.roomNum, req.rateplanId);
                    */
                    if (reqData == "")
                    {
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    else
                    {
                        PriceRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            result = Rts_B2CBLL.Instance.GetRtsRoomTypeOutPrice(RtsHotelUrl, RtsSiteCode, RtsPassword, request.arrivalDate, request.departureDate, request.hotelId, request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                        }
                    }
                    context.Response.Write(result);
                    break;
                //创建订单
                case "Create_RtsOrder":
                    /*List<Customer> customers = new List<Customer>();
                    Customer tppObj1 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 21 ,sex=1};
                    Customer tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 21, sex = 1 };
                    //Customer tppObj3 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 21, sex = 1 };
                    Customer tppObj4 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 7, sex = 1 };
                    customers.Add(tppObj1);
                    customers.Add(tppObj2);
                    //customers.Add(tppObj3);
                    customers.Add(tppObj4);
                    //result = Rts_B2CBLL.Instance.Create_Order(RtsBookingsUrl, RtsSiteCode, RtsPassword, RtsSalesCompCode, RtsSalesSiteCode, RtsSalesUserNo, "4405cbd8-bbbe-4c33-a7f6-f7163332e589", "5320013289891487613", customers);
                    */
                    if (reqData == "")
                    {
                        resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        CreateOrderRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateOrderRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else
                        {
                            result = Rts_B2CBLL.Instance.Create_Order(RtsBookingsUrl, RtsSiteCode, RtsPassword, RtsSalesCompCode, RtsSalesSiteCode, RtsSalesUserNo, request.roomTypeId, request.ratePlanId, request.customers, request.inOrderNum, Convert.ToDateTime(request.arrivalDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(request.departureDate).ToString("yyyy-MM-dd"), request.roomNum);
                        }
                    }
                    context.Response.Write(result);
                    break;
                //取消订单
                case "Cancel_RtsOrder":
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "")
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = Rts_B2CBLL.Instance.Cancel_Order(RtsBookingsUrl, RtsSiteCode, RtsPassword, OrderNum, RtsSalesUserNo);
                    }
                    context.Response.Write(result);
                    break;
                //获取订单明细
                case "Get_RtsOrderDetail":
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "")
                    {
                        resp = new Respone() { code = "99", mes = "获取订单明细失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = Rts_B2CBLL.Instance.Get_OrderDetail(RtsBookingDetailsUrl, RtsSiteCode, RtsPassword, OrderNum);
                    }
                    context.Response.Write(result);
                    break;
            }
        }
        public class PriceRequest
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public class CreateOrderRequest
        {
            public string inOrderNum { get; set; }//传入订单号
            public string hotelId { get; set; }//酒店id
            public string roomTypeId { get; set; }//房型id
            public string ratePlanId { get; set; }//房型价格计划ID
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public int roomNum { get; set; }//房间数
            public List<roomCustomers> customers { get; set; }//入住客户信息
        }
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}