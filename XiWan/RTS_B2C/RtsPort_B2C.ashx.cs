﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Data.Redis;
using XiWan.BLL;
using XiWan.Model.Entities;
using Newtonsoft.Json;


namespace XiWan.RTS_B2C
{
    /// <summary>
    /// RtsPort_B2C 的摘要说明
    /// </summary>
    public class RtsPort_B2C : IHttpHandler
    {

        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        int input_page = 0;
        int input_row = 0;
        string _HotelName = string.Empty;// 酒店名称
        string _beginTime = string.Empty;//入住时间
        string _endTime = string.Empty;//退房时间
        string _ddlGreadeCode = string.Empty;//酒店等級
        string _ddlLocationCode = string.Empty;//区域
        string _ddlRoomTypeCode1 = "0";//房间1类型
        string _Room1ChildAge1 = string.Empty;//房间1儿童岁数
        string _Room1ChildAge2 = string.Empty;//房间1儿童岁数
        string _Room1Count = string.Empty;//房间1房间数
        string _ddlRoomTypeCode2 = "0";//房间2类型
        string _Room2ChildAge1 = string.Empty;//房间2儿童岁数
        string _Room2ChildAge2 = string.Empty;//房间2儿童岁数
        string _Room2Count = string.Empty;//房间2房间数
        string _ddlRoomTypeCode3 = "0";//房间3类型
        string _Room3ChildAge1 = string.Empty;//房间3儿童岁数
        string _Room3ChildAge2 = string.Empty;//房间3儿童岁数
        string _Room3Count = string.Empty;//房间3房间数
        string _ddlRoomTypeCode4 = "0";//房间4类型
        string _Room4ChildAge1 = string.Empty;//房间4儿童岁数
        string _Room4ChildAge2 = string.Empty;//房间4儿童岁数
        string _Room4Count = string.Empty;//房间4房间数
        string _ddlRoomTypeCode5 = "0";//房间5类型
        string _Room5ChildAge1 = string.Empty;//房间5儿童岁数
        string _Room5ChildAge2 = string.Empty;//房间5儿童岁数
        string _Room5Count = string.Empty;//房间5房间数
        string _nationality = string.Empty;
        string destination = string.Empty;
        string _ItemCode = string.Empty;
        string _ItemNo = string.Empty;
        string _RoomTypeCode = string.Empty;
        string _BedTypeCode = string.Empty;
        string _BreakfastTypeName = string.Empty;
        string _BookingCode = string.Empty;
        string AgentReferenceNumber = string.Empty;
        string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelB2CUrl");
        string RtsBookingsUrl = CCommon.GetWebConfigValue("RtsBookingsB2CUrl");
        string RtsBookingDetailsUrl = CCommon.GetWebConfigValue("RtsBookingDetailsB2CUrl");
        string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCodeB2C");
        string RtsPassword = CCommon.GetWebConfigValue("RtsPasswordB2C");
        string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCodeB2C");
        string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCodeB2C");
        string RtsSalesUserNo = CCommon.GetWebConfigValue("RtsSalesUserNoB2C");
        public void ProcessRequest(HttpContext context)
        {
            string result = string.Empty;
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            if (type != "")
            {
                destination = context.Request["destination"] ?? "";
                input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                _ItemCode = context.Request["ItemCode"] ?? "";
                _nationality = context.Request["nationality"] ?? "";
                _HotelName = context.Request["HotelName"] ?? "";
                _beginTime = context.Request["beginTime"] ?? "";
                _endTime = context.Request["endTime"] ?? "";
                _ddlGreadeCode = context.Request["ddlGreadeCode"] ?? "0";
                _ItemNo = context.Request["ItemNo"] ?? "";
                _RoomTypeCode = context.Request["RoomTypeCode"] ?? "";
                _BedTypeCode = context.Request["BedTypeCode"] ?? "";
                _ddlLocationCode = context.Request["ddlLocationCode"] ?? "";
                _ddlRoomTypeCode1 = context.Request["ddlRoomTypeCode1"] ?? "0";
                _Room1ChildAge1 = context.Request["Room1ChildAge1"] ?? "";
                _Room1ChildAge2 = context.Request["Room1ChildAge2"] ?? "";
                _Room1Count = context.Request["Room1Count"] ?? "0";
                _ddlRoomTypeCode2 = context.Request["ddlRoomTypeCode2"] ?? "0";
                _Room2ChildAge1 = context.Request["Room2ChildAge1"] ?? "";
                _Room2ChildAge2 = context.Request["Room2ChildAge2"] ?? "";
                _Room2Count = context.Request["Room2Count"] ?? "0";
                _ddlRoomTypeCode3 = context.Request["ddlRoomTypeCode3"] ?? "0";
                _Room3ChildAge1 = context.Request["Room3ChildAge1"] ?? "";
                _Room3ChildAge2 = context.Request["Room3ChildAge2"] ?? "";
                _Room3Count = context.Request["Room3Count"] ?? "0";
                _ddlRoomTypeCode4 = context.Request["ddlRoomTypeCode4"] ?? "0";
                _Room4ChildAge1 = context.Request["Room4ChildAge2"] ?? "";
                _Room4ChildAge2 = context.Request["Room4ChildAge2"] ?? "";
                _Room4Count = context.Request["Room4Count"] ?? "0";
                _ddlRoomTypeCode5 = context.Request["ddlRoomTypeCode5"] ?? "0";
                _Room5ChildAge1 = context.Request["Room5ChildAge1"] ?? "";
                _Room5ChildAge2 = context.Request["Room5ChildAge2"] ?? "";
                _Room5Count = context.Request["Room5Count"] ?? "0";
                switch (type)
                {
                    //国籍
                    case "GetRTSnationality":
                        result = Rts_B2CBLL.Instance.GetRTSnationality();
                        context.Response.Write(result);
                        break;
                    //区域 
                    case "GetRTSLocationCode":
                        result = Rts_B2CBLL.Instance.GetRtsLocationCode(destination);
                        context.Response.Write(result);
                        break;
                    //获取目的地
                    case "GetRTSdestination":
                        string strName = context.Request["paramName"] ?? "";
                        if (strName != "")
                            result = Rts_B2CBLL.Instance.GetRTSdestination(strName);
                        context.Response.Write(result);
                        break;
                    //判断目的地是否数据库存在
                    case "GetRTScityCodeYn":
                        context.Response.ContentType = "text/plain";
                        result = Rts_B2CBLL.Instance.GetRTScityCodeYn(destination);
                        context.Response.Write(result);
                        break;
                    //获取酒店列表
                    case "Get_RTSHotel":
                        string sqlWhere = " 1=1 ";
                        if (destination != "")
                        {
                            sqlWhere += string.Format(@" and CityCode='{0}'", destination);
                        }
                        else
                        {
                            sqlWhere += "and 1=0";
                        }
                        if (_HotelName != "")
                        {
                            sqlWhere += string.Format(@" and ItemName like '%{0}%'", _HotelName);
                        }
                        if (_ddlGreadeCode != "0")
                        {
                            sqlWhere += string.Format(@" and ItemGradeCode={0}", _ddlGreadeCode);
                        }
                        int totalCount = 0;
                        result = Rts_B2CBLL.Instance.GetHotel("RtsHotel", "*", "ItemName", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                        break;
                    //获取房型报价列表
                    case "Get_RTSHotelInformation":
                        result = Rts_B2CBLL.Instance.Quote(RtsHotelUrl, RtsSiteCode, RtsPassword, _nationality, destination, _beginTime, _endTime, _ddlGreadeCode.AsTargetType<int>(0), _ddlLocationCode, RtsSalesCompCode, _HotelName, _ItemCode, _ddlRoomTypeCode1, _Room1Count.AsTargetType<int>(0), _Room1ChildAge1.AsTargetType<int>(0), _Room1ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode2, _Room2Count.AsTargetType<int>(0), _Room2ChildAge1.AsTargetType<int>(0), _Room2ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode3, _Room3Count.AsTargetType<int>(0), _Room3ChildAge1.AsTargetType<int>(0), _Room3ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode4, _Room4Count.AsTargetType<int>(0), _Room4ChildAge1.AsTargetType<int>(0), _Room4ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode5, _Room5Count.AsTargetType<int>(0), _Room5ChildAge1.AsTargetType<int>(0), _Room5ChildAge2.AsTargetType<int>(0), input_page, input_row);
                        context.Response.Write(result);
                        //result = GetRTSHotelInformation();
                        break;
                    //查看取消期间
                    case "GetRTSCancelDeadline":
                        result = Rts_B2CBLL.Instance.GetRTSCancelDeadline(RtsBookingsUrl, RtsSiteCode, RtsPassword, _ItemCode, _ItemNo, _RoomTypeCode, _beginTime, _endTime, _nationality, _BedTypeCode);
                        context.Response.Write(result);
                        break;
                    //查询备注
                    case "GetRemarkHotelInformationForCustomerCount":
                        result = Rts_B2CBLL.Instance.GetRemarkHotelInformationForCustomerCount(RtsHotelUrl, RtsSiteCode, RtsPassword, _nationality, destination, _beginTime, _endTime, _ddlGreadeCode.AsTargetType<int>(0), _ddlLocationCode, RtsSalesCompCode, _ItemCode, _ItemNo, _RoomTypeCode, _ddlRoomTypeCode1, _Room1Count.AsTargetType<int>(0), _Room1ChildAge1.AsTargetType<int>(0), _Room1ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode2, _Room2Count.AsTargetType<int>(0), _Room2ChildAge1.AsTargetType<int>(0), _Room2ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode3, _Room3Count.AsTargetType<int>(0), _Room3ChildAge1.AsTargetType<int>(0), _Room3ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode4, _Room4Count.AsTargetType<int>(0), _Room4ChildAge1.AsTargetType<int>(0), _Room4ChildAge2.AsTargetType<int>(0), _ddlRoomTypeCode5, _Room5Count.AsTargetType<int>(0), _Room5ChildAge1.AsTargetType<int>(0), _Room5ChildAge2.AsTargetType<int>(0));
                        context.Response.Write(result);
                        break;
                    //下订单
                    case "CreateSystemBooking":
                        _BreakfastTypeName = context.Request["BreakfastTypeName"] ?? "";
                        result = Rts_B2CBLL.Instance.CreateSystemBooking(RtsBookingsUrl, RtsSiteCode, RtsPassword, _nationality, RtsSalesCompCode, RtsSalesSiteCode, RtsSalesUserNo, _ItemCode, _ItemNo, _beginTime, _endTime, _RoomTypeCode, _BreakfastTypeName);
                        context.Response.Write(result);
                        break;
                    //查询订单（接口）
                    case "GetBookingDetail":
                        _BookingCode = context.Request["BookingCode"] ?? "";
                        result = Rts_B2CBLL.Instance.GetBookingDetail(RtsBookingDetailsUrl, RtsSiteCode, RtsPassword, _BookingCode, AgentReferenceNumber);
                        context.Response.Write(result);
                        break;
                    //取消订单（接口）
                    case "BookingCancel":
                        AgentReferenceNumber = context.Request["AgentReferenceNumber"] ?? "";
                        _BookingCode = context.Request["BookingCode"] ?? "";
                        result = Rts_B2CBLL.Instance.BookingCancel(RtsBookingsUrl, RtsSiteCode, RtsPassword, _BookingCode, _ItemNo, RtsSalesUserNo, AgentReferenceNumber);
                        context.Response.Write(result);
                        break;
                    //获取订单明细（通过自定义字段）
                    case "GetAgentReferenceNumber":
                        AgentReferenceNumber = context.Request["AgentReferenceNumber"] ?? "";
                        result = Rts_B2CBLL.Instance.GetAgentReferenceNumber(RtsBookingDetailsUrl, RtsSiteCode, RtsPassword, AgentReferenceNumber);
                        context.Response.Write(result);
                        break;
                    //查询订单(数据库)
                    case "Get_RTSbooking":
                        totalCount = 0;
                        _BookingCode = context.Request["BookingCode"] ?? "";
                        sqlWhere = " 1=1  and Platform='Rts(B2C)'";
                        result = Rts_B2CBLL.Instance.GetRTSbookingBySql(" Booking ", " * ", " Id ", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                        break;
                    //获得优惠券
                    case "GetBookingVoucher":
                        _BookingCode = context.Request["BookingCode"] ?? "";
                        result = Rts_B2CBLL.Instance.GetBookingVoucher(RtsBookingsUrl, RtsSiteCode, RtsPassword, _BookingCode);
                        context.Response.Write(result);
                        break;
                    case "GetRtsRoomTypeOutPrice":
                        IntOccupancyInfo occupancy = new IntOccupancyInfo();
                        _ItemCode = context.Request["hotelId"] ?? "";
                        _beginTime = context.Request["arrivalDate"] ?? "";
                        _endTime = context.Request["departureDate"] ?? "";
                        string postOccupancy = context.Request["occupancy"] ?? "";
                        int RoomCount = (context.Request["roomNum"] ?? "").AsTargetType<int>(1);
                        //解析json
                        occupancy = Newtonsoft.Json.JsonConvert.DeserializeObject<IntOccupancyInfo>(postOccupancy, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        string RateplanId = context.Request["RateplanId"] ?? "";
                        result = Rts_B2CBLL.Instance.GetRtsRoomTypeOutPrice(RtsHotelUrl, RtsSiteCode, RtsPassword, _beginTime, _endTime, _ItemCode, occupancy, RoomCount, RateplanId);
                        context.Response.Write(result);
                        break;
                }
            }
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}