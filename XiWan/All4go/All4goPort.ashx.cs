﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL.All4go;

namespace XiWan.All4go
{
    /// <summary>
    /// All4goPort 的摘要说明
    /// </summary>
    public class All4goPort : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string result = string.Empty;
            context.Response.ContentType = "application/json";
            int input_page = Convert.ToInt16(context.Request["page"] ?? "0");
            int input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
            if (!string.IsNullOrEmpty(context.Request["type"]))
            {
                switch (context.Request["type"])
                {
                    //获取货币类型
                    case "GetCurrencyCode":
                        All4goBLL.Instance.GetCurrencyCode();
                        context.Response.Write("成功");
                        break;
                    //获取所有地点
                    case "GetLocationCode":
                        All4goBLL.Instance.GetLocationCode();
                        context.Response.Write("成功");
                        break;
                    //获取打包产品
                    case "GetPackages":
                        All4goBLL.Instance.GetPackages();
                        context.Response.Write("成功");
                        break;
                    //获取设施
                    case "GetFacilities":
                        All4goBLL.Instance.GetFacilities();
                        context.Response.Write("成功");
                        break;
                    //获取Giata酒店地址
                    case "GetLocationOfGiataHotel":
                        All4goBLL.Instance.GetLocationOfGiataHotel();
                        context.Response.Write("成功");
                        break;
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}