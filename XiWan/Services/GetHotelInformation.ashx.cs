﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Common;
using XiWan.DALFactory;
using System.Data;
using XiWan.DALFactory.Model;
using System.Text.RegularExpressions;

namespace XiWan.Services
{
    /// <summary>
    /// GetHotelInformation 获取酒店详细信息
    /// </summary>
    public class GetHotelInformation : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.ContentType = "text/json";
            context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            Respone result = new Respone();
            string RtsMD5 = CCommon.GetWebConfigValue("RtsMD5");
            //string password = CCommon.GetWebConfigValue("password");
            string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelUrl");
            string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCode");
            string RtsPassword = CCommon.GetWebConfigValue("RtsPassword");
            string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCode");
            string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCode");
            string postData = context.Request["data"] ?? "";
            LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口,请求数据:{0} , Time:{1} ", postData, DateTime.Now.ToString()), "RTS接口");
            try
            {
                if (postData == "")
                {
                    result = new Respone() { code = "99", mes = "请求参数异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                Request request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(postData);
                //数据校验
                if (request == null)
                {
                    result = new Respone() { code = "99", mes = "请求实体异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string itemCode = request.itemCode;
                if (string.IsNullOrEmpty(itemCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【酒店编码】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【酒店编码】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqUserName = request.userName;
                if (string.IsNullOrEmpty(reqUserName))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户名】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqPassword = request.password;
                if (string.IsNullOrEmpty(reqPassword))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户密码】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string md5 = request.md5;
                if (string.IsNullOrEmpty(md5))
                {
                    result = new Respone() { code = "99", mes = "缺少【md5】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string MD5 = Common.Common.Md5Encrypt32("userName:" + reqUserName + ";" + "password:" + reqPassword + ";");
                if (RtsMD5 == md5)
                {
                    string WebResp = string.Empty;
                    string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetHotelInformation xmlns='http://www.rts.co.kr/'><HotelInformationRTSWS><ST_GetHotelInformationRTSWS>
<LanguageCode>CS</LanguageCode><ItemCode>{2}</ItemCode></ST_GetHotelInformationRTSWS></HotelInformationRTSWS></GetHotelInformation></soap:Body>
</soap:Envelope>", RtsSiteCode, RtsPassword, itemCode);
                    try
                    {
                        //发起请求
                        WebResp = Common.Common.WebReq(RtsHotelUrl, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, WebResp, DateTime.Now.ToString()), "RTS接口");
                    }
                    catch (Exception ex)
                    {
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "RTS接口");
                    }
                }
                else
                {
                    result = new Respone() { code = "99", mes = "签名无效" };
                }
                LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", postData, Newtonsoft.Json.JsonConvert.SerializeObject(result), DateTime.Now.ToString()), "RTS接口");
            }
            catch (Exception ex)
            {
                result = new Respone() { code = "99", mes = "数据异常：" + ex.Message };
                LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , err:{1} , Time:{2} ", postData, ex.Message, DateTime.Now.ToString()), "RTS接口");
                context.Response.End();
                return;
            }
            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            context.Response.End();
        }
        public class Request
        {
            public string itemCode { get; set; }
            public string userName { get; set; }
            public string password { get; set; }
            public string md5 { get; set; }
        }
        public class Respone
        {
            public string code { get; set; }
            public result result { get; set; }
            public string mes { get; set; }
        }
        public class result
        {
            public string CancelDeadlineDate { get; set; }
            public string TypeCode { get; set; }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}