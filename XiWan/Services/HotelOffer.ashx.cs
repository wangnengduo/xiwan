﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Common;
using XiWan.DALFactory;
using System.Data;
using XiWan.DALFactory.Model;
using System.Text.RegularExpressions;
using XiWan.RTS;
using XiWan.Data.Redis;

namespace XiWan.Services
{
    /// <summary>
    /// HotelOffer 报价接口
    /// 超过5人接口无法报价，带儿童目前无法报价
    /// </summary>
    public class HotelOffer : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.ContentType = "text/json";
            context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            string strResult=string.Empty;
            string BedTypeCode=string.Empty;//床型
            string WebResp = string.Empty;//接口返回值
            string cityCode=string.Empty;//酒店所在城市的code
            Respone result = new Respone();
            string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelUrl");
            string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCode");
            string RtsPassword = CCommon.GetWebConfigValue("RtsPassword");
            string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCode");
            string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCode");
            string postData = context.Request["data"] ?? "";
            LogHelper.DoOperateLog(string.Format("Studio:RTS查询房型接口,请求数据:{0} , Time:{1} ", postData, DateTime.Now.ToString()), "RTS接口");

            int adult = 1;
            string itemCode = "";
            string checkInDate= "";
            string checkOutDate = "";
            string sql = string.Format(@"SELECT top 1 * FROM RtsHotel WITH(NOLOCK) where itemCode='{0}'", itemCode);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            if (dt.Rows.Count == 0)
            {
                strResult = "暂无报价";
            }
            else
            {
                cityCode = dt.Rows[0]["dt"].AsTargetType<string>("");
            }
            if (adult == 1)
            {
                BedTypeCode = "BED01";
            }
            else if (adult == 2)
            {
                BedTypeCode = "BED02";//或 BED03
            }
            else if (adult == 3)
            {
                BedTypeCode = "BED04";
            }
            else if (adult == 4)
            {
                BedTypeCode = "BED05";
            }
            else
            {
                strResult = "暂无报价";
            }
            //读取缓存 存在取缓存信息，不存在折调用接口读并做缓存
            List<Hotel.PriceInfo> r = RedisHelper.GetMem<List<Hotel.PriceInfo>>(itemCode + checkInDate + checkOutDate + adult);//读取缓存
            if (r.Count == 0)
            {
                string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetHotelSearchList xmlns='http://www.rts.co.kr/'><HotelSearchListNet><LanguageCode>CS</LanguageCode>
<TravelerNationality>{2}</TravelerNationality><CityCode>{3}</CityCode><CheckInDate>{4}</CheckInDate><CheckOutDate>{5}</CheckOutDate>
<StarRating>{6}</StarRating><LocationCode>{7}</LocationCode><SupplierCompCode>{8}</SupplierCompCode><AvailableHotelOnly>false</AvailableHotelOnly>
<RecommendHotelOnly>false</RecommendHotelOnly><ClientCurrencyCode>HKD</ClientCurrencyCode><ItemName>{9}</ItemName><SellerMarkup>*1</SellerMarkup>
<CompareYn>false</CompareYn><SortType></SortType><ItemCodeList><ItemCodeInfo><ItemCode>{10}</ItemCode><ItemNo>0</ItemNo></ItemCodeInfo></ItemCodeList>
<RoomsList><RoomsInfo><BedTypeCode>{11}</BedTypeCode><RoomCount>1</RoomCount><ChildAge1>0</ChildAge1><ChildAge2>0</ChildAge2></RoomsInfo>
</RoomsList></HotelSearchListNet></GetHotelSearchList></soap:Body></soap:Envelope>"
                            , RtsSiteCode, RtsPassword, "", cityCode, checkInDate, checkOutDate, 0, "", RtsSalesCompCode, "", itemCode, BedTypeCode);
                try
                {
                    //发起请求
                    WebResp = Common.Common.WebReq(RtsHotelUrl, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                    //日记
                    LogHelper.DoOperateLog(string.Format("Studio:RTS查询房型接口（向RTS请求）,向RTS请求数据:{0} , RTS返回数据：{1} ， Time:{2} ", strXml, WebResp, DateTime.Now.ToString()), "RTS接口");
                }
                catch (Exception ex)
                {
                    result = new Respone() { code = "99", mes = "数据异常：" + ex.Message };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    //日记
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口（向RTS请求） ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                Regex regex = new Regex("<PriceList>");//以PriceList分割
                string[] bit = regex.Split(WebResp);
                if (bit.Length > 1)
                {
                    WebResp = bit[1];
                    Regex regexHotel = new Regex("</PriceList>");
                    bit = regexHotel.Split(WebResp);
                    WebResp = bit[0];
                    WebResp = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <ArrayOfPriceInfo xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" + result + "</ArrayOfPriceInfo>");
                    List<Hotel.PriceInfo> priceInfoList = Common.Common.DESerializer<List<Hotel.PriceInfo>>(WebResp);
                    result = new Respone() { code = "00", result = priceInfoList, mes = "成功" };
                    //存储到缓存中
                    RedisHelper.SetMem<List<Hotel.PriceInfo>>(priceInfoList, itemCode + checkInDate + checkOutDate + adult,0);
                }
                else
                {
                    result = new Respone() { code = "99", mes = "解析失败" };
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "解析失败", strXml, DateTime.Now.ToString()), "RTS接口");
                }
            }
        }
        public class Respone
        {
            public string code { get; set; }
            public List<Hotel.PriceInfo> result { get; set; }
            public string mes { get; set; }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}