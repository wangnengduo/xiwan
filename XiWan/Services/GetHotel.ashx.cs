﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Common;
using System.Data;
using XiWan.DALFactory.Model;
using XiWan.DALFactory;

namespace XiWan.Services
{
    /// <summary>
    /// GetHotel 查询酒店
    /// </summary>
    public class GetHotel : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.ContentType = "text/json";
            context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            Respone result = new Respone();
            string userName = "1";//CCommon.GetWebConfigValue("userName");
            string password = "1"; //CCommon.GetWebConfigValue("password");
            string postData = context.Request["data"] ?? "";
            LogHelper.DoOperateLog(string.Format("Studio:查询酒店接口,请求数据:{0} , Time:{1} ", postData, DateTime.Now.ToString()), "RTS接口");
            try
            {
                if (postData == "")
                {
                    result = new Respone() { code = "99", mes = "请求参数异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:查询酒店接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                Request request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(postData);
                //数据校验
                if (request == null)
                {
                    result = new Respone() { code = "99", mes = "请求实体异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:查询酒店接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求实体异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string cityCode = request.cityCode;
                if (string.IsNullOrEmpty(cityCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【目的地】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:查询酒店接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【目的地】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                int starRating = request.starRating.AsTargetType<int>(0);
                if (starRating > 5)
                {
                    result = new Respone() { code = "99", mes = "酒店星级最高为5" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "酒店星级最高为5", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqUserName = request.userName;
                if (string.IsNullOrEmpty(reqUserName))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户名】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:查询酒店接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【用户名】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqPassword = request.password;
                if (string.IsNullOrEmpty(reqPassword))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户密码】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:查询酒店接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【用户密码】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string md5 = request.md5;
                if (string.IsNullOrEmpty(md5))
                {
                    result = new Respone() { code = "99", mes = "缺少【md5】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:查询酒店接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【md5】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string MD5 = Common.Common.Md5Encrypt32("userName:" + userName + ";" + "password:" + password + ";");
                if (MD5 == md5)
                {
                    string itemCode = request.itemCode;
                    string itemName = request.itemName;
                    string sqlWhere = " 1=1";
                    if (!(itemCode ==null || itemCode == ""))
                    {
                        sqlWhere += string.Format(@" and ItemCode='{0}'", itemCode);
                    }
                    if (!(itemName ==null || itemName == ""))
                    {
                        sqlWhere += string.Format(@" and ItemName like '%{0}%'", itemName);
                    }
                    if (starRating != 0)
                    {
                        sqlWhere += string.Format(@" and ItemGradeCode={0}", starRating);
                    }
                    string sql = string.Format(@"SELECT  * FROM  RtsHotel  WHERE   CityCode = '{0}' and {1}", cityCode, sqlWhere);
                    DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                    if (dt.Rows.Count == 0)
                    {
                        result = new Respone() { code = "99", mes = "没有符合数据，请确重新查询"};
                    }
                    else
                    {
                        List<RtsHotel> listHotel = Common.Common.ModelConvertHelper<RtsHotel>.ConvertToModel(dt);
                        result = new Respone() { code = "00", result = listHotel, mes = "成功" };
                    }
                }
                else
                {
                    result = new Respone() { code = "99", mes = "签名无效" };
                }
                LogHelper.DoOperateLog(string.Format("Studio:查询酒店接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", postData, Newtonsoft.Json.JsonConvert.SerializeObject(result), DateTime.Now.ToString()), "RTS接口");
            }
            catch (Exception ex)
            {
                result = new Respone() { code = "99", mes = "数据异常：" + ex.Message };
                LogHelper.DoOperateLog(string.Format("Err:查询酒店接口,请求数据:{0} , err:{1} , Time:{2} ", postData, ex.Message, DateTime.Now.ToString()), "RTS接口");
                context.Response.End();
                return;
            }
            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            context.Response.End();
        }
        public class Request
        {
            public string cityCode { get; set; }
            public string locationCode { get; set; }
            public string itemCode { get; set; }
            public string itemName { get; set; }
            public int starRating { get; set; }
            public string userName { get; set; }
            public string password { get; set; }
            public string md5 { get; set; }
        }
        public class Respone
        {
            public string code { get; set; }
            public List<RtsHotel> result { get; set; }
            public string mes { get; set; }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}