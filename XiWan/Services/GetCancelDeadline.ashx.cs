﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Common;
using XiWan.DALFactory;
using System.Data;
using XiWan.DALFactory.Model;
using System.Text.RegularExpressions;

namespace XiWan.Services
{
    /// <summary>
    /// GetCancelDeadline 查询取消期间
    /// </summary>
    public class GetCancelDeadline : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.ContentType = "text/json";
            context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            Respone result = new Respone();
            string userName = CCommon.GetWebConfigValue("userName");
            string password = CCommon.GetWebConfigValue("password");
            string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelUrl");
            string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCode");
            string RtsPassword = CCommon.GetWebConfigValue("RtsPassword");
            string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCode");
            string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCode");
            string postData = context.Request["data"] ?? "";
            LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口,请求数据:{0} , Time:{1} ", postData, DateTime.Now.ToString()), "RTS接口");
            try
            {
                if (postData == "")
                {
                    result = new Respone() { code = "99", mes = "请求参数异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                Request request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(postData);
                //数据校验
                if (request == null)
                {
                    result = new Respone() { code = "99", mes = "请求实体异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string itemCode = request.itemCode;
                if (string.IsNullOrEmpty(itemCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【酒店编码】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【酒店编码】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string itemNo = request.itemNo;
                if (string.IsNullOrEmpty(itemNo))
                {
                    result = new Respone() { code = "99", mes = "缺少【itemNo】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【itemNo】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string nationalityCode = request.nationalityCode;
                if (string.IsNullOrEmpty(nationalityCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【国籍编号】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【国籍编号】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string checkInDate = request.checkInDate;
                if (string.IsNullOrEmpty(checkInDate))
                {
                    result = new Respone() { code = "99", mes = "缺少【入住日期】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【入住日期】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string checkOutDate = request.checkOutDate;
                if (string.IsNullOrEmpty(checkOutDate))
                {
                    result = new Respone() { code = "99", mes = "缺少【退房日期】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【退房日期】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqUserName = request.userName;
                if (string.IsNullOrEmpty(reqUserName))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户名】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqPassword = request.password;
                if (string.IsNullOrEmpty(reqPassword))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户密码】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string md5 = request.md5;
                if (string.IsNullOrEmpty(md5))
                {
                    result = new Respone() { code = "99", mes = "缺少【md5】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string MD5 = Common.Common.Md5Encrypt32("userName:" + userName + ";" + "password:" + password + ";");
                if (MD5 == md5)
                {
                    List<listRooms> Rooms = request.rooms;
                    if (Rooms==null || Rooms.Count==0)
                    {
                        result = new Respone() { code = "99", mes = "请输入正确房型" };
                        context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请输入正确房型", DateTime.Now.ToString()), "RTS接口");
                        context.Response.End();
                        return;
                    }
                    string childXml = string.Empty;
                    string strRoomTypeCode = string.Empty;
                    for (int i = 0; i < Rooms.Count; i++)
                    {
                        string bedTypeCode = Rooms[i].bedTypeCode.AsTargetType<string>("0");
                        int reqChildAge1 = Rooms[i].childAge1.AsTargetType<int>(0);
                        int reqChildAge2 = Rooms[i].childAge2.AsTargetType<int>(0);
                        if (bedTypeCode != "BED1" || bedTypeCode != "BED2" || bedTypeCode != "BED3" || bedTypeCode != "BED4" || bedTypeCode != "BED5" || bedTypeCode != "BED10" || bedTypeCode != "BED11" || bedTypeCode != "BED12" || bedTypeCode != "BED13" || bedTypeCode != "BED14")
                        {
                            result = new Respone() { code = "99", mes = "房型输入不正确！" };
                            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                            LogHelper.DoOperateLog(string.Format("Err:RTS查询取消期间,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "房型输入不正确", DateTime.Now.ToString()), "RTS接口");
                            context.Response.End();
                            return;
                        }
                        else
                        {
                            if (bedTypeCode == "BED10" || bedTypeCode == "BED11" || bedTypeCode == "BED12" || bedTypeCode == "BED13" || bedTypeCode == "BED14")
                            {
                                string strBedTypeCode = bedTypeCode + ",";
                                if (reqChildAge1 == 0 && reqChildAge2 == 0)
                                {
                                    result = new Respone() { code = "99", mes = "预订带儿童房，儿童岁数必须大于0且小于等于12" };
                                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                    LogHelper.DoOperateLog(string.Format("Err:RTS查询取消期间,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "预订带儿童房，儿童岁数必须大于0且小于等于12", DateTime.Now.ToString()), "RTS接口");
                                    context.Response.End();
                                    return;
                                }
                                else if (reqChildAge1 > 0)
                                {
                                    strBedTypeCode += reqChildAge1 + ",";
                                }
                                else if (reqChildAge2 > 0)
                                {
                                    strBedTypeCode += reqChildAge2 + ",";
                                }
                                strRoomTypeCode += string.Format(@"<String>{0}</String>", strBedTypeCode.TrimEnd(','));
                            }
                            else
                            {
                                if (reqChildAge1 == 0 && reqChildAge2 == 0)
                                {
                                    strRoomTypeCode += string.Format(@"<String>{0}</String>", bedTypeCode);
                                }
                                else
                                {
                                    result = new Respone() { code = "99", mes = "不能带儿童房型，儿童岁数为空" };
                                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                    LogHelper.DoOperateLog(string.Format("Err:RTS查询取消期间,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "不能带儿童房型，儿童岁数为空", DateTime.Now.ToString()), "RTS接口");
                                    context.Response.End();
                                    return;
                                }
                            }
                        }
                    }
                    string WebResp = string.Empty;
                    string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType></BaseInfo>
</soap:Header><soap:Body><GetCancelDeadline xmlns='http://www.rts.co.kr/'><GetCancelDeadline><ItemCode>{2}</ItemCode><ItemNo>{3}</ItemNo><RoomTypeCode>{4}</RoomTypeCode>
<CheckInDate>{5}</CheckInDate><CheckOutDate>{6}</CheckOutDate><BedTypeCode>{7}</BedTypeCode><LanguageCode>CS</LanguageCode><TravelerNationality>{8}</TravelerNationality>
<BookingYn>Y</BookingYn><HotelRoomTypeId></HotelRoomTypeId></GetCancelDeadline></GetCancelDeadline></soap:Body></soap:Envelope>
", RtsSiteCode, RtsPassword, itemCode, itemNo, strRoomTypeCode, checkInDate, checkOutDate, nationalityCode);
                    try
                    {
                        //发起请求
                        WebResp = Common.Common.WebReq(RtsHotelUrl, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, WebResp, DateTime.Now.ToString()), "RTS接口");
                    }
                    catch (Exception ex)
                    {
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "RTS接口");
                    }
                    Regex regex = new Regex("<GetCancelDeadlineResponse>");//以GetCancelDeadlineResult分割
                    string[] bit = regex.Split(WebResp);
                    if (bit.Length > 1)
                    {
                        WebResp = bit[1];
                        Regex regexHotel = new Regex("</GetCancelDeadlineResponse>");
                        bit = regexHotel.Split(WebResp);
                        WebResp = bit[0];
                        WebResp = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <ArrayOfGetCancelDeadlineResult xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" + result + "</ArrayOfGetCancelDeadlineResult>");
                        result DeadlineResult = Common.Common.DESerializer<result>(WebResp);
                        result = new Respone() { code = "00", result = DeadlineResult, mes = "成功" };
                    }
                    else
                    {
                        result = new Respone() { code = "99", mes = "解析失败" };
                        LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "解析失败", strXml, DateTime.Now.ToString()), "RTS接口");
                    }
                }
                else
                {
                    result = new Respone() { code = "99", mes = "签名无效" };
                }
                LogHelper.DoOperateLog(string.Format("Studio:RTS获取取消期间接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", postData, Newtonsoft.Json.JsonConvert.SerializeObject(result), DateTime.Now.ToString()), "RTS接口");
            }
            catch (Exception ex)
            {
                result = new Respone() { code = "99", mes = "数据异常：" + ex.Message };
                LogHelper.DoOperateLog(string.Format("Err:RTS获取取消期间接口,请求数据:{0} , err:{1} , Time:{2} ", postData, ex.Message, DateTime.Now.ToString()), "RTS接口");
                context.Response.End();
                return;
            }
            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            context.Response.End();
        }
        public class Request
        {
            public string itemCode { get; set; }
            public string itemNo { get; set; }
            public List<listRooms> rooms
            {
                set;
                get;
            }
            public string nationalityCode { get; set; }
            public string checkInDate { get; set; }
            public string checkOutDate { get; set; }
            public string userName { get; set; }
            public string password { get; set; }
            public string md5 { get; set; }
        }
        public class listRooms
        {
            public string bedTypeCode { get; set; }
            public int childAge1 { get; set; }
            public int childAge2 { get; set; }
        }
        public class Respone
        {
            public string code { get; set; }
            public result result { get; set; }
            public string mes { get; set; }
        }
        public class result
        {
            public string CancelDeadlineDate { get; set; }
            public string TypeCode { get; set; }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}