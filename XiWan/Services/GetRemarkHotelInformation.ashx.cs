﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Common;
using XiWan.DALFactory;
using System.Data;
using XiWan.DALFactory.Model;
using System.Text.RegularExpressions;
using XiWan.RTS;

namespace XiWan.Services
{
    /// <summary>
    /// GetRemarkHotelInformation   重要的提醒 Important Notice:
    /// </summary>
    public class GetRemarkHotelInformation : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.Clear();
            context.Response.ContentType = "text/json";
            context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            Respone result = new Respone();
            string userName = CCommon.GetWebConfigValue("userName");
            string password = CCommon.GetWebConfigValue("password");
            string RtsHotelUrl = CCommon.GetWebConfigValue("RtsHotelUrl");
            string RtsSiteCode = CCommon.GetWebConfigValue("RtsSiteCode");
            string RtsPassword = CCommon.GetWebConfigValue("RtsPassword");
            string RtsSalesCompCode = CCommon.GetWebConfigValue("RtsSalesCompCode");
            string RtsSalesSiteCode = CCommon.GetWebConfigValue("RtsSalesSiteCode");
            string postData = context.Request["data"] ?? "";
            LogHelper.DoOperateLog(string.Format("Studio:RTS查询房型接口,请求数据:{0} , Time:{1} ", postData, DateTime.Now.ToString()), "RTS接口");
            try
            {
                if (postData == "")
                {
                    result = new Respone() { code = "99", mes = "请求参数异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求参数异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                Request request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(postData);
                //数据校验
                if (request == null)
                {
                    result = new Respone() { code = "99", mes = "请求实体异常" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "请求实体异常", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string cityCode = request.cityCode;
                if (string.IsNullOrEmpty(cityCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【目的地】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【目的地】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqUserName = request.userName;
                if (string.IsNullOrEmpty(reqUserName))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户名】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【用户名】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string reqPassword = request.password;
                if (string.IsNullOrEmpty(reqPassword))
                {
                    result = new Respone() { code = "99", mes = "缺少【用户密码】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【用户密码】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string itemCode = request.itemCode;
                if (string.IsNullOrEmpty(itemCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【酒店编号】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【酒店编号】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string nationalityCode = request.nationalityCode;
                if (string.IsNullOrEmpty(nationalityCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【国籍编号】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【国籍编号】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string checkInDate = request.checkInDate;
                if (string.IsNullOrEmpty(checkInDate))
                {
                    result = new Respone() { code = "99", mes = "缺少【入住日期】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【入住日期】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string checkOutDate = request.checkOutDate;
                if (string.IsNullOrEmpty(checkOutDate))
                {
                    result = new Respone() { code = "99", mes = "缺少【退房日期】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【退房日期】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string itemNo = request.itemNo;
                if (string.IsNullOrEmpty(itemNo))
                {
                    result = new Respone() { code = "99", mes = "缺少【itemNo】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【itemNo】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string roomTypeCode = request.roomTypeCode;
                if (string.IsNullOrEmpty(roomTypeCode))
                {
                    result = new Respone() { code = "99", mes = "缺少【房型编号】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【房型编号】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string md5 = request.md5;
                if (string.IsNullOrEmpty(md5))
                {
                    result = new Respone() { code = "99", mes = "缺少【md5】" };
                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "缺少【md5】", DateTime.Now.ToString()), "RTS接口");
                    context.Response.End();
                    return;
                }
                string MD5 = Common.Common.Md5Encrypt32("userName:" + userName + ";" + "password:" + password + ";");
                if (MD5 == md5)
                {
                    //result = new Respone() { code = "00", mes = "成功" };
                    List<listRooms> Rooms = request.rooms;
                    string childXml = string.Empty;
                    int reqRoomTotal = 0;//总房间数
                    int withChildrenTotal = 0;//带儿童房间数
                    for (int i = 0; i < Rooms.Count; i++)
                    {
                        string bedTypeCode = Rooms[i].bedTypeCode.AsTargetType<string>("0");
                        int reqChildAge1 = Rooms[i].childAge1.AsTargetType<int>(0);
                        int reqChildAge2 = Rooms[i].childAge2.AsTargetType<int>(0);
                        int reqRoomCount = Rooms[i].roomCount.AsTargetType<int>(0);
                        if (bedTypeCode != "BED1" || bedTypeCode != "BED2" || bedTypeCode != "BED3" || bedTypeCode != "BED4" || bedTypeCode != "BED5" || bedTypeCode != "BED10" || bedTypeCode != "BED11" || bedTypeCode != "BED12" || bedTypeCode != "BED13" || bedTypeCode != "BED14")
                        {
                            result = new Respone() { code = "99", mes = "房型输入不正确！" };
                            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                            LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "房型输入不正确", DateTime.Now.ToString()), "RTS接口");
                            context.Response.End();
                            return;
                        }
                        else
                        {
                            if (reqRoomCount < 1)
                            {
                                result = new Respone() { code = "99", mes = "房型输入后，房间数需大于0小于等于5！" };
                                context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "房型输入不正确", DateTime.Now.ToString()), "RTS接口");
                                context.Response.End();
                                return;
                            }
                            if (bedTypeCode == "BED10" || bedTypeCode == "BED11" || bedTypeCode == "BED12" || bedTypeCode == "BED13" || bedTypeCode == "BED14")
                            {
                                if (reqChildAge1 == 0 && reqChildAge2 == 0)
                                {
                                    result = new Respone() { code = "99", mes = "预订带儿童房，儿童岁数必须大于0且小于12" };
                                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "预订带儿童房，儿童岁数必须大于0且小于12", DateTime.Now.ToString()), "RTS接口");
                                    context.Response.End();
                                    return;
                                }
                                if ((reqChildAge1 >= 0 && reqChildAge1 <= 12) && (reqChildAge2 >= 0 && reqChildAge2 <= 12))
                                {
                                    childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                                        , bedTypeCode, reqRoomCount, reqChildAge1, reqChildAge2);
                                }
                                else
                                {
                                    result = new Respone() { code = "99", mes = "儿童岁数必须大于0且小于12" };
                                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "儿童岁数必须大于0且小于12", DateTime.Now.ToString()), "RTS接口");
                                    context.Response.End();
                                    return;
                                }
                                withChildrenTotal += 1;
                            }
                            else
                            {
                                if (reqChildAge1 == 0 && reqChildAge2 == 0)
                                {
                                    childXml += string.Format(@"<RoomsInfo><BedTypeCode>{0}</BedTypeCode><RoomCount>{1}</RoomCount><ChildAge1>{2}</ChildAge1><ChildAge2>{3}</ChildAge2></RoomsInfo>"
                                        , bedTypeCode, reqRoomCount, reqChildAge1, reqChildAge2);
                                }
                                else
                                {
                                    result = new Respone() { code = "99", mes = "不带儿童房，儿童岁数只能为0或空" };
                                    context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                                    LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "不带儿童房，儿童岁数只能为0或空", DateTime.Now.ToString()), "RTS接口");
                                    context.Response.End();
                                    return;
                                }
                            }
                            reqRoomTotal += reqRoomCount;
                        }

                    }
                    if (withChildrenTotal > 1)
                    {
                        result = new Respone() { code = "99", mes = "带儿童房一次最多只能一间！" };
                        context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "带儿童房一次最多只能一间", DateTime.Now.ToString()), "RTS接口");
                        context.Response.End();
                        return;
                    }
                    if (reqRoomTotal > 5)
                    {
                        result = new Respone() { code = "99", mes = "一次最多只能搜索5间房！" };
                        context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , 返回数据:{1} , Time:{2} ", postData, "一次最多只能搜索5间房！", DateTime.Now.ToString()), "RTS接口");
                        context.Response.End();
                        return;
                    }
                    string WebResp = string.Empty;
                    int starRating = request.starRating.AsTargetType<int>(0);
                    string locationCode = request.locationCode.AsTargetType<string>("");
                    //string itemName = request.itemName.AsTargetType<string>("");
                    string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:rts='http://www.rts.co.kr/' xmlns:soap='http://www.w3.org/2003/05/soap-envelope'><soap:Header><BaseInfo>
<SiteCode>{0}</SiteCode><Password>{1}</Password><RequestType>NetPartner</RequestType></BaseInfo></soap:Header><soap:Body>
<GetRemarkHotelInformation><HotelSearchListNet><LanguageCode>CS</LanguageCode><TravelerNationality>{2}</TravelerNationality>
<CityCode>{3}</CityCode><CheckInDate>{4}</CheckInDate><CheckOutDate>{5}</CheckOutDate><StarRating>{6}</StarRating><LocationCode>{7}</LocationCode>
<SupplierCompCode>{8}</SupplierCompCode><AvailableHotelOnly>false</AvailableHotelOnly><RecommendHotelOnly>false</RecommendHotelOnly>
<ClientCurrencyCode>HKD</ClientCurrencyCode><ItemName/><SellerMarkup>*1</SellerMarkup><CompareYn>false</CompareYn><SortType/><ItemCodeList>
<ItemCodeInfo><ItemCode>{9}</ItemCode><ItemNo>{10}</ItemNo></ItemCodeInfo></ItemCodeList><RoomsList><RoomsInfo>{11}</ RoomsList></HotelSearchListNet>
<RoomTypeCode>{12}</RoomTypeCode></GetRemarkHotelInformation></soap:Body></soap:Envelope>"
                , RtsSiteCode, RtsPassword, nationalityCode, cityCode, checkInDate, checkOutDate, starRating, locationCode, RtsSalesCompCode, itemCode, itemNo, childXml, roomTypeCode);
                    try
                    {
                        //发起请求
                        WebResp = Common.Common.WebReq(RtsHotelUrl, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                        //日记
                        LogHelper.DoOperateLog(string.Format("Studio:RTS查询房型接口（向RTS请求）,向RTS请求数据:{0} , RTS返回数据：{1} ， Time:{2} ", strXml, WebResp, DateTime.Now.ToString()), "RTS接口");
                    }
                    catch (Exception ex)
                    {
                        result = new Respone() { code = "99", mes = "数据异常：" + ex.Message };
                        context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                        //日记
                        LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口（向RTS请求） ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "RTS接口");
                        context.Response.End();
                        return;
                    }
                    Regex regex = new Regex("<PriceList>");//以PriceList分割
                    string[] bit = regex.Split(WebResp);
                    if (bit.Length > 1)
                    {
                        WebResp = bit[1];
                        Regex regexHotel = new Regex("</PriceList>");
                        bit = regexHotel.Split(WebResp);
                        WebResp = bit[0];
                        WebResp = string.Format(@"<?xml version=""1.0"" encoding=""utf-8""?> <ArrayOfPriceInfo xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">" + result + "</ArrayOfPriceInfo>");
                        List<Hotel.PriceInfo> priceInfoList = Common.Common.DESerializer<List<Hotel.PriceInfo>>(WebResp);
                        result = new Respone() { code = "00", result = priceInfoList, mes = "成功" };
                    }
                    else
                    {
                        result = new Respone() { code = "99", mes = "解析失败" };
                        LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口 ,err:{0} , 请求数据:{1} , Time:{2} ", "解析失败", strXml, DateTime.Now.ToString()), "RTS接口");
                    }
                }
                else
                {
                    result = new Respone() { code = "99", mes = "签名无效" };
                }
            }
            catch (Exception ex)
            {
                result = new Respone() { code = "99", mes = "数据异常：" + ex.Message };
                context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
                LogHelper.DoOperateLog(string.Format("Err:RTS查询房型接口,请求数据:{0} , err:{1} , Time:{2} ", postData, ex.Message, DateTime.Now.ToString()), "RTS接口");
                context.Response.End();
                return;
            }
            context.Response.Write(Newtonsoft.Json.JsonConvert.SerializeObject(result));
            LogHelper.DoOperateLog(string.Format("Studio:RTS查询房型接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", postData, Newtonsoft.Json.JsonConvert.SerializeObject(result), DateTime.Now.ToString()), "RTS接口");
            context.Response.End();
        }
        public class Request
        {
            public string cityCode { get; set; }
            public string locationCode { get; set; }
            public string itemCode { get; set; }
            public string itemNo { get; set; }
            public string nationalityCode { get; set; }
            public string checkInDate { get; set; }
            public string checkOutDate { get; set; }
            public int starRating { get; set; }
            public string roomTypeCode { get; set; }
            public string userName { get; set; }
            public string password { get; set; }
            public string md5 { get; set; }
            public List<listRooms> rooms
            {
                set;
                get;
            }
        }
        public class listRooms
        {
            public string bedTypeCode { get; set; }
            public int childAge1 { get; set; }
            public int childAge2 { get; set; }
            public int roomCount { get; set; }
        }
        public class Respone
        {
            public string code { get; set; }
            public List<Hotel.PriceInfo> result { get; set; }
            public string mes { get; set; }
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}