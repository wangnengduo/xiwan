﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.DALFactory;
using XiWan.Common;
using System.Data;
using System.Text;
using XiWan.DALFactory.Model;
using XiWan.Data.Redis;
using XiWan.BLL;
using XiWan.Model.Entities;

namespace XiWan.TravPax
{
    /// <summary>
    /// TravPaxPort 的摘要说明
    /// </summary>
    public class TravPaxPort : IHttpHandler
    {
        //获取配置中TravPax地址
        string url = CCommon.GetWebConfigValue("TravPaxUrl");
        string travPaxUsername = CCommon.GetWebConfigValue("TravPaxUsername");
        string travPaxPassword = CCommon.GetWebConfigValue("TravPaxPassword");
        string VIEWSTATE = CCommon.GetWebConfigValue("VIEWSTATE");

        string VIEWSTATEGENERATOR = CCommon.GetWebConfigValue("VIEWSTATEGENERATOR");
        string EVENTVALIDATION = CCommon.GetWebConfigValue("EVENTVALIDATION");
        string btnPost = CCommon.GetWebConfigValue("btnPost");
        string TravPaxCacheTime = CCommon.GetWebConfigValue("TravPaxCacheTime");
        string cityID = string.Empty;
        string countryCode = string.Empty;
        public void ProcessRequest(HttpContext context)
        {
            string result = string.Empty;
            context.Response.ContentType = "text/plain";
            if (!string.IsNullOrEmpty(context.Request["type"]))
            {
                string hotelID = context.Request["hotelID"] ?? "";
                string countryID = context.Request["countryID"] ?? "";
                //string cityID = context.Request["cityID"] ?? "";
                string referenceClient = context.Request["referenceClient"] ?? "";
                string nationalityID = context.Request["nationalityID"] ?? "";
                string beginTime = context.Request["beginTime"] ?? "";
                string endTime = context.Request["endTime"] ?? "";
                string ddlRoom = context.Request["ddlRoom"] ?? "";
                string rooms = context.Request["rooms"] ?? "";
                string showType = context.Request["_showType"] ?? "";
                int input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                int input_row = Convert.ToInt16(context.Request["rows"] ?? "0");

                switch (context.Request["type"])
                {
                    //获取国籍
                    case "GetTravPaxNationality":
                        context.Response.ContentType = "application/json";
                        result = TravPaxBLL.Instance.GetTravPaxnationality();
                        context.Response.Write(result);
                        break;
                    //获取目的地
                    case "Get_Destination":
                        context.Response.ContentType = "application/json";
                        string strName = context.Request["paramName"].ToString();
                        if (strName!="")
                            result = TravPaxBLL.Instance.GetTravPaxdestination(strName);
                        context.Response.Write(result);
                        break;
                    //酒店信息
                    case "Get_HotelInformation":
                        string _HotelName = context.Request["hotelName"] ?? "";
                        string _ddlGreadeCode = context.Request["ddlGreadeCode"] ?? "0";
                        string destination = context.Request["destination"] ?? "";
                        if (destination != "")
                        {
                            string[] ArrayDestination = destination.Split(',');
                            cityID = ArrayDestination[0];
                            countryCode = ArrayDestination[1];
                        }
                        string sqlWhere = " 1=1 ";
                        if (cityID != "" || countryCode != "")
                        {
                            if (cityID != "")
                                sqlWhere += string.Format(@" and CITY_ID='{0}'", cityID);
                            if (countryCode != "")
                                sqlWhere += string.Format(@" and COUNTRY_CODE='{0}'", countryCode);
                        }
                        else
                        {
                            sqlWhere += "and 1=0";
                        }
                        if (_HotelName != "")
                        {
                            sqlWhere += string.Format(@" and Hotel_Name like '%{0}%'", _HotelName);
                        }
                        if (_ddlGreadeCode != "0")
                        {
                            sqlWhere += string.Format(@" and HOTEL_RATING={0}", _ddlGreadeCode);
                        }
                        int totalCount = 0;
                        result = TravPaxBLL.Instance.GetHotel("TravPaxHotel", "*", "Hotel_Name", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                        break;
                    //获取酒店详情
                    case "Get_HotelDetail":
                        context.Response.ContentType = "application/json";
                        cityID = context.Request["cityID"] ?? "";
                        result = TravPaxBLL.Instance.GetRealHotelDetail(url, travPaxUsername, travPaxPassword, hotelID, countryID, cityID, referenceClient, nationalityID, beginTime, endTime, Convert.ToInt16(ddlRoom), rooms, input_page, input_row, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                    //获取报价
                    case "Get_HotelQuoteDetail":
                        context.Response.ContentType = "application/json";
                        result = TravPaxBLL.Instance.GetRealHotelQuote(url, travPaxUsername, travPaxPassword,TravPaxCacheTime, hotelID,nationalityID, beginTime, endTime, Convert.ToInt16(ddlRoom), rooms, input_page, input_row, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                    //获取取消期限
                    case "Get_CancellationPolicy":
                        context.Response.ContentType = "application/json";
                        string HotelID = context.Request["HotelID"] ?? "";
                        beginTime = context.Request["beginTime"] ?? "";
                        endTime = context.Request["endTime"] ?? "";
                        string Adults = context.Request["Adults"] ?? "";
                        string Childs = context.Request["Childs"] ?? "";
                        string HotelPriceID = context.Request["HotelPriceID"] ?? "";
                        result = TravPaxBLL.Instance.GetCancellationPolicy(url, travPaxUsername, travPaxPassword, TravPaxCacheTime, HotelID.AsTargetType<int>(0), HotelPriceID,nationalityID, Adults.AsTargetType<int>(0), Childs.AsTargetType<int>(0), beginTime, endTime, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                        //检查价格
                    case "GetHotelRecheckPrice":
                        context.Response.ContentType = "application/json";
                        HotelID = context.Request["HotelID"] ?? "";
                        beginTime = context.Request["beginTime"] ?? "";
                        endTime = context.Request["endTime"] ?? "";
                        Adults = context.Request["Adults"] ?? "";
                        Childs = context.Request["Childs"] ?? "";
                        HotelPriceID = context.Request["HotelPriceID"] ?? "";
                        result = TravPaxBLL.Instance.GetHotelRecheckPrice(url, travPaxUsername, travPaxPassword, TravPaxCacheTime, HotelID.AsTargetType<int>(0), HotelPriceID,nationalityID, Adults.AsTargetType<int>(0), Childs.AsTargetType<int>(0), beginTime, endTime, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                    case "CreatePNR":
                        context.Response.ContentType = "application/json";
                        HotelID = context.Request["HotelID"] ?? "";
                        beginTime = context.Request["beginTime"] ?? "";
                        endTime = context.Request["endTime"] ?? "";
                        Adults = context.Request["Adults"] ?? "";
                        Childs = context.Request["Childs"] ?? "";
                        HotelPriceID = context.Request["HotelPriceID"] ?? "";
                        result = TravPaxBLL.Instance.CreatePNR(url, travPaxUsername, travPaxPassword, TravPaxCacheTime, HotelID.AsTargetType<int>(0), HotelPriceID,nationalityID, Adults.AsTargetType<int>(0), Childs.AsTargetType<int>(0), beginTime, endTime, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                    case "TravPaxBooking":
                        context.Response.ContentType = "application/json";
                        totalCount = 0;
                        string _BookingCode = context.Request["BookingCode"] ?? "";
                        sqlWhere = " 1=1 AND Platform='TravPax'";
                        result = TravPaxBLL.Instance.GetBookingBySql("Booking ", " * ", " Id ", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                        break;
                    //查询订单（接口）
                    case "GetBookingDetail":
                        context.Response.ContentType = "application/json";
                        _BookingCode = context.Request["BookingCode"] ?? "";
                        string AgentReferenceNumber = context.Request["AgentReferenceNumber"] ?? "";
                        result = TravPaxBLL.Instance.GetBookingDetail(url, travPaxUsername, travPaxPassword, _BookingCode, AgentReferenceNumber, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                    //取消订单（接口）
                    case "BookingCancel":
                        context.Response.ContentType = "application/json";
                        _BookingCode = context.Request["BookingCode"] ?? "";
                        AgentReferenceNumber = context.Request["AgentReferenceNumber"] ?? "";
                        result = TravPaxBLL.Instance.BookingCancel(url, travPaxUsername, travPaxPassword, _BookingCode, AgentReferenceNumber, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                    //获取报价（供外部调用）最多一次搜索9条，大人数不能大于3，小孩数不能大于2
                    case "GetTravPaxRoomTypeOutPrice":
                        //IntOccupancyInfo occupancy=new List<IntOccupancyInfo>();
                        context.Response.ContentType = "application/json";
                        //string[] ArrayRoom = rooms.TrimEnd(';').Split(';');
                        string[] ArrayChilds;
                        IntOccupancyInfo occupancy = new IntOccupancyInfo();
                        string Age = string.Empty;
                        ArrayChilds = rooms.Split(',');
                        if (ArrayChilds[1] == "1")
                        {
                            Age += string.Format(@"{0}", ArrayChilds[2]);
                        }
                        else if (ArrayChilds[1] == "2")
                        {
                            Age += string.Format(@"{0},{1}", ArrayChilds[2], ArrayChilds[3]);
                        }
                        occupancy.adults = ArrayChilds[0].AsTargetType<int>(0);
                        occupancy.children = ArrayChilds[1].AsTargetType<int>(0);
                        occupancy.childAges = Age.TrimEnd(';');
                        result = TravPaxBLL.Instance.GetTravPaxRoomTypeOutPrice(url, travPaxUsername, travPaxPassword, hotelID, beginTime, endTime, occupancy, ddlRoom.AsTargetType<int>(0),"", VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        context.Response.Write(result);
                        break;
                }
            }
            context.Response.End();
        }

        //国籍
        public string GetTravPaxNationality()
        {
            string sql = "select ID,NAME from nationality ORDER BY NAME";
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        /// <summary>
        /// 发送
        /// </summary>
        /// <param name="data">相关参数</param>
        public DataTable RequestUrl(string requestData)
        {
            DataTable dt = new DataTable();
            string result = string.Empty;
            try
            {
                //发起请求
                result = Common.Common.PostHttp(url, requestData);
                //日记
                LogHelper.DoOperateLog(string.Format("Studio:TravPax接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", requestData, result, DateTime.Now.ToString()), "TravPax接口");
            }
            catch(Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Studio:TravPax接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, requestData, DateTime.Now.ToString()), "TravPax接口");
            }

            //TravPaxData travPaxData = new TravPaxData();
            //dt = travPaxData.DisposeData(result);
            return dt;
        }

        //获取酒店信息
        protected string GetHotel(string nationalityID, string destination, string beginTime, string endTime, int ddlRoom, string rooms, int input_page, int input_row)
        {
            string[] ArrayDestination = destination.Split(',');
            string cityID = ArrayDestination[0];
            string countryCode = ArrayDestination[1];
            string[] ArrayRoom = rooms.Split('；');
            string[] ArrayChilds;
            
            string strXml = string.Format(@"<CitySearchReq> <Username>{0}</Username> <Password>{1}</Password> <CheckInDate>{2}</CheckInDate> <CheckOutDate>{3}</CheckOutDate> <CountryID>{4}</CountryID> <CityID>{5}</CityID> <NationalityID>{6}</NationalityID> <Rooms>  "
                , travPaxUsername, travPaxPassword, "", "", countryCode, cityID, nationalityID);
            string childXml = string.Empty;
            for (int i = 0; i < ddlRoom; i++)
            {
                ArrayChilds = ArrayRoom[i].Split(',');
                if (ArrayChilds[1] == "1")
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2]);
                }
                else if (ArrayChilds[1] == "2")
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> <Age>{3}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2], ArrayChilds[3]);
                }
                else
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge/> </RoomRequest>", ArrayChilds[0], ArrayChilds[1]);
                }
            }
            string citySeachXml = strXml + childXml + " </Rooms> </CitySearchReq>";
            //请求接口
            //TravPaxPort tpp = new TravPaxPort();

            string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(citySeachXml), btnPost);
            
            DataTable cityDt = RequestUrl(postBoby);
            DataTable PageDt = Common.Common.GetPagedTable(cityDt, input_page, input_row);
            Hashtable hash = new Hashtable();
            hash.Add("total", cityDt.Rows.Count);
            hash.Add("rows", PageDt);
            return Newtonsoft.Json.JsonConvert.SerializeObject(hash); 
        }

        //获取酒店详情信息(缓存)
        protected string GetCachingHotelDetail(string hotelID, string countryID, string cityID, string referenceClient, string nationalityID, string beginTime, string endTime, int ddlRoom, string rooms, int input_page, int input_row)
        {
            string[] ArrayRoom = rooms.Split('；');
            string[] ArrayChilds;

            string strXml = string.Format(@"<HotelSearchReq> <Username>{0}</Username> <Password>{1}</Password> <ReferenceClient>{2}</ReferenceClient>
<CheckInDate>{3}</CheckInDate> <CheckOutDate>{4}</CheckOutDate> <CountryID>{5}</CountryID> <CityID>{6}</CityID> <HotelID>{7}</HotelID> <NationalityID>{8}</NationalityID> <Rooms>  "
                , travPaxUsername, travPaxPassword,"", beginTime, endTime,countryID, cityID, hotelID, nationalityID);
            string childXml = string.Empty;
            for (int i = 0; i < ddlRoom; i++)
            {
                ArrayChilds = ArrayRoom[i].Split(',');
                if (ArrayChilds[1] == "1")
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2]);
                }
                else if (ArrayChilds[1] == "2")
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> <Age>{3}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2], ArrayChilds[3]);
                }
                else
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge/> </RoomRequest>", ArrayChilds[0], ArrayChilds[1]);
                }
            }
            string hotelSearchXml = strXml + childXml + " </Rooms> </HotelSearchReq>";
            //请求接口
            //TravPaxPort tpp = new TravPaxPort();
            string postBoby = string.Format(@"__VIEWSTATE={0}&__VIEWSTATEGENERATOR={1}&__EVENTVALIDATION={2}&txtReq={3}&btnPost={4}", HttpUtility.UrlEncode(VIEWSTATE), HttpUtility.UrlEncode(VIEWSTATEGENERATOR), HttpUtility.UrlEncode(EVENTVALIDATION), HttpUtility.UrlEncode(hotelSearchXml), btnPost);
            DataTable dt = RequestUrl(postBoby);
            DataTable PageDt = Common.Common.GetPagedTable(dt, input_page, input_row);
            Hashtable hash = new Hashtable();
            hash.Add("total", dt.Rows.Count);
            hash.Add("rows", PageDt);
            return Newtonsoft.Json.JsonConvert.SerializeObject(hash);
        }

        //获取酒店详情信息(实时)
        protected string GetRealHotelDetail(string hotelID, string countryID, string cityID, string referenceClient, string nationalityID, string beginTime, string endTime, int ddlRoom, string rooms, int input_page, int input_row)
        {
            string[] ArrayRoom = rooms.Split('；');
            string[] ArrayChilds;

            string strXml = string.Format(@"<HotelRecheckPriceReq> <Username>{0}</Username> <Password>{1}</Password> <ReferenceClient>{2}</ReferenceClient>
<CheckInDate>{3}</CheckInDate> <CheckOutDate>{4}</CheckOutDate> <CountryID>{5}</CountryID> <CityID>{6}</CityID> <HotelID>{7}</HotelID> <NationalityID>{8}</NationalityID> <Rooms>  "
                , travPaxUsername, travPaxPassword, referenceClient, beginTime, endTime, countryID, cityID, hotelID, nationalityID);
            string childXml = string.Empty;
            for (int i = 0; i < ddlRoom; i++)
            {
                ArrayChilds = ArrayRoom[i].Split(',');
                if (ArrayChilds[1] == "1")
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2]);
                }
                else if (ArrayChilds[1] == "2")
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge> <Age>{2}</Age> <Age>{3}</Age> </ChildsAge> </RoomRequest>", ArrayChilds[0], ArrayChilds[1], ArrayChilds[2], ArrayChilds[3]);
                }
                else
                {
                    childXml += string.Format(@"<RoomRequest> <Adults>{0}</Adults> <Childs>{1}</Childs> <ChildsAge/> </RoomRequest>", ArrayChilds[0], ArrayChilds[1]);
                }
            }
            string citySeachXml = strXml + childXml + " </Rooms> </HotelRecheckPriceReq>";
            //请求接口
            //TravPaxPort tpp = new TravPaxPort();
            DataTable dt = RequestUrl(citySeachXml);
            DataTable PageDt = Common.Common.GetPagedTable(dt, input_page, input_row);
            Hashtable hash = new Hashtable();
            hash.Add("total", dt.Rows.Count);
            hash.Add("rows", PageDt);
            return Newtonsoft.Json.JsonConvert.SerializeObject(hash);
        }

        //获取目的地
        public string GetDestination(string strName)
        {
            string sql = string.Format(@"SELECT (CITYNAME+','+ CITY_DESTINATIONNAME +',' + NAME) as name,(CITY_ID+','+COUNTRY_CODE) as value FROM City_Destination where CITYNAME + CITY_DESTINATIONNAME + NAME like '%{0}%'", strName);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            IList<TableToLisModel> list = Common.Common.ModelConvertHelper<TableToLisModel>.ConvertToModel(dt);
            return Newtonsoft.Json.JsonConvert.SerializeObject(list);
        }

        public class TableToLisModel
        {
            public string value { get; set; }
            public string name { get; set; }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}