﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL;
using XiWan.DALFactory;
using XiWan.Model.Entities;

namespace XiWan.TravPax
{
    /// <summary>
    /// TravPax 的摘要说明
    /// </summary>
    public class TravPax : IHttpHandler
    {
        List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
        //获取配置中TravPax地址
        string url = CCommon.GetWebConfigValue("TravPaxUrl");
        string travPaxUsername = CCommon.GetWebConfigValue("TravPaxUsername");
        string travPaxPassword = CCommon.GetWebConfigValue("TravPaxPassword");
        string VIEWSTATE = CCommon.GetWebConfigValue("VIEWSTATE");

        string VIEWSTATEGENERATOR = CCommon.GetWebConfigValue("VIEWSTATEGENERATOR");
        string EVENTVALIDATION = CCommon.GetWebConfigValue("EVENTVALIDATION");
        string btnPost = CCommon.GetWebConfigValue("btnPost");
        string TravPaxCacheTime = CCommon.GetWebConfigValue("TravPaxCacheTime");
        string OrderNum = string.Empty;//订单号
        public void ProcessRequest(HttpContext context)
        {
            Respone resp = new Respone();
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string reqData = context.Request["data"] ?? "";
            string result = string.Empty;
            switch (type)
            {
                //获取报价  ReferenceClient传过来的为房型
                case "GetTravPaxRoomTypePrice":
                    /*IntOccupancyInfo ioi = new IntOccupancyInfo();
                    ioi.adults = 1;
                    ioi.children = 1;
                    ioi.childAges = "";
                    PriceRequest req = new PriceRequest();
                    req.hotelId = context.Request["hotelID"];
                    req.arrivalDate = context.Request["beginTime"];
                    req.departureDate = context.Request["endTime"];
                    req.IntOccupancyInfo = ioi;
                    req.rateplanId = "";
                    req.roomNum = context.Request["ddlRoom"].AsTargetType<int>(0);
                    result = TravPaxBLL.Instance.GetTravPaxRoomTypeOutPrice(url, travPaxUsername, travPaxPassword, req.hotelId, req.arrivalDate, req.departureDate, req.IntOccupancyInfo, req.roomNum, req.rateplanId, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                    */
                    if (reqData == "")
                    {
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    else
                    {
                        PriceRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            result = TravPaxBLL.Instance.GetTravPaxRoomTypeOutPrice(url, travPaxUsername, travPaxPassword, request.hotelId, request.arrivalDate,request.departureDate, request.IntOccupancyInfo,request.roomNum,request.rateplanId, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        }
                    }
                    context.Response.Write(result);
                    break;
                //创建订单
                case "Create_TravPaxOrder":
                    /*List<roomCustomers> roomCustomers = new List<roomCustomers>();

                    roomCustomers roomCustomer = new roomCustomers();
                    List<Customer> customers = new List<Customer>();
                    Customer tppObj1 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18, sex = 1 };
                    //Customer tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                    //Customer tppObj3 = new Customer() { firstName = "Ba", lastName = "Liang", name = "Ba Liang", age = 20, sex = 1 };

                    Customer tppObj3 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 10, sex = 1 };
                    //Customer tppObj4 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 12, sex = 1 };
                    customers.Add(tppObj1);
                    //customers.Add(tppObj2);
                    customers.Add(tppObj3);

                    //customers.Add(tppObj3);
                    //customers.Add(tppObj4);
                    roomCustomer.Customers = customers;
                    roomCustomers.Add(roomCustomer);


                    result = TravPaxBLL.Instance.Create_Order(url, travPaxUsername, travPaxPassword, "57653", "ef4fdb5d-e73e-49be-bab4-c2064ffc49fc", roomCustomers, "", "", "", 0, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                    */
                    if (reqData == "")
                    {
                        resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        CreateOrderRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateOrderRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else
                        {
                            result = TravPaxBLL.Instance.Create_Order(url, travPaxUsername, travPaxPassword, request.hotelId, request.ratePlanId, request.customers, request.inOrderNum, Convert.ToDateTime(request.arrivalDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(request.departureDate).ToString("yyyy-MM-dd"), request.roomNum, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                        }
                    }
                    context.Response.Write(result);
                    break;
                //取消订单
                case "Cancel_TravPaxOrder":
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "")
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = TravPaxBLL.Instance.Cancel_Order(url, travPaxUsername, travPaxPassword, OrderNum, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                    }
                    context.Response.Write(result);
                    break;
                //获取订单明细
                case "Get_TravPaxOrderDetail":
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "")
                    {
                        resp = new Respone() { code = "99", mes = "获取订单明细失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = TravPaxBLL.Instance.Get_OrderDetail(url, travPaxUsername, travPaxPassword, OrderNum, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                    }
                    context.Response.Write(result);
                    break;
            }
        }
        public class PriceRequest
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public class CreateOrderRequest
        {
            public string inOrderNum { get; set; }//传入订单号
            public string hotelId { get; set; }//酒店id
            public string roomTypeId { get; set; }//房型id
            public string ratePlanId { get; set; }//房型价格计划ID
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public int roomNum { get; set; }//房间数
            public List<roomCustomers> customers { get; set; }//每间房间入住客户信息
        }
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}