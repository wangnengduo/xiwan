﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL;
using XiWan.BLL.Restel;
using XiWan.DALFactory;
using XiWan.Model.Entities;

namespace XiWan.TravPax
{
    /// <summary>
    /// TravPax报价接口
    /// </summary>
    public class GetTravPaxRoomTypePrice : IHttpHandler
    {
        List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
        string url = CCommon.GetWebConfigValue("TravPaxUrl");
        string travPaxUsername = CCommon.GetWebConfigValue("TravPaxUsername");
        string travPaxPassword = CCommon.GetWebConfigValue("TravPaxPassword");
        string VIEWSTATE = CCommon.GetWebConfigValue("VIEWSTATE");
        string VIEWSTATEGENERATOR = CCommon.GetWebConfigValue("VIEWSTATEGENERATOR");
        string EVENTVALIDATION = CCommon.GetWebConfigValue("EVENTVALIDATION");
        string btnPost = CCommon.GetWebConfigValue("btnPost");
        string TravPaxCacheTime = CCommon.GetWebConfigValue("TravPaxCacheTime");
        public void ProcessRequest(HttpContext context)
        {
            /*IntOccupancyInfo occupancy = new IntOccupancyInfo();
            string hotelID = context.Request["hotelId"] ?? "";
            string beginTime = context.Request["arrivalDate"] ?? "";
            string endTime = context.Request["departureDate"] ?? "";
            string postOccupancy = context.Request["occupancy"] ?? "";
            int RoomCount = (context.Request["roomNum"] ?? "").AsTargetType<int>(1);
            string RateplanId = context.Request["RateplanId"] ?? "";
            //解析json
            occupancy = Newtonsoft.Json.JsonConvert.DeserializeObject<IntOccupancyInfo>(postOccupancy, new JsonSerializerSettings
            {
                Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                {
                    args.ErrorContext.Handled = true;
                }
            });*/
            string reqData = context.Request["data"] ?? "";
            string result = string.Empty;
            context.Response.ContentType = "application/json";
            if (reqData == "")
            {
                result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
            }
            else
            {
                Request request = Newtonsoft.Json.JsonConvert.DeserializeObject<Request>(reqData, new JsonSerializerSettings
                {
                    Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                    {
                        args.ErrorContext.Handled = true;
                    }
                });
                if (request == null)
                {
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                }
                else
                {
                    result = TravPaxBLL.Instance.GetTravPaxRoomTypeOutPrice(url, travPaxUsername, travPaxPassword, request.hotelId, request.arrivalDate, request.departureDate, request.IntOccupancyInfo, request.roomNum, request.rateplanId, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);

                }
            }
            context.Response.Write(result);   
        }

        public class Request
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}