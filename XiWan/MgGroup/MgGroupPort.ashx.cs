﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL.MgGroup;
using XiWan.Model.Entities;
using XiWan.DALFactory;
using System.Data;
using System.Collections;

namespace XiWan.MgGroup
{
    /// <summary>
    /// MgGroupPort 的摘要说明
    /// </summary>
    public class MgGroupPort : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string result = string.Empty;
            string HotelID = string.Empty;
            switch (type)
            {
                //从接口获取洲信息并保存到数据库
                case "GetHotels":
                    context.Response.ContentType = "text/plain";
                    result = MgGroupBLL.Instance.GetHotels();
                    context.Response.Write(result);
                    break;
                //获取国籍
                case "GetNationalityToSql":
                    result = MgGroupBLL.Instance.GetNationalityToSql();
                    context.Response.Write(result);
                    break;
                //获取目的地
                case "GetDestinationToSql":
                    string strName = context.Request["paramName"].ToString();
                    if (strName != "")
                        result = MgGroupBLL.Instance.GetDestinationToSql(strName);
                    context.Response.Write(result);
                    break;
                //酒店信息
                case "GetHotelToSql":
                    int input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    int input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    string _HotelName = context.Request["hotelName"] ?? "";
                    string _ddlGreadeCode = context.Request["ddlGreadeCode"] ?? "0";
                    string destination = context.Request["destination"] ?? "";
                    string CityCode = string.Empty;
                    string countryCode = string.Empty;
                    if (destination != "")
                    {
                        string[] ArrayDestination = destination.Split(',');
                        CityCode = ArrayDestination[0];
                        countryCode = ArrayDestination[1];
                    }
                    string sqlWhere = " 1=1 ";
                    if (CityCode != "" || countryCode != "")
                    {
                        if (CityCode != "")
                            CityCode += string.Format(@" and CityCode='{0}'", CityCode);
                        if (countryCode != "")
                            sqlWhere += string.Format(@" and countryCode='{0}'", countryCode);
                    }
                    else
                    {
                        sqlWhere += "and 1=0";
                    }
                    if (_HotelName != "")
                    {
                        sqlWhere += string.Format(@" and HotelName like '%{0}%'", _HotelName);
                    }
                    if (_ddlGreadeCode != "0")
                    {
                        sqlWhere += string.Format(@" and Rating={0}", _ddlGreadeCode);
                    }
                    int totalCount = 0;
                    result = MgGroupBLL.Instance.GetHotelToSql("MgGroupHotel", "*", "HotelName", input_row, input_page, sqlWhere, out totalCount);
                    context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                    break;
                //获取房型报价
                case "GetRoomTypePrice":
                    input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    string hotelID = context.Request["hotelID"] ?? "";
                    string beginTime = context.Request["beginTime"] ?? "";
                    string endTime = context.Request["endTime"] ?? "";
                    int RoomCount = (context.Request["ddlRoom"] ?? "").AsTargetType<int>(0);
                    string RateplanId = context.Request["RateplanId"] ?? "";
                    string rooms = context.Request["rooms"] ?? "";
                    List<IntOccupancyInfo> occupancys = new List<IntOccupancyInfo>();
                    string[] ArrayRooms = rooms.TrimEnd(';').Split(';');
                    for (int i = 0; i < RoomCount; i++)
                    {
                        IntOccupancyInfo occupancy=new IntOccupancyInfo();
                        occupancy.adults = ArrayRooms[i].AsTargetType<int>(0);
                        occupancys.Add(occupancy);
                    }
                    string rateplanId = context.Request["rateplanId"] ?? "";
                    string nationality = context.Request["nationality"] ?? "";
                    DataTable dt = MgGroupBLL.Instance.GetRoomTypePrice(hotelID, beginTime, endTime, occupancys, RoomCount, rateplanId, nationality);;
                    if (dt.Rows.Count == 0)
                    {
                        result = "{\"total\":0,\"rows\":[]}";
                    }
                    input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    DataTable PageDt = Common.Common.GetPagedTable(dt, input_page, input_row);
                    Hashtable hash = new Hashtable();
                    hash.Add("total", dt.Rows.Count);
                    hash.Add("rows", PageDt);
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(hash);
                    context.Response.Write(result);
                    break;
                //复查价格
                case "recheckPrice":
                    context.Response.ContentType = "text/plain";
                    hotelID = context.Request["hotelID"] ?? "";
                    beginTime = context.Request["beginTime"] ?? "";
                    endTime = context.Request["endTime"] ?? "";
                    RateplanId = context.Request["RateplanId"] ?? "";
                    rooms = context.Request["rooms"] ?? "";
                    RoomCount = (context.Request["ddlRoom"] ?? "").AsTargetType<int>(0);
                    List<IntOccupancyInfo> recheckOccupancys = new List<IntOccupancyInfo>();
                    ArrayRooms = rooms.TrimEnd(';').Split(';');
                    for (int i = 0; i < RoomCount; i++)
                    {
                        IntOccupancyInfo occupancy=new IntOccupancyInfo();
                        occupancy.adults = ArrayRooms[i].AsTargetType<int>(0);
                        recheckOccupancys.Add(occupancy);
                    }
                    if (MgGroupBLL.Instance.RecheckPrice(hotelID, beginTime, endTime, recheckOccupancys, RoomCount, RateplanId))
                    {
                        result = "1";
                    }
                    else
                    {
                        result = "0";
                    }
                    context.Response.Write(result);
                    break;
                //查看订单（本地数据库）
                case "Booking":
                    totalCount = 0;
                    string _BookingCode = context.Request["BookingCode"] ?? "";
                    input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    sqlWhere = " 1=1 AND Platform='MgGroup'";
                    result = MgGroupBLL.Instance.GetBookingBySql("Booking ", " * ", " Id ", input_row, input_page, sqlWhere, out totalCount);
                    context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                    break;
                //获取取消政策
                case "GetCancellationPolicy":
                    HotelID = context.Request["HotelID"].ToString();
                    RateplanId = context.Request["RateplanId"].ToString();
                    result = MgGroupBLL.Instance.GetHotelCancellationPolicy(HotelID, RateplanId);
                    context.Response.Write(result);
                    break;
                //创建订单
                case "book":
                    string roomArr = context.Request["roomArr"] ?? "";
                    RoomCount = (context.Request["RoomCount"] ?? "").AsTargetType<int>(0);
                    string[] arrayRooms = roomArr.TrimEnd('*').Split('*');
                    List<roomCustomers> roomCustomers = new List<roomCustomers>();
                    for (int i = 0; i < RoomCount; i++)
                    {
                        roomCustomers roomCustomer = new roomCustomers();
                        List<Customer> customers = new List<Customer>();
                        string[] bookArrayRooms = arrayRooms[i].Split(';');
                        string[] bookArrayRoomCount = bookArrayRooms[0].Split(',');
                        int people = bookArrayRoomCount[0].AsTargetType<int>(0);
                        if (people == 99)
                        {
                            people = 2;
                        }
                        for (int j = 0; j < people; j++)
                        {
                            string[] bookArrayRoom = bookArrayRooms[j].Split(',');
                            Customer tppObj = new Customer();
                            tppObj.firstName = bookArrayRoom[2];
                            tppObj.lastName = bookArrayRoom[3];
                            tppObj.age = bookArrayRoom[4].AsTargetType<int>(0);
                            if (bookArrayRoom[1] == "Mrs")
                            {
                                tppObj.sex = 2;
                            }
                            else if (bookArrayRoom[1] == "Mr")
                            {
                                tppObj.sex = 1;
                            }
                            else if (bookArrayRoom[1] == "Miss")
                            {
                                tppObj.sex = 3;
                            }
                            customers.Add(tppObj);
                        }
                        roomCustomer.Customers = customers;
                        roomCustomers.Add(roomCustomer);
                    }
                    hotelID = context.Request["HotelID"] ?? "";
                    RateplanId = context.Request["RateplanId"] ?? "";
                    beginTime = context.Request["beginTime"] ?? "";
                    endTime = context.Request["endTime"] ?? "";
                    result = MgGroupBLL.Instance.book(hotelID, RateplanId, roomCustomers, "", beginTime, endTime, 0);
                    context.Response.Write(result);
                    break;
                //取消订单
                case "Cancellation":
                    string BookingNumber = context.Request["BookingCode"] ?? "";
                    hotelID = context.Request["HotelID"] ?? "";
                    result = MgGroupBLL.Instance.cancel(hotelID, BookingNumber);
                    context.Response.Write(result);
                    break;
                //查看订单
                case "CheckingOrder":
                    hotelID = context.Request["HotelID"] ?? "";
                    BookingNumber = context.Request["BookingCode"] ?? "";
                    result = MgGroupBLL.Instance.AsianoverlandCheckingOrder(hotelID, BookingNumber);
                    context.Response.Write(result);
                    break;
                //查看订单
                case "GetBookingDetail":
                    hotelID = context.Request["HotelID"] ?? "";
                    string AgentBookingReference = context.Request["AgentBookingReference"] ?? "";
                    result = MgGroupBLL.Instance.AsianoverlandBookingDetail(hotelID, AgentBookingReference);
                    context.Response.Write(result);
                    break;
                //获取取消费用
                case "GetCancellationFee":
                    HotelID = context.Request["HotelID"].ToString();
                    BookingNumber = context.Request["BookingCode"].ToString();
                    result = MgGroupBLL.Instance.GetCancellationFee(HotelID, BookingNumber);
                    context.Response.Write(result);
                    break;
                //预订前是否做查看取消日期
                case "GetIsRecheckPrice":
                    context.Response.ContentType = "text/plain";
                    HotelID = context.Request["HotelID"].ToString();
                    RateplanId = context.Request["RateplanId"].ToString();
                    string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' AND RateplanId='{1}' and  Platform = 'MgGroup'", HotelID, RateplanId);
                    DataTable dtHp = ControllerFactory.GetController().GetDataTable(sql);
                    if (dtHp.Rows[0]["CheckID"].AsTargetType<string>("0") != "1")
                    {
                        result = "99";
                    }
                    context.Response.Write(result);
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}