﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace Survey.ashx
{
    /// <summary>
    /// treeHandler 的摘要说明
    /// </summary>
    public class treeHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string UserId=context.Request.QueryString["Id"];
            XiWan.BLL.Tb_Roles bll = new XiWan.BLL.Tb_Roles();
            List<XiWan.Model.Tb_Roles> list = bll.GetModelList("");
            XiWan.BLL.Tb_RolesAddUser rbll = new XiWan.BLL.Tb_RolesAddUser();
            XiWan.Model.Tb_RolesAddUser model = null;
            StringBuilder sb = new StringBuilder();
            sb.Append("[");
            foreach (XiWan.Model.Tb_Roles item in list)
            {
                model = rbll.GetModel(item.Id,UserId);
                sb.Append("{\"id\":"+item.Id+",");
                sb.Append("\"text\":\""+item.RolesName+"\"");
                if (model!=null)
                {
                    sb.Append(",\"checked\":true");
                }
                sb.Append("},");
            }
            sb.Remove(sb.Length-1,1);
            sb.Append("]");
            context.Response.Write(sb.ToString());
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}