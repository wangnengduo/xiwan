﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using XiWan.DBUtility;
using System.Data;

namespace Survey.ashx
{
    /// <summary>
    /// UserListHandler 的摘要说明
    /// </summary>
    public class UserListHandler : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            int row = int.Parse(context.Request["rows"].ToString());
            int page = int.Parse(context.Request["page"].ToString());
            int orgId = 0;
            if (context.Request["OrgId"] != null)
            {
                orgId = int.Parse(context.Request["OrgId"].ToString());
            }
            string strWhere = "";
            XiWan.BLL.Com_Organization obll = new XiWan.BLL.Com_Organization();
            List<XiWan.Model.Com_Organization> olist = obll.GetModelList("");
            StringBuilder sb2 = new StringBuilder();
            sb2.Append(orgId);
            sb2 = GetId(orgId, sb2, olist);
            strWhere = "UserId in (select UserId from Com_OrgAddUser where OrgId in (" + sb2.ToString() + "))";
            XiWan.BLL.View_Users bll = new XiWan.BLL.View_Users();
            List<XiWan.Model.View_Users> list = bll.GetList(row, page, strWhere, " Userid desc");
            StringBuilder sb = new StringBuilder();
            if (list.Count == 0)
            {
                sb.Append("{\"total\":0,\"rows\":[]}");
            }
            else
            {
                int count = bll.Selcount("");
                sb.Append("{\"total\":" + count + ",\"rows\":[");
                foreach (XiWan.Model.View_Users model in list)
                {
                    sb.Append("{\"Userid\":\"" + model.Userid + "\",");
                    sb.Append("\"LoginName\":\"" + model.LoginName + "\",");
                    sb.Append("\"UserRealName\":\"" + model.UserRealName + "\",");
                    sb.Append("\"Pass\":\"" + model.LoginPassword + "\",");
                    sb.Append("\"Mail\":\"" + model.Email + "\",");
                    sb.Append("\"Mobile\":\"" + model.Mobile + "\",");
                    sb.Append("\"Tel\":\"" + model.Tel + "\",");
                    sb.Append("\"Sex\":\"" + model.Sex + "\",");
                    sb.Append("\"Status\":\"" + model.Status + "\",");
                    string sql = "select Agency from Com_Organization where Id = (select OrgId from Com_OrgAddUser where UserId=" + model.Userid + ")";
                    object obj = DbHelperSQL.GetSingle(sql);
                    sb.Append("\"OrgId\":\"" + obj + "\",");
                    //string sql2 = "select Id,RolesName from Tb_Roles where Id = (select RolesId from Tb_RolesAddUser where UserId=" + model.Userid + ")";
                    //DataSet ds = DbHelperSQL.Query(sql2);
                    sb.Append("\"Email\":\"" + model.Email + "\",");
                    if (model.Status == 0)
                        sb.Append("\"Status\":\"未启用\"");
                    else if (model.Status == 1)
                        sb.Append("\"Status\":\"正常\"");
                    else
                        sb.Append("\"Status\":\"禁止登录\"");
                    sb.Append("},");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.Append("]}");
            }
            context.Response.Write(sb.ToString());
        }
        public StringBuilder GetId(int ParentId, StringBuilder sb, List<XiWan.Model.Com_Organization> list)
        {
            foreach (XiWan.Model.Com_Organization item in list)
            {
                if (item.ParentId == ParentId)
                {
                    if (sb.Length != 0)
                        sb.Append(",");
                    sb.Append("'" + item.Id + "'");
                    sb = GetId(item.Id, sb, list);
                }
            }
            return sb;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}