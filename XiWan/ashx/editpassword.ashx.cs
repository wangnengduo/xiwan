﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Survey.ashx
{
    /// <summary>
    /// editpassword 的摘要说明
    /// </summary>
    public class editpassword : IHttpHandler,IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            string pass = context.Request.QueryString["newpass"];
            XiWan.Model.View_Users viewUser = context.Session["User"] as XiWan.Model.View_Users;
            XiWan.BLL.Com_UserLogin bll = new XiWan.BLL.Com_UserLogin();
            XiWan.Model.Com_UserLogin model = bll.GetModel(viewUser.Userid);
            model.LoginPassword = XiWan.DBUtility.DESEncrypt.Encrypt(pass);
            if (bll.Update(model))
            {
                context.Response.Write("true");
            }
            else {
                context.Response.Write("false");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}