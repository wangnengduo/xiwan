﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;

namespace Survey.ashx
{
    /// <summary>
    /// loginout 的摘要说明
    /// </summary>
    public class loginout : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            XiWan.Model.View_Users viewUser = context.Session["User"] as XiWan.Model.View_Users;
            XiWan.BLL.Com_LoginLog lbll = new XiWan.BLL.Com_LoginLog();
            XiWan.Model.Com_LoginLog lmodel = new XiWan.Model.Com_LoginLog();
            lmodel.LoginDate = DateTime.Now;
            lmodel.LoginIP =  HttpContext.Current.Request.UserHostAddress;
            lmodel.Status = "1";
            if (viewUser != null)
            {
                lmodel.Userid = viewUser.Userid;
                lbll.Add(lmodel);
            }
            context.Session["User"] = null;
            context.Response.Redirect("../login.aspx");
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}