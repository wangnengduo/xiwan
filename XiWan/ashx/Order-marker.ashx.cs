﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;
using System.Web;
using XiWan.DALFactory;

namespace XiWan.ashx
{
    /// <summary>
    /// Order_marker 的摘要说明
    /// </summary>
    public class Order_marker : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string result = string.Empty;
            if (!string.IsNullOrEmpty(type))
            {
                switch (type)
                {
                    case "BookingData":
                        result = GetBookingData();
                        context.Response.Write(result);
                        break;
                    case "BookingAmount":
                        result = GetBookingAmount();
                        context.Response.Write(result);
                        break;
                }
            }
        }
        /// <summary>
        /// 获得数据且用Json格式数据返回
        /// </summary>
        private string GetBookingData()
        {
            //测试并发
            //for (int i = 0; i < 1000; i++)
            //{
            //    ThreadPool.QueueUserWorkItem(a =>
            //    {
            //        LogHelper.DoOperateLog(string.Format("Err：Agoda预订 ,Err :{0} ， Time:{1} ", "预订xml解析出错", DateTime.Now.ToString()), "Agoda接口");
            //    });
            //}

            string sql = string.Format(@"with t as (
select datename(weekday,getdate()- t1.number) weeknum
,substring(convert(varchar,getdate()- t1.number,120),1,11) daynum
 from (select number  from MASTER..spt_values WHERE TYPE='P' AND number >=0 and number <=6  ) t1)
 select weeknum,CONVERT(VARCHAR,DATEPART(MM,daynum))+'月' + CONVERT(VARCHAR,DATEPART(DD,daynum))+'日' AS dayTime,
  (select count(*) from Restel_Order where substring(convert(varchar,CreatTime,120),1,11)=daynum and LegID!=99) as RestelOrder ,
  (select count(*) from SunSeries_Order where substring(convert(varchar,CreatTime,120),1,11)=daynum and Status=1) as SunSeriesOrder,
  (select count(*) from Agoda_Order where substring(convert(varchar,CreatTime,120),1,11)=daynum and Status=1) as AgodaOrder  
 from t ORDER BY t.daynum");
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            string dayTime = string.Empty;
            string RestelOrder = string.Empty;
            string SunSeriesOrder = string.Empty;
            string AgodaOrder = string.Empty;
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    dayTime += "\"" + dt.Rows[i]["dayTime"].AsTargetType<string>("") + "\"" + ",";
                    RestelOrder += dt.Rows[i]["RestelOrder"].AsTargetType<string>("") + ",";
                    SunSeriesOrder += dt.Rows[i]["SunSeriesOrder"].AsTargetType<string>("") + ",";
                    AgodaOrder += dt.Rows[i]["AgodaOrder"].AsTargetType<string>("") + ",";
                }
            }
            string result ="{\"dayTime\":[" + string.Format("{0}", dayTime.TrimEnd(',')) 
+ "],\"RestelOrder\":[" + string.Format("{0}", RestelOrder.TrimEnd(',')) + "],\"SunSeriesOrder\":[" + string.Format("{0}", SunSeriesOrder.TrimEnd(','))
+ "],\"AgodaOrder\":[" + string.Format("{0}", AgodaOrder.TrimEnd(',')) + "]}";
            return result;
        }
        public string GetBookingAmount()
        {
            string sql = string.Format(@"select count(*) AS count from Restel_Order where LegID!=99 union ALL select count(*) AS count from SunSeries_Order where Status=1
 union ALL select count(*) AS count from Agoda_Order where Status=1");
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            int RestelCount = 0;
            int SunSeriesCount = 0;
            int AgodaCount = 0;
            RestelCount = dt.Rows[0]["count"].AsTargetType<int>(0);
            SunSeriesCount = dt.Rows[1]["count"].AsTargetType<int>(0);
            AgodaCount = dt.Rows[2]["count"].AsTargetType<int>(0);
            string result = "{\"RestelCount\":" + string.Format("{0}", RestelCount) + ",\"SunSeriesCount\":" + string.Format("{0}", SunSeriesCount) + ",\"AgodaCount\":" + string.Format("{0}", AgodaCount) + "}";
            return result;
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}