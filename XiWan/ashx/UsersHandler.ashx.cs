﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.SessionState;
using System.Data;

namespace Survey.ashx
{
    /// <summary>
    /// UsersHandler 的摘要说明
    /// </summary>
    public class UsersHandler : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            XiWan.BLL.View_Users bll = new XiWan.BLL.View_Users();
            if (context.Request.QueryString["type"] == "edit")//获取要编辑的用户信息
            { 
                string Userid=context.Request.QueryString["Id"];
                DataSet ds = XiWan.DBUtility.DbHelperSQL.Query("select Id,RolesName from Tb_Roles where Id = (select RolesId from Tb_RolesAddUser where UserId=" + Userid + ")");
                string IdList = "";
                foreach (DataRow dr in ds.Tables[0].Rows) {
                    if (IdList != "")
                        IdList += ",";
                    IdList += dr["Id"].ToString() ;
                }
                context.Response.Write(IdList);  
            }
            else if (context.Request.QueryString["type"] == "sel")//浏览用户信息
            {
                string Userid = context.Request.QueryString["Id"];
                DataSet ds = XiWan.DBUtility.DbHelperSQL.Query("select Id,RolesName from Tb_Roles where Id = (select RolesId from Tb_RolesAddUser where UserId=" + Userid + ")");
                string IdList = "";
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (IdList != "")
                        IdList += ",";
                    IdList += dr["RolesName"].ToString();
                }
                context.Response.Write(IdList);  
            }
            else if (context.Request.QueryString["type"] == "del")//删除用户信息，同时删除用户角色表，用户部门表
            {
                string Userid = context.Request.QueryString["Id"];
                string sql = "delete Com_UserInfos where Userid='"+Userid+"'";
                string sql2 = "delete Com_UserLogin where Userid='" + Userid + "'";
                string sql3 = "delete Com_OrgAddUser where UserId='" + Userid + "'";
                string sql4 = "delete Tb_RolesAddUser where UserId='" + Userid + "'";
                List<string> list = new List<string>();
                list.Add(sql);
                list.Add(sql2);
                list.Add(sql3);
                list.Add(sql4);
                if (XiWan.DBUtility.DbHelperSQL.ExecuteSqlTran(list) > 0)
                {
                    context.Response.Write("true");
                }
                else
                {
                    context.Response.Write("false");
                }
            }
            else if (context.Request.QueryString["type"] == "pass")//修改密码
            {
                string Userid = context.Request.QueryString["UserId"];
                string pass = context.Request.QueryString["pass"];
                XiWan.BLL.Com_UserLogin bll2 = new XiWan.BLL.Com_UserLogin();
                XiWan.Model.Com_UserLogin model = new XiWan.Model.Com_UserLogin();
                model = bll2.GetModel(Userid);
                model.LoginPassword = XiWan.DBUtility.DESEncrypt.Encrypt(pass);
                if (bll2.Update(model))
                {
                    context.Response.Write("true");
                }
                else
                {
                    context.Response.Write("false");
                }
            }
            else if (context.Request.QueryString["type"] == "save")//保存编辑信息，或新增用户信息
            {
                string login = context.Request.QueryString["login"];
                string pass = context.Request.QueryString["pass"];
                string name = context.Request.QueryString["name"];
                string sex = context.Request.QueryString["sex"];
                string email = context.Request.QueryString["email"];
                string tel = context.Request.QueryString["tel"];
                string mobile = context.Request.QueryString["mobile"];
                string status = context.Request.QueryString["status"];
                string org = context.Request.QueryString["org"];
                string role = context.Request.QueryString["role"];
                XiWan.BLL.Com_UserInfos bll1 = new XiWan.BLL.Com_UserInfos();
                XiWan.BLL.Com_UserLogin bll2 = new XiWan.BLL.Com_UserLogin();
                XiWan.Model.Com_UserInfos item1 = new XiWan.Model.Com_UserInfos();
                XiWan.Model.Com_UserLogin model = new XiWan.Model.Com_UserLogin();
                string Userid = context.Request.QueryString["Userid"];
                if (Userid != null && Userid != "")
                {
                    item1 = bll1.GetModel(Userid);
                    model = bll2.GetModel(Userid);
                }
                else
                {
                    Userid = bll1.GetMaxId().ToString();
                    if (Userid == "1")
                    {
                        Userid = "1000000000";
                    }
                    item1.AddDate = DateTime.Now;
                }
                item1.Email = email;
                item1.Mobile = mobile;
                item1.Sex = sex;
                item1.Tel = tel;
                item1.Userid = Userid;
                item1.UserRealName = name;

                model.LoginName = login;
                model.LoginPassword = XiWan.DBUtility.DESEncrypt.Encrypt(pass);
                model.Status = int.Parse(status);
                model.UserId = Userid;
                XiWan.BLL.Com_OrgAddUser obll = new XiWan.BLL.Com_OrgAddUser();
                obll.Delete(Userid);
                if (org != null && org != "")
                {
                    XiWan.Model.Com_OrgAddUser omodel = new XiWan.Model.Com_OrgAddUser();
                    omodel.OrgId = int.Parse(org);
                    omodel.UserId = Userid;
                    obll.Add(omodel);
                }
                if (context.Request.QueryString["Userid"] != null && context.Request.QueryString["Userid"] != "")
                {
                    bll1.Update(item1);
                    bll2.Update(model);
                }
                else
                {
                    if (bll2.Exists(login))
                    {
                        obll.Delete(Userid);
                        context.Response.Write("false");
                    }
                    else
                    {
                        bll1.Add(item1);
                        bll2.Add(model);
                    }
                }
                saveRole(Userid,role);
            }
        }
        public bool saveRole(string Userid, string role)
        {
            List<string> list = new List<string>();
            string sql2 = "delete Tb_RolesAddUser where UserId='" + Userid + "'";
            list.Add(sql2);
            if (role != null && role != "")
            {
                string[] str = role.Split(',');
                foreach (string s in str)
                {
                    string sql = "insert into Tb_RolesAddUser(RolesId,UserId) values(" + s + ",'" + Userid + "')";
                    list.Add(sql);
                }
            }
            if (XiWan.DBUtility.DbHelperSQL.ExecuteSqlTran(list) > 0)
                return true;
            else
                return false;
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}