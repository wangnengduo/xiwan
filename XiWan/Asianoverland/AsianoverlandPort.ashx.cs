﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.BLL.Asianoverland;
using XiWan.Model.Entities;
using XiWan.DALFactory;

namespace XiWan.Asianoverland
{
    /// <summary>
    /// AsianoverlandPort 的摘要说明
    /// </summary>
    public class AsianoverlandPort : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            string result = string.Empty;
            context.Response.ContentType = "application/json";
            int input_page = Convert.ToInt16(context.Request["page"] ?? "0");
            int input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
            if (!string.IsNullOrEmpty(context.Request["type"]))
            {
                switch (context.Request["type"])
                {
                    //获取国家
                    case "GetCountry":
                        AsianoverlandBLL.Instance.GetCountry();
                        context.Response.Write("成功");
                        break;
                    //获取市
                    case "GetCity":
                        AsianoverlandBLL.Instance.GetCity();
                        context.Response.Write("成功");
                        break;
                    //获取国籍
                    case "GetNationality":
                        AsianoverlandBLL.Instance.GetNationality();
                        context.Response.Write("成功");
                        break;
                    //获取酒店
                    case "GetHotels":
                        AsianoverlandBLL.Instance.GetHotels();
                        context.Response.Write("成功");
                        break;
                    //从接口获取酒店详细信息并保存到数据库
                    case "GetHoteldetails":
                        AsianoverlandBLL.Instance.GetHoteldetails();
                        context.Response.Write("成功");
                        break;
                    //获取国籍
                    case "GetNationalityToSql":
                        result = AsianoverlandBLL.Instance.GetNationalityToSql();
                        context.Response.Write(result);
                        break;
                    //获取目的地
                    case "GetDestinationToSql":
                        string strName = context.Request["paramName"].ToString();
                        if (strName != "")
                            result = AsianoverlandBLL.Instance.GetDestinationToSql(strName);
                        context.Response.Write(result);
                        break;
                    //酒店信息
                    case "GetHotelToSql":
                        string _HotelName = context.Request["hotelName"] ?? "";
                        string _ddlGreadeCode = context.Request["ddlGreadeCode"] ?? "0";
                        string destination = context.Request["destination"] ?? "";
                        string CityCode = string.Empty;
                        string countryCode = string.Empty;
                        if (destination != "")
                        {
                            string[] ArrayDestination = destination.Split(',');
                            CityCode = ArrayDestination[0];
                            countryCode = ArrayDestination[1];
                        }
                        string sqlWhere = " 1=1 ";
                        if (CityCode != "" || countryCode != "")
                        {
                            if (CityCode != "")
                                CityCode += string.Format(@" and CityCode='{0}'", CityCode);
                            if (countryCode != "")
                                sqlWhere += string.Format(@" and countryCode='{0}'", countryCode);
                        }
                        else
                        {
                            sqlWhere += "and 1=0";
                        }
                        if (_HotelName != "")
                        {
                            sqlWhere += string.Format(@" and HotelName like '%{0}%'", _HotelName);
                        }
                        if (_ddlGreadeCode != "0")
                        {
                            sqlWhere += string.Format(@" and Rating={0}", _ddlGreadeCode);
                        }
                        int totalCount = 0;
                        result = AsianoverlandBLL.Instance.GetHotelToSql("AsianoverlandHotel", "*", "HotelName", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                        break;
                    //获取报价
                    case "GetHotelQuote":
                        List<IntOccupancyInfo> occupancys=new List<IntOccupancyInfo>();
                        string hotelID = context.Request["hotelID"] ?? "";
                        string beginTime = context.Request["beginTime"] ?? "";
                        string endTime = context.Request["endTime"] ?? "";
                        int RoomCount = (context.Request["ddlRoom"] ?? "").AsTargetType<int>(0);
                        string RateplanId = context.Request["RateplanId"] ?? "";
                        string rooms = context.Request["rooms"] ?? "";
                        string[] ArrayRooms = rooms.TrimEnd(';').Split(';');
                        for (int i = 0; i < RoomCount; i++)
                        {
                            IntOccupancyInfo occupancy=new IntOccupancyInfo();
                            string Age = string.Empty;
                            string[] ArrayRoom = ArrayRooms[i].Split(',');
                            occupancy.adults=ArrayRoom[0].AsTargetType<int>(0);
                            occupancy.children=ArrayRoom[1].AsTargetType<int>(0);
                            if (ArrayRoom[1] == "1")
                            {
                                occupancy.childAges=ArrayRoom[2];
                            }
                            else if (ArrayRoom[1] == "2")
                            {
                                occupancy.childAges=ArrayRoom[2] + "," + ArrayRoom[3];
                            }
                            occupancys.Add(occupancy);
                        }
                        string nationality = context.Request["nationality"] ?? "";
                        string countryResidence = context.Request["countryResidence"] ?? "";
                        DataTable dt = AsianoverlandBLL.Instance.GetHotelQuote(hotelID, beginTime, endTime, occupancys, RoomCount, RateplanId, nationality,countryResidence);
                        if (dt.Rows.Count == 0)
                        {
                            result = "{\"total\":0,\"rows\":[]}";
                        }
                        input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                        input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                        DataTable PageDt = Common.Common.GetPagedTable(dt, input_page, input_row);
                        Hashtable hash = new Hashtable();
                        hash.Add("total", dt.Rows.Count);
                        hash.Add("rows", PageDt);
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(hash);
                        context.Response.Write(result);
                    break;
                    //获取取消政策
                    case "GetCancellationPolicy":
                        string HotelID = context.Request["HotelID"].ToString();
                        RateplanId = context.Request["RateplanId"].ToString();
                        result = AsianoverlandBLL.Instance.GetHotelCancellationPolicy(HotelID, RateplanId);
                        context.Response.Write(result); 
                    break;
                    //取消订单
                    case "Cancellation":
                        string BookingNumber = context.Request["BookingCode"] ?? "";
                        hotelID = context.Request["HotelID"] ?? "";
                        result = AsianoverlandBLL.Instance.cancel(hotelID, BookingNumber);
                        context.Response.Write(result);
                    break;
                    //查看订单
                    case "CheckingOrder":
                        hotelID = context.Request["HotelID"] ?? "";
                        BookingNumber = context.Request["BookingCode"] ?? "";
                        result = AsianoverlandBLL.Instance.AsianoverlandCheckingOrder(hotelID, BookingNumber);
                        context.Response.Write(result);
                    break;
                    //查看订单
                    case "GetBookingDetail":
                        hotelID = context.Request["HotelID"] ?? "";
                        string AgentBookingReference = context.Request["AgentBookingReference"] ?? "";
                        result = AsianoverlandBLL.Instance.AsianoverlandBookingDetail(hotelID, AgentBookingReference);
                        context.Response.Write(result);
                        break; 
                    //查看订单（本地数据库）
                    case "Booking":
                        totalCount = 0;
                        string _BookingCode = context.Request["BookingCode"] ?? "";
                        input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                        input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                        sqlWhere = " 1=1 AND Platform='Asianoverland'";
                        result = AsianoverlandBLL.Instance.GetBookingBySql("Booking ", " * ", " Id ", input_row, input_page, sqlWhere, out totalCount);
                        context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                    break;
                    //创建订单
                    case "book":
                        string roomArr = context.Request["roomArr"] ?? "";
                        RoomCount = (context.Request["RoomCount"] ?? "").AsTargetType<int>(0);
                        string[] arrayRooms = roomArr.TrimEnd('*').Split('*');
                        List<roomCustomers> roomCustomers = new List<roomCustomers>();
                        for (int i = 0; i < RoomCount; i++)
                        {
                            roomCustomers roomCustomer = new roomCustomers();
                            List<Customer> customers = new List<Customer>();
                            string[] bookArrayRooms = arrayRooms[i].Split(';');
                            string[] bookArrayRoomCount = bookArrayRooms[0].Split(',');
                            int people = bookArrayRoomCount[0].AsTargetType<int>(0) + bookArrayRoomCount[1].AsTargetType<int>(0);
                            for (int j = 0; j < people; j++)
                            {
                                string[] bookArrayRoom = bookArrayRooms[j].Split(',');
                                Customer tppObj = new Customer();
                                tppObj.firstName = bookArrayRoom[3];
                                tppObj.lastName = bookArrayRoom[4];
                                tppObj.age = bookArrayRoom[5].AsTargetType<int>(0);
                                if (bookArrayRoom[2] == "Mrs")
                                {
                                    tppObj.sex = 2;
                                }
                                else if (bookArrayRoom[2] == "Mr")
                                {
                                    tppObj.sex = 1;
                                }
                                else if (bookArrayRoom[2] == "Child")
                                {
                                    tppObj.sex = 3;
                                }
                                customers.Add(tppObj);
                            }
                            roomCustomer.Customers = customers;
                            roomCustomers.Add(roomCustomer);
                        }
                            /*roomCustomers roomCustomer = new roomCustomers();
                            List<Customer> customers = new List<Customer>();
                            Customer tppObj1 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18, sex = 1 };
                            //Customer tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                            //Customer tppObj3 = new Customer() { firstName = "Ba", lastName = "Liang", name = "Ba Liang", age = 20, sex = 1 };
                            //Customer tppObj4 = new Customer() { firstName = "ShiYi", lastName = "Xie", name = "ShiYi Xie", age = 20, sex = 1 };

                            //Customer tppObj3 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 10, sex = 1 };
                            //Customer tppObj4 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 12, sex = 1 };
                            customers.Add(tppObj1);
                            //customers.Add(tppObj2);
                            //customers.Add(tppObj3);

                            //customers.Add(tppObj3);
                            //customers.Add(tppObj4);
                            roomCustomer.Customers = customers;
                            roomCustomers.Add(roomCustomer);

                            roomCustomers roomCustomer2 = new roomCustomers();
                            List<Customer> customers2 = new List<Customer>();
                            // tppObj12 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18, sex = 1 };
                            //Customer tppObj22 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                            //Customer tppObj32 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };

                            Customer tppObj32 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };
                            //Customer tppObj42 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 21, sex = 1 };
                            //Customer tppObj52 = new Customer() { firstName = "Jiu", lastName = "Liu", name = "Jiu Liu", age = 20, sex = 1 };
                            //Customer tppObj62 = new Customer() { firstName = "Shi", lastName = "Chen", name = "Shi Chen", age = 20, sex = 1 };
                            //.Add(tppObj12);
                            //customers.Add(tppObj22);
                            //customers.Add(tppObj32);

                            customers2.Add(tppObj32);
                            //customers2.Add(tppObj42);
                            //customers2.Add(tppObj52);
                            //customers2.Add(tppObj62);
                            roomCustomer2.Customers = customers2;
                            roomCustomers.Add(roomCustomer2);

                            //3
                            roomCustomers roomCustomer3 = new roomCustomers();
                            List<Customer> customers3 = new List<Customer>();

                            Customer tppObj43 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 31, sex = 1 };

                            customers3.Add(tppObj43);
                            roomCustomer3.Customers = customers3;
                            //roomCustomers.Add(roomCustomer3);

                            //4
                            roomCustomers roomCustomer4 = new roomCustomers();
                            List<Customer> customers4 = new List<Customer>();

                            Customer tppObj74 = new Customer() { firstName = "Si", lastName = "Zhang", name = "Si Zhang", age = 41, sex = 1 };

                            customers4.Add(tppObj74);
                            roomCustomer4.Customers = customers4;
                            //roomCustomers.Add(roomCustomer4);

                            //5
                            roomCustomers roomCustomer5 = new roomCustomers();
                            List<Customer> customers5 = new List<Customer>();

                            Customer tppObj75 = new Customer() { firstName = "Wu", lastName = "Zhang", name = "Wu Zhang", age = 51, sex = 1 };

                            customers5.Add(tppObj75);
                            roomCustomer5.Customers = customers5;
                            //roomCustomers.Add(roomCustomer5);

                            //6
                            roomCustomers roomCustomer6 = new roomCustomers();
                            List<Customer> customers6 = new List<Customer>();

                            Customer tppObj76 = new Customer() { firstName = "Liu", lastName = "Zhang", name = "Liu Zhang", age = 61, sex = 1 };

                            customers6.Add(tppObj76);
                            roomCustomer6.Customers = customers6;
                            //roomCustomers.Add(roomCustomer6);

                            //7
                            roomCustomers roomCustomer7 = new roomCustomers();
                            List<Customer> customers7 = new List<Customer>();

                            Customer tppObj77 = new Customer() { firstName = "Qi", lastName = "Zhang", name = "Qi Zhang", age = 71, sex = 1 };

                            customers7.Add(tppObj77);
                            roomCustomer7.Customers = customers7;
                            //roomCustomers.Add(roomCustomer7);

                            //8
                            roomCustomers roomCustomer8 = new roomCustomers();
                            List<Customer> customers8 = new List<Customer>();

                            Customer tppObj78 = new Customer() { firstName = "Ba", lastName = "Zhang", name = "Ba Zhang", age = 81, sex = 1 };

                            customers8.Add(tppObj78);
                            roomCustomer8.Customers = customers8;
                            //roomCustomers.Add(roomCustomer8);

                            //9
                            roomCustomers roomCustomer9 = new roomCustomers();
                            List<Customer> customers9 = new List<Customer>();

                            Customer tppObj79 = new Customer() { firstName = "Jiu", lastName = "Zhang", name = "Jiu Zhang", age = 91, sex = 1 };

                            customers9.Add(tppObj79);
                            roomCustomer9.Customers = customers9;
                            //roomCustomers.Add(roomCustomer9);

                            //10
                            roomCustomers roomCustomer10 = new roomCustomers();
                            List<Customer> customers10 = new List<Customer>();

                            Customer tppObj710 = new Customer() { firstName = "Shi", lastName = "Zhang", name = "Shi Zhang", age = 101, sex = 1 };

                            customers10.Add(tppObj710);
                            roomCustomer10.Customers = customers10;
                            //roomCustomers.Add(roomCustomer10);


                            // tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };*/

                        hotelID = context.Request["HotelID"] ?? "";
                        RateplanId = context.Request["RateplanId"] ?? "";
                        beginTime = context.Request["beginTime"] ?? "";
                        endTime = context.Request["endTime"] ?? "";
                        result = AsianoverlandBLL.Instance.book(hotelID, RateplanId, roomCustomers, "", beginTime, endTime, 0);
                        context.Response.Write(result);
                    break;
                    //获取取消费用
                    case "GetCancellationFee":
                        HotelID = context.Request["HotelID"].ToString();
                        BookingNumber = context.Request["BookingCode"].ToString();
                        result = AsianoverlandBLL.Instance.GetCancellationFee(HotelID, BookingNumber);
                        context.Response.Write(result);
                    break;
                    //预订前是否做查看取消日期
                    case "GetIsRecheckPrice":
                    context.Response.ContentType = "text/plain";
                        HotelID = context.Request["HotelID"].ToString();
                        RateplanId = context.Request["RateplanId"].ToString();
                        string sql = string.Format(@"select top 1 * from HotelPrice WITH(NOLOCK) WHERE HotelID='{0}' AND RateplanId='{1}' and  Platform = 'Asianoverland'", HotelID, RateplanId);
                        DataTable dtHp = ControllerFactory.GetController().GetDataTable(sql);
                        if (dtHp.Rows[0]["CheckID"].AsTargetType<string>("0") != "1")
                        {
                            result = "99";
                        }
                        context.Response.Write(result);
                    break;
                }
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}