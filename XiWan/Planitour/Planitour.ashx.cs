﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.IO;
using System.Text;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Data.Redis;
using XiWan.BLL;

namespace XiWan.Planitour
{
    /// <summary>
    /// Planitour 的摘要说明
    /// </summary>
    public class Planitour : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            string result = string.Empty;
            string url = string.Empty;
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            if (type != "")
            {
                switch (type)
                {
                    //国籍（从对方系统拿数据）
                    case "GetNationality":
                        context.Response.ContentType = "application/text";
                        result = PlanitourBLL.Instance.GetCountries(url);
                        context.Response.Write(result);
                        break;
                    //目的地（从对方系统拿数据）
                    case "GetDestinations":
                        context.Response.ContentType = "application/text";
                        int countryid = 0;
                        result = PlanitourBLL.Instance.GetDestinations(url,countryid);
                        context.Response.Write(result);
                        break; 
                }
            }
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}