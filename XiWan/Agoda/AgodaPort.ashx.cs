﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL.Agoda;
using XiWan.Model.Entities;
using XiWan.DALFactory;
using System.Data;
using System.Collections;

namespace XiWan.Agoda
{
    /// <summary>
    /// AgodaPort 的摘要说明
    /// </summary>
    public class AgodaPort : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string result = string.Empty;
            string HotelID = string.Empty;
            switch (type)
            {
                //从接口获取洲信息并保存到数据库
                case "GetContinent":
                    context.Response.ContentType = "text/plain";
                    result = AgodaBLL.Instance.GetContinent();
                    context.Response.Write(result);
                    break;
                //从接口获取国家信息并保存到数据库
                case "GetCountry":
                    context.Response.ContentType = "text/plain";
                    result = AgodaBLL.Instance.GetCountry();
                    context.Response.Write(result);
                    break;
                //从接口获取市信息并保存到数据库
                case "GetCity":
                    context.Response.ContentType = "text/plain";
                    result = AgodaBLL.Instance.GetCity();
                    context.Response.Write(result);
                    break;
                //从接口获取酒店信息并保存到数据库
                case "GetHotels":
                    context.Response.ContentType = "text/plain";
                    result = AgodaBLL.Instance.GetHotels();
                    context.Response.Write(result);
                    break;
                //从接口获取房型信息并保存到数据库
                case "GetRoomTypeSave":
                    context.Response.ContentType = "text/plain";
                    result = AgodaBLL.Instance.GetRoomTypeSave();
                    context.Response.Write(result);
                    break;
                //获取国籍
                case "GetAgodaNationality":
                    result = AgodaBLL.Instance.GetAgodaNationality();
                    context.Response.Write(result);
                    break;
                //获取目的地
                case "GetAgodaDestination":
                    string strName = context.Request["paramName"] ?? "";
                    if (strName != "")
                        result = AgodaBLL.Instance.GetAgodaDestination(strName);
                    context.Response.Write(result);
                    break;
                //酒店信息
                case "Get_HotelInformation":
                    string _HotelName = context.Request["hotelName"] ?? "";
                    string _ddlGreadeCode = context.Request["ddlGreadeCode"] ?? "0";
                    int input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    int input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    string destination = context.Request["destination"] ?? "";
                    string cityID = string.Empty;
                    string countryCode = string.Empty;
                    if (destination != "")
                    {
                        string[] ArrayDestination = destination.Split(',');
                        cityID = ArrayDestination[0];
                    }
                    string sqlWhere = " 1=1 ";
                    if (cityID != "" || countryCode != "")
                    {
                        if (cityID != "")
                            sqlWhere += string.Format(@" and CityId='{0}'", cityID);
                    }
                    else
                    {
                        sqlWhere += "and 1=0";
                    }
                    if (_HotelName != "")
                    {
                        sqlWhere += string.Format(@" and TranslatedName like '%{0}%'", _HotelName);
                    }
                    if (_ddlGreadeCode != "0")
                    {
                        sqlWhere += string.Format(@" and StarRating={0}", _ddlGreadeCode);
                    }
                    int totalCount = 0;
                    result = AgodaBLL.Instance.GetHotel("AgodaHotel", "*", "HotelName", input_row, input_page, sqlWhere, out totalCount);
                    context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                    break;
                //获取房型报价
                case "GetRoomTypePrice":
                    string hotelID = context.Request["hotelID"] ?? "";
                    string beginTime = context.Request["beginTime"] ?? "";
                    string endTime = context.Request["endTime"] ?? "";
                    int RoomCount = (context.Request["ddlRoom"] ?? "").AsTargetType<int>(0);
                    string RateplanId = context.Request["RateplanId"] ?? "";
                    int Adult = (context.Request["Adult"] ?? "").AsTargetType<int>(0);
                    int Child = (context.Request["Child"] ?? "").AsTargetType<int>(0);
                    string ChildAge1 = context.Request["ChildAge1"] ?? "";
                    string ChildAge2 = context.Request["ChildAge2"] ?? "";
                    string ChildAge3 = context.Request["ChildAge3"] ?? "";
                    string ChildAge4 = context.Request["ChildAge4"] ?? "";
                    string ChildAge5 = context.Request["ChildAge5"] ?? "";
                    string ChildAge6 = context.Request["ChildAge6"] ?? "";
                    string ChildAge7 = context.Request["ChildAge7"] ?? "";
                    string ChildAge8 = context.Request["ChildAge8"] ?? "";
                    string ChildAge9 = context.Request["ChildAge9"] ?? "";
                    string ChildAge10 = context.Request["ChildAge10"] ?? "";
                    string ChildAge11 = context.Request["ChildAge11"] ?? "";
                    string ChildAge12 = context.Request["ChildAge12"] ?? "";
                    IntOccupancyInfo occupancy = new IntOccupancyInfo();
                    occupancy.adults = Adult;
                    occupancy.children = Child;
                    string Age = string.Empty;
                    if (Child == 1)
                    {
                        occupancy.childAges = ChildAge1;
                    }
                    else if (Child == 2)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2;
                    }
                    else if (Child == 3)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3;
                    }
                    else if (Child == 4)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4;
                    }
                    else if (Child == 5)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5;
                    }
                    else if (Child == 6)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5 + "," + ChildAge6;
                    }
                    else if (Child == 7)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5 + "," + ChildAge6 + "," + ChildAge7;
                    }
                    else if (Child == 8)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5 + "," + ChildAge6 + "," + ChildAge7 + "," + ChildAge8;
                    }
                    else if (Child == 9)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5 + "," + ChildAge6 + "," + ChildAge7 + "," + ChildAge7 + "," + ChildAge9;
                    }
                    else if (Child == 10)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5 + "," + ChildAge6 + "," + ChildAge7 + "," + ChildAge8 + "," + ChildAge9 + "," + ChildAge10;
                    }
                    else if (Child == 11)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5 + "," + ChildAge6 + "," + ChildAge7 + "," + ChildAge8 + "," + ChildAge9 + "," + ChildAge10 + "," + ChildAge11;
                    }
                    else if (Child == 12)
                    {
                        occupancy.childAges = ChildAge1 + "," + ChildAge2 + "," + ChildAge3 + "," + ChildAge4 + "," + ChildAge5 + "," + ChildAge6 + "," + ChildAge7 + "," + ChildAge8 + "," + ChildAge9 + "," + ChildAge10 + "," + ChildAge11 + "," + ChildAge12;
                    }

                    DataTable dt = AgodaBLL.Instance.GetRoomTypePrice(hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId);
                    if (dt.Rows.Count == 0)
                    {
                        result = "{\"total\":0,\"rows\":[]}";
                    }
                    input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    DataTable PageDt = Common.Common.GetPagedTable(dt, input_page, input_row);
                    Hashtable hash = new Hashtable();
                    hash.Add("total", dt.Rows.Count);
                    hash.Add("rows", PageDt);
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(hash);
                    context.Response.Write(result);
                    break;
                //退单
                case "CancelBooking":
                    string bookingCode = context.Request["bookingCode"] ?? "";
                    HotelID = context.Request["HotelID"] ?? "";
                    result = AgodaBLL.Instance.cancel(HotelID, bookingCode);
                    context.Response.Write(result);
                    break;
                //获取订单信息
                case "CheckingOrder":
                    bookingCode = context.Request["bookingCode"] ?? "";
                    HotelID = context.Request["HotelID"] ?? "";
                    string BookingID = context.Request["BookingID"] ?? "";
                    string sqlBooking = string.Format(@"select * from Booking WITH(NOLOCK) where ID IN ({0})", BookingID);
                    DataTable dtBooking = ControllerFactory.GetController().GetDataTable(sqlBooking);
                    string[] idIn = dtBooking.AsEnumerable().Select(r => r.Field<string>("BookingCode")).ToArray();
                    bookingCode = string.Format("{0}", string.Join(",", idIn));
                    result = AgodaBLL.Instance.Get_OrderDetail(HotelID, bookingCode);
                    context.Response.Write(result);
                    break;
                //查询账单
                case "GetBookingList":
                    bookingCode = context.Request["bookingCode"] ?? "";
                    HotelID = context.Request["HotelID"] ?? "";
                    BookingID = context.Request["BookingID"] ?? "";
                    string sqlBookingList = string.Format(@"select * from Booking WITH(NOLOCK) where ID IN ({0})", BookingID);
                    DataTable dtBookingList = ControllerFactory.GetController().GetDataTable(sqlBookingList);
                    string[] idList = dtBookingList.AsEnumerable().Select(r => r.Field<string>("AgentBookingReference")).ToArray();
                    string AgentBookingReference = string.Format("{0}", string.Join(",", idList));
                    result = AgodaBLL.Instance.GetBookingList(HotelID, AgentBookingReference);
                    context.Response.Write(result);
                    break;
                //预订
                case "book":
                    string roomArr = context.Request["roomArr"] ?? "";
                    RoomCount = (context.Request["RoomCount"] ?? "").AsTargetType<int>(0);
                    Child = (context.Request["Child"] ?? "").AsTargetType<int>(0);
                    Adult = (context.Request["Adult"] ?? "").AsTargetType<int>(0);
                    string[] arrayRooms = roomArr.Split(';');
                    List<roomCustomers> roomCustomers = new List<roomCustomers>();
                    roomCustomers roomCustomer = new roomCustomers();
                    List<Customer> customers = new List<Customer>();
                    int people = Child + Adult;
                    for (int i = 0; i < people; i++)
                    {
                        string[] bookArrayRoom = arrayRooms[i].Split(',');
                        Customer tppObj = new Customer();
                        tppObj.firstName = bookArrayRoom[1];
                        tppObj.lastName = bookArrayRoom[2];
                        tppObj.age = bookArrayRoom[3].AsTargetType<int>(0);
                        if (bookArrayRoom[0] == "Mrs")
                        {
                            tppObj.sex = 2;
                        }
                        else if (bookArrayRoom[0] == "Mr")
                        {
                            tppObj.sex = 1;
                        }
                        customers.Add(tppObj);
                    }
                    roomCustomer.Customers = customers;
                    roomCustomers.Add(roomCustomer);
                    HotelID = context.Request["HotelID"] ?? "";
                    RateplanId = context.Request["RateplanId"] ?? "";
                    beginTime = context.Request["beginTime"] ?? "";
                    endTime = context.Request["endTime"] ?? "";
                    result = AgodaBLL.Instance.book(HotelID, RateplanId, roomCustomers, "", beginTime, endTime, RoomCount);
                    context.Response.Write(result);
                    break;
                //查看订单（本地数据库）
                case "Booking":
                    totalCount = 0;
                    string _BookingCode = context.Request["BookingCode"] ?? "";
                    input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    sqlWhere = " 1=1 ";
                    _BookingCode = context.Request["BookingNumber"] ?? "";
                    string InDate = context.Request["InDate"] ?? "";
                    string InNum = context.Request["InNum"] ?? "";
                    string Status = context.Request["Status"] ?? "";
                    if (_BookingCode != "")
                    {
                        sqlWhere = sqlWhere + string.Format("AND BookingCode='{0}' ", _BookingCode);
                    }
                    if (InNum != "")
                    {
                        sqlWhere = sqlWhere + string.Format("AND InOrderNum='{0}' ", InNum);
                    }
                    if (InDate != "")
                    {
                        sqlWhere = sqlWhere + string.Format("AND CONVERT(varchar(100), CreatTime, 23)='{0}' ", InDate);
                    }
                    if (Status != "0")
                    {
                        if (Status == "1")
                        {
                            sqlWhere = sqlWhere + "AND Status <> 0 ";
                        }
                        else if (Status == "2")
                        {
                            sqlWhere = sqlWhere + "AND Status=0 ";
                        }
                    }
                    result = AgodaBLL.Instance.GetBookingBySql("Agoda_Order ", " * ", " Id ", input_row, input_page, sqlWhere, out totalCount);
                    context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                    break;
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}