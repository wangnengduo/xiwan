---------
日志内容：Studio： Service 110 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd'><peticion><tipo>110</tipo><nombre></nombre>
<agencia></agencia><parametros><hotel>636739#994214#962243#601022#745388#758213#752553#</hotel><pais>MV</pais><provincia>MVMOL</provincia><poblacion></poblacion><categoria>0</categoria>
<radio>9</radio><fechaentrada>11/20/2018</fechaentrada><fechasalida>11/23/2018</fechasalida><marca></marca><afiliacion>RS</afiliacion>
<usuario>E33137</usuario><numhab1>1</numhab1><paxes1>1-1</paxes1><edades1>10</edades1><numhab2>1</numhab2><paxes2>2-1</paxes2><edades2>9</edades2>
<numhab3>0</numhab3><paxes3>0</paxes3><edades3>0</edades3><idioma>2</idioma><duplicidad>1</duplicidad><comprimido>2</comprimido><informacion_hotel>0</informacion_hotel></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_disponibilidad_110.dtd">
<respuesta>
	<tipo>110</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<param>
		<hotls num="4">
			<hot>
				<cod>745388</cod>
				<afi>RS</afi>
				<nom>TEST CERTIFICACION TEST</nom>
				<pro>MVMOL</pro>
				<prn>MOLDOVA2</prn>
				<pob>MOLDAVIA TEST POBL</pob>
				<cat>0</cat>
				<fen>20181120</fen>
				<fsa>20181123</fsa>
				<pdr>N</pdr>
				<cal>1</cal>
				<mar>HA#ST#D1#</mar>
				<res>
					<pax cod="1-1">
						<hab cod="04" desc="1 adult + 1 child">
							<reg cod="OB" prr="89.49" pvp="104.04" div="DO" esr="OK" nr="0">
								<lin>04#1#C+#29.83#0#OB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>04#1#C+#29.83#0#OB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>04#1#C+#29.83#0#OB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin>
							</reg>
							<reg cod="FI" prr="149.13" pvp="173.40" div="DO" esr="OK" nr="0">
								<lin>04#1#C+#49.71#0#FI#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>04#1#C+#49.71#0#FI#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>04#1#C+#49.71#0#FI#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin>
							</reg>
						</hab>
						<hab cod="DB" desc="full size bed (1 big bed)">
							<reg cod="OB" prr="89.49" pvp="104.04" div="DO" esr="OK" nr="0">
								<lin>DB#1#C+#29.83#0#OB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>DB#1#C+#29.83#0#OB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>DB#1#C+#29.83#0#OB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin>
							</reg>
							<reg cod="FI" prr="119.31" pvp="138.72" div="DO" esr="OK" nr="0">
								<lin>DB#1#C+#39.77#0#FI#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>DB#1#C+#39.77#0#FI#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>DB#1#C+#39.77#0#FI#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin>
							</reg>
						</hab>
						<hab cod="TW" desc="Twin (2 beds)">
							<reg cod="FI" prr="149.13" pvp="173.40" div="DO" esr="OK" nr="0">
								<lin>TW#1#C+#49.71#0#FI#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>TW#1#C+#49.71#0#FI#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>TW#1#C+#49.71#0#FI#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin>
							</reg>
							<reg cod="OB" prr="89.49" pvp="104.04" div="DO" esr="OK" nr="0">
								<lin>TW#1#C+#29.83#0#OB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>TW#1#C+#29.83#0#OB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin>
								<lin>TW#1#C+#29.83#0#OB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin>
							</reg>
						</hab>
					</pax>
					<pax cod="2-1">
						<hab cod="T+" desc="Superior Triple">
							<reg cod="OB" prr="89.49" pvp="104.04" div="DO" esr="OK" nr="0">
								<lin>T+#1#C+#29.83#0#OB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>T+#1#C+#29.83#0#OB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>T+#1#C+#29.83#0#OB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#745388#</lin>
							</reg>
							<reg cod="FI" prr="178.95" pvp="208.08" div="DO" esr="OK" nr="0">
								<lin>T+#1#C+#59.65#0#FI#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>T+#1#C+#59.65#0#FI#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>T+#1#C+#59.65#0#FI#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#745388#</lin>
							</reg>
						</hab>
						<hab cod="TN" desc="Double/Twin + child">
							<reg cod="FI" prr="164.04" pvp="190.74" div="DO" esr="OK" nr="0">
								<lin>TN#1#C+#54.68#0#FI#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>TN#1#C+#54.68#0#FI#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>TN#1#C+#54.68#0#FI#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#745388#</lin>
							</reg>
							<reg cod="OB" prr="89.49" pvp="104.04" div="DO" esr="OK" nr="0">
								<lin>TN#1#C+#29.83#0#OB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>TN#1#C+#29.83#0#OB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#745388#</lin>
								<lin>TN#1#C+#29.83#0#OB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#745388#</lin>
							</reg>
						</hab>
					</pax>
				</res>
				<pns>S</pns>
				<end>2</end>
				<enh>12</enh>
				<cat2>BA</cat2>
				<city_tax>From the date of: 31/12/2017 Test taxes for hotel 745388.</city_tax>
				<tipo_establecimiento>Hotel y apatamentos</tipo_establecimiento>
			</hot>
			<hot>
				<cod>962243</cod>
				<afi>RS</afi>
				<nom>THE MATURANA TEST RESORT</nom>
				<pro>MVMOL</pro>
				<prn>MOLDOVA2</prn>
				<pob>MOLDAVIA TEST POBL</pob>
				<cat>5</cat>
				<fen>20181120</fen>
				<fsa>20181123</fsa>
				<pdr>N</pdr>
				<cal>0</cal>
				<mar>HA#DS#</mar>
				<res>
					<pax cod="1-1">
						<hab cod="DB" desc="full size bed (1 big bed)">
							<reg cod="OB" prr="140.46" pvp="173.40" div="DO" esr="OK" nr="0">
								<lin>DB#1#VR#46.82#0#OB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>DB#1#VR#46.82#0#OB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>DB#1#VR#46.82#0#OB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#962243#</lin>
							</reg>
							<reg cod="BB" prr="168.57" pvp="208.08" div="DO" esr="OK" nr="0">
								<lin>DB#1#VR#56.19#0#BB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>DB#1#VR#56.19#0#BB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>DB#1#VR#56.19#0#BB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#962243#</lin>
							</reg>
						</hab>
						<hab cod="TW" desc="Twin (2 beds)">
							<reg cod="OB" prr="140.46" pvp="173.40" div="DO" esr="OK" nr="0">
								<lin>TW#1#VR#46.82#0#OB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>TW#1#VR#46.82#0#OB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>TW#1#VR#46.82#0#OB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#962243#</lin>
							</reg>
							<reg cod="BB" prr="168.57" pvp="208.08" div="DO" esr="OK" nr="0">
								<lin>TW#1#VR#56.19#0#BB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>TW#1#VR#56.19#0#BB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#962243#</lin>
								<lin>TW#1#VR#56.19#0#BB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#962243#</lin>
							</reg>
						</hab>
					</pax>
					<pax cod="2-1">
						<hab cod="TP" desc="Triple">
							<reg cod="OB" prr="140.46" pvp="173.40" div="DO" esr="OK" nr="0">
								<lin>TP#1#VR#46.82#0#OB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#962243#</lin>
								<lin>TP#1#VR#46.82#0#OB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#962243#</lin>
								<lin>TP#1#VR#46.82#0#OB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#962243#</lin>
							</reg>
							<reg cod="BB" prr="168.57" pvp="208.08" div="DO" esr="OK" nr="0">
								<lin>TP#1#VR#56.19#0#BB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#962243#</lin>
								<lin>TP#1#VR#56.19#0#BB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#962243#</lin>
								<lin>TP#1#VR#56.19#0#BB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#962243#</lin>
							</reg>
						</hab>
					</pax>
				</res>
				<pns>S</pns>
				<end>18</end>
				<enh>18</enh>
				<cat2>BA</cat2>
				<city_tax>From the date of: 23/03/2017 Prueba CityTax C�digo �nico.</city_tax>
				<tipo_establecimiento>Hotel</tipo_establecimiento>
			</hot>
			<hot>
				<cod>601022</cod>
				<afi>RS</afi>
				<nom>HOTEL XML TEST NON REFUND</nom>
				<pro>MVMOL</pro>
				<prn>MOLDOVA2</prn>
				<pob>MOLDAVIA TEST POBL</pob>
				<cat>4</cat>
				<fen>20181120</fen>
				<fsa>20181123</fsa>
				<pdr>N</pdr>
				<cal>0</cal>
				<mar>HA#EC#</mar>
				<res>
					<pax cod="1-1">
						<hab cod="TW" desc="Twin (2 beds)">
							<reg cod="BB" prr="210.69" div="DO" esr="OK" nr="1">
								<lin>TW#1#C #70.23#0#BB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#601022#</lin>
								<lin>TW#1#C #70.23#0#BB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#601022#</lin>
								<lin>TW#1#C #70.23#0#BB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#601022#</lin>
							</reg>
						</hab>
						<hab cod="D8" desc="DOUBLE STANDARD">
							<reg cod="BB" prr="210.69" div="DO" esr="OK" nr="1">
								<lin>D8#1#C #70.23#0#BB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#601022#</lin>
								<lin>D8#1#C #70.23#0#BB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#601022#</lin>
								<lin>D8#1#C #70.23#0#BB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#601022#</lin>
							</reg>
						</hab>
					</pax>
					<pax cod="2-1">
						<hab cod="TP" desc="Triple">
							<reg cod="BB" prr="280.92" div="DO" esr="OK" nr="1">
								<lin>TP#1#C #93.64#0#BB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#601022#</lin>
								<lin>TP#1#C #93.64#0#BB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#601022#</lin>
								<lin>TP#1#C #93.64#0#BB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#601022#</lin>
							</reg>
						</hab>
					</pax>
				</res>
				<pns>S</pns>
				<end>0</end>
				<enh>1.99</enh>
				<cat2>BA</cat2>
				<city_tax>From the date of: 01/07/2018 to 31/12/2018 english tax.</city_tax>
				<tipo_establecimiento>Hotel</tipo_establecimiento>
			</hot>
			<hot>
				<cod>636739</cod>
				<afi>RS</afi>
				<nom>HOTEL RESTEL TEST PRUEBA</nom>
				<pro>MVMOL</pro>
				<prn>MOLDOVA2</prn>
				<pob>MOLDAVIA TEST POBL</pob>
				<cat>5</cat>
				<fen>20181120</fen>
				<fsa>20181123</fsa>
				<pdr>N</pdr>
				<cal>0</cal>
				<mar>RS#</mar>
				<res>
					<pax cod="1-1">
						<hab cod="DB" desc="full size bed (1 big bed)">
							<reg cod="RO" prr="70.23" div="DO" esr="OK" nr="0">
								<lin>DB#1#VR#23.41#0#RO#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>DB#1#VR#23.41#0#RO#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>DB#1#VR#23.41#0#RO#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#636739#</lin>
							</reg>
							<reg cod="BB" prr="56.19" div="DO" esr="OK" nr="0">
								<lin>DB#1#VR#18.73#0#BB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>DB#1#VR#18.73#0#BB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>DB#1#VR#18.73#0#BB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#636739#</lin>
							</reg>
						</hab>
						<hab cod="TW" desc="Twin (2 beds)">
							<reg cod="BB" prr="56.19" div="DO" esr="OK" nr="0">
								<lin>TW#1#VR#18.73#0#BB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>TW#1#VR#18.73#0#BB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>TW#1#VR#18.73#0#BB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#636739#</lin>
							</reg>
							<reg cod="RO" prr="70.23" div="DO" esr="OK" nr="0">
								<lin>TW#1#VR#23.41#0#RO#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>TW#1#VR#23.41#0#RO#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#636739#</lin>
								<lin>TW#1#VR#23.41#0#RO#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#636739#</lin>
							</reg>
						</hab>
					</pax>
					<pax cod="2-1">
						<hab cod="TN" desc="Double/Twin + child">
							<reg cod="BB" prr="56.19" div="DO" esr="OK" nr="0">
								<lin>TN#1#VR#18.73#0#BB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TN#1#VR#18.73#0#BB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TN#1#VR#18.73#0#BB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#636739#</lin>
							</reg>
							<reg cod="RO" prr="70.23" div="DO" esr="OK" nr="0">
								<lin>TN#1#VR#23.41#0#RO#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TN#1#VR#23.41#0#RO#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TN#1#VR#23.41#0#RO#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#636739#</lin>
							</reg>
						</hab>
						<hab cod="TP" desc="Triple">
							<reg cod="RO" prr="70.23" div="DO" esr="OK" nr="0">
								<lin>TP#1#VR#23.41#0#RO#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TP#1#VR#23.41#0#RO#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TP#1#VR#23.41#0#RO#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#636739#</lin>
							</reg>
							<reg cod="BB" prr="56.19" div="DO" esr="OK" nr="0">
								<lin>TP#1#VR#18.73#0#BB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TP#1#VR#18.73#0#BB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#636739#</lin>
								<lin>TP#1#VR#18.73#0#BB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#636739#</lin>
							</reg>
						</hab>
					</pax>
				</res>
				<pns>N</pns>
				<end>0</end>
				<enh>12</enh>
				<cat2>BA</cat2>
				<city_tax>From the date of: 01/07/2018 to 31/12/2018 english tax.</city_tax>
				<tipo_establecimiento>Hotel</tipo_establecimiento>
			</hot>
		</hotls>
		<id>61627aba-9580-43c9-a3f7-3c90b2e2b2e7</id>
	</param>
</respuesta> , Time:2018/9/5 16:56:22 
发生时间：2018/9/5 16:56:22
---------
日志内容：Studio： Service 24 ,请求数据(Req) :<?xml version='1.0' encoding='iso-8859-1'?> <peticion> <tipo>24</tipo> 
<parametros> <codigo_cobol>745388</codigo_cobol> <entrada>20-11-2018</entrada> <salida>23-11-2018</salida> </parametros> </peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_hotobs.dtd">
<respuesta>
	<tipo>24</tipo>
	<nombre>null</nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<hotel>
			<codigo>291005</codigo>
			<codigo_cobol>745388</codigo_cobol>
			<observaciones num="2">
				<observacion>
					<obs_texto>NOTES AND COMMENTS OF THE TEST HOTEL</obs_texto>
					<obs_desde>20180101</obs_desde>
					<obs_hasta>20181231</obs_hasta>
				</observacion>
				<observacion>
					<obs_texto>ESTAS SON LAS NOTAS Y OBSERVACIONES DE HOTEL DE TEST</obs_texto>
					<obs_desde>20180101</obs_desde>
					<obs_hasta>20181231</obs_hasta>
				</observacion>
			</observaciones>
		</hotel>
		<id>daff91fe-a8c7-4f73-9f69-2b090ce3d474</id>
	</parametros>
</respuesta> , Time:2018/9/5 16:56:25 
发生时间：2018/9/5 16:56:25
---------
日志内容：Studio： Service 144 ,请求数据(Req) :<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_algGastosCanc.dtd'>
<peticion><tipo>144</tipo><nombre></nombre><agencia />
<parametros><datos_reserva><hotel>745388</hotel><lin>TW#1#C+#29.83#0#OB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin><lin>TW#1#C+#29.83#0#OB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin><lin>TW#1#C+#29.83#0#OB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin><lin>T+#1#C+#29.83#0#OB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#745388#</lin><lin>T+#1#C+#29.83#0#OB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#745388#</lin><lin>T+#1#C+#29.83#0#OB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#745388#</lin></datos_reserva></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_algGastosCancAll.dtd">
<respuesta>
	<tipo>144</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<politicaCanc fecha="20/11/2018">
			<dias_antelacion>7</dias_antelacion>
			<horas_antelacion>0</horas_antelacion>
			<noches_gasto>0</noches_gasto>
			<estCom_gasto>100</estCom_gasto>
			<concepto>En caso de cancelaci�n con menos de 7 dias de la entrada del cliente  el hotel facturar� 100 % de la estancia. </concepto>
			<entra_en_gastos>0</entra_en_gastos>
		</politicaCanc>
		<politicaCanc fecha="20/11/2018">
			<dias_antelacion>7</dias_antelacion>
			<horas_antelacion>0</horas_antelacion>
			<noches_gasto>0</noches_gasto>
			<estCom_gasto>100</estCom_gasto>
			<concepto>En caso de cancelaci�n con menos de 7 dias de la entrada del cliente  el hotel facturar� 100 % de la estancia. </concepto>
			<entra_en_gastos>0</entra_en_gastos>
		</politicaCanc>
		<id>6277bf74-f4b6-40ee-91e5-3299400c2221</id>
	</parametros>
</respuesta> , Time:2018/9/5 16:56:45 
发生时间：2018/9/5 16:56:45
---------
日志内容：Studio： Service 202 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_reserva_3.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>202</tipo><parametros><codigo_hotel>745388</codigo_hotel><nombre_cliente>San Zhang</nombre_cliente>
<observaciones></observaciones><num_mensaje /><num_expediente>5057200033018376820</num_expediente><forma_pago>25</forma_pago><tipo_targeta></tipo_targeta>
<num_targeta></num_targeta><cvv_targeta></cvv_targeta><mes_expiracion_targeta></mes_expiracion_targeta><ano_expiracion_targeta></ano_expiracion_targeta>
<titular_targeta></titular_targeta><ocupantes>San#Zhang#.#21#@Si#Li#.#10#@Wu#Wang#.#21#@Qi#Huang#.#21#@Ba#Liang#.#9#@</ocupantes><res><lin>TW#1#C+#29.83#0#OB#OK#20181120#20181121#DO#1-1:10#0#0#201809051056#745388#</lin><lin>TW#1#C+#29.83#0#OB#OK#20181121#20181122#DO#1-1:10#0#0#201809051056#745388#</lin><lin>TW#1#C+#29.83#0#OB#OK#20181122#20181123#DO#1-1:10#0#0#201809051056#745388#</lin><lin>T+#1#C+#29.83#0#OB#OK#20181120#20181121#DO#2-1:9#0#0#201809051056#745388#</lin><lin>T+#1#C+#29.83#0#OB#OK#20181121#20181122#DO#2-1:9#0#0#201809051056#745388#</lin><lin>T+#1#C+#29.83#0#OB#OK#20181122#20181123#DO#2-1:9#0#0#201809051056#745388#</lin></res></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_reserva.dtd">
<respuesta>
	<tipo>202</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<estado>00</estado>
		<n_localizador>30946345</n_localizador>
		<importe_total_reserva>178.98</importe_total_reserva>
		<divisa_total>DO</divisa_total>
		<n_mensaje>00000000</n_mensaje>
		<n_expediente>5057200033018376820</n_expediente>
		<observaciones></observaciones>
		<datos></datos>
		<id>43800e4a-88d4-4c42-9398-2dc908c7c912</id>
	</parametros>
</respuesta> , Time:2018/9/5 16:56:49 
发生时间：2018/9/5 16:56:49
---------
日志内容：Studio： Service 3 (AE) ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_reserva.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>3</tipo><parametros><localizador>30946345</localizador><accion>AE</accion></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_anul_confirm.dtd">
<respuesta>
	<tipo>3</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<estado>00</estado>
		<localizador>30946345</localizador>
		<localizador_corto>26728022</localizador_corto>
		<id>300c7164-ed09-40c2-9a8e-ad5b4c50ae0a</id>
	</parametros>
</respuesta> , Time:2018/9/5 16:57:06 
发生时间：2018/9/5 16:57:06
---------
日志内容：Studio： Service 2 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?><peticion><tipo>142</tipo><nombre></nombre><agencia></agencia>
<parametros><usuario>123593</usuario> <localizador>30946345</localizador> <idioma>2</idioma></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_gastosCancelReserva.dtd">
<respuesta>
	<tipo>142</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<politicaCanc localizador="30946345">
			<dias_antelacion>7</dias_antelacion>
			<horas_antelacion>0</horas_antelacion>
			<noches_gasto>0</noches_gasto>
			<estCom_gasto>100</estCom_gasto>
			<desc>Any cancellation with less than 7 day(s) before of the arrival of the client   the hotel will charge 100 % of the stay. </desc>
		</politicaCanc>
		<politicaCanc localizador="30946345">
			<dias_antelacion>7</dias_antelacion>
			<horas_antelacion>0</horas_antelacion>
			<noches_gasto>0</noches_gasto>
			<estCom_gasto>100</estCom_gasto>
			<desc>Any cancellation with less than 7 day(s) before of the arrival of the client   the hotel will charge 100 % of the stay. </desc>
		</politicaCanc>
		<id>9d774d59-5ae1-4ab4-940e-a25f0141c1d4</id>
	</parametros>
</respuesta> , Time:2018/9/5 16:57:10 
发生时间：2018/9/5 16:57:10
---------
日志内容：Studio： Service 401 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_cancelacion.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>401</tipo>
<parametros><localizador_largo>30946345</localizador_largo><localizador_corto>26728022</localizador_corto></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_cancelacion.dtd">
<respuesta>
	<tipo>401</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<estado>00</estado>
		<localizador>30946345</localizador>
		<localizador_baja>Su reserva ha sido anulada [Localizador baja:BA577135]</localizador_baja>
		<id>de944aa5-33ea-4fdd-872f-9f6b3880b79e</id>
	</parametros>
</respuesta> , Time:2018/9/5 16:57:20 
发生时间：2018/9/5 16:57:20
