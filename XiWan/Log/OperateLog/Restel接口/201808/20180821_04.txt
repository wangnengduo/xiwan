﻿---------
日志内容：Studio： Service 110 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_disponibilidad_110.dtd'><peticion><tipo>110</tipo><nombre></nombre>
<agencia></agencia><parametros><hotel>745388#</hotel><pais>MV</pais><provincia>MVMOL</provincia><poblacion></poblacion><categoria>0</categoria>
<radio>9</radio><fechaentrada>10/28/2018</fechaentrada><fechasalida>10/30/2018</fechasalida><marca></marca><afiliacion>RS</afiliacion>
<usuario>E33137</usuario><numhab1>2</numhab1><paxes1>2-0</paxes1><edades1>0</edades1><numhab2>0</numhab2><paxes2>0</paxes2><edades2>0</edades2>
<numhab3>0</numhab3><paxes3>0</paxes3><edades3>0</edades3><idioma>2</idioma><duplicidad>1</duplicidad><comprimido>2</comprimido><informacion_hotel>0</informacion_hotel></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_disponibilidad_110.dtd">
<respuesta>
	<tipo>110</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<param>
		<hotls num="1">
			<hot>
				<cod>745388</cod>
				<afi>RS</afi>
				<nom>TEST CERTIFICACION TEST</nom>
				<pro>MVMOL</pro>
				<prn>MOLDOVA2</prn>
				<pob>MOLDAVIA TEST POBL</pob>
				<cat>0</cat>
				<fen>20181028</fen>
				<fsa>20181030</fsa>
				<pdr>N</pdr>
				<cal>1</cal>
				<mar>HA#ST#D1#</mar>
				<res>
					<pax cod="2-0">
						<hab cod="DB" desc="full size bed (1 big bed)">
							<reg cod="FI" prr="157.12" pvp="182.70" div="DO" esr="OK" nr="0">
								<lin>DB#2#C+#78.56#0#FI#OK#20181028#20181029#DO#2-0:0#0#0#201808211019#745388#</lin>
								<lin>DB#2#C+#78.56#0#FI#OK#20181029#20181030#DO#2-0:0#0#0#201808211019#745388#</lin>
							</reg>
							<reg cod="OB" prr="117.84" pvp="137.02" div="DO" esr="OK" nr="0">
								<lin>DB#2#C+#58.92#0#OB#OK#20181028#20181029#DO#2-0:0#0#0#201808211019#745388#</lin>
								<lin>DB#2#C+#58.92#0#OB#OK#20181029#20181030#DO#2-0:0#0#0#201808211019#745388#</lin>
							</reg>
						</hab>
						<hab cod="TW" desc="Twin (2 beds)">
							<reg cod="FI" prr="196.40" pvp="228.38" div="DO" esr="OK" nr="0">
								<lin>TW#2#C+#98.20#0#FI#OK#20181028#20181029#DO#2-0:0#0#0#201808211019#745388#</lin>
								<lin>TW#2#C+#98.20#0#FI#OK#20181029#20181030#DO#2-0:0#0#0#201808211019#745388#</lin>
							</reg>
							<reg cod="OB" prr="117.84" pvp="137.02" div="DO" esr="OK" nr="0">
								<lin>TW#2#C+#58.92#0#OB#OK#20181028#20181029#DO#2-0:0#0#0#201808211019#745388#</lin>
								<lin>TW#2#C+#58.92#0#OB#OK#20181029#20181030#DO#2-0:0#0#0#201808211019#745388#</lin>
							</reg>
						</hab>
					</pax>
				</res>
				<pns>S</pns>
				<end>2</end>
				<enh>12</enh>
				<cat2>BA</cat2>
				<city_tax>From the date of: 31/12/2017 Test taxes for hotel 745388.</city_tax>
				<tipo_establecimiento>Hotel y apatamentos</tipo_establecimiento>
			</hot>
		</hotls>
		<id>bf9af37d-a9b4-40c2-a911-30c18fe009e4</id>
	</param>
</respuesta> , Time:2018/8/21 16:19:21 
发生时间：2018/8/21 16:19:21
---------
日志内容：Studio： Service 24 ,请求数据(Req) :<?xml version='1.0' encoding='iso-8859-1'?> <peticion> <tipo>24</tipo> 
<parametros> <codigo_cobol>745388</codigo_cobol> <entrada>28-10-2018</entrada> <salida>30-10-2018</salida> </parametros> </peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_hotobs.dtd">
<respuesta>
	<tipo>24</tipo>
	<nombre>null</nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<hotel>
			<codigo>291005</codigo>
			<codigo_cobol>745388</codigo_cobol>
			<observaciones num="2">
				<observacion>
					<obs_texto>NOTES AND COMMENTS OF THE TEST HOTEL</obs_texto>
					<obs_desde>20180101</obs_desde>
					<obs_hasta>20181231</obs_hasta>
				</observacion>
				<observacion>
					<obs_texto>ESTAS SON LAS NOTAS Y OBSERVACIONES DE HOTEL DE TEST</obs_texto>
					<obs_desde>20180101</obs_desde>
					<obs_hasta>20181231</obs_hasta>
				</observacion>
			</observaciones>
		</hotel>
		<id>6a89a566-ee05-46a2-b4c4-8bb299f5940b</id>
	</parametros>
</respuesta> , Time:2018/8/21 16:19:54 
发生时间：2018/8/21 16:19:54
---------
日志内容：Studio： Service 144 ,请求数据(Req) :<!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_algGastosCanc.dtd'>
<peticion><tipo>144</tipo><nombre></nombre><agencia />
<parametros><datos_reserva><hotel>745388</hotel><lin>DB#2#C+#58.92#0#OB#OK#20181028#20181029#DO#2-0:0#0#0#201808211019#745388#</lin><lin>DB#2#C+#58.92#0#OB#OK#20181029#20181030#DO#2-0:0#0#0#201808211019#745388#</lin></datos_reserva></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_algGastosCancAll.dtd">
<respuesta>
	<tipo>144</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<politicaCanc fecha="28/10/2018">
			<dias_antelacion>7</dias_antelacion>
			<horas_antelacion>0</horas_antelacion>
			<noches_gasto>0</noches_gasto>
			<estCom_gasto>100</estCom_gasto>
			<concepto>En caso de cancelaci�n con menos de 7 dias de la entrada del cliente  el hotel facturar� 100 % de la estancia. </concepto>
			<entra_en_gastos>0</entra_en_gastos>
		</politicaCanc>
		<id>a331753e-328d-415a-a35f-a72bd75bd460</id>
	</parametros>
</respuesta> , Time:2018/8/21 16:19:59 
发生时间：2018/8/21 16:19:59
---------
日志内容：Studio： Service 202 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1'?><!DOCTYPE peticion SYSTEM 'http://xml.hotelresb2b.com/xml/dtd/pet_reserva_3.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>202</tipo><parametros><codigo_hotel>745388</codigo_hotel><nombre_cliente>San Zhang</nombre_cliente>
<observaciones></observaciones><num_mensaje /><num_expediente>5505095773387426778</num_expediente><forma_pago>25</forma_pago><tipo_targeta></tipo_targeta>
<num_targeta></num_targeta><cvv_targeta></cvv_targeta><mes_expiracion_targeta></mes_expiracion_targeta><ano_expiracion_targeta></ano_expiracion_targeta>
<titular_targeta></titular_targeta><ocupantes>San#Zhang#.#21#@Si#Li#.#21#@Wu#Wang#.#21#@Liu#Huang#.#21#@</ocupantes><res><lin>DB#2#C+#58.92#0#OB#OK#20181028#20181029#DO#2-0:0#0#0#201808211019#745388#</lin><lin>DB#2#C+#58.92#0#OB#OK#20181029#20181030#DO#2-0:0#0#0#201808211019#745388#</lin></res></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_reserva.dtd">
<respuesta>
	<tipo>202</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<estado>00</estado>
		<n_localizador>30837070</n_localizador>
		<importe_total_reserva>117.84</importe_total_reserva>
		<divisa_total>DO</divisa_total>
		<n_mensaje>00000000</n_mensaje>
		<n_expediente>5505095773387426778</n_expediente>
		<observaciones>The client needs a baby cot.</observaciones>
		<datos></datos>
		<id>00635be2-a296-4b32-9470-1e7bea9d7296</id>
	</parametros>
</respuesta> , Time:2018/8/21 16:20:03 
发生时间：2018/8/21 16:20:03
---------
日志内容：Studio： Service 3 (AE) ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_reserva.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>3</tipo><parametros><localizador>30837070</localizador><accion>AE</accion></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_anul_confirm.dtd">
<respuesta>
	<tipo>3</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<estado>00</estado>
		<localizador>30837070</localizador>
		<localizador_corto>26614272</localizador_corto>
		<id>802807f4-9f17-403a-af36-552287207559</id>
	</parametros>
</respuesta> , Time:2018/8/21 16:20:32 
发生时间：2018/8/21 16:20:32
---------
日志内容：Studio： Service 142 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?><peticion><tipo>142</tipo><nombre></nombre><agencia></agencia>
<parametros><usuario>123593</usuario> <localizador>30837070</localizador> <idioma>2</idioma></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_gastosCancelReserva.dtd">
<respuesta>
	<tipo>142</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<politicaCanc localizador="30837070">
			<dias_antelacion>7</dias_antelacion>
			<horas_antelacion>0</horas_antelacion>
			<noches_gasto>0</noches_gasto>
			<estCom_gasto>100</estCom_gasto>
			<desc>Any cancellation with less than 7 day(s) before of the arrival of the client   the hotel will charge 100 % of the stay. </desc>
		</politicaCanc>
		<id>f7821e5f-15a7-4868-a6d4-30fb63bfa402</id>
	</parametros>
</respuesta> , Time:2018/8/21 16:20:47 
发生时间：2018/8/21 16:20:47
---------
日志内容：Studio： Service 401 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_cancelacion.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>401</tipo>
<parametros><localizador_largo>30837070</localizador_largo><localizador_corto>26614272</localizador_corto></parametros></peticion> , 返回数据(Resp) ：操作超时 , Time:2018/8/21 16:21:10 
发生时间：2018/8/21 16:21:10
---------
日志内容：Studio： Service 401 ,请求数据(Req) :<?xml version='1.0' encoding='ISO-8859-1' ?><!DOCTYPE peticion SYSTEM ' http://xml.hotelresb2b.com/xml/dtd/pet_cancelacion.dtd'>
<peticion><nombre></nombre><agencia></agencia><tipo>401</tipo>
<parametros><localizador_largo>30837070</localizador_largo><localizador_corto>26614272</localizador_corto></parametros></peticion> , 返回数据(Resp) ：<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_cancelacion.dtd">
<respuesta>
	<tipo>401</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<parametros>
		<estado>09</estado>
		<localizador>30837070</localizador>
		<localizador_baja>Error, la reserva no se puede cancelar, porque ya se encuentra cancelada.</localizador_baja>
		<id>780bc6cf-8e2a-430a-8ab9-07122a088b5f</id>
	</parametros>
</respuesta> , Time:2018/8/21 16:22:16 
发生时间：2018/8/21 16:22:16
