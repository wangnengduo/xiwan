---------
日志内容：Err：Restel获取下单时的标识Post数据 ,err:'s' 附近有语法错误。
字符串 '' 后的引号不完整。[update RestelLink_RoomsCache set Response='<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE respuesta SYSTEM "http://xml.hotelresb2b.com/xml/dtd/res_disponibilidad_110.dtd">
<respuesta>
	<tipo>110</tipo>
	<nombre></nombre>
	<agencia>GUANGZHOU XIWAN INT.TRAVEL AGENCY XML @</agencia>
	<param>
		<hotls num="1">
			<hot>
				<cod>610442</cod>
				<afi>RS</afi>
				<nom>PENNSYLVANIA</nom>
				<pro>USNEW</pro>
				<prn>NEW YORK</prn>
				<pob>NEW YORK, NY</pob>
				<cat>2</cat>
				<fen>20181120</fen>
				<fsa>20181123</fsa>
				<pdr>N</pdr>
				<cal>1</cal>
				<mar>HA#WA#</mar>
				<res>
					<pax cod="2-0">
						<hab cod="02" desc="Penn 5000 Room, 1 Double Bed Room Only - Main">
							<reg cod="OB" prr="376.67" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#81.40#0#OB#OK#20181120#20181121#DO#2-0:0#C1DS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#137.17#0#OB#OK#20181121#20181122#DO#2-0:0#C1DS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#158.10#0#OB#OK#20181122#20181123#DO#2-0:0#C1DS|OTMHUA#0#201811200232#610442#</lin>
							</reg>
						</hab>
						<hab cod="02" desc="Classic Room, 1 King Bed Room Only - Main">
							<reg cod="OB" prr="359.60" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#72.69#0#OB#OK#20181120#20181121#DO#2-0:0#A1KS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#132.88#0#OB#OK#20181121#20181122#DO#2-0:0#A1KS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#154.03#0#OB#OK#20181122#20181123#DO#2-0:0#A1KS|OTMHUA#0#201811200232#610442#</lin>
							</reg>
						</hab>
						<hab cod="02" desc="Classic Accessible Room, 1 King Bed Room Only - Main">
							<reg cod="OB" prr="376.67" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#81.40#0#OB#OK#20181120#20181121#DO#2-0:0#ADAK|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#137.17#0#OB#OK#20181121#20181122#DO#2-0:0#ADAK|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#158.10#0#OB#OK#20181122#20181123#DO#2-0:0#ADAK|OTMHUA#0#201811200232#610442#</lin>
							</reg>
						</hab>
						<hab cod="02" desc="Classic Room, 2 Twin Beds - Room Only">
							<reg cod="OB" prr="504.50" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#160.42#0#OB#OK#20181120#20181121#DO#2-0:0#A2TS|WHLHOT#0#201811200232#610442#</lin>
								<lin>02#1#SH#160.42#0#OB#OK#20181121#20181122#DO#2-0:0#A2TS|WHLHOT#0#201811200232#610442#</lin>
								<lin>02#1#SH#183.66#0#OB#OK#20181122#20181123#DO#2-0:0#A2TS|WHLHOT#0#201811200232#610442#</lin>
							</reg>
						</hab>
						<hab cod="02" desc="Classic Room, 4 Twin Beds Room Only - Main">
							<reg cod="OB" prr="507.98" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#122.07#0#OB#OK#20181120#20181121#DO#2-0:0#A4TL|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#182.50#0#OB#OK#20181121#20181122#DO#2-0:0#A4TL|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#203.41#0#OB#OK#20181122#20181123#DO#2-0:0#A4TL|OTMHUA#0#201811200232#610442#</lin>
							</reg>
						</hab>
						<hab cod="02" desc="Penn 5000 Room, 1 King Bed Room Only - Main">
							<reg cod="OB" prr="371.79" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#76.75#0#OB#OK#20181120#20181121#DO#2-0:0#C1KS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#136.94#0#OB#OK#20181121#20181122#DO#2-0:0#C1KS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#158.10#0#OB#OK#20181122#20181123#DO#2-0:0#C1KS|OTMHUA#0#201811200232#610442#</lin>
							</reg>
						</hab>
						<hab cod="02" desc="Classic Room, 2 Twin Beds Room Only - Main">
							<reg cod="OB" prr="369.70" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#74.43#0#OB#OK#20181120#20181121#DO#2-0:0#A2TS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#137.17#0#OB#OK#20181121#20181122#DO#2-0:0#A2TS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#158.10#0#OB#OK#20181122#20181123#DO#2-0:0#A2TS|OTMHUA#0#201811200232#610442#</lin>
							</reg>
						</hab>
						<hab cod="02" desc="Classic Room, 1 Double Bed Room Only - Main">
							<reg cod="OB" prr="337.16" div="DO" esr="OK" nr="0">
								<lin>02#1#SH#65.13#0#OB#OK#20181120#20181121#DO#2-0:0#A1DS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#125.56#0#OB#OK#20181121#20181122#DO#2-0:0#A1DS|OTMHUA#0#201811200232#610442#</lin>
								<lin>02#1#SH#146.47#0#OB#OK#20181122#20181123#DO#2-0:0#A1DS|OTMHUA#0#201811200232#610442#</lin>
							</reg>
						</hab>
					</pax>
				</res>
				<pns>N</pns>
				<end>0</end>
				<enh>12</enh>
				<int>SAB</int>
				<cat2>CO</cat2>
				<city_tax>From the date of: 31/12/2012 Included in the price: 3,5 USD City tax  //
NOT INCLUDED IN THE PRICE: Facility Fee $35+TAX per room per night.
Facility Fee Details:
 *    Local and long distance calling
 *    TapOut Fitness Center with indoor pool (Must be 18 years or older)
 *    Wi-Fi (high speed internet)
 *    (2) Welcome Drink coupons per stay (no cash value) and 15% discount on lunch, 20% discount on dinner at Stout & Feile.
 *    $5.00 off of Candytopia�s  admission ticket fee (Limited to 1 person and subject to availability) and free access to the retail store.
 *    10% off on Pennsy's participating vendors. (Limited to 1 person).</city_tax>
				<tipo_establecimiento>Hotel</tipo_establecimiento>
			</hot>
		</hotls>
		<id>ee3441b8-8862-46b6-b10f-2f62bfe0a690</id>
	</param>
</respuesta>',UpdateTime=GETDATE() where StrKey='610442%2018-11-20%2018-11-23%2%0%%1'] , Time:2018/11/20 9:32:53 
发生时间：2018/11/20 9:32:53
