---------
日志内容：TravPax：HotelSearchReq ,Req :<HotelSearchReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><CountryID>SG</CountryID><CityID>7355</CityID><HotelID>240237</HotelID><NationalityID>86</NationalityID><ReferenceClient /><Rooms><RoomRequest><Adults>2</Adults><Childs>0</Childs><ChildsAge /></RoomRequest></Rooms></HotelSearchReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<HotelSearchResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient>
  <ReferenceService>B8A4419E-C335-4425-9847-6C27CE235EDF</ReferenceService>
  <Version>5.0.456.0</Version>
  <Hotels>
    <Hotel>
      <HotelID>240237</HotelID>
      <HotelName>Fragrance Bugis</HotelName>
      <CountryID>SG</CountryID>
      <CityID>7355</CityID>
      <CityName />
      <StarRating>3</StarRating>
      <Address>33, Middle Road</Address>
      <Address2>188942 Singapore</Address2>
      <Fax />
      <Email />
      <RoomClasses>
        <RoomClass>
          <ClassName>Deluxe</ClassName>
          <Rooms>
            <Room>
              <RoomID>319</RoomID>
              <RoomName>Deluxe Double</RoomName>
              <Status>AL</Status>
              <BBCode>BB</BBCode>
              <TotalPrice>730</TotalPrice>
              <CurrencyCode>SGD</CurrencyCode>
              <AgreementID>73</AgreementID>
              <Adults>2</Adults>
              <Childs>0</Childs>
              <ChildsAge />
              <SupplierRef>
                <SupplierRef>
                  <ID>1</ID>
                  <Value>a55b9ac4-2ff0-4c9a-95ae-151d5c950aa5</Value>
                </SupplierRef>
              </SupplierRef>
              <SupplierCode>TPXv43</SupplierCode>
              <Supplier>Internal</Supplier>
              <IsPromotion>false</IsPromotion>
              <PromotionDescription />
              <IsOptional>false</IsOptional>
              <Penalty_Date>2018-10-29T00:00:00</Penalty_Date>
              <Penalty_Type>2</Penalty_Type>
              <Penalty_Val>730.00</Penalty_Val>
              <RoomPriceBreak>
                <DailyRate>
                  <DateOfRate>2018-11-07T00:00:00</DateOfRate>
                  <CurrencyCode>SGD</CurrencyCode>
                  <DailyRate>365</DailyRate>
                </DailyRate>
                <DailyRate>
                  <DateOfRate>2018-11-08T00:00:00</DateOfRate>
                  <CurrencyCode>SGD</CurrencyCode>
                  <DailyRate>365</DailyRate>
                </DailyRate>
              </RoomPriceBreak>
            </Room>
          </Rooms>
          <Status>AL</Status>
          <TotalPrice>730</TotalPrice>
          <CurrencyCode>SGD</CurrencyCode>
          <TotalRoom>1</TotalRoom>
          <SupplierCode>TPXv43</SupplierCode>
          <HotelOptionals />
          <IsOptional>false</IsOptional>
          <IsPromotion>false</IsPromotion>
        </RoomClass>
        <RoomClass>
          <ClassName>Studio</ClassName>
          <Rooms>
            <Room>
              <RoomID>252</RoomID>
              <RoomName>Studio Twin / Double</RoomName>
              <Status>AL</Status>
              <BBCode>RO</BBCode>
              <TotalPrice>1942</TotalPrice>
              <CurrencyCode>SGD</CurrencyCode>
              <AgreementID>56</AgreementID>
              <Adults>2</Adults>
              <Childs>0</Childs>
              <ChildsAge />
              <SupplierRef>
                <SupplierRef>
                  <ID>1</ID>
                  <Value>a03c4e58-f684-4709-8740-b345ac53f933</Value>
                </SupplierRef>
              </SupplierRef>
              <SupplierCode>TPXv43</SupplierCode>
              <Supplier>Internal</Supplier>
              <IsPromotion>false</IsPromotion>
              <PromotionDescription />
              <IsOptional>false</IsOptional>
              <Penalty_Date>2018-10-29T00:00:00</Penalty_Date>
              <Penalty_Type>2</Penalty_Type>
              <Penalty_Val>1942.00</Penalty_Val>
              <RoomPriceBreak>
                <DailyRate>
                  <DateOfRate>2018-11-07T00:00:00</DateOfRate>
                  <CurrencyCode>SGD</CurrencyCode>
                  <DailyRate>971</DailyRate>
                </DailyRate>
                <DailyRate>
                  <DateOfRate>2018-11-08T00:00:00</DateOfRate>
                  <CurrencyCode>SGD</CurrencyCode>
                  <DailyRate>971</DailyRate>
                </DailyRate>
              </RoomPriceBreak>
            </Room>
          </Rooms>
          <Status>AL</Status>
          <TotalPrice>1942</TotalPrice>
          <CurrencyCode>SGD</CurrencyCode>
          <TotalRoom>1</TotalRoom>
          <SupplierCode>TPXv43</SupplierCode>
          <HotelOptionals />
          <IsOptional>false</IsOptional>
          <IsPromotion>false</IsPromotion>
        </RoomClass>
      </RoomClasses>
      <CheckInDate>2018-11-07T00:00:00</CheckInDate>
      <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
    </Hotel>
  </Hotels>
</HotelSearchResp> ，SpendTime : 1 Time:2018/8/9 15:56:24 
发生时间：2018/8/9 15:56:24
---------
日志内容：Studio: CancellationPolicyReq ,Req :<CancellationPolicyReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><Supplier>TRAVPAX</Supplier><HotelID>240237</HotelID><CountryID>SG</CountryID><CityID>7355</CityID><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><NationalityID>86</NationalityID><ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient><RoomClass><TotalPrice>1942.0000</TotalPrice><CurrencyCode>SGD</CurrencyCode><Rooms><Room><ProductID>252</ProductID><Adults>2</Adults><Childs>0</Childs><ChildsAge /><MapRoomID>0</MapRoomID><AgreementID>56</AgreementID><SupplierCode>TRAVPAX</SupplierCode><SupplierRef><SupplierRef><ID>1</ID><Value>a03c4e58-f684-4709-8740-b345ac53f933</Value></SupplierRef></SupplierRef></Room></Rooms></RoomClass></CancellationPolicyReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<CancellationPolicyResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceService>B8A4419E-C335-4425-9847-6C27CE235EDF</ReferenceService>
  <ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient>
  <Version>5.0.456.0</Version>
  <ServiceID>240237</ServiceID>
  <PenaltyCurrency>SGD</PenaltyCurrency>
  <Description />
  <CancelType>2</CancelType>
  <CancelDate>2018-10-29T00:00:00</CancelDate>
  <CancelPrice>1942.00</CancelPrice>
  <CurrencyCode>SGD</CurrencyCode>
</CancellationPolicyResp> ，SpendTime : 1 , Time:2018/8/9 15:56:27 
发生时间：2018/8/9 15:56:27
---------
日志内容：Studio: CancellationPolicyReq ,Req :<CancellationPolicyReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><Supplier>TRAVPAX</Supplier><HotelID>240237</HotelID><CountryID>SG</CountryID><CityID>7355</CityID><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><NationalityID>86</NationalityID><ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient><RoomClass><TotalPrice>730.0000</TotalPrice><CurrencyCode>SGD</CurrencyCode><Rooms><Room><ProductID>319</ProductID><Adults>2</Adults><Childs>0</Childs><ChildsAge /><MapRoomID>0</MapRoomID><AgreementID>73</AgreementID><SupplierCode>TRAVPAX</SupplierCode><SupplierRef><SupplierRef><ID>1</ID><Value>a55b9ac4-2ff0-4c9a-95ae-151d5c950aa5</Value></SupplierRef></SupplierRef></Room></Rooms></RoomClass></CancellationPolicyReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<CancellationPolicyResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceService>B8A4419E-C335-4425-9847-6C27CE235EDF</ReferenceService>
  <ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient>
  <Version>5.0.456.0</Version>
  <ServiceID>240237</ServiceID>
  <PenaltyCurrency>SGD</PenaltyCurrency>
  <Description />
  <CancelType>2</CancelType>
  <CancelDate>2018-10-29T00:00:00</CancelDate>
  <CancelPrice>730.00</CancelPrice>
  <CurrencyCode>SGD</CurrencyCode>
</CancellationPolicyResp> ，SpendTime : 1 , Time:2018/8/9 15:56:32 
发生时间：2018/8/9 15:56:32
---------
日志内容：Studio:HotelRecheckPriceReq, Req : <HotelRecheckPriceReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><CountryID>SG</CountryID><CityID>7355</CityID><HotelID>240237</HotelID><NationalityID>86</NationalityID><ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient><RoomClass><TotalPrice>730.0000</TotalPrice><Rooms><Room><ProductID>319</ProductID><Adults>2</Adults><Childs>0</Childs><ChildsAge /><SupplierRef><SupplierRef><ID>1</ID><Value>a55b9ac4-2ff0-4c9a-95ae-151d5c950aa5</Value></SupplierRef></SupplierRef><MapRoomID>0</MapRoomID><AgreementID>73</AgreementID><SupplierCode>SupplierCode</SupplierCode></Room></Rooms></RoomClass></HotelRecheckPriceReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<HotelRecheckPriceResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Version>5.0.456.0</Version>
  <ReferenceService>B8A4419E-C335-4425-9847-6C27CE235EDF</ReferenceService>
  <ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient>
  <Hotels>
    <HotelID>240237</HotelID>
    <HotelName>Fragrance Bugis</HotelName>
    <CountryID>SG</CountryID>
    <CityID>7355</CityID>
    <StarRating>3</StarRating>
    <Address>33, Middle Road</Address>
    <Address2>188942 Singapore</Address2>
    <Fax />
    <Email />
    <RoomClasses>
      <RoomClass>
        <Rooms>
          <Room>
            <RoomID>319</RoomID>
            <RoomName>Deluxe Double</RoomName>
            <Status>AL</Status>
            <BBCode>BB</BBCode>
            <TotalPrice>730</TotalPrice>
            <CurrencyCode>SGD</CurrencyCode>
            <AgreementID>73</AgreementID>
            <Adults>2</Adults>
            <Childs>0</Childs>
            <SupplierRef>
              <SupplierRef>
                <ID>1</ID>
                <Value>EAAAAAZIXd1runlQH1lxyrpI5e6uif6dOZf740QquJz0grZ+/L0msOx6uw3nUZKDyNDQu0GYf5Bko/IG8phZK5Qki8TMYDVtabB5cqPbcZT8ijVn2v7OB4K4e6rXnf9wlkGLaqeC/rYZQLbloTBEC5l0P0Mo49Rz+XL0o5V2MIizeAtcmC18ZnwGNcQTDFMb9uf+E/rU7FzZhAA0kufwX2dB5BUKLfYYI8TxVLhmOegM7z9b2zNpxrzZfzeJrrcRX4NBOZXaJ3QVmnPI/IYHRCHMjDRJoM4QuposYRS6NLDoQPSr/5mcyUhVHBSDFHJfBI/1XcVgYBnR5545tWanrdO9JSuVzAq+I9Izrodksk8tbe35cyxC2IZsQ03Q1dcdbq1MhV9Qi1c9+Ik72zf2vrNpW97bl/vwq9Ox6zMPikVaEcIsaUjMjRTfKjG9bu9nCUJWFss7F0V20S53Bo8hGuMOSz5xkjvbMUMO5Agbuscp0YwdpZHaYiv/11OF7uUtTV1HUfS3vEWVpJa9wx5gnJDOuwQVJPpLyQyvTwPCtD7+dCqmnGe9Af5aZItP2BHAADGQW0qUiFB6vU6jDvZ86YJDsRG8gBnSad4zZLlHyJQZiGxDSQ06DkJWuHXMHxf6xPH6rcaxl6ncJfTnDQtJJ1jlQFSGD6HL5hEcnrAZAkMAYt7qXc9GeegtHMv7glku3MloYVqGOS1KJXf09vyal1e1HvSq7vGziWZPT6XSIV8Toq49z9VyVmm6ncSP9+/Q6ONgc3DYnhsBEK1B/i0z97tm1OnFg++bxFY5fZGpTyyJ/KgM6o0N/StNA9zWNand6x6gPUF2fmGxE/J9D4hCSYtZcsI9oLEaJ1NE0HKWRTLJw2enCHntLh+cOUnXTbvtJB4DvpgognlvRZrUQxlmBJFvPKyoWySDuxIVidvJF2TCRS9XBSzS/GdWhJYub2oVz9bvLRh0blfKxKxym00G7mo+qM2fRE4/YOXs5sEqyS/xCyCobqcMckZGCWv1+VFPszw5fMDJ2j/MjFJ4RIdKhOk0j23UKj5DMBl+hZpeYGauwNDan8RjO6t3VAjVhkA6RKCWR0ID9yt5bDb/2xkT3oMOAsiplbR8SjJyGL2nCiiQWPXLHeRdkIbwbt+bFF2dnZUIvQo0EG91Zwe/dumb5T9lwcUy3szZiik3ToWO/W93c3RVU0dVMohYyvivlYnki+/QwkN3b4Q5CDG297h04qGd8wekLt0UoPg/e9rfgynCbYmsN4xf4BO1/J0UJt0Ce4tofAuF8neNz+YeRBDXaEI9I6KjjIMs8g5LYgpep9BwkiPXxFIkSmKDdy82BKhYa4Sn3B/kmV3Q4OFHQjEfPjzUVdsD2L6ogXgyko9Aj/xgtMsj1G2Ubr6IAuzKJK8DaOBbp5ZP+aTalJas2FLCU9WsyYeo8hnmfWXc5l+7bulMVYiePXJ+SbwBNK4Ad/1BrLsuKiom/2WVGNXhlry0lJ5NNgW1yZawQEf1z9yBbpAGkOtxl4Sy3y3urvecZ9eKA81RvZaFfkDgLQirIHffmQGeY3uGjZoLx83Gy8igtjvjGczM9GrCwMiYf060zFyGFfxyW9O2WfDM1EFS6e+Dkt6HN8XINqffLTj0OaAUSqjOEJY+7ck0qTiwjMliwYoVIwytWziexqGv78dpkVQeHLiLxi2OuyXL3P8OoCJ8ofZ86ZXe4BfeUCMmHBiLTONxO+lmFLuVK9dGmEtrS1N51tN/1WVHFYglxjpPtMPmh2SQWU7n2XYWTME/1YjnyfOmFoAV4oMQAYjJVTlyF+qBcOAyXSddtY4DtNTeB2P617AjmeJ3fipnQ0+ZD3otKHW0TTHDZw==</Value>
              </SupplierRef>
            </SupplierRef>
            <SupplierCode>TPXv43</SupplierCode>
            <Supplier>Internal</Supplier>
            <IsPromotion>false</IsPromotion>
            <PromotionDescription />
            <IsOptional>false</IsOptional>
            <Penalty_Date>2018-10-29T00:00:00</Penalty_Date>
            <Penalty_Type>2</Penalty_Type>
            <Penalty_Val>730.00</Penalty_Val>
            <RoomPriceBreak>
              <DailyRate>
                <DateOfRate>2018-11-07T00:00:00</DateOfRate>
                <ServiceID>0</ServiceID>
                <CurrencyCode>SGD</CurrencyCode>
                <DailyRate>365</DailyRate>
              </DailyRate>
              <DailyRate>
                <DateOfRate>2018-11-08T00:00:00</DateOfRate>
                <ServiceID>0</ServiceID>
                <CurrencyCode>SGD</CurrencyCode>
                <DailyRate>365</DailyRate>
              </DailyRate>
            </RoomPriceBreak>
          </Room>
        </Rooms>
        <Status>AL</Status>
        <TotalPrice>730</TotalPrice>
        <CurrencyCode>SGD</CurrencyCode>
        <TotalRoom>1</TotalRoom>
        <SupplierCode>TPXv43</SupplierCode>
        <IsOptional>false</IsOptional>
        <IsPromotion>false</IsPromotion>
      </RoomClass>
    </RoomClasses>
    <CheckInDate>2018-11-07T00:00:00</CheckInDate>
    <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
  </Hotels>
</HotelRecheckPriceResp> ，SpendTime : 1 ， Time:2018/8/9 15:56:37 
发生时间：2018/8/9 15:56:37
---------
日志内容：Studio:CreatePNRReq, Req : <CreatePNRReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><Supplier>TRAVPAX</Supplier><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><CountryID>SG</CountryID><CityID>7355</CityID><HotelID>240237</HotelID><NationalityID>86</NationalityID><ReferenceClient>e620bbbe7bfe4077b91a1d7690578197</ReferenceClient><Leading><Nationality>66</Nationality><Title>Mr</Title><FirstName>Zhang</FirstName><LastName>San</LastName></Leading><RoomClass><TotalPrice>730.0000</TotalPrice><CurrencyCode>SGD</CurrencyCode><Rooms><Room><ProductID>319</ProductID><Adults>2</Adults><Childs>0</Childs><ChildsAge /><PaxInfo><Title>Mr.</Title><FirstName>Zhang</FirstName><LastName>San</LastName></PaxInfo><PaxInfo><Title>Mr.</Title><FirstName>Li</FirstName><LastName>Si</LastName></PaxInfo><SupplierRef><SupplierRef><ID>1</ID><Value>a55b9ac4-2ff0-4c9a-95ae-151d5c950aa5</Value></SupplierRef></SupplierRef></Room></Rooms></RoomClass></CreatePNRReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<PNRResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceService>B8A4419E-C335-4425-9847-6C27CE235EDF</ReferenceService>
  <Version>5.0.456.0</Version>
  <Supplier>TRAVPAX</Supplier>
  <PNRNo>IA101808000055</PNRNo>
  <PNRStatus>OK</PNRStatus>
  <Services>
    <Service>
      <ID>240237</ID>
      <Name>Fragrance Bugis</Name>
      <LegID>17321</LegID>
      <Leg_Status>VC</Leg_Status>
      <Rooms>
        <Room>
          <RoomName>Deluxe Double</RoomName>
          <Supplier>Internal</Supplier>
          <BBCode>BB</BBCode>
          <TotalPrice>365.0000</TotalPrice>
          <CurrencyCode>SGD</CurrencyCode>
          <Adults>2</Adults>
          <Childs>0</Childs>
          <IsPromotion>false</IsPromotion>
          <IsOptional>false</IsOptional>
        </Room>
      </Rooms>
      <CheckInDate>2018-11-07T00:00:00</CheckInDate>
      <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
      <NumberOfNight>2</NumberOfNight>
      <TotalAdults>2</TotalAdults>
      <TotalChilds>0</TotalChilds>
      <TotalRooms>1</TotalRooms>
      <CountryID>SG</CountryID>
      <CityID>7355</CityID>
      <TotalFare>730.0000</TotalFare>
      <FareCurrency>SGD</FareCurrency>
    </Service>
  </Services>
  <Leading>
    <Nationality>66</Nationality>
    <Title>Mr</Title>
    <FirstName>Zhang</FirstName>
    <LastName>San</LastName>
  </Leading>
  <CreateDate>2018-08-09T15:57:00</CreateDate>
  <Remark />
  <Refer>TPXv43/IA101808000055/17321/VC</Refer>
  <AgentRefer />
</PNRResp> ，SpendTime : 9 ， Time:2018/8/9 15:56:51 
发生时间：2018/8/9 15:56:51
---------
日志内容：Studio:CancelPNRReq, Req : <CancelPNRReq> <Username>BCNXIWGCertification</Username> <Password>7WmCewfStPkMl7F</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>IA101808000055</PNRNo> <ConfirmCode>TPXv43/IA101808000055/17321/VC</ConfirmCode> </CancelPNRReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<CancelPNRResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceService>B8A4419E-C335-4425-9847-6C27CE235EDF</ReferenceService>
  <Version>5.0.456.0</Version>
  <CancelDate>0001-01-01T00:00:00</CancelDate>
  <Supplier>TRAVPAX</Supplier>
  <PNRNo>IA101808000055</PNRNo>
  <PNRStatus>RQ</PNRStatus>
  <Services>
    <Service>
      <ID>240237</ID>
      <Name>Fragrance Bugis</Name>
      <LegID>17321</LegID>
      <Leg_Status>XX</Leg_Status>
      <Rooms>
        <Room>
          <RoomName>Deluxe Double</RoomName>
          <BBCode>BB</BBCode>
          <Adults>2</Adults>
          <Childs>0</Childs>
          <IsPromotion>false</IsPromotion>
          <IsOptional>false</IsOptional>
        </Room>
      </Rooms>
      <CheckInDate>2018-11-07T00:00:00</CheckInDate>
      <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
      <NumberOfNight>2</NumberOfNight>
      <TotalAdults>2</TotalAdults>
      <TotalChilds>0</TotalChilds>
      <TotalRooms>1</TotalRooms>
      <CityID>0</CityID>
      <TotalFare>730.0000</TotalFare>
      <FareCurrency>SGD</FareCurrency>
      <IsCanceled>true</IsCanceled>
    </Service>
  </Services>
</CancelPNRResp> ，SpendTime : 1 ， Time:2018/8/9 15:57:01 
发生时间：2018/8/9 15:57:01
