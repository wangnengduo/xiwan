---------
日志内容：TravPax：HotelSearchReq ,Req :<HotelSearchReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><CountryID>SG</CountryID><CityID>7355</CityID><HotelID>279688</HotelID><NationalityID>86</NationalityID><ReferenceClient /><Rooms><RoomRequest><Adults>2</Adults><Childs>0</Childs><ChildsAge /></RoomRequest></Rooms></HotelSearchReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<HotelSearchResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceClient>bb0bee1aba484c31a6974891f59235e8</ReferenceClient>
  <ReferenceService>E7918860-AAD4-402F-9412-00FB8F00A314</ReferenceService>
  <Version>5.0.456.0</Version>
  <Hotels>
    <Hotel>
      <HotelID>279688</HotelID>
      <HotelName>8 on Claymore Serviced Residences</HotelName>
      <CountryID>SG</CountryID>
      <CityID>7355</CityID>
      <CityName />
      <StarRating>3</StarRating>
      <Address>8 Claymore Hill</Address>
      <Address2>Singapore 229572</Address2>
      <Fax>+6567378688</Fax>
      <Email>info@8onclaymore.com.sg</Email>
      <RoomClasses>
        <RoomClass>
          <ClassName>Deluxe</ClassName>
          <Rooms>
            <Room>
              <RoomID>264</RoomID>
              <RoomName>Deluxe Twin / Double</RoomName>
              <Status>AL</Status>
              <BBCode>RO</BBCode>
              <TotalPrice>3798</TotalPrice>
              <CurrencyCode>SGD</CurrencyCode>
              <AgreementID>60</AgreementID>
              <Adults>2</Adults>
              <Childs>0</Childs>
              <ChildsAge />
              <SupplierRef>
                <SupplierRef>
                  <ID>1</ID>
                  <Value>db7b3b2d-db58-44ee-aeee-3d4ac5f0d0fc</Value>
                </SupplierRef>
              </SupplierRef>
              <SupplierCode>TPXv43</SupplierCode>
              <Supplier>Internal</Supplier>
              <IsPromotion>false</IsPromotion>
              <PromotionDescription />
              <IsOptional>false</IsOptional>
              <Penalty_Date>2018-10-31T00:00:00</Penalty_Date>
              <Penalty_Type>2</Penalty_Type>
              <Penalty_Val>3418.20</Penalty_Val>
              <RoomPriceBreak>
                <DailyRate>
                  <DateOfRate>2018-11-07T00:00:00</DateOfRate>
                  <CurrencyCode>SGD</CurrencyCode>
                  <DailyRate>1899</DailyRate>
                </DailyRate>
                <DailyRate>
                  <DateOfRate>2018-11-08T00:00:00</DateOfRate>
                  <CurrencyCode>SGD</CurrencyCode>
                  <DailyRate>1899</DailyRate>
                </DailyRate>
              </RoomPriceBreak>
            </Room>
          </Rooms>
          <Status>AL</Status>
          <TotalPrice>3798</TotalPrice>
          <CurrencyCode>SGD</CurrencyCode>
          <TotalRoom>1</TotalRoom>
          <SupplierCode>TPXv43</SupplierCode>
          <HotelOptionals />
          <IsOptional>false</IsOptional>
          <IsPromotion>false</IsPromotion>
        </RoomClass>
      </RoomClasses>
      <CheckInDate>2018-11-07T00:00:00</CheckInDate>
      <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
    </Hotel>
  </Hotels>
</HotelSearchResp> ，SpendTime : 2 Time:2018/8/9 16:30:53 
发生时间：2018/8/9 16:30:53
---------
日志内容：Studio: CancellationPolicyReq ,Req :<CancellationPolicyReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><Supplier>TRAVPAX</Supplier><HotelID>279688</HotelID><CountryID>SG</CountryID><CityID>7355</CityID><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><NationalityID>86</NationalityID><ReferenceClient>bb0bee1aba484c31a6974891f59235e8</ReferenceClient><RoomClass><TotalPrice>3798.0000</TotalPrice><CurrencyCode>SGD</CurrencyCode><Rooms><Room><ProductID>264</ProductID><Adults>2</Adults><Childs>0</Childs><ChildsAge /><MapRoomID>0</MapRoomID><AgreementID>60</AgreementID><SupplierCode>TRAVPAX</SupplierCode><SupplierRef><SupplierRef><ID>1</ID><Value>db7b3b2d-db58-44ee-aeee-3d4ac5f0d0fc</Value></SupplierRef></SupplierRef></Room></Rooms></RoomClass></CancellationPolicyReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<CancellationPolicyResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceService>E7918860-AAD4-402F-9412-00FB8F00A314</ReferenceService>
  <ReferenceClient>bb0bee1aba484c31a6974891f59235e8</ReferenceClient>
  <Version>5.0.456.0</Version>
  <ServiceID>279688</ServiceID>
  <PenaltyCurrency>SGD</PenaltyCurrency>
  <Description />
  <CancelType>2</CancelType>
  <CancelDate>2018-10-31T00:00:00</CancelDate>
  <CancelPrice>3418.20</CancelPrice>
  <CurrencyCode>SGD</CurrencyCode>
</CancellationPolicyResp> ，SpendTime : 1 , Time:2018/8/9 16:30:57 
发生时间：2018/8/9 16:30:57
---------
日志内容：Studio:HotelRecheckPriceReq, Req : <HotelRecheckPriceReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><CountryID>SG</CountryID><CityID>7355</CityID><HotelID>279688</HotelID><NationalityID>86</NationalityID><ReferenceClient>bb0bee1aba484c31a6974891f59235e8</ReferenceClient><RoomClass><TotalPrice>3798.0000</TotalPrice><Rooms><Room><ProductID>264</ProductID><Adults>2</Adults><Childs>0</Childs><ChildsAge /><SupplierRef><SupplierRef><ID>1</ID><Value>db7b3b2d-db58-44ee-aeee-3d4ac5f0d0fc</Value></SupplierRef></SupplierRef><MapRoomID>0</MapRoomID><AgreementID>60</AgreementID><SupplierCode>SupplierCode</SupplierCode></Room></Rooms></RoomClass></HotelRecheckPriceReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<HotelRecheckPriceResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Version>5.0.456.0</Version>
  <ReferenceService>E7918860-AAD4-402F-9412-00FB8F00A314</ReferenceService>
  <ReferenceClient>bb0bee1aba484c31a6974891f59235e8</ReferenceClient>
  <Hotels>
    <HotelID>279688</HotelID>
    <HotelName>8 on Claymore Serviced Residences</HotelName>
    <CountryID>SG</CountryID>
    <CityID>7355</CityID>
    <StarRating>3</StarRating>
    <Address>8 Claymore Hill</Address>
    <Address2>Singapore 229572</Address2>
    <Fax>+6567378688</Fax>
    <Email>info@8onclaymore.com.sg</Email>
    <RoomClasses>
      <RoomClass>
        <Rooms>
          <Room>
            <RoomID>264</RoomID>
            <RoomName>Deluxe Twin / Double</RoomName>
            <Status>AL</Status>
            <BBCode>RO</BBCode>
            <TotalPrice>3798</TotalPrice>
            <CurrencyCode>SGD</CurrencyCode>
            <AgreementID>60</AgreementID>
            <Adults>2</Adults>
            <Childs>0</Childs>
            <SupplierRef>
              <SupplierRef>
                <ID>1</ID>
                <Value>EAAAAP0T/asWA94isL3UkrNSvWOvVtaBqSm4OJSpPRDONPqL7SkDm369FLaDh/z4yAUEMRPKV28U2Ge/jQLhiD31NboQ0YlzpQ2vnhC2FljuGx0IIm1QjXShjzQUNhDbrVR1knothGiUHnjvZIF6NiiAmSPqxLPPfMLH94LPC8zmECC0eGYM8sgjURrQ5sFvDyJLvulzfuCT8zDvUYl4gzPVErYQqV9qLUUsVHadKnG3oDLDFRzmzNLVmi/lQCTnKTahQJVTCQCsBnrUwmI8hq04FE+VL27YPcf3RTY1FfY3VzgzwFUVlCk+8/GgZ+XZxH2UfD8om509nSdQRYYuGPLJwiEua++9q8FVep0BPmQl3eA9tUQXAGA2i32PZb/BOWp3QY4DS3OcDWgGkJA8ANiN6NtmtUAusm26xCnK5DAlXbvJRkNhRjOP+IPI3H+4HwV9vQKYBTjB+TTn5zQPwgDfPt8b+Z7/7lPk77HTguMqRL4+kiDi4EVliFxI3M/fJcWXfrkESFOkJzpRS6gCswq/BS4eRMGxIlkGkNSA+BnqHYJDYXgWq94jVMnj0ZzNkuTsnKJPaS2dHwpYBikGuuoA1R6xvUZgf/VO+tJD6r+h7Jb1AwICvrd/O/Gut6bP0loxhEAt/wMXwILneeigBabFoFmbRShU4NuAdQ6KUGA6ghBllEfNpzNYGB7nM606I4HYwTU9kYi0dSA/T3d9HGHbBgcnjAyAY1AspJuM+16OW+/TKSOaVhZpIEckLCvokKIgI6NzUxVk07QEBT8a+PJ/RWEocoKHvDcTSVWjf9usnTuDIls7rP/KbzCuMmM67owylr8sTFdLRdjQcG0W3o0krkogkcvJ1GvS6zYISlc6PDWrdq9FtZawK0HEY1jvGLWGE2YNa/5EyeqwtlEKjoRtiwo16NSyPfus9sObLZEo98K4BEwLdrPe1i52p5IUdDZ6h/FcxdJPEGyH46reGltsNAA7fFWKCNeL7fvy3uCUlyg4Ta0owdMTtOBskS91bao2rZox3npjEn5jPA7x8ic7GmZUxq2D0qAI0Shr4/WIsJALVRIZkWKvgr0KFt292WAVC9QFXcKAEvv/8/rZfWl4ktiQlMVp5OuX+MxQ07ffejGSR5nCZIgzfzDqwG7HgW19zAVJrK6mf67PlnY9xApPWHwgTY2j6b5reVbF1AMVa9MCx29jRqVVm9uguV3dQzae1XeTWk+pETI/EhB4iY825BnabOKqDkFopfzeBXXCSsO19MT4Sqo7oYpG7yRI/PK/UCLkAq7/QV9E+2Lg0ORgGdfK39xAV+569jlUqylJcoHgCB1dbUJH+3CUQw+xzSQajLCIcJYQS9a7i0+JT255ZFc5IzIoU9gZLu8rnX8KfdG43I5R2AIRN3pqUVQUMCs+MScNXYwnV6lgJxUvwUV+tNZTFL5UtmUv/2I5ePshroL3Z/bIsbYAcNPcxTB49DsPyc97dsICPWDUhjoCwCU7Der0dkTtQCceWTmxpCJVR9M194AtsgzANpvgE4KM9O4uOlx+nKi2ooY8L7f2DKHe+p+4C1Z8LKmyfy/TdeZPOivcZJi8yi5tIufiRqR9bx1Kyh2AGB7WPkYwlvvlBV85iUku0ONZJysR0CmT6v9oQ2JlDeBvmt52VSVpc0ZLc7GqFcbxH8RC8sxbqL1G9XFskkYsC8CNq75jcX37M21dif62SQykMkgW/ALcEvxxQ6NSV7pfapV90u1AdgqTJ9c8MZYvg2EEm2+7UcNOpVtAwJYzcG6woD35w4bPkkgjUJBLKl5uYFFJRRk+wt/M4QjRDQdOyzWj5hbMpxeH/JRmn5r/f6n0Kz5oQsoDMoiuZ/jSHZRkmK6qRgODtQ5dSRZeCDc=</Value>
              </SupplierRef>
            </SupplierRef>
            <SupplierCode>TPXv43</SupplierCode>
            <Supplier>Internal</Supplier>
            <IsPromotion>false</IsPromotion>
            <PromotionDescription />
            <IsOptional>false</IsOptional>
            <Penalty_Date>2018-10-31T00:00:00</Penalty_Date>
            <Penalty_Type>2</Penalty_Type>
            <Penalty_Val>3418.20</Penalty_Val>
            <RoomPriceBreak>
              <DailyRate>
                <DateOfRate>2018-11-07T00:00:00</DateOfRate>
                <ServiceID>0</ServiceID>
                <CurrencyCode>SGD</CurrencyCode>
                <DailyRate>1899</DailyRate>
              </DailyRate>
              <DailyRate>
                <DateOfRate>2018-11-08T00:00:00</DateOfRate>
                <ServiceID>0</ServiceID>
                <CurrencyCode>SGD</CurrencyCode>
                <DailyRate>1899</DailyRate>
              </DailyRate>
            </RoomPriceBreak>
          </Room>
        </Rooms>
        <Status>AL</Status>
        <TotalPrice>3798</TotalPrice>
        <CurrencyCode>SGD</CurrencyCode>
        <TotalRoom>1</TotalRoom>
        <SupplierCode>TPXv43</SupplierCode>
        <IsOptional>false</IsOptional>
        <IsPromotion>false</IsPromotion>
      </RoomClass>
    </RoomClasses>
    <CheckInDate>2018-11-07T00:00:00</CheckInDate>
    <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
  </Hotels>
</HotelRecheckPriceResp> ，SpendTime : 1 ， Time:2018/8/9 16:31:00 
发生时间：2018/8/9 16:31:00
---------
日志内容：Studio:CreatePNRReq, Req : <CreatePNRReq><Username>BCNXIWGCertification</Username><Password>7WmCewfStPkMl7F</Password><Supplier>TRAVPAX</Supplier><CheckInDate>2018-11-07</CheckInDate><CheckOutDate>2018-11-09</CheckOutDate><CountryID>SG</CountryID><CityID>7355</CityID><HotelID>279688</HotelID><NationalityID>86</NationalityID><ReferenceClient>bb0bee1aba484c31a6974891f59235e8</ReferenceClient><Leading><Nationality>66</Nationality><Title>Mr</Title><FirstName>Zhang</FirstName><LastName>San</LastName></Leading><RoomClass><TotalPrice>3798.0000</TotalPrice><CurrencyCode>SGD</CurrencyCode><Rooms><Room><ProductID>264</ProductID><Adults>2</Adults><Childs>0</Childs><ChildsAge /><PaxInfo><Title>Mr.</Title><FirstName>Zhang</FirstName><LastName>San</LastName></PaxInfo><PaxInfo><Title>Mr.</Title><FirstName>Li</FirstName><LastName>Si</LastName></PaxInfo><SupplierRef><SupplierRef><ID>1</ID><Value>db7b3b2d-db58-44ee-aeee-3d4ac5f0d0fc</Value></SupplierRef></SupplierRef></Room></Rooms></RoomClass></CreatePNRReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<PNRResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceService>E7918860-AAD4-402F-9412-00FB8F00A314</ReferenceService>
  <Version>5.0.456.0</Version>
  <Supplier>TRAVPAX</Supplier>
  <PNRNo>IA101808000057</PNRNo>
  <PNRStatus>OK</PNRStatus>
  <Services>
    <Service>
      <ID>279688</ID>
      <Name>8 on Claymore Serviced Residences</Name>
      <LegID>17324</LegID>
      <Leg_Status>VC</Leg_Status>
      <Rooms>
        <Room>
          <RoomName>Deluxe Twin / Double</RoomName>
          <Supplier>Internal</Supplier>
          <BBCode>RO</BBCode>
          <TotalPrice>1899.0000</TotalPrice>
          <CurrencyCode>SGD</CurrencyCode>
          <Adults>2</Adults>
          <Childs>0</Childs>
          <IsPromotion>false</IsPromotion>
          <IsOptional>false</IsOptional>
        </Room>
      </Rooms>
      <CheckInDate>2018-11-07T00:00:00</CheckInDate>
      <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
      <NumberOfNight>2</NumberOfNight>
      <TotalAdults>2</TotalAdults>
      <TotalChilds>0</TotalChilds>
      <TotalRooms>1</TotalRooms>
      <CountryID>SG</CountryID>
      <CityID>7355</CityID>
      <TotalFare>3798.0000</TotalFare>
      <FareCurrency>SGD</FareCurrency>
    </Service>
  </Services>
  <Leading>
    <Nationality>66</Nationality>
    <Title>Mr</Title>
    <FirstName>Zhang</FirstName>
    <LastName>San</LastName>
  </Leading>
  <CreateDate>2018-08-09T16:31:00</CreateDate>
  <Remark />
  <Refer>TPXv43/IA101808000057/17324/VC</Refer>
  <AgentRefer />
</PNRResp> ，SpendTime : 2 ， Time:2018/8/9 16:31:08 
发生时间：2018/8/9 16:31:08
---------
日志内容：Studio:CancelPNRReq, Req : <CancelPNRReq> <Username>BCNXIWGCertification</Username> <Password>7WmCewfStPkMl7F</Password> <Supplier>TRAVPAX</Supplier> <PNRNo>IA101808000057</PNRNo> <ConfirmCode>TPXv43/IA101808000057/17324/VC</ConfirmCode> </CancelPNRReq> , Resq ：<?xml version="1.0" encoding="utf-8"?>
<CancelPNRResp xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <ReferenceService>E7918860-AAD4-402F-9412-00FB8F00A314</ReferenceService>
  <Version>5.0.456.0</Version>
  <CancelDate>0001-01-01T00:00:00</CancelDate>
  <Supplier>TRAVPAX</Supplier>
  <PNRNo>IA101808000057</PNRNo>
  <PNRStatus>RQ</PNRStatus>
  <Services>
    <Service>
      <ID>279688</ID>
      <Name>8 on Claymore Serviced Residences</Name>
      <LegID>17324</LegID>
      <Leg_Status>XX</Leg_Status>
      <Rooms>
        <Room>
          <RoomName>Deluxe Twin / Double</RoomName>
          <BBCode>RO</BBCode>
          <Adults>2</Adults>
          <Childs>0</Childs>
          <IsPromotion>false</IsPromotion>
          <IsOptional>false</IsOptional>
        </Room>
      </Rooms>
      <CheckInDate>2018-11-07T00:00:00</CheckInDate>
      <CheckOutDate>2018-11-09T00:00:00</CheckOutDate>
      <NumberOfNight>2</NumberOfNight>
      <TotalAdults>2</TotalAdults>
      <TotalChilds>0</TotalChilds>
      <TotalRooms>1</TotalRooms>
      <CityID>0</CityID>
      <TotalFare>3798.0000</TotalFare>
      <FareCurrency>SGD</FareCurrency>
      <IsCanceled>true</IsCanceled>
    </Service>
  </Services>
</CancelPNRResp> ，SpendTime : 2 ， Time:2018/8/9 16:31:20 
发生时间：2018/8/9 16:31:20
