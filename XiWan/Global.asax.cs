﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using XiWan.DALFactory;
using XiWan.Data.SunSeries;
//定时器
namespace XiWan
{
    public class Global : System.Web.HttpApplication
    {
        System.Timers.Timer timer;
        protected void Application_Start(object sender, EventArgs e)
        {
            this.timer = new System.Timers.Timer(3600000);//3600000

            // 定时器激发事件
            this.timer.Elapsed += new System.Timers.ElapsedEventHandler(onTimer);
            double iTimersInterval = CConvert.StringToDouble(CCommon.GetWebConfigValue("TimersInterval"));
            iTimersInterval = iTimersInterval == 0 ? 3600000 : iTimersInterval;
            this.timer.Interval = iTimersInterval; //, 3600000;  //1秒=1000毫秒 3600000=1小时
            this.timer.Enabled = true;
            this.timer.AutoReset = true;


            // 启动定时器
            this.timer.Start();
            onTimer(null, null);
        }

        protected void Session_Start(object sender, EventArgs e)
        {

        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {

        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {

        }

        protected void Application_Error(object sender, EventArgs e)
        {
            try
            {
                /* 在出现未处理的错误时运行的代码*/
                Exception objErr = Server.GetLastError().GetBaseException();
                if (objErr.Message.Contains("无法验证数据") || objErr.Message.Contains("字符数组的无效长度"))
                {
                }
                else
                {
                    string error = "发生异常页: " + Request.Url.ToString() + "<br>";
                    LogHelper.DoOperateLog(error + objErr.ToString(), "ApplicationPage");
                }

                //error += "异常信息: " + objErr.Message + "<br>";
                //Server.ClearError();
                //Application["error"] = error;
            }
            catch (Exception ex)
            {
                //Application["error"] = "异常信息: " + ex.Message + "<br>";
            }
        }

        protected void Session_End(object sender, EventArgs e)
        {

        }

        protected void Application_End(object sender, EventArgs e)
        {
            try
            {
                //  在应用程序关闭时运行的代码
                // 关闭
                LogHelper.DoOperateLog("Application结束：" + DateTime.Now, "Application");

                // 重新激发
                //System.Threading.Thread.Sleep(1000);

                ////这里设置您的web地址，可以随便指向您的任意一个aspx页面甚至不存在的页面，目的是要激发Application_Start
                //string url = System.Configuration.ConfigurationSettings.AppSettings["ApplicationRedirectURL"].ToString(); ;
                //System.Net.HttpWebRequest myHttpWebRequest = (System.Net.HttpWebRequest)System.Net.WebRequest.Create(url);
                //System.Net.HttpWebResponse myHttpWebResponse = (System.Net.HttpWebResponse)myHttpWebRequest.GetResponse();
                //System.IO.Stream receiveStream = myHttpWebResponse.GetResponseStream();//得到回写的字节流
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog("Application重启服务错误：" + ex.Message, "Application");
            }
        }

        #region 应用程序定时检查任务
        void onTimer(object sender, EventArgs e)
        {
            try
            {
                //定时获取SunSeries  token
                //SunSeriesDAL sunSeries = new SunSeriesDAL();
                //sunSeries.GetAuthenticate();
            }
            catch (Exception ex)
            {
                LogHelper.DoOperateLog(ex.ToString(), "GlobalLog");
            }
        }
        #endregion
    }
}