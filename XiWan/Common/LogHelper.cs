﻿/*
 * 作者: Wong
 * 日期: 2018-6-13
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web;
using System.Data;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace Reptilian.Common
{
    public class LogHelper
    {
        public delegate void AsyncFactorWriter(ref string primefactor1, ref string primefactor2);
        ManualResetEvent waiter;
        #region 属性
        /// <summary>
        /// 日志目录获取
        /// </summary>
        public static string LogFolderRoot
        {
            get
            {
                string strPath;
                strPath = FilterToDefault(System.Configuration.ConfigurationSettings.AppSettings["LogFolderRoot"], "~/Log/");
                if (strPath.Contains(":"))
                {
                    return strPath;
                }
                else
                {
                    if (System.Web.HttpContext.Current != null)
                    {
                        return System.Web.HttpContext.Current.Server.MapPath(strPath);
                    }
                    else
                    {
                        return System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\Log\\";
                    }
                }
            }
        }
        #endregion

        #region 异常错误处理
        /// <summary>
        ///实现功能：写异常错误 
        /// </summary>
        /// <param name="sException">表示在应用程序执行期间发生的错误描述字段</param>
        public static void DoWriteLog(string errSource, string sException)
        {
            string sFilePath = LogFolderRoot + "Err\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            try
            {
                WriteErrorText(sFilePath, "错误来源：" + errSource + "；错误信息" + sException);
            }
            catch (System.Exception oErr)
            {
                //return oErr.Message.ToString();
            }
            // return sException;
        }

        /// <summary>
        ///实现功能：写错误信息
        ///传递参数说明：sPathFileName 日志文件的路径和文件
        ///sErrInfo 描述当前异常的消息
        ///返回结果说明：无
        /// </summary>
        /// <param name="sPathFileName"></param>
        /// <param name="sErrInfo"></param>
        private static void WriteErrorText(string sPathFileName, string sErrInfo)
        {
            if (GetWebConfigValue("IsWriteLog") == "0")
                return;
            IOHelper.CreateNotExistsFile(sPathFileName);
            System.IO.StreamWriter newStreamWriter = System.IO.File.AppendText(sPathFileName);
            newStreamWriter.WriteLine("--------");
            newStreamWriter.WriteLine("异常信息：" + sErrInfo + " ");
            newStreamWriter.WriteLine("发生时间：" + DateTime.Now.ToString() + "　");
            //			newStreamWriter.WriteLine(" 【触发ＩＰ】："+this.Request.Url. .UserHostName.ToString()  + "");
            //newStreamWriter.WriteLine("--------");
            newStreamWriter.Close();
        }
        #endregion

        #region 一般的日志记录
        /// <summary>
        /// 使用异步保存日志存取
        /// </summary>
        /// <param name="strLogText">操作信息</param>
        /// <param name="strTypeName">所要保存的自定义文件夹名称，可空，默认在OperateLog的public里</param>
        public static void DoOperateLog(string strLogText, string strTypeName)
        {
            //waiter = new ManualResetEvent(false);
            AsyncFactorWriter factorDelegate = new AsyncFactorWriter(LogHelper.WriteOperateText);

            if (strTypeName == null || strTypeName == "")
                strTypeName = "public";
            string sFilePath = LogFolderRoot + "OperateLog\\" + strTypeName + "\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

            IAsyncResult result = factorDelegate.BeginInvoke(
                              ref sFilePath,
                              ref strLogText,
                              null,
                              null);
            //waiter.Set();
            //factorDelegate.EndInvoke(ref sFilePath, ref strLogText, result);

        }

        /// <summary>
        /// 写操作信息
        /// </summary>
        /// <param name="sPathFileName"></param>
        /// <param name="strLogText"></param>
        private static void WriteOperateText(ref string sPathFileName, ref string strLogText)
        {
            try
            {
                if (GetWebConfigValue("IsWriteLog") == "0")
                    return;
                IOHelper.CreateNotExistsFile(sPathFileName);
                System.IO.StreamWriter newStreamWriter;
                newStreamWriter = System.IO.File.AppendText(sPathFileName);
                newStreamWriter.WriteLine("---------");
                newStreamWriter.WriteLine("日志内容：" + strLogText);
                newStreamWriter.WriteLine("发生时间：" + DateTime.Now.ToString());
                newStreamWriter.Close();
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

        /// <summary>
        /// 得到Web.Config文件的配置值appSettings
        /// </summary>
        /// <param name="key">appSettings键</param>
        /// <returns>获取成功返回字串，失败返回""</returns>
        static public string GetWebConfigValue(string key)
        {
            if (System.Web.Configuration.WebConfigurationManager.AppSettings[key] != null && System.Web.Configuration.WebConfigurationManager.AppSettings[key] != "")
                return System.Web.Configuration.WebConfigurationManager.AppSettings[key];
            else
                return "";
        }


        /// <summary>
        /// 过滤可对象，并返回要默认值
        /// </summary>
        /// <param name="objQuest">object</param>
        /// <param name="strDefaultVal">string</param>
        /// <returns>过滤后或设置为默认值的字符串</returns>
        public static string FilterToDefault(object objQuest, string strDefaultVal)
        {
            string strTempQuest = "";
            if (objQuest == null || objQuest.ToString().Trim() == "")
            {
                strTempQuest = strDefaultVal;
            }
            else
            {
                strTempQuest = objQuest.ToString();
            }

            return strTempQuest;
        }
    }
}
