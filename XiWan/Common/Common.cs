﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Web;
using System.Net;
using System.IO;
using System.Data;
using System.Reflection;
using System.Xml.Serialization;
using System.Security.Cryptography;

namespace XiWan.Common
{
    public class Common
    {
        /// <summary>
        /// Get函数
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        private string GetHttp(string url)
        {
            string content = "";
            try
            {
                System.Net.HttpWebRequest myRequest = (System.Net.HttpWebRequest)WebRequest.Create(url);
                myRequest.Method = "GET";
                HttpWebResponse myResponse = (HttpWebResponse)myRequest.GetResponse();
                System.IO.StreamReader reader = new System.IO.StreamReader(myResponse.GetResponseStream(), Encoding.UTF8);
                content = reader.ReadToEnd();
                reader.Close();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return content;
        }

        /// <summary>
        /// Post函数
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string PostHttp(string url, string body)
        {
            string responseContent = "";
            try
            {
                HttpWebRequest httpWebRequest = (HttpWebRequest)WebRequest.Create(url);

                httpWebRequest.ContentType = "application/x-www-form-urlencoded";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 20000;

                byte[] btBodys = Encoding.UTF8.GetBytes(body);
                httpWebRequest.ContentLength = btBodys.Length;
                httpWebRequest.GetRequestStream().Write(btBodys, 0, btBodys.Length);

                HttpWebResponse httpWebResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                StreamReader streamReader = new StreamReader(httpWebResponse.GetResponseStream());
                responseContent = streamReader.ReadToEnd();

                httpWebResponse.Close();
                streamReader.Close();
                httpWebRequest.Abort();
                httpWebResponse.Close();
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return responseContent;
        }

        /// <summary>
        /// WebReq函数
        /// </summary>
        /// <param name="url"></param>
        /// <param name="body">请求内容</param>
        /// <returns></returns>
        public static string WebReq(string url, string body)
        {
            string _returnstr = "";
            /*body = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header>
<BaseInfo xmlns='http://www.rts.co.kr/'>
<SiteCode>XWT1X-00</SiteCode>
<Password>test1234</Password>
<RequestType>NetPartner</RequestType>
</BaseInfo>
</soap:Header>
<soap:Body>
<GetHotelSearchList xmlns='http://www.rts.co.kr/'>
<HotelSearchListNet>
<LanguageCode>EN</LanguageCode>
<TravelerNationality>CN</TravelerNationality>
<CityCode>ROM</CityCode>
<CheckInDate>2018-07-21</CheckInDate>
<CheckOutDate>2018-07-23</CheckOutDate>
<StarRating>0</StarRating>
<LocationCode></LocationCode>
<SupplierCompCode></SupplierCompCode>
<AvailableHotelOnly>true</AvailableHotelOnly>
<RecommendHotelOnly>false</RecommendHotelOnly>
<ClientCurrencyCode>USD</ClientCurrencyCode>
<ItemName></ItemName>
<SellerMarkup>*1</SellerMarkup>
<CompareYn>false</CompareYn>
<SortType></SortType>
<ItemCodeList>
<ItemCodeInfo>
<ItemCode></ItemCode>
<ItemNo>0</ItemNo>
</ItemCodeInfo>
</ItemCodeList>
<RoomsList>
<RoomsInfo>
<BedTypeCode>BED01</BedTypeCode>
<RoomCount>1</RoomCount>
<ChildAge1>0</ChildAge1>
<ChildAge2>0</ChildAge2>
</RoomsInfo>
</RoomsList>
</HotelSearchListNet>
</GetHotelSearchList>
</soap:Body>
</soap:Envelope>");*/
            //发起请求
            WebRequest webRequest = WebRequest.Create(url);
            webRequest.ContentType = "text/xml; charset=utf-8";
            webRequest.Method = "POST";
            using (Stream requestStream = webRequest.GetRequestStream())
            {
                byte[] paramBytes = Encoding.UTF8.GetBytes(body);
                requestStream.Write(paramBytes, 0, paramBytes.Length);
            }
            //响应
            try
            {
                WebResponse webResponse = webRequest.GetResponse();
                using (StreamReader myStreamReader = new StreamReader(webResponse.GetResponseStream(), Encoding.UTF8))
                {
                    _returnstr = myStreamReader.ReadToEnd();
                }
            }
            catch (WebException ex)
            {
                _returnstr = new StreamReader(ex.Response.GetResponseStream()).ReadToEnd();
            }
            return _returnstr;
        }

        //table转为list
        public class ModelConvertHelper<T> where T : new()
        {
            public static List<T> ConvertToModel(DataTable dt)
            {
                // 定义集合    
                List<T> ts = new List<T>();
                // 获得此模型的类型   
                Type type = typeof(T);
                string tempName = "";
                foreach (DataRow dr in dt.Rows)
                {
                    T t = new T();
                    // 获得此模型的公共属性      
                    PropertyInfo[] propertys = t.GetType().GetProperties();
                    foreach (PropertyInfo pi in propertys)
                    {
                        tempName = pi.Name;  // 检查DataTable是否包含此列    
                        if (dt.Columns.Contains(tempName))
                        {
                            // 判断此属性是否有Setter      
                            if (!pi.CanWrite) continue;
                            object value = dr[tempName];
                            if (value != DBNull.Value)
                            {
                                pi.SetValue(t, value, null);
                            }
                        }
                    }
                    ts.Add(t);
                }
                return ts;
            }
        }

        /// <summary>
        /// 分页
        /// </summary>
        /// <param name="dt">总数据</param>
        /// <param name="PageIndex">第几页</param>
        /// <param name="PageSize">每页的记录数</param>
        /// <returns></returns>
        public static DataTable GetPagedTable(DataTable dt, int PageIndex, int PageSize)//PageIndex表示第几页，PageSize表示每页的记录数
        {
            if (PageIndex == 0)
                return dt;//0页代表每页数据，直接返回

            DataTable newdt = dt.Copy();
            newdt.Clear();//copy dt的框架

            int rowbegin = (PageIndex - 1) * PageSize;
            int rowend = PageIndex * PageSize;

            if (rowbegin >= dt.Rows.Count)
                return newdt;//源数据记录数小于等于要显示的记录，直接返回dt

            if (rowend > dt.Rows.Count)
                rowend = dt.Rows.Count;
            for (int i = rowbegin; i <= rowend - 1; i++)
            {
                DataRow newdr = newdt.NewRow();
                DataRow dr = dt.Rows[i];
                foreach (DataColumn column in dt.Columns)
                {
                    newdr[column.ColumnName] = dr[column.ColumnName];
                }
                newdt.Rows.Add(newdr);
            }
            return newdt;
        }
        //把xml转为实体
        public static string XmlSerialize<T>(T obj)
        {
            using (StringWriter sw = new StringWriter())
            {
                Type t = obj.GetType();
                XmlSerializer serializer = new XmlSerializer(obj.GetType());
                serializer.Serialize(sw, obj);
                sw.Close();
                return sw.ToString();
            }
        }

        public static T DESerializer<T>(string strXML) where T : class
        {
            try
            {
                using (StringReader sr = new StringReader(strXML))
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(T));
                    return serializer.Deserialize(sr) as T;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        /// <summary>
        /// MD5 32位 加密(小写)
        /// </summary>
        /// <param name="input">需要加密的字符串</param>
        /// <returns></returns>
        public static string Md5Encrypt32(string input)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] t = md5.ComputeHash(Encoding.UTF8.GetBytes(input));
            StringBuilder sb = new StringBuilder(32);
            for (int i = 0; i < t.Length; i++)
                sb.Append(t[i].ToString("x").PadLeft(2, '0'));
            return sb.ToString();
        }

        /// <summary>
        ///  MD5 16位 加密(大写)
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string Md5Encrypt16(string input)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            string result = BitConverter.ToString(md5.ComputeHash(Encoding.UTF8.GetBytes(input)), 4, 8);
            result = result.Replace("-", "");
            return result;
        }

        /// <summary>
        /// DES加密
        /// </summary>
        /// <param name="key">秘钥</param>
        /// <param name="encryptString">>需要加密的明文</param>
        /// <returns>返回明文</returns>
        public static string Encrypt(string key, string encryptString)
        {
            string str = string.Empty;
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(key.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(encryptString);

                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                str = Convert.ToBase64String(ms.ToArray());
            }
            catch (System.Exception error)
            {
                str = error.Message;
            }

            return str;
        }

        /// <summary>
        /// DES解密
        /// </summary>
        /// <param name="key">秘钥</param>
        /// <param name="decryptString">加密后的密文</param>
        /// <returns></returns>
        public static string Decrypt(string key, string decryptString)
        {
            string str = string.Empty;
            try
            {
                //key = Md5Encrypt16(key).Substring(0, 8);//由于秘钥只能是8位，所以用MD5加密后再取8位
                key = key.Substring(0, 8);
                byte[] keyBytes = Encoding.UTF8.GetBytes(key);
                byte[] keyIV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
                byte[] inputByteArray = Convert.FromBase64String(decryptString);
                DESCryptoServiceProvider provider = new DESCryptoServiceProvider();
                MemoryStream mStream = new MemoryStream();
                CryptoStream cStream = new CryptoStream(mStream, provider.CreateDecryptor(keyBytes, keyIV), CryptoStreamMode.Write);
                cStream.Write(inputByteArray, 0, inputByteArray.Length);
                cStream.FlushFinalBlock();
                str = Encoding.UTF8.GetString(mStream.ToArray());
            }
            catch (System.Exception error)
            {
                str = "0";
            }
            return str;
        }

        /// <summary>
        /// 生成GUID
        /// </summary>
        /// <returns></returns>
        public static string getGUID()
        {
            System.Guid guid = new Guid();
            guid = Guid.NewGuid();
            string str = guid.ToString();
            return str;
        }
    }
}
