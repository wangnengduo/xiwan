﻿
/*----------------------------------------------------------------------------------------------------
 * 代码说明：
 * 
 *      输入输出相关
 * 
 * 创建者： 
 * 
 * 创建日期：
 * 
 * 修改日志： 
 * 
 * BUG处理： 
 * 
 * 版权：开发公共基础类库(c)2005-2008
 ---------------------------------------------------------------------------------------------------*/

using System;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace Reptilian.Common
{
    /// <summary>
    /// 输入输出相关
    /// </summary>
    public class IOHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public IOHelper()
        {

        }
        /// <summary>
        /// 判断文件是否存在，若不存在则创建并创建相关的文件夹
        /// </summary>
        /// <param name="filePath"></param>
        public static void CreateNotExistsFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                //上传文件
                string pathRoot = filePath.Substring(0, filePath.LastIndexOf('\\'));

                try
                {
                    //创建文件夹
                    if (!Directory.Exists(pathRoot))
                    {
                        Directory.CreateDirectory(pathRoot);
                    }
                    //创建文件
                    if (!File.Exists(filePath))
                    {
                        File.Create(filePath).Close();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
    }        

}
