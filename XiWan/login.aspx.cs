﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Survey
{
    public partial class login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btLogin_Click(object sender, EventArgs e)
        {
            string name = txtName.Text;
            string pass = txtPass.Text;
            pass = XiWan.DBUtility.DESEncrypt.Encrypt(pass);
            XiWan.BLL.Com_UserLogin bll = new XiWan.BLL.Com_UserLogin();
            string UserId= bll.GetUserId(name,pass);
            if (UserId == null)
            {
               lblName.Text = "用户名或密码错误";
            }
            else
           {
               XiWan.Model.Com_UserLogin model = bll.GetModel(UserId);
               model.LastLoginDate = DateTime.Now;
               model.LastLoginIP = Page.Request.UserHostAddress;
               bll.Update(model);
               XiWan.Model.View_Users item = new XiWan.Model.View_Users();
               XiWan.BLL.View_Users vbll = new XiWan.BLL.View_Users();
               item = vbll.GetModel(UserId);
               Session["User"] = item;
               XiWan.BLL.Com_LoginLog lbll = new XiWan.BLL.Com_LoginLog();
               XiWan.Model.Com_LoginLog lmodel = new XiWan.Model.Com_LoginLog();
               lmodel.LoginDate = DateTime.Now;
               lmodel.LoginIP = Page.Request.UserHostAddress;
               lmodel.Status = "0";
               lmodel.Userid = UserId;
               lbll.Add(lmodel);
               Response.Redirect("index.html");
           }
        }
    }
}