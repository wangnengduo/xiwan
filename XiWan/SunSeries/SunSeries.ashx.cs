﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.BLL.SunSeries;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;

namespace XiWan.SunSeries
{
    /// <summary>
    /// SunSeries 的摘要说明
    /// </summary>
    public class SunSeries : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            List<RoomOutPrice.Room> rooms = new List<RoomOutPrice.Room>();
            Respone resp = new Respone();
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string reqData = context.Request["data"] ?? "";
            string result = string.Empty;
            string OrderNum = string.Empty;
            string hotelID = string.Empty;
            ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
            //LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求) ,请求数据 :{0} ,  Time:{1} ", "type:" + type + ";data:" + reqData, DateTime.Now.ToString()), "SunSeries接口(平台请求)");
            switch (type)
            {
                //获取token
                case "GetAuthenticateTo":
                    SunSeriesBLL.Instance.GetAuthenticateTo();
                    break;
                //获取报价
                case "GetRoomTypePrice":
                    string strRateplanId = string.Empty;
                    if (reqData == "")
                    {
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    else
                    {
                        PriceRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            strRateplanId = request.rateplanId;
                            result = SunSeriesBLL.Instance.GetSunSeriesRoomTypeOutPrice(request.hotelId, request.arrivalDate, request.departureDate, request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                        }
                    }
                    if (strRateplanId != null && strRateplanId != "")
                    {
                        LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求)获取报价 ,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + reqData, result, DateTime.Now.ToString()), "SunSeries接口(平台请求)");
                    }
                    context.Response.Write(result);
                    break;
                //创建订单
                case "Create_Order":
                    if (reqData == "")
                    {
                        resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        CreateOrderRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateOrderRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else if (request.inOrderNum=="")
                        {
                            resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败,订单号不能为空" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else
                        {
                            int IsClose = 1;
                            string sql = string.Format("SELECT * FROM SunSeries_Order where InOrderNum = '{0}' and HotelID='{1}'", request.inOrderNum, request.hotelId);
                            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                            if (dt.Rows.Count > 0)
                            {
                                resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败,该订单号已存在" };
                                result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                            }
                            else
                            {
                                string userHostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                                //string BookingReference = CCommon.GuidToLongID();//生成订单id
                                SunSeries_Order booking = new SunSeries_Order();
                                //保存订单到数据库
                                booking.InOrderNum = request.inOrderNum;
                                booking.AgentBookingReference = request.inOrderNum;
                                booking.HotelId = request.hotelId;
                                booking.Request = reqData;
                                booking.RoomCount = request.roomNum;
                                booking.RatePlanId = request.ratePlanId;
                                booking.CheckInDate = request.arrivalDate.AsTargetType<DateTime>(DateTime.MinValue);
                                booking.CheckOutDate = request.departureDate.AsTargetType<DateTime>(DateTime.MinValue);
                                booking.IsClose = 1;
                                booking.SellingPrice = request.TotalMoney;
                                booking.CreatTime = DateTime.Now;
                                booking.UpdateTime = DateTime.Now;
                                booking.CreatIP = userHostAddress;
                                booking.EntityState = e_EntityState.Added;
                                if (dt.Rows.Count == 0)
                                {
                                    con.Save(booking);
                                }
                                else
                                {
                                    string strUpdateSql = string.Format(@"update SunSeries_Order set IsClose=1,UpdateTime=GETDATE()  where InOrderNum='{0}' and HotelId='{1}' ", request.inOrderNum, request.hotelId);
                                    con.Update(strUpdateSql);
                                }
                                result = SunSeriesBLL.Instance.Create_Order(request.hotelId, request.ratePlanId, request.customers, request.inOrderNum, Convert.ToDateTime(request.arrivalDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(request.departureDate).ToString("yyyy-MM-dd"), request.roomNum, request.TotalMoney);
                            }
                        }
                    }
                    //string userHostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求)创建订单,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + reqData, result, DateTime.Now.ToString()), "SunSeries接口(平台请求预订)");
                    context.Response.Write(result);
                    break;
                //取消订单
                case "Cancel_Order":
                    hotelID = context.Request["hotelID"] ?? "";
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "" || hotelID == "" || OrderNum == "null" || hotelID == "null")
                    {
                        resp = new Respone() { code = "99", mes = "取消订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = SunSeriesBLL.Instance.cancel(hotelID, OrderNum);
                    }
                    string hostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求)取消订单 ,请求数据 :{0} , 返回数据：{1}，创建订单ip：{2} ， Time:{3} ", type + ",hotelID:" + hotelID + ",OrderNum:" + OrderNum, result, hostAddress, DateTime.Now.ToString()), "SunSeries接口(平台请求预订)");
                    context.Response.Write(result);
                    break;
                //获取订单明细
                case "Get_OrderDetail":
                    hotelID = context.Request["hotelID"] ?? "";
                    OrderNum = context.Request["OrderNum"] ?? "";
                    if (OrderNum == "" || hotelID == "" || OrderNum == "null" || hotelID == "null")
                    {
                        resp = new Respone() { code = "99", mes = "获取订单明细失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        result = SunSeriesBLL.Instance.SunSeriesCheckingOrder(hotelID, OrderNum);
                    }
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求)获取订单明细 ,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + ",hotelID:" + hotelID + ",OrderNum:" + OrderNum, result, DateTime.Now.ToString()), "SunSeries接口(平台请求)");
                    context.Response.Write(result);
                    break;
                //获取报价
                case "GetRoomTypePriceToOriginal":
                    context.Response.ContentType = "text/plain";
                    if (reqData == "")
                    {
                        result = "参数不正确";
                    }
                    else
                    {
                        PriceRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            result = "参数不正确";
                        }
                        else
                        {
                            result = SunSeriesBLL.Instance.GetRoomTypePriceToOriginal(request.hotelId, request.arrivalDate, request.departureDate, request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                        }
                    }
                    //LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求)获取报价 ,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + reqData, result, DateTime.Now.ToString()), "SunSeries接口(平台请求)");
                    context.Response.Write(result);
                    break;
                //获取报价
                case "GetRoomTypePriceTo":
                    string strRateplanIdTo = string.Empty;
                    if (reqData == "")
                    {
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                    }
                    else
                    {
                        PriceRequest request = Newtonsoft.Json.JsonConvert.DeserializeObject<PriceRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (request == null)
                        {
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(rooms);
                        }
                        else
                        {
                            strRateplanIdTo = request.rateplanId;
                            result = SunSeriesBLL.Instance.GetSunSeriesRoomTypeOutPriceTo(request.hotelId, request.arrivalDate, request.departureDate, request.IntOccupancyInfo, request.roomNum, request.rateplanId);
                        }
                    }
                    if (strRateplanIdTo != null && strRateplanIdTo != "")
                    {
                        LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求)获取报价 ,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + reqData, result, DateTime.Now.ToString()), "SunSeries接口(平台请求)");
                    }
                    context.Response.Write(result);
                    break;
                //创建订单
                case "Create_OrderTo":
                    if (reqData == "")
                    {
                        resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                        result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                    }
                    else
                    {
                        CreateOrderToRequest requestTo = Newtonsoft.Json.JsonConvert.DeserializeObject<CreateOrderToRequest>(reqData, new JsonSerializerSettings
                        {
                            Error = delegate(object obj, Newtonsoft.Json.Serialization.ErrorEventArgs args)
                            {
                                args.ErrorContext.Handled = true;
                            }
                        });
                        if (requestTo == null)
                        {
                            resp = new Respone() { code = "99", mes = "创建订单失败,请求参数不正确" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else if (requestTo.inOrderNum == "")
                        {
                            resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败,订单号不能为空" };
                            result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                        }
                        else
                        {
                            int IsClose = 1;
                            string sql = string.Format("SELECT * FROM SunSeries_Order where InOrderNum = '{0}' and HotelID='{1}'", requestTo.inOrderNum, requestTo.hotelId);
                            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
                            if (dt.Rows.Count > 0)
                            {
                                IsClose = dt.Rows[0]["IsClose"].AsTargetType<int>(0);
                            }
                            if (dt.Rows.Count > 0 && IsClose == 1)
                            {
                                resp = new Respone() { code = "99", Status = "预订失败", mes = "预订失败,该订单号已存在" };
                                result = Newtonsoft.Json.JsonConvert.SerializeObject(resp);
                            }
                            else
                            {
                                string userHostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                                //string BookingReference = CCommon.GuidToLongID();//生成订单id
                                SunSeries_Order booking = new SunSeries_Order();
                                //保存订单到数据库
                                booking.InOrderNum = requestTo.inOrderNum;
                                booking.AgentBookingReference = requestTo.inOrderNum;
                                booking.HotelId = requestTo.hotelId;
                                booking.Request = reqData;
                                booking.RoomCount = requestTo.roomNum;
                                booking.RatePlanId = requestTo.ratePlanId;
                                booking.CheckInDate = requestTo.arrivalDate.AsTargetType<DateTime>(DateTime.MinValue);
                                booking.CheckOutDate = requestTo.departureDate.AsTargetType<DateTime>(DateTime.MinValue);
                                booking.IsClose = 1;
                                booking.SellingPrice = requestTo.TotalMoney;
                                booking.CreatTime = DateTime.Now;
                                booking.UpdateTime = DateTime.Now;
                                booking.CreatIP = userHostAddress;
                                booking.EntityState = e_EntityState.Added;
                                if (dt.Rows.Count == 0)
                                {
                                    con.Save(booking);
                                }
                                else
                                {
                                    string strUpdateSql = string.Format(@"update SunSeries_Order set IsClose=1,UpdateTime=GETDATE()  where InOrderNum='{0}' and HotelId='{1}' ", requestTo.inOrderNum, requestTo.hotelId);
                                    con.Update(strUpdateSql);
                                }
                                result = SunSeriesBLL.Instance.Create_OrderTo(requestTo.hotelId, requestTo.ratePlanId, requestTo.bookingInfo, requestTo.inOrderNum, Convert.ToDateTime(requestTo.arrivalDate).ToString("yyyy-MM-dd"), Convert.ToDateTime(requestTo.departureDate).ToString("yyyy-MM-dd"), requestTo.roomNum, requestTo.TotalMoney);
                            }
                        }
                    }
                    //string userHostAddress = System.Web.HttpContext.Current.Request.UserHostAddress;//获取用户ip
                    LogHelper.DoOperateLog(string.Format("Studio：SunSeries(平台请求)创建订单,请求数据 :{0} , 返回数据：{1} ， Time:{2} ", type + reqData, result, DateTime.Now.ToString()), "SunSeries接口(平台请求预订)");
                    context.Response.Write(result);
                    break;
            }
        }

        public class CreateOrderToRequest
        {
            public string inOrderNum { get; set; }//传入订单号
            public string hotelId { get; set; }//酒店id
            public string roomTypeId { get; set; }//房型id
            public string ratePlanId { get; set; }//房型价格计划ID
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public int roomNum { get; set; }//房间数
            public BookingInfo bookingInfo { get; set; }//入住客户信息
            public decimal TotalMoney { get; set; }//订单总金额卖价
        }
        public class PriceRequest
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public class CreateOrderRequest
        {
            public string inOrderNum { get; set; }//传入订单号
            public string hotelId { get; set; }//酒店id
            public string roomTypeId { get; set; }//房型id
            public string ratePlanId { get; set; }//房型价格计划ID
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public int roomNum { get; set; }//房间数
            public List<roomCustomers> customers { get; set; }//入住客户信息
            public decimal TotalMoney { get; set; }//订单总金额卖价
        }
        public class Respone
        {
            public string code { get; set; }//成功00，失败99
            public string orderNum { get; set; }//订单号
            public decimal orderTotal { get; set; }//预订总额
            public string Status { get; set; }
            public string mes { get; set; }//成功返回成功，失败返回失败原因
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}