﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.BLL.SunSeries;
using XiWan.Model.Entities;
using XiWan.DALFactory;
using System.Data;
using System.Collections;
using XiWan.Data;

namespace XiWan.SunSeries
{
    /// <summary>
    /// SunSeriesPort 的摘要说明
    /// </summary>
    public class SunSeriesPort : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "application/json";
            string type = context.Request["type"] ?? "";
            string result = string.Empty;
            switch (type)
            {
                //获取token
                case "GetAuthenticate":
                    SunSeriesBLL.Instance.GetAuthenticate();
                    break;
                //获取国家到数据库
                case "GetSunSeriesCountries":
                    SunSeriesBLL.Instance.GetSunSeriesCountries();
                    break;
                //获取城市到数据库
                case "GetSunSeriesCities":
                    SunSeriesBLL.Instance.GetSunSeriesCities();
                    break;
                //获取国籍到数据库
                case "GetSunSeriesNationalities":
                    SunSeriesBLL.Instance.GetSunSeriesNationalities();
                    break;
                //获取酒店到数据库
                case "GetSunSeriesHotels":
                    SunSeriesBLL.Instance.GetSunSeriesHotels();
                    break;
                //获取国籍
                case "GetSunSeriesNationality":
                    result = SunSeriesBLL.Instance.GetSunSeriesNationality();
                    context.Response.Write(result);
                    break;
                //获取目的地
                case "GetSunSeriesDestination":
                    string strName = context.Request["paramName"].ToString();
                    if (strName != "")
                        result = SunSeriesBLL.Instance.GetSunSeriesDestination(strName);
                    context.Response.Write(result);
                    break;
                //酒店信息
                case "Get_HotelInformation":
                    string _HotelName = context.Request["hotelName"] ?? "";
                    string _ddlGreadeCode = context.Request["ddlGreadeCode"] ?? "0";
                    int input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    int input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    string destination = context.Request["destination"] ?? "";
                    string cityID = string.Empty;
                    string countryCode = string.Empty;
                    if (destination != "")
                    {
                        string[] ArrayDestination = destination.Split(',');
                        cityID = ArrayDestination[0];
                    }
                    string sqlWhere = " 1=1 ";
                    if (cityID != "" || countryCode != "")
                    {
                        if (cityID != "")
                            sqlWhere += string.Format(@" and CityId='{0}'", cityID);
                    }
                    else
                    {
                        sqlWhere += "and 1=0";
                    }
                    if (_HotelName != "")
                    {
                        sqlWhere += string.Format(@" and HotelName like '%{0}%'", _HotelName);
                    }
                    if (_ddlGreadeCode != "0")
                    {
                        sqlWhere += string.Format(@" and stars={0}", _ddlGreadeCode);
                    }
                    int totalCount = 0;
                    result = SunSeriesBLL.Instance.GetHotel("SunSeriesHotel", "*", "HotelName", input_row, input_page, sqlWhere, out totalCount);
                    context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                    break;
                //获取房型报价
                case "GetRoomTypePrice":
                    List<IntOccupancyInfo> occupancys=new List<IntOccupancyInfo>();
                    string hotelID = context.Request["hotelID"] ?? "";
                    string beginTime = context.Request["beginTime"] ?? "";
                    string endTime = context.Request["endTime"] ?? "";
                    int RoomCount = (context.Request["ddlRoom"] ?? "").AsTargetType<int>(0);
                    string RateplanId = context.Request["RateplanId"] ?? "";
                    string rooms = context.Request["rooms"] ?? "";
                    string[] ArrayRooms = rooms.TrimEnd(';').Split(';');
                    for (int i = 0; i < RoomCount; i++)
                    {
                        IntOccupancyInfo occupancy=new IntOccupancyInfo();
                        string Age = string.Empty;
                        string[] ArrayRoom = ArrayRooms[i].Split(',');
                        occupancy.adults=ArrayRoom[0].AsTargetType<int>(0);
                        occupancy.children=ArrayRoom[1].AsTargetType<int>(0);
                        if (ArrayRoom[1] == "1")
                        {
                            occupancy.childAges=ArrayRoom[2];
                        }
                        else if (ArrayRoom[1] == "2")
                        {
                            occupancy.childAges=ArrayRoom[2] + "," + ArrayRoom[3];
                        }
                        occupancys.Add(occupancy);
                    }
                    DataTable dt = SunSeriesBLL.Instance.GetRoomType(hotelID, beginTime, endTime, occupancys, RoomCount, RateplanId);
                    if (dt.Rows.Count == 0)
                    {
                        result = "{\"total\":0,\"rows\":[]}";
                    }
                    input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    DataTable PageDt = Common.Common.GetPagedTable(dt, input_page, input_row);
                    Hashtable hash = new Hashtable();
                    hash.Add("total", dt.Rows.Count);
                    hash.Add("rows", PageDt);
                    result = Newtonsoft.Json.JsonConvert.SerializeObject(hash);
                    context.Response.Write(result);
                    break;
                //检查报价
                case "createQuote":
                    hotelID = context.Request["HotelID"] ?? "";
                    string HotelPriceID = context.Request["HotelPriceID"] ?? "";
                    result = SunSeriesBLL.Instance.createQuote(hotelID, "", HotelPriceID);
                    context.Response.Write(result);
                    break;
                //创建订单
                case "book":
                    List<roomCustomers> roomCustomers = new List<roomCustomers>();

                    roomCustomers roomCustomer = new roomCustomers();
                    List<Customer> customers = new List<Customer>();
                    Customer tppObj1 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18, sex = 1 };
                    //Customer tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                    //Customer tppObj3 = new Customer() { firstName = "Ba", lastName = "Liang", name = "Ba Liang", age = 20, sex = 1 };
                    //Customer tppObj4 = new Customer() { firstName = "ShiYi", lastName = "Xie", name = "ShiYi Xie", age = 20, sex = 1 };

                    //Customer tppObj3 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 10, sex = 1 };
                    //Customer tppObj4 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 12, sex = 1 };
                    customers.Add(tppObj1);
                    //customers.Add(tppObj2);
                    //customers.Add(tppObj3);

                    //customers.Add(tppObj3);
                    //customers.Add(tppObj4);
                    roomCustomer.Customers = customers;
                    roomCustomers.Add(roomCustomer);

                    roomCustomers roomCustomer2 = new roomCustomers();
                    List<Customer> customers2 = new List<Customer>();
                    // tppObj12 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18, sex = 1 };
                    //Customer tppObj22 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                    //Customer tppObj32 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };

                    Customer tppObj32 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };
                    //Customer tppObj42 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 21, sex = 1 };
                    //Customer tppObj52 = new Customer() { firstName = "Jiu", lastName = "Liu", name = "Jiu Liu", age = 20, sex = 1 };
                    //Customer tppObj62 = new Customer() { firstName = "Shi", lastName = "Chen", name = "Shi Chen", age = 20, sex = 1 };
                    //.Add(tppObj12);
                    //customers.Add(tppObj22);
                    //customers.Add(tppObj32);

                    customers2.Add(tppObj32);
                    //customers2.Add(tppObj42);
                    //customers2.Add(tppObj52);
                    //customers2.Add(tppObj62);
                    roomCustomer2.Customers = customers2;
                    roomCustomers.Add(roomCustomer2);

                    //3
                    roomCustomers roomCustomer3 = new roomCustomers();
                    List<Customer> customers3 = new List<Customer>();
                    
                    Customer tppObj43 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 31, sex = 1 };
                    
                    customers3.Add(tppObj43);
                    roomCustomer3.Customers = customers3;
                    roomCustomers.Add(roomCustomer3);

                    //4
                    roomCustomers roomCustomer4 = new roomCustomers();
                    List<Customer> customers4 = new List<Customer>();

                    Customer tppObj74 = new Customer() { firstName = "Si", lastName = "Zhang", name = "Si Zhang", age = 41, sex = 1 };
                    
                    customers4.Add(tppObj74);
                    roomCustomer4.Customers = customers4;
                    roomCustomers.Add(roomCustomer4);

                    //5
                    roomCustomers roomCustomer5 = new roomCustomers();
                    List<Customer> customers5 = new List<Customer>();

                    Customer tppObj75 = new Customer() { firstName = "Wu", lastName = "Zhang", name = "Wu Zhang", age = 51, sex = 1 };

                    customers5.Add(tppObj75);
                    roomCustomer5.Customers = customers5;
                    roomCustomers.Add(roomCustomer5);

                    //6
                    roomCustomers roomCustomer6 = new roomCustomers();
                    List<Customer> customers6 = new List<Customer>();

                    Customer tppObj76 = new Customer() { firstName = "Liu", lastName = "Zhang", name = "Liu Zhang", age = 61, sex = 1 };

                    customers6.Add(tppObj76);
                    roomCustomer6.Customers = customers6;
                    //roomCustomers.Add(roomCustomer6);

                    //7
                    roomCustomers roomCustomer7 = new roomCustomers();
                    List<Customer> customers7 = new List<Customer>();

                    Customer tppObj77 = new Customer() { firstName = "Qi", lastName = "Zhang", name = "Qi Zhang", age = 71, sex = 1 };

                    customers7.Add(tppObj77);
                    roomCustomer7.Customers = customers7;
                    //roomCustomers.Add(roomCustomer7);

                    //8
                    roomCustomers roomCustomer8 = new roomCustomers();
                    List<Customer> customers8 = new List<Customer>();

                    Customer tppObj78 = new Customer() { firstName = "Ba", lastName = "Zhang", name = "Ba Zhang", age = 81, sex = 1 };

                    customers8.Add(tppObj78);
                    roomCustomer8.Customers = customers8;
                    //roomCustomers.Add(roomCustomer8);

                    //9
                    roomCustomers roomCustomer9 = new roomCustomers();
                    List<Customer> customers9 = new List<Customer>();

                    Customer tppObj79 = new Customer() { firstName = "Jiu", lastName = "Zhang", name = "Jiu Zhang", age = 91, sex = 1 };

                    customers9.Add(tppObj79);
                    roomCustomer9.Customers = customers9;
                    //roomCustomers.Add(roomCustomer9);

                    //10
                    roomCustomers roomCustomer10 = new roomCustomers();
                    List<Customer> customers10 = new List<Customer>();

                    Customer tppObj710 = new Customer() { firstName = "Shi", lastName = "Zhang", name = "Shi Zhang", age = 101, sex = 1 };

                    customers10.Add(tppObj710);
                    roomCustomer10.Customers = customers10;
                    //roomCustomers.Add(roomCustomer10);


                    // tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };

                    hotelID = context.Request["HotelID"] ?? "";
                    HotelPriceID = context.Request["HotelPriceID"] ?? "";
                    RateplanId = context.Request["RateplanId"] ?? "";
                    beginTime = context.Request["beginTime"] ?? "";
                    endTime = context.Request["endTime"] ?? "";
                    result = SunSeriesBLL.Instance.book(hotelID, RateplanId, roomCustomers, "", beginTime, endTime, 0, HotelPriceID);
                    context.Response.Write(result);
                    break;
                //查看订单（本地数据库）
                case "SunSeriesBooking":
                    context.Response.ContentType = "application/json";
                    totalCount = 0;
                    sqlWhere = " 1=1 ";
                    string _BookingCode = context.Request["BookingNumber"] ?? "";
                    string InDate = context.Request["InDate"] ?? "";
                    string InNum = context.Request["InNum"] ?? "";
                    string Status = context.Request["Status"] ?? "";
                    if (_BookingCode != "")
                    {
                        sqlWhere = sqlWhere + string.Format("AND BookingCode='{0}' ", _BookingCode);
                    }
                    if (InNum != "")
                    {
                        sqlWhere = sqlWhere + string.Format("AND InOrderNum='{0}' ", InNum);
                    }
                    if (InDate != "")
                    {
                        sqlWhere = sqlWhere + string.Format("AND CONVERT(varchar(100), CreatTime, 23)='{0}' ", InDate);
                    }
                    if (Status != "0")
                    {
                        if (Status == "1")
                        {
                            sqlWhere = sqlWhere + "AND Status <> 0 ";
                        }
                        else if (Status == "2")
                        {
                            sqlWhere = sqlWhere + "AND Status=0 ";
                        }
                    }
                    input_page = Convert.ToInt16(context.Request["page"] ?? "0");
                    input_row = Convert.ToInt16(context.Request["rows"] ?? "0");
                    result = SunSeriesBLL.Instance.GetBookingBySql("SunSeries_Order ", " * ", " Id ", input_row, input_page, sqlWhere, out totalCount);
                    context.Response.Write("{\"total\": " + totalCount.ToString() + ",\"rows\":" + result + "}");
                    break;
                //取消订单
                case "SunSeriesCancellation":
                    context.Response.ContentType = "application/json";
                    string BookingNumber = context.Request["BookingCode"] ?? "";
                    hotelID = context.Request["HotelID"] ?? "";
                    //result = SunSeriesBLL.Instance.cancel(hotelID, BookingNumber);
                    context.Response.Write(result);
                    break;
                //查看订单
                case "SunSeriesCheckingOrder":
                    context.Response.ContentType = "application/json";
                    hotelID = context.Request["HotelID"] ?? "";
                    BookingNumber = context.Request["BookingCode"] ?? "";
                    result = SunSeriesBLL.Instance.SunSeriesCheckingOrder(hotelID, BookingNumber);
                    context.Response.Write(result);
                    break;
                //查询报价
                case "GetSunSeriesRoomTypePrice":
                    IntOccupancyInfo occupancy1 = new IntOccupancyInfo();
                    hotelID = context.Request["hotelID"] ?? "";
                    beginTime = context.Request["beginTime"] ?? "";
                    endTime = context.Request["endTime"] ?? "";
                    RoomCount = (context.Request["ddlRoom"] ?? "").AsTargetType<int>(0);
                    RateplanId = context.Request["RateplanId"] ?? "";
                    rooms = context.Request["rooms"] ?? "";
                    ArrayRooms = rooms.TrimEnd(';').Split(';');
                    string[] ArrayRoom1 = ArrayRooms[0].Split(',');
                    occupancy1.adults = ArrayRoom1[0].AsTargetType<int>(0);
                    occupancy1.children = ArrayRoom1[1].AsTargetType<int>(0);
                    if (ArrayRoom1[1] == "1")
                    {
                        occupancy1.childAges = ArrayRoom1[2];
                    }
                    else if (ArrayRoom1[1] == "2")
                    {
                        occupancy1.childAges = ArrayRoom1[2] + "," + ArrayRoom1[3];
                    }
                    result = SunSeriesBLL.Instance.GetSunSeriesRoomTypeOutPrice(hotelID, beginTime, endTime, occupancy1, RoomCount, "d37bedd7-9aef-4fff-8af7-bc516d2ef9fa");
                    context.Response.Write(result);
                    break;
                //创建订单
                case "Create_Order":
                    hotelID = context.Request["hotelID"] ?? "";
                    RateplanId = context.Request["RateplanId"] ?? "";
                    List<roomCustomers> roomCustomers1 = new List<roomCustomers>();
                    roomCustomers roomCustomer1 = new roomCustomers();
                    List<Customer> customers1 = new List<Customer>();
                    //Customer tppObj1 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18 ,sex=1};
                    // tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                    //Customer tppObj3 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };
                    //Customer tppObj4 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 21, sex = 1 };
                    //customers1.Add(tppObj1);
                    //customers1.Add(tppObj2);
                    //roomCustomer1.Customers = customers1;

                    // roomCustomer2 = new roomCustomers();
                    //List<Customer> customers2 = new List<Customer>();
                    //Customer tppObj1 = new Customer() { firstName = "San", lastName = "Zhang", name = "San Zhang", age = 18 ,sex=1};
                    //Customer tppObj2 = new Customer() { firstName = "Si", lastName = "Li", name = "Si Li", age = 19, sex = 1 };
                    //Customer tppObj3 = new Customer() { firstName = "Wu", lastName = "Wang", name = "Wu Wang", age = 20, sex = 1 };
                    //Customer tppObj4 = new Customer() { firstName = "Liu", lastName = "Huang", name = "Liu Huang", age = 21, sex = 1 };
                    //customers1.Add(tppObj1);
                    //customers2.Add(tppObj2);
                    //roomCustomer2.Customers = customers2;

                    //roomCustomers1.Add(roomCustomer1);
                    //roomCustomers1.Add(roomCustomer2);
                    //result = SunSeriesBLL.Instance.Create_Order(hotelID, RateplanId, roomCustomers1, "", "2018-11-21", "2018-11-22", 2,0);
                    
                    context.Response.Write(result);
                    break;
                //Create_Order(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum);
            }
        }

        public class PriceRequest
        {
            public string hotelId { get; set; }//酒店id
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public IntOccupancyInfo IntOccupancyInfo { get; set; }//房型
            public string rateplanId { get; set; }//房型价格计划ID
            public int roomNum { get; set; }//房间数
        }
        public class CreateOrderRequest
        {
            public string inOrderNum { get; set; }//传入订单号
            public string hotelId { get; set; }//酒店id
            public string roomTypeId { get; set; }//房型id
            public string ratePlanId { get; set; }//房型价格计划ID
            public string arrivalDate { get; set; }//入住时间
            public string departureDate { get; set; }//退房时间
            public int roomNum { get; set; }//房间数
            public List<roomCustomers> customers { get; set; }//入住客户信息
        }
        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}