﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;

namespace XiWan.UI.GridStyle
{
    [Serializable]
    public class ButtonStyleCollection : List<ButtonStyle>
    {
        /// <summary>
        /// 添加Button
        /// </summary>
        /// <param name="commandName">对应的commandName</param>
        /// <param name="text">显示的文本</param>
        public void Add(string commandName, string text)
        {
            ButtonStyle btn = new ButtonStyle();
            btn.CommandName = commandName;
            btn.Text = text;
            this.Add(btn);
        }
        /// <summary>
        /// 添加Button
        /// </summary>
        /// <param name="commandName">对应的commandName</param>
        /// <param name="text">显示的文本</param>
        /// <param name="eventType">客服段事件</param>
        /// <param name="clientMethod">客服段方法</param>
        public void Add(string commandName, string text, e_EventType eventType, string clientMethod)
        {
            ButtonStyle btn = new ButtonStyle();
            btn.CommandName = commandName;
            btn.Text = text;
            btn.EventType = eventType;
            btn.ClientMethod = clientMethod;
            this.Add(btn);
        }
        /// <summary>
        /// 添加Button
        /// </summary>
        /// <param name="commandName">对应的commandName</param>
        /// <param name="text">显示的文本</param>
        /// <param name="eventType">客服段事件</param>
        /// <param name="clientMethod">客服段方法</param>
        /// <param name="isPostBack">是否回传到服务器</param>
        public void Add(string commandName, string text, e_EventType eventType, string clientMethod, bool isPostBack)
        {
            ButtonStyle btn = new ButtonStyle();
            btn.CommandName = commandName;
            btn.Text = text;
            btn.EventType = eventType;
            btn.ClientMethod = clientMethod;
            btn.IsPostBack = isPostBack;
            this.Add(btn);
        }
    }
}
