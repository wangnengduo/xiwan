﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace XiWan.UI.GridStyle
{
    [Serializable]
    public class ColumnStyle
    {
        public delegate void ColumnStyleControl(ref Control con);
        public ColumnStyleControl ColumnControl = null;
        public ColumnStyle()
        {
        }
        public ColumnStyle(string columnsName, string columnsDesc)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
        }
        public ColumnStyle(string columnsName, string columnsDesc, string columnsWidth)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._columnsWidth = columnsWidth;
        }
        public ColumnStyle(string columnsName, string columnsDesc, bool columnsTotal)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._columnsTotal = columnsTotal;
        }
        public ColumnStyle(string columnsName, string columnsDesc, string columnsWidth, bool columnsTotal)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._columnsWidth = columnsWidth;
            this._columnsTotal = columnsTotal;
        }
        public ColumnStyle(string columnsName, string columnsDesc, string columnsWidth, string columnsType)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._columnsWidth = columnsWidth;
            this._columnsType = columnsType;
        }
        public ColumnStyle(string columnsName, string columnsDesc, string columnsWidth, string columnsType, bool columnsTotal)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._columnsWidth = columnsWidth;
            this._columnsType = columnsType;
            this._columnsTotal = columnsTotal;
        }
        public ColumnStyle(string columnsName, string columnsDesc, e_GridShowType showType)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._showType = showType;
        }
        public ColumnStyle(string columnsName, string columnsDesc, string controlWidth, e_GridShowType showType)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._controlWidth = controlWidth;
            this._showType = showType;
        }

        public ColumnStyle(string columnsName, string columnsDesc, string controlWidth, e_GridShowType showType, bool readOnly)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._controlWidth = controlWidth;
            this._showType = showType;
            this._readOnly = readOnly;
        }

        public ColumnStyle(string columnsName, string columnsDesc, string controlWidth, string columnsType, e_GridShowType showType)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._columnsType = columnsType;
            this._showType = showType;
            this._controlWidth = controlWidth;
        }

        public ColumnStyle(string columnsName, string columnsDesc, string controlWidth, string columnsType, e_GridShowType showType, bool isNotNull)
        {
            this._columnsName = columnsName;
            this._columnsDesc = columnsDesc;
            this._columnsType = columnsType;
            this._showType = showType;
            this._controlWidth = controlWidth;
            this._isNotNull = isNotNull;
        }

        public const string DataKey = "[@Key@]";

        private string _columnsName = null;
        /// <summary>
        /// 列名。一般对应字段名。
        /// </summary>
        public string ColumnsName
        {
            get { return _columnsName; }
            set { _columnsName = value; }
        }

        private string _columnsDesc = null;
        /// <summary>
        /// 列的中文描述。
        /// </summary>
        public string ColumnsDesc
        {
            get { return _columnsDesc; }
            set { _columnsDesc = value; }
        }

        private string _columnsType = "C";
        /// <summary>
        /// 列的类型。如果没有设置ColumnsFormat，根据此类型自动设置ColumnsFormat。可选类型如下：C字符型 N数值型 %百分比 B布尔型 D日期型 T日期时间型（格式化到分钟）。
        /// </summary>
        public string ColumnsType
        {
            get { return _columnsType; }
            set { _columnsType = value; }
        }

        private int _columnsTypeRemark = 2;
        /// <summary>
        /// 小数位。当ColumnsType被设置为N数值型或者%百分比时，可以通过此属性设置小数位。
        /// </summary>
        public int ColumnsTypeRemark
        {
            get { return _columnsTypeRemark; }
            set { _columnsTypeRemark = value; }
        }

        private string _columnsWidth = "10%";
        /// <summary>
        /// 列的宽度。按百分比，默认10%。
        /// </summary>
        public string ColumnsWidth
        {
            get { return _columnsWidth; }
            set { _columnsWidth = value; }
        }
        /// <summary>
        /// 设置该列是否有合计
        /// </summary>
        private bool _columnsTotal = false;
        public bool ColumnsTotal
        {
            get { return _columnsTotal; }
            set { _columnsTotal = value; }
        }

        private bool _readOnly = false;
        /// <summary>
        /// 是否为只读 只有ShowType!=e_GridShowType.Literal时有效
        /// </summary>
        public bool ReadOnly
        {
            get { return _readOnly; }
            set { _readOnly = value; }
        }

        private e_GridShowType _showType = e_GridShowType.Literal;
        /// <summary>
        /// 显示的样式
        /// </summary>
        public e_GridShowType ShowType
        {
            get { return _showType; }
            set { _showType = value; }
        }
        private string _controlWidth = null;

        /// <summary>
        /// 控件的宽度
        /// </summary>
        public string ControlWidth
        {
            get { return _controlWidth; }
            set { _controlWidth = value; }
        }
        private Dictionary<e_EventType, string> _eventClient = null;
        /// <summary>
        /// 客服端 事件和方法 key事件 value方法
        /// </summary>
        public Dictionary<e_EventType, string> EventClient
        {
            get { return _eventClient; }
            set { _eventClient = value; }
        }
        /// <summary>
        /// 客服端 事件和方法
        /// </summary>
        /// <param name="e">事件</param>
        /// <param name="value">方法</param>
        public void SetEventClient(e_EventType e, string value)
        {
            if (_eventClient == null)
                _eventClient = new Dictionary<e_EventType, string>();
            _eventClient.Add(e, value);
        }
        private e_TextAlign headAlign = e_TextAlign.Center;

        /// <summary>
        /// 网格标题对齐方式
        /// </summary>
        public e_TextAlign HeadAlign
        {
            get { return headAlign; }
            set { headAlign = value; }
        }
        private e_TextAlign itemAlign = e_TextAlign.Left;

        /// <summary>
        /// 网格内容对齐方式
        /// </summary>
        public e_TextAlign ItemAlign
        {
            get { return itemAlign; }
            set { itemAlign = value; }
        }

        private System.Collections.IEnumerable _dataSource = null;

        /// <summary>
        /// 当 e_GridShowType.DropDownList时 DropDownList 的数据源
        /// </summary>
        public System.Collections.IEnumerable DataSource
        {
            get { return _dataSource; }
            set { _dataSource = value; }
        }

        private string _dataTextField = null;
        /// <summary>
        /// 当 e_GridShowType.DropDownList时 DropDownList的Test值
        /// </summary>
        public string DataTextField
        {
            get { return _dataTextField; }
            set { _dataTextField = value; }
        }
        private string _dataValueField = null;
        /// <summary>
        /// 当 e_GridShowType.DropDownList时 DropDownList的Value值
        /// </summary>
        public string DataValueField
        {
            get { return _dataValueField; }
            set { _dataValueField = value; }
        }
        private bool _isNotNull = false;
        /// <summary>
        /// 当 不为e_GridShowType.Literal时有效 是否必填
        /// </summary>
        public bool IsNotNull
        {
            get { return _isNotNull; }
            set { _isNotNull = value; }
        }
        /// <summary>
        /// 当 e_GridShowType.RadioButton时有效 比如 RadioTexts[0]男 RadioTexts[1]女
        /// </summary>
        private string[] radioTexts = null;

        public string[] RadioTexts
        {
            get { return radioTexts; }
            set { radioTexts = value; }
        }
        private ButtonStyleCollection _buttonCollection = null;

        public ButtonStyleCollection ButtonCollection
        {
            get { return _buttonCollection; }
            set
            {
                itemAlign = e_TextAlign.Center;
                _buttonCollection = value;
            }
        }

        private bool _textBoxSelectReadOnly = false;
        /// <summary>
        /// 只有e_GridShowType.TextBoxSelect时有效  是否将e_GridShowType.TextBoxSelect类型的文本框设置为只读
        /// </summary>
        public bool TextBoxSelectReadOnly
        {
            get { return _textBoxSelectReadOnly; }
            set { _textBoxSelectReadOnly = value; }
        }
        private bool isCheckAll = false;

        /// <summary>
        /// 是否是全选列
        /// </summary>
        public bool IsCheckAll
        {
            get { return isCheckAll; }
            set
            {
                isCheckAll = value;
            }
        }
        private bool isRowNumberer = false;

        public bool IsRowNumberer
        {
            get { return isRowNumberer; }
            set
            {
                isRowNumberer = value;
            }
        }
        private string _href = null;

        public string Href
        {
            get { return _href; }
            set { _href = value; }
        }
        private string _dataFields = null;
        /// <summary>
        /// 多个字段用,号隔开
        /// </summary>
        public string DataFields
        {
            get { return _dataFields; }
            set { _dataFields = value; }
        }
        private bool _sorting = false;

        /// <summary>
        /// 是否支持排序
        /// </summary>
        public bool Sorting
        {
            get { return _sorting; }
            set { _sorting = value; }
        }
    }
}
