﻿/*
 * 列表统计接口
 * 温兵 2010-11-19 morning 
 * */

namespace XiWan.UI.GridStyle
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Data;

   public  interface IQueryStatistic
    {
        /// <summary>
        /// 获取统计信息(如合计)
        /// 温兵 2010-11-19 Morning
        /// </summary>
        /// <param name="queryCondition">查询的条件</param>
        /// <returns></returns>
        DataTable GetStatistics(object queryCondition);
    }
}
