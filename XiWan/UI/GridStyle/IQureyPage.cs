﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XiWan.UI.GridStyle
{

    /// <summary>
    /// 分页查询接口
    /// </summary>
    public interface IQureyPage
    {

        /// <summary>
        /// 获取分页数据
        /// <returns></returns>
        /// </summary>
        /// <param name="queryCondition">对象/页面查询条件</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="pageSize">页大小</param>
        /// <param name="pageCount">页数</param>
        /// <param name="recordCount">记录数</param>
        /// <returns></returns>
        DataTable GetPage(object queryCondition, int pageIndex, int pageSize, out int pageCount, out int recordCount);
    }
}
