﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XiWan.UI.GridStyle
{
    [Serializable]
    public class ButtonStyle
    {
        /// <summary>
        /// 命令名称
        /// </summary>
        public string CommandName;
        /// <summary>
        /// 显示文件
        /// </summary>
        public string Text;
        /// <summary>
        /// 客服端事件
        /// </summary>
        public e_EventType EventType;
        /// <summary>
        /// 客服端方法
        /// </summary>
        public string ClientMethod;
        /// <summary>
        /// 是否回传到服务器
        /// </summary>
        public bool IsPostBack = true;
        /// <summary>
        /// 按钮的样式 考虑到不好设置默认值所以该属性只对e_GridShowType.Button有效
        /// </summary>
        public string CssClass = "button1";
    }
}
