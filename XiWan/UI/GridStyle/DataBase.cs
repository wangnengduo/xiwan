﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using XiWan.DALFactory;

namespace XiWan.UI.GridStyle
{
    public class DataBase : IDisposable
    {
        string strConnectionString = CCommon.GetWebConfigValue("strConnectionString");
        #region 执行语句返回结果集

        #region 分页执行
        internal DataTable GetPage(string searchSql, int pageIndex, int pageSize, out int pageCount, out int rowCount)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            System.Data.SqlClient.SqlCommand cmd = null;
            System.Data.SqlClient.SqlDataAdapter sd = null;
            pageCount = 0;
            rowCount = 0;

            try
            {
                SqlParameter prmSql = new SqlParameter("@Sql", SqlDbType.NVarChar);
                SqlParameter prmPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
                SqlParameter prmPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
                SqlParameter prmPageCount = new SqlParameter("@PageCount", SqlDbType.Int);
                SqlParameter prmRowCount = new SqlParameter("@RowCount", SqlDbType.Int);
                prmSql.Value = searchSql;

                if (pageIndex < 1) pageIndex = 1;

                prmPageIndex.Value = pageIndex;
                prmPageSize.Value = pageSize;
                prmPageCount.Value = 0;
                prmPageCount.Direction = ParameterDirection.InputOutput;
                prmRowCount.Value = 0;
                prmRowCount.Direction = ParameterDirection.InputOutput;
                pageCount = (int)prmPageCount.Value;
                rowCount = (int)prmRowCount.Value;

                using (SqlConnection sc = new SqlConnection(strConnectionString))
                {
                    cmd = sc.CreateCommand();
                    cmd.CommandText = "procSelectPage";
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add(prmSql);
                    cmd.Parameters.Add(prmPageIndex);
                    cmd.Parameters.Add(prmPageSize);
                    cmd.Parameters.Add(prmPageCount);
                    cmd.Parameters.Add(prmRowCount);

                    sd = new System.Data.SqlClient.SqlDataAdapter(cmd);
                    sd.Fill(ds);

                    pageCount = (int)prmPageCount.Value;
                    rowCount = (int)prmRowCount.Value;

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        DataColumn dclRowStat = ds.Tables[1].Columns[ds.Tables[1].Columns.Count - 1];
                        if (dclRowStat.ColumnName.ToUpper() == "ROWSTAT")
                        {
                            ds.Tables[1].Columns.Remove(dclRowStat);
                            ds.Tables[1].AcceptChanges();
                        }
                        dt = ds.Tables[1];
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //throw new Exception(ex.Message + Core.Base.DataType.StringHelper.Crlf + Core.Base.DataType.StringHelper.Crlf + "查询语句：" + Core.Base.DataType.StringHelper.Crlf + searchSql);
                //Core.Base.Err.WriteLog("/" + ex.GetBaseException() + "/" + DateTime.Now + "/", Core.Base.DataType.StringHelper.Crlf + Core.Base.DataType.StringHelper.Crlf + "查询语句：" + Core.Base.DataType.StringHelper.Crlf + searchSql);
            }

            return dt;
        }
        #endregion

        #region 分页执行
        /// <summary>
        /// 根据语句生成分页 ，注意主键所在的表若需换名，则必须以pTb 命名。例如 pTb.TableKeyName ,若TableKeyName不需加上表前缀，则无此要求。
        /// </summary>
        /// <param name="SearchInfo"></param>
        /// <returns></returns>
        internal DataTable GetPage(PageSearchInfo SearchInfo)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            System.Data.SqlClient.SqlCommand cmd = null;
            System.Data.SqlClient.SqlDataAdapter sd = null;
            
            SearchInfo.PageCount = 0;
            SearchInfo.RecordCount = 0;

            string sort = SearchInfo.Sort;
            int pageIndex = SearchInfo.PageIndex;
            int pageSize = SearchInfo.PageSize;
            string sql = SearchInfo.SearchSql;
            string sPageCount = SearchInfo.OldPageCount;

            //使用检索key的方式减少全表扫描情况  //效率最好
            if (SearchInfo.TableFilterKey != "" && sort != "")
            {
                #region 使用检索key的方式减少全表扫描情况
                StringBuilder strSql = new StringBuilder();
                int locationCount = pageIndex * pageSize;
                int iEnd = sql.IndexOf("EndSelect") + 9;
                sort = sort.ToLower().Trim();
                //排序需要加上主键字段
                /*
                if (sort == "asc" || sort == "desc")
                {
                    sort = " order by " + SearchInfo.TableFilterKey + " " + sort;
                }
                else
                {
                    sort = " order by " + SearchInfo.TableFilterKey + " desc";
                }
                */
                if (string.IsNullOrEmpty(SearchInfo.SortCulumns))
                {
                    SearchInfo.SortCulumns = SearchInfo.TableFilterKey;
                }
                if (string.IsNullOrEmpty(SearchInfo.OrderByCulumns))
                {
                    SearchInfo.OrderByCulumns = SearchInfo.TableFilterKey + " desc";
                }
                sort = " order by " + SearchInfo.OrderByCulumns;

                string strTempFromWhere = " " + sql.Substring(iEnd, sql.Length - iEnd);
                //记录数检查
                if (pageIndex < 2 || sPageCount == "")
                    strSql.Append("select count(1) from (select " + SearchInfo.TableFilterKey + strTempFromWhere + ")a ");
                else
                    strSql.Append("select " + sPageCount + " as PageCount ");
                //分页
                #region not in 分页方式 不是很理想
                //string strTempSql = " select top " + (pageSize * (pageIndex - 1)).ToString() + " " + SearchInfo.TableFilterKey + " " + strTempFromWhere + sort;
                //iEnd = sql.ToLower().IndexOf("select")+6;
                //strSql.Append("select top " + pageSize + " " + sql.Substring(iEnd, sql.Length - iEnd)  + " and " + SearchInfo.TableFilterKey + " not in(" + strTempSql + ")  " + sort);
                #endregion

                //查询主键 使用in的方式分页
                iEnd = sql.ToLower().IndexOf("select") + 6;
                string PreTb = SearchInfo.TableFilterKey;
                if (PreTb.Contains("."))
                {
                    PreTb = PreTb.Substring(0, PreTb.IndexOf('.')) + ".";
                }
                else
                {
                    PreTb = "pTb.";
                }


                string strTempSql = "select top " + pageSize + " " + SearchInfo.TableFilterKey.Replace(PreTb, "") + " from(select top " + locationCount + " row_number() over(" + sort.Replace(PreTb, "") + ") #@$,*from (";
                strTempSql += "select " + SearchInfo.SortCulumns + strTempFromWhere + "";
                strTempSql += ")a)a where #@$>" + (locationCount - pageSize);

                strSql.Append("select top " + pageSize + " " + sql.Substring(iEnd, sql.Length - iEnd) + " and " + SearchInfo.TableFilterKey + " in(" + strTempSql + ")  " + sort);

                DataSet dst = GetDataSet(strSql.ToString());
                 
                int rowCount = Convert.ToInt32(dst.Tables[0].Rows[0][0]);
                SearchInfo.RecordCount = rowCount;

                if (rowCount == 0)
                {
                    SearchInfo.PageCount = 0;
                }
                else
                {
                    if (pageIndex > 1 && !string.IsNullOrEmpty(sPageCount))
                    {
                        SearchInfo.PageCount = int.Parse(sPageCount);
                    }
                    else
                    {

                        SearchInfo.PageCount = rowCount / pageSize;
                        if (rowCount % pageSize > 0)
                            SearchInfo.PageCount++;                         
                    }

                }
                return dst.Tables[1];
                #endregion
            }
           
            return dt;
        }
        #endregion

        #region 执行语句返回datatable结果集
        internal DataSet GetDataSet(string searchSql)
        {
            DataSet ds = new DataSet(); 
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlConnection conn = null;

            //输出执行语句 以便测试            
            SqlHelper.Instance.WriteLog(searchSql);

            try
            {
                conn = new System.Data.SqlClient.SqlConnection(strConnectionString);
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = searchSql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 900;//60*15
                System.Data.SqlClient.SqlDataAdapter sda = new System.Data.SqlClient.SqlDataAdapter(cmd);
                sda.Fill(ds);                 
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //this.Message = ex.Message;
                //throw new Exception(searchSql, ex);
                //Core.Base.Err.WriteLog("/" + ex.GetBaseException() + "/" + DateTime.Now + "/", Core.Base.DataType.StringHelper.Crlf + Core.Base.DataType.StringHelper.Crlf + "查询语句：" + Core.Base.DataType.StringHelper.Crlf + searchSql);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return ds;
        }
        #endregion 


        #region 执行语句返回datatable结果集
        internal DataTable GetDataTable(string searchSql)
        {
            DataSet ds = GetDataSet(searchSql);
            DataTable dt = new DataTable();

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }
        #endregion 

        internal void ExecuteNonQuery(string strSQL)
        {
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlConnection conn = null;
            try
            {
                conn = new System.Data.SqlClient.SqlConnection(strConnectionString);
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = strSQL;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;                
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }
        }
        #endregion 


        #region IDisposable 成员

        public void Dispose()
        {
            //throw new NotImplementedException();
        }

        #endregion
    }
}
