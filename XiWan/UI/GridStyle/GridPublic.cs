﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XiWan.UI.GridStyle
{
    [Serializable]
    public struct s_GridHead
    {
        public s_GridHead(string title, int start, int end)
        {
            Title = title;
            StartIndex = start;
            EndIndex = end;
        }
        public int StartIndex;
        public int EndIndex;
        public string Title;
    }
    public enum e_GridShowType
    {
        Literal,
        TextBox,
        DropDownList,
        /// <summary>
        /// 链接即(a标签)
        /// </summary>
        HyperLink,
        CheckBox,
        Button,
        LinkButton,
        RadioButton,
        TextBoxSelect,
        Control,
        /// <summary>
        /// 特殊类型 一般在网格控件里面才使用这种类型
        /// </summary>
        particular,
    }
    public enum e_EventType
    {
        OnBlur,
        OnChange,
        OnFocus,
        OnPropertyChange,
        OnClick,
        OnKeyUp,
    }
    public enum e_TextAlign
    {
        /// <summary>
        /// 左对齐
        /// </summary>
        Left,
        /// <summary>
        /// 居中
        /// </summary>
        Center,
        /// <summary>
        /// 右对齐
        /// </summary>
        Right,
    }
    public enum e_CommandName
    {
        Delete,
        Edit,
        View,
    }
}
