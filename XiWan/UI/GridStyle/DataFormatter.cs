using System.Diagnostics;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Collections;

namespace XiWan.UI.GridStyle
{
    public class DataFormatter
	{		
        #region 构造函数
        private static DataFormatter ms_instance;
        private static object ms_sync = new Object();

        private DataFormatter()
        {
        }

        public static DataFormatter Instance
        {
            get
            {
                if (ms_instance == null)
                {
                    lock (ms_sync)
                    {
                        if (ms_instance == null) ms_instance = new DataFormatter();
                    }
                }
                return ms_instance;
            }
        }
        #endregion

		#region 变量
		// 日期
        public const string c_Date = "yyyy-MM-dd";
        public const string c_DateMinute = "yyyy-MM-dd HH:mm";
        public const string c_DateSecond = "yyyy-MM-dd HH:mm:ss";

        public const string c_TimeB = " 00:00:00.000";
        public const string c_TimeE = " 23:59:59.998";

        // 数值
        public const string c_Decimal = "f2";
		#endregion
		
        public string GetBeginDate(DateTime value)
        {
            return value.ToString(c_Date) + c_TimeB;
        }

        public string GetEndDate(DateTime value)
        {
            return value.ToString(c_Date) + c_TimeE;
        }

        public string GetBeginDate()
        {
            return Convert.ToDateTime(System.DateTime.Today.Year.ToString() + "-" + System.DateTime.Today.Month.ToString() + "-1").ToString(c_Date);
        }

        public string GetEndDate()
        {
            return Convert.ToDateTime(System.DateTime.Today.Year.ToString() + "-" + System.DateTime.Today.Month.ToString() + "-1").AddMonths(1).AddDays(-1).ToString(c_Date);
        }
	}	
}
