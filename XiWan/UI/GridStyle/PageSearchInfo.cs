﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XiWan.UI.GridStyle
{
    public class PageSearchInfo
    {
        /// <summary>
        /// 返回参数1
        /// </summary>
        public int PageIndex { get; set; }
        public int PageSize { get; set; }
        public int PageCount { get; set; }
        public int RecordCount { get; set; }

        public string SearchSql { get; set; }

        /// <summary>
        /// 表主键
        /// </summary>
        public string TableFilterKey { get; set; }

        /// <summary>
        /// 排序方式 desc 或 asc
        /// </summary>
        public string Sort { get; set; }

        /// <summary>
        /// 排序列
        /// </summary>
        public string SortCulumns { get; set; }

        /// <summary>
        /// 排序列
        /// </summary>
        public string OrderByCulumns { get; set; }

        public string OldPageCount { get; set; }

    }
}
