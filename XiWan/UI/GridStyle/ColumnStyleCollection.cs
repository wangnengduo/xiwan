﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XiWan.UI.GridStyle
{
    [Serializable]
    public class ColumnStyleCollection : List<ColumnStyle>
    {
        private ColumnStyle _last = null;

        public ColumnStyle Last
        {
            get { return _last; }
        }
        /// <summary>
        /// 添加列
        /// </summary>
        /// <param name="columnsName">内容绑定字段</param>
        /// <param name="columnsDesc">表头文本</param>
        /// <returns></returns>
        public void Add(string columnsName, string columnsDesc)
        {
            _last = new ColumnStyle(columnsName, columnsDesc);
            this.Add(_last);
        }
        /// <summary>
        /// 添加列
        /// </summary>
        /// <param name="columnsName">内容绑定字段</param>
        /// <param name="columnsDesc">表头文本</param>
        /// <param name="columnsWidth">列宽度</param>
        /// <returns></returns>
        public void Add(string columnsName, string columnsDesc, string columnsWidth)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, columnsWidth);
            this.Add(_last);
        }
        /// <summary>
        /// 添加列
        /// </summary>
        /// <param name="columnsName">内容绑定字段</param>
        /// <param name="columnsDesc">表头文本</param>
        /// <param name="columnsTotal">是否显示合计</param>
        /// <returns></returns>
        public void Add(string columnsName, string columnsDesc, bool columnsTotal)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, columnsTotal);
            this.Add(_last);
        }

        public void Add(string columnsName, string columnsDesc, string columnsWidth, bool columnsTotal)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, columnsWidth, columnsTotal);
            this.Add(_last);
        }
        public void Add(string columnsName, string columnsDesc, string columnsWidth, string columnsType)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, columnsWidth, columnsType);
            this.Add(_last);
        }
        public void Add(string columnsName, string columnsDesc, string columnsWidth, string columnsType, bool columnsTotal)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, columnsWidth, columnsType, columnsTotal);
            this.Add(_last);
        }

        public void Add(string columnsName, string columnsDesc, string columnsType, e_GridShowType showType, bool readOnly)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, columnsType, showType, readOnly);
            this.Add(_last);
        }

        public void Add(string columnsName, string columnsDesc, string columnsType, e_GridShowType showType)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, columnsType, showType);
            this.Add(_last);
        }

        public void Add(string columnsName, string columnsDesc, e_GridShowType showType)
        {
            _last = new ColumnStyle(columnsName, columnsDesc, showType);
            this.Add(_last);
        }
        public void Add(ColumnStyle.ColumnStyleControl ColumnControl)
        {
            _last = new ColumnStyle();
            _last.ColumnControl = ColumnControl;
            _last.ShowType = e_GridShowType.Control;
            this.Add(_last);
        }
        //public ColumnStyleCollection BuildColumnStyleCollection(string columnsName, string columnsType, string columnsDesc, string columnsWidth, string columnsFormat, string columnsAction)
        //{
        //    ColumnStyleCollection list = new ColumnStyleCollection();
        //    return list;
        //}
    }
}
