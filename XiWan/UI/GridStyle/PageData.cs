﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XiWan.DALFactory;
using System.Data.Linq;
using System.Data.Linq.SqlClient;
using System.Data;
using System.Collections;


namespace XiWan.UI.GridStyle
{
    public class PageData : DataBase
    {
        #region 初始化本类
        private static PageData _Instance;
        public static PageData Instance
        {
            get
            {
                return new PageData(); 
                /*
                if (_Instance == null)
                    _Instance = new PageData();
                return _Instance;
                */
            }
        }
        #endregion

        #region 获取分页信息
        /// <summary>
        ///获取分页信息
        /// </summary>
        /// <param name="strTBName">表名</param>
        /// <returns></returns>
        public DataTable GetPageInfo(string searchSql, int pageIndex, int pageSize, out int pageCount, out int rowCount)
        {
            return GetPage(searchSql, pageIndex, pageSize, out  pageCount, out  rowCount);
        }
        #endregion 

        #region 获取查询
        /// <summary>
        ///获取查询
        /// </summary>
        /// <param name="strTBName">表名</param>
        /// <returns></returns>
        public DataTable GetDtResult(string searchSql)
        {
            return GetDataTable(searchSql);
        }
        #endregion 

        public void ExecuteCommand(string strSQL)
        {
            ExecuteNonQuery(strSQL);
        }
    }
}
