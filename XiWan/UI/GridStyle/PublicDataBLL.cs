﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using XiWan.DALFactory;

namespace XiWan.UI.GridStyle
{
    public class PublicDataBLL
    {

        #region 获取查询结果集
        /// <summary>
        ///获取查询结果集
        /// </summary>
        /// <param name="searchSql">sql语句</param>
        /// <returns></returns>
        public static DataTable GetDtResult(string searchSql)
        {
            return GetDtResult(searchSql);
        }
        #endregion  

        #region 执行语句返回datatable结果集
        internal DataTable GetDataTable(string searchSql)
        {
            DataSet ds = GetDataSet(searchSql);
            DataTable dt = new DataTable();

            if (ds != null && ds.Tables.Count > 0)
            {
                dt = ds.Tables[0];
            }

            return dt;
        }
        #endregion 

        #region 执行语句返回datatable结果集
        internal DataSet GetDataSet(string searchSql)
        {
            string strConnectionString = CCommon.GetWebConfigValue("strConnectionString");
            DataSet ds = new DataSet();
            System.Data.SqlClient.SqlCommand cmd = new System.Data.SqlClient.SqlCommand();
            System.Data.SqlClient.SqlConnection conn = null;

            //输出执行语句 以便测试            
            SqlHelper.Instance.WriteLog(searchSql);

            try
            {
                conn = new System.Data.SqlClient.SqlConnection(strConnectionString);
                conn.Open();
                cmd.Connection = conn;
                cmd.CommandText = searchSql;
                cmd.CommandType = CommandType.Text;
                cmd.CommandTimeout = 900;//60*15
                System.Data.SqlClient.SqlDataAdapter sda = new System.Data.SqlClient.SqlDataAdapter(cmd);
                sda.Fill(ds);
                conn.Close();
            }
            catch (Exception ex)
            {
                throw ex;
                //this.Message = ex.Message;
                //throw new Exception(searchSql, ex);
                //Core.Base.Err.WriteLog("/" + ex.GetBaseException() + "/" + DateTime.Now + "/", Core.Base.DataType.StringHelper.Crlf + Core.Base.DataType.StringHelper.Crlf + "查询语句：" + Core.Base.DataType.StringHelper.Crlf + searchSql);
            }
            finally
            {
                if (conn.State == ConnectionState.Open)
                {
                    conn.Close();
                }
            }

            return ds;
        }
        #endregion 

        #region 获取分页信息
        /// <summary>
        ///获取分页信息
        /// </summary>
        /// <param name="strTBName">表名</param>
        /// <returns></returns>
        public static DataTable GetPage(string searchSql, int pageIndex, int pageSize, out int pageCount, out int rowCount)
        {
            return PageData.Instance.GetPageInfo(searchSql, pageIndex, pageSize, out  pageCount, out  rowCount);
        }
        #endregion

    }
}
