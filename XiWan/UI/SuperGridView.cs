﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI;
using System.Collections;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Text;
using System.Reflection;
using System.Data.SqlClient;
using XiWan.DALFactory;
using XiWan.UI.GridStyle;
using XiWan.UI;

//删除 修改 保存 全选
//链接 变量 默认日期不显示
//小计 合计 分页 多从表头，分组，排序
namespace XiWan.UI
{
    public class SuperGridView : GridView
    {
        #region 字段

        private bool isCheckAll = false;
        private bool isRowNumberer = false;
        private bool showDeleteButton = false;
        private bool showEditButton = false;
        private bool showViewButton = false;
        private Dictionary<string, decimal> m_listTotalData = null;
        private ColumnStyleCollection columns = null;       //列样式
        private ColumnStyle style = null;
        private bool isEmptyDataSource = false;
        private bool showHeadEmpty = true;
        private bool readOnly = false;
        //private bool isGridPostBack = false;
        private TextBox txtSkip = null;
        private DropDownList ddlPageSize = null;

        private bool IsEmptyData = false;

        //wcf调用
        //private bool m_isWcf = false;
        //private string m_queryClassName = "";
        //private Hashtable m_wcfSearchField = null;
        private IQureyPage m_iQureyPage;
        #endregion

        #region 属性
        /// <summary>
        ///是否显示合计行
        /// </summary>
        public bool ShowStatisticRow
        {
            get
            {
                if (ViewState["ShowStatisticRow"] == null)
                    return false;
                else
                    return bool.Parse(ViewState["ShowStatisticRow"].ToString());
            }
            set
            {
                this.ShowFooter = value;
                ViewState["ShowStatisticRow"] = value;
            }
        }

        #region 通过属性添加列

        /// <summary>
        /// 是否虚拟的分页，目前出现当记录数木少于pagesize时页脚出不来，这里模拟一个
        /// </summary>
        private bool IsVirtualPager
        {
            get;
            set;
        }

        public bool ReadOnly
        {
            set
            {
                readOnly = value;
                ViewState["ReadOnly"] = value;
            }
            get
            {
                return ViewState["ReadOnly"] == null ? false : Convert.ToBoolean(ViewState["ReadOnly"]);
            }
        }
        public bool ShowViewButton
        {
            set { showViewButton = value; }
        }
        public bool ShowEditButton
        {
            set { showEditButton = value; }
        }
        public bool ShowDeleteButton
        {
            set { showDeleteButton = value; }
        }
        /// <summary>
        /// 是否显示全选列
        /// </summary>
        public bool IsCheckAll
        {
            set { isCheckAll = value; }
        }
        /// <summary>
        /// 是否显示序列号
        /// </summary>
        public bool IsRowNumberer
        {
            set { isRowNumberer = value; }
        }

        #endregion

        /// <summary>
        /// 合计数据
        /// </summary>
        public Dictionary<string, decimal> ListTotalData
        {
            set
            {
                this.ShowFooter = true;
                m_listTotalData = value;
            }
        }
        /// <summary>
        /// 空数据时是否显示表头默认显示
        /// </summary>
        public bool ShowHeadEmpty
        {
            get
            {
                return ViewState["ShowHeadEmpty"] == null ? true : Convert.ToBoolean(ViewState["ShowHeadEmpty"]);
            }
            set
            {
                showHeadEmpty = value;
                ViewState["ShowHeadEmpty"] = value;
            }
        }

        public IQureyPage IQueyPage
        {
            get
            {
                return ViewState["m_iQureyPage"] as IQureyPage;
            }
            set
            {
                m_iQureyPage = value;
                ViewState["m_iQureyPage"] = value;
            }
        }

        public IQueryStatistic IQueryStatistics
        {
            get
            {
                if (ViewState["IQueryStatistics"] == null)
                    return null;
                else
                    return ViewState["IQueryStatistics"] as IQueryStatistic;
            }
            set
            {
                ViewState["IQueryStatistics"] = value;
            }
        }

        /// <summary>
        /// 查询条件 所必须
        /// 周真
        /// </summary>
        public object QueryCondition
        {
            get
            {
                return ViewState["QueryCondition"] as object;
            }
            set { ViewState["QueryCondition"] = value; }
        }

        //private int _AccessType;
        /// <summary>
        /// 1表示使用sql方式获取；2表示使用类方式方法获取
        /// </summary>
        public int AccessType
        {
            get
            {
                return CConvert.ObjectToInt(ViewState["AccessType"]);
            }
            set
            {
                //_AccessType = value;
                ViewState["AccessType"] = value;
            }
        }

        //public Hashtable WcfSearchField
        //{
        //    get
        //    {
        //        return ViewState["m_wcfSearchField"] as Hashtable;
        //    }
        //    set
        //    {
        //        m_wcfSearchField = value;
        //        ViewState["m_wcfSearchField"] = value;
        //    }
        //}

        //public string QueryClassName
        //{
        //    get
        //    {
        //        return ViewState["m_queryClassName"] == null ? "" : ViewState["m_queryClassName"].ToString();
        //    }
        //    set
        //    {
        //        m_queryClassName = value;
        //        ViewState["m_queryClassName"] = value;
        //    }
        //}
        #endregion

        #region 列样式

        /// <summary>
        /// 列样式
        /// </summary>
        public ColumnStyleCollection CurrentGridColumns
        {
            get
            {
                return ViewState["CurrentGridColumns"] as ColumnStyleCollection;
            }
            set
            {
                columns = value;
                SetColumns();
                InitColumns();
                ViewState["CurrentGridColumns"] = value;
            }
        }

        private void SetColumns()
        {
            if (this.readOnly)
            {
                for (int i = columns.Count - 1; i >= 0; i--)
                {
                    if (columns[i].DataSource != null)
                    {
                        columns[i].DataSource = null;
                    }
                    if (columns[i].ShowType == e_GridShowType.LinkButton || columns[i].ShowType == e_GridShowType.Button)
                    {
                        columns.RemoveAt(i);
                    }
                }
            }
            else
            {
                foreach (ColumnStyle csType in columns)
                {
                    if (csType.DataSource != null && csType.DataSource is DataRowCollection)
                    {
                        DataRowCollection rows = csType.DataSource as DataRowCollection;
                        string[,] strs = new string[rows.Count, 2];
                        for (int i = 0; i < rows.Count; i++)
                        {
                            strs[i, 0] = rows[i][csType.DataTextField] == DBNull.Value ? "" : rows[i][csType.DataTextField].ToString();
                            strs[i, 1] = rows[i][csType.DataValueField] == DBNull.Value ? "" : rows[i][csType.DataValueField].ToString();
                        }
                        csType.DataSource = strs;
                    }
                }
                if (showViewButton)
                {
                    ColumnStyle csType = GetButtonStyle();
                    csType.ButtonCollection.Add(e_CommandName.View.ToString(), "查看", e_EventType.OnClick, this.ClientID + "_View(" + ColumnStyle.DataKey + ")", false);
                }
                if (showEditButton)
                {
                    ColumnStyle csType = GetButtonStyle();
                    csType.ButtonCollection.Add(e_CommandName.Edit.ToString(), "修改", e_EventType.OnClick, this.ClientID + "_Edit(" + ColumnStyle.DataKey + ")", false);
                }
                if (showDeleteButton)
                {
                    ColumnStyle csType = GetButtonStyle();
                    csType.ButtonCollection.Add(e_CommandName.Delete.ToString(), "删除", e_EventType.OnClick, "return confirm('您确定要删除吗？')");
                }
            }
            foreach (ColumnStyle csType in columns)
            {
                if (csType.ColumnsTotal)
                    this.ShowFooter = true;
            }
            if (isCheckAll)
            {
                ColumnStyle csType = new ColumnStyle();
                csType.IsCheckAll = true;
                csType.ColumnsWidth = "1%";
                columns.Insert(0, csType);
            }
            if (isRowNumberer)
            {
                ColumnStyle csType = new ColumnStyle();
                csType.IsRowNumberer = true;
                csType.ColumnsWidth = "1%";
                csType.ColumnsDesc = "序列号";
                columns.Insert(0, csType);
            }
        }

        private ColumnStyle GetButtonStyle()
        {
            ColumnStyle csType = columns.FirstOrDefault(
                p => p.ButtonCollection != null
                );
            if (csType == null)
            {
                csType = new ColumnStyle();
                csType.ButtonCollection = new ButtonStyleCollection();
                csType.ShowType = e_GridShowType.LinkButton;
                csType.ColumnsDesc = "操作";
                columns.Add(csType);
            }
            return csType;
        }

        #endregion

        #region 回传视图状态

        protected override void LoadViewState(object savedState)
        {
            base.LoadViewState(savedState);
            InitPostBackValue();
        }
        private void InitPostBackValue()
        {
            columns = CurrentGridColumns;
            showHeadEmpty = ShowHeadEmpty;
            readOnly = ReadOnly;
            //isGridPostBack = true;
        }
        protected override void TrackViewState()
        {
            base.TrackViewState();
        }
        protected override object SaveViewState()
        {
            return base.SaveViewState();
        }

        #endregion

        #region 初始化网格

        public SuperGridView()
        {
            this.AutoGenerateColumns = false;
            if (string.IsNullOrEmpty(this.HeaderStyle.CssClass))
                this.HeaderStyle.CssClass = "trHead";
            this.Width = Unit.Percentage(100);
        }
        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            this.PageSize = 20;
        }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
        }

        #endregion

        #region 删除编辑取消更新事件的处理

        protected override void OnRowDeleting(GridViewDeleteEventArgs e)
        {
            GridViewDeleteEventHandler handler = base.Events[GetValue(typeof(GridView), this, "EventRowDeleting")] as GridViewDeleteEventHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected override void OnRowCancelingEdit(GridViewCancelEditEventArgs e)
        {
            GridViewCancelEditEventHandler handler = base.Events[GetValue(typeof(GridView), this, "EventRowCancelingEdit")] as GridViewCancelEditEventHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected override void OnRowEditing(GridViewEditEventArgs e)
        {
            GridViewEditEventHandler handler = base.Events[GetValue(typeof(GridView), this, "EventRowEditing")] as GridViewEditEventHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        protected override void OnRowUpdating(GridViewUpdateEventArgs e)
        {
            GridViewUpdateEventHandler handler = base.Events[GetValue(typeof(GridView), this, "EventRowUpdating")] as GridViewUpdateEventHandler;
            if (handler != null)
            {
                handler(this, e);
            }
        }

        #endregion

        #region 合计和小计
        //private void TotaRowlData()
        //{
        //    //绑定统计信息
        //    if (IQueryStatistics != null)
        //    {
        //        DataTable statistic = GetStatistics(QueryCondition);
        //        if (statistic != null && statistic.Rows.Count > 0)
        //        {
        //            GridViewRow gridRow = this.FooterRow;
        //            for (int i = 0; i < this.columns.Count; i++)
        //            {
        //                if (columns[i].ColumnsWidth == "0%")
        //                    gridRow.Cells.RemoveAt(i);
        //            }
        //            gridRow.Cells[0].Text = "<label class=\"StatisticLabel\"><B>合计:<B></label>";
        //            for (int i =0; i<this.columns.Count;i++)
        //            {
        //                foreach (DataColumn dc in statistic.Columns)
        //                {
        //                    if (this.columns[i].ColumnsName == dc.ColumnName)
        //                    {
        //                        //处理宽度为0%列
        //                        int j = 0;
        //                        for (int k=0; k < i; k++)
        //                        {
        //                            if (this.columns[k].ColumnsWidth != "0%")
        //                                j++;
        //                        }
        //                        gridRow.Cells[j].Text = "<label class=\"StatisticText\">" + statistic.Rows[0][dc.ColumnName].ToString() + "</label>";
        //                    }
        //                }
        //            }
        //        }
        //    }
        //}
        private void TotaRowlData()
        {
            //绑定统计信息
            if (IQueryStatistics != null || !string.IsNullOrEmpty(QuerySumSQL))
            {
                DataTable statistic = null;

                if (!string.IsNullOrEmpty(QuerySumSQL))
                {   
                    //合计语句
                    if (PageSumInfo == null || PageIndex < 1)
                    {
                        statistic = XiWan.UI.GridStyle.PublicDataBLL.GetDtResult(QuerySumSQL);
                        PageSumInfo = statistic;                  
                    }
                    else
                    {
                        statistic = PageSumInfo;
                    }
                }
                else
                {
                    statistic = GetStatistics(QueryCondition);
                }

                if (statistic != null && statistic.Rows.Count > 0)
                {
                    GridViewRow gridRow = this.FooterRow;
                    for (int i = 0; i < this.columns.Count; i++)
                    {
                        if (columns[i].ColumnsWidth == "0%")
                            gridRow.Cells.RemoveAt(i);
                    }
                    gridRow.Cells[0].Text = "<label class=\"StatisticLabel\"><B>合计:<B></label>";

                    string strValue = "";
                    for (int i = 0; i < this.columns.Count; i++)
                    {
                        foreach (DataColumn dc in statistic.Columns)
                        {
                            if (this.columns[i].ColumnsName == dc.ColumnName)
                            {
                                //处理宽度为0%列
                                int j = 0;
                                for (int k = 0; k < i; k++)
                                {
                                    if (this.columns[k].ColumnsWidth != "0%")
                                        j++;
                                }
                                strValue = statistic.Rows[0][dc.ColumnName].ToString();
                                if (strValue.IndexOf('.') > 0)
                                    strValue = string.Format("{0:N2}",CConvert.StringToDecimal(strValue));

                                gridRow.Cells[j].Text = "<label class=\"StatisticText\">" + strValue + "</label>";
                            }
                        }
                    }
                }
            }
            else if (m_listTotalData == null)
            {
                string totalstr = "<strong>{0}</strong>";
                List<string> totalFileds = new List<string>();
                foreach (ColumnStyle item in columns)
                {
                    if (item.ColumnsTotal)
                    {
                        totalFileds.Add(item.ColumnsName);
                    }
                }
                if (totalFileds.Count > 0)
                {
                    GridViewRow gridRow = this.FooterRow;
                    if (AllowPaging)
                    {
                        gridRow.Cells[0].Text = string.Format(totalstr, "小计：");
                    }
                    else
                    {
                        gridRow.Cells[0].Text = string.Format(totalstr, "合计：");
                    }
                    gridRow.Cells[0].Attributes["nowrap"] = "nowrap";
                    m_listTotalData = new Dictionary<string, decimal>();
                    foreach (string filed in totalFileds)
                    {
                        m_listTotalData.Add(filed, 0.00m);
                    }
                    object ojb = null;
                    if (this.DataSource is DataTable)
                    {
                        DataTable dtb = this.DataSource as DataTable;
                        foreach (DataRow row in dtb.Rows)
                        {
                            foreach (string filed in totalFileds)
                            {
                                ojb = row[filed];
                                m_listTotalData[filed] += ojb == DBNull.Value ? 0.00m : Convert.ToDecimal(ojb);
                            }
                        }
                    }
                    else
                    {
                        IEnumerable list = this.DataSource as IEnumerable;
                        foreach (object item in list)
                        {
                            foreach (string filed in totalFileds)
                            {
                                ojb = GetValue(item, filed);
                                m_listTotalData[filed] += ojb == DBNull.Value ? 0.0m : Convert.ToDecimal(ojb);
                            }
                        }
                    }
                    foreach (KeyValuePair<string, decimal> item in m_listTotalData)
                    {
                        SetTotal(gridRow);
                    }
                }
            }
            else
            {
                GridViewRow gridRow = this.FooterRow;
                for (int i = 0; i < this.columns.Count; i++)
                {
                    if (this.columns[i].ColumnsWidth != "0%")
                    {
                        gridRow.Cells[i].Text = "<strong>合计</strong>";
                        gridRow.Cells[i].Attributes["nowrap"] = "nowrap";
                        break;
                    }
                }

                SetTotal(gridRow);
            }
        }

        private void SetTotal(GridViewRow gridRow)
        {
            foreach (KeyValuePair<string, decimal> item in m_listTotalData)
            {
                string value = item.Value.ToString();
                int index = value.IndexOf('.');
                int indexField = IndexOf(item.Key);
                if (index > 0)
                    gridRow.Cells[indexField].Text = string.Format("{0:N2}", item.Value);// decimal.Round(item.Value, 2).ToString();
                else
                    gridRow.Cells[indexField].Text = value;
                gridRow.Cells[indexField].HorizontalAlign = HorizontalAlign.Right;
            }
            for (int i = 0; i < columns.Count; i++)
            {
                if (columns[i].ColumnsWidth == "0%")
                {
                    gridRow.Cells[i].Style.Add("display", "none");
                }
            }
        }

        //private void TotaRowlData()
        //{
        //if (m_listTotalData == null)
        //{
        //    string totalstr = "<strong>{0}</strong>";
        //    List<string> totalFileds = new List<string>();
        //    foreach (ColumnStyle item in columns)
        //    {
        //        if (item.ColumnsTotal)
        //        {
        //            totalFileds.Add(item.ColumnsName);
        //        }
        //    }
        //    if (totalFileds.Count > 0)
        //    {
        //        GridViewRow gridRow = this.FooterRow;
        //        if (AllowPaging)
        //        {
        //            gridRow.Cells[0].Text = string.Format(totalstr, "小计：");
        //        }
        //        else
        //        {
        //            gridRow.Cells[0].Text = string.Format(totalstr, "合计：");
        //        }
        //        m_listTotalData = new Dictionary<string, decimal>();
        //        foreach (string filed in totalFileds)
        //        {
        //            m_listTotalData.Add(filed, 0.0m);
        //        }
        //        object ojb = null;
        //        if (this.DataSource is DataTable)
        //        {
        //            DataTable dtb = this.DataSource as DataTable;
        //            foreach (DataRow row in dtb.Rows)
        //            {
        //                foreach (string filed in totalFileds)
        //                {
        //                    ojb = row[filed];
        //                    m_listTotalData[filed] += ojb == DBNull.Value ? 0.0m : Convert.ToDecimal(ojb);
        //                }
        //            }
        //        }
        //        else
        //        {
        //            IEnumerable list = this.DataSource as IEnumerable;
        //            foreach (object item in list)
        //            {
        //                foreach (string filed in totalFileds)
        //                {
        //                    ojb = GetValue(item, filed);
        //                    m_listTotalData[filed] += ojb == DBNull.Value ? 0.0m : Convert.ToDecimal(ojb);
        //                }
        //            }
        //        }
        //        foreach (KeyValuePair<string, decimal> item in m_listTotalData)
        //        {
        //            gridRow.Cells[IndexOf(item.Key)].Text = item.Value.ToString();
        //        }
        //    }
        //}
        //else
        //{
        //    GridViewRow gridRow = this.FooterRow;
        //    gridRow.Cells[0].Text = "<strong>合计</strong>";
        //    foreach (KeyValuePair<string, decimal> item in m_listTotalData)
        //    {
        //        gridRow.Cells[IndexOf(item.Key)].Text = item.Value.ToString();
        //    }
        //}
        //}

        #endregion

        #region 空数据处理

        private void EmptyData()
        {
            //zhouzhen /
            if (this.DataSource == null)
            {
                return;
            }
            //zhouzhen
            object data = null;
            if (this.DataSource is DataTable)
            {
                DataTable dtb = this.DataSource as DataTable;
                if (dtb.Rows.Count == 0)
                {
                    //zhouzhen /
                    return;
                    //zhouzhen
                    //DataRow row = dtb.NewRow();
                    //dtb.Rows.Add(row);
                    //data = dtb;
                }
            }
            else if (this.DataSource is IEnumerable)
            {
                IEnumerable item = this.DataSource as IEnumerable;
                bool f = true;
                foreach (object o in item)
                {
                    f = false;
                    break;
                }
                if (f)
                {
                    data = this.GetEmptyData();
                }
            }
            else if (this.DataSource is DataView)
            {
                DataView dv = this.DataSource as DataView;
                if (dv.Count == 0)
                {//zhouzhen /
                    return;
                    //zhouzhen
                    //dv.AddNew();
                    //data = dv;
                }
            }
            else if (this.DataSource is DataSet)
            {
                DataSet ds = this.DataSource as DataSet;
                if (ds.Tables[0].Rows.Count == 0)
                {//zhouzhen /
                    return;
                    //zhouzhen
                    //DataRow row = ds.Tables[0].NewRow();
                    //ds.Tables[0].Rows.Add(row);
                    //data = ds.Tables[0];
                }
            }
            if (data != null)
            {
                this.isEmptyDataSource = true;
                this.DataSource = data;
            }
        }

        public void EmptyDataBind()
        {
            this.isEmptyDataSource = true;
            this.DataSource = this.GetEmptyData();
            this.DataBind();
        }

        private DataTable GetEmptyData()
        {
            DataTable dtb = new DataTable();
            foreach (ColumnStyle item in columns)
            {
                Type t = null;
                switch (item.ColumnsType)
                {
                    case "N":
                        t = typeof(decimal);
                        break;
                    case "B":
                        t = typeof(bool);
                        break;
                    case "D":
                    case "T":
                        t = typeof(DateTime);
                        break;
                    default:
                        t = typeof(string);
                        break;
                }
                if (item.ShowType == e_GridShowType.TextBoxSelect)
                {
                    string[] strFields = item.ColumnsName.Split(',');
                    foreach (string field in strFields)
                    {
                        if (!dtb.Columns.Contains(field))
                            dtb.Columns.Add(field, t);
                    }
                }
                else
                {
                    if (item.ColumnsName != null && !dtb.Columns.Contains(item.ColumnsName))
                        dtb.Columns.Add(item.ColumnsName, t);
                }
            }
            foreach (string item in this.DataKeyNames)
            {
                if (!dtb.Columns.Contains(item))
                {
                    dtb.Columns.Add(item);
                }
            }
            DataRow r = dtb.NewRow();
            dtb.Rows.Add(r);
            return dtb;
        }

        #endregion

        #region 重写网格行创建 数据绑定 相关方法

        protected override void OnRowCreated(GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.DataRow:
                    this.DataRowCreated(e);
                    break;
                case DataControlRowType.EmptyDataRow:
                    break;
                case DataControlRowType.Footer:
                    break;
                case DataControlRowType.Header:
                    this.HeaderCreated(e);
                    break;
                case DataControlRowType.Pager:
                    if (this.AllowPaging)
                        SetPaging(e);
                    break;
                case DataControlRowType.Separator:
                    break;
                default:
                    break;
            }

            base.OnRowCreated(e);
        }

        protected override void OnDataBinding(EventArgs e)
        {
            if (this.isEmptyDataSource == false)
            {
                this.EmptyData();
            }
            base.OnDataBinding(e);
        }

        protected override void OnRowDataBound(GridViewRowEventArgs e)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.DataRow:
                    this.DataRowBound(e);
                    this.SetWidth(e);//add by wenbing 2010-11-15
                    SetValue(e);
                    //if (IsChangeRiskReportCellColor.Equals("true"))//Add by Sleven 2010-12-23
                    //{
                    //    SetRiskReportQueryCellColor(e);
                    //}
                    break;
                case DataControlRowType.EmptyDataRow:
                    break;
                case DataControlRowType.Footer:
                    break;
                case DataControlRowType.Header:
                    this.SetWidth(e);//add by wenbing 2010-11-15
                    break;
                case DataControlRowType.Pager:
                    break;
                case DataControlRowType.Separator:
                    break;
                default:
                    break;
            }
            base.OnRowDataBound(e);
        }


        #region 风控系统根据是逾期就改变指定单元格变为指定颜色 【案件号 客户名 与逾期天数显示为红色】Add by Sleven 2010-12-23
        
        /// <summary>
        /// 是否干煸风控报表单元格颜色
        /// </summary>
        public string IsChangeRiskReportCellColor
        {
            get 
            {
                if (ViewState["IsChangeRiskReportCellColor"] == null)
                {
                    return "false";
                }
                return ViewState["IsChangeRiskReportCellColor"].ToString(); 
            }
            set 
            {
                ViewState["IsChangeRiskReportCellColor"] = value;
            }
        }

        /// <summary>
        /// 设置风控系统中报表单元格变色 【仅适用于风控系统报表查询的制定列条件调整指定列单元格变为红色】
        /// </summary>
        private void SetRiskReportQueryCellColor(GridViewRowEventArgs e)
        { 
            string strOverdueDays = string.Empty;
            try
            {
                strOverdueDays = e.Row.Cells[18].Text.Equals("") ? "0" : e.Row.Cells[18].Text.ToString();
                if (Convert.ToInt32(strOverdueDays) > 0)
                {
                    e.Row.Cells[1].Style.Add("Color", "Red");
                    e.Row.Cells[2].Style.Add("Color", "Red");
                    e.Row.Cells[18].Style.Add("Color", "Red");
                }
            }
            catch
            {
            }
            

        }
        #endregion 风控系统根据是逾期就改变指定单元格变为指定颜色 【案件号 客户名 与逾期天数显示为红色】

        /// <summary>
        /// 设置单元格宽度 
        /// wenbing
        /// 2010-11-15
        /// </summary>
        /// <param name="e"></param>
        private void SetWidth(GridViewRowEventArgs e)
        {
            for (int i = 0; i < CurrentGridColumns.Count; i++)
            {
                if (CurrentGridColumns[i].ColumnsWidth == "0%")
                {
                    e.Row.Cells[i].Style.Add("display", "none");
                }
            }
        }

        //zhouzhen start
        /// <summary>
        /// 
        /// </summary>
        /// <param name="e"></param>
        private void SetValue(GridViewRowEventArgs e)
        {
            for (int i = 0; i < CurrentGridColumns.Count; i++)
            {
                if (e.Row.Cells[i].Text == "true" || e.Row.Cells[i].Text == "True")
                {
                    e.Row.Cells[i].Text = "是";
                }
                if (e.Row.Cells[i].Text == "false" || e.Row.Cells[i].Text == "False")
                {
                    e.Row.Cells[i].Text = "否";
                }
            }
        }
        //zhouzhen end

        protected override void OnDataBound(EventArgs e)
        {
            this.TotaRowlData();
            this.DataTimeDefault();
            base.OnDataBound(e);
        }

        #endregion

        #region 在对行数据绑定后处理

        private void DataRowBound(GridViewRowEventArgs e)
        {
            AddRowNumberer(e);
        }

        private void AddRowNumberer(GridViewRowEventArgs e)
        {
            Literal lit = null;
            for (int i = 0; i < columns.Count; i++)
            {
                if (columns[i].IsRowNumberer)
                {
                    lit = e.Row.Cells[i].Controls[0] as Literal;
                    if (this.AllowPaging)
                        lit.Text = ((e.Row.RowIndex + 1) + this.PageIndex * this.PageSize).ToString();
                    else
                        lit.Text = (e.Row.RowIndex + 1).ToString();
                    e.Row.Cells[i].Controls.Add(lit);
                }
            }
        }

        #endregion

        #region 行创建事件处理

        private void HeaderCreated(GridViewRowEventArgs e)
        {
            if (this.readOnly == false)
            {
                this.CreateTemplateHead(e);
            }
            for (int i = 0; i < columns.Count; i++)
            {
                switch (columns[i].HeadAlign)
                {
                    case e_TextAlign.Left:
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Left;
                        break;
                    case e_TextAlign.Right:
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                        break;
                }
                if (columns[i].IsCheckAll)
                {
                    CheckBox chb = new CheckBox();
                    e.Row.Cells[i].Controls.Add(chb);
                    chb.Attributes["onclick"] = "CheckAll(this,'" + this.ID + "'," + i + ")";
                    if (columns[i].ColumnsDesc != null)
                        e.Row.Cells[i].Controls.Add(new LiteralControl(columns[i].ColumnsDesc));
                }
                e.Row.Cells[i].Attributes["nowrap"] = "nowrap";
            }
        }

        private void DataRowCreated(GridViewRowEventArgs e)
        {
            if (IsEmptyData == true)
                return;
            if (this.readOnly == false)
            {
                this.CreateTemplate(e);
            }
            e.Row.Attributes["onmouseover"] = "grid_Mover(this)";
            e.Row.Attributes["onmouseout"] = "grid_Mout(this)";
            if (e.Row.RowIndex % 2 == 0)
            {
                e.Row.Style.Add("background-color", "#F5f5f5");
            }
            for (int i = 0; i < columns.Count; i++)
            {
                switch (columns[i].ItemAlign)
                {
                    case e_TextAlign.Center:
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Center;
                        break;
                    case e_TextAlign.Right:
                        e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;
                        break;
                }
                if (columns[i].IsRowNumberer)
                {
                    e.Row.Cells[i].Controls.Add(new Literal());
                }
                if (columns[i].IsCheckAll)
                {
                    CheckBox cb = new CheckBox();
                    cb.Attributes["title"] = this.DataKeys[e.Row.RowIndex].Value.ToString();
                    e.Row.Cells[i].Controls.Add(cb);
                    //e.Row.Cells[i].Controls.Add(new CheckBox());
                }
                if (columns[i].ShowType == e_GridShowType.Control)
                {
                    if (this.readOnly)
                    {
                        Control c = null;
                        columns[i].ColumnControl(ref c);
                        e.Row.Cells[i].Controls.Add(c);
                        e.Row.Cells[i].Enabled = false;
                    }
                }
            }
        }

        #endregion

        #region 数据绑定和分页

        protected override void OnPageIndexChanging(GridViewPageEventArgs e)
        {
            this.PageIndex = e.NewPageIndex;
            //isGridPostBack = false;
            if (!IsVirtualPager)
                QueryBind();
        }

        private void SetPaging(GridViewRowEventArgs e)
        {
            int buttonCount = this.PagerSettings.PageButtonCount;
            int pageIndex = this.PageIndex;
            int pageCount = this.PageCount;
            TableCell tc = e.Row.Cells[0];
            tc.Controls.Clear();
            if (ddlPageSize == null)
            {
                ddlPageSize = new DropDownList();
                ddlPageSize.AutoPostBack = true;
                ddlPageSize.Items.Add("5");
                ddlPageSize.Items.Add("10");
                ddlPageSize.Items.Add("15");
                ddlPageSize.Items.Add("20");
                ddlPageSize.Items.Add("50");
                ddlPageSize.SelectedIndexChanged += new EventHandler(ddlPageSize_SelectedIndexChanged);
            }
            ddlPageSize.SelectedValue = this.PageSize.ToString();
            tc.Attributes.Add("class", "pager");
            tc.Controls.Add(new LiteralControl("共<font color='red'>" + this.RecordCount + "</font>条&nbsp;&nbsp;每页"));
            tc.Controls.Add(ddlPageSize);
            if (IsVirtualPager)//虚拟的分页只有一页记录
                tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;第<font color='red'>" + (pageIndex + 1) + "</font>页&nbsp;&nbsp;共<font color='red'>1</font>页&nbsp;&nbsp;"));
            else
                tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;第<font color='red'>" + (pageIndex + 1) + "</font>页&nbsp;&nbsp;共<font color='red'>" + pageCount + "</font>页&nbsp;&nbsp;"));
            if (!IsVirtualPager)
            {
                AddFirstAndPrev(tc, pageIndex);
                if (pageIndex == 0 && pageCount >= buttonCount)
                {
                    AddPagingButton(buttonCount, tc, pageIndex);
                }
                else if (pageCount < buttonCount)
                {
                    AddPagingButton(pageCount, tc, pageIndex);
                }
                else
                {
                    AddPagingButton(buttonCount, tc, pageIndex, pageCount);
                }
                AddNextAndLast(tc, pageIndex, pageCount);
                AddSkipPageButton(tc);
            }
            else
            {
                AddFirstAndPrev(tc, pageIndex);
                AddNextAndLast(tc, pageIndex, pageCount);
                AddSkipPageButton(tc);
            }
        }

        //private void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    this.PageSize = int.Parse(ddlPageSize.SelectedValue);
        //    QueryBind();
        //}
        private void ddlPageSize_SelectedIndexChanged(object sender, EventArgs e)
        {
            int size = int.Parse(ddlPageSize.SelectedValue);
            //if (Core.Base.Web.SessionManager.GetSession(this.Page.UniqueID + "pagesize") != null)
            //{
            //    if (size == int.Parse(Core.Base.Web.SessionManager.GetSession(this.Page.UniqueID + "pagesize").ToString()))
            //        return;
            //}
            //else
            //    Core.Base.Web.SessionManager.Add(this.Page.UniqueID + "pagesize", size.ToString());
            this.PageSize = size;
            int recordCount = RecordCount;
            int pageCount = recordCount / size;
            if (recordCount % size > 0)
                pageCount++;
            if ((PageIndex + 1) > pageCount)
            {
                PageIndex = 0;
            }
            QueryBind();//重新绑定数据 wenbing 2010-11-17
            //if (string.IsNullOrEmpty(QuerySQL))
            //{
            //    GridViewPageEventHandler handler = base.Events[GetValue(typeof(GridView), this, "EventPageIndexChanging")] as GridViewPageEventHandler;
            //    if (handler != null)
            //    {
            //        GridViewPageEventArgs e1 = new GridViewPageEventArgs(this.PageIndex);
            //        handler(this, e1);
            //    }
            //}
            //else
            //    QueryBind();
        }

        /// <summary>
        ///2010-11-30 从旧项目移过来
        ///jdx  选中行的主键集合。
        /// </summary>
        public ArrayList SelectedKeys
        {
            get
            {
                ArrayList list = null;
                CheckBox chk = null;

                foreach (GridViewRow row in this.Rows)
                {
                    chk = (CheckBox)row.FindControl("ctl00");
                    if (chk != null && chk.Checked == true)
                    {
                        if (list == null)
                        {
                            list = new ArrayList();
                        }
                        list.Add(this.DataKeys[row.RowIndex].Value);
                    }
                }

                return list;
            }
        }


        /// <summary>
        /// 添加跳转查找功能
        /// </summary>
        /// <param name="tc"></param>
        private void AddSkipPageButton(TableCell tc)
        {
            Button btnSkip = new Button();
            txtSkip = new TextBox();
            txtSkip.Width = 40;
            //txtSkip.ReadOnly = true;  //暂时停用转到输入框
            btnSkip.Text = "转到";
            btnSkip.Click += new EventHandler(btnSkip_Click);
            //btnSkip.Enabled = false;  //暂时停用转到按钮功能
            
            tc.Controls.Add(txtSkip);
            tc.Controls.Add(btnSkip);
        }

        private void btnSkip_Click(object sender, EventArgs e)
        {
            string skipPage = txtSkip.Text.Trim();
            string message = null;
            //if (skipPage == "")
            //{
            //    //message = "请填写转到页数！";
            //}
            //else if (IfNumber(skipPage))
            //{
            //    if (int.Parse(skipPage) <= 0)
            //    {
            //        //message = "页数必须是大于0的正整数！";
            //    }
            //    else if (int.Parse(skipPage) <= this.PageCount)
            //    {
            //        Button btnSkip = sender as Button;
            //        btnSkip.CommandName = "Page";
            //        btnSkip.CommandArgument = skipPage;
            //    }
            //    else
            //    {
            //        message = "请输入小于等于当前总页的数字！";
            //    }
            //}
            //else
            //{
            //    message = "页数必须是小于等于当前总页的正整数！";
            //}
            //if (message != null)
            //{
            //    this.Page.ClientScript.RegisterStartupScript(this.Page.GetType(), Guid.NewGuid().ToString(), "<script type=\"text/javascript\">alert('" + message + "')</script>");
            //}

            //Modify by Sleven 2010-12-24
            if (skipPage == "")
            {
                skipPage = "1";  //不输入默认到第一页
            }
            else if (IfNumber(skipPage))
            {
                if (int.Parse(skipPage) <= 0)
                {
                    skipPage = "1";  //小于等于0的也默认给第一页
                }
                if (int.Parse(skipPage) > this.PageCount)
                {
                    skipPage = this.PageCount.ToString();
                }
            }
            else
            {
                skipPage = "1";  //输入的不是数字默认也给第一页
            }
            Button btnSkip = sender as Button;
            btnSkip.CommandName = "Page";
            btnSkip.CommandArgument = skipPage;
        }

        private void AddNextAndLast(TableCell tc, int pageIndex, int pageCount)
        {
            LinkButton Next = new LinkButton();
            LinkButton Last = new LinkButton();
            if (!String.IsNullOrEmpty(PagerSettings.NextPageImageUrl))
            {
                Next.Text = "<img src='" + ResolveUrl(PagerSettings.NextPageImageUrl) + "' border='0'/>";
            }
            else
            {
                Next.Text = PagerSettings.NextPageText;
            }
            Next.CommandName = "Page";
            Next.CommandArgument = "Next";
            if (!String.IsNullOrEmpty(PagerSettings.LastPageImageUrl))
            {
                Last.Text = "<img src='" + ResolveUrl(PagerSettings.LastPageImageUrl) + "' border='0'/>";
            }
            else
            {
                Last.Text = PagerSettings.LastPageText;
            }
            Last.CommandName = "Page";
            Last.CommandArgument = "Last";
            if ((pageIndex + 1) == pageCount)
            {
                Last.Enabled = false;
                Next.Enabled = false;
            }
            else
            {
                Next.Enabled = true;
                Last.Enabled = true;
            }
            if (IsVirtualPager)
            {
                Next.Enabled = false;
                Last.Enabled = false;
            }
            tc.Controls.Add(Next);
            tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
            tc.Controls.Add(Last);
            tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
        }

        private void AddFirstAndPrev(TableCell tc, int pageIndex)
        {
            LinkButton First = new LinkButton();
            LinkButton Prev = new LinkButton();
            if (!String.IsNullOrEmpty(PagerSettings.FirstPageImageUrl))
            {
                First.Text = "<img src='" + ResolveUrl(PagerSettings.FirstPageImageUrl) + "' border='0'/>";
            }
            else
            {
                First.Text = PagerSettings.FirstPageText;
            }
            First.CommandName = "Page";
            First.CommandArgument = "First";
            if (!String.IsNullOrEmpty(PagerSettings.PreviousPageImageUrl))
            {
                Prev.Text = "<img src='" + ResolveUrl(PagerSettings.PreviousPageImageUrl) + "' border='0'/>";
            }
            else
            {
                Prev.Text = PagerSettings.PreviousPageText;
            }
            Prev.CommandName = "Page";
            Prev.CommandArgument = "Prev";

            if (pageIndex <= 0)
            {
                First.Enabled = Prev.Enabled = false;
            }
            else
            {
                First.Enabled = Prev.Enabled = true;
            }
            if (IsVirtualPager)
            {
                First.Enabled = false;
                Prev.Enabled = false;
            }
            tc.Controls.Add(First);
            tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
            tc.Controls.Add(Prev);
            tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
        }

        private void AddPagingButton(int buttonCount, TableCell tc, int pageIndex, int pageCount)
        {
            string test = "";
            int rightCount = buttonCount / 2;
            int leftCount = buttonCount % 2 == 0 ? rightCount - 1 : rightCount;
            int i = pageIndex - leftCount;
            if (i < 0)
                i = 0;
            int count = i + buttonCount;
            if (count > pageCount)
            {
                count = pageCount;
                i = count - buttonCount;
            }
            for (; i < count; i++)
            {
                test = (i + 1).ToString();
                if (i == pageIndex)
                    tc.Controls.Add(new LiteralControl("<font color=\"Red\"><strong>" + test + "</strong></font>"));
                else
                {
                    LinkButton lb = new LinkButton();
                    lb.Text = test;
                    lb.CommandName = "Page";
                    lb.CommandArgument = test;
                    tc.Controls.Add(lb);
                }
                tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
            }
        }

        private static void AddPagingButton(int buttonCount, TableCell tc, int pageIndex)
        {
            string test = "";
            for (int i = 0; i < buttonCount; i++)
            {
                test = (i + 1).ToString();
                if (i == pageIndex)
                {
                    tc.Controls.Add(new LiteralControl("<font color=\"Red\"><strong>" + test + "</strong></font>"));
                }
                else
                {
                    LinkButton lb = new LinkButton();
                    lb.Text = test;
                    lb.CommandName = "Page";
                    lb.CommandArgument = test;
                    tc.Controls.Add(lb);
                }
                tc.Controls.Add(new LiteralControl("&nbsp;&nbsp;"));
            }
        }
        /// <summary>
        /// 当前索引页
        /// </summary>
        public override int PageIndex
        {
            get
            {
                return this.NewPageIndex;
            }
            set
            {
                this.NewPageIndex = value;
            }
        }
        private int NewPageIndex
        {
            get
            {
                return ViewState["NewPageIndex"] == null ? 0 : Convert.ToInt32(ViewState["NewPageIndex"]);
            }
            set
            {
                ViewState["NewPageIndex"] = value;
            }
        }

        /// <summary>
        /// 总页数
        /// </summary>
        public override int PageCount
        {
            get
            {
                return NewPageCount;
            }
        }
        private int NewPageCount
        {
            get
            {
                return ViewState["NewPageCount"] == null ? 0 : Convert.ToInt32(ViewState["NewPageCount"]);
            }
            set
            {
                ViewState["NewPageCount"] = value;
            }
        }

        public int RecordCount
        {
            get { return NewRecordCount; }
            set { ViewState["RecordCount"] = value; }
        }
        private int NewRecordCount
        {
            get
            {
                return ViewState["NewRecordCount"] == null ? 0 : Convert.ToInt32(ViewState["NewRecordCount"]);
            }
            set
            {
                ViewState["NewRecordCount"] = value;
            }
        }

        /// <summary>
        /// 当前页的总计缓存
        /// </summary>
        private DataTable PageSumInfo
        {
            get
            {
                return ViewState["PageSumInfo"] == null ? null : (DataTable)ViewState["PageSumInfo"];
            }
            set
            {
                ViewState["PageSumInfo"] = value;
            }
        }

        public string QuerySQL
        {
            get
            {
                return ViewState["QuerySQL"] == null ? "" : ViewState["QuerySQL"].ToString();
            }
            set
            {
                ViewState["QuerySQL"] = value;
            }
        }

        public string QuerySumSQL
        {
            get
            {
                return ViewState["QuerySumSQL"] == null ? "" : ViewState["QuerySumSQL"].ToString();
            }
            set
            {
                ViewState["QuerySumSQL"] = value;
            }
        }


        public string OrderByFiled
        {
            get
            {
                return ViewState["OrderByFiled"] == null ? "" : ViewState["OrderByFiled"].ToString();
            }
            set
            {
                ViewState["OrderByFiled"] = value;
            }
        }

        public string FunctionsStr
        {
            get
            {
                return ViewState["FunctionsStr"] == null ? "" : "," + ViewState["FunctionsStr"].ToString();
            }
            set
            {
                ViewState["FunctionsStr"] = value;
            }
        }

        //是来显示全选
        public bool EnableCheckAll
        {
            get
            {
                if (ViewState["EnableCheckAll"] == null)
                {
                    return false;
                }
                return (bool)ViewState["EnableCheckAll"];
            }
            set
            {
                ViewState["EnableCheckAll"] = value;
            }
        }


        public void QueryBind()
        {
            
                int pageCount = 0;
                int recordCount = 0;
                if (this.DataSource == null)
                {
                    DataTable dt = GetPage(QueryCondition, QuerySQL, OrderByFiled, FunctionsStr, PageIndex + 1, PageSize, out pageCount, out recordCount, this.Timeout);
                    if (dt == null || dt.Rows.Count == 0)
                    {
                        FillEmptyString(out dt);
                        pageCount = 1;
                    }
                    this.DataSource = dt;
                }

                if (this.AllowPaging)
                {
                    //页面缓存记录数量, 第二页开始后将不做取记录数
                    if (PageIndex > 0 && RecordCount > 0)
                    {
                        recordCount = RecordCount;
                    }
                    //  计算页数
                    if (recordCount % PageSize == 0)
                    {
                        pageCount = recordCount / PageSize;
                    }
                    else
                    {
                        pageCount = Math.Abs(recordCount / PageSize) + 1;
                    }

                    //当删除数据时，如果当前页只有一条记录并且总页数大于一，则跳到前一页
                    if (this.PageIndex >= pageCount && pageCount != 0)
                    {
                        this.PageIndex--;
                        this.DataSource = GetPage(QueryCondition, QuerySQL, OrderByFiled, FunctionsStr, PageIndex + 1, PageSize, out pageCount, out recordCount, this.Timeout);
                    }
                    this.NewPageCount = pageCount;
                    this.NewRecordCount = recordCount;
                    if (pageCount == 1 && recordCount <= PageSize)
                    {
                        IsVirtualPager = true;
                        this.NewPageCount = 2;
                        this.DataBind();
                        this.NewPageCount = 1;//如果不设回去会有问题，造成页面查询事件失效
                    }
                    else
                    {
                        this.DataBind();
                    }
                }
                else
                {
                    //this.DataSource = null;
                    this.DataBind();
                }
        }

        /// <summary>
        /// 当数据源为空时，填充空数据
        /// </summary>
        private void FillEmptyString(out DataTable dt)
        {
            IsEmptyData = true;

            dt = new DataTable();
            foreach (ColumnStyle cs in this.columns)
            {
                dt.Columns.Add(cs.ColumnsName);
            }
            foreach (string name in this.DataKeyNames)
            {
                bool flag = false;
                foreach (ColumnStyle cs in this.columns)
                {
                    if (name == cs.ColumnsName)
                    {
                        flag = true;
                        break;
                    }
                }
                if (!flag)
                    dt.Columns.Add(name);
            }
            dt.Rows.Add(new object[] { "" });
            foreach (string name in this.DataKeyNames)
            {
                dt.Rows[0][name] = "";
            }
            foreach (ColumnStyle cs in this.columns)
            {
                if (cs.ColumnsWidth != "0%" && cs.ColumnsName != null)
                {
                    dt.Rows[0][cs.ColumnsName] = "无相关记录！";
                    break;
                }
            }
        }
             

        private DataTable GetStatistics(object queryCondition)
        {
            if (PageSumInfo == null || PageIndex < 1)
            {
                DataTable dt = this.IQueryStatistics.GetStatistics(queryCondition);
                PageSumInfo = dt;
                return dt;
            }
            else
            {
                return PageSumInfo;
            }
        }


        public DataTable GetPage(object queryCondition, string sql, string sort, string strFunction, int pageIndex, int pageSize, out int pageCount, out int rowCount, int timeout)
        {
            //wcf调用
            if (AccessType==1)
            {
                //QueryProxy.QueryServiceClient qsc = new WebControlLibrary.QueryProxy.QueryServiceClient();
                return XiWan.UI.GridStyle.PublicDataBLL.GetPage(sql, pageIndex, pageSize, out pageCount, out rowCount);

            }else
            {
                return this.IQueyPage.GetPage(queryCondition, pageIndex, pageSize, out pageCount, out rowCount);
            }


            //分页这里面本来要打算写一个高性能存储过程的 由于时间关系暂时不写
            /*
            if (pageIndex == 0 || (pageIndex * pageSize) <= 43000)
            {
                StringBuilder strSql = new StringBuilder();
                int locationCount = pageIndex * pageSize;
                strSql.Append("select count(*) from (" + sql + ")a ");
                if (pageIndex == 1)
                {
                    strSql.Append("select top " + pageSize + " *" + strFunction + " from(" + sql + ")a order by " + sort);
                }
                else
                {
                    strSql.Append("select top " + pageSize + " *" + strFunction + " from(select top " + locationCount + " row_number() over(order by " + sort + ") #@$,*from (");
                    strSql.Append(sql);
                    strSql.Append(")a)a where #@$>" + (locationCount - pageSize));
                }
                DataSet dst = DAL.ControllerFactory.Controller.GetDataSet(strSql.ToString());
                rowCount = Convert.ToInt32(dst.Tables[0].Rows[0][0]);
                if (rowCount == 0)
                {
                    pageCount = 0;
                }
                else
                {
                    pageCount = rowCount / pageSize;
                    if (rowCount % pageSize > 0)
                        pageCount++;
                }
                return dst.Tables[1];
            }
            else
            {
                if (strFunction != "")
                {
                    sql = "select *" + strFunction + " from(" + sql + ")a";
                }
                SqlParameter prmSql = new SqlParameter("@Sql", sql);
                SqlParameter prmPageIndex = new SqlParameter("@PageIndex", SqlDbType.Int);
                SqlParameter prmPageSize = new SqlParameter("@PageSize", SqlDbType.Int);
                SqlParameter prmPageCount = new SqlParameter("@PageCount", SqlDbType.Int);
                SqlParameter prmRowCount = new SqlParameter("@RowCount", SqlDbType.Int);
                prmPageIndex.Value = pageIndex;
                prmPageSize.Value = pageSize;
                prmPageCount.Value = 0;
                prmPageCount.Direction = ParameterDirection.InputOutput;
                prmRowCount.Value = 0;
                prmRowCount.Direction = ParameterDirection.InputOutput;
                DataSet dst = new DataSet();
                DAL.ControllerBase controller = DAL.ControllerFactory.GetController();
                controller.CommandTimeout = timeout;
                controller.FillDataSetSP("procSelectPage", new SqlParameter[] { prmSql, prmPageIndex, prmPageSize, prmPageCount, prmRowCount }, dst);
                pageCount = (int)prmPageCount.Value;
                rowCount = (int)prmRowCount.Value;
                // 去掉多出的列ROWSSTAT
                DataColumn dclRowStat = dst.Tables[1].Columns[dst.Tables[1].Columns.Count - 1];
                if (dclRowStat.ColumnName.ToUpper() == "ROWSTAT")
                {
                    dst.Tables[1].Columns.Remove(dclRowStat);
                    dst.Tables[1].AcceptChanges();
                }
                return dst.Tables[1];
            }
            */
        }

        public int Timeout
        {
            get
            {
                return ViewState["Timeout"] == null ? 60 : Convert.ToInt32(ViewState["Timeout"]);
            }
            set
            {
                this.ViewState["Timeout"] = value;
            }
        }

        #endregion

        #region 模板列

        private void CreateTemplateHead(GridViewRowEventArgs e)
        {
            for (int i = 0; i < columns.Count; i++)
            {
                if (columns[i].ShowType != e_GridShowType.Literal)
                {
                    e.Row.Cells[i].Text = columns[i].ColumnsDesc;
                }
            }
        }

        private void CreateTemplate(GridViewRowEventArgs e)
        {
            ColumnStyle csType = null;
            for (int i = 0; i < columns.Count; i++)
            {
                csType = columns[i];
                WebControl con = null;
                switch (csType.ShowType)
                {
                    case e_GridShowType.TextBox:
                        con = new TextBox();
                        break;
                    case e_GridShowType.DropDownList:
                        con = new DropDownList();
                        break;
                    case e_GridShowType.CheckBox:
                        con = new CheckBox();
                        break;
                    case e_GridShowType.RadioButton:
                        RadioButton rdb1 = new RadioButton();
                        RadioButton rdb2 = new RadioButton();
                        if (csType.RadioTexts == null)
                        {
                            rdb1.Text = "男";
                            rdb2.Text = "女";
                        }
                        else
                        {
                            rdb1.Text = style.RadioTexts[0];
                            rdb2.Text = style.RadioTexts[1];
                        }
                        Panel panel = new Panel();
                        panel.Controls.Add(rdb1);
                        panel.Controls.Add(rdb2);
                        con = panel;
                        break;
                    case e_GridShowType.Button:
                        Panel panel1 = new Panel();
                        foreach (ButtonStyle item in csType.ButtonCollection)
                        {
                            if (item.IsPostBack)
                            {
                                Button btn = new Button();
                                btn.CommandName = item.CommandName;
                                btn.Text = item.Text;
                                panel1.Controls.Add(btn);
                                LiteralControl lit = new LiteralControl();
                                lit.Text = "&nbsp;";
                                panel1.Controls.Add(lit);
                                //if (isGridPostBack == false)
                                btn.Attributes["row"] = i.ToString();
                                btn.DataBinding += new EventHandler(con_DataBinding);
                            }
                            else
                            {
                                Literal lit = new Literal();
                                if (string.IsNullOrEmpty(item.CssClass))
                                    lit.Text = "<input type=\"button\" " + item.EventType + "=\"" + item.ClientMethod + "\" value=\"" + item.Text + "\"/>";
                                else
                                    lit.Text = "<input type=\"button\" class=" + item.CssClass + " " + item.EventType + "=\"" + item.ClientMethod + "\" value=\"" + item.Text + "\"/>";
                                LiteralControl lit1 = new LiteralControl();
                                lit1.Text = "&nbsp;";
                                panel1.Controls.Add(lit);
                                panel1.Controls.Add(lit1);
                            }
                        }
                        con = panel1;
                        if (con.Controls.Count > 1)
                        {
                            con.Controls.RemoveAt(con.Controls.Count - 1);
                        }
                        break;
                    case e_GridShowType.LinkButton:
                        Panel panel2 = new Panel();
                        foreach (ButtonStyle item in csType.ButtonCollection)
                        {
                            if (item.IsPostBack)
                            {
                                LinkButton lnk = new LinkButton();
                                lnk.CommandName = item.CommandName;
                                lnk.Text = item.Text;
                                panel2.Controls.Add(lnk);
                                LiteralControl lit = new LiteralControl();
                                lit.Text = "&nbsp;";
                                panel2.Controls.Add(lit);
                                //if (isGridPostBack == false)
                                lnk.Attributes["row"] = i.ToString();
                                lnk.DataBinding += new EventHandler(con_DataBinding);
                            }
                            else
                            {
                                Literal lit = new Literal();
                                if (item.EventType == e_EventType.OnClick)
                                {
                                    lit.Text = "<a href=\"javascript:" + item.ClientMethod + "\">" + item.Text + "</a>";
                                }
                                else
                                {
                                    lit.Text = "<a href=\"javascript:void(0)\" " + item.EventType + "=\"" + item.ClientMethod + "\">" + item.Text + "</a>";
                                }
                                LiteralControl lit1 = new LiteralControl();
                                lit1.Text = "&nbsp;";
                                panel2.Controls.Add(lit);
                                panel2.Controls.Add(lit1);
                            }
                        }
                        con = panel2;
                        if (con.Controls.Count > 1)
                        {
                            con.Controls.RemoveAt(con.Controls.Count - 1);
                        }
                        break;
                    case e_GridShowType.TextBoxSelect:
                        Panel panelts = new Panel();
                        HtmlInputHidden hid = new HtmlInputHidden();
                        TextBox txt = new TextBox();
                        panelts.Controls.Add(hid);
                        panelts.Controls.Add(txt);
                        if (csType.IsNotNull)
                        {
                            LiteralControl lit = new LiteralControl();
                            lit.Text = "<font color=\"red\">*</font>";
                            panelts.Controls.Add(lit);
                        }
                        HtmlInputButton btnts = new HtmlInputButton();
                        btnts.Value = "...";
                        panelts.Controls.Add(btnts);
                        con = panelts;
                        hid.ID = "0" + e.Row.RowIndex + "_" + i;
                        break;
                    case e_GridShowType.HyperLink:
                        con = new HyperLink();
                        break;
                    case e_GridShowType.Control:
                        Control c = null;
                        csType.ColumnControl(ref c);
                        e.Row.Cells[i].Controls.Add(c);
                        if (this.readOnly || csType.ReadOnly)
                            e.Row.Cells[i].Enabled = false;
                        break;
                }
                if (con == null || csType.ShowType == e_GridShowType.Control)
                    continue;
                //if (isGridPostBack == false)
                con.Attributes["row"] = i.ToString();
                con.DataBinding += new EventHandler(con_DataBinding);
                e.Row.Cells[i].Controls.Add(con);
                if (csType.IsNotNull && csType.ShowType != e_GridShowType.TextBoxSelect)
                {
                    LiteralControl lit = new LiteralControl();
                    if (csType.ReadOnly)
                        lit.Text = "<font color=\"Gray\">*</font>";
                    else
                        lit.Text = "<font color=\"red\">*</font>";
                    e.Row.Cells[i].Controls.Add(lit);
                }
            }
        }

        private void con_DataBinding(object sender, EventArgs e)
        {
            Control con = sender as Control;
            if (con != null)
            {
                WebControl webCon = con as WebControl;
                style = columns[Convert.ToInt32(webCon.Attributes["row"])];
                webCon.Attributes.Remove("row");
                GridViewRow container = con.NamingContainer as GridViewRow;
                if (container != null)
                {
                    switch (style.ShowType)
                    {
                        case e_GridShowType.TextBox:
                            SetTextBox(con, container.DataItem);
                            break;
                        case e_GridShowType.DropDownList:
                            SetDropDownList(con, container.DataItem);
                            break;
                        case e_GridShowType.CheckBox:
                            SetCheckBox(con, container.DataItem);
                            break;
                        case e_GridShowType.RadioButton:
                            SetRadioButton(con, container.DataItem);
                            break;
                        case e_GridShowType.Button:
                            SetButton(con, container.DataItem, container);
                            break;
                        case e_GridShowType.LinkButton:
                            SetLinkButton(con, container.DataItem, container);
                            break;
                        case e_GridShowType.TextBoxSelect:
                            SetTextBoxSelect(con, container.DataItem);
                            break;
                        case e_GridShowType.HyperLink:
                            SetHyperLink(con, container.DataItem);
                            break;
                    }
                }
            }
        }

        private void SetHyperLink(Control con, object objData)
        {
            HyperLink hlk = con as HyperLink;
            if (hlk != null)
            {
                string dataValue = GetIsNullOrEmpty(DataBinder.Eval(objData, style.ColumnsName));
                if (style.Href == null)
                {
                    hlk.NavigateUrl = "javascript:void(0)";
                }
                else
                {
                    hlk.NavigateUrl = style.Href;
                }
                hlk.Text = dataValue;
                SetAttributes(hlk);
            }
        }

        private void SetTextBoxSelect(Control con, object objData)
        {
            Panel panel = con as Panel;
            string[] strNames = style.ColumnsName.Split(',');
            HtmlInputHidden hid = panel.Controls[0] as HtmlInputHidden;
            TextBox txt = panel.Controls[1] as TextBox;
            object dataValue = null;
            if (strNames.Length > 1)
            {
                dataValue = DataBinder.Eval(objData, strNames[0]);
                if ((dataValue != DBNull.Value) && (dataValue != null))
                {
                    hid.Value = dataValue.ToString();
                }
                dataValue = DataBinder.Eval(objData, strNames[1]);
                if ((dataValue != DBNull.Value) && (dataValue != null))
                {
                    if (style.ColumnsType == "N")
                        txt.Text = decimal.Round(Convert.ToDecimal(dataValue), style.ColumnsTypeRemark).ToString();
                    else if (style.ColumnsType == "D" || style.ColumnsType == "T")
                    {
                        if (!DataTypeHelper.IsDateTimeEmpty(Convert.ToDateTime(dataValue)))
                            txt.Text = dataValue.ToString();
                    }
                    else
                        txt.Text = dataValue.ToString();
                }
            }
            else
            {
                dataValue = DataBinder.Eval(objData, strNames[0]);
                if ((dataValue != DBNull.Value) && (dataValue != null))
                {
                    if (style.ColumnsType == "N")
                        txt.Text = decimal.Round(Convert.ToDecimal(dataValue), style.ColumnsTypeRemark).ToString();
                    else if (style.ColumnsType == "D" || style.ColumnsType == "T")
                    {
                        if (!DataTypeHelper.IsDateTimeEmpty(Convert.ToDateTime(dataValue)))
                            txt.Text = dataValue.ToString();
                    }
                    else
                        txt.Text = dataValue.ToString();
                }
            }
            SetAttributes(panel);
        }

        private void SetLinkButton(Control con, object objData, GridViewRow container)
        {
            LinkButton lnk = con as LinkButton;
            if (lnk != null)
            {
                lnk.CommandArgument = Convert.ToString(container.DataItemIndex);
            }
            else
            {
                if (con is Panel)
                {
                    SetAttributes(con as WebControl);
                }
            }
        }

        private void SetRadioButton(Control con, object objData)
        {
            Panel panel = con as Panel;
            RadioButton rdb1 = panel.Controls[0] as RadioButton;
            RadioButton rdb2 = panel.Controls[1] as RadioButton;
            //SetStyle(rdb1);
            //SetStyle(rdb2);
            string group = Guid.NewGuid().ToString();
            rdb1.GroupName = group;
            rdb2.GroupName = group;
            object dataValue = DataBinder.Eval(objData, style.ColumnsName);
            if ((dataValue != DBNull.Value) && (dataValue != null))
            {
                bool value = Convert.ToBoolean(dataValue);
                if (value)
                {
                    rdb1.Checked = true;
                }
                else
                {
                    rdb2.Checked = true;
                }
            }
            SetAttributes(panel);
        }

        private void SetButton(Control con, object objData, GridViewRow container)
        {
            Button btn = con as Button;
            if (btn != null)
                btn.CommandArgument = Convert.ToString(container.DataItemIndex);
            else
            {
                if (con is Panel)
                    SetAttributes(con as WebControl);
            }
        }

        private void SetCheckBox(Control con, object objData)
        {
            CheckBox chb = con as CheckBox;
            if (chb != null)
            {
                object dataValue = DataBinder.Eval(objData, style.ColumnsName);
                if ((dataValue != DBNull.Value) && (dataValue != null))
                {
                    bool value = Convert.ToBoolean(dataValue);
                    chb.Checked = value;
                }
                SetAttributes(chb);
            }
        }

        #region DropDownList

        private void SetDropDownList(Control con, object objData)
        {
            DropDownList ddlItem = con as DropDownList;
            if (ddlItem != null)
            {
                if (isEmptyDataSource)
                {
                    DropDownListReadOnlyData(ddlItem);
                }
                else
                {
                    if ((style.ColumnsName != "") && (style.ColumnsName != null))
                    {
                        object dataValue = DataBinder.Eval(objData, style.ColumnsName);
                        if ((dataValue != DBNull.Value) && (dataValue != null))
                        {
                            string value = dataValue.ToString();
                            if (style.ReadOnly)
                            {
                                DropDownListSelectData(ddlItem, value);
                            }
                            else
                            {
                                DropDownListData(ddlItem, value);
                            }
                        }
                        else if (style.ReadOnly)
                        {
                            DropDownListReadOnlyData(ddlItem);
                        }
                        else
                        {
                            DropDownListAllData(ddlItem);
                        }
                    }
                    else if (style.ReadOnly)
                    {
                        DropDownListReadOnlyData(ddlItem);
                    }
                    else
                    {
                        DropDownListAllData(ddlItem);
                    }
                }
                SetAttributes(ddlItem);
            }
        }

        private void DropDownListAllData(DropDownList ddlItem)
        {
            ListItem lst = null;
            if (style.DataSource is string[,])
            {
                string[,] strs = style.DataSource as string[,];
                for (int j = 0; j < strs.Length / 2; j++)
                {
                    lst = new ListItem(strs[j, 0], strs[j, 1]);
                    ddlItem.Items.Add(lst);
                }
            }
            else
            {
                ddlItem.DataSource = style.DataSource;
                ddlItem.DataTextField = style.DataTextField;
                ddlItem.DataValueField = style.DataValueField;
                ddlItem.DataBind();
            }
        }
        /// <summary>
        /// 只加载选择项
        /// </summary>
        /// <param name="ddlItem"></param>
        /// <param name="value"></param>
        private void DropDownListSelectData(DropDownList ddlItem, string value)
        {
            bool flag = true;
            if (style.DataSource is string[,])
            {
                string[,] strs = style.DataSource as string[,];
                for (int j = 0; j < strs.Length / 2; j++)
                {
                    if (strs[j, 1] == value)
                    {
                        flag = false;
                        ddlItem.Items.Add(new ListItem(strs[j, 0], strs[j, 1]));
                        break;
                    }
                }
            }
            else
            {
                object itemText = null;
                object itemValue = null;
                string strValue = null;
                string strText = null;
                foreach (object item in style.DataSource)
                {
                    itemValue = this.GetValue(item, style.DataValueField);
                    strValue = GetIsNullOrEmpty(itemValue);
                    if (strValue == value)
                    {
                        flag = false;
                        itemValue = this.GetValue(item, style.DataTextField);
                        strText = GetIsNullOrEmpty(itemText);
                        ddlItem.Items.Add(new ListItem(strText, strValue));
                        break;
                    }
                }
            }
            if (flag)
            {
                DropDownListReadOnlyData(ddlItem);
            }
        }
        /// <summary>
        /// 加载全部并选中选择现
        /// </summary>
        /// <param name="ddlItem"></param>
        /// <param name="value"></param>
        private void DropDownListData(DropDownList ddlItem, string value)
        {
            ListItem lst = null;
            if (style.DataSource is string[,])
            {
                string[,] strs = style.DataSource as string[,];
                for (int j = 0; j < strs.Length / 2; j++)
                {
                    lst = new ListItem(strs[j, 0], strs[j, 1]);
                    if (strs[j, 1] == value)
                        lst.Selected = true;
                    ddlItem.Items.Add(lst);
                }
            }
            else
            {
                object itemText = null;
                object itemValue = null;
                string strValue = null;
                string strText = null;
                foreach (object item in style.DataSource)
                {
                    itemText = this.GetValue(item, style.DataTextField);
                    itemValue = this.GetValue(item, style.DataValueField);
                    strText = GetIsNullOrEmpty(itemText);
                    strValue = GetIsNullOrEmpty(itemValue);
                    lst = new ListItem(strText, strValue);
                    if (strValue == value)
                        lst.Selected = true;
                    ddlItem.Items.Add(lst);
                }
            }
        }

        /// <summary>
        /// 只加载第一项
        /// </summary>
        /// <param name="ddlItem"></param>
        private void DropDownListReadOnlyData(DropDownList ddlItem)
        {
            if (style.DataSource is string[,])
            {
                string[,] strs = style.DataSource as string[,];
                if (strs.Length > 1)
                {
                    for (int j = 0; j < 1; j++)
                    {
                        ddlItem.Items.Add(new ListItem(strs[j, 0], strs[j, 1]));
                    }
                }
            }
            else
            {
                foreach (object item in style.DataSource)
                {
                    object text = this.GetValue(item, style.DataTextField);
                    object value = this.GetValue(item, style.DataValueField);
                    text = text == null ? "" : text;
                    value = value == null ? "" : value;
                    ddlItem.Items.Add(new ListItem(text.ToString(), value.ToString()));
                    break;
                }
            }
        }
        #endregion

        private void SetTextBox(Control con, object objData)
        {
            TextBox txtItem = con as TextBox;
            if (txtItem != null)
            {
                object dataValue = DataBinder.Eval(objData, style.ColumnsName);
                if ((dataValue != DBNull.Value) && (dataValue != null))
                {
                    bool isShow = true;
                    string value = "";
                    switch (style.ColumnsType)
                    {
                        case "N":
                            value = decimal.Round(Convert.ToDecimal(dataValue), Convert.ToInt32(style.ColumnsTypeRemark)).ToString();
                            string eventValue = txtItem.Attributes["onblur"];
                            string clientStr = "obj=document.getElementById('" + txtItem.ClientID + "');if(isNaN(obj.value)){alert('必须输入数字！');obj.focus();return;};";
                            txtItem.Attributes["id"] = txtItem.ClientID;
                            txtItem.Attributes["class"] = "TxtRight";
                            if (eventValue == null)
                                txtItem.Attributes.Add("onblur", clientStr);
                            else
                                txtItem.Attributes["onblur"] = clientStr + eventValue;
                            break;
                        case "D":
                            DateTime time = (DateTime)dataValue;
                            isShow = !DataTypeHelper.IsDateTimeEmpty(time);
                            value = time.ToString("yyyy-MM-dd");
                            txtItem.Attributes["onfocus"] = "calendar()";
                            break;
                        case "T":
                            time = (DateTime)dataValue;
                            isShow = !DataTypeHelper.IsDateTimeEmpty(time);
                            value = time.ToString("yyyy-MM-dd");
                            txtItem.Attributes["onfocus"] = "calendar()";
                            break;
                        default:
                            value = dataValue.ToString();
                            break;
                    }
                    //txtItem.ReadOnly = style.ReadOnly;
                    if (isShow)
                    {
                        txtItem.Text = value;
                    }
                }
                SetAttributes(txtItem);
            }
        }
        /// <summary>
        /// 通用添加属性处理方法
        /// </summary>
        /// <param name="webCon"></param>
        private void SetAttributes(WebControl webCon)
        {
            if (webCon == null) return;
            if (style.ControlWidth != null)
                webCon.Width = Unit.Parse(style.ControlWidth);

            if (this.isEmptyDataSource)
            {
                webCon.Enabled = false;
            }
            else
            {
                if (style.ReadOnly == false)
                {
                    if (style.EventClient != null)
                    {
                        if (style.ShowType == e_GridShowType.TextBoxSelect)
                        {
                            TextBoxSelectAddJS(webCon);
                        }
                        else if (style.ShowType == e_GridShowType.RadioButton)
                        {
                            RadioButtonAddJS(webCon);
                        }
                        else if (style.ShowType == e_GridShowType.HyperLink)
                        {
                            HyperLinkAddJS(webCon);
                        }
                        else
                        {
                            foreach (e_EventType evType in style.EventClient.Keys)
                            {
                                webCon.Attributes.Add(evType.ToString(), GetParamValue(style.EventClient[evType], webCon));
                            }
                        }
                    }
                    if (style.ButtonCollection != null && (style.ShowType == e_GridShowType.LinkButton || style.ShowType == e_GridShowType.Button))
                    {
                        LinkButtonOrButtonAddJS(webCon);
                    }
                    else if (style.ShowType == e_GridShowType.HyperLink)
                    {
                        HyperLinkUrlParam(webCon);
                    }
                }
                else
                {
                    webCon.Enabled = false;
                }
            }
        }

        private void HyperLinkAddJS(WebControl webCon)
        {
            HyperLink hlk = webCon as HyperLink;
            if (hlk != null)
            {
                foreach (e_EventType evType in style.EventClient.Keys)
                {
                    if (evType == e_EventType.OnClick)
                    {
                        hlk.NavigateUrl = "javascript:" + GetParamValue(style.EventClient[evType], webCon);
                    }
                    else
                    {
                        webCon.Attributes.Add(evType.ToString(), GetParamValue(style.EventClient[evType], webCon));
                    }
                }
            }
        }

        private void HyperLinkUrlParam(WebControl webCon)
        {
            HyperLink hlk = webCon as HyperLink;
            if (hlk != null)
            {
                if (style.Href != null)
                    hlk.NavigateUrl = GetParamValue(hlk.NavigateUrl, webCon);
            }
        }

        /// <summary>
        /// 处理参数
        /// </summary>
        /// <param name="strJS"></param>
        /// <returns></returns>
        private string GetParamValue(string strJS, Control con)
        {
            GridViewRow gridRow = con.NamingContainer as GridViewRow;
            if (gridRow != null)
            {
                string strValue = "";
                for (int i = 0; i < this.DataKeys[gridRow.RowIndex].Values.Count; i++)
                {
                    if (i == 0)
                        strValue = this.DataKeys[gridRow.RowIndex].Values[i].ToString();
                    else
                        strValue += "," + this.DataKeys[gridRow.RowIndex].Values[i].ToString();
                }

                if (strJS != null)
                {
                    strJS = strJS.Replace(ColumnStyle.DataKey, strValue);
                }
                if (style.DataFields != null)
                {
                    string[] dataFileds = style.DataFields.Split(',');
                    string[] dataValues = new string[dataFileds.Length];
                    DataRowView dv = gridRow.DataItem as DataRowView;
                    for (int i = 0; i < dataFileds.Length; i++)
                    {
                        dataValues[i] = GetIsNullOrEmpty(dv[dataFileds[i]]);
                    }
                    strJS = string.Format(strJS, dataValues);
                }
            }
            return strJS;
        }

        private void RadioButtonAddJS(WebControl webCon)
        {
            string[] strs = null;
            int i = 0;
            foreach (e_EventType evType in style.EventClient.Keys)
            {
                strs = style.EventClient[evType].Split('|');
                foreach (Control item in webCon.Controls)
                {
                    if (item is RadioButton)
                    {
                        if (strs[i] != "")
                        {
                            webCon = item as WebControl;
                            webCon.Attributes.Add(evType.ToString(), GetParamValue(strs[i], webCon));
                        }
                        i++;
                    }
                }
            }
        }

        private void TextBoxSelectAddJS(WebControl webCon)
        {
            string jsFunction = null;
            HtmlInputHidden hid = webCon.Controls[0] as HtmlInputHidden;
            TextBox txt = webCon.Controls[1] as TextBox;
            HtmlInputButton btn = null;
            if (style.IsNotNull)
                btn = webCon.Controls[3] as HtmlInputButton;
            else
                btn = webCon.Controls[2] as HtmlInputButton;
            foreach (e_EventType evType in style.EventClient.Keys)
            {
                jsFunction = style.EventClient[evType];
                if (jsFunction.Contains("@txtId"))
                {
                    jsFunction = jsFunction.Replace("@txtId", "" + txt.ClientID + "");
                }
                if (jsFunction.Contains("@hidId"))
                {
                    jsFunction = jsFunction.Replace("@hidId", "" + hid.ClientID + "");
                }
                btn.Attributes.Add(evType.ToString(), GetParamValue(jsFunction, webCon));
            }
            if (style.TextBoxSelectReadOnly)
            {
                //txt.Attributes["ContentEditable"] = "false";
                txt.ReadOnly = false;
            }
        }

        private void LinkButtonOrButtonAddJS(WebControl webCon)
        {
            int i = 0;
            WebControl con = null;
            foreach (Control item in webCon.Controls)
            {
                if (item is LinkButton)
                {
                    con = item as WebControl;
                    con.Attributes.Add(style.ButtonCollection[i].EventType.ToString(), GetParamValue(style.ButtonCollection[i].ClientMethod, webCon));
                    i++;
                }
                else if (item is Button)
                {
                    Button btn = item as Button;
                    btn.Attributes["class"] = style.ButtonCollection[i].CssClass;
                    con = btn;
                    con.Attributes.Add(style.ButtonCollection[i].EventType.ToString(), GetParamValue(style.ButtonCollection[i].ClientMethod, webCon));
                    i++;
                }
                else if (item is Literal)
                {
                    Literal lit = item as Literal;
                    lit.Text = GetParamValue(lit.Text, lit);
                    i++;
                }
            }
        }

        private void ColumnStyleIsNotNullJS(ColumnStyleCollection columns)
        {
            if (columns == null)
            {
                return;
            }
            List<int> listIndex = new List<int>();
            for (int i = 0; i < columns.Count; i++)
            {
                if (columns[i].IsNotNull)
                {
                    listIndex.Add(i);
                }
            }
            if (listIndex.Count > 0)
            {
                StringBuilder strJS = new StringBuilder();
                strJS.Append("<script type=\"text/javascript\">");
                strJS.Append("function __CheckGridNull(){");
                strJS.Append("var tbl=document.getElementById(\"" + this.ClientID + "\");");
                strJS.Append("for(var i=1;i<tbl.rows.length;i++){");
                string varName = "";
                foreach (int item in listIndex)
                {
                    varName = item.ToString();
                    switch (columns[item].ShowType)
                    {
                        case e_GridShowType.TextBox:
                            varName = "txt" + varName;
                            strJS.Append("var " + varName + "=tbl.rows[i].cells[" + item + "].firstChild;");
                            strJS.Append("if(" + varName + ".value==\"\"){");
                            strJS.Append("alert('" + columns[item].ColumnsDesc + "不能为空!');return false;}");
                            break;
                        case e_GridShowType.DropDownList:
                            varName = "ddl" + varName;
                            strJS.Append("var " + varName + "=tbl.rows[i].cells[" + item + "].firstChild;");
                            strJS.Append("if(" + varName + ".options[" + varName + ".selectedIndex].value=='-1'){");
                            strJS.Append("alert('" + columns[item].ColumnsDesc + "不能为空!');return false;}");
                            break;
                        case e_GridShowType.TextBoxSelect:
                            varName = "txt" + varName;
                            strJS.Append("var " + varName + "=tbl.rows[i].cells[" + item + "].firstChild.childNodes(1);");
                            strJS.Append("if(" + varName + ".value==\"\"){");
                            strJS.Append("alert('" + columns[item].ColumnsDesc + "不能为空!');return false;}");
                            break;
                    }
                }
                strJS.Append("}}");
                strJS.Append("</script>");
                this.Page.RegisterStartupScript(Guid.NewGuid().ToString(), strJS.ToString());
            }
        }

        #endregion

        #region 加载行列

        private void InitColumns()
        {
            this.Columns.Clear();

            if (EnableCheckAll)
            {
                TemplateField _CheckAllTempFile = new TemplateField();
                _CheckAllTempFile.HeaderText = "<input type=\"checkbox\" id=\"ChbCheckAll\" onclick=\"CheckAll(this,'" + this.ID + "')\" name=\"ChbCheckAll\" value=\"1\" />";
                _CheckAllTempFile.HeaderStyle.Width = Unit.Percentage(Convert.ToDouble(3)); ;
                this.Columns.Add(_CheckAllTempFile);
            }

            if (columns.Any(p => p.Sorting))
            {
                this.AllowSorting = true;
            }
            if (this.readOnly)
            {
                foreach (ColumnStyle item in columns)
                {
                    InitShowTypeColumns(item);            //添加普通列
                }
            }
            else
            {
                foreach (ColumnStyle item in columns)
                {
                    if (item.ShowType == e_GridShowType.Literal)
                    {
                        InitShowTypeColumns(item);            //添加普通列
                    }
                    else
                    {
                        InitShowTypeTemplate(item);            //添加模板列
                    }
                }
            }
        }

        private void InitShowTypeColumns(ColumnStyle csType)
        {
            DataControlField dcf = null;

            BoundField field = new BoundField();
            if (this.readOnly)
            {
                if (csType.ShowType == e_GridShowType.TextBoxSelect)
                {
                    string[] fields = csType.ColumnsName.Split(',');
                    if (fields.Length > 1)
                        field.DataField = fields[1];
                    else
                        field.DataField = csType.ColumnsName;
                }
                else if (csType.ShowType == e_GridShowType.DropDownList)
                {
                    field.DataField = csType.DataTextField;
                }
                else
                {
                    field.DataField = csType.ColumnsName;
                }
            }
            else
                field.DataField = csType.ColumnsName;
            field.HeaderText = csType.ColumnsDesc;
            field.HeaderStyle.Width = Unit.Parse(csType.ColumnsWidth);

            if (csType.ColumnsType == "N")
            {
                field.DataFormatString = "{0:F" + csType.ColumnsTypeRemark + "}";
            }

            dcf = field;
            if (csType.Sorting)
                dcf.SortExpression = csType.ColumnsName;
            dcf.HeaderText = csType.ColumnsDesc;
            this.Columns.Add(dcf);
        }
        private void InitShowTypeTemplate(ColumnStyle csType)
        {
            TemplateField tempField = new TemplateField();
            tempField.HeaderStyle.Width = Unit.Parse(csType.ColumnsWidth);
            this.Columns.Add(tempField);
        }

        #endregion

        #region 排序

        protected override void OnSorting(GridViewSortEventArgs e)
        {
            Dictionary<string, SortDirection> dicSort = GetSortDirection;
            if (GetSortDirection == null)
            {
                dicSort = new Dictionary<string, SortDirection>();
                string orderByFiled = OrderByFiled.Trim();
                string[] sort = orderByFiled.Split(' ');
                if (orderByFiled.Contains(',') == false && sort[0].ToLower() == e.SortExpression.ToLower() && sort[sort.Length - 1].ToLower() != "desc")
                {
                    dicSort.Add(e.SortExpression, SortDirection.Ascending);
                    OrderByFiled = e.SortExpression + " desc";
                }
                else
                {
                    dicSort.Add(e.SortExpression, SortDirection.Descending);
                    OrderByFiled = e.SortExpression;
                }
            }
            else
            {
                KeyValuePair<string, SortDirection> kvp = dicSort.FirstOrDefault(
                    p => p.Key == e.SortExpression
                    );
                if (kvp.Key == null)
                {
                    OrderByFiled = e.SortExpression;
                    dicSort.Add(e.SortExpression, SortDirection.Descending);
                }
                else if (SortDirection.Ascending == kvp.Value)
                {
                    OrderByFiled = e.SortExpression;
                    dicSort[e.SortExpression] = SortDirection.Descending;
                }
                else
                {
                    OrderByFiled = e.SortExpression + " desc";
                    dicSort[e.SortExpression] = SortDirection.Ascending;
                }
            }
            GetSortDirection = dicSort;
            this.QueryBind();
        }

        private Dictionary<string, SortDirection> GetSortDirection
        {
            get
            {
                return ViewState["GetSortDirection"] == null ? null : ViewState["GetSortDirection"] as Dictionary<string, SortDirection>;
            }
            set
            {
                ViewState["GetSortDirection"] = value;
            }
        }
        #endregion

        #region 其他

        protected override void OnRowCommand(GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Page" || e.CommandName == "Sort" || e.CommandName == "")
                return;
            base.OnRowCommand(e);
        }

        protected override void OnPreRender(EventArgs e)
        {
            ColumnStyleIsNotNullJS(columns);
        }
        /// <summary>
        /// 默认时间不显示 修改wenbing 2010-11-15
        /// </summary>
        private void DataTimeDefault()
        {
            List<int> listIndexD = new List<int>();
            List<int> listIndexW = new List<int>(); //格式化为万的数据
            for (int i = 0; i < columns.Count; i++)
            {
                if ((columns[i].ColumnsType == "D") && (columns[i].ShowType == e_GridShowType.Literal))
                    listIndexD.Add(i);
                if ((columns[i].ColumnsType == "W") && (columns[i].ShowType == e_GridShowType.Literal))
                    listIndexW.Add(i);
            }
            if (listIndexD.Count > 0 || listIndexW.Count>0)
            {
                foreach (GridViewRow item in this.Rows)
                {
                    //日期格式
                    foreach (int i in listIndexD)
                    {
                        if (item.Cells[i].Text != "&nbsp;" && item.Cells[i].Text != "")
                        {
                            DateTime time = DateTime.Parse(item.Cells[i].Text);
                            if (DataTypeHelper.IsDateTimeEmpty(time))
                            {
                                item.Cells[i].Text = "";
                            }
                            else
                            {
                                item.Cells[i].Text = time.ToString(DataFormatter.c_Date);
                            }
                        }
                    }
                    //格式化为万元
                    foreach (int i in listIndexW)
                    {
                        if (item.Cells[i].Text != "&nbsp;" && item.Cells[i].Text != "")
                        {
                            item.Cells[i].Text = string.Format("{0:F2}", CConvert.StringToDecimal(item.Cells[i].Text) / 10000);                           
                        }
                    }
                }
            }
        }

        #endregion

        #region 合并列和分组行

        /// <summary>
        /// 根据条件列合并GridView列中相同的行
        /// </summary>
        /// <param name="mergeCellIndex">分组的索引</param>
        /// <param name="sameCellIndex">根据分组合并的索引</param>
        public void GroupRows(int[] mergeCellIndex, int[] sameCellIndex)
        {
            int i = 0, rowSpanNum = 1;
            string[] testStr = new string[] { "", "" };
            while (i < this.Rows.Count - 1)
            {
                GridViewRow gvr = this.Rows[i];
                for (++i; i < this.Rows.Count; i++)
                {
                    GridViewRow gvrNext = this.Rows[i];
                    //初始化判断字符串
                    testStr[0] = "";
                    testStr[1] = "";
                    for (int k = 0; k < sameCellIndex.Length; k++)
                    {
                        testStr[0] += gvr.Cells[sameCellIndex[k]].Text.ToString();
                        testStr[1] += gvrNext.Cells[sameCellIndex[k]].Text.ToString();
                    }
                    if (testStr[0] == testStr[1])
                    {
                        for (int x = 0; x < mergeCellIndex.Length; x++)
                        {
                            gvrNext.Cells[mergeCellIndex[x]].Visible = false;
                        }
                        rowSpanNum++;
                    }
                    else
                    {
                        for (int x = 0; x < mergeCellIndex.Length; x++)
                        {
                            gvr.Cells[mergeCellIndex[x]].RowSpan = rowSpanNum;
                        }
                        rowSpanNum = 1;
                        break;
                    }

                    if (i == this.Rows.Count - 1)
                    {
                        for (int x = 0; x < mergeCellIndex.Length; x++)
                        {
                            gvr.Cells[mergeCellIndex[x]].RowSpan = rowSpanNum;
                        }
                    }
                }
            }
        }

        /// <summary>
        ///  跨列多从表头
        ///  2009-08-02
        /// </summary>
        /// <param name="e"></param>
        /// <param name="topHead">表头top名称</param>
        /// <param name="startColumns">开始行</param>
        /// <param name="endColumns">结束行</param>
        public void SetHead(GridViewRowEventArgs e, string topHead, int startColumns, int endColumns)
        {
            switch (e.Row.RowType)
            {
                case DataControlRowType.Header:
                    TableCellCollection tcHeader = e.Row.Cells;
                    bool flag = true;                                                           //这个GridView是否是第一次调用这个方法如果是酒true
                    string[] str = new string[endColumns - startColumns + 1];                   //列名
                    int j = 0;
                    int startIndex = 0;                                                         //前面的
                    string rowStyle = "class=\"trHead\"";
                    for (int i = startColumns; i <= endColumns; i++)                            //把列名保存
                    {
                        string[] strs = tcHeader[i].Text.Split(new string[] { "</th></tr><tr " + rowStyle + ">" }, StringSplitOptions.RemoveEmptyEntries);
                        if (strs.Length > 1)
                        {
                            str[j] = strs[0];
                            startIndex = i;
                        }
                        else
                        {
                            str[j] = tcHeader[i].Text;
                        }
                        str[j] += "," + tcHeader[i].Attributes["FieldName"];
                        if (i != startColumns)
                            tcHeader[i].Visible = false;                                        //隐藏不显示第二行的部分标题
                        j++;
                    }
                    int index = 0;                                                              //没有隐藏列计算最大下标
                    for (int i = 0; i < tcHeader.Count; i++)
                    {
                        if (tcHeader[i].ColumnSpan > 0)
                        {
                            flag = false;
                        }
                        if (i != startColumns && i != endColumns && tcHeader[i].ColumnSpan == 0 && tcHeader[i].Visible)
                        {
                            tcHeader[i].RowSpan = 2;
                        }
                        if (tcHeader[i].Visible)
                        {
                            index = i;
                        }
                    }
                    tcHeader[startColumns].ColumnSpan = endColumns - startColumns + 1;
                    tcHeader[startColumns].RowSpan = 0;
                    tcHeader[startColumns].Text = topHead;
                    tcHeader[startColumns].Attributes.Remove("FieldName");
                    if (startIndex == 0)
                    {
                        startIndex = index;
                    }
                    string before = tcHeader[startIndex].Text;
                    if (startIndex == endColumns)
                    {
                        string[] s = before.Split(new string[] { "</th></tr><tr " + rowStyle + ">" }, StringSplitOptions.None);
                        System.Text.StringBuilder sb1 = new System.Text.StringBuilder();
                        sb1.Append(topHead);
                        sb1.Append("</th></tr><tr " + rowStyle + ">");
                        sb1.Append(s[1]);
                        before = sb1.ToString();
                    }
                    System.Text.StringBuilder sb = new System.Text.StringBuilder();
                    sb.Append(before + "</th>");
                    if (flag)
                    {
                        sb.Append("</tr><tr " + rowStyle + ">");
                    }
                    for (int i = 0; i < str.Length; i++)
                    {
                        string[] strs = str[i].Split(',');
                        sb.Append("<th FieldName=\"" + strs[1] + "\" nowrap=\"nowrap\">" + strs[0] + "</th>");
                    }
                    tcHeader[index].Text = sb.ToString().Remove(sb.ToString().Length - 5);
                    break;
            }
        }

        #endregion

        #region 辅助通用方法

        private object GetValue(object obj, string column)
        {
            PropertyInfo p = obj.GetType().GetProperty(column);
            object objValue = null;
            if (p != null)
            {
                objValue = p.GetValue(obj, null);
            }
            return objValue;
        }
        private object GetValue(Type type, object obj, string column)
        {
            FieldInfo fi = type.GetField(column, BindingFlags.NonPublic | BindingFlags.Static);
            object objValue = null;
            if (fi != null)
            {
                objValue = fi.GetValue(obj);
            }
            return objValue;
        }
        private string GetIsNullOrEmpty(object obj)
        {
            if (obj == DBNull.Value || obj == null)
                return "";
            return obj.ToString();
        }
        private bool IfNumber(string Text)
        {
            bool flag = true;
            string pattern = @"^\d+(\.\d)?$";
            if (Text.Trim() != "")
            {
                if (!System.Text.RegularExpressions.Regex.IsMatch(Text.Trim(), pattern))
                {
                    flag = false;
                }
                else
                {
                    flag = true;
                }
            }
            return flag;
        }
        /// <summary>
        /// 根据字段获取列下标 没找到改字段返回 -1
        /// </summary>
        /// <param name="field">字段名</param>
        /// <returns>返回下标</returns>
        public int IndexOf(string field)
        {
            int i = 0;
            bool flag = false;
            for (; i < columns.Count; i++)
            {
                if (columns[i].ColumnsName == field)
                {
                    flag = true;
                    break;
                }
            }
            if (flag)
                return i;
            return -1;
        }
        #endregion

        #region 获取全选值

        /// <summary>
        /// 获取全选第一列的主键值
        /// </summary>
        /// <returns></returns>
        public List<string> FirstCheckAllKey()
        {
            int i = 0;
            for (; i < columns.Count; i++)
            {
                if (columns[i].IsCheckAll)
                    break;
            }
            return GetCheckAllKeyByIndex(i);
        }

        public List<string> GetCheckAllKeyByIndex(int index)
        {
            CheckBox chb = null;
            List<string> lst = new List<string>();
            for (int i = 0; i < this.Rows.Count; i++)
            {
                chb = this.Rows[i].Cells[index].Controls[0] as CheckBox;
                if (chb != null && chb.Checked)
                {
                    lst.Add(this.DataKeys[i].Value.ToString());
                }
            }
            return lst;
        }
        #endregion

        /// <summary>
        /// 主要作用就是在记录没有那么多的时候用空白行填充，防止页面走形
        /// wenbing 
        /// 2010-11-17
        /// </summary>
        /// <param name="pageSize"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        private DataTable FillWithBlank(int pageSize, DataTable dt)
        {
            if (dt.Rows.Count < pageSize)
            {
                for (int i = dt.Rows.Count - 1; i < pageSize; i++)
                {
                    DataRow dr = dt.NewRow(); dt.Rows.Add(dr);
                }
            }
            return dt;
        }
    }
}
