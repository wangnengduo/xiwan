// [实体版本]v2.7
// 2018-06-14 14:07:08
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DAL;

namespace XiWan.Entities
{
    [Serializable]
    public class RoomClass : EntityBase
    {

        #region 初始化
        public RoomClass() : base()
        {
        }
    
        public RoomClass(e_EntityState state) : base(state)
        {
        }
    
        static RoomClass()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("RoomClass");
    
            DataColumn dclClassName = new DataColumn(c_ClassName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclClassName);
            dclClassName.Caption = "房间类型名称";
            dclClassName.AllowDBNull = true;
            dclClassName.MaxLength = 100;
    
            DataColumn dclCreateTime = new DataColumn(c_CreateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreateTime);
            dclCreateTime.Caption = "创建时间";
            dclCreateTime.AllowDBNull = true;
    
            DataColumn dclCurrencyCode = new DataColumn(c_CurrencyCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCurrencyCode);
            dclCurrencyCode.Caption = "货币种类";
            dclCurrencyCode.AllowDBNull = true;
            dclCurrencyCode.MaxLength = 100;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "酒店ID";
            dclHotelID.AllowDBNull = true;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "自增主键";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsClose = new DataColumn(c_IsClose,typeof(System.Boolean));
            m_dtbEntitySchema.Columns.Add(dclIsClose);
            dclIsClose.Caption = "是否已作废";
            dclIsClose.AllowDBNull = true;
    
            DataColumn dclIsOptional = new DataColumn(c_IsOptional,typeof(System.Boolean));
            m_dtbEntitySchema.Columns.Add(dclIsOptional);
            dclIsOptional.Caption = "是否可预订";
            dclIsOptional.AllowDBNull = true;
    
            DataColumn dclIsPromotion = new DataColumn(c_IsPromotion,typeof(System.Boolean));
            m_dtbEntitySchema.Columns.Add(dclIsPromotion);
            dclIsPromotion.Caption = "是否搞活动";
            dclIsPromotion.AllowDBNull = true;
    
            DataColumn dclStatus = new DataColumn(c_Status,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStatus);
            dclStatus.Caption = "状态";
            dclStatus.AllowDBNull = true;
            dclStatus.MaxLength = 100;
    
            DataColumn dclSupplier = new DataColumn(c_Supplier,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclSupplier);
            dclSupplier.Caption = "供应商";
            dclSupplier.AllowDBNull = true;
            dclSupplier.MaxLength = 200;
    
            DataColumn dclSupplierCode = new DataColumn(c_SupplierCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclSupplierCode);
            dclSupplierCode.Caption = "供应商code";
            dclSupplierCode.AllowDBNull = true;
            dclSupplierCode.MaxLength = 100;
    
            DataColumn dclTotalPrice = new DataColumn(c_TotalPrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclTotalPrice);
            dclTotalPrice.Caption = "价格";
            dclTotalPrice.AllowDBNull = true;
    
            DataColumn dclTotalRoom = new DataColumn(c_TotalRoom,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclTotalRoom);
            dclTotalRoom.Caption = "可用房间数";
            dclTotalRoom.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(RoomClass);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "ClassName,CreateTime,CurrencyCode,HotelID,ID,IsClose,IsOptional,IsPromotion,Status,Supplier,SupplierCode,TotalPrice,TotalRoom";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_ClassName = "ClassName";
        public const string c_CreateTime = "CreateTime";
        public const string c_CurrencyCode = "CurrencyCode";
        public const string c_HotelID = "HotelID";
        public const string c_ID = "ID";
        public const string c_IsClose = "IsClose";
        public const string c_IsOptional = "IsOptional";
        public const string c_IsPromotion = "IsPromotion";
        public const string c_Status = "Status";
        public const string c_Supplier = "Supplier";
        public const string c_SupplierCode = "SupplierCode";
        public const string c_TotalPrice = "TotalPrice";
        public const string c_TotalRoom = "TotalRoom";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _className = string.Empty;
    
        /// <summary>
        /// 房间类型名称[字段]
        /// </summary>
        public System.String ClassName
        {
            get
            {
                return _className;
            }
            set
            {
                AddOriginal("ClassName", _className, value);
                _className = value;
            }
        }
    
        private System.DateTime _createTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 创建时间[字段]
        /// </summary>
        public System.DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                AddOriginal("CreateTime", _createTime, value);
                _createTime = value;
            }
        }
    
        private System.String _currencyCode = string.Empty;
    
        /// <summary>
        /// 货币种类[字段]
        /// </summary>
        public System.String CurrencyCode
        {
            get
            {
                return _currencyCode;
            }
            set
            {
                AddOriginal("CurrencyCode", _currencyCode, value);
                _currencyCode = value;
            }
        }
    
        private System.Int32 _hotelID = 0;
    
        /// <summary>
        /// 酒店ID[字段]
        /// </summary>
        public System.Int32 HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// 自增主键[字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Boolean _isClose = false;
    
        /// <summary>
        /// 是否已作废[字段]
        /// </summary>
        public System.Boolean IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                AddOriginal("IsClose", _isClose, value);
                _isClose = value;
            }
        }
    
        private System.Boolean _isOptional = false;
    
        /// <summary>
        /// 是否可预订[字段]
        /// </summary>
        public System.Boolean IsOptional
        {
            get
            {
                return _isOptional;
            }
            set
            {
                AddOriginal("IsOptional", _isOptional, value);
                _isOptional = value;
            }
        }
    
        private System.Boolean _isPromotion = false;
    
        /// <summary>
        /// 是否搞活动[字段]
        /// </summary>
        public System.Boolean IsPromotion
        {
            get
            {
                return _isPromotion;
            }
            set
            {
                AddOriginal("IsPromotion", _isPromotion, value);
                _isPromotion = value;
            }
        }
    
        private System.String _status = string.Empty;
    
        /// <summary>
        /// 状态[字段]
        /// </summary>
        public System.String Status
        {
            get
            {
                return _status;
            }
            set
            {
                AddOriginal("Status", _status, value);
                _status = value;
            }
        }
    
        private System.String _supplier = string.Empty;
    
        /// <summary>
        /// 供应商[字段]
        /// </summary>
        public System.String Supplier
        {
            get
            {
                return _supplier;
            }
            set
            {
                AddOriginal("Supplier", _supplier, value);
                _supplier = value;
            }
        }
    
        private System.String _supplierCode = string.Empty;
    
        /// <summary>
        /// 供应商code[字段]
        /// </summary>
        public System.String SupplierCode
        {
            get
            {
                return _supplierCode;
            }
            set
            {
                AddOriginal("SupplierCode", _supplierCode, value);
                _supplierCode = value;
            }
        }
    
        private System.Decimal _totalPrice = 0.0m;
    
        /// <summary>
        /// 价格[字段]
        /// </summary>
        public System.Decimal TotalPrice
        {
            get
            {
                return _totalPrice;
            }
            set
            {
                AddOriginal("TotalPrice", _totalPrice, value);
                _totalPrice = value;
            }
        }
    
        private System.Int32 _totalRoom = 0;
    
        /// <summary>
        /// 可用房间数[字段]
        /// </summary>
        public System.Int32 TotalRoom
        {
            get
            {
                return _totalRoom;
            }
            set
            {
                AddOriginal("TotalRoom", _totalRoom, value);
                _totalRoom = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static RoomClass DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as RoomClass;
        }
    
        public RoomClass Clone()
        {
            return EntityBase.Clone(this) as RoomClass;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class RoomClassCollection : EntityCollectionBase
    {

        public RoomClassCollection()
        {
        }

        private static Type m_typ = typeof(RoomClassCollection);
        private static Type m_typItem = typeof(RoomClass);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public RoomClass this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (RoomClass)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public RoomClass this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new RoomClass GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as RoomClass;
        }

        public int Add(RoomClass value)
        {
            return List.Add(value);
        }

        public void Insert(int index, RoomClass value)
        {
            List.Insert(index, value);
        }

        public void Remove(RoomClass value)
        {
            List.Remove(value);
        }

        public void Delete(RoomClass value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new RoomClassCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(RoomClass value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(RoomClass value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class RoomClassQuery : XiWan.DAL.EntityQueryBase
    {

        public RoomClassQuery()
        {
            m_strTableName = "RoomClass";

            m_prmClassName.SourceColumn = "ClassName";
            this.Parameters.Add(m_prmClassName);

            m_prmCreateTime.SourceColumn = "CreateTime";
            this.Parameters.Add(m_prmCreateTime);

            m_prmCurrencyCode.SourceColumn = "CurrencyCode";
            this.Parameters.Add(m_prmCurrencyCode);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsClose.SourceColumn = "IsClose";
            this.Parameters.Add(m_prmIsClose);

            m_prmIsOptional.SourceColumn = "IsOptional";
            this.Parameters.Add(m_prmIsOptional);

            m_prmIsPromotion.SourceColumn = "IsPromotion";
            this.Parameters.Add(m_prmIsPromotion);

            m_prmStatus.SourceColumn = "Status";
            this.Parameters.Add(m_prmStatus);

            m_prmSupplier.SourceColumn = "Supplier";
            this.Parameters.Add(m_prmSupplier);

            m_prmSupplierCode.SourceColumn = "SupplierCode";
            this.Parameters.Add(m_prmSupplierCode);

            m_prmTotalPrice.SourceColumn = "TotalPrice";
            this.Parameters.Add(m_prmTotalPrice);

            m_prmTotalRoom.SourceColumn = "TotalRoom";
            this.Parameters.Add(m_prmTotalRoom);
        }
        private IDataParameter m_prmClassName= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["ClassName"]);
        public IDataParameter ClassName
        {
            get { return m_prmClassName; }
            set { m_prmClassName = value; }
        }

        private IDataParameter m_prmCreateTime= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["CreateTime"]);
        public IDataParameter CreateTime
        {
            get { return m_prmCreateTime; }
            set { m_prmCreateTime = value; }
        }

        private IDataParameter m_prmCurrencyCode= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["CurrencyCode"]);
        public IDataParameter CurrencyCode
        {
            get { return m_prmCurrencyCode; }
            set { m_prmCurrencyCode = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsClose= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["IsClose"]);
        public IDataParameter IsClose
        {
            get { return m_prmIsClose; }
            set { m_prmIsClose = value; }
        }

        private IDataParameter m_prmIsOptional= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["IsOptional"]);
        public IDataParameter IsOptional
        {
            get { return m_prmIsOptional; }
            set { m_prmIsOptional = value; }
        }

        private IDataParameter m_prmIsPromotion= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["IsPromotion"]);
        public IDataParameter IsPromotion
        {
            get { return m_prmIsPromotion; }
            set { m_prmIsPromotion = value; }
        }

        private IDataParameter m_prmStatus= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["Status"]);
        public IDataParameter Status
        {
            get { return m_prmStatus; }
            set { m_prmStatus = value; }
        }

        private IDataParameter m_prmSupplier= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["Supplier"]);
        public IDataParameter Supplier
        {
            get { return m_prmSupplier; }
            set { m_prmSupplier = value; }
        }

        private IDataParameter m_prmSupplierCode= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["SupplierCode"]);
        public IDataParameter SupplierCode
        {
            get { return m_prmSupplierCode; }
            set { m_prmSupplierCode = value; }
        }

        private IDataParameter m_prmTotalPrice= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["TotalPrice"]);
        public IDataParameter TotalPrice
        {
            get { return m_prmTotalPrice; }
            set { m_prmTotalPrice = value; }
        }

        private IDataParameter m_prmTotalRoom= EntityBase.NewParameter(RoomClass.m_dtbEntitySchema.Columns["TotalRoom"]);
        public IDataParameter TotalRoom
        {
            get { return m_prmTotalRoom; }
            set { m_prmTotalRoom = value; }
        }

    }
    #endregion
}