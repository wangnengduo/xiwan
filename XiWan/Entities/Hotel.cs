// [实体版本]v2.7
// 2018-06-14 14:06:22
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DAL;

namespace XiWan.Entities
{
    [Serializable]
    public class Hotel : EntityBase
    {

        #region 初始化
        public Hotel() : base()
        {
        }
    
        public Hotel(e_EntityState state) : base(state)
        {
        }
    
        static Hotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("Hotel");
    
            DataColumn dclAddress = new DataColumn(c_Address,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress);
            dclAddress.Caption = "酒店地址";
            dclAddress.AllowDBNull = true;
            dclAddress.MaxLength = 500;
    
            DataColumn dclAddress2 = new DataColumn(c_Address2,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress2);
            dclAddress2.Caption = "酒店地址2";
            dclAddress2.AllowDBNull = true;
            dclAddress2.MaxLength = 500;
    
            DataColumn dclCheckInDate = new DataColumn(c_CheckInDate,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckInDate);
            dclCheckInDate.Caption = "入住时间";
            dclCheckInDate.AllowDBNull = true;
    
            DataColumn dclCheckOutDate = new DataColumn(c_CheckOutDate,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckOutDate);
            dclCheckOutDate.Caption = "退房时间";
            dclCheckOutDate.AllowDBNull = true;
    
            DataColumn dclCityID = new DataColumn(c_CityID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclCityID);
            dclCityID.Caption = "城市ID";
            dclCityID.AllowDBNull = true;
    
            DataColumn dclCityName = new DataColumn(c_CityName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityName);
            dclCityName.Caption = "城市名称";
            dclCityName.AllowDBNull = true;
            dclCityName.MaxLength = 300;
    
            DataColumn dclCitySearchID = new DataColumn(c_CitySearchID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclCitySearchID);
            dclCitySearchID.Caption = "城市查询ID";
            dclCitySearchID.AllowDBNull = true;
    
            DataColumn dclCountryID = new DataColumn(c_CountryID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryID);
            dclCountryID.Caption = "国家code";
            dclCountryID.AllowDBNull = true;
            dclCountryID.MaxLength = 100;
    
            DataColumn dclCountryName = new DataColumn(c_CountryName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryName);
            dclCountryName.Caption = "国家名称";
            dclCountryName.AllowDBNull = true;
            dclCountryName.MaxLength = 300;
    
            DataColumn dclCreateTime = new DataColumn(c_CreateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreateTime);
            dclCreateTime.Caption = "创建时间";
            dclCreateTime.AllowDBNull = true;
    
            DataColumn dclEmail = new DataColumn(c_Email,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclEmail);
            dclEmail.Caption = "邮箱";
            dclEmail.AllowDBNull = true;
            dclEmail.MaxLength = 100;
    
            DataColumn dclFax = new DataColumn(c_Fax,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclFax);
            dclFax.Caption = "传真";
            dclFax.AllowDBNull = true;
            dclFax.MaxLength = 50;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "接口返回酒店ID";
            dclHotelID.AllowDBNull = true;
    
            DataColumn dclHotelName = new DataColumn(c_HotelName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelName);
            dclHotelName.Caption = "酒店名称";
            dclHotelName.AllowDBNull = true;
            dclHotelName.MaxLength = 100;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "自增主键";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsClose = new DataColumn(c_IsClose,typeof(System.Boolean));
            m_dtbEntitySchema.Columns.Add(dclIsClose);
            dclIsClose.Caption = "是否已作废";
            dclIsClose.AllowDBNull = true;
    
            DataColumn dclStarRating = new DataColumn(c_StarRating,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclStarRating);
            dclStarRating.Caption = "酒店评级";
            dclStarRating.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(Hotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Address,Address2,CheckInDate,CheckOutDate,CityID,CityName,CitySearchID,CountryID,CountryName,CreateTime,Email,Fax,HotelID,HotelName,ID,IsClose,StarRating";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Address = "Address";
        public const string c_Address2 = "Address2";
        public const string c_CheckInDate = "CheckInDate";
        public const string c_CheckOutDate = "CheckOutDate";
        public const string c_CityID = "CityID";
        public const string c_CityName = "CityName";
        public const string c_CitySearchID = "CitySearchID";
        public const string c_CountryID = "CountryID";
        public const string c_CountryName = "CountryName";
        public const string c_CreateTime = "CreateTime";
        public const string c_Email = "Email";
        public const string c_Fax = "Fax";
        public const string c_HotelID = "HotelID";
        public const string c_HotelName = "HotelName";
        public const string c_ID = "ID";
        public const string c_IsClose = "IsClose";
        public const string c_StarRating = "StarRating";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _address = string.Empty;
    
        /// <summary>
        /// 酒店地址[字段]
        /// </summary>
        public System.String Address
        {
            get
            {
                return _address;
            }
            set
            {
                AddOriginal("Address", _address, value);
                _address = value;
            }
        }
    
        private System.String _address2 = string.Empty;
    
        /// <summary>
        /// 酒店地址2[字段]
        /// </summary>
        public System.String Address2
        {
            get
            {
                return _address2;
            }
            set
            {
                AddOriginal("Address2", _address2, value);
                _address2 = value;
            }
        }
    
        private System.DateTime _checkInDate = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 入住时间[字段]
        /// </summary>
        public System.DateTime CheckInDate
        {
            get
            {
                return _checkInDate;
            }
            set
            {
                AddOriginal("CheckInDate", _checkInDate, value);
                _checkInDate = value;
            }
        }
    
        private System.DateTime _checkOutDate = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 退房时间[字段]
        /// </summary>
        public System.DateTime CheckOutDate
        {
            get
            {
                return _checkOutDate;
            }
            set
            {
                AddOriginal("CheckOutDate", _checkOutDate, value);
                _checkOutDate = value;
            }
        }
    
        private System.Int32 _cityID = 0;
    
        /// <summary>
        /// 城市ID[字段]
        /// </summary>
        public System.Int32 CityID
        {
            get
            {
                return _cityID;
            }
            set
            {
                AddOriginal("CityID", _cityID, value);
                _cityID = value;
            }
        }
    
        private System.String _cityName = string.Empty;
    
        /// <summary>
        /// 城市名称[字段]
        /// </summary>
        public System.String CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                AddOriginal("CityName", _cityName, value);
                _cityName = value;
            }
        }
    
        private System.Int32 _citySearchID = 0;
    
        /// <summary>
        /// 城市查询ID[字段]
        /// </summary>
        public System.Int32 CitySearchID
        {
            get
            {
                return _citySearchID;
            }
            set
            {
                AddOriginal("CitySearchID", _citySearchID, value);
                _citySearchID = value;
            }
        }
    
        private System.String _countryID = string.Empty;
    
        /// <summary>
        /// 国家code[字段]
        /// </summary>
        public System.String CountryID
        {
            get
            {
                return _countryID;
            }
            set
            {
                AddOriginal("CountryID", _countryID, value);
                _countryID = value;
            }
        }
    
        private System.String _countryName = string.Empty;
    
        /// <summary>
        /// 国家名称[字段]
        /// </summary>
        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                AddOriginal("CountryName", _countryName, value);
                _countryName = value;
            }
        }
    
        private System.DateTime _createTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 创建时间[字段]
        /// </summary>
        public System.DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                AddOriginal("CreateTime", _createTime, value);
                _createTime = value;
            }
        }
    
        private System.String _email = string.Empty;
    
        /// <summary>
        /// 邮箱[字段]
        /// </summary>
        public System.String Email
        {
            get
            {
                return _email;
            }
            set
            {
                AddOriginal("Email", _email, value);
                _email = value;
            }
        }
    
        private System.String _fax = string.Empty;
    
        /// <summary>
        /// 传真[字段]
        /// </summary>
        public System.String Fax
        {
            get
            {
                return _fax;
            }
            set
            {
                AddOriginal("Fax", _fax, value);
                _fax = value;
            }
        }
    
        private System.Int32 _hotelID = 0;
    
        /// <summary>
        /// 接口返回酒店ID[字段]
        /// </summary>
        public System.Int32 HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.String _hotelName = string.Empty;
    
        /// <summary>
        /// 酒店名称[字段]
        /// </summary>
        public System.String HotelName
        {
            get
            {
                return _hotelName;
            }
            set
            {
                AddOriginal("HotelName", _hotelName, value);
                _hotelName = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// 自增主键[字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Boolean _isClose = false;
    
        /// <summary>
        /// 是否已作废[字段]
        /// </summary>
        public System.Boolean IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                AddOriginal("IsClose", _isClose, value);
                _isClose = value;
            }
        }
    
        private System.Int32 _starRating = 0;
    
        /// <summary>
        /// 酒店评级[字段]
        /// </summary>
        public System.Int32 StarRating
        {
            get
            {
                return _starRating;
            }
            set
            {
                AddOriginal("StarRating", _starRating, value);
                _starRating = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static Hotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as Hotel;
        }
    
        public Hotel Clone()
        {
            return EntityBase.Clone(this) as Hotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class HotelCollection : EntityCollectionBase
    {

        public HotelCollection()
        {
        }

        private static Type m_typ = typeof(HotelCollection);
        private static Type m_typItem = typeof(Hotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public Hotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (Hotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public Hotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new Hotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as Hotel;
        }

        public int Add(Hotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, Hotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(Hotel value)
        {
            List.Remove(value);
        }

        public void Delete(Hotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new HotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(Hotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(Hotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class HotelQuery : XiWan.DAL.EntityQueryBase
    {

        public HotelQuery()
        {
            m_strTableName = "Hotel";

            m_prmAddress.SourceColumn = "Address";
            this.Parameters.Add(m_prmAddress);

            m_prmAddress2.SourceColumn = "Address2";
            this.Parameters.Add(m_prmAddress2);

            m_prmCheckInDate.SourceColumn = "CheckInDate";
            this.Parameters.Add(m_prmCheckInDate);

            m_prmCheckOutDate.SourceColumn = "CheckOutDate";
            this.Parameters.Add(m_prmCheckOutDate);

            m_prmCityID.SourceColumn = "CityID";
            this.Parameters.Add(m_prmCityID);

            m_prmCityName.SourceColumn = "CityName";
            this.Parameters.Add(m_prmCityName);

            m_prmCitySearchID.SourceColumn = "CitySearchID";
            this.Parameters.Add(m_prmCitySearchID);

            m_prmCountryID.SourceColumn = "CountryID";
            this.Parameters.Add(m_prmCountryID);

            m_prmCountryName.SourceColumn = "CountryName";
            this.Parameters.Add(m_prmCountryName);

            m_prmCreateTime.SourceColumn = "CreateTime";
            this.Parameters.Add(m_prmCreateTime);

            m_prmEmail.SourceColumn = "Email";
            this.Parameters.Add(m_prmEmail);

            m_prmFax.SourceColumn = "Fax";
            this.Parameters.Add(m_prmFax);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmHotelName.SourceColumn = "HotelName";
            this.Parameters.Add(m_prmHotelName);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsClose.SourceColumn = "IsClose";
            this.Parameters.Add(m_prmIsClose);

            m_prmStarRating.SourceColumn = "StarRating";
            this.Parameters.Add(m_prmStarRating);
        }
        private IDataParameter m_prmAddress= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["Address"]);
        public IDataParameter Address
        {
            get { return m_prmAddress; }
            set { m_prmAddress = value; }
        }

        private IDataParameter m_prmAddress2= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["Address2"]);
        public IDataParameter Address2
        {
            get { return m_prmAddress2; }
            set { m_prmAddress2 = value; }
        }

        private IDataParameter m_prmCheckInDate= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CheckInDate"]);
        public IDataParameter CheckInDate
        {
            get { return m_prmCheckInDate; }
            set { m_prmCheckInDate = value; }
        }

        private IDataParameter m_prmCheckOutDate= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CheckOutDate"]);
        public IDataParameter CheckOutDate
        {
            get { return m_prmCheckOutDate; }
            set { m_prmCheckOutDate = value; }
        }

        private IDataParameter m_prmCityID= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CityID"]);
        public IDataParameter CityID
        {
            get { return m_prmCityID; }
            set { m_prmCityID = value; }
        }

        private IDataParameter m_prmCityName= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CityName"]);
        public IDataParameter CityName
        {
            get { return m_prmCityName; }
            set { m_prmCityName = value; }
        }

        private IDataParameter m_prmCitySearchID= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CitySearchID"]);
        public IDataParameter CitySearchID
        {
            get { return m_prmCitySearchID; }
            set { m_prmCitySearchID = value; }
        }

        private IDataParameter m_prmCountryID= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CountryID"]);
        public IDataParameter CountryID
        {
            get { return m_prmCountryID; }
            set { m_prmCountryID = value; }
        }

        private IDataParameter m_prmCountryName= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CountryName"]);
        public IDataParameter CountryName
        {
            get { return m_prmCountryName; }
            set { m_prmCountryName = value; }
        }

        private IDataParameter m_prmCreateTime= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["CreateTime"]);
        public IDataParameter CreateTime
        {
            get { return m_prmCreateTime; }
            set { m_prmCreateTime = value; }
        }

        private IDataParameter m_prmEmail= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["Email"]);
        public IDataParameter Email
        {
            get { return m_prmEmail; }
            set { m_prmEmail = value; }
        }

        private IDataParameter m_prmFax= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["Fax"]);
        public IDataParameter Fax
        {
            get { return m_prmFax; }
            set { m_prmFax = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmHotelName= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["HotelName"]);
        public IDataParameter HotelName
        {
            get { return m_prmHotelName; }
            set { m_prmHotelName = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsClose= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["IsClose"]);
        public IDataParameter IsClose
        {
            get { return m_prmIsClose; }
            set { m_prmIsClose = value; }
        }

        private IDataParameter m_prmStarRating= EntityBase.NewParameter(Hotel.m_dtbEntitySchema.Columns["StarRating"]);
        public IDataParameter StarRating
        {
            get { return m_prmStarRating; }
            set { m_prmStarRating = value; }
        }

    }
    #endregion
}