// [实体版本]v2.7
// 2018-06-14 14:04:15
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.Data;
using XiWan.Data.DAL;

namespace XiWan.Entities
{
    [Serializable]
    public class CitySearch : EntityBase
    {

        #region 初始化
        public CitySearch() : base()
        {
        }
    
        public CitySearch(e_EntityState state) : base(state)
        {
        }
    
        static CitySearch()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("CitySearch");
    
            DataColumn dclCreateTime = new DataColumn(c_CreateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreateTime);
            dclCreateTime.Caption = "创建时间";
            dclCreateTime.AllowDBNull = true;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "主键";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsClose = new DataColumn(c_IsClose,typeof(System.Boolean));
            m_dtbEntitySchema.Columns.Add(dclIsClose);
            dclIsClose.Caption = "是否已作废";
            dclIsClose.AllowDBNull = true;
    
            DataColumn dclMSG = new DataColumn(c_MSG,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclMSG);
            dclMSG.Caption = "接口返回的备注";
            dclMSG.AllowDBNull = true;
            dclMSG.MaxLength = 500;
    
            DataColumn dclReferenceClient = new DataColumn(c_ReferenceClient,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclReferenceClient);
            dclReferenceClient.Caption = "对方系统生成属性";
            dclReferenceClient.AllowDBNull = true;
            dclReferenceClient.MaxLength = 50;
    
            DataColumn dclReferenceService = new DataColumn(c_ReferenceService,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclReferenceService);
            dclReferenceService.Caption = "接口返回的GUID";
            dclReferenceService.AllowDBNull = true;
            dclReferenceService.MaxLength = 50;
    
            DataColumn dclResponseTime = new DataColumn(c_ResponseTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclResponseTime);
            dclResponseTime.Caption = "返回时间";
            dclResponseTime.AllowDBNull = true;
    
            DataColumn dclSessionID = new DataColumn(c_SessionID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclSessionID);
            dclSessionID.Caption = "接口返回值";
            dclSessionID.AllowDBNull = true;
            dclSessionID.MaxLength = 100;
    
            DataColumn dclVersion = new DataColumn(c_Version,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclVersion);
            dclVersion.Caption = "接口返回的版本号";
            dclVersion.AllowDBNull = true;
            dclVersion.MaxLength = 10;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(CitySearch);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "CreateTime,ID,IsClose,MSG,ReferenceClient,ReferenceService,ResponseTime,SessionID,Version";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_CreateTime = "CreateTime";
        public const string c_ID = "ID";
        public const string c_IsClose = "IsClose";
        public const string c_MSG = "MSG";
        public const string c_ReferenceClient = "ReferenceClient";
        public const string c_ReferenceService = "ReferenceService";
        public const string c_ResponseTime = "ResponseTime";
        public const string c_SessionID = "SessionID";
        public const string c_Version = "Version";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.DateTime _createTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 创建时间[字段]
        /// </summary>
        public System.DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                AddOriginal("CreateTime", _createTime, value);
                _createTime = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// 主键[字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Boolean _isClose = false;
    
        /// <summary>
        /// 是否已作废[字段]
        /// </summary>
        public System.Boolean IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                AddOriginal("IsClose", _isClose, value);
                _isClose = value;
            }
        }
    
        private System.String _mSG = string.Empty;
    
        /// <summary>
        /// 接口返回的备注[字段]
        /// </summary>
        public System.String MSG
        {
            get
            {
                return _mSG;
            }
            set
            {
                AddOriginal("MSG", _mSG, value);
                _mSG = value;
            }
        }
    
        private System.String _referenceClient = string.Empty;
    
        /// <summary>
        /// 对方系统生成属性[字段]
        /// </summary>
        public System.String ReferenceClient
        {
            get
            {
                return _referenceClient;
            }
            set
            {
                AddOriginal("ReferenceClient", _referenceClient, value);
                _referenceClient = value;
            }
        }
    
        private System.String _referenceService = string.Empty;
    
        /// <summary>
        /// 接口返回的GUID[字段]
        /// </summary>
        public System.String ReferenceService
        {
            get
            {
                return _referenceService;
            }
            set
            {
                AddOriginal("ReferenceService", _referenceService, value);
                _referenceService = value;
            }
        }
    
        private System.DateTime _responseTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 返回时间[字段]
        /// </summary>
        public System.DateTime ResponseTime
        {
            get
            {
                return _responseTime;
            }
            set
            {
                AddOriginal("ResponseTime", _responseTime, value);
                _responseTime = value;
            }
        }
    
        private System.String _sessionID = string.Empty;
    
        /// <summary>
        /// 接口返回值[字段]
        /// </summary>
        public System.String SessionID
        {
            get
            {
                return _sessionID;
            }
            set
            {
                AddOriginal("SessionID", _sessionID, value);
                _sessionID = value;
            }
        }
    
        private System.String _version = string.Empty;
    
        /// <summary>
        /// 接口返回的版本号[字段]
        /// </summary>
        public System.String Version
        {
            get
            {
                return _version;
            }
            set
            {
                AddOriginal("Version", _version, value);
                _version = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static CitySearch DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as CitySearch;
        }
    
        public CitySearch Clone()
        {
            return EntityBase.Clone(this) as CitySearch;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class CitySearchCollection : EntityCollectionBase
    {

        public CitySearchCollection()
        {
        }

        private static Type m_typ = typeof(CitySearchCollection);
        private static Type m_typItem = typeof(CitySearch);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public CitySearch this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (CitySearch)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public CitySearch this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new CitySearch GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as CitySearch;
        }

        public int Add(CitySearch value)
        {
            return List.Add(value);
        }

        public void Insert(int index, CitySearch value)
        {
            List.Insert(index, value);
        }

        public void Remove(CitySearch value)
        {
            List.Remove(value);
        }

        public void Delete(CitySearch value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new CitySearchCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(CitySearch value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(CitySearch value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class CitySearchQuery : XiWan.Data.DAL.EntityQueryBase
    {

        public CitySearchQuery()
        {
            m_strTableName = "CitySearch";

            m_prmCreateTime.SourceColumn = "CreateTime";
            this.Parameters.Add(m_prmCreateTime);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsClose.SourceColumn = "IsClose";
            this.Parameters.Add(m_prmIsClose);

            m_prmMSG.SourceColumn = "MSG";
            this.Parameters.Add(m_prmMSG);

            m_prmReferenceClient.SourceColumn = "ReferenceClient";
            this.Parameters.Add(m_prmReferenceClient);

            m_prmReferenceService.SourceColumn = "ReferenceService";
            this.Parameters.Add(m_prmReferenceService);

            m_prmResponseTime.SourceColumn = "ResponseTime";
            this.Parameters.Add(m_prmResponseTime);

            m_prmSessionID.SourceColumn = "SessionID";
            this.Parameters.Add(m_prmSessionID);

            m_prmVersion.SourceColumn = "Version";
            this.Parameters.Add(m_prmVersion);
        }
        private IDataParameter m_prmCreateTime= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["CreateTime"]);
        public IDataParameter CreateTime
        {
            get { return m_prmCreateTime; }
            set { m_prmCreateTime = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsClose= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["IsClose"]);
        public IDataParameter IsClose
        {
            get { return m_prmIsClose; }
            set { m_prmIsClose = value; }
        }

        private IDataParameter m_prmMSG= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["MSG"]);
        public IDataParameter MSG
        {
            get { return m_prmMSG; }
            set { m_prmMSG = value; }
        }

        private IDataParameter m_prmReferenceClient= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["ReferenceClient"]);
        public IDataParameter ReferenceClient
        {
            get { return m_prmReferenceClient; }
            set { m_prmReferenceClient = value; }
        }

        private IDataParameter m_prmReferenceService= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["ReferenceService"]);
        public IDataParameter ReferenceService
        {
            get { return m_prmReferenceService; }
            set { m_prmReferenceService = value; }
        }

        private IDataParameter m_prmResponseTime= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["ResponseTime"]);
        public IDataParameter ResponseTime
        {
            get { return m_prmResponseTime; }
            set { m_prmResponseTime = value; }
        }

        private IDataParameter m_prmSessionID= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["SessionID"]);
        public IDataParameter SessionID
        {
            get { return m_prmSessionID; }
            set { m_prmSessionID = value; }
        }

        private IDataParameter m_prmVersion= EntityBase.NewParameter(CitySearch.m_dtbEntitySchema.Columns["Version"]);
        public IDataParameter Version
        {
            get { return m_prmVersion; }
            set { m_prmVersion = value; }
        }

    }
    #endregion
}