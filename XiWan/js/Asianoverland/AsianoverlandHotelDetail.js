﻿/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Asianoverland/CustomerInformation.html" />
var _hotelID;
var _countryID;
var _cityID;
var _referenceClient;
var _nationality;
var _countryResidence;
var _beginTime;
var _endTime;
var AdultCount = 0;
var ChildsCount = 0;
var rooms;
var ddlRoom;

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
$(function () {
    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });

    $("#Destination").combobox({
        valueField: 'value',
        textField: 'name',
        url: "Asianoverland/AsianoverlandPort.ashx?type=Get_Destination",
        mode: "remote", //从服务器加载就设置为‘remote‘
        hasDownArrow: false,  //为true时显示下拉选项图标
        onBeforeLoad: function (parm) {  //在请求加载数据之前触发，返回 false 则取消加载动作         
            var value = $(this).combobox("getValue");
            if (value) {
                parm.paramName = value;
                return true;
            }
            return false;
        }
    })
});

//初始页面
$(document).ready(function () {
    AdultCount = 0;
    _hotelID = GetQueryString("HotelID");
    _countryID = GetQueryString("CountryID");
    _cityID = GetQueryString("CityID");
    _referenceClient = GetQueryString("ReferenceClient");
    _nationality = GetQueryString("NationalityID");
    _countryResidence = GetQueryString("countryResidence");
    _beginTime = GetQueryString("CheckInDate");
    _endTime = GetQueryString("CheckOutDate");
    ddlRoom=RoomCount = GetQueryString("DdlRoom");
    rooms=Rooms = GetQueryString("Rooms");
    document.getElementById('ddlRoom').value = RoomCount;
    ChangeRoom(RoomCount);
    var ArrayRooms = Rooms.split(";");
    for (var i = 1; i <= RoomCount; i++) {
        var room = ArrayRooms[i - 1].split(",");
        AdultCount = Number(AdultCount) + Number(room[0]);
        ChildsCount = Number(ChildsCount) + Number(room[1]);
        refreshText(i, room[0], room[1], room[2], room[3])

    }
    $('#starDate').datebox("setValue", _beginTime);
    $('#stopDate').datebox("setValue", _endTime);
    //设置目的地
    document.getElementById("room1").style.display = "block";
    Get_AsianoverlandHotelDetailList(RoomCount, Rooms);
});


//查询
function search_Click() {
    ddlRoom = $("#ddlRoom option:selected").val();//房间数
    var showType = ' <%= showType %>';
    if ($("#Room1Child option:selected").val() == 2) {
        if (Trim($("#txtRoom1Child1").val()) == "" || Trim($("#txtRoom1Child2").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0 || Trim($("#txtRoom1Child2").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    if ($("#Room1Child option:selected").val() == 1) {
        if (Trim($("#txtRoom1Child1").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    AdultCount = $("#Room1Adult option:selected").val();
    ChildsCount = $("#Room1Child option:selected").val();
    rooms = $("#Room1Adult option:selected").val() + "," + $("#Room1Child option:selected").val() + "," + Trim($("#txtRoom1Child1").val()) + "," + Trim($("#txtRoom1Child2").val()) + ";";
    for (var i = 2; i <= ddlRoom; i++) {
        var Adult = "#Room" + i + "Adult option:selected";
        var Child = "#Room" + i + "Child option:selected";
        var ChildAge1 = "#txtRoom" + i + "Child1";
        var ChildAge2 = "#txtRoom" + i + "Child2";
        if ($(Child).val() > 1) {
            if (Trim($(ChildAge1).val()) == "" || Trim($(ChildAge2).val()) == "") {
                alert("请输入儿童岁数");
                return false;
            }
            else if (Trim($(ChildAge1).val()) < 0 || Trim($(ChildAge2).val()) < 0) {
                alert("儿童岁数不能小于0");
                return false;
            }
        }
        else if ($(Child).val() == 1) {
            if (Trim($(ChildAge1).val()) == "") {
                alert("请输入儿童岁数");
                return false;
            }
            else if (Trim($(ChildAge1).val()) < 0) {
                alert("儿童岁数不能小于0");
                return false;
            }
        }
        rooms += $(Adult).val() + "," + $(Child).val() + "," + $(ChildAge1).val() + "," + $(ChildAge2).val() + ";";
        AdultCount = Number(AdultCount) + Number($(Adult).val());
        ChildsCount = Number(ChildsCount) + Number($(Child).val());
    }
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    Get_AsianoverlandHotelDetailList(ddlRoom, rooms);
}

//展示列表 _ddlRoom房间数，_rooms需要预订房间详情，type:0为缓存，1为实时
function Get_AsianoverlandHotelDetailList(_ddlRoom, _rooms) {

    _beginTime = $('#_easyui_textbox_input5').val();//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间

    var $width = $("#showHotelDetailDiv").width();
    var $height = $(window).height();
    var $datagrid = $("#AsianoverlandHotelDetailList");
    $datagrid.datagrid({
        height: $height - 92,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: true,
        pagination: true,
        pageSize: 20,//每页显示的记录条数，默认为20
        pageList: [10, 15, 20, 30, 40, 60],//可以设置每页记录条数的列表
        rownumbers: true,
        url: '../Asianoverland/AsianoverlandPort.ashx',
        queryParams: { type: 'GetHotelQuote', hotelID: _hotelID, countryID: _countryID, referenceClient: _referenceClient, nationality: _nationality,countryResidence:_countryResidence, cityID: _cityID, beginTime: _beginTime, endTime: _endTime, ddlRoom: _ddlRoom, rooms: _rooms },
        idField: 'HotelID',
        columns: [[
        //{ field: 'HotelID', title: 'HotelID', hidden: true },
        {
            field: 'ClassName', width: '25%', title: "房型", align: 'center', editor: { type: 'text', options: { required: true } }
        }
        //,
        //{
        //    field: 'Status', width: '15%', title: "状态", align: 'center', formatter: function (value, row, index) {
        //        if (row.Status == "AL") {
        //            return "可预订";
        //        }
        //        else if (row.Status == "RQ") {
        //            return "根据要求";
        //        }
        //        else {
        //            return row.RoomStatus;
        //        }
        //    }
        //}
        //,
        //{
        //    field: 'TotalRoom', width: '7%', title: "可预订数量", align: 'center', formatter: function (value, row, index) {
        //        return row.TotalRoom;
        //    }
        //}
        ,
        {
            field: 'RMBprice', width: '15%', title: "价格", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'CurrencyCode', width: '25%', title: "币种", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'HotelID', width: '20%', title: "操作", align: 'center', formatter: function (value, row, index) {
                var Action = "<a  href=\"javascript:void(0)\" onclick=\"CancelDeadline('" + row.HotelID + "','" + row.RateplanId + "')\">查看取消日期</a>\ | <a  href=\"javascript:void(0)\" onclick=\"book('" + row.HotelID + "','" + row.RateplanId + "')\">预订</a>";
                return Action;
                //return  "<a href=\"javascript:void(0)\" onclick=\"book()\"> 预订 </a>";
            }
        }
        //,
        //{
        //    field: 'Adults', width: '5%', title: "大人数", align: 'center', formatter: function (value, row, index) {
        //        return row.Adults;
        //    }
        //}
        //,
        //{
        //    field: 'Childs', width: '5%', title: "儿童数", align: 'center', formatter: function (value, row, index) {
        //        return row.Childs;
        //    }
        //}

        ]],
        onLoadSuccess: function (data) {
            if (data.total == 0) {
                alert("没有数据！");
            }
            //dispalyEasyUILoad();
            //$(this).datagrid("autoMergeCells", ['ItemName', 'StarRating']);
        }
    });
    var p = $("#AsianoverlandHotelDetailList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}

function CancelDeadline(HotelID, RateplanId) {
    var paramPost = {};
    paramPost.HotelID = HotelID;
    paramPost.RateplanId = RateplanId
    $.post("../Asianoverland/AsianoverlandPort.ashx?type=GetCancellationPolicy", paramPost, function (data) {
        if (data.code == "99") {
            alert(data.mes)
        }
        else { alert("获取成功:" + data.mes); }
    });
}

function read(HotelID, RateplanId) {
    var paramPost = {};
    paramPost.beginTime = _beginTime;
    paramPost.endTime = _endTime;
    paramPost.HotelID = HotelID;
    paramPost.RateplanId = RateplanId
    $.post("../Asianoverland/AsianoverlandPort.ashx?type=GetHotelRecheckPrice", paramPost, function (data) {
        if (data.code == "99") {
            alert(data.mes);
        }
        else {
            if (data.result == "AL") {
                alert("可预订，金额为：" + data.mes);
            }
            else { alert("不可以预订"); }
        }
    });
}
function book(HotelID, RateplanId) {
    //预订前先检查价格
    var paramPost = {};
    paramPost.HotelID = HotelID;
    paramPost.RateplanId = RateplanId;
    $.get("../Asianoverland/AsianoverlandPort.ashx?type=GetIsRecheckPrice", paramPost, function (data) {
        if (data == "99") {
            alert("请先查看取消日期,再预订(Please click the button first查看取消日期)");
            return false;
        }
        var url = "../Asianoverland/CustomerInformation.html?HotelID=" + HotelID + "&RateplanId=" + RateplanId + "&number_of_rooms=" + ddlRoom + "&roomDetails=" + rooms + "&CheckInDate=" + _beginTime + "&CheckOutDate=" + _endTime;
        var name = "客户信息";
        var iWidth = "950";//宽度
        var iHeight = "700";//高度
        openwindow(url, name, iWidth, iHeight);
    });
    
    
    
    //winOpen("../Asianoverland/CustomerInformation.html", 500, 200);
    //return false;
    /*var paramPost = {};
    paramPost.beginTime = _beginTime;
    paramPost.endTime = _endTime;
    paramPost.HotelID = HotelID;
    paramPost.RateplanId = RateplanId;
    if (window.confirm("请确认是否已查看取消日期及查阅提示？") == true) {
        //("确定");
        //return true;
        $.post("../Asianoverland/AsianoverlandPort.ashx?type=book", paramPost, function (data) {
            if (data.code == "99") {
                alert(data.mes)
            }
            else {
                alert(data.mes);
            }
        });
    }
    else {
        //("取消");
        return false;
    }*/
}

//选择房间数时控制房间显示隐藏
function ChangeRoom(Roomsm) {
    for (var j = 1; j <= 9; j++) {
        var room = "room" + j;
        document.getElementById(room).style.display = "none";
    }
    for (var j = 1; j <= Roomsm; j++) {
        var room = "room" + j;
        document.getElementById(room).style.display = "block";
    }
}

$.extend($.fn.datagrid.methods, {
    autoMergeCells: function (jq, fields) {
        return jq.each(function () {
            var target = $(this);
            if (!fields) {
                fields = target.datagrid("getColumnFields");
            }
            var rows = target.datagrid("getRows");
            var i = 0,
            j = 0,
            temp = {};
            for (i; i < rows.length; i++) {
                var row = rows[i];
                j = 0;
                for (j; j < fields.length; j++) {
                    var field = fields[j];
                    var tf = temp[field];
                    if (!tf) {
                        tf = temp[field] = {};
                        tf[row[field]] = [i];
                    } else {
                        var tfv = tf[row[field]];
                        if (tfv) {
                            tfv.push(i);
                        } else {
                            tfv = tf[row[field]] = [i];
                        }
                    }
                }
            }
            $.each(temp, function (field, colunm) {
                $.each(colunm, function () {
                    var group = this;

                    if (group.length > 1) {
                        var before,
                        after,
                        megerIndex = group[0];
                        for (var i = 0; i < group.length; i++) {
                            before = group[i];
                            after = group[i + 1];
                            if (after && (after - before) == 1) {
                                continue;
                            }
                            var rowspan = before - megerIndex + 1;
                            if (rowspan > 1) {
                                target.datagrid('mergeCells', {
                                    index: megerIndex,
                                    field: field,
                                    rowspan: rowspan
                                });
                            }
                            if (after && (after - before) != 1) {
                                megerIndex = after;
                            }
                        }
                    }
                });
            });
        });
    }
});

function refreshText(roomCount, Adult, Child, Child1Age, Child2Age) {
    if (roomCount == 1) {
        ChangeRoom1Child(Child);
        $("#txtRoom1Child1").val(Child1Age);
        $("#txtRoom1Child2").val(Child2Age);
        document.getElementById('Room1Adult').value = Adult;
        document.getElementById('Room1Child').value = Child;
    }
    else if (roomCount == 2) {
        ChangeRoom2Child(Child);
        $("#txtRoom2Child1").val(Child1Age);
        $("#txtRoom2Child2").val(Child2Age);
        document.getElementById('Room2Adult').value = Adult;
        document.getElementById('Room2Child').value = Child;
    }
    else if (roomCount == 3) {
        ChangeRoom3Child(Child);
        $("#txtRoom3Child1").val(Child1Age);
        $("#txtRoom3Child2").val(Child2Age);
        document.getElementById('Room3Adult').value = Adult;
        document.getElementById('Room3Child').value = Child;
    }
    else if (roomCount == 4) {
        ChangeRoom4Child(Child);
        $("#txtRoom4Child1").val(Child1Age);
        $("#txtRoom4Child2").val(Child2Age);
        document.getElementById('Room4Adult').value = Adult;
        document.getElementById('Room4Child').value = Child;
    }
    else if (roomCount == 5) {
        ChangeRoom5Child(Child);
        $("#txtRoom5Child1").val(Child1Age);
        $("#txtRoom5Child2").val(Child2Age);
        document.getElementById('Room5Adult').value = Adult;
        document.getElementById('Room5Child').value = Child;
    }
    else if (roomCount == 6) {
        ChangeRoom6Child(Child);
        $("#txtRoom6Child1").val(Child1Age);
        $("#txtRoom6Child2").val(Child2Age);
        document.getElementById('Room6Adult').value = Adult;
        document.getElementById('Room6Child').value = Child;
    }
    else if (roomCount == 7) {
        ChangeRoom7Child(Child);
        $("#txtRoom7Child1").val(Child1Age);
        $("#txtRoom7Child2").val(Child2Age);
        document.getElementById('Room7Adult').value = Adult;
        document.getElementById('Room7Child').value = Child;
    }
    else if (roomCount == 8) {
        ChangeRoom8Child(Child);
        $("#txtRoom8Child1").val(Child1Age);
        $("#txtRoom8Child2").val(Child2Age);
        document.getElementById('Room8Adult').value = Adult;
        document.getElementById('Room8Child').value = Child;
    }
    else if (roomCount == 9) {
        ChangeRoom9Child(Child);
        $("#txtRoom9Child1").val(Child1Age);
        $("#txtRoom9Child2").val(Child2Age);
        document.getElementById('Room9Adult').value = Adult;
        document.getElementById('Room9Child').value = Child;
    }
}



//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

//房间1对儿童岁数框控制
function ChangeRoom1Child(Childs) {
    document.getElementById("txtRoom1Child1").style.display = "none";
    document.getElementById("txtRoom1Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom1Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom1Child1").style.display = "block";
        document.getElementById("txtRoom1Child2").style.display = "block";
    }
    else {
        $("#txtRoom1Child1").val("");
        $("#txtRoom1Child2").val("");
    }
}

//房间2对儿童岁数框控制
function ChangeRoom2Child(Childs) {
    document.getElementById("txtRoom2Child1").style.display = "none";
    document.getElementById("txtRoom2Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom2Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom2Child1").style.display = "block";
        document.getElementById("txtRoom2Child2").style.display = "block";
    }
    else {
        $("#txtRoom2Child1").val("");
        $("#txtRoom2Child2").val("");
    }
}

//房间3对儿童岁数框控制
function ChangeRoom3Child(Childs) {
    document.getElementById("txtRoom3Child1").style.display = "none";
    document.getElementById("txtRoom3Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom3Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom3Child1").style.display = "block";
        document.getElementById("txtRoom3Child2").style.display = "block";
    }
    else {
        $("#txtRoom3Child1").val("");
        $("#txtRoom3Child2").val("");
    }
}

//房间4对儿童岁数框控制
function ChangeRoom4Child(Childs) {
    document.getElementById("txtRoom4Child1").style.display = "none";
    document.getElementById("txtRoom4Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom4Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom4Child1").style.display = "block";
        document.getElementById("txtRoom4Child2").style.display = "block";
    }
    else {
        $("#txtRoom4Child1").val("");
        $("#txtRoom4Child2").val("");
    }
}

//房间5对儿童岁数框控制
function ChangeRoom5Child(Childs) {
    document.getElementById("txtRoom5Child1").style.display = "none";
    document.getElementById("txtRoom5Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom5Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom5Child1").style.display = "block";
        document.getElementById("txtRoom5Child2").style.display = "block";
    }
    else {
        $("#txtRoom5Child1").val("");
        $("#txtRoom5Child2").val("");
    }
}

//房间6对儿童岁数框控制
function ChangeRoom6Child(Childs) {
    document.getElementById("txtRoom6Child1").style.display = "none";
    document.getElementById("txtRoom6Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom6Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom6Child1").style.display = "block";
        document.getElementById("txtRoom6Child2").style.display = "block";
    }
    else {
        $("#txtRoom6Child1").val("");
        $("#txtRoom6Child2").val("");
    }
}

//房间7对儿童岁数框控制
function ChangeRoom7Child(Childs) {
    document.getElementById("txtRoom7Child1").style.display = "none";
    document.getElementById("txtRoom7Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom7Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom7Child1").style.display = "block";
        document.getElementById("txtRoom7Child2").style.display = "block";
    }
    else {
        $("#txtRoom6Child1").val("");
        $("#txtRoom7Child2").val("");
    }
}

//房间8对儿童岁数框控制
function ChangeRoom8Child(Childs) {
    document.getElementById("txtRoom8Child1").style.display = "none";
    document.getElementById("txtRoom8Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom8Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom8Child1").style.display = "block";
        document.getElementById("txtRoom8Child2").style.display = "block";
    }
    else {
        $("#txtRoom8Child1").val("");
        $("#txtRoom8Child2").val("");
    }
}

//房间9对儿童岁数框控制
function ChangeRoom9Child(Childs) {
    document.getElementById("txtRoom9Child1").style.display = "none";
    document.getElementById("txtRoom9Child2").style.display = "none";
    if (Childs == "1") {
        document.getElementById("txtRoom9Child1").style.display = "block";
    }
    else if (Childs == "2") {
        document.getElementById("txtRoom9Child1").style.display = "block";
        document.getElementById("txtRoom9Child2").style.display = "block";
    }
    else {
        $("#txtRoom9Child1").val("");
        $("#txtRoom9Child2").val("");
    }
}

//查询
function searchOur_Click() {
    var ddlRoom = $("#ddlRoom option:selected").val();//房间数
    var showType = ' <%= showType %>';
    if ($("#Room1Child option:selected").val() == 2) {
        if (Trim($("#txtRoom1Child1").val()) == "" || Trim($("#txtRoom1Child2").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0 || Trim($("#txtRoom1Child2").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    if ($("#Room1Child option:selected").val() == 1) {
        if (Trim($("#txtRoom1Child1").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    AdultCount = $("#Room1Adult option:selected").val();
    ChildsCount = $("#Room1Child option:selected").val();
    var rooms = $("#Room1Adult option:selected").val() + "," + $("#Room1Child option:selected").val() + "," + Trim($("#txtRoom1Child1").val()) + "," + Trim($("#txtRoom1Child2").val()) + ";";
    for (var i = 2; i <= ddlRoom; i++) {
        var Adult = "#Room" + i + "Adult option:selected";
        var Child = "#Room" + i + "Child option:selected";
        var ChildAge1 = "#txtRoom" + i + "Child1";
        var ChildAge2 = "#txtRoom" + i + "Child2";
        if ($(Child).val() > 1) {
            if (Trim($(ChildAge1).val()) == "" || Trim($(ChildAge2).val()) == "") {
                alert("请输入儿童岁数");
                return false;
            }
            else if (Trim($(ChildAge1).val()) < 0 || Trim($(ChildAge2).val()) < 0) {
                alert("儿童岁数不能小于0");
                return false;
            }
        }
        else if ($(Child).val() == 1) {
            if (Trim($(ChildAge1).val()) == "") {
                alert("请输入儿童岁数");
                return false;
            }
            else if (Trim($(ChildAge1).val()) < 0) {
                alert("儿童岁数不能小于0");
                return false;
            }
        }
        rooms += $(Adult).val() + "," + $(Child).val() + "," + $(ChildAge1).val() + "," + $(ChildAge2).val() + ";";
        AdultCount = Number(AdultCount) + Number($(Adult).val());
        ChildsCount = Number(ChildsCount) + Number($(Child).val());
    }
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    GetAsianoverlandRoomTypeOutPrice(ddlRoom, rooms);
}

//展示列表 _ddlRoom房间数，_rooms需要预订房间详情，type:0为缓存，1为实时
function GetAsianoverlandRoomTypeOutPrice(_ddlRoom, _rooms) {

    _beginTime = $('#_easyui_textbox_input5').val();//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    var paramPost = {};
    paramPost.hotelID = _hotelID;
    paramPost.countryID = _countryID;
    paramPost.referenceClient = _referenceClient;
    paramPost.nationalityID = _nationality;
    paramPost.cityID = _cityID;
    paramPost.beginTime = _beginTime;
    paramPost.endTime = _endTime;
    paramPost.ddlRoom = _ddlRoom;
    paramPost.rooms = _rooms
    $.post("../Asianoverland/Asianoverland.ashx?type=GetAsianoverlandRoomTypePrice", paramPost, function (data) {

    });
}