﻿var _HotelName;// 酒店名称
var _nationality;//国籍
var _destinationValue;//目的地
var _beginTime;//入住时间
var _endTime;//退房时间
var _ddlGreadeCode;///酒店等級
var _ddlLocationCode;///区域
var _ddlRoomTypeCode1 = "0";///房间1类型
var _Room1ChildAge1;///房间1儿童岁数
var _Room1ChildAge2;///房间1儿童岁数
var _Room1Count;///房间1房间数
var _ddlRoomTypeCode2 = "0";///房间2类型
var _Room2ChildAge1;///房间2儿童岁数
var _Room2ChildAge2;///房间2儿童岁数
var _Room2Count;///房间2房间数
var _ddlRoomTypeCode3 = "0";///房间3类型
var _Room3ChildAge1;///房间3儿童岁数
var _Room3ChildAge2;///房间3儿童岁数
var _Room3Count;///房间3房间数
var _ddlRoomTypeCode4 = "0";///房间4类型
var _Room4ChildAge1;///房间4儿童岁数
var _Room4ChildAge2;///房间4儿童岁数
var _Room4Count;///房间4房间数
var _ddlRoomTypeCode5 = "0";///房间5类型
var _Room5ChildAge1;///房间5儿童岁数
var _Room5ChildAge2;///房间5儿童岁数
var _Room5Count;///房间5房间数


$(function () {
    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });
    //设置目的地
    $("#txtCityCode").combobox({
        valueField: 'value',
        textField: 'name',
        url: "../RTS/RtsPort.ashx?type=GetRTSdestination",
        mode: "remote", //从服务器加载就设置为‘remote‘
        hasDownArrow: false,  //为true时显示下拉选项图标
        onBeforeLoad: function (parm) {  //在请求加载数据之前触发，返回 false 则取消加载动作         
            var value = $(this).combobox("getValue");
            if (value) {
                parm.paramName = value;
                return true;
            }
            return false;
        }
    })

    //判断目的地是否数据库存在
    $('#txtCityCode').combobox('textbox').bind('blur', function () {
        var destinationValue = Trim($('#txtCityCode').combobox('getValue'));//目的地
        if (destinationValue != "")
        {
            ChangeLocationCode();
        }
    });
});
//区域
function ChangeLocationCode()
{
    var txtCityCode = Trim($('#txtCityCode').combobox('getValue'));//目的地
    if (txtCityCode != "") {
        $.post("../RTS/RtsPort.ashx?type=GetRTScityCodeYn", { destination: txtCityCode }, function (data) {
            if (data == "1")
            {
                $('select[name=ddlLocationCode]').empty();
                $('select[name=ddlLocationCode]').append("<option value=\"\">请选择</option>");
                $.post("../RTS/RtsPort.ashx?type=GetRTSLocationCode", { destination: txtCityCode }, function (data) {
                    $.each(data, function () {
                        $('select[name=ddlLocationCode]').append("<option value='" + this.CODE + "'>" + this.NAME + "</option>");
                    });
                });
            }
        });
    } 
}

//初始儿童岁数  为不可操作
function InitializationChildAge()
{
    //国籍赋值
    $.ajaxSettings.async = false;
    $.post("../RTS/RtsPort.ashx?type=GetRTSnationality", null, function (data) {
        $('select[name=ddlNationality]').empty();
        $('select[name=ddlNationality]').append("<option value=\"0\">请选择</option>");
        $.each(data, function () {
            $('select[name=ddlNationality]').append("<option value='" + this.ID + "'>" + this.NAME + "</option>");
        });
    });

    document.getElementById("Room1ChildAge1").disabled = true;
    document.getElementById("Room1ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room1ChildAge2").disabled = true;
    document.getElementById("Room1ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room1Count").disabled = true;
    document.getElementById("Room1Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room2ChildAge1").disabled = true;
    document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room2ChildAge2").disabled = true;
    document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room2Count").disabled = true;
    document.getElementById("Room2Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room3ChildAge1").disabled = true;
    document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room3ChildAge2").disabled = true;
    document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room3Count").disabled = true;
    document.getElementById("Room3Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room4ChildAge1").disabled = true;
    document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room4ChildAge2").disabled = true;
    document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room4Count").disabled = true;
    document.getElementById("Room4Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room5ChildAge1").disabled = true;
    document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room5ChildAge2").disabled = true;
    document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room5Count").disabled = true;
    document.getElementById("Room5Count").style.backgroundColor = "#dddddd";

    Get_RTShotelList();
}

//房间类型改变
function ChangeRoomType1(RoomType)
{ 
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型

    if (RoomType != "0") {
        if (TypeCode2 != "0" || TypeCode3 != "0" || TypeCode4 != "0" || TypeCode5 != "0") {
            var Room2Count = $("#Room2Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room1Count").disabled = false;
        document.getElementById("Room1Count").style.backgroundColor = "#fff";
        document.getElementById('Room1Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            //document.getElementById('Room1Count').value = 1;
            document.getElementById('Room1ChildAge1').value = 1;
            document.getElementById("Room1ChildAge1").disabled = false;
            document.getElementById("Room1ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room1ChildAge2").disabled = false;
            document.getElementById("Room1ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room1Count").disabled = true;
            document.getElementById("Room1Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room1ChildAge1").disabled = true;
            document.getElementById("Room1ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room1ChildAge1').value = 0;
            document.getElementById("Room1ChildAge2").disabled = true;
            document.getElementById("Room1ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room1ChildAge2').value = 0;
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode2').value = 0;
                document.getElementById("Room2ChildAge1").disabled = true;
                document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge1').value = 0;
                document.getElementById("Room2ChildAge2").disabled = true;
                document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge2').value = 0;
                document.getElementById("Room2Count").disabled = true;
                document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room2Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room5Count = $("#Room5Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room5Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else
    {
        document.getElementById("Room1ChildAge1").disabled = true;
        document.getElementById("Room1ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room1ChildAge1').value = 0;
        document.getElementById("Room1ChildAge2").disabled = true;
        document.getElementById("Room1ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room1ChildAge2').value = 0;
        document.getElementById("Room1Count").disabled = true;
        document.getElementById("Room1Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room1Count').value = 0;
    }
}

function ChangeRoomType2(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode3 != "0" || TypeCode4 != "0" || TypeCode5 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room1Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room2Count").disabled = false;
        document.getElementById("Room2Count").style.backgroundColor = "#fff";
        document.getElementById('Room2Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room2ChildAge1').value = 1;
            document.getElementById("Room2ChildAge1").disabled = false;
            document.getElementById("Room2ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room2ChildAge2").disabled = false;
            document.getElementById("Room2ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room2Count").disabled = true;
            document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room2ChildAge1").disabled = true;
            document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room2ChildAge1').value = 0;
            document.getElementById("Room2ChildAge2").disabled = true;
            document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room2ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode2').value = 0;
                document.getElementById("Room2ChildAge1").disabled = true;
                document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge1').value = 0;
                document.getElementById("Room2ChildAge2").disabled = true;
                document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge2').value = 0;
                document.getElementById("Room2Count").disabled = true;
                document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room2Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room5Count = $("#Room5Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room5Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room2ChildAge1").disabled = true;
        document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room2ChildAge1').value = 0;
        document.getElementById("Room2ChildAge2").disabled = true;
        document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room2ChildAge2').value = 0;
        document.getElementById("Room2Count").disabled = true;
        document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room2Count').value = 0;
    }
}

function ChangeRoomType3(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode2 != "0" || TypeCode4 != "0" || TypeCode5 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room2Count = $("#Room2Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room1Count) + Number(Room2Count) + Number(Room4Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room3Count").disabled = false;
        document.getElementById("Room3Count").style.backgroundColor = "#fff";
        document.getElementById('Room3Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room3ChildAge1').value = 1;
            document.getElementById("Room3ChildAge1").disabled = false;
            document.getElementById("Room3ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room3ChildAge2").disabled = false;
            document.getElementById("Room3ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room3Count").disabled = true;
            document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room3ChildAge1").disabled = true;
            document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room3ChildAge1').value = 0;
            document.getElementById("Room3ChildAge2").disabled = true;
            document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room3ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room5Count = $("#Room5Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room5Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room3ChildAge1").disabled = true;
        document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room3ChildAge1').value = 0;
        document.getElementById("Room3ChildAge2").disabled = true;
        document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room3ChildAge2').value = 0;
        document.getElementById("Room3Count").disabled = true;
        document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room3Count').value = 0;
    }
}

function ChangeRoomType4(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode2 != "0" || TypeCode3 != "0" || TypeCode5 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room2Count = $("#Room2Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room4Count").disabled = false;
        document.getElementById("Room4Count").style.backgroundColor = "#fff";
        document.getElementById('Room4Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room4ChildAge1').value = 1;
            document.getElementById("Room4ChildAge1").disabled = false;
            document.getElementById("Room4ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room4ChildAge2").disabled = false;
            document.getElementById("Room4ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room4Count").disabled = true;
            document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room4ChildAge1").disabled = true;
            document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room4ChildAge1').value = 0;
            document.getElementById("Room4ChildAge2").disabled = true;
            document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room4ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room4Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room4ChildAge1").disabled = true;
        document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room4ChildAge1').value = 0;
        document.getElementById("Room4ChildAge2").disabled = true;
        document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room4ChildAge2').value = 0;
        document.getElementById("Room4Count").disabled = true;
        document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room4Count').value = 0;
    }
}

function ChangeRoomType5(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode2 != "0" || TypeCode3 != "0" || TypeCode4 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room2Count = $("#Room2Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room5Count").disabled = false;
        document.getElementById("Room5Count").style.backgroundColor = "#fff";
        document.getElementById('Room5Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room5ChildAge1').value = 1;
            document.getElementById("Room5ChildAge1").disabled = false;
            document.getElementById("Room5ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room5ChildAge2").disabled = false;
            document.getElementById("Room5ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room5Count").disabled = true;
            document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room5ChildAge1").disabled = true;
            document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room5ChildAge1').value = 0;
            document.getElementById("Room5ChildAge2").disabled = true;
            document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room5ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room4Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room5ChildAge1").disabled = true;
        document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room5ChildAge1').value = 0;
        document.getElementById("Room5ChildAge2").disabled = true;
        document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room5ChildAge2').value = 0;
        document.getElementById("Room5Count").disabled = true;
        document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room5Count').value = 0;
    }
}

function ChangeRoom1Count(Room1Count)
{
    var Room2Count = $("#Room2Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5)
    {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room1Count').value = 0;
        return false;
    }
}

function ChangeRoom2Count(Room2Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room2Count').value = 0;
        return false;
    }
}

function ChangeRoom3Count(Room3Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room2Count = $("#Room2Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room3Count').value = 0;
        return false;
    }
}

function ChangeRoom4Count(Room4Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room2Count = $("#Room2Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room4Count').value = 0;
        return false;
    }
}

function ChangeRoom5Count(Room4Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room2Count = $("#Room2Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room5Count').value = 0;
        return false;
    }
}

//查询
function search_Click() {
    _HotelName = Trim($('#txtHotelName').val());// 酒店名称
    _nationality = Trim($("#ddlNationality option:selected").val());//国籍
    _destinationValue = Trim($('#txtCityCode').combobox('getValue'));//目的地
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    _ddlGreadeCode = $("#ddlGreadeCode option:selected").val();///酒店等級
    _ddlLocationCode = $("#ddlLocationCode option:selected").val();///区域
    _ddlRoomTypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    _Room1ChildAge1 = $("#Room1ChildAge1 option:selected").val();///房间1儿童岁数
    _Room1ChildAge2 = $("#Room1ChildAge2 option:selected").val();///房间1儿童岁数
    _Room1Count = $("#Room1Count option:selected").val();///房间1房间数
    _ddlRoomTypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    _Room2ChildAge1 = $("#Room2ChildAge1 option:selected").val();///房间2儿童岁数
    _Room2ChildAge2 = $("#Room2ChildAge2 option:selected").val();///房间2儿童岁数
    _Room2Count = $("#Room2Count option:selected").val();///房间2房间数
    _ddlRoomTypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    _Room3ChildAge1 = $("#Room3ChildAge1 option:selected").val();///房间3儿童岁数
    _Room3ChildAge2 = $("#Room3ChildAge2 option:selected").val();///房间3儿童岁数
    _Room3Count = $("#Room3Count option:selected").val();///房间3房间数
    _ddlRoomTypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    _Room4ChildAge1 = $("#Room4ChildAge1 option:selected").val();///房间4儿童岁数
    _Room4ChildAge2 = $("#Room4ChildAge2 option:selected").val();///房间4儿童岁数
    _Room4Count = $("#Room4Count option:selected").val();///房间4房间数
    _ddlRoomTypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    _Room5ChildAge1 = $("#Room5ChildAge1 option:selected").val();///房间5儿童岁数
    _Room5ChildAge2 = $("#Room5ChildAge2 option:selected").val();///房间5儿童岁数
    _Room5Count = $("#Room5Count option:selected").val();///房间5房间数
    if (_nationality == "0")
    {
        alert("请选择国籍");
        return false;
    }
    if (_ddlRoomTypeCode1 == "0" && _ddlRoomTypeCode2 == "0" && _ddlRoomTypeCode3 == "0" && _ddlRoomTypeCode4 == "0" && _ddlRoomTypeCode5 == "0")
    {
        alert("请选择房型");
        return false;
    }
    if (_destinationValue == "")
    {
        alert("请输入目的地并选择");
        return false;
    }
    else if (_destinationValue != "") {
        $.post("../RTS/RtsPort.ashx?type=GetRTScityCodeYn", { destination: _destinationValue }, function (data) {
            if (data == "0") {
                alert("请输入目的地并选择");
                return false;
            }
        });
    }
    if (Number(_Room1Count) + Number(_Room2Count) + Number(_Room3Count) + Number(_Room4Count) + Number(_Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room1Count').value = 0;
        return false;
    }
    Get_RTShotelList();
}

function EasyUILoad() {
    $("<div class=\"datagrid-mask\"></div>").css({ display: "block", width: "100%", height: "auto !important" }).appendTo("body");
    $("<div class=\"datagrid-mask-msg\"></div>").html("<img  class ='img1' /> 正在运行，请稍候。。。").appendTo("body").css({ display: "block", left: ($(document.body).outerWidth(true) - 190) / 2, top: ($(window).height() - 45) / 2 });
}
 
function dispalyEasyUILoad() {
    $(".datagrid-mask").remove();
    $(".datagrid-mask-msg").remove();
}
//查询显示列表
function Get_RTShotelList() {
    var paramRoom1 = '&Room1ChildAge1=' + _Room1ChildAge1 + '&Room1ChildAge2=' + _Room1ChildAge2 + '&Room1Count=' + _Room1Count;
    var paramRoom2 = '&Room2ChildAge1=' + _Room2ChildAge1 + '&Room2ChildAge2=' + _Room2ChildAge2 + '&Room2Count=' + _Room2Count;
    var paramRoom3 = '&Room3ChildAge1=' + _Room3ChildAge1 + '&Room3ChildAge2=' + _Room3ChildAge2 + '&Room3Count=' + _Room3Count;
    var paramRoom4 = '&Room4ChildAge1=' + _Room4ChildAge1 + '&Room4ChildAge2=' + _Room4ChildAge2 + '&Room4Count=' +  _Room4Count;
    var paramRoom5 = '&Room5ChildAge1=' + _Room5ChildAge1 + '&Room4ChildAge2=' + _Room5ChildAge2 + '&Room5Count=' +  _Room5Count;
    var paramDeatail = paramRoom1 + paramRoom2 + paramRoom3 + paramRoom4 + paramRoom5;
    var paramPost = {};
    paramPost.type = "Get_RTSHotel";
    paramPost.HotelName = _HotelName;// 酒店名称
    paramPost.nationality = _nationality;//国籍
    paramPost.destination = _destinationValue;//目的地
    paramPost.beginTime = _beginTime;//入住时间
    paramPost.endTime = _endTime;//退房时间
    paramPost.ddlGreadeCode = _ddlGreadeCode;///酒店等級
    paramPost.ddlLocationCode = _ddlLocationCode;///区域
    paramPost.ddlRoomTypeCode1 = _ddlRoomTypeCode1;///房间1类型
    paramPost.Room1ChildAge1 = _Room1ChildAge1;///房间1儿童岁数
    paramPost.Room1ChildAge2 = _Room1ChildAge2;///房间1儿童岁数
    paramPost.Room1Count = _Room1Count;///房间1房间数
    paramPost.ddlRoomTypeCode2 = _ddlRoomTypeCode2;///房间2类型
    paramPost.Room2ChildAge1 = _Room2ChildAge1;///房间2儿童岁数
    paramPost.Room2ChildAge2 = _Room2ChildAge2;///房间2儿童岁数
    paramPost.Room2Count = _Room2Count;///房间2房间数
    paramPost.ddlRoomTypeCode3 = _ddlRoomTypeCode3;///房间3类型
    paramPost.Room3ChildAge1 = _Room3ChildAge1;///房间3儿童岁数
    paramPost.Room3ChildAge2 = _Room3ChildAge2;///房间3儿童岁数
    paramPost.Room3Count = _Room3Count;///房间3房间数
    paramPost.ddlRoomTypeCode4 = _ddlRoomTypeCode4;///房间4类型
    paramPost.Room4ChildAge1 = _Room4ChildAge1;///房间4儿童岁数
    paramPost.Room4ChildAge2 = _Room4ChildAge2;///房间4儿童岁数
    paramPost.Room4Count = _Room4Count;///房间4房间数
    paramPost.ddlRoomTypeCode5 = _ddlRoomTypeCode5;///房间5类型
    paramPost.Room5ChildAge1 = _Room5ChildAge1;///房间5儿童岁数
    paramPost.Room5ChildAge2 = _Room5ChildAge2;///房间5儿童岁数
    paramPost.Room5Count = _Room5Count;///房间5房间数
    var $width = $("#showHotelDiv").width();
    var $height = $(window).height();
    var editRow = undefined;
    var $datagrid = $("#RtsHotelList");
    $datagrid.datagrid({
        height: $height - 20,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: true,
        pagination: true,
        pageNumber: 1,
        pageSize: 20,//每页显示的记录条数，默认为10 
        pageList: [10, 15, 20, 30, 40, 60],//可以设置每页记录条数的列表 
        rownumbers: true,
        url: '../RTS/RtsPort.ashx',
        queryParams: paramPost,
        idField: 'NUMBER',
        columns: [[
            { field: 'NUMBER', title: 'ItemCode', hidden: true },

            {
                field: 'ItemName', title: '酒店名称', align: 'center', width: '30%',
                formatter: function (value, row) {
                    return "<a href=\"javascript:void(0)\" onclick=\"top.addTab('" + row.ItemName + "' + '酒店详情','../RTS/RTSHotelDetail.html?ItemCode='+ '" + row.ItemCode + "' + '&HotelName=' + '" + row.ItemName + "'+ '&nationality=' + '" + _nationality + "'+ '&destination=' + '" + row.CityCode + "'+ '&beginTime=' + '" + _beginTime + "'+ '&endTime=' + '" + _endTime + "'+ '&ddlRoomTypeCode1=' + '" + _ddlRoomTypeCode1 + "'+ '&ddlRoomTypeCode2=' + '" + _ddlRoomTypeCode2 + "'+ '&ddlRoomTypeCode3=' + '" + _ddlRoomTypeCode3 + "'+ '&ddlRoomTypeCode4=' + '" + _ddlRoomTypeCode4 + "'+ '&ddlRoomTypeCode5=' + '" + _ddlRoomTypeCode5 + "'+  '" + paramDeatail + "')\" > " + row.ItemName + " </a>";
                }
            },
            {
                field: 'ItemGradeCode', width: '20%', title: "星级", align: 'center', formatter: function (value, row, index) {
                    var repayHtml = "<div>";
                    var strHtml = "";
                    var j = row.ItemGradeCode;
                    if (j != "") {
                        for (var i = 1; i <= j; i++) {
                            strHtml += "<svg width=\"14px\" height=\"14px\" viewBox=\"0 0 512 512\" xml:space=\"preserve\" style=\"margin:0 1px;\"><polygon fill=\"#FFEA00\" stroke=\"#C1AB60\" stroke-width=\"37.6152\" stroke-linecap=\"round\" stroke-linejoin=\"round\" stroke-miterlimit=\"10\" points=\"259.216,29.942 330.27,173.919 489.16,197.007 374.185,309.08 401.33,467.31 259.216,392.612 117.104,467.31 144.25,309.08 29.274,197.007 188.165,173.919 \"></polygon></svg>";
                        }
                        return repayHtml + strHtml + "</div>";
                    }
                    else { return row.ItemGradeCode }
                } 
            },
            {
                field: 'Address', width: '25%', title: "地址", align: 'center', editor: { type: 'text', options: { required: true } }
            },
            {
                field: 'PhoneNo', width: '15%', title: "联系电话", align: 'center', editor: { type: 'text', options: { required: true } }
            },
            {
                field: 'FaxNo', width: '10%', title: "传真", align: 'center', editor: { type: 'text', options: { required: true } }
            }//,
            //{
            //    field: 'ItemCode', width: '5%', title: "操作", align: 'center', formatter: function (value, row, index) {
            //        return  "<a href=\"javascript:void(0)\" onclick=\"book()\"> 预订 </a>";
            //    } 
            //}
        ]],
        
        onBeforeLoad: function (data) {
            EasyUILoad();
        },
        onLoadSuccess: function (data) {
            dispalyEasyUILoad();
            //$(this).datagrid("autoMergeCells", ['ItemName', 'StarRating']);
        }
    });
   var p = $("#RtsHotelList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字 
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}


//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}