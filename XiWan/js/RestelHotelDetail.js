﻿var _hotelID;
var _HotelName;
var _ProvinceCode;
var _nationality;
var _beginTime;
var _endTime;

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
$(function () {
    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });

    $("#Destination").combobox({
        valueField: 'value',
        textField: 'name',
        url: "Restel/RestelPort.ashx?type=Get_Destination",
        mode: "remote", //从服务器加载就设置为‘remote‘
        hasDownArrow: false,  //为true时显示下拉选项图标
        onBeforeLoad: function (parm) {  //在请求加载数据之前触发，返回 false 则取消加载动作         
            var value = $(this).combobox("getValue");
            if (value) {
                parm.paramName = value;
                return true;
            }
            return false;
        }
    })
});

//初始页面
$(document).ready(function () {
    _hotelID = GetQueryString("HotelID");
    _HotelName = GetQueryString("HotelName");
    _ProvinceCode = GetQueryString("ProvinceCode");
    _nationality = GetQueryString("NationalityID");
    _beginTime = GetQueryString("CheckInDate");
    _endTime = GetQueryString("CheckOutDate");
    Rooms = GetQueryString("Rooms");
    var ArrayRooms = Rooms.split(";");
    var ArrayRoom1 = ArrayRooms[0].split(",");
    var ArrayRoom2 = ArrayRooms[1].split(",");
    var ArrayRoom3 = ArrayRooms[2].split(",");
    document.getElementById('Room1Adult').value = ArrayRoom1[0];
    document.getElementById('Room1Child').value = ArrayRoom1[1];
    document.getElementById('Room1ChildAge1').value = ArrayRoom1[2];
    document.getElementById('Room1ChildAge2').value = ArrayRoom1[3];
    document.getElementById('Room1Count').value = ArrayRoom1[4];
    document.getElementById('Room2Adult').value = ArrayRoom2[0];
    document.getElementById('Room2Child').value = ArrayRoom2[1];
    document.getElementById('Room2ChildAge1').value = ArrayRoom2[2];
    document.getElementById('Room2ChildAge2').value = ArrayRoom2[3];
    document.getElementById('Room2Count').value = ArrayRoom2[4];
    document.getElementById('Room3Adult').value = ArrayRoom3[0];
    document.getElementById('Room3Child').value = ArrayRoom3[1];
    document.getElementById('Room3ChildAge1').value = ArrayRoom3[2];
    document.getElementById('Room3ChildAge2').value = ArrayRoom3[3];
    document.getElementById('Room3Count').value = ArrayRoom3[4];
    $('#starDate').datebox("setValue", _beginTime);
    $('#stopDate').datebox("setValue", _endTime);

    Get_RestelHotelDetailList( Rooms);
});


//查询
function search_Click() {
    
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    var room1 = $("#Room1Adult option:selected").val() + "," + $("#Room1Child option:selected").val() + "," + $("#Room1ChildAge1").val() + "," + $("#Room1ChildAge2").val() + "," + $("#Room1Count").val() + ";";
    var room2 = $("#Room2Adult option:selected").val() + "," + $("#Room2Child option:selected").val() + "," + $("#Room2ChildAge1").val() + "," + $("#Room2ChildAge2").val() + "," + $("#Room2Count").val() + ";";
    var room3 = $("#Room3Adult option:selected").val() + "," + $("#Room3Child option:selected").val() + "," + $("#Room3ChildAge1").val() + "," + $("#Room3ChildAge2").val() + "," + $("#Room3Count").val();
    var rooms = room1 + room2 + room3;
    Get_RestelHotelDetailList(rooms);
}

//展示列表 _ddlRoom房间数，_rooms需要预订房间详情，type:0为缓存，1为实时
function Get_RestelHotelDetailList(_rooms) {

    _beginTime = $('#_easyui_textbox_input5').val();//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间

    var $width = $("#showHotelDetailDiv").width();
    var $height = $(window).height();
    var $datagrid = $("#RestelHotelDetailList");
    $datagrid.datagrid({
        height: $height - 92,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: false,
        pagination: true,
        pageSize: 20,//每页显示的记录条数，默认为20
        pageList: [10, 15, 20, 30, 40, 60],//可以设置每页记录条数的列表
        rownumbers: true,
        url: '../Restel/RestelPort.ashx',
        queryParams: { type: 'GetRealHotelSQuote', hotelCode: _hotelID, HotelName: _HotelName, nationality : _nationality, CheckInDate: _beginTime, CheckOutDate: _endTime, Rooms: _rooms },
        idField: 'ID',
        columns: [[
        { field: 'ID', checkbox: true },
        { field: 'HotelID', width: '10%', title: "HotelID", align: 'center', editor: { type: 'text', options: { required: true } } },
        {
            field: 'ClassName', width: '25%', title: "房型", align: 'center', editor: { type: 'text', options: { required: true } }
        }
        ,
        {
            field: 'RoomAdults', width: '10%', title: "成人数", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'RoomChilds', width: '10%', title: "儿童数", align: 'center', editor: { type: 'text', options: { required: true } }
        }
        ,
        {
            field: 'RMBprice', width: '15%', title: "价格(人民币)", align: 'center', editor: { type: 'text', options: { required: true } }
        }
        ,
        {
            field: 'TotalPrice', width: '15%', title: "价格", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'CurrencyCode', width: '10%', title: "币种", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'IsBreakfast', width: '10%', title: "早餐", align: 'center', formatter: function (value, row, index) {
                if (row.IsBreakfast == "1")
                    return "包含早餐";
                else {
                    return "不含早餐";
                }
            }
        }
        ]],
        toolbar: [{
            text: '取消期限', iconCls: 'icon-search', handler: function () {
                var rows = $(":checkbox[name=ID]:checked");
                var IDs = new Array();
                if (rows.length>0) {
                    $.each(rows, function (index, item) {
                        IDs[index] = item.value;
                    });
                    paramPost = {};
                    paramPost.HotelPriceID = IDs.toString();
                    $.post("../Restel/RestelPort.ashx?type=Get_CancellationPolicy", paramPost, function (data) {
                        if (data.code == "99") {
                            alert(data.mes)
                        }
                        else { alert("获取成功:" + data.mes); }
                    });
                }
                else {
                    $.messager.alert({
                        title: '系统提示',
                        msg: '请选择需要查询酒店所在的行'
                    });
                }
            }
        }, '-', {
            text: '预订', iconCls: 'icon-search', handler: function () {
                var rows = $(":checkbox[name=ID]:checked");
                var IDs = new Array();
                if (rows.length > 0) {
                    $.each(rows, function (index, item) {
                        IDs[index] = item.value;
                    });
                    paramPost = {};
                    paramPost.HotelPriceID = IDs.toString();
                    $.post("../Restel/RestelPort.ashx?type=RestelReserva", paramPost, function (data) {
                        if (data.code == "99") {
                            alert(data.mes)
                        }
                        else { alert(data.mes); }
                    });
                }
                else {
                    $.messager.alert({
                        title: '系统提示',
                        msg: '请选择需要查询酒店所在的行'
                    });
                }
            }
        }, '-', {
            text: '查看酒店评论', iconCls: 'icon-search', handler: function () {
                paramPost = {};
                paramPost.HotelCode = _hotelID;
                paramPost.CheckInDate = _beginTime;
                paramPost.CheckOutDate = _endTime;
                $.post("../Restel/RestelPort.ashx?type=GetHotelRemarks", paramPost, function (data) {
                    if (data.code == "99") {
                        alert(data.mes)
                    }
                    else { alert(data.mes); }
                });
            }
        }],
        onLoadSuccess: function (data) {
            //$datagrid.datagrid("uncheckAll");//再取消选中所有行
            if (data.total == 0) {
                alert("没有数据！");
            }
        }
    });
    var p = $("#RestelHotelDetailList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}

function CancelDeadline(HotelID, HotelPriceID) {
    var paramPost = {};
    paramPost.beginTime = _beginTime//国籍
    paramPost.endTime = _endTime;//目的地
    paramPost.Adults = AdultCount;
    paramPost.Childs = ChildsCount;
    paramPost.HotelID = HotelID;
    paramPost.nationalityID = _nationality
    paramPost.HotelPriceID = HotelPriceID
    $.post("../Restel/RestelPort.ashx?type=Get_CancellationPolicy", paramPost, function (data) {
        if (data.code == "99") {
            alert(data.mes)
        }
        else { alert("获取成功:" + data.mes); }
    });
}

function read(HotelID, HotelPriceID) {
    var paramPost = {};
    paramPost.beginTime = _beginTime//国籍
    paramPost.endTime = _endTime;//目的地
    paramPost.Adults = AdultCount;
    paramPost.Childs = ChildsCount;
    paramPost.HotelID = HotelID;
    paramPost.nationalityID = _nationality
    paramPost.HotelPriceID = HotelPriceID
    $.post("../Restel/RestelPort.ashx?type=GetHotelRecheckPrice", paramPost, function (data) {
        if (data.code == "99") {
            alert(data.mes);
        }
        else
        {
            if (data.result == "AL") {
                alert("可预订，金额为：" + data.mes);
            }
            else { alert("不可以预订");}
        }
    });
}
function book(HotelID, HotelPriceID) {
    var paramPost = {};
    paramPost.beginTime = _beginTime//国籍
    paramPost.endTime = _endTime;//目的地
    paramPost.Adults = AdultCount;
    paramPost.Childs = ChildsCount;
    paramPost.HotelID = HotelID;
    paramPost.nationalityID = _nationality
    paramPost.HotelPriceID = HotelPriceID;
    if (window.confirm("请确认是否已查看取消日期及查阅提示？") == true) {
        //("确定");
        //return true;
        $.post("../Restel/RestelPort.ashx?type=CreatePNR", paramPost, function (data) {
            if (data.code == "99") {
                alert(data.mes)
            }
            else {
                if (data.rseult == "RQ")
                    alert("根据要求（无法获得预订确认）");
                else if (data.result == "VC")
                    alert("已扣除确认和付款 / 信用");
                else if (data.result == "OK")
                    alert("根据分配确认");
                else if (data.result == "OKX")
                    alert("使用XML供应商确认");
                else if (data.result == "OKS")
                    alert("已收到确认和书面确认");
                else if (data.result == "XX")
                    alert("已取消");
                else if (data.result == "XXX")
                    alert("已取消");
                else if (data.result == "XXS")
                    alert("已取消");
                else
                    alert(data.result);
            }
        });
    }
    else {
        //("取消");
        return false;
    }
}


//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}



//查询
function searchOut_Click() {

    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    var room1 = $("#Room1Adult option:selected").val() + "," + $("#Room1Child option:selected").val() + "," + $("#Room1ChildAge1").val() + "," + $("#Room1ChildAge2").val() + "," + $("#Room1Count").val() + ";";
    var room2 = $("#Room2Adult option:selected").val() + "," + $("#Room2Child option:selected").val() + "," + $("#Room2ChildAge1").val() + "," + $("#Room2ChildAge2").val() + "," + $("#Room2Count").val() + ";";
    var room3 = $("#Room3Adult option:selected").val() + "," + $("#Room3Child option:selected").val() + "," + $("#Room3ChildAge1").val() + "," + $("#Room3ChildAge2").val() + "," + $("#Room3Count").val();
    var rooms = room1 + room2 + room3;
    GetRoomTypeOutPrice(rooms);
}

//展示列表 _ddlRoom房间数，_rooms需要预订房间详情，type:0为缓存，1为实时
function GetRoomTypeOutPrice(_rooms) {

    _beginTime = $('#_easyui_textbox_input5').val();//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    var paramPost = {};
    paramPost.hotelCode= _hotelID;
    paramPost.HotelName= _HotelName;
    paramPost.nationality = _nationality;
    paramPost.CheckInDate = _beginTime;
    paramPost.CheckOutDate = _endTime;
    paramPost.Room1Count = $("#Room1Count").val()
    paramPost.Rooms = _rooms;
    $.post("../Restel/Restel.ashx?type=GetRoomTypePrice", paramPost, function (data) {

    });
}