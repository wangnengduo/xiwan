﻿var _ItemCode;
var _HotelName;
var _nationality;//国籍
var _destinationValue;//目的地
var _beginTime;//入住时间
var _endTime;//退房时间
var _ddlGreadeCode;///酒店等級
var _ddlLocationCode;///区域
var _ddlRoomTypeCode1 = "0";///房间1类型
var _Room1ChildAge1;///房间1儿童岁数
var _Room1ChildAge2;///房间1儿童岁数
var _Room1Count;///房间1房间数
var _ddlRoomTypeCode2 = "0";///房间2类型
var _Room2ChildAge1;///房间2儿童岁数
var _Room2ChildAge2;///房间2儿童岁数
var _Room2Count;///房间2房间数
var _ddlRoomTypeCode3 = "0";///房间3类型
var _Room3ChildAge1;///房间3儿童岁数
var _Room3ChildAge2;///房间3儿童岁数
var _Room3Count;///房间3房间数
var _ddlRoomTypeCode4 = "0";///房间4类型
var _Room4ChildAge1;///房间4儿童岁数
var _Room4ChildAge2;///房间4儿童岁数
var _Room4Count;///房间4房间数
var _ddlRoomTypeCode5 = "0";///房间5类型
var _Room5ChildAge1;///房间5儿童岁数
var _Room5ChildAge2;///房间5儿童岁数
var _Room5Count;///房间5房间数

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}

$(document).ready(function () {
    InitializationChildAge()

    _ItemCode = GetQueryString("ItemCode");
    _HotelName = GetQueryString("HotelName");// 酒店名称
    _nationality = GetQueryString("nationality");//国籍
    _destinationValue = GetQueryString("destination");//目的地
    _beginTime = GetQueryString("beginTime");
    _endTime = GetQueryString("endTime");
    _ddlRoomTypeCode1 = GetQueryString("ddlRoomTypeCode1");
    _ddlRoomTypeCode2 = GetQueryString("ddlRoomTypeCode2");
    _ddlRoomTypeCode3 = GetQueryString("ddlRoomTypeCode3");
    _ddlRoomTypeCode4 = GetQueryString("ddlRoomTypeCode4");
    _ddlRoomTypeCode5 = GetQueryString("ddlRoomTypeCode5");
    document.getElementById('ddlRoomTypeCode1').value = _ddlRoomTypeCode1;
    document.getElementById('ddlRoomTypeCode2').value = _ddlRoomTypeCode2;
    document.getElementById('ddlRoomTypeCode3').value = _ddlRoomTypeCode3;
    document.getElementById('ddlRoomTypeCode4').value = _ddlRoomTypeCode4;
    document.getElementById('ddlRoomTypeCode5').value = _ddlRoomTypeCode5;
    ChangeRoomType1(_ddlRoomTypeCode1);
    ChangeRoomType2(_ddlRoomTypeCode2);
    ChangeRoomType3(_ddlRoomTypeCode3);
    ChangeRoomType4(_ddlRoomTypeCode4);
    ChangeRoomType5(_ddlRoomTypeCode5);
    _Room1ChildAge1 = GetQueryString("Room1ChildAge1");
    _Room1ChildAge2 = GetQueryString("Room1ChildAge2");
    _Room1Count = GetQueryString("Room1Count");
    _Room2ChildAge1 = GetQueryString("Room2ChildAge1");
    _Room2ChildAge2 = GetQueryString("Room2ChildAge2");
    _Room2Count = GetQueryString("Room2Count");
    _Room3ChildAge1 = GetQueryString("Room3ChildAge1");
    _Room3ChildAge2 = GetQueryString("Room3ChildAge2");
    _Room3Count = GetQueryString("Room3Count");
    _Room4ChildAge1 = GetQueryString("Room4ChildAge1");
    _Room4ChildAge2 = GetQueryString("Room4ChildAge2");
    _Room4Count = GetQueryString("Room4Count");
    _Room5ChildAge1 = GetQueryString("Room5ChildAge1");
    _Room5ChildAge2 = GetQueryString("Room5ChildAge2");
    _Room5Count = GetQueryString("Room5Count");
    document.getElementById('Room1ChildAge1').value = _Room1ChildAge1;
    document.getElementById('Room1ChildAge2').value = _Room1ChildAge2;
    document.getElementById('Room2ChildAge1').value = _Room2ChildAge1;
    document.getElementById('Room2ChildAge2').value = _Room2ChildAge2;
    document.getElementById('Room3ChildAge1').value = _Room3ChildAge1;
    document.getElementById('Room3ChildAge2').value = _Room3ChildAge2;
    document.getElementById('Room4ChildAge1').value = _Room4ChildAge1;
    document.getElementById('Room4ChildAge2').value = _Room4ChildAge2;
    document.getElementById('Room5ChildAge1').value = _Room5ChildAge1;
    document.getElementById('Room5ChildAge2').value = _Room5ChildAge2;
    document.getElementById('Room1Count').value = _Room1Count;
    document.getElementById('Room2Count').value = _Room2Count;
    document.getElementById('Room3Count').value = _Room3Count;
    document.getElementById('Room4Count').value = _Room4Count;
    document.getElementById('Room5Count').value = _Room5Count;

    Get_RTShotelList();
    });

$(function () {

    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });

    $('#starDate').datebox("setValue", _beginTime);
    $('#stopDate').datebox("setValue", _endTime);
});

//初始儿童岁数  为不可操作
function InitializationChildAge() {
    document.getElementById("Room1ChildAge1").disabled = true;
    document.getElementById("Room1ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room1ChildAge2").disabled = true;
    document.getElementById("Room1ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room1Count").disabled = true;
    document.getElementById("Room1Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room2ChildAge1").disabled = true;
    document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room2ChildAge2").disabled = true;
    document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room2Count").disabled = true;
    document.getElementById("Room2Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room3ChildAge1").disabled = true;
    document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room3ChildAge2").disabled = true;
    document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room3Count").disabled = true;
    document.getElementById("Room3Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room4ChildAge1").disabled = true;
    document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room4ChildAge2").disabled = true;
    document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room4Count").disabled = true;
    document.getElementById("Room4Count").style.backgroundColor = "#dddddd";

    document.getElementById("Room5ChildAge1").disabled = true;
    document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
    document.getElementById("Room5ChildAge2").disabled = true;
    document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
    document.getElementById("Room5Count").disabled = true;
    document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
}

function ChangeRoomType1(RoomType) {
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型

    if (RoomType != "0") {
        if (TypeCode2 != "0" || TypeCode3 != "0" || TypeCode4 != "0" || TypeCode5 != "0") {
            var Room2Count = $("#Room2Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room1Count").disabled = false;
        document.getElementById("Room1Count").style.backgroundColor = "#fff";
        document.getElementById('Room1Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            //document.getElementById('Room1Count').value = 1;
            document.getElementById('Room1ChildAge1').value = 1;
            document.getElementById("Room1ChildAge1").disabled = false;
            document.getElementById("Room1ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room1ChildAge2").disabled = false;
            document.getElementById("Room1ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room1Count").disabled = true;
            document.getElementById("Room1Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room1ChildAge1").disabled = true;
            document.getElementById("Room1ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room1ChildAge1').value = 0;
            document.getElementById("Room1ChildAge2").disabled = true;
            document.getElementById("Room1ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room1ChildAge2').value = 0;
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode2').value = 0;
                document.getElementById("Room2ChildAge1").disabled = true;
                document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge1').value = 0;
                document.getElementById("Room2ChildAge2").disabled = true;
                document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge2').value = 0;
                document.getElementById("Room2Count").disabled = true;
                document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room2Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room5Count = $("#Room5Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room5Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room1ChildAge1").disabled = true;
        document.getElementById("Room1ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room1ChildAge1').value = 0;
        document.getElementById("Room1ChildAge2").disabled = true;
        document.getElementById("Room1ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room1ChildAge2').value = 0;
        document.getElementById("Room1Count").disabled = true;
        document.getElementById("Room1Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room1Count').value = 0;
    }
}

function ChangeRoomType2(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode3 != "0" || TypeCode4 != "0" || TypeCode5 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room1Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room2Count").disabled = false;
        document.getElementById("Room2Count").style.backgroundColor = "#fff";
        document.getElementById('Room2Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room2ChildAge1').value = 1;
            document.getElementById("Room2ChildAge1").disabled = false;
            document.getElementById("Room2ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room2ChildAge2").disabled = false;
            document.getElementById("Room2ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room2Count").disabled = true;
            document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room2ChildAge1").disabled = true;
            document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room2ChildAge1').value = 0;
            document.getElementById("Room2ChildAge2").disabled = true;
            document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room2ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode2').value = 0;
                document.getElementById("Room2ChildAge1").disabled = true;
                document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge1').value = 0;
                document.getElementById("Room2ChildAge2").disabled = true;
                document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room2ChildAge2').value = 0;
                document.getElementById("Room2Count").disabled = true;
                document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room2Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room5Count = $("#Room5Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room5Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room2ChildAge1").disabled = true;
        document.getElementById("Room2ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room2ChildAge1').value = 0;
        document.getElementById("Room2ChildAge2").disabled = true;
        document.getElementById("Room2ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room2ChildAge2').value = 0;
        document.getElementById("Room2Count").disabled = true;
        document.getElementById("Room2Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room2Count').value = 0;
    }
}

function ChangeRoomType3(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode2 != "0" || TypeCode4 != "0" || TypeCode5 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room2Count = $("#Room2Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room1Count) + Number(Room2Count) + Number(Room4Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room3Count").disabled = false;
        document.getElementById("Room3Count").style.backgroundColor = "#fff";
        document.getElementById('Room3Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room3ChildAge1').value = 1;
            document.getElementById("Room3ChildAge1").disabled = false;
            document.getElementById("Room3ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room3ChildAge2").disabled = false;
            document.getElementById("Room3ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room3Count").disabled = true;
            document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room3ChildAge1").disabled = true;
            document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room3ChildAge1').value = 0;
            document.getElementById("Room3ChildAge2").disabled = true;
            document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room3ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode3').value = 0;
                document.getElementById("Room3ChildAge1").disabled = true;
                document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge1').value = 0;
                document.getElementById("Room3ChildAge2").disabled = true;
                document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room3ChildAge2').value = 0;
                document.getElementById("Room3Count").disabled = true;
                document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room3Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room5Count = $("#Room5Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room5Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room3ChildAge1").disabled = true;
        document.getElementById("Room3ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room3ChildAge1').value = 0;
        document.getElementById("Room3ChildAge2").disabled = true;
        document.getElementById("Room3ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room3ChildAge2').value = 0;
        document.getElementById("Room3Count").disabled = true;
        document.getElementById("Room3Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room3Count').value = 0;
    }
}

function ChangeRoomType4(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode2 != "0" || TypeCode3 != "0" || TypeCode5 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room2Count = $("#Room2Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room5Count = $("#Room5Count option:selected").val();
            if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room5Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room4Count").disabled = false;
        document.getElementById("Room4Count").style.backgroundColor = "#fff";
        document.getElementById('Room4Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room4ChildAge1').value = 1;
            document.getElementById("Room4ChildAge1").disabled = false;
            document.getElementById("Room4ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room4ChildAge2").disabled = false;
            document.getElementById("Room4ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room4Count").disabled = true;
            document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room4ChildAge1").disabled = true;
            document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room4ChildAge1').value = 0;
            document.getElementById("Room4ChildAge2").disabled = true;
            document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room4ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode4').value = 0;
                document.getElementById("Room4ChildAge1").disabled = true;
                document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge1').value = 0;
                document.getElementById("Room4ChildAge2").disabled = true;
                document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room4ChildAge2').value = 0;
                document.getElementById("Room4Count").disabled = true;
                document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room4Count').value = 0;
            }
            if (RoomType == TypeCode5) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room4Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room4ChildAge1").disabled = true;
        document.getElementById("Room4ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room4ChildAge1').value = 0;
        document.getElementById("Room4ChildAge2").disabled = true;
        document.getElementById("Room4ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room4ChildAge2').value = 0;
        document.getElementById("Room4Count").disabled = true;
        document.getElementById("Room4Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room4Count').value = 0;
    }
}

function ChangeRoomType5(RoomType) {
    var TypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    var TypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    var TypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    var TypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    if (RoomType != "0") {
        if (TypeCode1 != "0" || TypeCode2 != "0" || TypeCode3 != "0" || TypeCode4 != "0") {
            var Room1Count = $("#Room1Count option:selected").val();
            var Room2Count = $("#Room2Count option:selected").val();
            var Room3Count = $("#Room3Count option:selected").val();
            var Room4Count = $("#Room4Count option:selected").val();
            if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + 1 > 5) {
                alert("最多能同时搜寻5间房间，请重新选择。");
                return false;
            }
        }
        document.getElementById("Room5Count").disabled = false;
        document.getElementById("Room5Count").style.backgroundColor = "#fff";
        document.getElementById('Room5Count').value = 1;
        if (RoomType == "BED10" || RoomType == "BED11" || RoomType == "BED12" || RoomType == "BED13" || RoomType == "BED14") {
            document.getElementById('Room5ChildAge1').value = 1;
            document.getElementById("Room5ChildAge1").disabled = false;
            document.getElementById("Room5ChildAge1").style.backgroundColor = "#fff";
            document.getElementById("Room5ChildAge2").disabled = false;
            document.getElementById("Room5ChildAge2").style.backgroundColor = "#fff";
            document.getElementById("Room5Count").disabled = true;
            document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
        }
        else {
            document.getElementById("Room5ChildAge1").disabled = true;
            document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
            document.getElementById('Room5ChildAge1').value = 0;
            document.getElementById("Room5ChildAge2").disabled = true;
            document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
            document.getElementById('Room5ChildAge2').value = 0;
            if (RoomType == TypeCode1) {
                var Room1Count = $("#Room1Count option:selected").val();
                document.getElementById('Room1Count').value = Number(Room1Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
            if (RoomType == TypeCode2) {
                var Room2Count = $("#Room2Count option:selected").val();
                document.getElementById('Room2Count').value = Number(Room2Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
            if (RoomType == TypeCode3) {
                var Room3Count = $("#Room3Count option:selected").val();
                document.getElementById('Room3Count').value = Number(Room3Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
            if (RoomType == TypeCode4) {
                var Room4Count = $("#Room4Count option:selected").val();
                document.getElementById('Room4Count').value = Number(Room4Count) + 1;
                document.getElementById('ddlRoomTypeCode5').value = 0;
                document.getElementById("Room5ChildAge1").disabled = true;
                document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge1').value = 0;
                document.getElementById("Room5ChildAge2").disabled = true;
                document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
                document.getElementById('Room5ChildAge2').value = 0;
                document.getElementById("Room5Count").disabled = true;
                document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
                document.getElementById('Room5Count').value = 0;
            }
        }
    }
    else {
        document.getElementById("Room5ChildAge1").disabled = true;
        document.getElementById("Room5ChildAge1").style.backgroundColor = "#dddddd";
        document.getElementById('Room5ChildAge1').value = 0;
        document.getElementById("Room5ChildAge2").disabled = true;
        document.getElementById("Room5ChildAge2").style.backgroundColor = "#dddddd";
        document.getElementById('Room5ChildAge2').value = 0;
        document.getElementById("Room5Count").disabled = true;
        document.getElementById("Room5Count").style.backgroundColor = "#dddddd";
        document.getElementById('Room5Count').value = 0;
    }
}

function ChangeRoom1Count(Room1Count) {
    var Room2Count = $("#Room2Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room1Count').value = 0;
        return false;
    }
}

function ChangeRoom2Count(Room2Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room2Count').value = 0;
        return false;
    }
}

function ChangeRoom3Count(Room3Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room2Count = $("#Room2Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room3Count').value = 0;
        return false;
    }
}

function ChangeRoom4Count(Room4Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room2Count = $("#Room2Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room5Count = $("#Room5Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room4Count').value = 0;
        return false;
    }
}

function ChangeRoom5Count(Room4Count) {
    var Room1Count = $("#Room1Count option:selected").val();
    var Room2Count = $("#Room2Count option:selected").val();
    var Room3Count = $("#Room3Count option:selected").val();
    var Room4Count = $("#Room4Count option:selected").val();
    if (Number(Room1Count) + Number(Room2Count) + Number(Room3Count) + Number(Room4Count) + Number(Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room5Count').value = 0;
        return false;
    }
}

//查询
function search_Click() {
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    _ddlRoomTypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    _Room1ChildAge1 = $("#Room1ChildAge1 option:selected").val();///房间1儿童岁数
    _Room1ChildAge2 = $("#Room1ChildAge2 option:selected").val();///房间1儿童岁数
    _Room1Count = $("#Room1Count option:selected").val();///房间1房间数
    _ddlRoomTypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    _Room2ChildAge1 = $("#Room2ChildAge1 option:selected").val();///房间2儿童岁数
    _Room2ChildAge2 = $("#Room2ChildAge2 option:selected").val();///房间2儿童岁数
    _Room2Count = $("#Room2Count option:selected").val();///房间2房间数
    _ddlRoomTypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    _Room3ChildAge1 = $("#Room3ChildAge1 option:selected").val();///房间3儿童岁数
    _Room3ChildAge2 = $("#Room3ChildAge2 option:selected").val();///房间3儿童岁数
    _Room3Count = $("#Room3Count option:selected").val();///房间3房间数
    _ddlRoomTypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    _Room4ChildAge1 = $("#Room4ChildAge1 option:selected").val();///房间4儿童岁数
    _Room4ChildAge2 = $("#Room4ChildAge2 option:selected").val();///房间4儿童岁数
    _Room4Count = $("#Room4Count option:selected").val();///房间4房间数
    _ddlRoomTypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    _Room5ChildAge1 = $("#Room5ChildAge1 option:selected").val();///房间5儿童岁数
    _Room5ChildAge2 = $("#Room5ChildAge2 option:selected").val();///房间5儿童岁数
    _Room5Count = $("#Room5Count option:selected").val();///房间5房间数
    if (_ddlRoomTypeCode1 == "0" && _ddlRoomTypeCode2 == "0" && _ddlRoomTypeCode3 == "0" && _ddlRoomTypeCode4 == "0" && _ddlRoomTypeCode5 == "0") {
        alert("请选择房型");
        return false;
    }
    if (Number(_Room1Count) + Number(_Room2Count) + Number(_Room3Count) + Number(_Room4Count) + Number(_Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room1Count').value = 0;
        return false;
    }
    Get_RTShotelList();
}

function EasyUILoad() {
    $("<div class=\"datagrid-mask\"></div>").css({ display: "block", width: "100%", height: "auto !important" }).appendTo("body");
    $("<div class=\"datagrid-mask-msg\"></div>").html("<img  class ='img1' /> 正在运行，请稍候。。。").appendTo("body").css({ display: "block", left: ($(document.body).outerWidth(true) - 190) / 2, top: ($(window).height() - 45) / 2 });
}

function dispalyEasyUILoad() {
    $(".datagrid-mask").remove();
    $(".datagrid-mask-msg").remove();
}

//查询显示列表
function Get_RTShotelList() {
    var paramPost = {};
    paramPost.type = "Get_RTSHotelInformation";
    paramPost.ItemCode = _ItemCode;
    paramPost.HotelName = _HotelName;// 酒店名称
    paramPost.nationality = _nationality//国籍
    paramPost.destination = _destinationValue;//目的地
    paramPost.beginTime = _beginTime;//入住时间
    paramPost.endTime = _endTime;//退房时间
    paramPost.ddlRoomTypeCode1 = _ddlRoomTypeCode1;///房间1类型
    paramPost.Room1ChildAge1 = _Room1ChildAge1;///房间1儿童岁数
    paramPost.Room1ChildAge2 = _Room1ChildAge2;///房间1儿童岁数
    paramPost.Room1Count = _Room1Count;///房间1房间数
    paramPost.ddlRoomTypeCode2 = _ddlRoomTypeCode2;///房间2类型
    paramPost.Room2ChildAge1 = _Room2ChildAge1;///房间2儿童岁数
    paramPost.Room2ChildAge2 = _Room2ChildAge2;///房间2儿童岁数
    paramPost.Room2Count = _Room2Count;///房间2房间数
    paramPost.ddlRoomTypeCode3 = _ddlRoomTypeCode3;///房间3类型
    paramPost.Room3ChildAge1 = _Room3ChildAge1;///房间3儿童岁数
    paramPost.Room3ChildAge2 = _Room3ChildAge2;///房间3儿童岁数
    paramPost.Room3Count = _Room3Count;///房间3房间数
    paramPost.ddlRoomTypeCode4 = _ddlRoomTypeCode4;///房间4类型
    paramPost.Room4ChildAge1 = _Room4ChildAge1;///房间4儿童岁数
    paramPost.Room4ChildAge2 = _Room4ChildAge2;///房间4儿童岁数
    paramPost.Room4Count = _Room4Count;///房间4房间数
    paramPost.ddlRoomTypeCode5 = _ddlRoomTypeCode5;///房间5类型
    paramPost.Room5ChildAge1 = _Room5ChildAge1;///房间5儿童岁数
    paramPost.Room5ChildAge2 = _Room5ChildAge2;///房间5儿童岁数
    paramPost.Room5Count = _Room5Count;///房间5房间数
    var $width = $("#showHotelDiv").width();
    var $height = $(window).height();
    var editRow = undefined;
    var $datagrid = $("#RtsHotelList");
    $datagrid.datagrid({
        height: $height - 20,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: true,
        pagination: true,
        pageNumber: 1,
        pageSize: 20,//每页显示的记录条数，默认为10 
        pageList: [10, 15, 20, 30, 40, 60],//可以设置每页记录条数的列表 
        rownumbers: true,
        url: '../RTS_B2C/RtsPort_B2C.ashx',
        queryParams: paramPost,
        idField: 'NUMBER',
        columns: [[
            { field: 'NUMBER', title: 'ItemCode', hidden: true },

            {
                field: 'RoomTypeName', width: '25%', title: "房型", align: 'center', editor: { type: 'text', options: { required: true } }
            },
            {
                field: 'BreakfastTypeName', width: '15%', title: "早餐", align: 'center', formatter: function (value, row, index) {
                    if (row.BreakfastTypeName == "None" || row.BreakfastTypeName=="")
                        return "不含早餐";
                    else if (row.BreakfastTypeName == "Continental Breakfast")
                        return "包含早餐";
                    else
                    {
                        return "包含早餐";
                    }
                } 
            },
            {
                field: 'PriceStatus', width: '10%', title: "状态", align: 'center', formatter: function (value, row, index) {
                    if (row.PriceStatus == "Available")
                        return "可预订";
                    else
                        return "不可预订";
                } 
            },
            {
                field: 'SellerNetPrice', width: '20%', title: "价格", align: 'center', editor: { type: 'text', options: { required: true } }
            },
            {
                field: 'RoomTypeCode', width: '20%', title: "操作", align: 'center', formatter: function (value, row, index) {
                    var Action = "<a  href=\"javascript:void(0)\" onclick=\"CancelDeadline('" + row.ItemCode + "','" + row.ItemNo + "','" + row.RoomTypeCode + "')\">查看取消日期</a>\ | <a  href=\"javascript:void(0)\" onclick=\"read('" + row.ItemCode + "','" + row.ItemNo + "','" + row.RoomTypeCode + "')\">查阅提示</a>\ | <a  href=\"javascript:void(0)\" onclick=\"book('" + row.ItemCode + "','" + row.ItemNo + "','" + row.RoomTypeCode + "','" + row.BreakfastTypeName + "')\">预订</a>";
                    return Action;
                    //return  "<a href=\"javascript:void(0)\" onclick=\"book()\"> 预订 </a>";
                } 
            }
        ]],
        //toolbar: [{
        //    text: '查阅提示', iconCls: 'icon-search', handler: function () {
        //        var row = $datagrid.datagrid('getSelected');
        //        var index = $datagrid.datagrid('getRowIndex', row);
        //        if (row) {
        //            paramPost = {};
        //            paramPost.itemCode = row.ItemCode;
        //            paramPost.userName = 1;
        //            paramPost.password = 1;// 酒店名称
        //            paramPost.md5 = "1985bb2e80b3dc441cdad32527e6c4b7";
        //            $.post("../Services/GetHotelInformation.ashx?data=" + JSON.stringify(paramPost), "", function (data) {

        //            });
        //        }
        //        else {
        //            $.messager.alert({
        //                title: '系统提示',
        //                msg: '请选择需要查询酒店所在的行'
        //            });
        //        }
        //    }

        //}],
        onBeforeLoad: function (data) {
            //EasyUILoad();
        },
        onLoadSuccess: function (data) {
            if (data.total == 0) {
                alert("没有数据！");
            }
            //dispalyEasyUILoad();
            //$(this).datagrid("autoMergeCells", ['ItemName', 'StarRating']);
        }
    });
    var p = $("#RtsHotelList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字 
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}
//查看取消日期
function CancelDeadline(DeadlineItemCode, DeadlineItemNo, DeadlineRoomTypeCode)
{
    var paramCancelDeadline = "";
    if (_ddlRoomTypeCode1 != "0") {
        if (_ddlRoomTypeCode1 == "BED10" || _ddlRoomTypeCode1 == "BED11" || _ddlRoomTypeCode1 == "BED12" || _ddlRoomTypeCode1 == "BED13" || _ddlRoomTypeCode1 == "BED14") {
            var ChildAge = "";
            if (_Room1ChildAge1 != "0") {
                ChildAge += _Room1ChildAge1 + ",";
            }
            else { ChildAge += ","; }
            if (_Room1ChildAge2 != "0") {
                ChildAge += _Room1ChildAge2 + ",";
            }
            else { ChildAge += ","; }
            paramCancelDeadline += _ddlRoomTypeCode1 + "," + ChildAge + _Room1Count + ";";
        }
        else {
            paramCancelDeadline += _ddlRoomTypeCode1 + "," + "," + "," + _Room1Count + ";";
        }
    }
    if (_ddlRoomTypeCode2 != "0") {
        if (_ddlRoomTypeCode2 == "BED10" || _ddlRoomTypeCode2 == "BED11" || _ddlRoomTypeCode2 == "BED12" || _ddlRoomTypeCode2 == "BED13" || _ddlRoomTypeCode2 == "BED14") {
            var ChildAge = "";
            if (_Room2ChildAge1 != "0") {
                ChildAge += _Room2ChildAge1 + ",";
            }
            else { ChildAge += ","; }
            if (_Room2ChildAge2 != "0") {
                ChildAge += _Room2ChildAge2 + ",";
            }
            else { ChildAge += ","; }
            paramCancelDeadline += _ddlRoomTypeCode2 + "," + ChildAge + _Room2Count + ";";
        }
        else {
            paramCancelDeadline += _ddlRoomTypeCode2 + "," + "," + "," + _Room2Count + ";";
        }
    }
    if (_ddlRoomTypeCode3 != "0") {
        if (_ddlRoomTypeCode3 == "BED10" || _ddlRoomTypeCode3 == "BED11" || _ddlRoomTypeCode3 == "BED12" || _ddlRoomTypeCode3 == "BED13" || _ddlRoomTypeCode3 == "BED14") {
            var ChildAge = "";
            if (_Room3ChildAge1 != "0") {
                ChildAge += _Room3ChildAge1 + ",";
            }
            else { ChildAge += ","; }
            if (_Room3ChildAge2 != "0") {
                ChildAge += _Room3ChildAge2 + ",";
            }
            else { ChildAge += ","; }
            paramCancelDeadline += _ddlRoomTypeCode3 + "," + ChildAge + _Room3Count + ";";
        }
        else {
            paramCancelDeadline += _ddlRoomTypeCode3 + "," + "," + "," + _Room3Count + ";";
        }
    }
    if (_ddlRoomTypeCode4 != "0") {
        if (_ddlRoomTypeCode4 == "BED10" || _ddlRoomTypeCode4 == "BED11" || _ddlRoomTypeCode4 == "BED12" || _ddlRoomTypeCode4 == "BED13" || _ddlRoomTypeCode4 == "BED14") {
            var ChildAge = "";
            if (_Room4ChildAge1 != "0") {
                ChildAge += _Room4ChildAge1 + ",";
            }
            else { ChildAge += ","; }
            if (_Room4ChildAge2 != "0") {
                ChildAge += _Room4ChildAge2 + ",";
            }
            else { ChildAge += ","; }
            paramCancelDeadline += _ddlRoomTypeCode4 + "," + ChildAge + _Room4Count + ";";
        }
        else {
            paramCancelDeadline += _ddlRoomTypeCode4 + "," + "," + "," + _Room4Count + ";";
        }
    }
    if (_ddlRoomTypeCode5 != "0") {
        if (_ddlRoomTypeCode5 == "BED10" || _ddlRoomTypeCode5 == "BED11" || _ddlRoomTypeCode5 == "BED12" || _ddlRoomTypeCode5 == "BED13" || _ddlRoomTypeCode5 == "BED14") {
            var ChildAge = "";
            if (_Room5ChildAge1 != "0") {
                ChildAge += _Room5ChildAge1 + ",";
            }
            else { ChildAge += ","; }
            if (_Room5ChildAge2 != "0") {
                ChildAge += _Room5ChildAge2 + ",";
            }
            else { ChildAge += ","; }
            paramCancelDeadline += _ddlRoomTypeCode5 + "," + ChildAge + _Room5Count + ";";
        }
        else {
            paramCancelDeadline += _ddlRoomTypeCode5 + "," + "," + "," + _Room5Count + ";";
        }
    }
    $.post("../RTS_B2C/RtsPort_B2C.ashx?type=GetRTSCancelDeadline", { ItemCode: DeadlineItemCode, ItemNo: DeadlineItemNo, RoomTypeCode: DeadlineRoomTypeCode, beginTime: _beginTime, endTime: _endTime, nationality: _nationality, BedTypeCode: paramCancelDeadline }, function (data) {
        if (data.code == "99") {
            alert(data.mes)
        }
        else
        {
            var result = data.mes.split(";");
            var CancelDeadlineDate = result[0];
            var TypeCode = result[1];
            if (CancelDeadlineDate != "")
            {
                CancelDeadlineDate = "最后取消日期为:" + CancelDeadlineDate;
            }
            if (TypeCode == "Normal")
                alert("获取成功:" + CancelDeadlineDate + "  在截止日期之后和入住前的修改或取消，从入住日期起100％的首晚房费100％预订。");
            else if (TypeCode == "Other")
                alert("获取成功:" + CancelDeadlineDate + "  截止日期后修改或取消，收取预订总额的100％。");
            else if (TypeCode == "Penalty")
                alert("获取成功:" + CancelDeadlineDate + "  确认预订后，如果修改/取消，请收取预订总额的100％。 （不退还的条件）");
            else 
                ("获取成功:" + data.mes);
        }
    });
}
function read(bookItemCode, bookItemNo, bookRoomTypeCode)
{
    var paramPost = {};
    paramPost.HotelName = _HotelName;// 酒店名称
    paramPost.nationality = _nationality//国籍
    paramPost.destination = _destinationValue;//目的地
    paramPost.beginTime = _beginTime;//入住时间
    paramPost.endTime = _endTime;//退房时间
    paramPost.ddlGreadeCode = _ddlGreadeCode;///酒店等級
    paramPost.ddlLocationCode = _ddlLocationCode;///区域
    paramPost.ddlRoomTypeCode1 = _ddlRoomTypeCode1;///房间1类型
    paramPost.Room1ChildAge1 = _Room1ChildAge1;///房间1儿童岁数
    paramPost.Room1ChildAge2 = _Room1ChildAge2;///房间1儿童岁数
    paramPost.Room1Count = _Room1Count;///房间1房间数
    paramPost.ddlRoomTypeCode2 = _ddlRoomTypeCode2;///房间2类型
    paramPost.Room2ChildAge1 = _Room2ChildAge1;///房间2儿童岁数
    paramPost.Room2ChildAge2 = _Room2ChildAge2;///房间2儿童岁数
    paramPost.Room2Count = _Room2Count;///房间2房间数
    paramPost.ddlRoomTypeCode3 = _ddlRoomTypeCode3;///房间3类型
    paramPost.Room3ChildAge1 = _Room3ChildAge1;///房间3儿童岁数
    paramPost.Room3ChildAge2 = _Room3ChildAge2;///房间3儿童岁数
    paramPost.Room3Count = _Room3Count;///房间3房间数
    paramPost.ddlRoomTypeCode4 = _ddlRoomTypeCode4;///房间4类型
    paramPost.Room4ChildAge1 = _Room4ChildAge1;///房间4儿童岁数
    paramPost.Room4ChildAge2 = _Room4ChildAge2;///房间4儿童岁数
    paramPost.Room4Count = _Room4Count;///房间4房间数
    paramPost.ddlRoomTypeCode5 = _ddlRoomTypeCode5;///房间5类型
    paramPost.Room5ChildAge1 = _Room5ChildAge1;///房间5儿童岁数
    paramPost.Room5ChildAge2 = _Room5ChildAge2;///房间5儿童岁数
    paramPost.Room5Count = _Room5Count;///房间5房间数
    paramPost.ItemCode = bookItemCode;
    paramPost.ItemNo = bookItemNo;
    paramPost.RoomTypeCode = bookRoomTypeCode;
    $.post("../RTS_B2C/RtsPort_B2C.ashx?type=GetRemarkHotelInformationForCustomerCount", paramPost, function (data) {
        if (data.code == "99") {
            alert(data.mes)
        }
        else { alert("获取成功:" + data.result) }
    });
}
function book(hotelCode, hotelNo, bookRoomTypeCode, breakfastTypeName) {
    var paramPost = {};
    paramPost.nationality = _nationality//国籍
    paramPost.destination = _destinationValue;//目的地
    paramPost.beginTime = _beginTime;//入住时间
    paramPost.endTime = _endTime;//退房时间
    paramPost.ddlRoomTypeCode1 = _ddlRoomTypeCode1;///房间1类型
    paramPost.Room1ChildAge1 = _Room1ChildAge1;///房间1儿童岁数
    paramPost.Room1ChildAge2 = _Room1ChildAge2;///房间1儿童岁数
    paramPost.Room1Count = _Room1Count;///房间1房间数
    paramPost.ddlRoomTypeCode2 = _ddlRoomTypeCode2;///房间2类型
    paramPost.Room2ChildAge1 = _Room2ChildAge1;///房间2儿童岁数
    paramPost.Room2ChildAge2 = _Room2ChildAge2;///房间2儿童岁数
    paramPost.Room2Count = _Room2Count;///房间2房间数
    paramPost.ddlRoomTypeCode3 = _ddlRoomTypeCode3;///房间3类型
    paramPost.Room3ChildAge1 = _Room3ChildAge1;///房间3儿童岁数
    paramPost.Room3ChildAge2 = _Room3ChildAge2;///房间3儿童岁数
    paramPost.Room3Count = _Room3Count;///房间3房间数
    paramPost.ddlRoomTypeCode4 = _ddlRoomTypeCode4;///房间4类型
    paramPost.Room4ChildAge1 = _Room4ChildAge1;///房间4儿童岁数
    paramPost.Room4ChildAge2 = _Room4ChildAge2;///房间4儿童岁数
    paramPost.Room4Count = _Room4Count;///房间4房间数
    paramPost.ddlRoomTypeCode5 = _ddlRoomTypeCode5;///房间5类型
    paramPost.Room5ChildAge1 = _Room5ChildAge1;///房间5儿童岁数
    paramPost.Room5ChildAge2 = _Room5ChildAge2;///房间5儿童岁数
    paramPost.Room5Count = _Room5Count;///房间5房间数
    paramPost.ItemCode = hotelCode;
    paramPost.ItemNo = hotelNo;
    paramPost.RoomTypeCode = bookRoomTypeCode;
    paramPost.BreakfastTypeName = breakfastTypeName;
    if (window.confirm("请确认是否已查看取消日期及查阅提示？") == true) {
        //("确定");
        //return true;
        $.post("../RTS_B2C/RtsPort_B2C.ashx?type=CreateSystemBooking", paramPost, function (data) {
            if (data.code == "99") {
                alert(data.mes)
            }
            else { alert("预订成功:" + data.result) }
        });
    }
    else {
        //("取消");
        return false;
    }
}

//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

//外部查询
function searchOur_Click() {
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    _ddlRoomTypeCode1 = $("#ddlRoomTypeCode1 option:selected").val();///房间1类型
    _Room1ChildAge1 = $("#Room1ChildAge1 option:selected").val();///房间1儿童岁数
    _Room1ChildAge2 = $("#Room1ChildAge2 option:selected").val();///房间1儿童岁数
    _Room1Count = $("#Room1Count option:selected").val();///房间1房间数
    _ddlRoomTypeCode2 = $("#ddlRoomTypeCode2 option:selected").val();///房间2类型
    _Room2ChildAge1 = $("#Room2ChildAge1 option:selected").val();///房间2儿童岁数
    _Room2ChildAge2 = $("#Room2ChildAge2 option:selected").val();///房间2儿童岁数
    _Room2Count = $("#Room2Count option:selected").val();///房间2房间数
    _ddlRoomTypeCode3 = $("#ddlRoomTypeCode3 option:selected").val();///房间3类型
    _Room3ChildAge1 = $("#Room3ChildAge1 option:selected").val();///房间3儿童岁数
    _Room3ChildAge2 = $("#Room3ChildAge2 option:selected").val();///房间3儿童岁数
    _Room3Count = $("#Room3Count option:selected").val();///房间3房间数
    _ddlRoomTypeCode4 = $("#ddlRoomTypeCode4 option:selected").val();///房间4类型
    _Room4ChildAge1 = $("#Room4ChildAge1 option:selected").val();///房间4儿童岁数
    _Room4ChildAge2 = $("#Room4ChildAge2 option:selected").val();///房间4儿童岁数
    _Room4Count = $("#Room4Count option:selected").val();///房间4房间数
    _ddlRoomTypeCode5 = $("#ddlRoomTypeCode5 option:selected").val();///房间5类型
    _Room5ChildAge1 = $("#Room5ChildAge1 option:selected").val();///房间5儿童岁数
    _Room5ChildAge2 = $("#Room5ChildAge2 option:selected").val();///房间5儿童岁数
    _Room5Count = $("#Room5Count option:selected").val();///房间5房间数
    if (_ddlRoomTypeCode1 == "0" && _ddlRoomTypeCode2 == "0" && _ddlRoomTypeCode3 == "0" && _ddlRoomTypeCode4 == "0" && _ddlRoomTypeCode5 == "0") {
        alert("请选择房型");
        return false;
    }
    if (Number(_Room1Count) + Number(_Room2Count) + Number(_Room3Count) + Number(_Room4Count) + Number(_Room5Count) > 5) {
        alert("最多能同时搜寻5间房间，请重新选择。");
        document.getElementById('Room1Count').value = 0;
        return false;
    }
    searchOur_List();
}

//查询显示列表
function searchOur_List() {
    var paramPost = {};
    paramPost.ItemCode = _ItemCode;
    paramPost.HotelName = _HotelName;// 酒店名称
    paramPost.nationality = _nationality//国籍
    paramPost.destination = _destinationValue;//目的地
    paramPost.beginTime = _beginTime;//入住时间
    paramPost.endTime = _endTime;//退房时间
    paramPost.ddlRoomTypeCode1 = _ddlRoomTypeCode1;///房间1类型
    paramPost.Room1ChildAge1 = _Room1ChildAge1;///房间1儿童岁数
    paramPost.Room1ChildAge2 = _Room1ChildAge2;///房间1儿童岁数
    paramPost.Room1Count = _Room1Count;///房间1房间数
    paramPost.ddlRoomTypeCode2 = _ddlRoomTypeCode2;///房间2类型
    paramPost.Room2ChildAge1 = _Room2ChildAge1;///房间2儿童岁数
    paramPost.Room2ChildAge2 = _Room2ChildAge2;///房间2儿童岁数
    paramPost.Room2Count = _Room2Count;///房间2房间数
    paramPost.ddlRoomTypeCode3 = _ddlRoomTypeCode3;///房间3类型
    paramPost.Room3ChildAge1 = _Room3ChildAge1;///房间3儿童岁数
    paramPost.Room3ChildAge2 = _Room3ChildAge2;///房间3儿童岁数
    paramPost.Room3Count = _Room3Count;///房间3房间数
    paramPost.ddlRoomTypeCode4 = _ddlRoomTypeCode4;///房间4类型
    paramPost.Room4ChildAge1 = _Room4ChildAge1;///房间4儿童岁数
    paramPost.Room4ChildAge2 = _Room4ChildAge2;///房间4儿童岁数
    paramPost.Room4Count = _Room4Count;///房间4房间数
    paramPost.ddlRoomTypeCode5 = _ddlRoomTypeCode5;///房间5类型
    paramPost.Room5ChildAge1 = _Room5ChildAge1;///房间5儿童岁数
    paramPost.Room5ChildAge2 = _Room5ChildAge2;///房间5儿童岁数
    paramPost.Room5Count = _Room5Count;///房间5房间数

    $.post("../Rts_B2C/Rts_B2C.ashx?type=GetRtsRoomTypePrice", paramPost, function (data) {

    });

}