﻿/// <reference path="D:\work\XiWan\XiWan\Agoda/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\Agoda/CustomerInformation.html" />
var _hotelID;
var _countryID;
var _cityID;
var _referenceClient;
var _nationality;
var _beginTime;
var _endTime;
var Adult;
var Child;
var ChildAge1;
var ChildAge2;
var ChildAge3;
var ChildAge4;
var ChildAge5;
var ChildAge6;
var ChildAge7;
var ChildAge8;
var ChildAge9;
var ChildAge10;
var ChildAge11;
var ChildAge12;
var RoomCount;

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
$(function () {
    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });

    $("#Destination").combobox({
        valueField: 'value',
        textField: 'name',
        url: "Agoda/AgodaPort.ashx?type=Get_Destination",
        mode: "remote", //从服务器加载就设置为‘remote‘
        hasDownArrow: false,  //为true时显示下拉选项图标
        onBeforeLoad: function (parm) {  //在请求加载数据之前触发，返回 false 则取消加载动作         
            var value = $(this).combobox("getValue");
            if (value) {
                parm.paramName = value;
                return true;
            }
            return false;
        }
    })
});

//初始页面
$(document).ready(function () {
    AdultCount = 0;
    _hotelID = GetQueryString("HotelID");
    _beginTime = GetQueryString("CheckInDate");
    _endTime = GetQueryString("CheckOutDate");
    RoomCount = GetQueryString("DdlRoom");
    Adult = GetQueryString("Adult");
    Child = GetQueryString("Child");
    ChildAge1 = GetQueryString("ChildAge1");
    ChildAge2 = GetQueryString("ChildAge2");
    ChildAge3 = GetQueryString("ChildAge3");
    ChildAge4 = GetQueryString("ChildAge4");
    ChildAge5 = GetQueryString("ChildAge5");
    ChildAge6 = GetQueryString("ChildAge6");
    ChildAge7 = GetQueryString("ChildAge7");
    ChildAge8 = GetQueryString("ChildAge8");
    ChildAge9 = GetQueryString("ChildAge9");
    ChildAge10 = GetQueryString("ChildAge10");
    ChildAge11 = GetQueryString("ChildAge11");
    ChildAge12 = GetQueryString("ChildAge12");
    document.getElementById('ddlRoom').value = RoomCount;
    document.getElementById('Room1Adult').value = Adult;
    document.getElementById('Room1Child').value = Child;
    $("#txtChildAge1").val(ChildAge1);
    $("#txtChildAge2").val(ChildAge2);
    $("#txtChildAge3").val(ChildAge3);
    $("#txtChildAge4").val(ChildAge4);
    $("#txtChildAge5").val(ChildAge5);
    $("#txtChildAge6").val(ChildAge6);
    $("#txtChildAge7").val(ChildAge7);
    $("#txtChildAge8").val(ChildAge8);
    $("#txtChildAge9").val(ChildAge9);
    $("#txtChildAge10").val(ChildAge10);
    $("#txtChildAge11").val(ChildAge11);
    $("#txtChildAge12").val(ChildAge12);
    
    $('#starDate').datebox("setValue", _beginTime);
    $('#stopDate').datebox("setValue", _endTime);

    Get_AgodaHotelDetailList();

});


//查询
function search_Click() {
    RoomCount = $("#ddlRoom option:selected").val();//房间数
    
    Adult = $("#Room1Adult option:selected").val();
    Child = $("#Room1Child option:selected").val();
    ChildAge1 = $('#txtChildAge1').val();
    ChildAge2 = $('#txtChildAge2').val();
    ChildAge3 = $('#txtChildAge3').val();
    ChildAge4 = $('#txtChildAge4').val();
    ChildAge5 = $('#txtChildAge5').val();
    ChildAge6 = $('#txtChildAge6').val();
    ChildAge7 = $('#txtChildAge7').val();
    ChildAge8 = $('#txtChildAge8').val();
    ChildAge9 = $('#txtChildAge9').val();
    ChildAge10 = $('#txtChildAge10').val();
    ChildAge11 = $('#txtChildAge11').val();
    ChildAge12 = $('#txtChildAge12').val();

    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    Get_AgodaHotelDetailList();
}

//展示列表 _ddlRoom房间数，_rooms需要预订房间详情，type:0为缓存，1为实时
function Get_AgodaHotelDetailList() {

    _beginTime = $('#_easyui_textbox_input5').val();//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间

    var $width = $("#showHotelDetailDiv").width();
    var $height = $(window).height();
    var $datagrid = $("#AgodaHotelDetailList");
    $datagrid.datagrid({
        height: $height - 92,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: true,
        pagination: true,
        pageSize: 140,//每页显示的记录条数，默认为20
        pageList: [10, 15, 20, 30, 40, 60, 80, 100, 120, 140],//可以设置每页记录条数的列表
        rownumbers: true,
        url: '../Agoda/AgodaPort.ashx',
        queryParams: { type: 'GetRoomTypePrice', hotelID: _hotelID, beginTime: _beginTime, endTime: _endTime, ddlRoom: RoomCount, Adult: Adult, Child: Child, ChildAge1: ChildAge1, ChildAge2: ChildAge2, ChildAge3: ChildAge3, ChildAge4: ChildAge4, ChildAge5: ChildAge5, ChildAge6: ChildAge6, ChildAge7: ChildAge7, ChildAge8: ChildAge8, ChildAge9: ChildAge9, ChildAge10: ChildAge10, ChildAge11: ChildAge11, ChildAge12: ChildAge12 },
        idField: 'HotelID',
        columns: [[
        //{ field: 'ID', checkbox: true },
        //{ field: 'HotelID', title: 'HotelID', hidden: true },
        {
            field: 'ClassName', width: '25%', title: "房型", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'Surcharge', width: '10%', title: "额外到支付附加费", align: 'center', editor: { type: 'text', options: { required: true } }
        }
        //,
        //{
        //    field: 'Status', width: '15%', title: "状态", align: 'center', formatter: function (value, row, index) {
        //        if (row.Status == "AL") {
        //            return "可预订";
        //        }
        //        else if (row.Status == "RQ") {
        //            return "根据要求";
        //        }
        //        else {
        //            return row.RoomStatus;
        //        }
        //    }
        //}
        //,
        , {
            field: 'RoomsLeft', width: '10%', title: "可预订数量", align: 'center', formatter: function (value, row, index) {
                return row.RoomsLeft;
            }
        }
        ,
        {
            field: 'IsBreakfast', width: '10%', title: "早餐数量", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'RMBprice', width: '15%', title: "价格", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'CurrencyCode', width: '10%', title: "币种", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'HotelID', width: '20%', title: "操作", align: 'center', formatter: function (value, row, index) {
                var Action = "<a  href=\"javascript:void(0)\" onclick=\"book('" + row.HotelID + "','" + row.RateplanId + "')\">预订</a>";
                return Action;
                //return  "<a href=\"javascript:void(0)\" onclick=\"book()\"> 预订 </a>";
            }
        }
        ]],
        onLoadSuccess: function (data) {
            if (data.total == 0) {
                alert("没有数据！");
            }
            //dispalyEasyUILoad();
            //$(this).datagrid("autoMergeCells", ['ItemName', 'StarRating']);
        }
    });
    var p = $("#AgodaHotelDetailList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}

function book(HotelID, RateplanId) {
    alert("无该权限，请联系管理员");
    //var url = "../Agoda/CustomerInformation.html?HotelID=" + HotelID + "&RateplanId=" + RateplanId + "&number_of_rooms=" + RoomCount + "&Adult=" + Adult + "&Child=" + Child + "&CheckInDate=" + _beginTime + "&CheckOutDate=" + _endTime;
    //var name = "客户信息";
    //var iWidth = "950";//宽度
    //var iHeight = "700";//高度
    //openwindow(url, name, iWidth, iHeight);
}


//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}


