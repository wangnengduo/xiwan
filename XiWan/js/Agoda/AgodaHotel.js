﻿$(function () {
    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });
    //设置目的地
    $("#Destination").combobox({
        valueField: 'value',
        textField: 'name',
        url: "../Agoda/AgodaPort.ashx?type=GetAgodaDestination",
        mode: "remote", //从服务器加载就设置为‘remote‘
        hasDownArrow: false,  //为true时显示下拉选项图标
        onBeforeLoad: function (parm) {  //在请求加载数据之前触发，返回 false 则取消加载动作         
            var value = $(this).combobox("getValue");
            if (value) {
                parm.paramName = value;
                return true;
            }
            return false;
        }
    })
});

//查询
function search_Click() {
    var nationality = $("#nationality option:selected").val();//国籍
    var destination = $('#Destination').combobox('getValue');//目的地
    var beginTime = $('#_easyui_textbox_input5').val();//入住时间
    var endTime = $('#_easyui_textbox_input4').val();//退房时间
    var ddlRoom = $("#ddlRoom option:selected").val();//房间数
    var hotelName = Trim($('#txtHotelName').val());// 酒店名称
    var _ddlGreadeCode = $("#ddlGreadeCode option:selected").val();///酒店等級
    if (nationality == "0") {
        alert("请选择国籍");
        return false;
    }
    if (destination == "") {
        alert("请填写正确目的地");
        return false;
    }
    
    var Adult = $("#Room1Adult option:selected").val();
    var Child = $("#Room1Child option:selected").val();
    var ChildAge1 = $('#txtChildAge1').val();
    var ChildAge2 = $('#txtChildAge2').val();
    var ChildAge3 = $('#txtChildAge3').val();
    var ChildAge4 = $('#txtChildAge4').val();
    var ChildAge5 = $('#txtChildAge5').val();
    var ChildAge6 = $('#txtChildAge6').val();
    var ChildAge7 = $('#txtChildAge7').val();
    var ChildAge8 = $('#txtChildAge8').val();
    var ChildAge9 = $('#txtChildAge9').val();
    var ChildAge10 = $('#txtChildAge10').val();
    var ChildAge11 = $('#txtChildAge11').val();
    var ChildAge12 = $('#txtChildAge12').val();
    
    Get_AgodaHotelList(hotelName, _ddlGreadeCode, nationality, destination, beginTime, endTime, ddlRoom, Adult, Child, ChildAge1, ChildAge2, ChildAge3, ChildAge4, ChildAge5, ChildAge6, ChildAge7, ChildAge8, ChildAge9, ChildAge10, ChildAge11, ChildAge12);
    //document.getElementById("GridView1_initialDiv").style.display = "none";
}

function Get_AgodaHotelList(_hotelName, _ddlGreadeCode, _nationality, _destination, _beginTime, _endTime, _ddlRoom, Adult, Child, ChildAge1, ChildAge2, ChildAge3, ChildAge4, ChildAge5, ChildAge6, ChildAge7, ChildAge8, ChildAge9, ChildAge10, ChildAge11, ChildAge12) {
    var $width = $("#showHotelDiv").width();
    var $height = $(window).height();
    var editRow = undefined;
    var $datagrid = $("#AgodaHotelList");
    $datagrid.datagrid({
        height: $height - 20,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: true,
        pagination: true,
        pageSize: 20,//每页显示的记录条数，默认为10 
        pageList: [10, 15, 20, 30, 40, 60],//可以设置每页记录条数的列表 
        rownumbers: true,
        url: '../Agoda/AgodaPort.ashx',
        queryParams: { type: 'Get_HotelInformation', hotelName: _hotelName, ddlGreadeCode: _ddlGreadeCode, nationalityID: _nationality, destination: _destination, beginTime: _beginTime, endTime: _endTime, ddlRoom: _ddlRoom, Adult: Adult, Child: Child, ChildAge1: ChildAge1, ChildAge2: ChildAge2, ChildAge3: ChildAge3, ChildAge4: ChildAge4, ChildAge5: ChildAge5, ChildAge6: ChildAge6, ChildAge7: ChildAge7, ChildAge8: ChildAge8, ChildAge9: ChildAge9, ChildAge10: ChildAge10, ChildAge11: ChildAge11, ChildAge12: ChildAge12 },
        idField: 'Rownum',
        columns: [[
         { field: 'Rownum', title: 'HotelID', hidden: true },

          {
              //field: 'HotelName', width: '25%', title: "酒店名称", align: 'center', editor: { type: 'text', options: { required: true } }
              field: 'HotelName', title: '酒店名称', align: 'center', width: '25%',
              formatter: function (value, row) {
                  return "<a href=\"javascript:void(0)\" onclick=\"top.addTab('" + row.HotelName + "' + '酒店详情','../Agoda/AgodaHotelDetail.html?HotelID='+ '" + row.HotelId + "' + '&HotelName=' + '" + row.HotelName + "' + '&CheckInDate=' + '" + _beginTime + "' + '&CheckOutDate=' + '" + _endTime + "' + '&DdlRoom=' + '" + _ddlRoom + "' + '&Adult=' + '" + Adult + "' + '&Child=' + '" + Child + "' + '&ChildAge1=' + '" + ChildAge1 + "' + '&ChildAge2=' + '" + ChildAge2 + "' + '&ChildAge3=' + '" + ChildAge3 + "'+ '&ChildAge4=' + '" + ChildAge4 + "'+ '&ChildAge5=' + '" + ChildAge5 + "'+ '&ChildAge6=' + '" + ChildAge6 + "'+ '&ChildAge7=' + '" + ChildAge7 + "'+ '&ChildAge8=' + '" + ChildAge8 + "'+ '&ChildAge9=' + '" + ChildAge9 + "'+ '&ChildAge10=' + '" + ChildAge10 + "'+ '&ChildAge11=' + '" + ChildAge11 + "'+ '&ChildAge12=' + '" + ChildAge12 + "')\" > " + row.HotelName + " </a>";
              }
          },
          //{
          //    field: 'HotelID', title: '酒店实时详情', align: 'center', width: '10%',
          //    formatter: function (value, row) {
          //        return "<a href=\"javascript:void(0)\" onclick=\"top.addTab('" + row.HotelName + "' + '酒店实时详情','AgodaHotelDetail.aspx?HotelID='+ '" + row.HotelID + "' + '&HotelName=' + '" + row.HotelName + "' + '&CheckInDate=' + '" + _beginTime + "' + '&CheckOutDate=' + '" + _endTime + "' + '&CountryID=' + '" + row.CountryID + "' + '&CityID=' + '" + row.CityID + "' + '&NationalityID=' + '" + _nationality + "' + '&ReferenceClient=' + '" + row.ReferenceClient + "' + '&Rooms=' + '" + _rooms + "' + '&DdlRoom=' + '" + _ddlRoom + "' +  '&Address=' + '" + row.Address + "' + '&CountryName=' + '" + row.CountryName + "' + '&StarRating=' + '" + row.StarRatingCount + "' + '&type=' + '" + 1 + "')\" >" + "查看" + "</a>";
          //    }
          //},
           {
               field: 'TranslatedName', width: '25%', title: "酒店名称(中文)", align: 'center', editor: { type: 'text', options: { required: true } }
           },
          {
              field: 'StarRating', width: '15%', title: "星级", align: 'center', editor: { type: 'text', options: { required: true } }
          },
          {
              field: 'AccommodationType', width: '15%', title: "酒店类型", align: 'center', editor: { type: 'text', options: { required: true } }
          }
        ]]
    });
    var p = $("#AgodaHotelList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字 
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}


//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}