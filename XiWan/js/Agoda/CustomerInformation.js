﻿var _hotelID;
var _beginTime;
var _endTime;
var RoomCount;
var roomDetails;
var RateplanId;
var Adult;
var Child;

//初始页面
$(document).ready(function () {
    _hotelID = GetQueryString("HotelID");
    _beginTime = GetQueryString("CheckInDate");
    _endTime = GetQueryString("CheckOutDate");
    RoomCount = GetQueryString("number_of_rooms");
    RateplanId = GetQueryString("RateplanId");
    Adult = GetQueryString("Adult");
    Child = GetQueryString("Child");
    var people = Number(Adult) + Number(Child);
    displayRooms(people)
});

function displayRooms(people)
{
    for (var m = 1; m <= people; m++) {
        var peopleNone = "People" + m;
        document.getElementById(peopleNone).style.display = "block";
    }
}

function submit_Click()
{
    var people = Number(Adult) + Number(Child);
    var paramPost = {};
    paramPost.beginTime = _beginTime;
    paramPost.endTime = _endTime;
    paramPost.HotelID = _hotelID;
    paramPost.RateplanId = RateplanId;
    paramPost.RoomCount = RoomCount;
    paramPost.Adult = Adult;
    paramPost.Child = Child;
    var roomArr = ""; 
    if (window.confirm("确认预订吗？(Confirm booking?)") == true) {
        for (var j = 0; j < people; j++) {
            var ddlSalutation = "#ddl" + "Salutation" + (j + 1);
            var RoomSalutation = $(ddlSalutation + " option:selected").val();
            var varRoomFirstName = "#" + "FirstName" + (j + 1);
            var RoomFirstName = $(varRoomFirstName).val();//名
            var varRoomLastName = "#" + "LastName" + (j + 1);
            var RoomLastName = $(varRoomLastName).val();//性
            var varRoomAge = "#" + "Age" + (j + 1);
            var RoomAge = $(varRoomAge).val(); //年龄
            roomArr +=  RoomSalutation + "," + RoomFirstName + ',' + RoomLastName + ',' + RoomAge + ';';
            /*myarr[j] = new Array(); //再声明二维 
            for (var k = 0; k < 3; k++) { //二维长度为3
                if (k == 0) {
                        
                        
                    myarr[j][k] = RoomSalutation;//document.getElementById(ddlRoomSalutation).value; // 赋值，每个数组元素的值
                }
                else if (k == 1) {
                        
                    myarr[j][k] = $(RoomFirstName).val();//名
                }
                else if (k == 2) {
                    var RoomLastName = "#Room" + i + "LastName" + (j + 1);
                    myarr[j][k] = $(RoomLastName).val();//性
                }
            }*/
        }
        roomArr = roomArr.substring(0, roomArr.lastIndexOf(';'))
        paramPost.roomArr = roomArr;
        alert("无该权限，请联系管理员");
        return false;
        //("确定");
        //return true;
        //关闭预订权限
        /*$.post("../Agoda/AgodaPort.ashx?type=book", paramPost, function (data) {
            if (data.code == "99") {
                alert(data.mes)
            }
            else {
                alert(data.mes + "(booking succeed)");
                window.close();
            }
        });*/
    }
    else {
        //("取消");
        return false;
    }
}
