﻿var _hotelID;
var _beginTime;
var _endTime;
var RoomCount;
var roomDetails;
var RateplanId;

//初始页面
$(document).ready(function () {
    _hotelID = GetQueryString("HotelID");
    _beginTime = GetQueryString("CheckInDate");
    _endTime = GetQueryString("CheckOutDate");
    RoomCount = GetQueryString("number_of_rooms");
    roomDetails = GetQueryString("roomDetails");
    RateplanId = GetQueryString("RateplanId");
    displayRooms(RoomCount)
});

function displayRooms(RoomCount)
{
    var ArrayRooms = roomDetails.split(";");
    for (var m = 1; m <= 9; m++) {
        var roomNone = "Room" + m;
        document.getElementById(roomNone).style.display = "none";
    }
    for (var i = 1; i <= RoomCount; i++) {
        var ArrayRoom = ArrayRooms[i - 1].split(",");
        var totalPeople = Number(ArrayRoom[0]);
        if (totalPeople == "99")
        {
            totalPeople = 2;
        }
        var roomBlock = "Room" + i;
        document.getElementById(roomBlock).style.display = "block";
        for (var j = 1; j <= 4; j++) {
            var RoomCustomerNone = "Room" + i + "Customer" + j;
            document.getElementById(RoomCustomerNone).style.display = "none";
        }
        for (var k = 1; k <= totalPeople; k++) {
            var RoomCustomerBlock = "Room" + i + "Customer" + k;
            document.getElementById(RoomCustomerBlock).style.display = "block";
        }
    }
}

function submit_Click()
{
    var ArrayRooms = roomDetails.split(";");
    var paramPost = {};
    paramPost.beginTime = _beginTime;
    paramPost.endTime = _endTime;
    paramPost.HotelID = _hotelID;
    paramPost.RateplanId = RateplanId;
    paramPost.RoomCount = RoomCount;
    var roomArr = ""; 
    if (window.confirm("确认预订吗？(Confirm booking?)") == true) {
        var ArrayRooms = roomDetails.split(";");
        for (var i = 1; i <= RoomCount; i++) { 
            var totalPeople = ArrayRooms[i - 1];
            if (totalPeople == "99") {
                totalPeople = 2;
            }
            for (var j = 0; j < totalPeople; j++) {
                var ddlRoomSalutation = "#ddlRoom" + i + "Salutation" + (j + 1);
                var RoomSalutation = $(ddlRoomSalutation + " option:selected").val();
                var varRoomFirstName = "#Room" + i + "FirstName" + (j + 1);
                var RoomFirstName = $(varRoomFirstName).val();//名
                var varRoomLastName = "#Room" + i + "LastName" + (j + 1);
                var RoomLastName = $(varRoomLastName).val();//性
                var varRoomAge = "#Room" + i + "Age" + (j + 1);
                var RoomAge = $(varRoomAge).val(); //年龄
                roomArr += ArrayRooms[i - 1] + ',' + RoomSalutation + "," + RoomFirstName + ',' + RoomLastName + ',' + RoomAge + ';';
                /*myarr[j] = new Array(); //再声明二维 
                for (var k = 0; k < 3; k++) { //二维长度为3
                    if (k == 0) {
                        
                        
                        myarr[j][k] = RoomSalutation;//document.getElementById(ddlRoomSalutation).value; // 赋值，每个数组元素的值
                    }
                    else if (k == 1) {
                        
                        myarr[j][k] = $(RoomFirstName).val();//名
                    }
                    else if (k == 2) {
                        var RoomLastName = "#Room" + i + "LastName" + (j + 1);
                        myarr[j][k] = $(RoomLastName).val();//性
                    }
                }*/
            }
            roomArr = roomArr.substring(0, roomArr.lastIndexOf(';')) + '*';
        }
        paramPost.roomArr = roomArr;
        //("确定");
        //return true;
        $.post("../MgGroup/MgGroupPort.ashx?type=book", paramPost, function (data) {
            if (data.code == "99") {
                alert(data.mes)
            }
            else {
                alert(data.mes + "(booking succeed)");
                window.close();
            }
        });
    }
    else {
        //("取消");
        return false;
    }
}
