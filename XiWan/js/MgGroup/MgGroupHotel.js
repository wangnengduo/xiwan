﻿$(function () {
    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });
    //设置目的地
    $("#Destination").combobox({
        valueField: 'value',
        textField: 'name',
        url: "../MgGroup/MgGroupPort.ashx?type=GetDestinationToSql",
        mode: "remote", //从服务器加载就设置为‘remote‘
        hasDownArrow: false,  //为true时显示下拉选项图标
        onBeforeLoad: function (parm) {  //在请求加载数据之前触发，返回 false 则取消加载动作         
            var value = $(this).combobox("getValue");
            if (value) {
                parm.paramName = value;
                return true;
            }
            return false;
        }
    })
});

//查询
function search_Click() {
    var nationality = $("#nationality option:selected").val();//国籍
    var destination = $('#Destination').combobox('getValue');//目的地
    var beginTime = $('#_easyui_textbox_input5').val();//入住时间
    var endTime = $('#_easyui_textbox_input4').val();//退房时间
    var ddlRoom = $("#ddlRoom option:selected").val();//房间数
    var hotelName = Trim($('#txtHotelName').val());// 酒店名称
    var _ddlGreadeCode = $("#ddlGreadeCode option:selected").val();///酒店等級
    if (nationality == "0") {
        alert("请选择国籍");
        return false;
    }
    if (destination == "") {
        alert("请填写正确目的地");
        return false;
    }

    if ($("#Room1Child option:selected").val() == 2) {
        if (Trim($("#txtRoom1Child1").val()) == "" || Trim($("#txtRoom1Child2").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0 || Trim($("#txtRoom1Child2").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    if ($("#Room1Child option:selected").val() == 1) {
        if (Trim($("#txtRoom1Child1").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    var rooms = $("#Room1Adult option:selected").val() + ";";
    for (var i = 2; i <= ddlRoom; i++) {
        var Adult = "#Room" + i + "Adult option:selected";
        
        rooms += $(Adult).val() + ";";
    }
    Get_MgGroupHotelList(hotelName, _ddlGreadeCode, nationality, destination, beginTime, endTime, ddlRoom, rooms);
    //document.getElementById("GridView1_initialDiv").style.display = "none";
}

function Get_MgGroupHotelList(_hotelName, _ddlGreadeCode, _nationality, _destination, _beginTime, _endTime, _ddlRoom, _rooms) {
    var $width = $("#showHotelDiv").width();
    var $height = $(window).height();
    var editRow = undefined;
    var $datagrid = $("#MgGroupHotelList");
    $datagrid.datagrid({
        height: $height - 20,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: true,
        pagination: true,
        pageSize: 20,//每页显示的记录条数，默认为10 
        pageList: [10, 15, 20, 30, 40, 60],//可以设置每页记录条数的列表 
        rownumbers: true,
        url: '../MgGroup/MgGroupPort.ashx',
        queryParams: { type: 'GetHotelToSql', hotelName: _hotelName, ddlGreadeCode: _ddlGreadeCode, nationalityID: _nationality, destination: _destination, beginTime: _beginTime, endTime: _endTime, ddlRoom: _ddlRoom, rooms: _rooms },
        idField: 'Rownum',
        columns: [[
         { field: 'Rownum', title: 'SERVICE_ID', hidden: true },

          {
              //field: 'HOTEL_NAME', width: '25%', title: "酒店名称", align: 'center', editor: { type: 'text', options: { required: true } }
              field: 'HotelName', title: '酒店名称', align: 'center', width: '25%',
              formatter: function (value, row) {
                  return "<a href=\"javascript:void(0)\" onclick=\"top.addTab('" + row.HotelName + "' + '酒店详情','../MgGroup/MgGroupHotelDetail.html?HotelID='+ '" + row.HotelID + "' + '&HotelName=' + '" + row.HotelName + "' + '&CheckInDate=' + '" + _beginTime + "' + '&CheckOutDate=' + '" + _endTime + "'  + '&NationalityID=' + '" + _nationality + "'  + '&Rooms=' + '" + _rooms + "' + '&DdlRoom=' + '" + _ddlRoom + "' )\" > " + row.HotelName + " </a>";
              }
          },
          //{
          //    field: 'HotelID', title: '酒店实时详情', align: 'center', width: '10%',
          //    formatter: function (value, row) {
          //        return "<a href=\"javascript:void(0)\" onclick=\"top.addTab('" + row.HotelName + "' + '酒店实时详情','MgGroupHotelDetail.aspx?HotelID='+ '" + row.HotelID + "' + '&HotelName=' + '" + row.HotelName + "' + '&CheckInDate=' + '" + _beginTime + "' + '&CheckOutDate=' + '" + _endTime + "' + '&CountryID=' + '" + row.CountryID + "' + '&CityID=' + '" + row.CityID + "' + '&NationalityID=' + '" + _nationality + "' + '&ReferenceClient=' + '" + row.ReferenceClient + "' + '&Rooms=' + '" + _rooms + "' + '&DdlRoom=' + '" + _ddlRoom + "' +  '&Address=' + '" + row.Address + "' + '&CountryName=' + '" + row.CountryName + "' + '&StarRating=' + '" + row.StarRatingCount + "' + '&type=' + '" + 1 + "')\" >" + "查看" + "</a>";
          //    }
          //},
           {
               field: 'Address1', width: '25%', title: "地址", align: 'center', editor: { type: 'text', options: { required: true } }
           },
          {
              field: 'Rating', width: '15%', title: "星级", align: 'center', editor: { type: 'text', options: { required: true } }
          },
          {
              field: 'Telephone', width: '15%', title: "电话", align: 'center', editor: { type: 'text', options: { required: true } }
          },
          {
              field: 'Email', width: '20%', title: "邮箱", align: 'center', editor: { type: 'text', options: { required: true } }
          }
        ]]
    });
    var p = $("#MgGroupHotelList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字 
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}

//选择房间数时控制房间显示隐藏
function ChangeRoom(Roomsm) {
    for (var j = 1; j <= 9; j++) {
        var room = "room" + j;
        document.getElementById(room).style.display = "none";
    }
    for (var j = 1; j <= Roomsm; j++) {
        var room = "room" + j;
        document.getElementById(room).style.display = "block";
    }
}

//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}