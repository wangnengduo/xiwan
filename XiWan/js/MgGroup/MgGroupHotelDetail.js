﻿/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
/// <reference path="D:\work\XiWan\XiWan\MgGroupBLL/CustomerInformation.html" />
var _hotelID;
var _countryID;
var _cityID;
var _referenceClient;
var _nationality;
var _beginTime;
var _endTime;
var AdultCount = 0;
var ChildsCount = 0;
var rooms;
var ddlRoom;

function GetQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);
    if (r != null) return unescape(r[2]); return null;
}
$(function () {
    //入住时间大于当前时间
    $('#starDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
            return d1 <= date
        }
    });
    //退房时间大于入住时间
    $('#stopDate').datebox().datebox('calendar').calendar({
        validator: function (date) {
            var now = new Date();
            var d1 = new Date($('#_easyui_textbox_input5').val());
            return d1 < date
        }
    });
    //入住时间改变事件
    $('#starDate').datebox({
        onSelect: function (date) {
            $('#stopDate').datebox('setValue', date.getFullYear() + "-" + (date.getMonth() + 1) + "-" + (date.getDate() + 1));
        }
    });

    $("#Destination").combobox({
        valueField: 'value',
        textField: 'name',
        url: "MgGroup/MgGroupPort.ashx?type=Get_Destination",
        mode: "remote", //从服务器加载就设置为‘remote‘
        hasDownArrow: false,  //为true时显示下拉选项图标
        onBeforeLoad: function (parm) {  //在请求加载数据之前触发，返回 false 则取消加载动作         
            var value = $(this).combobox("getValue");
            if (value) {
                parm.paramName = value;
                return true;
            }
            return false;
        }
    })
});

//初始页面
$(document).ready(function () {
    AdultCount = 0;
    _hotelID = GetQueryString("HotelID");
    _nationality = GetQueryString("NationalityID");
    _beginTime = GetQueryString("CheckInDate");
    _endTime = GetQueryString("CheckOutDate");
    ddlRoom = RoomCount = GetQueryString("DdlRoom");
    rooms = Rooms = GetQueryString("Rooms");
    document.getElementById('ddlRoom').value = RoomCount;
    ChangeRoom(RoomCount);
    var ArrayRooms = Rooms.split(";");
    for (var i = 1; i <= RoomCount; i++) {
        refreshText(i, ArrayRooms[i - 1])

    }
    $('#starDate').datebox("setValue", _beginTime);
    $('#stopDate').datebox("setValue", _endTime);
    //设置目的地
    document.getElementById("room1").style.display = "block";
    Get_MgGroupHotelDetailList(RoomCount, Rooms);
});


//查询
function search_Click() {
    ddlRoom = $("#ddlRoom option:selected").val();//房间数
    var showType = ' <%= showType %>';
    if ($("#Room1Child option:selected").val() == 2) {
        if (Trim($("#txtRoom1Child1").val()) == "" || Trim($("#txtRoom1Child2").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0 || Trim($("#txtRoom1Child2").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    if ($("#Room1Child option:selected").val() == 1) {
        if (Trim($("#txtRoom1Child1").val()) == "") {
            alert("请输入儿童岁数");
            return false;
        }
        else if (Trim($("#txtRoom1Child1").val()) < 0) {
            alert("儿童岁数不能小于0");
            return false;
        }
    }
    rooms = $("#Room1Adult option:selected").val() + ";";
    for (var i = 2; i <= ddlRoom; i++) {
        var Adult = "#Room" + i + "Adult option:selected";
        rooms += $(Adult).val() + ";";
    }
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    Get_MgGroupHotelDetailList(ddlRoom, rooms);
}

//展示列表 _ddlRoom房间数，_rooms需要预订房间详情，type:0为缓存，1为实时
function Get_MgGroupHotelDetailList(_ddlRoom, _rooms) {

    _beginTime = $('#_easyui_textbox_input5').val();//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间

    var $width = $("#showHotelDetailDiv").width();
    var $height = $(window).height();
    var $datagrid = $("#MgGroupHotelDetailList");
    $datagrid.datagrid({
        height: $height - 92,
        width: $width,
        fitColumns: true,
        collapsible: true,
        singleSelect: true,
        pagination: true,
        pageSize: 20,//每页显示的记录条数，默认为20
        pageList: [10, 15, 20, 30, 40, 60],//可以设置每页记录条数的列表
        rownumbers: true,
        url: '../MgGroup/MgGroupPort.ashx',
        queryParams: { type: 'GetRoomTypePrice', hotelID: _hotelID,  nationality: _nationality, beginTime: _beginTime, endTime: _endTime, ddlRoom: _ddlRoom, rooms: _rooms },
        idField: 'HotelID',
        columns: [[
        //{ field: 'HotelID', title: 'HotelID', hidden: true },
        {
            field: 'ClassName', width: '25%', title: "房型", align: 'center', editor: { type: 'text', options: { required: true } }
        }
        //,
        //{
        //    field: 'Status', width: '15%', title: "状态", align: 'center', formatter: function (value, row, index) {
        //        if (row.Status == "AL") {
        //            return "可预订";
        //        }
        //        else if (row.Status == "RQ") {
        //            return "根据要求";
        //        }
        //        else {
        //            return row.RoomStatus;
        //        }
        //    }
        //}
        //,
        //{
        //    field: 'TotalRoom', width: '7%', title: "可预订数量", align: 'center', formatter: function (value, row, index) {
        //        return row.TotalRoom;
        //    }
        //}
        ,{
            field: 'CurrencyCode', width: '10%', title: "币种", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'Price', width: '15%', title: "价格(原始价)", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'RMBprice', width: '15%', title: "价格(人民币)", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'BreakfastName', width: '15%', title: "BFType", align: 'center', editor: { type: 'text', options: { required: true } }
        },
        {
            field: 'HotelID', width: '20%', title: "操作", align: 'center', formatter: function (value, row, index) {
                var Action = "<a  href=\"javascript:void(0)\" onclick=\"CancelDeadline('" + row.HotelID + "','" + row.RateplanId + "')\">查看取消日期</a>\ |<a  href=\"javascript:void(0)\" onclick=\"recheckPrice('" + row.HotelID + "','" + row.RateplanId + "')\">复查价格</a>\ | <a  href=\"javascript:void(0)\" onclick=\"book('" + row.HotelID + "','" + row.RateplanId + "')\">预订</a>";
                return Action;
                //return  "<a href=\"javascript:void(0)\" onclick=\"book()\"> 预订 </a>";
            }
        }
        //,
        //{
        //    field: 'Adults', width: '5%', title: "大人数", align: 'center', formatter: function (value, row, index) {
        //        return row.Adults;
        //    }
        //}
        //,
        //{
        //    field: 'Childs', width: '5%', title: "儿童数", align: 'center', formatter: function (value, row, index) {
        //        return row.Childs;
        //    }
        //}

        ]],
        onLoadSuccess: function (data) {
            if (data.total == 0) {
                alert("没有数据！");
            }
            //dispalyEasyUILoad();
            //$(this).datagrid("autoMergeCells", ['ItemName', 'StarRating']);
        }
    });
    var p = $("#MgGroupHotelDetailList").datagrid('getPager');
    $(p).pagination({
        beforePageText: '第',//页数文本框前显示的汉字
        afterPageText: '页    共 {pages} 页',
        displayMsg: '当前显示 {from} - {to} 条记录   共 {total} 条记录'

    });
}

function CancelDeadline(HotelID, RateplanId) {
    var paramPost = {};
    paramPost.HotelID = HotelID;
    paramPost.RateplanId = RateplanId
    $.post("../MgGroup/MgGroupPort.ashx?type=GetCancellationPolicy", paramPost, function (data) {
        if (data.code == "99") {
            alert(data.mes)
        }
        else { alert("获取成功:" + data.mes); }
    });
}

function recheckPrice(HotelID, RateplanId) {
    var paramPost = {};
    paramPost.beginTime = _beginTime;
    paramPost.endTime = _endTime;
    paramPost.HotelID = HotelID;
    paramPost.ddlRoom = ddlRoom;
    paramPost.RateplanId = RateplanId;
    paramPost.rooms = rooms;
    $.post("../MgGroup/MgGroupPort.ashx?type=recheckPrice", paramPost, function (data) {
        if (data == "1") {
            alert("已复查成功");
        }
        else {
            alert("复查失败");
        }
    });
}
function book(HotelID, RateplanId) {
    //预订前先检查价格
    var paramPost = {};
    paramPost.HotelID = HotelID;
    paramPost.RateplanId = RateplanId;
    $.get("../MgGroup/MgGroupPort.ashx?type=GetIsRecheckPrice", paramPost, function (data) {
        if (data == "99") {
            alert("请先复查价格,再预订(Please click the button first复查价格)");
            return false;
        }
        var url = "../MgGroup/CustomerInformation.html?HotelID=" + HotelID + "&RateplanId=" + RateplanId + "&number_of_rooms=" + ddlRoom + "&roomDetails=" + rooms + "&CheckInDate=" + _beginTime + "&CheckOutDate=" + _endTime;
        var name = "客户信息";
        var iWidth = "950";//宽度
        var iHeight = "700";//高度
        openwindow(url, name, iWidth, iHeight);
    });
}

//选择房间数时控制房间显示隐藏
function ChangeRoom(Roomsm) {
    for (var j = 1; j <= 9; j++) {
        var room = "room" + j;
        document.getElementById(room).style.display = "none";
    }
    for (var j = 1; j <= Roomsm; j++) {
        var room = "room" + j;
        document.getElementById(room).style.display = "block";
    }
}

$.extend($.fn.datagrid.methods, {
    autoMergeCells: function (jq, fields) {
        return jq.each(function () {
            var target = $(this);
            if (!fields) {
                fields = target.datagrid("getColumnFields");
            }
            var rows = target.datagrid("getRows");
            var i = 0,
            j = 0,
            temp = {};
            for (i; i < rows.length; i++) {
                var row = rows[i];
                j = 0;
                for (j; j < fields.length; j++) {
                    var field = fields[j];
                    var tf = temp[field];
                    if (!tf) {
                        tf = temp[field] = {};
                        tf[row[field]] = [i];
                    } else {
                        var tfv = tf[row[field]];
                        if (tfv) {
                            tfv.push(i);
                        } else {
                            tfv = tf[row[field]] = [i];
                        }
                    }
                }
            }
            $.each(temp, function (field, colunm) {
                $.each(colunm, function () {
                    var group = this;

                    if (group.length > 1) {
                        var before,
                        after,
                        megerIndex = group[0];
                        for (var i = 0; i < group.length; i++) {
                            before = group[i];
                            after = group[i + 1];
                            if (after && (after - before) == 1) {
                                continue;
                            }
                            var rowspan = before - megerIndex + 1;
                            if (rowspan > 1) {
                                target.datagrid('mergeCells', {
                                    index: megerIndex,
                                    field: field,
                                    rowspan: rowspan
                                });
                            }
                            if (after && (after - before) != 1) {
                                megerIndex = after;
                            }
                        }
                    }
                });
            });
        });
    }
});


//去掉空格
function Trim(str) {
    return str.replace(/(^\s*)|(\s*$)/g, "");
}

//查询
function searchOur_Click() {
    var ddlRoom = $("#ddlRoom option:selected").val();//房间数
    
    AdultCount = $("#Room1Adult option:selected").val();
    var rooms = $("#Room1Adult option:selected").val() + ";";
    for (var i = 2; i <= ddlRoom; i++) {
        var Adult = "#Room" + i + "Adult option:selected";
        
        rooms += $(Adult).val()  + ";";
        AdultCount = Number(AdultCount) + Number($(Adult).val());
    }
    _beginTime = Trim($('#_easyui_textbox_input5').val());//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    GetMgGroupRoomTypeOutPrice(ddlRoom, rooms);
}

//展示列表 _ddlRoom房间数，_rooms需要预订房间详情
function GetMgGroupRoomTypeOutPrice(_ddlRoom, _rooms) {

    _beginTime = $('#_easyui_textbox_input5').val();//入住时间
    _endTime = $('#_easyui_textbox_input4').val();//退房时间
    var paramPost = {};
    paramPost.hotelID = _hotelID;
    paramPost.countryID = _countryID;
    paramPost.referenceClient = _referenceClient;
    paramPost.nationalityID = _nationality;
    paramPost.cityID = _cityID;
    paramPost.beginTime = _beginTime;
    paramPost.endTime = _endTime;
    paramPost.ddlRoom = _ddlRoom;
    paramPost.rooms = _rooms
    $.post("../MgGroup/MgGroupPort.ashx?type=GetMgGroupRoomTypePrice", paramPost, function (data) {

    });
}

function refreshText(roomCount, Adult) {
    if (roomCount == 1) {
        document.getElementById('Room1Adult').value = Adult;
    }
    else if (roomCount == 2) {
        document.getElementById('Room2Adult').value = Adult;
    }
    else if (roomCount == 3) {
        document.getElementById('Room3Adult').value = Adult;
    }
    else if (roomCount == 4) {
        document.getElementById('Room4Adult').value = Adult;
    }
    else if (roomCount == 5) {
        document.getElementById('Room5Adult').value = Adult;
    }
    else if (roomCount == 6) {
        document.getElementById('Room6Adult').value = Adult;
    }
    else if (roomCount == 7) {
        document.getElementById('Room7Adult').value = Adult;
    }
    else if (roomCount == 8) {
        document.getElementById('Room8Adult').value = Adult;
    }
    else if (roomCount == 9) {
        document.getElementById('Room9Adult').value = Adult;
    }
}



function openwindow(url, name, iWidth, iHeight) {
    var url;                            //转向网页的地址;
    var name;                           //网页名称，可为空;
    var iWidth;                         //弹出窗口的宽度;
    var iHeight;                        //弹出窗口的高度;
    //window.screen.height获得屏幕的高，window.screen.width获得屏幕的宽
    var iTop = (window.screen.height - 30 - iHeight) / 2;       //获得窗口的垂直位置;
    var iLeft = (window.screen.width - 10 - iWidth) / 2;        //获得窗口的水平位置;
    window.open(url, name, 'height=' + iHeight + ',,innerHeight=' + iHeight + ',width=' + iWidth + ',innerWidth=' + iWidth + ',top=' + iTop + ',left=' + iLeft + ',toolbar=no,menubar=no,scrollbars=auto,resizeable=no,location=no,status=no');
}