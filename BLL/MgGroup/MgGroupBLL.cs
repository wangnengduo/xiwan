﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.Data.Common;
using XiWan.Data.MgGroup;
using XiWan.Model.Entities;

namespace XiWan.BLL.MgGroup
{
    public class MgGroupBLL
    {

        #region 初始化本类
        private static MgGroupBLL _Instance;

        private MgGroupDAL _MgGroupData;
        
        int _totalCount;

        public MgGroupBLL()
        {
            _MgGroupData = MgGroupData;
        }

        //对外公开的单实例静态对象
        public static MgGroupBLL Instance
        {
            get
            {
                return new MgGroupBLL(); 
            }
        }

        //数据访问对象定义
        private MgGroupDAL MgGroupData
        {
            get
            {
                if (_MgGroupData == null)
                    _MgGroupData = new MgGroupDAL();
                return _MgGroupData;
            }
        }    
        #endregion

        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetHotels()
        {
            return MgGroupData.GetHotels();
        }

        //国籍
        public string GetNationalityToSql()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(MgGroupData.GetNationalityToSql());
        }
        //目的地
        public string GetDestinationToSql(string strName)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(MgGroupData.GetDestinationToSql(strName));
        }
        /// <summary>
        /// 获取酒店信息
        /// </summary>
        /// <returns></returns>
        public string GetHotelToSql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }

        /// <summary>
        /// 查询报价
        /// </summary>
        /// <returns></returns>
        public DataTable GetRoomTypePrice(string hotelID, string CheckInDate, string CheckoutDate, List<IntOccupancyInfo> occupancys, int RoomCount, string rateplanId, string nationality)
        {
            return MgGroupData.GetRoomTypePrice(hotelID, CheckInDate, CheckoutDate, occupancys, RoomCount, rateplanId, nationality);
        }
        /// <summary>
        /// 复查价格
        /// </summary>
        /// <returns></returns>
        public bool RecheckPrice(string hotelID, string CheckInDate, string CheckoutDate, List<IntOccupancyInfo> occupancys, int RoomCount, string rateplanId)
        {
            return MgGroupData.RecheckPrice(hotelID, CheckInDate, CheckoutDate, occupancys, RoomCount, rateplanId);
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public string GetBookingBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        //获取取消政策
        public string GetHotelCancellationPolicy(string HotelID, string RateplanId)
        {
            return MgGroupData.GetHotelCancellationPolicy(HotelID, RateplanId);
        }
        //创建订单
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            return MgGroupData.book(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum);
        }
        //取消订单
        public string cancel(string hotelID, string bookingCode)
        {
            return MgGroupData.cancel(hotelID, bookingCode);
        }
        //查询订单
        public string AsianoverlandCheckingOrder(string hotelID, string bookingCode)
        {
            return MgGroupData.CheckingOrder(hotelID, bookingCode);
        }
        //查询订单
        public string AsianoverlandBookingDetail(string hotelID, string AgentBookingReference)
        {
            return MgGroupData.BookingDetail(hotelID, AgentBookingReference);
        }
        //获取取消费用
        public string GetCancellationFee(string HotelID, string bookingCode)
        {
            return MgGroupData.GetCancellationFee(HotelID, bookingCode);
        }
    }
}