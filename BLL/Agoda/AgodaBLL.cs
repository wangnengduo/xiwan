﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.DALFactory;
using XiWan.Data.Agoda;
using XiWan.Data.Common;
using XiWan.Model.Entities;

namespace XiWan.BLL.Agoda
{
    public class AgodaBLL
    {
        #region 初始化本类
        private static AgodaBLL _Instance;

        private AgodaDAL _AgodaData;
        
        int _totalCount;

        public AgodaBLL()
        {
            _AgodaData = AgodaData;
        }

        //对外公开的单实例静态对象
        public static AgodaBLL Instance
        {
            get
            {
                return new AgodaBLL(); 
            }
        }

        //数据访问对象定义
        private AgodaDAL AgodaData
        {
            get
            {
                if (_AgodaData == null)
                    _AgodaData = new AgodaDAL();
                return _AgodaData;
            }
        }    
        #endregion

        /// <summary>
        /// 从接口获取洲信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetContinent()
        {
            return AgodaData.GetContinent();
        }

        /// <summary>
        /// 从接口获取国家信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetCountry()
        {
            return AgodaData.GetCountry();
        }

        /// <summary>
        /// 从接口获取市信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetCity()
        {
            return AgodaData.GetCity();
        }

        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetHotels()
        {
            return AgodaData.GetHotels();
        }

        /// <summary>
        /// 从接口获取房型信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public string GetRoomTypeSave()
        {
            return AgodaData.GetRoomTypeSave();
        }

        //国籍
        public string GetAgodaNationality()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(AgodaData.GetNationality());
        }
        //目的地
        public string GetAgodaDestination(string strName)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(AgodaData.GetDestination(strName));
        }
        /// <summary>
        /// 获取酒店信息
        /// </summary>
        /// <returns></returns>
        public string GetHotel(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        /// <summary>
        /// 获取房型报价
        /// </summary>
        public DataTable GetRoomTypePrice(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancys, int RoomCount, string RateplanId)
        {
            return AgodaData.GetRtsRoomTypePrice(hotelID, beginTime, endTime, occupancys, RoomCount, RateplanId);
        }
        //预订
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            return AgodaData.book( HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum);
        }
        //取消
        public string cancel(string HotelId,string bookingCode)
        {
            return AgodaData.Cancel_Order(HotelId, bookingCode);
        }
        //获取订单明细
        public string Get_OrderDetail(string HotelId, string BookingNumber)
        {
            return AgodaData.Get_OrderDetail(HotelId, BookingNumber);
        }
        //查询账单
        public string GetBookingList(string hotelID, string AgentBookingReference)
        {
            return AgodaData.GetBookingList(hotelID, AgentBookingReference);
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public string GetBookingBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }

        //报价
        public string GetRoomTypeOutPrice(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            return AgodaData.GetRoomTypeOutPrice(hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId);
        }
        //创建订单
        public string Create_Order(string HotelID, string RateplanId, BookingInfo customers, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney)
        {
            return AgodaData.Create_Order(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum,TotalMoney);
        }
    //    public string WebPost(string url, string body)
    //    {
    //        //return AgodaData.WebPost(url, body);
    //    }
    }
}