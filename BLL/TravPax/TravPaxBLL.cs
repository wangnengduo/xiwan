﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Data;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using XiWan.Data.Common;
using System.Collections;
using System.Text.RegularExpressions;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;
namespace XiWan.BLL
{
    public class TravPaxBLL
    {
        #region 初始化本类
        private static TravPaxBLL _Instance;

        private TravPaxDAL _TravPaxData;
        
        int _totalCount;

        public TravPaxBLL()
        {
            _TravPaxData = TravPaxData;
        }

        //对外公开的单实例静态对象
        public static TravPaxBLL Instance
        {
            get
            {
                return new TravPaxBLL(); 
            }
        }

        //数据访问对象定义
        private TravPaxDAL TravPaxData
        {
            get
            {
                if (_TravPaxData == null)
                    _TravPaxData = new TravPaxDAL();
                return _TravPaxData;
            }
        }    
        #endregion
        //国籍
        public string GetTravPaxnationality()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(TravPaxData.GetNationality());
        }
        //目的地
        public string GetTravPaxdestination(string strName)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(TravPaxData.GetDestination(strName));
        }
        //通过城市查询酒店
        public string SearchCity(string url, string travPaxUsername, string travPaxPassword, string CountryCode, string CityID,string NationalityID, string beginTime, string endTime, int ddlRoom, string rooms, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            return TravPaxData.SearchCity(url, travPaxUsername, travPaxPassword, CountryCode, CityID,NationalityID, beginTime, endTime, ddlRoom,  rooms, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
        }
        /// <summary>
        /// 获取酒店信息
        /// </summary>
        /// <returns></returns>
        public string GetHotel(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        /// <summary>
        /// 获取报价
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="hotelID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="ddlRoom"></param>
        /// <param name="rooms"></param>
        /// <param name="input_page"></param>
        /// <param name="input_row"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string GetRealHotelQuote(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, string hotelID,string NationalityID, string beginTime, string endTime, int ddlRoom, string rooms, int input_page, int input_row, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            string[] ArrayRoom = rooms.TrimEnd(';').Split(';');
            string[] ArrayChilds;
            int Adults = 0;
            int Childs = 0;
            for (int i = 0; i < ddlRoom; i++)
            {
                ArrayChilds = ArrayRoom[i].Split(',');
                Adults += ArrayChilds[0].AsTargetType<int>(0);
                Childs += ArrayChilds[1].AsTargetType<int>(0);
            }

            string sql = string.Format(@"select HotelID,ClassName,Price as TotalPrice,CurrencyCode,IsBreakfast from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='TravPax' AND Adults={1} AND Childs={2} AND CheckInDate='{3}' AND CheckOutDate='{4}' AND IsClose=1 AND DATEDIFF( MINUTE, UpdateTime, GETDATE()) < {5} order by Price desc"
                    , hotelID, Adults, Childs, beginTime, endTime, TravPaxCacheTime);
            DataTable dt = ControllerFactory.GetController().GetDataTable(sql);
            if (dt.Rows.Count == 0 )
            {
                TravPaxData.GetRealHotelSQuote(url, travPaxUsername, travPaxPassword, hotelID, NationalityID, beginTime, endTime, ddlRoom, Adults, Childs, rooms, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
                sql = string.Format(@"select ID,HotelID,ClassName,Price as TotalPrice,CurrencyCode,IsBreakfast from HotelPrice WITH(NOLOCK) where HotelID='{0}' AND Platform='TravPax' AND Adults={1} AND Childs={2} AND CheckInDate='{3}' AND CheckOutDate='{4}' AND IsClose=1 order by Price desc"
                    , hotelID, Adults,Childs, beginTime, endTime);
                dt = ControllerFactory.GetController().GetDataTable(sql);
            }


            DataTable PageDt = Common.GetPagedTable(dt, input_page, input_row);
            Hashtable hash = new Hashtable();
            hash.Add("total", dt.Rows.Count);
            hash.Add("rows", PageDt);
            return Newtonsoft.Json.JsonConvert.SerializeObject(hash);
        }

        public string GetRealHotelDetail(string url, string travPaxUsername, string travPaxPassword, string hotelID, string countryID, string cityID, string referenceClient, string nationalityID, string beginTime, string endTime, int ddlRoom, string rooms, int input_page, int input_row, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            DataTable dt = TravPaxData.GetRealHotelDetail(url, travPaxUsername, travPaxPassword, hotelID, countryID, cityID, referenceClient, nationalityID, beginTime, endTime, ddlRoom, rooms, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
            if (dt.Rows.Count == 0)
            {
                return "{\"total\":0,\"rows\":[]}";
            }
            DataTable PageDt = Common.GetPagedTable(dt, input_page, input_row);
            Hashtable hash = new Hashtable();
            hash.Add("total", dt.Rows.Count);
            hash.Add("rows", PageDt);
            return Newtonsoft.Json.JsonConvert.SerializeObject(hash);
        }
        /// <summary>
        /// 取消期间
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string GetCancellationPolicy(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID,string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            string result = string.Empty;
            result = TravPaxData.GetCancellationPolicy(url, travPaxUsername, travPaxPassword, TravPaxCacheTime,HotelID,HotelPriceID, NationalityID, Adults, Childs, beginTime, endTime, VIEWSTATE,  VIEWSTATEGENERATOR,  EVENTVALIDATION,  btnPost);
            return result;
        }
        //检查价格
        public string GetHotelRecheckPrice(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID,string HotelPriceID,string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            string result = string.Empty;
            result = TravPaxData.GetHotelRecheckPrice(url, travPaxUsername, travPaxPassword, TravPaxCacheTime, HotelID,HotelPriceID, NationalityID, Adults, Childs, beginTime, endTime, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
            return result;
        }
        //创建订单
        public string CreatePNR(string url, string travPaxUsername, string travPaxPassword, string TravPaxCacheTime, int HotelID, string HotelPriceID, string NationalityID, int Adults, int Childs, string beginTime, string endTime, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            string result = string.Empty;
            result = TravPaxData.CreatePNR(url, travPaxUsername, travPaxPassword, TravPaxCacheTime, HotelID,HotelPriceID, NationalityID, Adults, Childs, beginTime, endTime, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
            return result;
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public string GetBookingBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
            //return Newtonsoft.Json.JsonConvert.SerializeObject(RtsData.GetRTSbookingBySql());
        }
        /// <summary>
        /// 获取订单明细
        /// </summary>
        public string GetBookingDetail(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string AgentReferenceNumber, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            return TravPaxData.GetBookingDetail(url, travPaxUsername, travPaxPassword, BookingCode, AgentReferenceNumber, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <returns></returns>
        public string BookingCancel(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string AgentReferenceNumber, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            return TravPaxData.BookingCancel(url, travPaxUsername, travPaxPassword, BookingCode, AgentReferenceNumber, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
        }
        /// <summary>
        /// 获取报价（供外部调用）最多一次搜索9条，大人数不能大于3，小孩数不能大于2
        /// </summary>
        /// <param name="url">请求地址</param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="hotelID"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="occupancy"></param>
        /// <param name="RoomCount"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        public string  GetTravPaxRoomTypeOutPrice(string url, string travPaxUsername, string travPaxPassword, string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount,string RateplanId, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            return TravPaxData.GetTravPaxRoomTypeOutPrice(url, travPaxUsername, travPaxPassword, hotelID, beginTime, endTime, occupancy, RoomCount,RateplanId, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
        }
        /// <summary>
        /// 创建订单（外部调用）在创建订单前必须先检查价格
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="HotelID"></param>
        /// <param name="Adults"></param>
        /// <param name="Childs"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="VIEWSTATE"></param>
        /// <param name="VIEWSTATEGENERATOR"></param>
        /// <param name="EVENTVALIDATION"></param>
        /// <param name="btnPost"></param>
        /// <returns></returns>
        public string Create_Order(string url, string travPaxUsername, string travPaxPassword, string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            return TravPaxData.Create_Order(url, travPaxUsername, travPaxPassword, HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
        }
        /// <summary>
        /// 取消订单（外部调用）
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string Cancel_Order(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            return TravPaxData.Cancel_Order(url, travPaxUsername, travPaxPassword, BookingCode, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
        }
        /// <summary>
        /// 查询订单
        /// </summary>
        /// <param name="url"></param>
        /// <param name="travPaxUsername"></param>
        /// <param name="travPaxPassword"></param>
        /// <param name="BookingCode"></param>
        /// <param name="AgentReferenceNumber"></param>
        /// <returns></returns>
        public string Get_OrderDetail(string url, string travPaxUsername, string travPaxPassword, string BookingCode, string VIEWSTATE, string VIEWSTATEGENERATOR, string EVENTVALIDATION, string btnPost)
        {
            return TravPaxData.Get_OrderDetail(url, travPaxUsername, travPaxPassword, BookingCode, VIEWSTATE, VIEWSTATEGENERATOR, EVENTVALIDATION, btnPost);
        }
    }
}