﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.Data.Common;
using XiWan.Data.Restel;
using XiWan.DALFactory;
using XiWan.Model.Entities;

namespace XiWan.BLL.Restel
{
    public class RestelBLL
    {
        #region 初始化本类
        private static RestelBLL _Instance;

        private RestelDAL _RestelData;
        
        int _totalCount;

        public RestelBLL()
        {
            _RestelData = RestelData;
        }

        //对外公开的单实例静态对象
        public static RestelBLL Instance
        {
            get
            {
                return new RestelBLL(); 
            }
        }

        //数据访问对象定义
        private RestelDAL RestelData
        {
            get
            {
                if (_RestelData == null)
                    _RestelData = new RestelDAL();
                return _RestelData;
            }
        }    
        #endregion
        /// <summary>
        /// 从接口获取国家信息
        /// </summary>
        /// <returns></returns>
        public void GetRestelCountry()
        {
            RestelData.GetRestelCountry();
        }
        /// <summary>
        /// 从接口获取省信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetProvinces()
        {
            RestelData.GetProvinces();
        }
        /// <summary>
        /// 从接口获取TOWN信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetTown()
        {
            RestelData.GetTown();
        }
        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetHotel()
        {
            RestelData.GetHotel();
        }
         /// <summary>
        /// 保存房型
        /// </summary>
        /// <returns></returns>
        public string GetRoomType(string hotelCode, string nationality, string rooms, string CheckInDate, string CheckoutDate, string RestelCacheTime)
        {
            DataTable dt = new DataTable();
            int numhab1 = 0;
            int numhab2 = 0;
            int numhab3 = 0;
            string paxes1 = "0";
            string paxes2 = "0";
            string paxes3 = "0";
            string edades1 = "0";
            string edades2 = "0";
            string edades3 = "0";
            string[] ArrayRooms = rooms.Split(';');
            string[] ArrayRoom1 = ArrayRooms[0].Split(',');
            string[] ArrayRoom2 = ArrayRooms[1].Split(',');
            string[] ArrayRoom3 = ArrayRooms[2].Split(',');
            int Adults1 = 0;
            int Adults2 = 0;
            int Adults3 = 0;
            int Childs1 = 0;
            int Childs2 = 0;
            int Childs3 = 0;
            if (ArrayRoom1[0].AsTargetType<int>(0) > 0 && ArrayRoom1[4].AsTargetType<int>(0) > 0)
            {
                edades1 = string.Empty;
                numhab1 = ArrayRoom1[4].AsTargetType<int>(0);
                Adults1 = ArrayRoom1[0].AsTargetType<int>(0);
                Childs1 = ArrayRoom1[1].AsTargetType<int>(0);
                paxes1 = Adults1 + "-" + Childs1;
                if (ArrayRoom1[2].AsTargetType<int>(0) == 0 && ArrayRoom1[3].AsTargetType<int>(0) == 0)
                {
                    edades1 = "0";
                }
                else
                {
                    if (ArrayRoom1[2].AsTargetType<int>(0) > 0)
                    {
                        edades1 = ArrayRoom1[2] + ",";
                    }
                    if (ArrayRoom1[3].AsTargetType<int>(0) > 0)
                    {
                        edades1 += ArrayRoom1[3] + ",";
                    }
                    edades1 = edades1.TrimEnd(',');
                }
            }
            if (ArrayRoom2[0].AsTargetType<int>(0) > 0 && ArrayRoom2[4].AsTargetType<int>(0) > 0)
            {
                edades2 = string.Empty;
                numhab2 = ArrayRoom2[4].AsTargetType<int>(0);
                Adults2 = ArrayRoom2[0].AsTargetType<int>(0);
                Childs2 = ArrayRoom2[1].AsTargetType<int>(0);
                paxes2 = Adults2 + "-" + Childs2;
                if (ArrayRoom2[2].AsTargetType<int>(0) == 0 && ArrayRoom2[3].AsTargetType<int>(0) == 0)
                {
                    edades2 = "0";
                }
                else
                {
                    if (ArrayRoom2[2].AsTargetType<int>(0) > 0)
                    {
                        edades2 = ArrayRoom2[2] + ",";
                    }
                    if (ArrayRoom2[3].AsTargetType<int>(0) > 0)
                    {
                        edades2 += ArrayRoom2[3] + ",";
                    }
                    edades2 = edades2.TrimEnd(',');
                }
            }
            if (ArrayRoom3[0].AsTargetType<int>(0) > 0 && ArrayRoom3[4].AsTargetType<int>(0) > 0)
            {
                edades3 = string.Empty;
                numhab3 = ArrayRoom3[4].AsTargetType<int>(0);
                Adults3 = ArrayRoom3[0].AsTargetType<int>(0);
                Childs3 = ArrayRoom3[1].AsTargetType<int>(0);
                paxes3 = Adults3 + "-" + Childs3;
                if (ArrayRoom3[2].AsTargetType<int>(0) == 0 && ArrayRoom3[3].AsTargetType<int>(0) == 0)
                {
                    edades3 = "0";
                }
                else
                {
                    if (ArrayRoom3[2].AsTargetType<int>(0) > 0)
                    {
                        edades3 = ArrayRoom3[2] + ",";
                    }
                    if (ArrayRoom3[3].AsTargetType<int>(0) > 0)
                    {
                        edades3 += ArrayRoom3[3] + ",";
                    }
                    edades3 = edades3.TrimEnd(',');
                }
            }
            int Adults = Adults1 + Adults2 + Adults3;
            int Childs = Childs1 + Childs2 + Childs3;
            
            string room = string.Format(@"<numhab1>{0}</numhab1><paxes1>{1}</paxes1><edades1>{2}</edades1><numhab2>{3}</numhab2><paxes2>{4}</paxes2><edades2>{5}</edades2>
<numhab3>{6}</numhab3><paxes3>{7}</paxes3><edades3>{8}</edades3>", numhab1, paxes1, edades1, numhab2, paxes2, edades2, numhab3, paxes3, edades3);
            dt = RestelData.GetRoomTpe( hotelCode, nationality, room, CheckInDate, CheckoutDate, Adults, Childs);
            if (dt.Rows.Count == 0)
            {
                return "{\"total\":0,\"rows\":[]}";
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        /// <summary>
        /// 从接口获取报价
        /// </summary>RestelCacheTime缓存的有效时间长度（分钟）
        /// <returns></returns>
        public string GetRealHotelSQuote(string hotelCode,string nationality, string rooms, string CheckInDate, string CheckoutDate,string RestelCacheTime)
        {
            DataTable dt = new DataTable();
            int numhab1 = 0;
            int numhab2 = 0;
            int numhab3 = 0;
            string paxes1 = "0";
            string paxes2 = "0";
            string paxes3 = "0";
            string edades1 = "0";
            string edades2 = "0";
            string edades3 = "0";
            string[] ArrayRooms = rooms.Split(';');
            string[] ArrayRoom1 = ArrayRooms[0].Split(',');
            string[] ArrayRoom2 = ArrayRooms[1].Split(',');
            string[] ArrayRoom3 = ArrayRooms[2].Split(',');
            int Adults1 = 0;
            int Adults2 = 0;
            int Adults3 = 0;
            int Childs1 = 0;
            int Childs2 = 0;
            int Childs3 = 0;
            if(ArrayRoom1[0].AsTargetType<int>(0) > 0 && ArrayRoom1[4].AsTargetType<int>(0) > 0)
            {
                edades1=string.Empty;
                numhab1 = ArrayRoom1[4].AsTargetType<int>(0);
                Adults1 = ArrayRoom1[0].AsTargetType<int>(0);
                Childs1 = ArrayRoom1[1].AsTargetType<int>(0);
                paxes1 = Adults1 + "-" + Childs1;
                if(ArrayRoom1[2].AsTargetType<int>(0) == 0 && ArrayRoom1[3].AsTargetType<int>(0) == 0)
                {
                    edades1="0";
                }
                else
                {
                    if(ArrayRoom1[2].AsTargetType<int>(0) > 0)
                    {
                        edades1= ArrayRoom1[2] + ",";
                    }
                    if(ArrayRoom1[3].AsTargetType<int>(0) > 0)
                    {
                        edades1 += ArrayRoom1[3] + ",";
                    }
                    edades1 = edades1.TrimEnd(',');
                }
            }
            if(ArrayRoom2[0].AsTargetType<int>(0) > 0 && ArrayRoom2[4].AsTargetType<int>(0) > 0)
            {
                edades2=string.Empty;
                numhab2 = ArrayRoom2[4].AsTargetType<int>(0);
                Adults2 = ArrayRoom2[0].AsTargetType<int>(0);
                Childs2 = ArrayRoom2[1].AsTargetType<int>(0);
                paxes2 = Adults2 + "-" + Childs2;
                if(ArrayRoom2[2].AsTargetType<int>(0) == 0 && ArrayRoom2[3].AsTargetType<int>(0) == 0)
                {
                    edades2="0";
                }
                else
                {
                    if(ArrayRoom2[2].AsTargetType<int>(0) > 0)
                    {
                        edades2= ArrayRoom2[2] + ",";
                    }
                    if(ArrayRoom2[3].AsTargetType<int>(0) > 0)
                    {
                        edades2 += ArrayRoom2[3] + ",";
                    }
                    edades2 = edades2.TrimEnd(',');
                }
            }
            if(ArrayRoom3[0].AsTargetType<int>(0) > 0 && ArrayRoom3[4].AsTargetType<int>(0) > 0)
            {
                edades3=string.Empty;
                numhab3 = ArrayRoom3[4].AsTargetType<int>(0);
                Adults3 = ArrayRoom3[0].AsTargetType<int>(0);
                Childs3 = ArrayRoom3[1].AsTargetType<int>(0);
                paxes3 = Adults3 + "-" + Childs3;
                if(ArrayRoom3[2].AsTargetType<int>(0) == 0 && ArrayRoom3[3].AsTargetType<int>(0) == 0)
                {
                    edades3="0";
                }
                else
                {
                    if(ArrayRoom3[2].AsTargetType<int>(0) > 0)
                    {
                        edades3= ArrayRoom3[2] + ",";
                    }
                    if(ArrayRoom3[3].AsTargetType<int>(0) > 0)
                    {
                        edades3 += ArrayRoom3[3] + ",";
                    }
                    edades3 = edades3.TrimEnd(',');
                }
            }
            int Adults = Adults1 + Adults2 + Adults3;
            int Childs = Childs1 + Childs2 + Childs3;
            string sqlDt = string.Format(@"select ID,HotelID,ClassName,Price as TotalPrice,CurrencyCode,IsBreakfast,RoomAdults,RoomChilds from HotelPrice WITH(NOLOCK) where HotelID={0}
AND Platform='Restel'  AND Adults={1} AND Childs={2}  AND CheckInDate='{3}' AND CheckOutDate='{4}'  AND IsClose=1 AND DATEDIFF( MINUTE, UpdateTime, GETDATE()) < {5}  order by ID"
                , hotelCode, Adults, Childs, CheckInDate, CheckoutDate, RestelCacheTime);
            dt = ControllerFactory.GetController().GetDataTable(sqlDt);
            if (dt.Rows.Count > 0)
            {
                return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
            }
            string room = string.Format(@"<numhab1>{0}</numhab1><paxes1>{1}</paxes1><edades1>{2}</edades1><numhab2>{3}</numhab2><paxes2>{4}</paxes2><edades2>{5}</edades2>
<numhab3>{6}</numhab3><paxes3>{7}</paxes3><edades3>{8}</edades3>", numhab1, paxes1, edades1, numhab2, paxes2, edades2, numhab3, paxes3, edades3);
            dt = RestelData.GetRealHotelSQuote(hotelCode, nationality,room, CheckInDate, CheckoutDate, Adults, Childs);
            if (dt.Rows.Count == 0)
            {
                return "{\"total\":0,\"rows\":[]}";
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        //国籍
        public string GetRestelnationality()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(RestelData.GetNationality());
        }
        //目的地
        public string GetResteldestination(string strName)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(RestelData.GetDestination(strName));
        }
        /// <summary>
        /// 获取酒店信息(通过数据)
        /// </summary>
        /// <returns></returns>
        public string GetHotelBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        //获取取消期限
        public string GetCancellationPolicy(string HotelPriceIDs)
        {
            return RestelData.GetCancellationPolicy(HotelPriceIDs);
        }
        //预订
        public string Reserva(string HotelPriceIDs)
        {
            return RestelData.Reserva(HotelPriceIDs);
        }
        /// <summary>
        /// 预订确认 或 取消预订
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber"></param>
        /// <param name="ReservationType"></param>
        /// <returns></returns>
        public string ReservaConfirmation(string BookingNumber, string ReservationType)
        {
            return RestelData.ReservaConfirmation(BookingNumber, ReservationType);
        }
        //取消订单
        public string ReservaCancellation(string BookingNumber)
        {
            return RestelData.ReservaCancellation(BookingNumber);
        }
        //查看订单
        public string ReservationInformation(string BookingNumber)
        {
            return RestelData.ReservationInformation( BookingNumber);
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public string GetBookingBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        //查看订单
        public string RestlLastReservations(string seachMode,string BookingNumber,string searchDate,string searchHotelName,string searchCustomerName)
        {
            return RestelData.RestlLastReservations(seachMode, BookingNumber, searchDate, searchHotelName, searchCustomerName);
        }
        /// <summary>
        /// 查看预订后取消生成的费用
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber"></param>
        /// <param name="Codusu"></param>
        /// <returns></returns>
        public string GetCancellationFees(string BookingNumber)
        {
            return RestelData.GetCancellationFees(BookingNumber);
        }
        /// <summary>
        /// 查询酒店评论
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelCode"></param>
        /// <returns></returns>
        public string GetHotelRemarks(string HotelCode, string CheckInDate, string CheckoutDate)
        {
            return RestelData.GetHotelRemarks(HotelCode, CheckInDate, CheckoutDate);
        }
        /// <summary>
        /// 获取报价对外接口(提供外部调用接口)
        /// </summary>
        /// <returns></returns>
        public string GetRestelRoomTypeOutPrice( string hotelCode, string CheckInDate, string CheckoutDate, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            return RestelData.GetRestelRoomTypeOutPrice(hotelCode, CheckInDate, CheckoutDate, occupancy, RoomCount, RateplanId);
        }
        /// <summary>
        /// 创建订单（提供外部接口）
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="HotelID"></param>
        /// <param name="RateplanId"></param>
        /// <param name="customers"></param>
        /// <param name="inOrderNum"></param>
        /// <param name="beginTime"></param>
        /// <param name="endTime"></param>
        /// <param name="roomNum"></param>
        /// <returns></returns>
        public string Create_Order(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney, string BookingReference)
        {
            return RestelData.Create_Order(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum, TotalMoney, BookingReference);
        }
        /// <summary>
        /// 查询订单（提供外部接口）
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="Codusu"></param>
        /// <param name="BookingNumber"></param>
        /// <returns></returns>
        public string Get_OrderDetail( string HotelID, string BookingNumber)
        {
            return RestelData.Get_OrderDetail(HotelID, BookingNumber);
        }
        /// <summary>
        /// 取消订单（提供对外接口）
        /// </summary>
        /// <param name="RestelUrl"></param>
        /// <param name="Identidad"></param>
        /// <param name="BookingNumber"></param>
        /// <returns></returns>
        public string Cancel_Order(string HotelID, string BookingNumber)
        { 
            return RestelData.Cancel_Order(HotelID, BookingNumber);
        }
        //获取可卖酒店信息
        public void GetSaleableHotel()
        {
            RestelData.GetSaleableHotel();
        }
        //获取补充服务17，补充可卖酒店信息
        public void GetSaleableHotelInformation()
        {
            RestelData.GetSaleableHotelInformation();
        }
    }
}