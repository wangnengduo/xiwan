﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Data;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using XiWan.Data.Common;
using System.Collections;
using System.Text.RegularExpressions;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;

namespace XiWan.BLL
{
    public class PlanitourBLL
    {
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        #region 初始化本类
        private static PlanitourBLL _Instance;

        private PlanitourDAL _PlanitourData;
        
        int _totalCount;

        public PlanitourBLL()
        {
            _PlanitourData = PlanitourData;
        }

        //对外公开的单实例静态对象
        public static PlanitourBLL Instance
        {
            get
            {
                return new PlanitourBLL(); 
            }
        }

        //数据访问对象定义
        private PlanitourDAL PlanitourData
        {
            get
            {
                if (_PlanitourData == null)
                    _PlanitourData = new PlanitourDAL();
                return _PlanitourData;
            }
        }    
        #endregion

        /// <summary>
        /// 获取国家信息（从对方系统拿数据）
        /// </summary>
        /// <param name="url"></param>
        public string GetCountries(string url)
        {
            string result = "0";
            if (PlanitourData.GetCountries(url))
            {
                result = "1";
            }
            return result;
        }
        /// <summary>
        /// 获取目的地（从对方系统拿数据）
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetDestinations(string url,int countryid)
        {
            string result = "0";
            if (PlanitourData.GetDestinations(url, countryid))
            {
                result = "1";
            }
            return result;
        }
    }
}