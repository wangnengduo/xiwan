﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.DALFactory;
using XiWan.Data.Common;
using XiWan.Data.SunSeries;
using XiWan.Model.Entities;

namespace XiWan.BLL.SunSeries
{
    public class SunSeriesBLL
    {
        #region 初始化本类
        private static SunSeriesBLL _Instance;

        private SunSeriesDAL _SunSeriesData;

        int _totalCount;

        public SunSeriesBLL()
        {
            _SunSeriesData = SunSeriesData;
        }

        //对外公开的单实例静态对象
        public static SunSeriesBLL Instance
        {
            get
            {
                return new SunSeriesBLL();
            }
        }

        //数据访问对象定义
        private SunSeriesDAL SunSeriesData
        {
            get
            {
                if (_SunSeriesData == null)
                    _SunSeriesData = new SunSeriesDAL();
                return _SunSeriesData;
            }
        }
        #endregion

        /// <summary>
        /// 获取token
        /// </summary>
        public void GetAuthenticateTo()
        {
            SunSeriesData.GetAuthenticateTo();
        }

        /// <summary>
        /// 获取token
        /// </summary>
        public void GetAuthenticate()
        {
            SunSeriesData.GetAuthenticate();
        }
        /// <summary>
        /// 获取国家到数据库
        /// </summary>
        public void GetSunSeriesCountries()
        {
            SunSeriesData.GetSunSeriesCountries();
        }
        /// <summary>
        /// 获取城市到数据库
        /// </summary>
        public void GetSunSeriesCities()
        {
            SunSeriesData.GetSunSeriesCities();
        }
        /// <summary>
        /// 获取国籍到数据库
        /// </summary>
        public void GetSunSeriesNationalities()
        {
            SunSeriesData.GetSunSeriesNationalities();
        }
        /// <summary>
        /// 获取酒店
        /// </summary>
        public void GetSunSeriesHotels()
        {
            SunSeriesData.GetSunSeriesHotels();
        }
        //国籍
        public string GetSunSeriesNationality()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(SunSeriesData.GetNationality());
        }
        //目的地
        public string GetSunSeriesDestination(string strName)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(SunSeriesData.GetDestination(strName));
        }
        /// <summary>
        /// 获取酒店信息
        /// </summary>
        /// <returns></returns>
        public string GetHotel(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        /// <summary>
        /// 保存房型
        /// </summary>
        public DataTable GetRoomType(string hotelID, string beginTime, string endTime, List<IntOccupancyInfo> occupancys, int RoomCount, string RateplanId)
        {
            return SunSeriesData.GetRoomType(hotelID, beginTime, endTime, occupancys, RoomCount, RateplanId);
        }
        /// <summary>
        /// 获取房型报价
        /// </summary>
        public DataTable GetRoomTypePrice(string hotelID, string beginTime, string endTime, List<IntOccupancyInfo> occupancys, int RoomCount, string RateplanId)
        {
            return SunSeriesData.GetRoomTypePrice(hotelID, beginTime, endTime, occupancys, RoomCount, RateplanId);
        }
        //重新报价
        public string createQuote(string hotelID, string rateplanId, string HotelPriceID)
        {
            return SunSeriesData.createQuote(hotelID, rateplanId, HotelPriceID);
        }
        //创建订单
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, string HotelPriceID)
        {
            return SunSeriesData.book(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum, HotelPriceID);
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public string GetBookingBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        //取消订单
        public string cancel(string hotelID, string bookingCode)
        {
            return SunSeriesData.cancel(hotelID, bookingCode);
        }
        //查询订单
        public string SunSeriesCheckingOrder(string hotelID, string bookingCode)
        {
            return SunSeriesData.SunSeriesCheckingOrder(hotelID, bookingCode);
        }
        //报价
        public string GetSunSeriesRoomTypeOutPrice(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        { 
            return SunSeriesData.GetSunSeriesRoomTypeOutPrice( hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId);
        }
        //创建订单
        public string Create_Order(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney)
        {
            return SunSeriesData.Create_Order(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum, TotalMoney);
        }
        //报价接口返回原始数据
        public string GetRoomTypePriceToOriginal(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            return SunSeriesData.GetRoomTypePriceToOriginal( hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId);
        }


        //报价
        public string GetSunSeriesRoomTypeOutPriceTo(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            return SunSeriesData.GetSunSeriesRoomTypeOutPriceTo(hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId);
        }

        //创建订单
        public string Create_OrderTo(string HotelID, string RateplanId, BookingInfo bookingInfo, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney)
        {
            return SunSeriesData.Create_OrderTo(HotelID, RateplanId, bookingInfo, inOrderNum, beginTime, endTime, roomNum, TotalMoney);
        }
    }
}