﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Data.All4go;

namespace XiWan.BLL.All4go
{
    public class All4goBLL
    {
        #region 初始化本类
        private static All4goBLL _Instance;

        private All4goDAL _All4goData;
        
        int _totalCount;

        public All4goBLL()
        {
            _All4goData = All4goData;
        }

        //对外公开的单实例静态对象
        public static All4goBLL Instance
        {
            get
            {
                return new All4goBLL(); 
            }
        }

        //数据访问对象定义
        private All4goDAL All4goData
        {
            get
            {
                if (_All4goData == null)
                    _All4goData = new All4goDAL();
                return _All4goData;
            }
        }    
        #endregion

        //获取货币类型
        public void GetCurrencyCode()
        {
            All4goData.GetCurrencyCode();
        }
        //获取所有地点
        public void GetLocationCode()
        {
            All4goData.GetLocationCode();
        }

        //获取打包产品
        public void GetPackages()
        {
            All4goData.GetPackages();
        }

        //获取设施
        public void GetFacilities()
        {
            All4goData.GetFacilities();
        }

        //获取Giata酒店地址
        public void GetLocationOfGiataHotel()
        {
            All4goData.GetLocationOfGiataHotel();
        }
    }
}