﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using XiWan.DALFactory;
using XiWan.Data.Asianoverland;
using XiWan.Data.Common;
using XiWan.Model.Entities;

namespace XiWan.BLL.Asianoverland
{
    public class AsianoverlandBLL
    {
        #region 初始化本类
        private static AsianoverlandBLL _Instance;

        private AsianoverlandDAL _AsianoverlandData;
        
        int _totalCount;

        public AsianoverlandBLL()
        {
            _AsianoverlandData = AsianoverlandData;
        }

        //对外公开的单实例静态对象
        public static AsianoverlandBLL Instance
        {
            get
            {
                return new AsianoverlandBLL(); 
            }
        }
        //数据访问对象定义
        private AsianoverlandDAL AsianoverlandData
        {
            get
            {
                if (_AsianoverlandData == null)
                    _AsianoverlandData = new AsianoverlandDAL();
                return _AsianoverlandData;
            }
        }    
        #endregion

        /// <summary>
        /// 从接口获取国家信息保存到数据
        /// </summary>
        /// <returns></returns>
        public void GetCountry()
        {
            AsianoverlandData.GetCountry();
        }
        /// <summary>
        /// 从接口获取市信息保存到数据
        /// </summary>
        /// <returns></returns>
        public void GetCity()
        {
            AsianoverlandData.GetCity();
        }
        /// <summary>
        /// 从接口获取国籍信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetNationality()
        {
            AsianoverlandData.GetNationality();
        }
        /// <summary>
        /// 从接口获取酒店信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetHotels()
        {
            AsianoverlandData.GetHotels();
        }
        /// <summary>
        /// 从接口获取酒店详细信息并保存到数据库
        /// </summary>
        /// <returns></returns>
        public void GetHoteldetails()
        {
            AsianoverlandData.GetHoteldetails();
        }
        //国籍
        public string GetNationalityToSql()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(AsianoverlandData.GetNationalityToSql());
        }
        //目的地
        public string GetDestinationToSql(string strName)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(AsianoverlandData.GetDestinationToSql(strName));
        }
        /// <summary>
        /// 获取酒店信息
        /// </summary>
        /// <returns></returns>
        public string GetHotelToSql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        //获取报价
        public DataTable GetHotelQuote(string hotelID, string beginTime, string endTime, List<IntOccupancyInfo> occupancys, int RoomCount, string RateplanId, string nationality, string countryResidence)
        {
            return AsianoverlandData.GetHotelQuote(hotelID, Convert.ToDateTime(beginTime).ToString("dd/MM/yyyy"), Convert.ToDateTime(endTime).ToString("dd/MM/yyyy"), occupancys, RoomCount, RateplanId, nationality, countryResidence);
        }
        //获取取消政策
        public string GetHotelCancellationPolicy(string HotelID, string RateplanId)
        {
            return AsianoverlandData.GetHotelCancellationPolicy(HotelID, RateplanId);
        }
        //取消订单
        public string cancel(string hotelID, string bookingCode)
        {
            return AsianoverlandData.cancel(hotelID, bookingCode);
        }
        //查询订单
        public string AsianoverlandCheckingOrder(string hotelID, string bookingCode)
        {
            return AsianoverlandData.AsianoverlandCheckingOrder(hotelID, bookingCode);
        }
        //查询订单
        public string AsianoverlandBookingDetail(string hotelID, string AgentBookingReference)
        {
            return AsianoverlandData.AsianoverlandBookingDetail(hotelID, AgentBookingReference);
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public string GetBookingBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
        }
        //创建订单
        public string book(string HotelID, string RateplanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            return AsianoverlandData.book(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum);
        }
        //获取取消费用
        public string GetCancellationFee(string HotelID, string bookingCode)
        {
            return AsianoverlandData.GetCancellationFee(HotelID, bookingCode);
        }

        //报价
        public string GetRoomTypeOutPrice(string hotelID, string beginTime, string endTime, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            return "";//AsianoverlandData.GetRoomTypeOutPrice(hotelID, beginTime, endTime, occupancy, RoomCount, RateplanId);
        }
        //创建订单
        public string Create_Order(string HotelID, string RateplanId, BookingInfo customers, string inOrderNum, string beginTime, string endTime, int roomNum, decimal TotalMoney)
        {
            return "";//AsianoverlandData.Create_Order(HotelID, RateplanId, customers, inOrderNum, beginTime, endTime, roomNum, TotalMoney);
        }
    }
}