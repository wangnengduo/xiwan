﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using XiWan.Data;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;
using XiWan.Data.Common;
using System.Collections;
using System.Text.RegularExpressions;
using XiWan.DALFactory;
using XiWan.DALFactory.Model;
using XiWan.Model.Entities;

namespace XiWan.BLL
{
    public class RtsBLL
    {
        ControllerBase con = ControllerFactory.GetNewController(e_ConsType.Main);
        #region 初始化本类
        private static RtsBLL _Instance;

        private RtsDAL _RtsData;
        
        int _totalCount;

        public RtsBLL()
        {
            _RtsData = RtsData;
        }

        //对外公开的单实例静态对象
        public static RtsBLL Instance
        {
            get
            {
                return new RtsBLL(); 
            }
        }

        //数据访问对象定义
        private RtsDAL RtsData
        {
            get
            {
                if (_RtsData == null)
                    _RtsData = new RtsDAL();
                return _RtsData;
            }
        }    
        #endregion

        //获取目的地
        public string GetRTSnationality()
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(RtsData.GetRtsNationalityBySql());
        }
        //国籍
        public string GetRTSdestination(string strName)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(RtsData.GetRtsDestinationBySql(strName));
        }
        //区域 
        public string GetRtsLocationCode(string destination)
        {
            return Newtonsoft.Json.JsonConvert.SerializeObject(RtsData.GetRtsLocationCodeBySql(destination));
        }
        //判断目的地是否数据库存在
        public string GetRTScityCodeYn(string destination)
        {
            string re = "0";
            DataTable dt = RtsData.GetRTScityCodeYnBySql(destination);
            if (dt.Rows.Count > 0)
            {
                re = "1";
            }
            return re;
        }
        /// <summary>
        /// 获取酒店
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columns"></param>
        /// <param name="order"></param>
        /// <param name="pageSize"></param>
        /// <param name="pageIndex"></param>
        /// <param name="sqlWhere"></param>
        /// <param name="totalCount"></param>
        /// <returns></returns>
        public string GetHotel(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
            //return RtsData.GetRTSHotelBySql(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
        }
        /// <summary>
        /// 获取报价
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="SiteCode"></param>
        /// <param name="Password"></param>
        /// <param name="Nationality"></param>
        /// <param name="Destination"></param>
        /// <param name="BeginTime"></param>
        /// <param name="EndTime"></param>
        /// <param name="staring"></param>
        /// <param name="LocationCode"></param>
        /// <param name="SalesCompCode"></param>
        /// <param name="ItemName"></param>
        /// <param name="ItemCode"></param>
        /// <param name="RoomTypeCode1"></param>
        /// <param name="Room1Count"></param>
        /// <param name="Room1ChildAge1"></param>
        /// <param name="Room1ChildAge2"></param>
        /// <param name="RoomTypeCode2"></param>
        /// <param name="Room2Count"></param>
        /// <param name="Room2ChildAge1"></param>
        /// <param name="Room2ChildAge2"></param>
        /// <param name="RoomTypeCode3"></param>
        /// <param name="Room3Count"></param>
        /// <param name="Room3ChildAge1"></param>
        /// <param name="Room3ChildAge2"></param>
        /// <param name="RoomTypeCode4"></param>
        /// <param name="Room4Count"></param>
        /// <param name="Room4ChildAge1"></param>
        /// <param name="Room4ChildAge2"></param>
        /// <param name="RoomTypeCode5"></param>
        /// <param name="Room5Count"></param>
        /// <param name="Room5ChildAge1"></param>
        /// <param name="Room5ChildAge2"></param>
        /// <param name="input_page"></param>
        /// <param name="input_row"></param>
        /// <returns></returns>
        public string Quote(string Url,string SiteCode, string Password, string Nationality, string Destination, string BeginTime, string EndTime, int staring, string LocationCode, string SalesCompCode, string ItemName, string ItemCode, string RoomTypeCode1, int Room1Count, int Room1ChildAge1, int Room1ChildAge2, string RoomTypeCode2, int Room2Count, int Room2ChildAge1, int Room2ChildAge2, string RoomTypeCode3, int Room3Count, int Room3ChildAge1, int Room3ChildAge2, string RoomTypeCode4, int Room4Count, int Room4ChildAge1, int Room4ChildAge2, string RoomTypeCode5, int Room5Count, int Room5ChildAge1, int Room5ChildAge2,int input_page,int input_row)
        {
            DataTable dt = RtsData.GetRTSQuote(Url, SiteCode, Password, BeginTime, EndTime, staring, LocationCode, SalesCompCode, ItemName, ItemCode, RoomTypeCode1, Room1Count, Room1ChildAge1, Room1ChildAge2, RoomTypeCode2, Room2Count, Room2ChildAge1, Room2ChildAge2, RoomTypeCode3, Room3Count, Room3ChildAge1, Room3ChildAge2, RoomTypeCode4, Room4Count, Room4ChildAge1, Room4ChildAge2, RoomTypeCode5, Room5Count, Room5ChildAge1, Room5ChildAge2, input_page, input_row);
            if (dt.Rows.Count == 0)
            {
                return "{\"total\":0,\"rows\":[]}";
            }
            DataTable PageDt = Common.GetPagedTable(dt, input_page, input_row);
            Hashtable hash = new Hashtable();
            hash.Add("total", dt.Rows.Count);
            hash.Add("rows", PageDt);
            return Newtonsoft.Json.JsonConvert.SerializeObject(hash);
        }
        /// <summary>
        /// 获取取消期间
        /// </summary>
        /// <returns></returns>
        public string GetRTSCancelDeadline(string Url, string SiteCode, string Password, string ItemCode, string ItemNo, string RoomTypeCode, string BeginTime, string EndTime, string Nationality, string BedTypeCode)
        {
            return RtsData.GetRTSCancelDeadline(Url, SiteCode, Password, ItemCode, ItemNo, RoomTypeCode, BeginTime, EndTime, Nationality, BedTypeCode);
        }
        /// <summary>
        /// 获取提示
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="SiteCode"></param>
        /// <param name="Password"></param>
        /// <param name="Nationality"></param>
        /// <param name="Destination"></param>
        /// <param name="BeginTime"></param>
        /// <param name="EndTime"></param>
        /// <param name="staring"></param>
        /// <param name="LocationCode"></param>
        /// <param name="SalesCompCode"></param>
        /// <param name="ItemCode"></param>
        /// <param name="ItemNo"></param>
        /// <param name="RoomTypeCode"></param>
        /// <param name="RoomTypeCode1"></param>
        /// <param name="Room1Count"></param>
        /// <param name="Room1ChildAge1"></param>
        /// <param name="Room1ChildAge2"></param>
        /// <param name="RoomTypeCode2"></param>
        /// <param name="Room2Count"></param>
        /// <param name="Room2ChildAge1"></param>
        /// <param name="Room2ChildAge2"></param>
        /// <param name="RoomTypeCode3"></param>
        /// <param name="Room3Count"></param>
        /// <param name="Room3ChildAge1"></param>
        /// <param name="Room3ChildAge2"></param>
        /// <param name="RoomTypeCode4"></param>
        /// <param name="Room4Count"></param>
        /// <param name="Room4ChildAge1"></param>
        /// <param name="Room4ChildAge2"></param>
        /// <param name="RoomTypeCode5"></param>
        /// <param name="Room5Count"></param>
        /// <param name="Room5ChildAge1"></param>
        /// <param name="Room5ChildAge2"></param>
        /// <returns></returns>
        public string GetRemarkHotelInformationForCustomerCount(string Url, string SiteCode, string Password, string Nationality, string Destination, string BeginTime, string EndTime, int staring, string LocationCode, string SalesCompCode, string ItemCode, string ItemNo, string RoomTypeCode, string RoomTypeCode1, int Room1Count, int Room1ChildAge1, int Room1ChildAge2, string RoomTypeCode2, int Room2Count, int Room2ChildAge1, int Room2ChildAge2, string RoomTypeCode3, int Room3Count, int Room3ChildAge1, int Room3ChildAge2, string RoomTypeCode4, int Room4Count, int Room4ChildAge1, int Room4ChildAge2, string RoomTypeCode5, int Room5Count, int Room5ChildAge1, int Room5ChildAge2)
        {
            return RtsData.GetRemarkHotelInformationForCustomerCount( Url,  SiteCode,  Password,  Nationality,  Destination,  BeginTime,  EndTime,  staring,  LocationCode,  SalesCompCode,  ItemCode,  ItemNo,  RoomTypeCode,  RoomTypeCode1,  Room1Count,  Room1ChildAge1,  Room1ChildAge2,  RoomTypeCode2,  Room2Count,  Room2ChildAge1,  Room2ChildAge2,  RoomTypeCode3,  Room3Count,  Room3ChildAge1,  Room3ChildAge2,  RoomTypeCode4,  Room4Count,  Room4ChildAge1,  Room4ChildAge2,  RoomTypeCode5,  Room5Count,  Room5ChildAge1,  Room5ChildAge2);
        }
        /// <summary>
        /// 创建订单
        /// </summary>
        public string CreateSystemBooking(string Url, string SiteCode, string Password, string Nationality, string SalesCompCode, string SalesSiteCode, string SalesUserNo, string ItemCode, string ItemNo, string BeginTime, string EndTime, string RoomTypeCode, string BreakfastTypeName)
        {
            return  RtsData.CreateSystemBooking(Url, SiteCode, Password, Nationality, SalesCompCode, SalesSiteCode, SalesUserNo, ItemCode, ItemNo, BeginTime, EndTime, RoomTypeCode, BreakfastTypeName);
        }
        /// <summary>
        /// 获取订单明细
        /// </summary>
        public string GetBookingDetail(string Url, string SiteCode, string Password, string BookingCode, string AgentReferenceNumber)
        {
            return RtsData.GetBookingDetail(Url, SiteCode, Password, BookingCode, AgentReferenceNumber);
        }
        /// <summary>
        /// 获取订单明细（通过自定义字段）
        /// </summary>
        public string GetAgentReferenceNumber(string Url, string SiteCode, string Password, string AgentReferenceNumber)
        {
            return RtsData.GetAgentReferenceNumber(Url, SiteCode, Password, AgentReferenceNumber);
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <returns></returns>
        public string BookingCancel(string Url, string SiteCode, string Password, string BookingCode, string ItemNo, string SalesUserNo, string AgentReferenceNumber)
        {
            return RtsData.BookingCancel( Url, SiteCode, Password, BookingCode, ItemNo, SalesUserNo, AgentReferenceNumber);
        }
        /// <summary>
        /// 从数据库查询订单信息
        /// </summary>
        /// <returns></returns>
        public string GetRTSbookingBySql(string tableName, string columns, string order, int pageSize, int pageIndex, string sqlWhere, out int totalCount)
        {
            DataTable dt = SqlPagerHelper.GetPager(tableName, columns, order, pageSize, pageIndex, sqlWhere, out totalCount);
            return Newtonsoft.Json.JsonConvert.SerializeObject(dt);
            //return Newtonsoft.Json.JsonConvert.SerializeObject(RtsData.GetRTSbookingBySql());
        }
        /// <summary>
        /// 获得优惠券
        /// </summary>
        /// <returns></returns>
        public string GetBookingVoucher(string Url, string SiteCode, string Password, string BookingCode)
        {
            Respone resp = new Respone();
            string result = string.Empty;
            string strXml = string.Format(@"<?xml version='1.0' encoding='utf-8'?>
<soap:Envelope xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xmlns:xsd='http://www.w3.org/2001/XMLSchema' xmlns:soap='http://schemas.xmlsoap.org/soap/envelope/'>
<soap:Header><BaseInfo xmlns='http://www.rts.co.kr/'><SiteCode>XXXXX-00</SiteCode><Password>XXXX</Password><RequestType>NetPartner</RequestType>
</BaseInfo></soap:Header><soap:Body><GetBookingVoucher xmlns='http://www.rts.co.kr/'><BookingVoucher><BookingCode>CNWF23370</BookingCode>
</BookingVoucher></GetBookingVoucher></soap:Body></soap:Envelope>", SiteCode, Password, BookingCode);
            try
            {
                //发起请求
                result = Common.WebReq(Url, strXml).Replace("&lt;", "<").Replace("&gt;", ">").Replace("<![CDATA[", "").Replace("]]>", "");
                //日记
                LogHelper.DoOperateLog(string.Format("Studio:RTS获得优惠券接口,请求数据:{0} , 返回数据：{1} ， Time:{2} ", strXml, result, DateTime.Now.ToString()), "RTS接口");
            }
            catch (Exception ex)
            {
                //日记
                LogHelper.DoOperateLog(string.Format("Studio:RTS获得优惠券接口 ,err:{0} , 请求数据:{1} , Time:{2} ", ex.Message, strXml, DateTime.Now.ToString()), "RTS接口");
                resp = new Respone() { code = "99", mes = "取消失败" + ex.ToString() };
                return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
            }
            return Newtonsoft.Json.JsonConvert.SerializeObject(resp);
        }
        /// <summary>
        /// 获取报价对外接口(提供外部调用接口)
        /// </summary>
        /// <param name="Url"></param>
        /// <param name="SiteCode"></param>
        /// <param name="Password"></param>
        /// <param name="BeginTime"></param>
        /// <param name="EndTime"></param>
        /// <param name="ItemCode"></param>
        /// <param name="occupancy"></param>
        /// <param name="RoomCount"></param>
        /// <returns></returns>
        public string GetRtsRoomTypeOutPrice(string Url, string SiteCode, string Password, string BeginTime, string EndTime, string ItemCode, IntOccupancyInfo occupancy, int RoomCount, string RateplanId)
        {
            return "";//RtsData.GetRtsRoomTypeOutPrice(Url, SiteCode, Password, BeginTime, EndTime, ItemCode, occupancy, RoomCount, RateplanId);
        }
        /// <summary>
        /// 创建订单
        /// </summary>
        public string Create_Order(string Url, string SiteCode, string Password, string SalesCompCode, string SalesSiteCode, string SalesUserNo, string roomTypeId, string ratePlanId, List<roomCustomers> customers, string inOrderNum, string beginTime, string endTime, int roomNum)
        {
            return RtsData.Create_Order(Url, SiteCode, Password, SalesCompCode, SalesSiteCode, SalesUserNo, roomTypeId, ratePlanId, customers, inOrderNum, beginTime, endTime, roomNum);
        }
        /// <summary>
        /// 取消订单
        /// </summary>
        /// <returns></returns>
        public string Cancel_Order(string Url, string SiteCode, string Password, string BookingCode, string SalesUserNo)
        {
            return RtsData.Cancel_Order(Url, SiteCode, Password, BookingCode, SalesUserNo);
        }
        /// <summary>
        /// 获取订单明细
        /// </summary>
        public string Get_OrderDetail(string Url, string SiteCode, string Password, string BookingCode)
        {
            return RtsData.Get_OrderDetail(Url, SiteCode, Password, BookingCode);
        }
        public class Respone
        {
            public string code { get; set; }
            public string result { get; set; }
            public string mes { get; set; }
        }
    }
}