using System.Reflection;
using System.Runtime.CompilerServices;

[assembly: AssemblyTitle("XiWan.DBUtility(20090623)")]
[assembly: AssemblyDescription("Data Access Application Model By LiTianPing")]
[assembly: AssemblyConfiguration("LiTianPing")]
[assembly: AssemblyCompany("Maticsoft")]
[assembly: AssemblyProduct("XiWan.DBUtility")]
[assembly: AssemblyCopyright("Copyright (C) Maticsoft 2004-2010")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("3.5.0")]
[assembly: AssemblyDelaySign(false)]
[assembly: AssemblyKeyFile("")]
[assembly: AssemblyKeyName("")]
