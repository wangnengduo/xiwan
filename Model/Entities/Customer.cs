﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Model.Entities
{
    public class Customer
    {
        public string name { get; set; }//姓名
        public string lastName { get; set; }//姓
        public string firstName { get; set; }//名
        public int sex { get; set; }//性别：1男  2女
        public int age { get; set; }//岁数
    }
}