﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Model.Entities
{
    public class RoomOutPrice
    {
        /// <summary>
        /// 每日房价房态
        /// </summary>
        [Serializable]
        public class Rate : CommonRate
        {
            public double AmountBeforeTax { get; set; }
            public int Bonus { get; set; }

            public string DateStr
            {
                get
                {
                    return base.Date.ToString("M-dd");
                }
            }
            public string WeekStr
            {
                get
                {
                    string[] weekname = new string[] { "周日", "周一", "周二", "周三", "周四", "周五", "周六" };
                    return ServiceBase.ToUnicode(weekname[(int)base.Date.DayOfWeek]);
                }
            }
            public bool Available { get; set; }
            /// <summary>
            /// 即时确认  1(即时确认)/0(非即时确认)
            /// </summary>
            public bool InstantCode { get; set; }

            public decimal CostPrice { get; set; }

        }
        /// <summary>
        /// 价格计划类，属于房型的下一级，用于输出json格式的对象
        /// </summary>
        public class RatePlan
        {
            private string _RoomInventoryStatusCode { get; set; }
            /// <summary>
            /// 房态 0满房，1可住
            /// </summary>
            public string RoomInventoryStatusCode
            {
                get
                {
                    if (Available) { return "1"; }
                    else
                    { return "0"; }
                }
                // set { _RoomInventoryStatusCode = value; }
            }

            /// <summary>
            /// 房间ID
            /// </summary>
            public string RoomId { get; set; }
            /// <summary>
            /// 房型名称
            /// </summary>
            public string RoomName { get; set; }
            /// <summary>
            /// 床型
            /// </summary>
            public string BedType = "";
            /// <summary>
            /// 入住时间内不能超售的最小值。当大于0小于5时，表示目前仅剩的房量;0表示房量充足
            /// </summary>
            public int CurrentAlloment { get; set; }
            /// <summary>
            /// 客人类型 All=统一价
            ///Chinese =内宾价，需提示客人“须持大陆身份证入住”；
            ///OtherForeign =外宾价，需提示客人“须持国外护照入住”；
            ///HongKong =港澳台客人价，需提示客人“须持港澳台身份证入住”；
            ///Japanese=日本客人价，需提示客人“须持日本护照入住”
            /// </summary>
            public string CustomerType { get; set; }
            /// <summary>
            /// 预定最少数量
            /// </summary>
            public int MinAmount { get; set; }

            private int _RoomBonus { get; set; }
            /// <summary>
            /// 房间返现
            /// </summary>
            public int RoomBonus
            {
                get
                {
                    if (_RoomBonus == -1)
                    {
                        if (Rates.Count > 0)
                        {
                            _RoomBonus = Rates[0].Bonus;
                        }
                        else
                        {
                            _RoomBonus = 0;
                        }
                    }
                    return _RoomBonus;
                }
                set
                {
                    _RoomBonus = value;
                }
            }
            /// <summary>
            /// 
            /// 担保返回数据
            /// </summary>
            [Serializable]
            public class VouchResult
            {
                /// <summary>
                /// 是否需要担保	0－不担保1－担保	N
                /// </summary>
                public String IsVouch { get; set; }
                /// <summary>
                /// 担保类型	1－首晚担保2－全额担保	Y
                /// </summary>
                public int VouchMoneyType { get; set; }	//	
                /// <summary>
                /// 担保中文描述		Y
                /// </summary>
                public String CNDescription { get; set; }//
                public int Count { get; set; }
                public string LastTime { get; set; }
                /// <summary>
                /// 担保英文描述		Y
                /// </summary>
                public String ENDescription { get; set; }//
                /// <summary>
                /// //最晚取消变更时间	担保订单可取消的时间，如果返回的时间小于当前时间，则代表此订单不可变更取消	Y
                /// </summary>
                public DateTime LastCancelTime { get; set; }
                /// <summary>
                /// 超时点钟，值范围[0,23]，超时时显示 非必有字段 (同程)
                /// </summary>
                public int? OverTime { get; set; }
            }
            /// <summary>
            /// 房型id
            /// </summary>
            public string RoomTypeId { get; set; }
            /// <summary>
            /// 价格计划id
            /// </summary>
            public string RatePlanId { get; set; }
            /// <summary>
            /// 价格计划名称
            /// </summary>
            public string RatePlanName { get; set; }
            /// <summary>
            /// 宽带情况
            /// </summary>
            public string Net { get; set; }
            /// <summary>
            /// 房型图片
            /// </summary>
            public string RoomTypeImg { get; set; }
            /// <summary>
            /// 房型均价
            /// </summary>      ;
            public string AveragePrice { get; set; }
            /// <summary>
            /// 加床价
            /// </summary>
            public string AddBed = "-1";
            /// <summary>
            /// 早餐情况
            /// </summary>
            public string Breakfast { get; set; }
            /// <summary>
            /// 担保条件
            /// </summary>
            public string Guarantee { get; set; }
            /// <summary>
            /// 附加信息
            /// </summary>
            public string AddValue { get; set; }
            /// <summary>
            /// 促销信息
            /// </summary>
            public string Promotion { get; set; }
            public string XRatePlanName { get; set; }
            /// <summary>
            ///支付方式。目前全部前台支付
            /// </summary>
            public string PayMent { get; set; }
            /// <summary>
            /// 描述
            /// </summary>
            public string Desc { get; set; }
            /// <summary>
            /// 房态 true=有房,false=满房
            /// </summary>
            public bool Available { get; set; }
            public string VouchNode { get; set; }
            public VouchResult Vouch { get; set; }
            /// <summary>
            /// 房价对象List
            /// </summary>
            public List<Rate> Rates { get; set; }
            public int ExtraBonus { get; set; }
            public string Currency = "RMB";
            public decimal SMoney { get; set; }
            /// <summary>
            /// 即时确认  1(即时确认)/0(非即时确认)
            /// </summary>
            public bool InstantCode { get; set; }
            public decimal CnPrice { get; set; }

            public decimal RMBPrice { get; set; }

            public bool ViennaDirectFlag { get; set; }

            public string CurrencyCode { get; set; }

            public int MinDays { get; set; }
            public string InvoiceMode { get; set; }
        }
        public class Room
        {
            /// <summary>
            /// 附加信息
            /// </summary>
            public string AddValue { get; set; }
            /// <summary>
            /// 是否需要提供手机号
            /// </summary>
            public bool IsHasMobile { get; set; }
            /// <summary>
            /// 房型名称
            /// </summary>
            public string RoomName { get; set; }
            /// <summary>
            /// 加床情况
            /// </summary>
            public string AddBed { get; set; }
            /// <summary>
            /// 房间ID，请使用字符型，不同酒店不会有同样的房间ID
            /// </summary>
            public string RoomId { get; set; }
            /// <summary>
            /// 房型ID，不同酒店之间有重复
            /// </summary>
            public string RoomTypeId { get; set; }
            /// <summary>
            /// 床型数据
            /// </summary>
            public string BedType { get; set; }
            /// <summary>
            /// 宽带数据
            /// </summary>
            public string Net { get; set; }
            /// <summary>
            /// 房间面积
            /// </summary>
            public string Area { get; set; }
            /// <summary>
            /// 房间楼层
            /// </summary>
            public string RoomFloor { get; set; }
            /// <summary>
            /// 房型描述
            /// </summary>
            public string Desc { get; set; }
            /// <summary>
            /// 房型价格
            /// </summary>
            public int RoomPrice { get; set; }
            /// <summary>
            /// 房型说明
            /// </summary>
            public string Note { get; set; }
            /// <summary>
            /// 房型详情
            /// </summary>
            public string Detail { get; set; }
            public int ExtraMoney { get; set; }
            /// <summary>
            /// 广告渠道 qunar/lp/
            /// </summary>
            public string AdWay { get; set; }
            public string XRoomId { get; set; }
            public string XRoomName { get; set; }
            public List<RatePlan> RatePlan { get; set; }



            public string MroomId { get; set; }
            /// <summary>
            /// 母房型名称（经过处理过的名称）
            /// </summary>
            public string MroomName { get; set; }
            public string MroomNameEn { get; set; }

            /// <summary>
            /// 原始艺龙母房型名称
            /// </summary>
            public string YMroomName { get; set; }

            //国际酒店成人和儿童数量
            public int Adults { get; set; }
            public int Children { get; set; }
            //儿童年龄（多个时用 | 分割）
            public string ChildAges { get; set; }

            //最大入住人数 0表示没有设置
            public int MaxPerson { get; set; }

            //产品来源elong hds
            public string ProductSource { get; set; }

            //当前房型库存
            public int CurrentAlloment { get; set; }

            public Product ProductELIntel { get; set; }

        }
    }
}