﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XiWan.Model.Entities
{
    [Serializable]
    public class HotelWithPrice
    {
        public int HotelId { get; set; }
        public decimal LowestPrice { get; set; }
        public HotelDetail Detail { get; set; }
        public List<Product> Products { get; set; }
        public Gift Gifts { get; set; }
    }
    [Serializable]
    public class HotelDetail
    {
        public string HotelNameCn { get; set; }
        public string HotelNameEn { get; set; }
        public string OpenningDate { get; set; }
        public string LastDecorateDate { get; set; }
        public string Star { get; set; }
        public string SummaryCn { get; set; }
        public string SummaryEn { get; set; }
        public string AddressCn { get; set; }
        public string AddressEn { get; set; }
        public List<string> AcceptCreditCards { get; set; }
        public List<string> HotelFacilities { get; set; }
        public string HotelPolicy { get; set; }
        public string HotelFee { get; set; }
        public List<string> HotelImages { get; set; }
        public string HotelTelephone { get; set; }
        public GeoInfo GeoInfo { get; set; }


    }
    [Serializable]
    public class GeoInfo
    {
        public string GeoLongitude { get; set; }
        public string GeoLatitude { get; set; }
    }
    [Serializable]
    public class RegionInfo
    {
        public int RegionId { get; set; }
        public string RegionNameCn { get; set; }
        public string RegionNameEn { get; set; }
        public string CountryCode { get; set; }
        public string CountryNameCn { get; set; }
        public string CountryNameEn { get; set; }
    }
    [Serializable]
    public class Product
    {
        /// <summary>
        /// 产品唯一编码,成单需要产品ID
        /// </summary>
        public string ProductId { get; set; }
        public string RoomName { get; set; }
        public string ProductDesc { get; set; }
        public bool IsPromotion { get; set; }
        /// <summary>
        /// 销售总价,包含了房费、税费、服务费和加人费，不包括保险
        /// </summary>
        public decimal TotalPrice { get; set; }
        /// <summary>
        /// 总结算价,当采用结算价模式的时候才返回，成单的总价计算将以结算价为基础了。
        /// </summary>
        public decimal CostTotalPrice { get; set; }

        public List<NightlyPrice> NightlyPrices { get; set; }
        /// <summary>
        /// 房费的均价,不包括税费、服务费和加人费
        /// </summary>
        public decimal AveragePrice { get; set; }
        /// <summary>
        /// 房费的结算均价
        /// </summary>
        public decimal CostAveragePrice { get; set; }
        public decimal TaxesAndFees { get; set; }
        /// <summary>
        /// 加人费
        /// </summary>
        public decimal ExtraPersonFees { get; set; }

        /// <summary>
        /// 可用库存
        /// </summary>
        public int CurrentAlloment { get; set; }
        /// <summary>
        /// 最多入住人数
        /// </summary>
        public int MaxPersonNum { get; set; }

        /// <summary>
        /// 0-不可取消 1-限时取消
        /// </summary>
        public int CancellationType { get; set; }
        public string CancellationDesc { get; set; }
        /// <summary>
        /// 取消政策
        /// </summary>
        public List<Cancellation> CancellationLists { get; set; }


        /// <summary>
        /// 如果为空，默认酒店当地时间14点(可以办理入住的时间)
        /// </summary>
        public string ArriveStartTime { get; set; }
        /// <summary>
        /// 如果为空，默认酒店当地时间24点
        /// </summary>
        public string ArriveEndTime { get; set; }

        /// <summary>
        /// 1--仅限中国国籍的客人入住
        /// </summary>
        public int Nationality { get; set; }


        public string Breakfast { get; set; }
        public string Internet { get; set; }
        public string SmokingPreference { get; set; }
        public List<string> FacilityList { get; set; }
        public List<string> GiftIds { get; set; }
        public List<BedType> BedTypes { get; set; }
        public List<string> RoomImageList { get; set; }
        public Insurance Insurance { get; set; }

        /// <summary>
        /// 房型聚合ID	聚合的房型ID;int64类型；[仅供参考，此信息引起的投诉，艺龙免责]
        /// </summary>
        public string MroomId { get; set; }

        /// <summary>
        /// 房型聚合名称[仅供参考，此信息引起的投诉，艺龙免责]
        /// </summary>
        public string MroomName { get; set; }

        /// <summary>
        /// 房型聚合英文名称  [仅供参考，此信息引起的投诉，艺龙免责]
        /// </summary>
        public string MroomNameEn { get; set; }

        /// <summary>
        /// 到店另付费
        /// </summary>
        public decimal UnIncludeTotal { get; set; }
        /// <summary>
        /// 到店另付费明细列表
        /// </summary>
        public List<UnInclude> UnIncludeList { get; set; }

        /// <summary>
        ///是否立即确认  0：不是立即确认；1 是立即确认
        /// </summary>
        public int IsInstantConfirm { get; set; }

    }
    [Serializable]
    public class NightlyPrice
    {
        public DateTime Date { get; set; }
        /// <summary>
        /// 原始房价,促销前的房价
        /// </summary>
        public decimal BasePrice { get; set; }
        /// <summary>
        /// 当日销售房价
        /// </summary>
        public decimal CurrentPrice { get; set; }
    }
    [Serializable]
    public class Cancellation
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string LocalStartDate { get; set; }
        public string LocalEndDate { get; set; }
        /// <summary>
        /// 0-不可退款 1-可退款
        /// </summary>
        public string Refundable { get; set; }
        /// <summary>
        /// 当可退款的时候才看这个属性决定罚金是多少。不可退款的时候可以理解为罚金就是总金额
        /// </summary>
        public decimal CustomerPrice { get; set; }
    }
    [Serializable]
    public class BedType
    {
        public string BedTypeId { get; set; }
        public string BedTypeName { get; set; }
    }
    [Serializable]
    public class Insurance
    {
        public string InsuranceId { get; set; }
        public string InsuranceName { get; set; }
        public string InsuranceCode { get; set; }
        public string Summary { get; set; }
        public string Description { get; set; }
        /// <summary>
        /// 投保比率
        /// </summary>
        public decimal GuaranteeRate { get; set; }
        /// <summary>
        /// 可赔偿最高比率
        /// </summary>
        public decimal CompensationRate { get; set; }
        /// <summary>
        /// 投保金额
        /// </summary>
        public decimal InsuranceFee { get; set; }
    }
    [Serializable]
    public class Gift
    {

    }

    /// <summary>
    /// 到店另付费明细
    /// </summary>
    [Serializable]
    public class UnInclude
    {
        public string Name { get; set; }
        public string Currency { get; set; }
        public decimal Amount { get; set; }
    }

 


}
