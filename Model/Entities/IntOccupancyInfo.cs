﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Model.Entities
{
    public class IntOccupancyInfo
    {
        public int adults { get; set; }
        public string childAges { get; set; }
        public int children { get; set; }
    }
}