﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.Model.Entities
{
    public class Reservation
    {
        /// <summary>
        /// 酒店在美联国际接口中的唯 一标识   
        /// </summary>
        public string hotel_id { get; set; }

        /// <summary>
        /// 美联国际确认码
        /// </summary>
        public string aic_reservation_id { get; set; }
        /// <summary>
        /// 对接方平台唯一的订单表示 
        /// </summary>
        public string partner_reservation_id { get; set; }
        /// <summary>
        /// 订单状态
        /// 1 wait confirm 2 confirmed 3 confirm failed 4 Cancelled 5 cancel pending 6 cancel failed
        /// </summary>
        public string reservation_status { get; set; }
        /// <summary>
        /// 酒店方确认码
        /// </summary>
        public string hotel_confirmation_number { get; set; }
        /// <summary>
        /// 酒店方取消码
        /// </summary>
        public string hotel_cancellation_number { get; set; }
        /// <summary>
        /// 个人的特殊要求
        /// </summary>
        public string special_request { get; set; }
        /// <summary>
        /// 入住日期
        /// </summary>
        public string check_in_date { get; set; }
        /// <summary>
        /// 退房日期
        /// </summary>
        public string check_out_date { get; set; }
        /// <summary>
        /// 每个房间内入住的成人数
        /// </summary>
        public string adult_number { get; set; }
        /// <summary>
        /// 每个房间内入住的小孩数
        /// </summary>
        public string kids_number { get; set; }
        /// <summary>
        /// 所有个人信息
        /// </summary>
        public List<Guests> guests { get; set; }
        /// <summary>
        /// 所有小孩年龄
        /// </summary>
        public List<int> children_ages { get; set; }
        /// <summary>
        /// 房间数
        /// </summary>
        public string total_rooms { get; set; }
        /// <summary>
        /// 订单的费用
        /// </summary>
        public string total_amount { get; set; }
        /// <summary>
        /// 货币种类
        /// </summary>
        public string currency { get; set; }
        /// <summary>
        /// 取消时间 UTC
        /// </summary>
        public string cancellation_time { get; set; }
    }
    /// <summary>
    /// 包含所有个人信息，每个房间内 得客人信息是一个独立的 subarray 
    /// </summary>
    public class Guests
    {
        /// <summary>
        /// 
        /// </summary>
        public List<string> adult_name_prefix { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> adult_given_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> adult_surname { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> child_name_prefix { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> child_given_name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<string> child_surname { get; set; }
    }

}