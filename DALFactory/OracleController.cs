/*
 * 作者: Wong
 * 日期: 2018-6-13
 * 数据库访问类
 */
using System.Diagnostics;
using Microsoft.VisualBasic;
using System;
using System.Text;
using System.IO;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Data.OracleClient;
using System.Runtime.Serialization.Formatters.Binary;

namespace XiWan.DALFactory
{
    /// <summary>
    /// SQL Server数据访问基类。
    /// </summary>
    public class OracleController : ControllerBase
    {

        #region 变量
        public const string c_CannotEmpty = "不能为空！";
        public const string c_CannotDelete = "数据已经被引用！不允许删除。";
        #endregion
        
        #region 构造函数
        public OracleController() : base()
        {
            m_controllerType = e_ControllerType.Oracle;
            m_strFlagParameter = ":";
            m_strSqlSelect = "SELECT a.{0} FROM {1} a {2} WHERE {3}";
        }
        #endregion

        #region 重写
        protected override void InitializeCommand(IDbCommand cmd, CommandType cmdType, string cmdText, IDataParameter[] parameters)
        {
            if (cmd == null) throw new Exception("command object is nothing");

            cmd.CommandType = cmdType;
            cmd.CommandText = cmdText;
            cmd.CommandTimeout = m_intCommandTimeout;

            if ((parameters != null) && (parameters.Length > 0))
            {
                foreach (IDataParameter prm in parameters)
                {
                    cmd.Parameters.Add(prm as OracleParameter);
                }
            }
        }

        protected override IDataParameter GetReturnValueParameter()
        {
            OracleParameter prmReturnValue = new OracleParameter("@RETURN_VALUE", SqlDbType.Int);
            prmReturnValue.Direction = ParameterDirection.ReturnValue;
            return prmReturnValue as IDataParameter;
        }

        protected override IDbConnection NewConnection()
        {
            return new OracleConnection(ConnectionString);
        }

        /// <summary>
        /// 填充数据集。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="dst"></param>
        /// <returns></returns>
        public override int FillDataSet(string text, DataSet dst)
        {
            OracleConnection cnn = CreateConnection() as OracleConnection;
            OracleCommand cmd = CreateCommand(cnn) as OracleCommand;
            OracleDataAdapter dap = new OracleDataAdapter(cmd);
            int intReturn = 0;

            try
            {
                InitializeCommand(cmd, CommandType.Text, text, null);
                intReturn = dap.Fill(dst);
            }
            finally
            {
                dap.Dispose();
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return intReturn;
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="condition"></param>
        /// <param name="dtb"></param>
        /// <param name="withSchema"></param>
        /// <returns></returns>
        public override int FillDataTable(string text, string condition, DataTable dtb, bool withSchema)
        {
            OracleConnection cnn = CreateConnection() as OracleConnection;
            OracleCommand cmd = CreateCommand(cnn) as OracleCommand;
            OracleDataAdapter dap = new OracleDataAdapter(cmd);

            string strSql = "";
            int intReturn = 0;

            if ((condition != null) && (condition.Length > 0))
            {
                strSql = text + " where " + condition;
            }
            else
            {
                strSql = text;
            }

            try
            {
                InitializeCommand(cmd, CommandType.Text, strSql, null);
                if (withSchema)
                {
                    dap.FillSchema(dtb, SchemaType.Source);
                    EntityBase.InitializeColumnDefalutValue(dtb);
                }
                intReturn = dap.Fill(dtb);                
            }
            finally
            {
                dap.Dispose();
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return intReturn;
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <param name="dtb"></param>
        /// <param name="withSchema"></param>
        /// <returns></returns>
        public override int FillDataTable(string text, IDataParameter[] paras, DataTable dtb, bool withSchema)
        {
            OracleConnection cnn = CreateConnection() as OracleConnection;
            OracleCommand cmd = CreateCommand(cnn) as OracleCommand;
            OracleDataAdapter dap = new OracleDataAdapter(cmd);
            int intReturn = 0;

            try
            {
                InitializeCommand(cmd, CommandType.Text, text, paras);
                if (withSchema)
                {
                    dap.FillSchema(dtb, SchemaType.Mapped);
                    EntityBase.InitializeColumnDefalutValue(dtb);
                }
                intReturn = dap.Fill(dtb);
            }
            finally
            {
                dap.Dispose();
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return intReturn;
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="paras"></param>
        /// <param name="dtb"></param>
        /// <returns></returns>
        public override int FillDataTableSP(string spName, IDataParameter[] paras, DataTable dtb)
        {
            OracleConnection cnn = CreateConnection() as OracleConnection;
            OracleCommand cmd = CreateCommand(cnn) as OracleCommand;
            OracleDataAdapter dap = new OracleDataAdapter(cmd);
            int intReturn = 0;

            try
            {
                InitializeCommand(cmd, CommandType.StoredProcedure, spName, paras);
                intReturn = dap.Fill(dtb);
            }
            finally
            {
                dap.Dispose();
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return intReturn;
        }

        /// <summary>
        /// 填充数据集。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="paras"></param>
        /// <param name="dst"></param>
        /// <returns></returns>
        public override int FillDataSetSP(string spName, IDataParameter[] paras, DataSet dst)
        {
            OracleConnection cnn = CreateConnection() as OracleConnection;
            OracleCommand cmd = CreateCommand(cnn) as OracleCommand;
            OracleDataAdapter dap = new OracleDataAdapter(cmd);
            int intReturn = 0;

            try
            {
                InitializeCommand(cmd, CommandType.StoredProcedure, spName, paras);
                intReturn = dap.Fill(dst);
            }
            finally
            {
                dap.Dispose();
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return intReturn;
        }
        #endregion
       
        #region 序列化
        //public string SerializeObject(object obj)
        //{
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    MemoryStream memoryStream = new MemoryStream();
        //    formatter.Serialize(memoryStream, obj);
        //    byte[] aryByte = memoryStream.ToArray();
        //    memoryStream.Close();
        //    return Convert.ToBase64String(aryByte, 0, aryByte.Length);
        //}

        //public object DeserializeObject(string base64ObjectString)
        //{
        //    byte[] aryByte = Convert.FromBase64String(base64ObjectString);
        //    BinaryFormatter formatter = new BinaryFormatter();
        //    MemoryStream memoryStream = new MemoryStream(aryByte, 0, aryByte.Length);
        //    object obj = (object)formatter.Deserialize(memoryStream);
        //    memoryStream.Close();
        //    return obj;
        //}
        #endregion

        #region 查询
        //public string GetExpression(e_Operator op, string keyValue)
        //{
        //    return GetExpression(op, keyValue, "");
        //}

        //public string GetExpression(e_Operator op, string keyValue, string field)
        //{
        //    string strExpression = "";
        //    switch (op)
        //    {
        //        case e_Operator.EqualStr:
        //            strExpression = "='" + keyValue + "'";
        //            break;
        //        case e_Operator.Equal:
        //            strExpression = "=" + keyValue;
        //            break;
        //        case e_Operator.Like:
        //            strExpression = "like '%" + keyValue + "%'";
        //            break;
        //        case e_Operator.LeftLike:
        //            strExpression = "like '%" + keyValue + "'";
        //            break;
        //        case e_Operator.RightLike:
        //            strExpression = "like '" + keyValue + "%'";
        //            break;
        //        default:
        //            break;
        //    }
        //    if (field.Length > 0)
        //    {
        //        strExpression = field + " " + strExpression;
        //    }
        //    strExpression = " " + strExpression + " ";
        //    return strExpression;
        //}

        ///// <summary>
        ///// 生成判断一个ID是否在以字符串存储的多个ID列表中的SQL表达式。
        ///// </summary>
        ///// <param name="keyValue"></param>
        ///// <param name="field"></param>
        ///// <returns></returns>
        //public string GetIDListExpression(string keyValue, string field)
        //{
        //    StringBuilder sb = new StringBuilder(100);
        //    sb.Append(" ");
        //    sb.Append("(" + field + "='" + keyValue + "')");
        //    sb.Append(" or ");
        //    sb.Append("(" + field + " like '" + keyValue + ",%')");
        //    sb.Append(" or ");
        //    sb.Append("(" + field + " like '%," + keyValue + "')");
        //    sb.Append(" or ");
        //    sb.Append("(" + field + " like '%," + keyValue + ",%')");
        //    sb.Append(" ");
        //    return sb.ToString();
        //}
        #endregion


    }
}
