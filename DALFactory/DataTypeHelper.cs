/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System.Diagnostics;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Collections;

namespace XiWan.DALFactory
{
    public class DataTypeHelper
    {
        private static DateTime m_dtmEmptyDateTime = new DateTime(1900, 1, 1, 0, 0, 0,0);

        // 时间类型空值
        public static DateTime EmptyDateTime
        {
            get
            {
                return m_dtmEmptyDateTime;
            }
        }

        public static int EmptyInt
        {
            get
            {
                return int.MinValue;
            }
        }

        public static decimal EmptyDecimal
        {
            get
            {
                return decimal.MinValue;
            }
        }

        public static bool IsDateTimeEmpty(DateTime value)
        {
            return value.Equals(EmptyDateTime) || value.Equals(DateTime.MinValue);
        }

        public static bool IsIntEmpty(int value)
        {
            return value == int.MinValue;
        }

        public static bool IsDecimalEmpty(decimal value)
        {
            return value == decimal.MinValue;
        }

        public static bool IsDigitEmpty(decimal value)
        {
            if (value == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsDigitEmpty(int value)
        {
            if (value == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsDigitEmpty(string value)
        {
            if (value == "0" || value == "0.0" || value == "0.00")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsDigit(string typename)
        {
            string strTypeName = typename;
            if (strTypeName.IndexOf("System.") == -1)
            {
                strTypeName = "System." + strTypeName;
            }
            if ((typename == "System.Decimal") || (typename == "System.Single") || (typename == "System.Double") || (typename == "System.Int16") || (typename == "System.Int32") || (typename == "System.Int64"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsInt(string typename)
        {
            string strTypeName = typename;
            if (strTypeName.IndexOf("System.") == -1)
            {
                strTypeName = "System." + strTypeName;
            }
            if ((strTypeName == "System.Int16") || (strTypeName == "System.Int32") || (strTypeName == "System.Int64"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static bool IsDecimal(string typename)
        {
            string strTypeName = typename;
            if (strTypeName.IndexOf("System.") == -1)
            {
                strTypeName = "System." + strTypeName;
            }
            if ((strTypeName == "System.Decimal") || (strTypeName == "System.Single"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// 判断字符串是否为数值。
        /// </summary>
        /// <param name="strVal"></param>
        /// <returns></returns>
        public static bool IsNumeric(string strVal)
        {
            if (strVal != "")
            {
                System.Text.RegularExpressions.Regex reg1 = new System.Text.RegularExpressions.Regex("-?([0]|([1-9]+\\d{0,}?))(.[\\d]+)?$");
                return reg1.IsMatch(strVal);
            }
            else
                return true;
        }
    }
}