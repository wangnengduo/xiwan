﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 * 加密
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Web;

namespace XiWan.DALFactory
{
    public class DESEncryptor
    {
        #region 私有成员

        // 输入字符串 
        private string m_inputString = null;

        // 输出字符串 
        private string m_outString = null;

        // 输入文件路径 
        private string m_inputFilePath = null;

        // 输出文件路径 
        private string m_outFilePath = null;

        // 加密密钥 
        private string m_encryptKey = null;

        // 解密密钥 
        private string m_decryptKey = null;

        //加密向量
        private string m_encryptIv = null;

        //解密向量
        private string m_decryptIv = null;

        private byte[] m_key = null;

        private byte[] m_IV = null;

        // 提示信息 
        private string m_noteMessage = null;
        #endregion

        #region 公共属性

        // 输入字符串 
        public string InputString
        {
            get
            {
                return m_inputString;
            }
            set
            {
                m_inputString = value;
            }
        }

        // 输出字符串
        public string OutString
        {
            get
            {
                return m_outString;
            }
            set
            {
                m_outString = value;
            }
        }

        // 输入文件路径 
        public string InputFilePath
        {
            get
            {
                return m_inputFilePath;
            }
            set
            {
                m_inputFilePath = value;
            }
        }

        // 输出文件路径 
        public string OutFilePath
        {
            get
            {
                return m_outFilePath;
            }
            set
            {
                m_outFilePath = value;
            }
        }

        // 加密密钥 
        public string EncryptKey
        {
            get
            {
                return m_encryptKey;
            }
            set
            {
                m_encryptKey = value;
            }
        }

        // 解密密钥 
        public string DecryptKey
        {
            get
            {
                return m_decryptKey;
            }
            set
            {
                m_decryptKey = value;
            }
        }

        // 加密向量 
        public string EncryptIv
        {
            get
            {
                return m_encryptIv;
            }
            set
            {
                m_encryptIv = value;
            }
        }

        // 解密向量 
        public string DecryptIv
        {
            get
            {
                return m_decryptIv;
            }
            set
            {
                m_decryptIv = value;
            }
        }

        // 错误信息 
        public string NoteMessage
        {
            get
            {
                return m_noteMessage;
            }
            set
            {
                m_noteMessage = value;
            }
        }
        #endregion

        #region 构造函数

        public DESEncryptor()
        {
        }
        #endregion

        #region DES加密字符串
        // 加密字符串 
        // 注意:密钥必须为８位 
        // </summary> 
        // <param name="strText">字符串</param> 
        // <param name="encryptKey">密钥</param> 

        public void DesEncrypt()
        {
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(this.m_encryptKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                byte[] inputByteArray = Encoding.UTF8.GetBytes(this.m_inputString);

                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateEncryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                this.m_outString = Convert.ToBase64String(ms.ToArray());
            }
            catch (System.Exception error)
            {
                this.m_noteMessage = error.Message;
            }
        }
        #endregion

        #region DES解密字符串
        // <param name="this.inputString">加了密的字符串</param> 
        // <param name="decryptKey">密钥</param>  
        public void DesDecrypt()
        {
            byte[] byKey = null;
            byte[] IV = { 0x12, 0x34, 0x56, 0x78, 0x90, 0xAB, 0xCD, 0xEF };
            byte[] inputByteArray = new Byte[this.m_inputString.Length];

            try
            {
                byKey = System.Text.Encoding.UTF8.GetBytes(m_decryptKey.Substring(0, 8));
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                inputByteArray = Convert.FromBase64String(this.m_inputString);
                MemoryStream ms = new MemoryStream();
                CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(byKey, IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.Encoding encoding = new System.Text.UTF8Encoding();
                this.m_outString = encoding.GetString(ms.ToArray());
            }
            catch (System.Exception error)
            {
                this.m_noteMessage = error.Message;
            }
        }

        #endregion

        #region DES加密字符串为文件
        // DES加密文件 
        // <param name="strText">字符串</param> 
        // <param name="this.outFilePath">输出文件路径</param> 
        // <param name="encryptKey">密钥</param> 
        public void DesEncryptToFile()
        {
            try
            {
                SetKeyIV(true);
                byte[] inputByteArray = Encoding.UTF8.GetBytes(this.m_inputString);

                FileStream fout = new FileStream(this.m_outFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                DESCryptoServiceProvider des = new DESCryptoServiceProvider();
                CryptoStream cs = new CryptoStream(fout, des.CreateEncryptor(m_key, m_IV), CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);

                cs.FlushFinalBlock();
                cs.Close();
                fout.Close(); ;
            }
            catch (System.Exception error)
            {
                this.m_noteMessage = error.Message;
            }
        }
        #endregion

        #region DES加密字符串为文件
        // DES加密文件 
        // <param name="strText">字符串</param> 
        // <param name="this.outFilePath">输出文件路径</param> 
        // <param name="encryptKey">密钥</param> 
        public void DesEncryptStream(MemoryStream ms)
        {
            try
            {
                SetKeyIV(true);

                FileStream fout = new FileStream(this.m_outFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                fout.SetLength(0);

                //Create variables to help with read and write. 
                byte[] bin = new byte[100]; //This is intermediate storage for the encryption. 
                long rdlen = 0; //This is the total number of bytes written. 
                long totlen = ms.Length; //This is the total length of the input file. 
                int len; //This is the number of bytes to be written at a time. 

                DES des = new DESCryptoServiceProvider();
                CryptoStream encStream = new CryptoStream(fout, des.CreateEncryptor(m_key, m_IV), CryptoStreamMode.Write);

                //Read from the input file, then encrypt and write to the output file. 
                ms.Position = 0;
                while (rdlen < totlen)
                {
                    len = ms.Read(bin, 0, bin.Length);
                    encStream.Write(bin, 0, len);
                    rdlen = rdlen + len;
                }

                encStream.FlushFinalBlock();
                encStream.Close();
                fout.Close();
                ms.Close();
            }
            catch (System.Exception error)
            {
                this.m_noteMessage = error.Message;
            }
        }
        #endregion

        #region DES加密文件
        // DES加密文件 
        // <param name="this.inputFilePath">源文件路径</param> 
        // <param name="this.outFilePath">输出文件路径</param> 
        // <param name="encryptKey">密钥</param> 
        public void FileDesEncrypt()
        {
            try
            {
                SetKeyIV(true);
                FileStream fin = new FileStream(this.m_inputFilePath, FileMode.Open, FileAccess.Read);
                FileStream fout = new FileStream(this.m_outFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                fout.SetLength(0);

                //Create variables to help with read and write. 
                byte[] bin = new byte[100]; //This is intermediate storage for the encryption. 
                long rdlen = 0; //This is the total number of bytes written. 
                long totlen = fin.Length; //This is the total length of the input file. 
                int len; //This is the number of bytes to be written at a time. 

                DES des = new DESCryptoServiceProvider();
                CryptoStream encStream = new CryptoStream(fout, des.CreateEncryptor(m_key, m_IV), CryptoStreamMode.Write);

                //Read from the input file, then encrypt and write to the output file. 
                while (rdlen < totlen)
                {
                    len = fin.Read(bin, 0, bin.Length);
                    encStream.Write(bin, 0, len);
                    rdlen = rdlen + len;
                }

                encStream.FlushFinalBlock();
                encStream.Close();
                fout.Close();
                fin.Close();
            }
            catch (System.Exception error)
            {
                this.m_noteMessage = error.Message.ToString();
            }
        }

        #endregion

        #region DES解密文件
        // 解密文件 
        // <param name="this.inputFilePath">加密了的文件路径</param> 
        // <param name="this.outFilePath">输出文件路径</param> 
        // <param name="decryptKey">密钥</param> 
        public void FileDesDecrypt()
        {
            try
            {
                SetKeyIV(false);
                FileStream fin = new FileStream(this.m_inputFilePath, FileMode.Open, FileAccess.Read);
                FileStream fout = new FileStream(this.m_outFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                fout.SetLength(0);

                //Create variables to help with read and write. 
                byte[] bin = new byte[100]; //This is intermediate storage for the encryption.
                long rdlen = 0; //This is the total number of bytes written.  
                long totlen = fin.Length; //This is the total length of the input file. 
                int len; //This is the number of bytes to be written at a time. 
                DES des = new DESCryptoServiceProvider();
                CryptoStream encStream = new CryptoStream(fout, des.CreateDecryptor(m_key, m_IV), CryptoStreamMode.Write);

                //Read from the input file, then encrypt and write to the output file. 

                while (rdlen < totlen)
                {
                    len = fin.Read(bin, 0, bin.Length);
                    encStream.Write(bin, 0, len);
                    rdlen = rdlen + len;
                }

                encStream.FlushFinalBlock();
                encStream.Close();
                fout.Close();
                fin.Close();
            }
            catch (System.Exception error)
            {
                this.m_noteMessage = error.Message.ToString();
            }
        }

        public MemoryStream GetDesDecryptStream()
        {
            try
            {
                SetKeyIV(false);
                FileStream fin = new FileStream(this.InputFilePath, FileMode.Open, FileAccess.Read);
                MemoryStream msTemp = new MemoryStream();

                //Create variables to help with read and write. 
                byte[] bin = new byte[100]; //This is intermediate storage for the encryption.
                long rdlen = 0; //This is the total number of bytes written.  
                long totlen = fin.Length; //This is the total length of the input file. 
                int len;                  //This is the number of bytes to be written at a time. 
                DES des = new DESCryptoServiceProvider();
                CryptoStream encStream = new CryptoStream(msTemp, des.CreateDecryptor(m_key, m_IV), CryptoStreamMode.Write);

                //Read from the input file, then encrypt and write to the output file. 
                msTemp.Position = 0;
                while (rdlen < totlen)
                {
                    len = fin.Read(bin, 0, bin.Length);
                    encStream.Write(bin, 0, len);
                    rdlen = rdlen + len;
                }
                encStream.FlushFinalBlock();

                StreamReader sr = new StreamReader(msTemp);
                msTemp.Position = 0;

                string netStr = sr.ReadToEnd();
                int i = netStr.Length;
                encStream.Close();
                fin.Close();
                sr.Close();

                MemoryStream ms = new MemoryStream();
                byte[] buf = System.Text.Encoding.UTF8.GetBytes(netStr);

                ms.Write(buf, 0, buf.Length);
                ms.Position = 0;  // If we dont do this, nothing is returned!
                return ms;
            }
            catch (System.Exception error)
            {
                this.m_noteMessage = error.Message.ToString();
            }
            return null;
        }

        private void SetKeyIV(bool isEncrypt)
        {
            if (isEncrypt)
            {
                m_key = System.Text.Encoding.UTF8.GetBytes(m_encryptKey.Substring(13, 4) + m_encryptKey.Substring(5, 4));
                m_IV = System.Text.Encoding.UTF8.GetBytes(this.m_encryptIv.Substring(15, 4) + m_encryptIv.Substring(7, 4));
            }
            else
            {
                m_key = System.Text.Encoding.UTF8.GetBytes(m_decryptKey.Substring(13, 4) + m_decryptKey.Substring(5, 4));
                m_IV = System.Text.Encoding.UTF8.GetBytes(this.m_decryptIv.Substring(15, 4) + m_decryptIv.Substring(7, 4));
            }
        }
        #endregion

        #region MD5
        // <param name="strText">text</param> 
        // <returns>md5 Encrypt string</returns>
        public string MD5Encrypt(string strInput)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(strInput));
            return System.Text.Encoding.Default.GetString(result);
        }
        #endregion
    }
}
