﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Web;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;

namespace XiWan.DALFactory
{
    /// <summary>
    /// 输入输出相关
    /// </summary>
    public class IOHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public IOHelper()
        {

        }


        #region 保存文件与图片文件       
        #region 一般文件上传
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="postFile"></param>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static bool UploadFile(HttpPostedFile postFile, string fileName)
        {
            string path = fileName.Substring(0, fileName.LastIndexOf('\\'));

            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                postFile.SaveAs(fileName);
            }
            catch (Exception ex)
            {
#if DEBUG
                throw ex;
#endif

                return false;
            }
            return true;
        }
        #endregion 

        #region 保存文件与图片文件,根据设置保存不同的文件
        /// <summary>
        /// 保存文件与图片文件,根据设置保存不同的文件
        /// 若为图片文件，则根据设置分别保存不同规格的图片：
        /// </summary>
        /// <param name="postFile">文件HttpPostedFile</param>
        /// <param name="strFilePath">保存路径</param>
        /// <param name="SourceWidth">SourceWidth=1表示保存原来图,SourceWidth>1表示不保存原来图，而是缩放到此宽度的图保存；若为0不保存</param>
        /// <param name="ImgBWidth">保存第二张缩略图的宽度(50小)，保存后的文件以b.jpg结尾；若为0不保存</param>
        /// <param name="ImgCWidth">保存第三张缩略图的宽度(120中)，保存后的文件以c.jpg结尾；若为0不保存</param>
        /// <param name="strAllowType">允许的文件后缀，例如.jpg|.jpeg|.gif|.png|.bmp|.wbmp</param>
        /// <param name="iAllowSize">允许上传的文件大小</param>
        /// <param name="iWidth">如果是图片文件，则获取到的原图宽度</param>
        /// <param name="iHeight">如果是图片文件，则获取到的原图高度</param>
        /// <returns>Message:上传成功</returns>
        public static string UploadFile(HttpPostedFile postFile, string strFilePath, int SourceWidth, int ImgBWidth, int ImgCWidth, string strAllowType, int iAllowSize, out int iWidth, out int iHeight)
        {
            //通过 Stream 作为参数来保存图片 string strSaveingPath,int SourceWidth,int ImgBWidth, int ImgCWitdh,
            Stream sr = postFile.InputStream;
            string strFileType = (System.IO.Path.GetExtension(postFile.FileName)).ToString().ToLower();
            int iFileSize = postFile.ContentLength;

            string Path = "";		//  原图的保存路径
            string Ext_name = "";
            string imgExt = "";
            int isok = 0;			//  是否允许保存的文件类型
            string result = "";		//  返回的提示信息

            iWidth = 0;
            iHeight = 0;

            Path = strFilePath;     //  得到保存路径

            Ext_name = strAllowType;//  允许保存的文件类型
            imgExt = strFileType;

            //1.是否允许上传该文件类型
            if (Ext_name == "")
            {
                isok = 1;			//  允许上传文件
            }
            else
            {
                //进行判断后，进行保存操作
                string[] type;
                type = Ext_name.ToString().Split('|');    //    将允许保存的文件类型进行分解
                foreach (string i in type)
                {
                    if (i == strFileType)
                        isok = 1;
                }
            }

            if (isok == 0)
            {
                result = " 对不起,不能上传扩展名为(" + strFileType.ToString() + ")的文件";
                return result;
            }


            //2.是否允许上传本大小的文件--------------------
            if (iAllowSize < iFileSize)
            {
                result = " 对不起,上传的文件不能超过" + (iAllowSize / 1024).ToString() + "K";
                return result;
            }


            //	检查原路径是否存在
            try
            {
                string tempPath = Path.Substring(0, Path.LastIndexOf('\\'));
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }

            }
            catch (Exception ex)
            {
                result = ex.ToString();
                return result;
            }

            //3判断是否为图片文件
            if (imgExt == ".jpg" || imgExt == ".jpeg" || imgExt == ".gif" || imgExt == ".png" || imgExt == ".bmp" || imgExt == ".wbmp")
            {
                //允许保存，则进行相应的保存操作
                try
                {

                    System.Drawing.Image oriImg, newImg;
                    oriImg = System.Drawing.Image.FromStream(sr);
                    iWidth = oriImg.Width;
                    iHeight = oriImg.Height;

                    //	保存原图(原)
                    if (SourceWidth == 1)
                    {
                        oriImg.Save(Path);
                    }

                    // 保存A图(大)非原图的大图 控制图在某个范围大小内
                    if (SourceWidth > 1)
                    {
                        if (imgExt == ".gif")
                        {
                            oriImg.Save(Path);//保存原图
                        }
                        else
                        {
                            Path = Path.Substring(0, Path.LastIndexOf(".")) + ".jpg";
                            if (oriImg.Width < SourceWidth)
                                SourceWidth = oriImg.Width;
                            newImg = oriImg.GetThumbnailImage(SourceWidth, SourceWidth * oriImg.Height / oriImg.Width, null, new System.IntPtr(0));
                            newImg.Save(Path + "", System.Drawing.Imaging.ImageFormat.Jpeg);//保存缩略图	
                        }
                    }

                    //	保存B图(小)
                    if (ImgBWidth > 0)
                    {
                        string savingBPath = Path.Substring(0, Path.LastIndexOf(".")) + "b.jpg";
                        newImg = oriImg.GetThumbnailImage(ImgBWidth, ImgBWidth * oriImg.Height / oriImg.Width, null, new System.IntPtr(0));
                        newImg.Save(savingBPath + "", System.Drawing.Imaging.ImageFormat.Jpeg);//保存缩略图		
                    }

                    //	保存c图(中)
                    if (ImgCWidth > 0)
                    {
                        string savingCPath = Path.Substring(0, Path.LastIndexOf(".")) + "c.jpg";
                        newImg = oriImg.GetThumbnailImage(ImgCWidth, ImgCWidth * oriImg.Height / oriImg.Width, null, new System.IntPtr(0));
                        newImg.Save(savingCPath + "", System.Drawing.Imaging.ImageFormat.Jpeg);//保存缩略图		
                    }
                    result = "上传成功";
                }
                catch (Exception ex)
                {
                    result += "上传失败，错误：" + ex.Message + "";
                }
            }
            else
            {
                //上传其他类型的文件---------------------------------------------------------------------------------------------------
                int BuffSize = 1024 * 400;    //分块大小为 40KB  我测试过虽然了限制可以达500MB，不过只能上传 100MB多一点
                int iBlock = 0;

                try
                {
                    Stream fClientStream;
                    FileStream fWStream;
                    fClientStream = sr;                  //(Stream)postedFile.InputStream;
                    //if(File.Exists(Path)==false) 
                    fWStream = new FileStream(Path, FileMode.Create);//不可覆盖      

                    for (; iBlock < iFileSize / BuffSize; iBlock++)
                    {
                        byte[] bBuff = new byte[BuffSize];
                        fClientStream.Read(bBuff, 0, BuffSize);
                        MemoryStream memStream = new MemoryStream(bBuff);
                        memStream.WriteTo(fWStream);
                        memStream.Close();
                    }

                    byte[] bSubBuff = new byte[iFileSize % BuffSize];
                    fClientStream.Read(bSubBuff, 0, iFileSize % BuffSize);
                    MemoryStream SubStream = new MemoryStream(bSubBuff);
                    SubStream.WriteTo(fWStream);
                    SubStream.Close();
                    fWStream.Close();
                    fWStream = null;
                    fClientStream.Close();

                    result = "上传成功";	           //上传成功
                }
                catch (Exception ex)
                {
                    result = "上传失败，错误警告：" + ex.Message + "";        //上传失败
                }
                //------------------------------------------------------------------------------------------------------------------------------

            }
            return result;
        }
        #endregion 
        #endregion


        #region 上传文件2
        /// <summary>
        /// 上传文件
        /// </summary>
        /// <param name="postFile">发送的文件对象</param>
        /// <param name="savePath">保存到的路径</param>
        /// <param name="fileType">文件类型  eg: txt|ext|jpg </param>
        /// <param name="maxSize">文件最大值(KB)   -1 为没限制</param>
        /// <returns></returns>
        public static bool Upload(HttpPostedFile postFile,string savePath,string fileType,int maxSize)
        {
            //验证扩展名是否正确
            fileType = fileType.ToLower();
            string extName = GetExtensionName(postFile.FileName).ToLower();

            if (!string.IsNullOrEmpty(fileType) && fileType.IndexOf(extName) == -1) return false;

            //验证文件大小
            if (maxSize != -1 && postFile.ContentLength > maxSize * 1024) return false;

            //上传文件
            string path = savePath.Substring(0, savePath.LastIndexOf('\\'));

            try
            {
                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }
                postFile.SaveAs(savePath);
            }
            catch (Exception ex)
            {
#if DEBUG
                throw ex;
#endif
                return false;
            }
            return true;
        }
        #endregion 

        #region 操作相关方法
        /// <summary>
        /// 验证文件是否指定的类型
        /// </summary>
        /// <param name="fileName">文件名称</param>
        /// <param name="fileType">文件类型 格式“txt|doc|xls”</param>
        /// <returns></returns>
        public static bool ValidateFileType(string fileName,string fileType)
        {
            if (fileName == "" || fileType == "") return true;

            string exName = fileName.Substring(fileName.LastIndexOf('.') + 1);
            string[] sType = fileType.Split('|');

            for(int i = 0;i < sType.Length;i++)
            {
                if (sType[i].ToLower() == exName.ToLower()) return true;
            }

            return false;
        }

        /// <summary>
        /// 获取文件扩展名
        /// </summary>
        /// <param name="fileName"></param>
        /// <returns></returns>
        public static string GetExtensionName(string fileName)
        {
            return fileName.Substring(fileName.LastIndexOf('.') + 1);            
        }

        /// <summary>
        /// 获取文件名
        /// </summary>
        /// <param name="filePath">文件路径</param>
        /// <returns></returns>
        public static string GetFileName(string filePath)
        {
            return filePath.Substring(filePath.LastIndexOf("\\") + 1);
        }

        /// <summary>
        /// 判断文件是否存在，若不存在则创建并创建相关的文件夹
        /// </summary>
        /// <param name="filePath"></param>
        public static void CreateNotExistsFile(string filePath)
        {
            if (!File.Exists(filePath))
            {
                //上传文件
                string pathRoot = filePath.Substring(0, filePath.LastIndexOf('\\'));

                try
                {
                    //创建文件夹
                    if (!Directory.Exists(pathRoot))
                    {
                        Directory.CreateDirectory(pathRoot);
                    }
                    //创建文件
                    if (!File.Exists(filePath))
                    {
                        File.Create(filePath).Close();
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }
        #endregion 
    }        

}
