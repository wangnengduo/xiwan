﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace XiWan.DALFactory
{
    [Serializable]
    public enum e_ControllerType
    {
        Sql = 1,
        Oracle = 2
    }

    [Serializable]
    public enum e_EntityState
    {
        Added,
        Modified,
        Deleted,
        Unchanged,
        Invalid
    }

    public class EntitesPublic
    {
        public static DataSet GetEntityExt
        {
            get { return EntityExt(); }

        }
        private static DataSet EntityExt()
        {
            System.IO.Stream stream;
            DataSet dst = new DataSet();
            string strPath = "Entities.EntityExt.xml";
            stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strPath);
            dst.ReadXml(stream);
            return dst;
        }
    }

    public interface IPropertyAccessor
    {
        object GetValue(string column);
        void SetValue(string column, object value);
    }

    /// <summary>
    /// 用于执行信息的返回
    /// </summary>
    [Serializable]
    public class ReturnInfo
    {
        //
        public bool ExecuteTrue = false;
        /// <summary>
        /// 返回参数1
        /// </summary>
        public string Param1 { get; set; }

        /// <summary>
        /// 返回参数2
        /// </summary>
        public string Param2 { get; set; }

        /// <summary>
        /// 返回参数3
        /// </summary>
        public string Param3 { get; set; }

        /// <summary>
        /// 返回参数4
        /// </summary>
        public string Param4 { get; set; }
        /// <summary>
        /// 返回参数5
        /// </summary>
        public string Param5 { get; set; }

        /// <summary>
        /// 执行返回的消息或错误消息
        /// </summary>
        public string Msg { get; set; }
    }
}
