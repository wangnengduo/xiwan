﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 * 数据访问工厂类
 */
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace XiWan.DALFactory
{
    /// <summary>
    /// 数据访问工厂类。
    /// </summary>
    public class ControllerFactory
    {

        #region 初始化
        static ControllerFactory()
        {  
            // 设置事务隔离级别
            if (System.Configuration.ConfigurationManager.AppSettings["IsolationLevel"] != null)
            {
                switch (System.Configuration.ConfigurationManager.AppSettings["IsolationLevel"].ToString())
                {
                    case "ReadUncommitted":
                        m_tranIsolationLevel = System.Data.IsolationLevel.ReadUncommitted;
                        break;

                    case "ReadCommitted":
                        m_tranIsolationLevel = System.Data.IsolationLevel.ReadCommitted;
                        break;

                    case "RepeatableRead":
                        m_tranIsolationLevel = System.Data.IsolationLevel.RepeatableRead;
                        break;

                    case "Serializable":
                        m_tranIsolationLevel = System.Data.IsolationLevel.Serializable;
                        break;
                }
            }
        }
        #endregion

   
        private static e_ControllerType m_controllerType = e_ControllerType.Sql;
        private static IsolationLevel m_tranIsolationLevel = IsolationLevel.ReadUncommitted;
        private static Dictionary<int, ControllerBase> m_controllerList = new Dictionary<int, ControllerBase>();

        //private static string _ConnectionName = "Main";
        ///// <summary>
        ///// 设置访问的数据库链接名
        ///// </summary>
        //public static string ConnectionName
        //{
        //    get { return _ConnectionName; }
        //    set { _ConnectionName = value; }
        //}

        public static void SetConnection(ControllerBase controller,e_ConsType eType)
        {
             string ConnectionName = eType.ToString();

            // 读取配置信息：连接字符串
            if (System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionName] != null)
            {
                controller.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionName].ConnectionString;
                switch (System.Configuration.ConfigurationManager.ConnectionStrings[ConnectionName].ProviderName)
                {
                    case "System.Data.SqlClient":
                        m_controllerType = e_ControllerType.Sql;
                        EntityBase.ControllerType = e_ControllerType.Sql;
                        break;
                    case "System.Data.OracleClient":
                        m_controllerType = e_ControllerType.Oracle;
                        EntityBase.ControllerType = e_ControllerType.Oracle;
                        break;
                    default:
                        throw new Exception("连接字符串的providerName参数未设置或者不正确。");
                }
            }
            
        }
         
        /// <summary>
        /// 数据源的类型。SQL或者Oracle
        /// </summary>
        public static e_ControllerType ControllerType
        {
            get { return m_controllerType; }
            set { m_controllerType = value; }
        }

        /// <summary>
        /// 事务隔离级别。默认ReadUncommitted
        /// </summary>
        public static IsolationLevel TranIsolationLevel
        {
            get { return m_tranIsolationLevel; }
            set { m_tranIsolationLevel = value; }
        }
        
        /// <summary>
        /// 获取一个数据访问控制器实例。每次访问属性都将重新创建一个实例，适合只调用一次的操作，如果有多个操作，建议使用GetController()方法。
        /// </summary>
        public static ControllerBase Controller
        {
            get
            {
                return GetController();
            }
        }

        /// <summary>
        /// 获取一个数据访问控制器实例。
        /// </summary>
        /// <returns></returns>
        public static ControllerBase GetController()
        {
            // 读取配置信息：连接字符串     
            ControllerBase controller = GetController(m_controllerType);
            SetConnection(controller, e_ConsType.Main);
            return controller;

        }

        /// <summary>
        /// 获取一个数据访问控制器实例。
        /// </summary>
        /// <returns></returns>
        public static ControllerBase GetNewController(e_ConsType eType)
        {    
            ControllerBase controller = GetController(m_controllerType);
            SetConnection(controller, eType);
            return controller;
        }
        public static ControllerBase GetNewController(e_ConsType eType,bool blNew)
        {
            ControllerBase controller = GetController(m_controllerType,blNew);
            SetConnection(controller, eType);
            return controller;
        }

        /// <summary>
        /// 获取一个数据访问控制器实例。可以指定Oracle或Sql。
        /// </summary>
        /// <param name="controllerType"></param>
        /// <returns></returns>
        public static ControllerBase GetController(e_ControllerType controllerType)
        {           
            return GetController(controllerType, false);
        }

        public static ControllerBase GetController(e_ControllerType controllerType,bool blNew)
        {
            ControllerBase controller = null;

            // 先获取同一个进程的实例，没有则新建实例
            if (blNew == false)
            {
                int intProcessID = System.Diagnostics.Process.GetCurrentProcess().Id;
                if (m_controllerList.ContainsKey(intProcessID))
                {
                    controller = m_controllerList[intProcessID];
                }
            }
            if(controller==null)
            {
                if (controllerType == e_ControllerType.Oracle)
                {
                    controller = new OracleController();
                }
                else
                {
                    controller = new SqlController();
                }
                controller = new SqlController();
            }

            return controller;
        }

        /// <summary>
        /// 设置进程内共享的控制器实例。
        /// </summary>
        /// <param name="controller"></param>
        public static void SetControllerSessionShare(ControllerBase controller)
        {
            int intProcessID = Process.GetCurrentProcess().Id;
            if (m_controllerList.ContainsKey(intProcessID))
            {
                return;
            }

            StackTrace st=new StackTrace(true);            
            controller.ProcessMethodName = st.GetFrame(1).GetMethod().Name.ToString();
            controller.ProcessID = intProcessID;

            m_controllerList[intProcessID] = controller;
        }
    }

    /// <summary>
    /// 数据库链接名，直接调用该字符串做为链接名，请设置web.config时一致
    /// </summary>
    [Serializable]
    public enum e_ConsType
    {
        /// <summary>
        /// 主数据库链接名,默认的静态类使用的库
        /// </summary>
        Main = 1,
        /// <summary>
        /// 数据库链接2，需调用GetNewController 方法的参数
        /// </summary>
        Main2 = 2,
        /// <summary>
        /// 主数据库链接3，需调用GetNewController 方法的参数
        /// </summary>
        Main3 = 3,
        /// <summary>
        /// 主数据库链接4，需调用GetNewController 方法的参数
        /// </summary>
        Main4 = 4,
        /// <summary>
        /// 主数据库链接MobileAuth，需调用GetNewController 方法的参数
        /// </summary>
        MobileAuth = 5
    }
}
