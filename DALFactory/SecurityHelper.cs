﻿/*
 * 作者: Wong
 * 日期: 2018-6-13
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Security.Cryptography;

namespace XiWan.DALFactory
{
    /// <summary>
    /// 加密方式
    /// </summary>
    public enum EncryptMode
    {
        /// <summary>
        /// 不加密
        /// </summary>
        None = 0,
        /// <summary>
        /// 16位MD5 -- (常规)
        /// </summary>
        MD5_16 = 1,
        /// <summary>
        /// 16位MD5 --> 方式2
        /// </summary>
        MD5_16_2 = 2,
        /// <summary>
        /// 32位MD5
        /// </summary>
        MD5_32 = 3
    }

    /// <summary>
    /// 安全相关辅助类
    /// </summary>
    public class SecurityHelper
    {
        /// <summary>
        /// 
        /// </summary>
        public SecurityHelper()
        {

        }

        /// <summary>
        /// 加密
        /// </summary>
        /// <param name="sInput">字符串</param>
        /// <param name="encryptMode">加密方式</param>
        public static string Encrypt(string sInput, EncryptMode encryptMode)
        {
            if (sInput == null || sInput == "")
            {
                return "";
            }
            switch (encryptMode)
            {
                case EncryptMode.MD5_16:
                    sInput = SecurityHelper.EncryptToMD5(sInput, 16);
                    break;
                case EncryptMode.MD5_32:
                    sInput = SecurityHelper.EncryptToMD5(sInput, 32);
                    break;
                case EncryptMode.MD5_16_2:
                    sInput = SecurityHelper.EncryptToMD5(sInput, 7,16); //成绩系统使用的位数是 7 - 16
                    break;               
            }

            return sInput;
        }

        /// <summary>
        /// 加密URL参数值
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="sKey">密钥</param>
        public static string EncryptUrlParameter(string str, string sKey)
        {
            if (str == null || str == "")
            {
                return "";
            }
            str = Encrypt(str, sKey);
            str = str.Replace("+","%2B");  //把"+"号转换为URL可识别码，若不转换则会变为空格

            return str;
        }

        /// <summary>
        /// DES 变形解密
        /// </summary>
        /// <param name="sInput">字符串</param>
        /// <param name="sKey">密钥</param>
        public static string Decrypt(string sInput, string sKey)
        {
            if (sInput==null||sInput == "")
            {
                return "";
            }
            ICryptoTransform decrypter;

            System.Security.Cryptography.TripleDES des = System.Security.Cryptography.TripleDESCryptoServiceProvider.Create();
            PasswordDeriveBytes pd = new PasswordDeriveBytes(sKey, null);

            des.Key = pd.GetBytes(24); 
            des.IV = pd.GetBytes(8);
            des.Mode = CipherMode.ECB;
            des.Padding = PaddingMode.PKCS7;
            decrypter = des.CreateDecryptor();


            byte[] input = StringHelper.ToByte(sInput);
            byte[] output = decrypter.TransformFinalBlock(input, 0, input.Length);

            return StringHelper.NormalToStr(output);
        }

        /// <summary>
        /// DES 变形加密
        /// </summary>
        /// <param name="sInput">字符串</param>
        /// <param name="sKey">密钥</param>
        public static string Encrypt(string sInput, string sKey)
        {
            if (sInput == null || sInput == "")
            {
                return "";
            }
            ICryptoTransform encrypter;

            System.Security.Cryptography.TripleDES des = System.Security.Cryptography.TripleDESCryptoServiceProvider.Create();
            PasswordDeriveBytes pd = new PasswordDeriveBytes(sKey, null);

            des.Key = pd.GetBytes(24);
            des.IV = pd.GetBytes(8);
            des.Mode = CipherMode.ECB;
            des.Padding = PaddingMode.PKCS7;
            encrypter = des.CreateEncryptor();

            byte[] input = StringHelper.NormalToByte(sInput);
            byte[] output = encrypter.TransformFinalBlock(input, 0, input.Length);
            return StringHelper.ToStr(output);
        }

        /// <summary>
        /// 使用32位MD5加密
        /// </summary>
        /// <param name="str">待加密字符串</param>
        /// <returns></returns>
        public static string EncryptToMD5(string str)
        {
            return EncryptToMD5(str, 32);
        }

        /// <summary>
        /// 使用MD5加密
        /// </summary>
        /// <param name="str">待加密字符串</param>
        /// <param name="code">加密位数  16 位 32 位</param>
        /// <returns></returns>
        public static string EncryptToMD5(string str, int code)
        {
            if (code == 32)
                return EncryptToMD5(str, 0, 32);
            else
                return EncryptToMD5(str, 8, 16); //默认8-16位
        }

        /// <summary>
        /// 使用MD5加密
        /// </summary>
        /// <param name="str">待加密字符串</param>
        /// <param name="startIndex">截取的开始位置</param>
        /// <param name="endIndex">截取的结束</param>
        /// <returns></returns>
        private static string EncryptToMD5(string str,int startIndex,int endIndex)
        {
            if (str == null || str == "")
            {
                return "";
            }
            string sEncrypt = "";

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(System.Text.Encoding.Default.GetBytes(str));

            for (int i = 0; i < result.Length; i++)
            {
                sEncrypt += string.Format("{0:x}", result[i]).PadLeft(2,'0');
            }

            //16位MD5加密（取32位加密的9~25字符）
            return sEncrypt.ToLower().Substring(startIndex, endIndex);
        }



        #region 转换成 MD5 值
        /// <summary>
        /// 转换成 MD5 值
        /// </summary>
        /// <param name="Sourcein">要转换的 MD5 值的字符串</param>
        /// <returns>返回 MD5 值</returns>
        public static string GetMd5(string Sourcein)
        {
            MD5CryptoServiceProvider MD5CSP = new MD5CryptoServiceProvider();
            byte[] MD5Source = System.Text.Encoding.UTF8.GetBytes(Sourcein);
            byte[] MD5Out = MD5CSP.ComputeHash(MD5Source);
            return System.Convert.ToBase64String(MD5Out);
        }
        #endregion

        #region MD5加密解密
        //加密
        public static string MD5Encrypt(string pToEncrypt, string sKey)
        {
            try
            {
                System.Security.Cryptography.DESCryptoServiceProvider des = new System.Security.Cryptography.DESCryptoServiceProvider();
                byte[] inputByteArray = System.Text.Encoding.Default.GetBytes(pToEncrypt);

                des.Key = System.Text.ASCIIEncoding.ASCII.GetBytes(sKey);
                des.IV = System.Text.ASCIIEncoding.ASCII.GetBytes(sKey);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                System.Security.Cryptography.CryptoStream cs = new System.Security.Cryptography.CryptoStream(ms, des.CreateEncryptor(), System.Security.Cryptography.CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();
                System.Text.StringBuilder ret = new System.Text.StringBuilder();
                foreach (byte b in ms.ToArray())
                {
                    ret.AppendFormat("{0:X2}", b);
                }

                return ret.ToString();
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        //解密
        public static string MD5Decrypt(string pToDecrypt, string sKey)
        {
            try
            {
                System.Security.Cryptography.DESCryptoServiceProvider des = new System.Security.Cryptography.DESCryptoServiceProvider();
                byte[] inputByteArray = new byte[pToDecrypt.Length / 2];
                for (int x = 0; x < pToDecrypt.Length / 2; x++)
                {
                    int i = (System.Convert.ToInt32(pToDecrypt.Substring(x * 2, 2), 16));
                    inputByteArray[x] = (byte)i;
                }

                des.Key = System.Text.ASCIIEncoding.ASCII.GetBytes(sKey);
                des.IV = System.Text.ASCIIEncoding.ASCII.GetBytes(sKey);
                System.IO.MemoryStream ms = new System.IO.MemoryStream();
                System.Security.Cryptography.CryptoStream cs = new System.Security.Cryptography.CryptoStream(ms, des.CreateDecryptor(), System.Security.Cryptography.CryptoStreamMode.Write);
                cs.Write(inputByteArray, 0, inputByteArray.Length);
                cs.FlushFinalBlock();

                System.Text.StringBuilder ret = new System.Text.StringBuilder();

                return System.Text.Encoding.Default.GetString(ms.ToArray());
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }


        #endregion

        #region  常规 DES加/解密
        /// <summary>
        /// 常规 DES加密
        /// </summary>
        /// <param name="code"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DESCommonEncrypt(string code, string key)
        {
            DESEncryptor des = new DESEncryptor();
            des.EncryptKey = key;
            des.InputString = code;
            des.DesEncrypt();
            return des.OutString;
        }
 
        /// <summary>
        /// 常规 DES解密
        /// </summary>
        /// <param name="code"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string DESCommonDecrypt(string code, string key)
        {
            DESEncryptor des = new DESEncryptor();
            des.DecryptKey = key;
            des.InputString = code;
            des.DesDecrypt();
            return des.OutString;
        }
        #endregion 

    }
}
