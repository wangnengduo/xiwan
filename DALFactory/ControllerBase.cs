﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Data.OleDb;
using System.Diagnostics;

namespace XiWan.DALFactory
{
    /// <summary>
    /// 数据访问基类。
    /// </summary>
    public class ControllerBase
    {
        #region 构造函数
        protected ControllerBase()
        {
        }
        #endregion

        #region 变量
        protected e_ControllerType m_controllerType = e_ControllerType.Sql;
        protected IsolationLevel m_tranIsolationLevel = IsolationLevel.ReadUncommitted;
        protected Dictionary<EntityBase, EntityModifyInfo> m_entityList = null;
                
        // SQL关键字
        protected string m_strFlagComma = "*";
        protected string m_strFlagParameter = "@";
        protected string m_strFlagEqual = "=";
        protected string m_strFlagLike = " like ";
        protected string m_strFlagAll = "*";
        protected string m_strFlagAnd = " AND ";
        protected string m_strParaWhere = "_Where";
        protected string m_strParaOriginal = "_Original";
        protected string m_strParaSelect = "_Select";
        protected string m_strParaInsert = "_Insert";
        protected string m_strParaUpdate = "_Update";
        protected string m_strParaDelete = "_Delete";

        protected string m_strSqlInsert = "INSERT INTO {0}({1}) VALUES({2})";
        protected string m_strSqlDelete = "DELETE FROM {0} WHERE {1}";
        protected string m_strSqlUpdate = "UPDATE {0} SET {1} WHERE {2}";
        protected string m_strSqlSelect = "SELECT a.{0} FROM {1} a WITH(NOLOCK) {2} WHERE {3}";

        private string m_strConnectionString = "";

        // SQL缓存
        protected static Dictionary<string, string> m_sqlList = new Dictionary<string, string>();
        #endregion

        #region 属性
        /// <summary>
        /// 数据源的类型。SQL或者Oracle
        /// </summary>
        protected internal e_ControllerType ControllerType
        {
            get { return m_controllerType;}
            set { m_controllerType = value; }
        }

        /// <summary>
        /// 事务隔离级别。默认ReadUncommitted
        /// </summary>
        public IsolationLevel TranIsolationLevel
        {
            get { return m_tranIsolationLevel; }
            set { m_tranIsolationLevel = value; }
        }

        /// <summary>
        /// 连接字符串。
        /// </summary>
        public string ConnectionString
        {
            get
            {
                return m_strConnectionString;
            }
            set
            {
                m_strConnectionString = value;
            }
        }
      

        private DataTable EntityExt
        {
            get
            {
                return EntitesPublic.GetEntityExt.Tables[0];
            }
        }
        #endregion

        #region 缓存管理
        /// <summary>
        /// SQL语句缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns> 
        private string GetCachedSql(string key)
        {
            if (m_sqlList.ContainsKey(key))
            {
                return m_sqlList[key];
            }
            else
            {
                return null;
            }
        }
        #endregion

        #region 连接管理
        protected IDbConnection m_connection = null;
        protected int m_intCommandTimeout = 30;

        /// <summary>
        /// 设置连接字符串。
        /// </summary>
        /// <param name="cnnSttring"></param>
        public void SetConnectionString(string cnnSttring)
        {
            m_strConnectionString = cnnSttring;
        }
        public string StaticConnectionString
        {
            get { return m_strConnectionString; }
            set { m_strConnectionString = value; }
        }

        public int CommandTimeout
        {
            get { return m_intCommandTimeout; }
            set { m_intCommandTimeout = value; }
        }

        protected virtual IDbConnection NewConnection()
        {
            throw new Exception("NewConnection");
        }

        protected IDbConnection CreateConnection()
        {
            if (m_blnIsTranExecuting)
            {
                if (m_connection != null && m_connection.State != ConnectionState.Open)
                {
                    m_connection.Open();
                }
                return m_connection;
            }
            else
            {
                if (m_strConnectionString.Length == 0)
                {
                    throw new Exception("未设置连接字符串");
                }
                return NewConnection();
            }
        }

        protected void OpenConnection(IDbConnection cnn)
        {
            if (!m_blnIsTranExecuting)
            {
                if (cnn != null && cnn.State != ConnectionState.Open)
                {
                    cnn.Open();
                }
            }
        }

        protected void CloseConnection(IDbConnection cnn)
        {
            if (!m_blnIsTranExecuting)
            {
                if (cnn != null && cnn.State != ConnectionState.Closed)
                {
                    cnn.Close();
                    cnn = null;
                }
            }
        }
        #endregion

        #region 事务管理
        protected IDbTransaction m_transaction = null;
        protected bool m_blnIsTranExecuting = false;
        protected internal int ProcessID { get; set; }
        protected internal string ProcessMethodName { get; set; }

        protected bool IsTranExecuting
        {
            get
            {
                return m_blnIsTranExecuting;
            }
        }

        private void ClearTran()
        {
            if (m_transaction != null)
            {
                m_transaction.Dispose();
                m_transaction = null;
            }
            if (m_connection != null)
            {
                if (m_connection.State != ConnectionState.Closed)
                {
                    m_connection.Close();
                }

                m_connection = null;
            }
        }

        ///// <summary>
        ///// 开始事务。
        ///// </summary>
        //public void BeginTran()
        //{
        //    BeginTran(this.TranIsolationLevel);
        //}

        ///// <summary>
        ///// 开始事务，并指定隔离级别。
        ///// </summary>
        ///// <param name="level"></param>
        //public void BeginTran(IsolationLevel level)
        //{
        //    m_entityList = new Dictionary<EntityBase, EntityModifyInfo>();

        //    try
        //    {
        //        ClearTran();
        //        m_blnIsTranExecuting = true;

        //        m_connection = NewConnection();
        //        m_connection.Open();
        //        m_transaction = m_connection.BeginTransaction(level);
        //    }
        //    catch (Exception ex)
        //    {
        //        throw (new Exception("BeginTran", ex));
        //    }
        //}

        /// <summary>
        /// 开始事务。
        /// </summary>
        public void BeginTran()
        {
            if (m_blnIsTranExecuting) return;
            
            m_entityList = new Dictionary<EntityBase, EntityModifyInfo>();

            try
            {
                ClearTran();
                m_blnIsTranExecuting = true;

                m_connection = NewConnection();
                m_connection.Open();
                m_transaction = m_connection.BeginTransaction(m_tranIsolationLevel);
            }
            catch (Exception ex)
            {
                throw (new Exception("BeginTran", ex));
            }
        }

        /// <summary>
        /// 提交事务。
        /// </summary>
        /// <returns></returns>
        public bool CommitTran()
        {
            // 在共享控制器进程中，只能由设置共享的方法来提交事务。
            if (this.ProcessID > 0)
            {
                StackTrace st = new StackTrace(true);
                if (this.ProcessMethodName != st.GetFrame(1).GetMethod().Name.ToString())
                {
                    return true;
                }
            }

            if (m_transaction == null)
            {
                throw (new Exception("transaction object is null"));
            }

            bool blnIsCompleted = false;

            try
            {
                m_blnIsTranExecuting = false;
                m_transaction.Commit();
                blnIsCompleted = true;
            }
            catch (Exception ex)
            {
                throw (new Exception("CommitTran", ex));
            }
            finally
            {
                try
                {
                    ClearTran();
                }
                catch (Exception ex)
                {
                    throw new Exception("CommitTran", ex);
                }
            }

            // 提交成功时清除记录的实体状态。
            if (blnIsCompleted)
            {
                m_entityList.Clear();
            }

            return blnIsCompleted;
        }

        /// <summary>
        /// 回滚事务。
        /// </summary>
        public void RollbackTran()
        {
            // 在共享控制器进程中，只能由设置共享的方法来回滚事务。
            if (this.ProcessID > 0)
            {
                StackTrace st = new StackTrace(true);
                if (this.ProcessMethodName != st.GetFrame(1).GetMethod().Name.ToString())
                {
                    return;
                }
            }

            if (m_transaction == null)
            {
                throw (new Exception("transaction object is null"));
            }

            try
            {
                m_transaction.Rollback();
                m_blnIsTranExecuting = false;
            }
            catch (Exception ex)
            {
                throw (new Exception("RollbackTran", ex));
            }
            finally
            {
                try
                {
                    ClearTran();
                }
                catch (Exception ex)
                {
                    throw new Exception("RollbackTran", ex);
                }
            }

            // 回滚成功时恢复实体状态。
            if (!m_blnIsTranExecuting)
            {
                foreach (KeyValuePair<EntityBase, EntityModifyInfo> item in m_entityList)
                {
                    item.Key.EntityState = item.Value.EntityState;
                    item.Key.ModifiedList = item.Value.ModifiedList;
                    //item.Key.RejectChanges();
                }                
            }
        }
        #endregion

        #region 数据访问
        /// <summary>
        /// 创建Command对象。
        /// </summary>
        /// <param name="cnn"></param>
        /// <returns></returns>
        protected IDbCommand CreateCommand(IDbConnection cnn)
        {
            IDbCommand cmd = null;

            if (m_blnIsTranExecuting)
            {
                cmd = m_connection.CreateCommand();
                cmd.Transaction = m_transaction;
            }
            else
            {
                cmd = cnn.CreateCommand();
            }

            return cmd;
        }

        /// <summary>
        /// 初始化Command对象。
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="cmdType"></param>
        /// <param name="cmdText"></param>
        /// <param name="parameters"></param>
        protected virtual void InitializeCommand(IDbCommand cmd, CommandType cmdType, string cmdText, IDataParameter[] parameters)
        {
            if (cmd == null) throw new Exception("command object is null");

            cmd.CommandType = cmdType;
            cmd.CommandText = cmdText;

            if ((parameters != null) && (parameters.Length > 0))
            {
                foreach (IDataParameter prm in parameters)
                {
                    cmd.Parameters.Add(prm);
                }
            }
        }

        // 连接SQL
        private static string BuildSql(string columns, string tableName, string condition)
        {
            string strSql = "SELECT {0} FROM {1} WITH(NOLOCK) {2}";
            string strCondition = "";

            if (condition.Length > 0)
            {
                if (condition.ToLower().IndexOf("where") > 0 && condition.ToLower().IndexOf("where") < 10)
                {
                    strCondition = condition;
                }
                else
                {
                    strCondition = " WHERE " + condition;
                }
            }

            return string.Format(strSql, columns, tableName, strCondition);
        }

        /// <summary>
        /// 获取数据集。
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public DataSet GetDataSet(string commandText)
        {
            DataSet dst = new DataSet();
            FillDataSet(commandText, dst);
            return dst;
        }

        /// <summary>
        /// 根据表名，获取数据表。
        /// </summary>
        /// <param name="tableName"></param>
        /// <returns></returns>
        public DataTable GetDataTableByName(string tableName)
        {
            return GetDataTableByCondition(tableName, "", m_strFlagAll);
        }

        /// <summary>
        /// 根据表名，获取数据表（可指定返回哪些列）。
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        public DataTable GetDataTableByName(string tableName, string fieldList)
        {
            return GetDataTableByCondition(tableName, "", fieldList);
        }

        /// <summary>
        /// 根据表名，获取数据表。
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public DataTable GetDataTableByCondition(string tableName, string condition)
        {
            return GetDataTableByCondition(tableName, condition, m_strFlagAll);
        }

        /// <summary>
        /// 根据表名，获取数据表（可指定返回哪些列）。
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="condition"></param>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        public DataTable GetDataTableByCondition(string tableName, string condition, string fieldList)
        {
            string strFieldList = m_strFlagAll;
            if (fieldList != null && fieldList.Length > 0)
            {
                strFieldList = fieldList;
            }

            string strCommandText = BuildSql(strFieldList, tableName, condition);
            return GetDataTable(strCommandText);
        }
        
        /// <summary>
        /// 获取数据表。
        /// </summary>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public DataTable GetDataTable(string commandText)
        {
            DataTable dtb = new DataTable();
            FillDataTable(commandText, "", dtb);
            return dtb;
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="dtb"></param>
        /// <returns></returns>
        public int FillDataTable(string text, DataTable dtb)
        {
            return FillDataTable(text, "", dtb, false);
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="condition"></param>
        /// <param name="dtb"></param>
        /// <returns></returns>
        public int FillDataTable(string text, string condition, DataTable dtb)
        {
            return FillDataTable(text, condition, dtb, false);
        }

        /// <summary>
        /// 填充数据表。可选填充数据表的结构信息。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="condition"></param>
        /// <param name="dtb"></param>
        /// <param name="withSchema"></param>
        /// <returns></returns>
        public virtual int FillDataTable(string text, string condition, DataTable dtb, bool withSchema)
        {
            throw new Exception("FillDataTable");
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <param name="dtb"></param>
        /// <returns></returns>
        public int FillDataTable(string text, IDataParameter[] paras, DataTable dtb)
        {
            return FillDataTable(text, paras, dtb, false);
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <param name="dtb"></param>
        /// <param name="withSchema"></param>
        /// <returns></returns>
        public virtual int FillDataTable(string text, IDataParameter[] paras, DataTable dtb, bool withSchema)
        {
            throw new Exception("FillDataTable");
        }

        /// <summary>
        /// 填充数据表。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="paras"></param>
        /// <param name="dtb"></param>
        /// <returns></returns>
        public virtual int FillDataTableSP(string spName, IDataParameter[] paras, DataTable dtb)
        {
            throw new Exception("FillDataTableSP");
        }

        /// <summary>
        /// 填充数据集。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="paras"></param>
        /// <param name="dst"></param>
        /// <returns></returns>
        public virtual int FillDataSetSP(string spName, IDataParameter[] paras, DataSet dst)
        {
            throw new Exception("FillDataSetSP");
        }

        /// <summary>
        /// 填充数据集。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="dst"></param>
        /// <returns></returns>
        public virtual int FillDataSet(string text, DataSet dst)
        {
            throw new Exception("FillDataSetSP");
        }

        /// <summary>
        /// 获取数据行。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public DataRow GetDataRow(string text)
        {
            DataTable dtb = new DataTable();

            FillDataTable(text, dtb);

            if (dtb.Rows.Count > 0)
            {
                return dtb.Rows[0];
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 填充数据行。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool FillDataRow(string text, DataRow row)
        {
            return FillDataRow(text, null, row);
        }

        /// <summary>
        /// 填充数据行。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool FillDataRow(string text, IDataParameter[] paras, DataRow row)
        {
            IDbConnection cnn = CreateConnection();
            IDbCommand cmd = CreateCommand(cnn);
            IDataReader dr = null;
            int intIndex = 0;

            try
            {
                InitializeCommand(cmd, CommandType.Text, text, paras);

                OpenConnection(cnn);

                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                if (dr.Read())
                {
                    if (dr.FieldCount == row.Table.Columns.Count)
                    {
                        for (intIndex = 0; intIndex <= dr.FieldCount - 1; intIndex++)
                        {
                            row[intIndex] = dr[intIndex];
                        }
                    }
                    else
                    {
                        for (intIndex = 0; intIndex <= dr.FieldCount - 1; intIndex++)
                        {
                            row[dr.GetName(intIndex)] = dr[intIndex];
                        }
                    }
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[" + text + "]", ex);
            }
            finally
            {
                dr.Close();
                cmd.Dispose();
                CloseConnection(cnn);
            }
        }       

        /// <summary>
        ///  执行SQL，返回受影响的记录数。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string text)
        {
            return ExecuteNonQuery(text, null);
        }

        /// <summary>
        ///  执行SQL，返回受影响的记录数。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public int ExecuteNonQuery(string text, IDataParameter[] paras)
        {
            IDbConnection cnn = CreateConnection();
            IDbCommand cmd = CreateCommand(cnn);
            int intReturn = 0;

            try
            {
                InitializeCommand(cmd, CommandType.Text, text, paras);

                OpenConnection(cnn);

                intReturn = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[" + text + "]", ex);
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return intReturn;
        }

        /// <summary>
        /// 执行SQL，返回标量值。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public object ExecuteScalar(string text)
        {
            return ExecuteScalar(text, "");
        }

        /// <summary>
        /// 执行SQL，返回标量值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public object ExecuteScalar(string text, string condition)
        {
            IDbConnection cnn = CreateConnection();
            IDbCommand cmd = CreateCommand(cnn);
            string strSql = "";
            object objReturn = null;

            if ((condition != null) && (condition.Length > 0))
            {
                strSql = text + " where " + condition;
            }
            else
            {
                strSql = text;
            }

            try
            {
                InitializeCommand(cmd, CommandType.Text, strSql, null);

                OpenConnection(cnn);

                objReturn = cmd.ExecuteScalar();
                cmd.Parameters.Clear();
                return objReturn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[" + text + "]", ex);
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(cnn);
            }
        }

        /// <summary>
        /// 执行SQL，返回标量值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public object ExecuteScalar(string text, IDataParameter[] paras)
        {
            IDbConnection cnn = CreateConnection();
            IDbCommand cmd = CreateCommand(cnn);
            object objReturn = null;

            try
            {
                InitializeCommand(cmd, CommandType.Text, text, paras);

                OpenConnection(cnn);

                objReturn = cmd.ExecuteScalar();
                cmd.Parameters.Clear();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + "[" + text + "]", ex);
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return objReturn;
        }

        /// <summary>
        /// 返回String值。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string GetString(string text)
        {
            return GetString(text, null);
        }

        /// <summary>
        /// 返回String值，如果执行结果为空或Null，返回对应数据类型的默认值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public string GetString(string text, IDataParameter[] paras)
        {
            string strValue = GetStringNull(text, paras);
            if (strValue == null) strValue = "";
            return strValue;
        }

        /// <summary>
        /// 返回String值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public string GetStringNull(string text, IDataParameter[] paras)
        {
            object objValue = ExecuteScalar(text, paras);
            if (objValue != null && Convert.IsDBNull(objValue) == false)
            {
                return objValue.ToString();
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 返回Int值。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public int GetInt(string text)
        {
            return GetInt(text, null);
        }

        /// <summary>
        /// 返回Int值，如果执行结果为空或Null，返回对应数据类型的默认值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public int GetInt(string text, IDataParameter[] paras)
        {
            int intValue = 0;
            object objValue = ExecuteScalar(text, paras);
            if (objValue != null && Convert.IsDBNull(objValue) == false)
            {
                intValue = int.Parse(objValue.ToString());
            }
            return intValue;
        }

        /// <summary>
        /// 返回Long值。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public long GetLong(string text)
        {
            return GetLong(text, null);
        }

        /// <summary>
        /// 返回Long值，如果执行结果为空或Null，返回对应数据类型的默认值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public long GetLong(string text, IDataParameter[] paras)
        {
            long lngValue = 0;
            object objValue = ExecuteScalar(text, paras);
            if (objValue != null && Convert.IsDBNull(objValue) == false)
            {
                lngValue = long.Parse(objValue.ToString());
            }
            return lngValue;
        }

        /// <summary>
        /// 返回Decimal值。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public decimal GetDecimal(string text)
        {
            return GetDecimal(text, null);
        }

        /// <summary>
        /// 返回Decimal值，如果执行结果为空或Null，返回对应数据类型的默认值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public decimal GetDecimal(string text, IDataParameter[] paras)
        {
            decimal decValue = 0.0m;
            object objValue = ExecuteScalar(text, paras);
            if (objValue != null && Convert.IsDBNull(objValue) == false)
            {
                decValue = decimal.Parse(objValue.ToString());
            }
            return decValue;
        }

        /// <summary>
        /// 返回Bool值。
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public bool GetBool(string text)
        {
            return GetBool(text, null);
        }

        /// <summary>
        /// 返回Bool值，如果执行结果为空或Null，返回对应数据类型的默认值。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public bool GetBool(string text, IDataParameter[] paras)
        {
            bool blnValue = false;
            object objValue = ExecuteScalar(text, paras);
            if (objValue != null && Convert.IsDBNull(objValue) == false)
            {
                if (objValue.ToString() == "1" || objValue.ToString().ToLower() == "true")
                {
                    blnValue = true;
                }
                else
                {
                    blnValue = false;
                }
            }
            return blnValue;
        }

        /// <summary>
        /// 获取返回值。
        /// </summary>
        /// <returns></returns>
        protected virtual IDataParameter GetReturnValueParameter()
        {
            return null;
        }

        /// <summary>
        /// 执行存储过程，获取Return返回的数值。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public int ExecuteSPReturnValue(string spName, IDataParameter para)
        {
            return ExecuteSPReturnValue(spName, new IDataParameter[] { para });
        }

        /// <summary>
        /// 执行存储过程，获取Return返回的数值。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="para1"></param>
        /// <param name="para2"></param>
        /// <returns></returns>
        public int ExecuteSPReturnValue(string spName, IDataParameter para1, IDataParameter para2)
        {
            return ExecuteSPReturnValue(spName, new IDataParameter[] { para1, para2 });
        }

        /// <summary>
        /// 执行存储过程，获取Return返回的数值。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public int ExecuteSPReturnValue(string spName, IDataParameter[] paras)
        {
            // 增加返回参数				
            IDataParameter[] parasNew = new IDataParameter[paras.Length + 1];
            paras.CopyTo(parasNew, 1);
            parasNew[0] = GetReturnValueParameter();

            int intReturnValue = -1;
            ExecuteSP(spName, parasNew, ref intReturnValue);
            return intReturnValue;
        }

        /// <summary>
        /// 执行存储过程，返回标量值。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="para"></param>
        /// <returns></returns>
        public object ExecuteSP(string spName, IDataParameter para)
        {
            return ExecuteSP(spName, new IDataParameter[] { para });
        }

        /// <summary>
        /// 执行存储过程，返回标量值。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="para1"></param>
        /// <param name="para2"></param>
        /// <returns></returns>
        public object ExecuteSP(string spName, IDataParameter para1, IDataParameter para2)
        {
            return ExecuteSP(spName, new IDataParameter[] { para1, para2 });
        }

        /// <summary>
        /// 执行存储过程，返回标量值。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="paras"></param>
        /// <returns></returns>
        public object ExecuteSP(string spName, IDataParameter[] paras)
        {
            int intReturnValue = -1;
            return ExecuteSP(spName, paras, ref intReturnValue);
        }

        /// <summary>
        /// 执行存储过程，返回标量值。
        /// </summary>
        /// <param name="spName"></param>
        /// <param name="paras"></param>
        /// <param name="RETURN_VALUE"></param>
        /// <returns></returns>
        public object ExecuteSP(string spName, IDataParameter[] paras, ref int RETURN_VALUE)
        {
            IDbConnection cnn = CreateConnection();
            IDbCommand cmd = CreateCommand(cnn);
            object objReturn = null;

            try
            {
                InitializeCommand(cmd, CommandType.StoredProcedure, spName, paras);
                OpenConnection(cnn);
                cmd.ExecuteNonQuery();

                foreach (IDataParameter prm in cmd.Parameters)
                {
                    if (prm.Direction == ParameterDirection.Output)
                    {
                        objReturn = prm.Value;
                    }
                    if (prm.Direction == ParameterDirection.ReturnValue)
                    {
                        RETURN_VALUE = int.Parse(prm.Value.ToString());
                    }
                }

                cmd.Parameters.Clear();
            }
            finally
            {
                cmd.Dispose();
                CloseConnection(cnn);
            }

            return objReturn;
        }
        #endregion

        #region 实体相关的SQL语句
        // 生成获取实体的Select
        private string BuildSqlSelect(EntityBase entity)
        {
            return BuildSqlSelect(entity.Type.Name, entity.ColumnsName, entity.ExtendedList);
        }

        // 生成获取实体的Select
        private string BuildSqlSelect(Type entityType)
        {
            FieldInfo fieldColumnsName = entityType.GetField("m_strEntityColumnsName", BindingFlags.Static | BindingFlags.NonPublic);
            string strColumnsName = fieldColumnsName.GetValue(null) as string;

            FieldInfo fieldExtended = entityType.GetField("m_listEntityExtended", BindingFlags.Static | BindingFlags.NonPublic);
            Hashtable extendedList = fieldExtended.GetValue(null) as Hashtable;

            return BuildSqlSelect(entityType.Name, strColumnsName, extendedList);
        }

        // 生成获取实体的Select
        private string BuildSqlSelect(string table, string columns, Hashtable extendedList)
        {
            // 根据实体的扩展属性定义，生成获取数据的SQL语句
            string strExtColumns = "";
            string strJoin = "";
            GetEntityExtendedSql(table, extendedList, out strExtColumns, out strJoin);

            string strSql = string.Format(m_strSqlSelect, columns.Replace(",", ",a.") + strExtColumns, table, strJoin, "{0}");
            m_sqlList.Add(table + m_strParaSelect, strSql);

            return strSql;
        }

        // 获取实体的扩展属性相关的SQL
        private void GetEntityExtendedSql(string table, Hashtable extendedList, out string extColumns, out string join)
        {
            ArrayList listJoin = new ArrayList();
            DataRow[] arrExtFields = this.EntityExt.Select("TableName='*' or TableName='" + table + "'");
            string strExtColumns = "";
            string strJoin = "";

            if (arrExtFields.Length > 0 && extendedList != null)
            {
                string strColumnName = "";
                string strExtColumnName = "";
                string strExtColumnDesc = "";
                string strRelationTable = "";
                string strRelationTableAlias = "";
                string strRelationColumn = "";
                string strRelationOutput = "";
                int intJoinIndex = 0;

                foreach (DataRow drwExtField in arrExtFields)
                {
                    strColumnName = drwExtField["ColumnName"].ToString();
                    strExtColumnName = drwExtField["ExtColumnName"].ToString();
                    strExtColumnDesc = drwExtField["ExtColumnDesc"].ToString();
                    strRelationTable = drwExtField["RelationTable"].ToString();

                    if (this.EntityExt.Columns.Contains("RelationTableAlias"))
                    {
                        strRelationTableAlias = drwExtField["RelationTableAlias"].ToString();
                    }
                    else
                    {
                        strRelationTableAlias = "";
                    }

                    strRelationColumn = drwExtField["RelationColumn"].ToString();
                    strRelationOutput = drwExtField["RelationOutput"].ToString();

                    if (!extendedList.Contains(strExtColumnName))
                    {
                        continue;
                    }

                    switch (drwExtField["Type"].ToString())
                    {
                        case "2":   // 表达式
                            if (strRelationOutput.IndexOf("{") >= 0)
                            {
                                strExtColumns += "," + strExtColumnName + "=" + string.Format(strRelationOutput, ("a." + strColumnName.Replace(",", ",a.")).Split(','));
                            }
                            else
                            {
                                strExtColumns += "," + strExtColumnName + "=" + strRelationOutput;
                            }
                            break;

                        case "1":   // 表关联                            
                            if (strRelationTableAlias.Length == 0)
                            {
                                intJoinIndex += 1;
                                strRelationTableAlias = "t" + intJoinIndex.ToString();
                            }

                            if (!listJoin.Contains(strRelationTableAlias))
                            {
                                listJoin.Add(strRelationTableAlias);
                                strJoin += " left join " + strRelationTable + " " + strRelationTableAlias + " on " + strRelationTableAlias + "." + strRelationColumn + "=a." + strColumnName;
                            }

                            if (strRelationOutput.IndexOf("{") >= 0)
                            {
                                strExtColumns += "," + strExtColumnName + "=" + string.Format(strRelationOutput, "a." + strColumnName);
                            }
                            else
                            {
                                strExtColumns += "," + strExtColumnName + "=" + strRelationTableAlias + "." + strRelationOutput;
                            }
                            break;

                        case "0":   // 不处理       
                            strExtColumns += "," + strExtColumnName + "=null";
                            break;
                    }
                }
            }

            extColumns = strExtColumns;
            join = strJoin;
        }
        #endregion

        #region 实体操作(增删改查)
        /// <summary>
        /// 检查实体完整性。
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        protected bool Check(EntityBase entity)
        {
            bool blnResult = true;

            if (entity.EntityState == e_EntityState.Invalid)
            {
                throw new Exception("Check e_EntityState.Invalid");
            }

            return blnResult;
        }

        /// <summary>
        /// 保存实体。根据EntityState判断采用Insert或Update方法。
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public virtual bool Save(EntityBase entity)
        {
            if (!Check(entity))
            {
                return false;
            }
            if (!entity.HasChange)
            {
                return true;
            }

            bool blnIsCompleted = false;
            
            // 在多表更新的情况下，回滚事务后，EntityState没有回滚
            switch (entity.EntityState)
            {
                case e_EntityState.Added:
                    blnIsCompleted = Insert(entity);
                    break;
                case e_EntityState.Modified:
                    blnIsCompleted = Update(entity);
                    break;
            }
            
            // 操作完成时更新实体的状态
            if (blnIsCompleted)
            {
                // 启动事务时，记录实体的初始状态                
                if (IsTranExecuting)
                {
                    m_entityList.Add(entity, new EntityModifyInfo(entity));
                }
                
                entity.EntityState = e_EntityState.Unchanged;
            }

            return blnIsCompleted;
        }

        /// <summary>
        /// 保存实体集合。根据EntityState判断采用Delete、Insert或Update方法。
        /// </summary>
        /// <param name="entityCollection"></param>
        /// <returns></returns>
        public virtual bool Save(EntityCollectionBase entityCollection)
        {
            // Delete
            EntityCollectionBase deletedCollection = entityCollection.GetDeleted();
            if (deletedCollection != null && deletedCollection.Count > 0)
            {
                // 逐个删除
                foreach (EntityBase entity in deletedCollection)
                {
                    if (!Delete(entity))
                    {
                        return false;
                    }
                }  
                //IDataParameter[] paras = GetEntityKeyParameters(deletedCollection.ItemType);
                //if (paras.Length == 1)
                //{
                //    // 批量删除
                //    string strKeys = deletedCollection.GetValueString(paras[0].SourceColumn);
                //    string strWhere = GetEntityWhere(entityCollection.ItemType).Split('=')[0] + " IN (" + strKeys + ")";
                //    string strSql = string.Format(m_strSqlDelete, entityCollection.ItemType.Name, strWhere);

                //    int intReturn = ExecuteNonQuery(strSql);
                //    if (intReturn != 1)
                //    {
                //        return false;
                //    }
                //}
                //else
                //{
                //    // 逐个删除
                //    foreach (EntityBase entity in deletedCollection)
                //    {
                //        if (!Delete(entity))
                //        {
                //            return false;
                //        }
                //    }                    
                //}
            }
                        
            // 逐个Insert、Update
            foreach (EntityBase entity in entityCollection)
            {
                if (!Save(entity))
                {
                    return false;
                }
            }    

            //EntityCollectionBase modifiedCollection = entityCollection.GetModified();
            //if (modifiedCollection != null && modifiedCollection.Count > 0)
            //{
            //    foreach (EntityBase entity in modifiedCollection)
            //    {
            //        string strSql = "";
            //        if (m_sqlList.ContainsKey(modifiedCollection.ItemType.Name + m_strParaUpdate))
            //        {
            //            // 从缓存中获取SQL
            //            strSql = m_sqlList[modifiedCollection.ItemType.Name + m_strParaSelect];
            //        }
                    
            //        if (!Save(entity))
            //        {
            //            return false;
            //        }
            //    }
            //}

            return true;
        }

        /// <summary>
        /// 写入实体。
        /// </summary>
        /// <param name="entity"></param>
        public bool Insert(EntityBase entity)
        {
            // 设置参数
            IDataParameter[] paras = entity.GetAllParameters();
            string strColumns = entity.ColumnsName;
            
            // 排除自增量列
            if (entity.IdentityColumnName.Length > 0)
            {
                paras = DataTableHelper.Instance.ExcludeParameter(paras, entity.IdentityColumnName);
                paras = DataTableHelper.Instance.ExcludeParameter(paras, entity.IdentityColumnName + m_strParaWhere); 
                strColumns = StringHelper.Instance.Exclude(strColumns, entity.IdentityColumnName);
            }

            // 排除只读列
            foreach (DataColumn dcl in entity.Schema.Columns)
            {
                if (dcl.ReadOnly)
                {
                    paras = DataTableHelper.Instance.ExcludeParameter(paras, dcl.ColumnName);
                    paras = DataTableHelper.Instance.ExcludeParameter(paras, dcl.ColumnName + m_strParaWhere);
                    strColumns = StringHelper.Instance.Exclude(strColumns, dcl.ColumnName);
                }
            }
            
            string strParameterColumns = m_strFlagParameter + strColumns.Replace(",", "," + m_strFlagParameter);
            string strSql = string.Format(m_strSqlInsert, entity.Schema.TableName, strColumns, strParameterColumns);

            SetParameterValue(paras, entity);

            // 执行
            int intReturn = 0;
            if (entity.IdentityColumnName.Length > 0)
            {
                object objReturn = null;

                if (this.ControllerType == e_ControllerType.Oracle)
                {
                    ExecuteNonQuery(strSql, paras);
                    strSql = "select SEQUENCE_" + entity.Schema.TableName + ".Currval into :" + entity.IdentityColumnName + " from dual";
                    objReturn = GetInt(strSql);
                }
                else
                {
                    strSql += ";select @@Identity;";
                    objReturn = GetInt(strSql, paras);
                }
                
                if (!Convert.IsDBNull(objReturn))
                {
                    intReturn = 1;
                    entity.SetValue(entity.IdentityColumnName, objReturn);
                }
            }
            else
            {
                intReturn = ExecuteNonQuery(strSql, paras);
            }
            
            // 返回
            if (intReturn <= 0)
            {
                throw (new Exception("写入失败。数据可能已经存在。"));
            }
            return true;
        }

        /// <summary>
        /// 更新实体。
        /// </summary>
        /// <param name="entity"></param>
        public bool Update(EntityBase entity)
        {
            // 设置更新字段
            IDataParameter[] paras = null;
            string strColumns = "";
            if (entity.HasChange)
            {
                paras = entity.GetUpdateParameters();
                strColumns = entity.ModifiedColumnsName;
            }
            else
            {
                paras = entity.GetAllParameters();
                strColumns = entity.ColumnsName;
            }

            // 排除标识列
            if (entity.IdentityColumnName.Length > 0)
            {
                paras = DataTableHelper.Instance.ExcludeParameter(paras, entity.IdentityColumnName);
                strColumns = StringHelper.Instance.Exclude(strColumns, entity.IdentityColumnName);
            }

            StringBuilder sb = new StringBuilder(100);
            foreach (string strColumn in strColumns.Split(','))
            {
                if (sb.Length > 0)
                {
                    sb.Append(",");
                }
                sb.Append(strColumn);
                sb.Append("=");
                sb.Append(m_strFlagParameter);
                sb.Append(strColumn);
            }

            string strSql = string.Format(m_strSqlUpdate, entity.Schema.TableName, sb.ToString(), GetEntityWhere(entity));
            SetParameterValue(paras, entity);

            // 执行
            int intReturn = ExecuteNonQuery(strSql, paras);
            if (intReturn <= 0)
            {
                throw (new Exception("更新失败。数据可能已经被其它用户更改了。"));
            }

            return true;
        }

        /// <summary>
        /// 删除实体集合。
        /// </summary>
        /// <param name="entityCollection"></param>
        public bool Delete(EntityCollectionBase entityCollection)
        {
            // Delete
            foreach (EntityBase entity in entityCollection.GetDeleted())
            {
                if (!Delete(entity))
                {
                    return false;
                }
            }

            // Insert、Update
            foreach (EntityBase entity in entityCollection)
            {
                if (!Delete(entity))
                {
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// 删除实体。
        /// </summary>
        /// <param name="entity"></param>
        public bool Delete(EntityBase entity)
        {
            bool blnIsCompleted = DeleteByKey(entity.Type, entity.KeyValues);
            
            if (blnIsCompleted)
            {
                // 启动事务时，记录实体的初始状态                
                if (IsTranExecuting)
                {
                    m_entityList.Add(entity, new EntityModifyInfo(entity));
                }

                entity.EntityState = e_EntityState.Invalid;
            }

            return blnIsCompleted;
        }

        /// <summary>
        /// 删除数据。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public bool DeleteByKey(Type typ, object[] keyValues)
        {
            IDataParameter[] paras = GetEntityKeyParameters(typ);
            SetParameterValue(paras, keyValues);

            string strWhere = GetEntityWhere(typ);
            string strSql = string.Format(m_strSqlDelete, typ.Name, strWhere);

            int intReturn = ExecuteNonQuery(strSql, paras);
            if (intReturn != 1)
            {
                return false;
            }
            return true;
        }

        /// <summary>
        /// 填充实体数据。
        /// </summary>
        /// <param name="text"></param>
        /// <param name="paras"></param>
        /// <param name="entity"></param>
        /// <returns></returns>
        private bool FillEntity(string text, IDataParameter[] paras, EntityBase entity)
        {
            bool blnIsCompleted = false;
            IDbConnection cnn = CreateConnection();
            IDbCommand cmd = CreateCommand(cnn);
            IDataReader dr = null;

            try
            {
                InitializeCommand(cmd, CommandType.Text, text, paras);

                OpenConnection(cnn);

                dr = cmd.ExecuteReader(CommandBehavior.SingleRow);

                if (dr.Read())
                {
                    entity.SetValues(dr);
                    blnIsCompleted = true;
                }                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.Replace("'","\"") + "[" + text + "]", ex);
            }
            finally
            {
                if (dr != null)
                {
                    dr.Close();
                }
                if (cmd != null)
                {
                    cmd.Dispose();
                }
                if (cnn.State == ConnectionState.Open)
                {
                    CloseConnection(cnn);
                }
            }

            return blnIsCompleted;
        }

        /// <summary>
        /// 根据主键值，获取实体。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="keyValue1"></param>
        /// <returns></returns>
        public EntityBase GetByKey(Type typ, object keyValue1)
        {
            return GetByKey(typ, new object[] { keyValue1 });
        }

        /// <summary>
        /// 根据主键值，获取实体。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="keyValue1"></param>
        /// <param name="keyValue2"></param>
        /// <returns></returns>
        public EntityBase GetByKey(Type typ, object keyValue1, object keyValue2)
        {
            return GetByKey(typ, new object[] { keyValue1, keyValue2 });
        }

        /// <summary>
        /// 根据主键值，获取实体。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="keyValue1"></param>
        /// <param name="keyValue2"></param>
        /// <param name="keyValue3"></param>
        /// <returns></returns>
        public EntityBase GetByKey(Type typ, object keyValue1, object keyValue2, object keyValue3)
        {
            return GetByKey(typ, new object[] { keyValue1, keyValue2, keyValue3 });
        }

        /// <summary>
        /// 根据主键值，获取实体。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public EntityBase GetByKey(Type typ, object[] keyValues)
        {
            return GetByKey(typ, keyValues, m_strFlagAll);
        }

        /// <summary>
        /// 获取实体。返回指定字段。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="keyValues"></param>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        private EntityBase GetByKey(Type typ, object[] keyValues, string fieldList)
        {
            EntityBase entity = Activator.CreateInstance(typ, new object[] { e_EntityState.Unchanged }) as EntityBase;
            IDataParameter[] paras = entity.GetKeyParameters();
            string strSql = GetCachedSql(entity.Type.Name + m_strParaSelect);
            if (strSql == null)
            {
                strSql = BuildSqlSelect(entity);
            }

            string strCondition = GetEntityWhere(entity);

            strSql = string.Format(strSql, strCondition);   
            SetParameterValue(paras, keyValues);

            if (!FillEntity(strSql, paras, entity))
            {
                entity = null;
            }

            return entity;
        }

        /// <summary>
        /// 根据条件，获取实体。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public EntityBase GetByCondition(Type typ, string condition)
        {
            EntityBase entity = Activator.CreateInstance(typ, new object[] { e_EntityState.Unchanged }) as EntityBase;
            string strSql = GetCachedSql(entity.Type.Name + m_strParaSelect);
            if (strSql == null)
            {
                strSql = BuildSqlSelect(entity);
            }
            
            string strCondition = condition;
            if (strCondition == null || strCondition.Length == 0)
            {
                strCondition = "1=1";
            }

            strSql = string.Format(strSql, strCondition);

            if (!FillEntity(strSql, null, entity))
            {
                entity = null;
            }
            return entity;
        }

        /// <summary>
        /// 获取实体集合。
        /// </summary>
        /// <param name="collectionType"></param>
        /// <returns></returns>
        public EntityCollectionBase GetCollection(Type collectionType)
        {
            return GetCollectionByCondition(collectionType, "");
        }

        /// <summary>
        /// 根据条件，获取实体集合。
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        public EntityCollectionBase GetCollectionByCondition(Type collectionType, string condition)
        {
            EntityCollectionBase list = Activator.CreateInstance(collectionType, new object[] { }) as EntityCollectionBase;

            string strSql = GetCachedSql(list.ItemType.Name + m_strParaSelect);
            if (strSql == null)
            {
                strSql = BuildSqlSelect(list.ItemType);
            }

            string strCondition = condition;
            if (strCondition == null || strCondition.Length == 0)
            {
                strCondition = "1=1";
            }
            strSql = string.Format(strSql, strCondition);
            
            return GetCollection(list, strSql);
        }

        /// <summary>
        /// 根据commandText，获取实体集合。
        /// </summary>
        /// <param name="collectionType"></param>
        /// <param name="commandText"></param>
        /// <returns></returns>
        public EntityCollectionBase GetCollection(Type collectionType, string commandText)
        {
            EntityCollectionBase list = Activator.CreateInstance(collectionType, new object[] { }) as EntityCollectionBase;
            return GetCollection(list, commandText);
        }

        private EntityCollectionBase GetCollection(EntityCollectionBase list, string commandText)
        {
            EntityBase entity = null;

            IDbConnection cnn = CreateConnection();

            IDbCommand cmd = CreateCommand(cnn);
            cmd.CommandText = commandText;
            //cnn.Open();
            if (cnn != null && cnn.State != ConnectionState.Open)
            {
                cnn.Open();
            }
            using (IDataReader dr = cmd.ExecuteReader())
            {
                while (dr.Read())
                {
                    entity = Activator.CreateInstance(list.ItemType, new object[] { e_EntityState.Unchanged }) as EntityBase;
                    entity.SetValues(dr);
                    list.Add(entity);
                }
                if (dr != null)
                {
                    dr.Close();
                } 
            }
            return list;
        }
        
        private IDataParameter[] GetEntityKeyParameters(Type typ)
        {
            FieldInfo fieldSchema = typ.GetField("m_dtbEntitySchema", BindingFlags.Static | BindingFlags.NonPublic);
            FieldInfo fieldParameters = typ.GetField("m_listEntityParameters", BindingFlags.Static | BindingFlags.NonPublic);
            DataTable dtb = fieldSchema.GetValue(null) as DataTable;
            Hashtable list = fieldParameters.GetValue(null) as Hashtable;
            IDataParameter[] paras = DataTableHelper.Instance.GetParameters(list, null, dtb.PrimaryKey);
            return paras;
        }

        private string GetEntityWhere(EntityBase entity)
        {
            string strWhere = "";
            if (this.ControllerType == e_ControllerType.Oracle)
            {
                strWhere = entity.Sql_Where.Replace("@", ":");
            }
            else
            {
                strWhere = entity.Sql_Where;
            }
            return strWhere;
        }

        private string GetEntityWhere(Type typ)
        {
            FieldInfo fieldWhere = typ.GetField("m_strSqlWhere", BindingFlags.Static | BindingFlags.NonPublic);
            string strWhere = fieldWhere.GetValue(null).ToString();

            if (this.ControllerType == e_ControllerType.Oracle)
            {
                strWhere = strWhere.Replace("@", ":");
            }

            return strWhere;
        }

        /// <summary>
        /// 设置参数值。
        /// </summary>
        /// <param name="paras"></param>
        /// <param name="keyValues"></param>
        protected virtual void SetParameterValue(IDataParameter[] paras, object[] keyValues)
        {
            for (int intIndex = 0; intIndex <= paras.Length - 1; intIndex++)
            {
                paras[intIndex].Value = keyValues[intIndex];
            }
        }

        /// <summary>
        /// 设置参数值。
        /// </summary>
        /// <param name="paras"></param>
        /// <param name="entity"></param>
        protected virtual void SetParameterValue(IDataParameter[] paras, EntityBase entity)
        {
            PropertyInfo p = null;
            string strColumnName = "";

            foreach (IDataParameter prm in paras)
            {
                strColumnName = prm.ParameterName.Replace(m_strFlagParameter, "");
                if (strColumnName.IndexOf(m_strParaWhere) > 0)
                {
                    strColumnName = strColumnName.Replace(m_strParaWhere, "");
                    if (entity.ModifiedList != null && entity.ModifiedList.ContainsKey(strColumnName))
                    {
                        prm.Value = entity.ModifiedList[strColumnName];
                    }
                    else
                    {
                        p = entity.Type.GetProperty(strColumnName);
                        prm.Value = p.GetValue(entity, null);
                    }
                }
                else
                {
                    p = entity.Type.GetProperty(strColumnName);
                    prm.Value = p.GetValue(entity, null);
                    if (entity.Schema.Columns[strColumnName].DataType.Equals(typeof(DateTime)) && DataTypeHelper.IsDateTimeEmpty((DateTime)prm.Value))
                    {
                        prm.Value = System.DBNull.Value;
                    }
                }
            }
        }

        public bool Update(string strSql)
        {
            int intReturn = ExecuteNonQuery(strSql);
            /*if (intReturn <= 0)
            {
                throw (new Exception("更新失败。数据可能已经被其它用户更改了。"));
            }*/

            return true;
        }
        #endregion

        #region 全局变量
        protected int GetIdentity()
        {
            string strSql = "select @@IDENTITY";
            int intValue = 0;
            object objValue = ExecuteScalar(strSql);
            if (!Convert.IsDBNull(objValue))
            {
                intValue = (int)objValue;
            }
            return intValue;
        }
        #endregion
    }
}
