﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Data ;
using System.Data.SqlClient;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;

namespace XiWan.DALFactory
{
    public class ConditionBuilder
    {
        #region 构造函数
        public ConditionBuilder()
        {
        }

        public ConditionBuilder(IDataParameter[] paras)
        {
            CreateConditon(paras);
        }
        #endregion

        #region 变量
        StringCollection m_scCondition = new StringCollection();
        Hashtable m_hash = new Hashtable();
        private string m_strCondition = "";

        private List<IDataParameter> m_listParameters = new List<IDataParameter>();

        public List<IDataParameter> Parameters
        {
            get { return m_listParameters; }
            set { m_listParameters = value; }
        }
        #endregion
        
        #region 根据参数数组创建Condition
        private void CreateConditon(IDataParameter[] paras)
        {
            foreach (IDataParameter prm in paras)
            {
                if (prm.Value != null && prm.Value.ToString().Trim().Length > 0)
                {
                    m_listParameters.Add(prm);
                    switch ((prm as SqlParameter).SqlDbType)
                    {
                        case SqlDbType.NVarChar:
                            AddString(prm);
                            break;
                        case SqlDbType.VarChar:
                            AddString(prm);
                            break;
                        case SqlDbType.NChar:
                            AddString(prm);
                            break;
                        case SqlDbType.Char:
                            AddString(prm);
                            break;
                        case SqlDbType.Int:
                            AddInt(prm);
                            break;                        
                        case SqlDbType.SmallInt:
                            AddInt(prm);
                            break;
                        case SqlDbType.BigInt:  // 作特殊处理
                            AddDecimal(prm);
                            break;
                        case SqlDbType.Decimal:
                            AddDecimal(prm);
                            break;
                        case SqlDbType.Float:
                            AddDecimal(prm);
                            break;
                        case SqlDbType.Real:
                            AddDecimal(prm);
                            break;
                        case SqlDbType.Money:
                            AddDecimal(prm);
                            break;
                        case SqlDbType.Bit:
                            AddBit(prm);
                            break;
                        case SqlDbType.DateTime:
                            AddDateTime(prm);
                            break;
                        case SqlDbType.SmallDateTime:
                            AddDateTime(prm);
                            break;
                    }
                }
            }
        }

        private void AddString(IDataParameter prm)
        {
            m_scCondition.Add(prm.SourceColumn + " LIKE '%" + prm.Value.ToString() + "%'");
        }

        private void AddDateTime(IDataParameter prm)
        {
            string strPostfix = prm.ParameterName.Substring(prm.ParameterName.Length - 2);
            DateTime value = (DateTime)prm.Value;

            if (strPostfix   == "_B")
            {
                m_scCondition.Add(prm.SourceColumn + " >= '" + value.ToString(SystemInfo.c_Format_Date) + SystemInfo.c_Sql_DateTime_B + "'");
            }
            else if (strPostfix == "_E")
            {
                m_scCondition.Add(prm.SourceColumn + " <= '" + value.ToString(SystemInfo.c_Format_Date) + SystemInfo.c_Sql_DateTime_E + "'");
            }
            else
            {
                m_scCondition.Add(prm.SourceColumn + " = '" + value.ToString(SystemInfo.c_Format_DateSecond) + "'");
            }
        }

        private void AddDecimal(IDataParameter prm)
        {
            string strPostfix = prm.ParameterName.Substring(prm.ParameterName.Length - 2);
            decimal value = (decimal)prm.Value;
            string strOperator = " = ";

            if (strPostfix == "_B")
            {
                strOperator = " <= ";
            }
            else if (strPostfix == "_E")
            {
                strOperator = " >= ";
            }
            m_scCondition.Add(prm.SourceColumn + strOperator + value.ToString());
        }

        private void AddInt(IDataParameter prm)
        {
            if (prm.Value.ToString() == "0" && prm.ParameterName.Substring(prm.ParameterName.Length - 2) == "ID")
            {
                // 查询字段ID通常为类别查询，0表示全部类别，故忽略此条件
            }
            else
            {
                m_scCondition.Add(prm.SourceColumn + " = " + prm.Value.ToString());
            }
        }

        private void AddBit(IDataParameter prm)
        {
            string strValue = "0";
            if (prm.Value.ToString() == bool.TrueString) strValue = "1";
            m_scCondition.Add(prm.SourceColumn + " = " + strValue);
        }
        #endregion

        public void Add(string item)
        {
            m_scCondition.Add(item);
        }

        public void AddOr(string item)
        {
            m_scCondition.Add(item);
            m_hash.Add(item, "or");
        }

        public string BuildCondition()
        {
            return BuildCondition(" and ");
        }

        public string BuildCondition(string logic)
        {
            StringBuilder sb = new StringBuilder(200);
            foreach (string strItem in m_scCondition)
            {
                if (sb.Length == 0)
                {
                    sb.Append("(");
                    sb.Append(strItem);
                    sb.Append(")");
                }
                else
                {
                    if (m_hash[strItem] != null)
                    {
                        sb.Append(" or (");
                    }
                    else
                    {
                        sb.Append(" " + logic + " (");
                    }
                    sb.Append(strItem);
                    sb.Append(")");
                }
            }
            m_strCondition = sb.ToString();
            return m_strCondition;
        }
    }
}
