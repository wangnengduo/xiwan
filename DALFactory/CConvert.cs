﻿/*
 * 作者: Wong
 * 日期: 2018-6-13
 * 功能:常用数据类型转换类及方法，线程安全 
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using System.Data;
using System.Collections;

using System.Xml.Linq;

namespace XiWan.DALFactory
{

    /// <summary>
    /// 常用数据类型转换类及方法，线程安全
    /// </summary>
    public class CConvert
    {

        #region 基本类型转换

        #region 将字符串string转换为整型int
        /// <summary>
        /// 将字符串string转换为整型int
        /// </summary>
        /// <param name="str">需要转换的字串</param>
        /// <returns>成功返回转换后的结果，失败返回0</returns>
        static public int StringToInt(string str)
        {
            if (str == null)
                return 0;
            else if (str == "")
                return 0;
            else if (str.IndexOf(".") > 0)
            {
                decimal d;
                if (decimal.TryParse(str, out d))
                {
                    int i;
                    try
                    {
                        i = System.Convert.ToInt32(d);
                    }
                    catch
                    {
                        i = 0;
                    }
                    return i;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                int i;
                if (int.TryParse(str, out i))
                    return i;
                else
                    return 0;
            }
        }
        #endregion

        #region 将字符串string转换为无符号整型byte
        /// <summary>
        /// 将字符串string转换为无符号整型byte
        /// </summary>
        /// <param name="str">需要转换的字串</param>
        /// <returns>成功返回转换后的结果，失败返回0</returns>
        static public byte StringToByte(string str)
        {
            if (str == null)
                return 0;
            else if (str == "")
                return 0;
            else if (str.IndexOf(".") > 0)
            {
                decimal d;
                if (decimal.TryParse(str, out d))
                {
                    byte i;
                    try
                    {
                        i = System.Convert.ToByte(d);
                    }
                    catch
                    {
                        i = 0;
                    }
                    return i;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                byte i;
                if (byte.TryParse(str, out i))
                    return i;
                else
                    return 0;
            }
        }
        #endregion

        #region 将字符串string转换为整型decimal
        /// <summary>
        /// 将字符串string转换为整型decimal
        /// </summary>
        /// <param name="str">需要转换的字串</param>
        /// <returns>成功返回转换后的结果，失败返回0</returns>
        static public decimal StringToDecimal(object str)
        {
            if (str == null)
            {
                return 0;
            }
            else
            {
                try
                {
                    return Convert.ToDecimal(str);
                }
                catch
                {
                    return 0;
                }
            }
        }
        #endregion

        #region 将表的0转为空
        /// <summary>
        /// 将字符串string转换为整型decimal
        /// </summary>
        /// <param name="str">需要转换的字串</param>
        /// <returns>成功返回转换后的结果，失败返回0</returns>
        static public object TraceZeroToNull(object str)
        {
            if (str != DBNull.Value)
            {
                try
                {
                    if (Convert.ToDecimal(str) == 0)
                        str = DBNull.Value;
                }
                catch{}
            }
            return str;
        }
        #endregion




        #region 将字符串string转换为整型double
        /// <summary>
        /// 将字符串string转换为整型double
        /// </summary>
        /// <param name="str">需要转换的字串</param>
        /// <returns>成功返回转换后的结果，失败返回0</returns>
        static public double StringToDouble(object str)
        {
            if (str == null)
                return 0.0;
            else
            {
                try
                {
                    return Convert.ToDouble(str);
                }
                catch
                {
                    return 0.0;
                }                
            }
        }
        #endregion

        #region 将双精度double转换为个位数四舍五入的整型
        /// <summary>
        /// 将双精度double转换为个位数四舍五入的整型
        /// </summary>
        /// <returns></returns>
        static public int DoubleToTicketPrice(double dou)
        {
            try
            {
                return System.Convert.ToInt32((dou / 10).ToString("f0")) * 10;
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 将字符串string转换为布尔bool
        /// <summary>
        /// 将字符串string转换为布尔bool
        /// </summary>
        /// <param name="str">需要转换的字串，通常情况传True或False</param>
        /// <returns>如果转换成功，返回转换后的值，否则，返回假</returns>
        static public bool StringToBoolean(object strObj)
        {
            if (strObj == null)
                return false;
            else
            {
                string str = strObj.ToString();
                if (str == "")
                    return false;
                else
                {

                    if (str == "1") return true;
                    bool b = false;
                    if (bool.TryParse(str, out b)) return b;            //如果转换成功，返回转换后的值，否则，返回假
                    return false;
                }
            }
        }
        #endregion

        #region 把字符串转换成日期形式
        /// <summary>
        /// 把字符串转换成日期形式
        /// </summary>
        /// <param name="str">需要转换的字符串</param>
        /// <param name="dtValue">传唤失败时返回的日期</param>
        /// <returns>返回转换后的日期</returns>
        static public DateTime StringToDateTime(string str, DateTime dtValue)
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(str, out dt))
            {
                return dt;
            }
            else
            {
                return dtValue;
            }
        }

        /// <summary>
        /// 把字符串转换成日期形式
        /// </summary>
        /// <param name="str">需要转换的字符串</param>
        /// <param name="dtValue">传唤失败时返回的日期</param>
        /// <returns>返回转换后的日期</returns>
        static public DateTime? StringToDateTime(string str, DateTime? dtValue)
        {
            DateTime dt = new DateTime();
            if (DateTime.TryParse(str, out dt))
            {
                return dt;
            }
            else
            {
                return dtValue;
            }
        }
        #endregion

        #region 把字符串转换成日期形式
        /// <summary>
        /// 把字符串转换成日期形式
        /// </summary>
        /// <param name="str">需要转换的字符串</param>
        /// <returns>返回转换后的日期,如果失败返回日期类型最小值</returns>
        static public DateTime StringToDateTime(string str)
        {
            return StringToDateTime(str, DateTime.MinValue);
        }
        #endregion

        #region 把字符串转换成日期形式 MM月dd HH:mm
        static public string StringToFormatDateTime(object str)
        {
            if (str == null) return "";
            if (str.GetType() == System.DateTime.Now.GetType())
            {
                return System.Convert.ToDateTime(str).ToString("MM月dd HH:mm");
            }
            else
            {
                return str.ToString();
            }
        }

        #endregion

        #region 把字符串转换成日期形式 yyyy-MM-dd HH:mm
        static public string StringToFormatLongTime(object str)
        {
            if (str == null) return "";
            return System.Convert.ToDateTime(str).ToString("yyyy-MM-dd HH:mm").Substring(0, 16);
        }

        static public string StringToFormatShortTime(object str)
        {
            if (str == null) return "";

            return System.Convert.ToDateTime(str).ToString("yyyy-MM-dd HH:mm").Substring(11, 5);

        }
        static public string DateTimeParseToShort(object str)
        {
            if (str == null || str == DBNull.Value) return "";
            try
            {
                return System.Convert.ToDateTime(str).ToString("yyyy-MM-dd");
            }
            catch (Exception ex)
            {
                return  "";
            }

        }
        #endregion

        #region 将对象转换为int
        /// <summary>
        /// 将对象转换为int
        /// </summary>
        /// <param name="obj"></param>
        static public int ObjectToInt(object obj)
        {
            if (obj == null)
            {
                return 0;
            }
            try
            {
                return System.Convert.ToInt32(obj);                
            }
            catch(Exception ex1)
            {
                try
                {
                    if (obj.ToString().Contains("."))
                    {
                        return System.Convert.ToInt32(StringToDecimal(obj));
                    }
                }
                catch (Exception ex2)
                {                   
                }                
            }
            return 0;
        }
        #endregion

        #region 将对象转换为Int64
        /// <summary>
        /// 将对象转换为int
        /// </summary>
        /// <param name="obj"></param>
        static public Int64 ObjectToInt64(object obj)
        {
            if (obj == null)
            {
                return 0;
            }
            try
            {
                return System.Convert.ToInt64(obj);
            }
            catch
            {
                return 0;
            }
        }
        #endregion

        #region 将对象转换为字符串
        /// <summary>
        /// 将对象转换为字符串
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        static public string ObjectToString(object obj)
        {
            if (obj == null || obj == DBNull.Value)
            {
                return "";
            }
            return obj.ToString();
        }
        #endregion

        #region 将空的值转为0
        public static decimal TraceNull(decimal? dValue)
        {
            return dValue.HasValue ? dValue.Value : 0;
        }
        #endregion 


        #endregion

        #region //对象系列化与反序列化
        public static string SerializeObject(object o)
        {
            System.Runtime.Serialization.IFormatter obj = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

            MemoryStream ms = new MemoryStream();
            obj.Serialize(ms, o);

            BinaryReader br = new BinaryReader(ms);
            ms.Position = 0;
            byte[] bs = br.ReadBytes((int)ms.Length);
            ms.Close();
            return System.Convert.ToBase64String(bs);
        }

        public static object DeserializeObject(string str)
        {
            System.Runtime.Serialization.IFormatter obj = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
            byte[] bs = System.Convert.FromBase64String(str);
            MemoryStream ms = new MemoryStream();
            ms.Write(bs, 0, bs.Length);
            ms.Position = 0;
            object o = obj.Deserialize(ms);
            ms.Close();
            return o;
        }
        #endregion

        #region 范型集合和DataSet之间的转换

        #region 范型集合转为DataSet

        #region 数据集合转换成DataSet
        /// <summary>
        /// 数据集合转换成DataSet
        /// </summary>
        /// <param name="datas">数据集合转换成的Object数组</param>
        /// <returns></returns>
        public static DataSet ToDataSet(object[] datas)
        {
            DataSet result = new DataSet();
            DataTable _DataTable = new DataTable();

            if (datas == null || datas.Length == 0) return null;
            PropertyInfo[] propertys = datas[0].GetType().GetProperties();
            foreach (PropertyInfo pi in propertys)
            {
                if (pi.PropertyType.ToString().IndexOf("EntitySet") >= 0)
                {
                    continue;
                }
                _DataTable.Columns.Add(pi.Name, GetNotNullableType(pi.PropertyType));
            }

            if (datas.Length > 0)
            {
                for (int i = 0; i < datas.Length; i++)
                {
                    ArrayList tempList = new ArrayList();
                    foreach (PropertyInfo pi in propertys)
                    {
                        if (pi.PropertyType.ToString().IndexOf("EntitySet") >= 0)
                        {
                            continue;
                        }
                        object obj = pi.GetValue(datas[i], null);
                        tempList.Add(obj);
                    }
                    object[] array = tempList.ToArray();
                    _DataTable.LoadDataRow(array, true);
                }
            }
            result.Tables.Add(_DataTable);
            return result;
        }
        #endregion

        #region 泛型集合转换DataSet
        /// <summary>
        /// 泛型集合转换DataSet
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">泛型集合</param>
        /// <returns></returns>
        public static DataSet IListToDataSet<T>(IList<T> list) where T : class
        {
            return IListToDataSet<T>(list, null);
        }
        #endregion

        #region 泛型集合转换DataSet
        /// <summary>
        /// 泛型集合转换DataSet
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_List">泛型集合</param>
        /// <param name="p_PropertyName">待转换属性名数组</param>
        /// <returns></returns>
        public static DataSet IListToDataSet<T>(IList<T> p_List, params string[] p_PropertyName) where T : class
        {
            List<string> propertyNameList = new List<string>();
            if (p_PropertyName != null)
                propertyNameList.AddRange(p_PropertyName);

            DataSet result = new DataSet();
            DataTable _DataTable = new DataTable();
            if (p_List!=null&&p_List.Count > 0)
            {               
                PropertyInfo[] propertys = p_List[0].GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.PropertyType.ToString().IndexOf("EntitySet") >= 0)
                    {
                        continue;
                    }
                    if (propertyNameList.Count == 0)
                    {
                        // 没有指定属性的情况下全部属性都要转换
                        _DataTable.Columns.Add(pi.Name, GetNotNullableType(pi.PropertyType));
                    }
                    else
                    {
                        if (propertyNameList.Contains(pi.Name))
                            _DataTable.Columns.Add(pi.Name, GetNotNullableType(pi.PropertyType));
                    }
                }

                for (int i = 0; i < p_List.Count; i++)
                {
                    ArrayList tempList = new ArrayList();
                    foreach (PropertyInfo pi in propertys)
                    {
                        if (pi.PropertyType.ToString().IndexOf("EntitySet") >= 0)
                        {
                            continue;
                        }
                        if (propertyNameList.Count == 0)
                        {
                            object obj = pi.GetValue(p_List[i], null);
                            tempList.Add(obj);
                        }
                        else
                        {
                            if (propertyNameList.Contains(pi.Name))
                            {
                                object obj = pi.GetValue(p_List[i], null);
                                tempList.Add(obj);
                            }
                        }
                    }
                    object[] array = tempList.ToArray();
                    _DataTable.LoadDataRow(array, true);
                }
            }
            result.Tables.Add(_DataTable);
            return result;
        }
        #endregion

        #region 由泛型类架构转换DataTable架构
        /// <summary>
        /// 由泛型类架构转换DataTable架构 必须传一个对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="Obj">实际对象</param>
        /// <returns></returns>
        public static DataTable DtFromClass<T>(T Obj) where T : class
        {
            //List<string> propertyNameList = new List<string>();

            DataTable _DataTable = new DataTable();

            PropertyInfo[] propertys = Obj.GetType().GetProperties();
            foreach (PropertyInfo pi in propertys)
            {
                if (pi.PropertyType.ToString().IndexOf("EntitySet") >= 0)
                {
                    continue;
                }

                //增加列名
                _DataTable.Columns.Add(pi.Name, GetNotNullableType(pi.PropertyType));

            }
            return _DataTable;
        }
        #endregion

        #region 由泛型类架构转换DataTable
        /// <summary>
        /// 由泛型类架构转换DataTable架构 必须传一个对象
        /// </summary>
        /// <typeparam name="T">对象类型</typeparam>
        /// <param name="Obj">实际对象</param>
        /// <returns></returns>
        public static DataTable DtFromObject<T>(T Obj) where T : class
        {
            //List<string> propertyNameList = new List<string>();

            DataTable _DataTable = new DataTable(Obj.GetType().Name + (new Random()).Next(100).ToString());
            string strTempType = "";
            PropertyInfo[] propertys = Obj.GetType().GetProperties();
            foreach (PropertyInfo pi in propertys)
            {
                strTempType = pi.PropertyType.ToString();
                if (strTempType.IndexOf("EntitySet") >= 0 || strTempType.IndexOf("Entities") >= 0)
                {
                    continue;
                }

                try
                {
                    //增加列名
                    _DataTable.Columns.Add(pi.Name, GetNotNullableType(pi.PropertyType));
                }
                catch (Exception ex)
                { 
                }

            }

            if (Obj != null)
            {
                ArrayList tempList = new ArrayList();
                foreach (PropertyInfo pi in propertys)
                {
                    strTempType = pi.PropertyType.ToString();
                    if (strTempType.IndexOf("EntitySet") >= 0 || strTempType.IndexOf("Entities") >= 0)
                    {
                        continue;
                    }

                    object obj = pi.GetValue(Obj, null);
                    tempList.Add(obj);

                }
                object[] array = tempList.ToArray();
                _DataTable.LoadDataRow(array, true);
            }

            return _DataTable;
        }      
        #endregion

        #region 泛型集合转换DataTable
        /// <summary>
        /// 泛型集合转换DataSet
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_List">泛型集合</param>
        /// <param name="p_PropertyName">待转换属性名数组</param>
        /// <returns></returns>
        public static DataTable IListToDataTable<T>(IList<T> p_List, params string[] p_PropertyName) where T : class
        {
            List<string> propertyNameList = new List<string>();
            if (p_PropertyName != null)
                propertyNameList.AddRange(p_PropertyName);
            string strTempType = "";

            DataTable _DataTable = null;
            if (p_List != null && p_List.Count > 0)
            {
                _DataTable = new DataTable(p_List[0].GetType().Name);
                PropertyInfo[] propertys = p_List[0].GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    strTempType = pi.PropertyType.ToString();
                    if (strTempType.IndexOf("EntitySet") >= 0 || strTempType.IndexOf("Entities") >= 0)
                    {
                        continue;
                    }
                    if (propertyNameList.Count == 0)
                    {
                        // 没有指定属性的情况下全部属性都要转换
                        _DataTable.Columns.Add(pi.Name, GetNotNullableType(pi.PropertyType));
                    }
                    else
                    {
                        if (propertyNameList.Contains(pi.Name))
                            _DataTable.Columns.Add(pi.Name, GetNotNullableType(pi.PropertyType));
                    }
                }

                for (int i = 0; i < p_List.Count; i++)
                {
                    ArrayList tempList = new ArrayList();
                    foreach (PropertyInfo pi in propertys)
                    {
                        strTempType = pi.PropertyType.ToString();
                        if (strTempType.IndexOf("EntitySet") >= 0 || strTempType.IndexOf("Entities") >= 0)
                        {
                            continue;
                        }
                        if (propertyNameList.Count == 0)
                        {
                            object obj = pi.GetValue(p_List[i], null);
                            tempList.Add(obj);
                        }
                        else
                        {
                            if (propertyNameList.Contains(pi.Name))
                            {
                                object obj = pi.GetValue(p_List[i], null);
                                tempList.Add(obj);
                            }
                        }
                    }
                    object[] array = tempList.ToArray();
                    _DataTable.LoadDataRow(array, true);
                }
            } 
            return _DataTable;
        }
        #endregion
        #endregion

        #region DataSet装换为泛型集合

        #region DataSet装换为泛型集合
        /// <summary>
        /// DataSet装换为泛型集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_DataSet">DataSet</param>
        /// <param name="p_TableIndex">待转换数据表索引</param>
        /// <returns></returns>
        public static IList<T> DataSetToIList<T>(DataSet p_DataSet, int p_TableIndex) where T : class
        {
            if (p_DataSet == null || p_DataSet.Tables.Count < 0)
                return null;
            if (p_TableIndex > p_DataSet.Tables.Count - 1)
                return null;
            if (p_TableIndex < 0)
                p_TableIndex = 0;

            DataTable p_Data = p_DataSet.Tables[p_TableIndex];

            return DataTableToIList<T>(p_Data) ;
        }
        #endregion

        #region DataSet装换为泛型集合
        /// <summary>
        /// DataSet装换为泛型集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_DataSet">DataSet</param>
        /// <param name="p_TableName">待转换数据表名称</param>
        /// <returns></returns>
        public static IList<T> DataSetToIList<T>(DataSet p_DataSet, string p_TableName) where T : class
        {
            int _TableIndex = 0;
            if (p_DataSet == null || p_DataSet.Tables.Count < 0)
                return null;
            if (string.IsNullOrEmpty(p_TableName))
                return null;
            for (int i = 0; i < p_DataSet.Tables.Count; i++)
            {
                // 获取Table名称在Tables集合中的索引值
                if (p_DataSet.Tables[i].TableName.Equals(p_TableName))
                {
                    _TableIndex = i;
                    break;
                }
            }
            return DataSetToIList<T>(p_DataSet, _TableIndex);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="p_type"></param>
        /// <returns></returns>
        public static Type GetNotNullableType(Type p_type)
        {
            if (p_type == typeof(Int16?))
            {
                return typeof(Int16);
            }
            if (p_type == typeof(Int32?))
            {
                return typeof(Int32);
            }
            if (p_type == typeof(Int64?))
            {
                return typeof(Int64);
            }
            if (p_type == typeof(decimal?))
            {
                return typeof(decimal);
            }
            if (p_type == typeof(double?))
            {
                return typeof(double);
            }
            if (p_type == typeof(DateTime?))
            {
                return typeof(DateTime);
            }
            if (p_type == typeof(Boolean?))
            {
                return typeof(Boolean);
            }
            if (p_type == typeof(Guid?))
            {
                return typeof(Guid);
            }
            if (p_type == typeof(byte?))
            {
                return typeof(byte);
            }
            if (p_type == typeof(float?))
            {
                return typeof(float);
            }
            return p_type;
        }
        #endregion

        #region DataTable装换为泛型集合
        /// <summary>
        /// DataTable装换为泛型集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_DataSet">DataTable</param>
        /// <returns></returns>
        public static IList<T> DataTableToIList<T>(DataTable p_Data) where T : class
        {
            if (p_Data == null || p_Data.Rows.Count < 1)
                return null;               
            // 返回值初始化
            IList<T> result = new List<T>();
            for (int j = 0; j < p_Data.Rows.Count; j++)
            {
                T _t = (T)Activator.CreateInstance(typeof(T));
                PropertyInfo[] propertys = _t.GetType().GetProperties();
                foreach (PropertyInfo pi in propertys)
                {
                    for (int i = 0; i < p_Data.Columns.Count; i++)
                    {
                        // 属性与字段名称一致的进行赋值
                        if (pi.Name.Equals(p_Data.Columns[i].ColumnName))
                        {
                            // 数据库NULL值单独处理
                            if (p_Data.Rows[j][i] != DBNull.Value)
                                pi.SetValue(_t, p_Data.Rows[j][i], null);
                            else
                                pi.SetValue(_t, null, null);
                            break;
                        }
                    }
                }
                result.Add(_t);
            }
            return result;
        }
        
        #endregion

        #region DataTable装换为泛型集合
        /// <summary>
        /// DataTable装换为泛型集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_DataSet">DataTable</param>
        /// <returns></returns>
        public static List<T> DataTableToList<T>(DataTable p_Data) where T : class
        {
            if (p_Data == null || p_Data.Rows.Count < 1)
                return null;               
            // 返回值初始化
            List<T> result = new List<T>();
            string strTypeName = "";
            for (int j = 0; j < p_Data.Rows.Count; j++)
            {
                T _t = (T)Activator.CreateInstance(typeof(T));
                
                PropertyInfo[] propertys = _t.GetType().GetProperties();

                #region //检查是否有接收修改的字段
                PropertyInfo AttachP = null;
                for(int k=propertys.Length-1;k>=0;k--)
                {
                    if (propertys[k].Name.Equals("ChangedProperty"))
                    {
                        AttachP = propertys[k];
                        break;
                    }
                }
                #endregion 

                foreach (PropertyInfo pi in propertys)
                {
                    for (int i = 0; i < p_Data.Columns.Count; i++)
                    {
                        // 属性与字段名称一致的进行赋值
                        if (pi.Name.Equals(p_Data.Columns[i].ColumnName))
                        {
                            if(AttachP!=null)
                                AttachP.SetValue(_t,pi.Name, null);
                            // 数据库NULL值单独处理
                            if (p_Data.Rows[j][i] != DBNull.Value)
                            {
                                strTypeName = pi.PropertyType.FullName;
                                if (strTypeName.Contains("System.Nullable`1[["))
                                {
                                    strTypeName = strTypeName.Replace("System.Nullable`1[[", "");
                                    if(strTypeName.Contains(',')) 
                                        strTypeName = strTypeName.Substring(0, strTypeName.IndexOf(','));
                                }

                                if (strTypeName == p_Data.Rows[j][i].GetType().FullName)
                                {
                                    pi.SetValue(_t, p_Data.Rows[j][i], null);
                                }
                                else
                                {
                                    //不同类型，且空字符，则直接赋空值
                                    if (p_Data.Rows[j][i].GetType().FullName == "System.String" && p_Data.Rows[j][i].ToString().Trim() == "")
                                    {
                                        pi.SetValue(_t, null, null);
                                        break;
                                    }
                                    //其他
                                    switch (strTypeName)
                                    {
                                        case "System.String":
                                            pi.SetValue(_t, p_Data.Rows[j][i].ToString(), null);
                                            break;

                                        case "System.Int32":
                                            pi.SetValue(_t, CConvert.ObjectToInt(p_Data.Rows[j][i]), null);
                                            break;

                                        case "System.Decimal":
                                            pi.SetValue(_t, CConvert.StringToDecimal(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.DateTime":
                                            pi.SetValue(_t, CConvert.StringToDateTime(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.Boolean":
                                            pi.SetValue(_t, CConvert.StringToBoolean(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.Int64":
                                            pi.SetValue(_t, CConvert.ObjectToInt64(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.Int16":
                                            pi.SetValue(_t, System.Convert.ToInt16(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.Double":
                                            pi.SetValue(_t, CConvert.StringToDouble(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.Single":
                                            pi.SetValue(_t, Single.Parse(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.Char":
                                            pi.SetValue(_t, Char.Parse(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        case "System.Byte":
                                            pi.SetValue(_t, Byte.Parse(p_Data.Rows[j][i].ToString()), null);
                                            break;

                                        default:
                                            break;
                                    }
                                } 
                                
                            }
                            else
                                pi.SetValue(_t, null, null);
                            break;
                        }
                    }
                }
                result.Add(_t);
            }
            return result;
        }
        
        #endregion
     
        #endregion

        #endregion

        #region 获取表字段名字符串序列
        /// <summary>
        /// 获取表字段名字符串序列
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static string GetDatatableColumnsNames(DataTable dt)
        {
            string strColumns = "";
            foreach (DataColumn column in dt.Columns)
            {
                strColumns += column.ColumnName + ",";
            }
            strColumns = strColumns.TrimEnd(',');
            return strColumns;
        }
        #endregion 

        #region 不同对象之间数据Clone..

        #region 返回clone的实体给调用方
        /// <summary>
        /// 返回clone的实体给调用方
        /// </summary>
        /// <typeparam name="T">泛型类</typeparam>
        /// <param name="obj">T类型</param>
        /// <returns>返回T类型的新实体</returns>        
        public static T Clone<T>(T obj) where T : class
        {
            try
            {
                Type t = obj.GetType();
                T newObj = (T)Activator.CreateInstance(t);

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        pi.SetValue(newObj, pi.GetValue(obj, null), null);
                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        fi.SetValue(newObj, fi.GetValue(obj));
                    }
                    catch { }
                }

                return newObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Copy指定列的数据新对象 Copy列数组
        /// <summary>
        /// Copy指定列的数据新对象
        /// </summary>
        /// <typeparam name="T">对象类型，仅限于类</typeparam>
        /// <param name="obj">源对象</param>
        /// <param name="propertyNames">Copy列数组</param>
        public static T Clone<T>(T obj, string[] propertyNames) where T : class
        {
            try
            {
                Type t = obj.GetType();
                T newObj = (T)Activator.CreateInstance(t);

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        if (propertyNames.Where(o => o.Trim().ToLower() == pi.Name.Trim().ToLower()).Count() > 0)
                        {
                            pi.SetValue(newObj, pi.GetValue(obj, null), null);
                        }

                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        if (propertyNames.Where(o => o.Trim().ToLower() == fi.Name.Trim().ToLower()).Count() > 0)
                        {
                            fi.SetValue(newObj, fi.GetValue(obj));
                        }
                    }
                    catch { }
                }

                return newObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Clone 基础
        public static void Clone<T>(T source, T target) where T : class
        {
            try
            {
                Type t = source.GetType();

                if (target == null)
                { return; }

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        pi.SetValue(target, pi.GetValue(source, null), null);
                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        fi.SetValue(target, fi.GetValue(source));
                    }
                    catch { }
                }

                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Copy指定列的数据新对象
        /// <summary>
        /// Copy指定列的数据新对象
        /// </summary>
        /// <typeparam name="T">对象类型，仅限于类</typeparam>
        /// <param name="source">源对象</param>
        /// <param name="target">新对象，传入对象必须已实例化</param>
        /// <param name="propertyNames">Copy列数组</param>
        public static void Clone<T>(T source, T target, string[] propertyNames) where T : class
        {
            try
            {
                Type t = source.GetType();

                if (target == null)
                { return; }

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        if (propertyNames.Where(o => o.Trim().ToLower() == pi.Name.Trim().ToLower()).Count() > 0)
                        {
                            pi.SetValue(target, pi.GetValue(source, null), null);
                        }

                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        if (propertyNames.Where(o => o.Trim().ToLower() == fi.Name.Trim().ToLower()).Count() > 0)
                        {
                            fi.SetValue(target, fi.GetValue(source));
                        }
                    }
                    catch { }
                }

                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 批量获取备份数据
        /// <summary>
        /// 批量获取备份数据
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source"></param>
        /// <param name="propertyNames"></param>
        /// <returns></returns>
        public static List<T> Clone<T>(List<T> source, string[] propertyNames) where T : class
        {
            try
            {
                List<T> target = new List<T>();
                T obj;
                foreach (T item in source)
                {
                    obj = Clone<T>(item, propertyNames);

                    target.Add(obj);
                }

                return target;
            }
            catch
            {
                return null;
            }
        }
        #endregion

        #region 两个不同类型的对象数据复制
        /// <summary>
        /// 两个不同类型的对象数据复制
        /// </summary>
        /// <typeparam name="TSource">数据对象类</typeparam>
        /// <typeparam name="TTarget">要赋值的对象类</typeparam>
        /// <param name="source">数据对象</param>
        /// <returns></returns>
        public static TTarget Clone<TSource, TTarget>(TSource source)
            where TSource : class
            where TTarget : class
        {
            if (source == null)
            {
                return null;
            }

            try
            {
                Type tSource = source.GetType();
                Type tTarget = typeof(TTarget);

                // 催化剂实例对象
                TTarget target = (TTarget)Activator.CreateInstance(tTarget);

                PropertyInfo sProperty;
                foreach (PropertyInfo pi in tTarget.GetProperties())
                {
                    try
                    {
                        sProperty = tSource.GetProperty(pi.Name);

                        pi.SetValue(target, sProperty.GetValue(source, null), null);
                    }
                    catch { }
                }
                FieldInfo sfield;
                foreach (FieldInfo fi in tTarget.GetFields())
                {
                    try
                    {
                        sfield = tSource.GetField(fi.Name);

                        fi.SetValue(target, sfield.GetValue(source));
                    }
                    catch { }
                }
                return target;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 两个不同类型同属性的对象数组复制
        /// <summary>
        /// 两个不同类型同属性的对象数组复制
        /// </summary>
        /// <param name="arrSourceObj">数据源数组对象</param>
        /// <returns>返回的指定类型的数组对象</returns>
        public static TTarget[] CloneArrayObject<TSource, TTarget>(TSource[] arrSourceObj)
            where TSource : class
            where TTarget : class
        {
            if (arrSourceObj == null)
            {
                return null;
            }
            //复制返回的数组TTarget Clone<TSource, TTarget>(TSource source) where TSource : class where TTarget :class
            List<TTarget> iList = new List<TTarget>();

            for (int i = 0; i < arrSourceObj.Length; i++)
            {
                TTarget newObj;
                newObj = Clone<TSource, TTarget>(arrSourceObj[i]);
                iList.Add(newObj);
            }

            return iList.ToArray<TTarget>();
        }
        #endregion

        #region 两个不同类型同属性的对象数组复制,返回List<T>
        /// <summary>
        /// 两个不同类型同属性的对象数组复制,返回List<T>
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <typeparam name="TTarget"></typeparam>
        /// <param name="arrSourceObj"></param>
        /// <returns></returns>
        public static List<TTarget> CloneArrayToList<TSource, TTarget>(TSource[] arrSourceObj)
            where TSource : class
            where TTarget : class
        {
            if (arrSourceObj == null)
            {
                return null;
            }
            //复制返回的数组TTarget Clone<TSource, TTarget>(TSource source) where TSource : class where TTarget :class
            List<TTarget> iList = new List<TTarget>();

            for (int i = 0; i < arrSourceObj.Length; i++)
            {
                TTarget newObj;
                newObj = Clone<TSource, TTarget>(arrSourceObj[i]);
                iList.Add(newObj);
            }
            return iList;
        }
        #endregion 

        #endregion

        #region 小写金额转换成大写金额
        /// <summary>
        /// 小写金额转换成大写金额
        /// </summary>
        /// <param name="vpdMoney"></param>
        /// <returns></returns>
        public static string ConvertToChMoney(decimal vpdMoney)
        {
            string vpsCnMoney = "";
            string sCnDigit = "零壹贰叁肆伍陆柒捌玖";            //0-9所对应的汉字
            string sCnUnit = "万仟佰拾亿仟佰拾万仟佰拾元角分"; //数字位所对应的汉字
            string sTemp1 = "", sTemp2 = "";
            int i, j;
            string sCn1 = "";//数字的汉语读法
            string sCn2 = "";//数字位的汉字读法
            int iTemp, iZero = 0;//用来计算连续的零值是几个

            //将要转换的金额取绝对值并四舍五入取2位小数
            vpdMoney = Math.Round(Math.Abs(vpdMoney), 2);
            //将要转换的金额乘100并转换成字符串形式
            sTemp2 = ((long)(vpdMoney * 100)).ToString();
            //判断是否溢出
            j = sTemp2.Length;
            if (j > 15)
            {
                return "溢出";
            }
            //取出对应位数的汉字位置。如：199.01,j为5所以:佰拾元角分
            sCnUnit = sCnUnit.Substring(15 - j);

            //循环取出每一位转换成相应的值
            for (i = 0; i < j; i++)
            {
                sTemp1 = sTemp2.Substring(i, 1);//取出需转换的某一位的值
                iTemp = Convert.ToInt32(sTemp1);//转换为数字

                if (i != (j - 3) && i != (j - 7) && i != (j - 11) && i != (j - 15))
                {
                    //当所取位数不为元、万、亿、万亿上的数字时
                    if (sTemp1 == "0")
                    {
                        sCn1 = "";
                        sCn2 = "";
                        iZero = iZero + 1;
                    }
                    else
                    {
                        if (sTemp1 != "0" && iZero != 0)
                        {
                            sCn1 = "零" + sCnDigit.Substring(iTemp * 1, 1);
                            sCn2 = sCnUnit.Substring(i, 1);
                            iZero = 0;
                        }
                        else
                        {
                            sCn1 = sCnDigit.Substring(iTemp * 1, 1);
                            sCn2 = sCnUnit.Substring(i, 1);
                            iZero = 0;
                        }
                    }
                }
                else
                {
                    //该位是万亿，亿，万，元位等关键位
                    if (sTemp1 != "0" && iZero != 0)
                    {
                        sCn1 = "零" + sCnDigit.Substring(iTemp * 1, 1);
                        sCn2 = sCnUnit.Substring(i, 1);
                        iZero = 0;
                    }
                    else
                    {
                        if (sTemp1 != "0" && iZero == 0)
                        {
                            sCn1 = sCnDigit.Substring(iTemp * 1, 1);
                            sCn2 = sCnUnit.Substring(i, 1);
                            iZero = 0;
                        }
                        else
                        {
                            if (sTemp1 == "0" && iZero >= 3)
                            {
                                sCn1 = "";
                                sCn2 = "";
                                iZero++;
                            }
                            else
                            {
                                if (j >= 11)
                                {
                                    sCn1 = "";
                                    iZero++;
                                }
                                else
                                {
                                    sCn1 = "";
                                    sCn2 = sCnUnit.Substring(i, 1);
                                    iZero++;
                                }
                            }
                        }
                    }
                }
                if (i == (j - 11) || i == (j - 3))
                {
                    //如果该位是亿位或元位，则必须写上
                    sCn2 = sCnUnit.Substring(i, 1);
                }
                vpsCnMoney = vpsCnMoney + sCn1 + sCn2;

                if (i == j - 1 && sTemp1 == "0")
                {
                    //最后一位（分）为0时，加上“整”
                    vpsCnMoney = vpsCnMoney + '整';
                }
            }
            if (vpdMoney == 0)
            {
                vpsCnMoney = "零元整";
            }

            return vpsCnMoney;
        }
        #endregion 

    }


}
