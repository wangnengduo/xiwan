﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Data;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;

namespace XiWan.DALFactory
{
    [Serializable]
    public class EntityCollectionBase : System.Collections.CollectionBase 
    {
        public EntityCollectionBase()
        {
        }

        protected EntityCollectionBase m_collectionDeleted = null;

        public virtual Type Type
        {
            get { return null; }
        }

        public virtual Type ItemType
        {
            get { return null; }
        }

        /// <summary>
        /// 供Controller创建实体集合使用。
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public int Add(EntityBase value)
        {
            return List.Add(value);
        }

        #region 查找集合中的实体
        /// <summary>
        /// 通过主键查找集合中的实体。
        /// </summary>
        /// <param name="keyValues"></param>
        /// <returns></returns>
        public EntityBase GetItem(object keyValues)
        {
            if (keyValues == null || this.Count == 0)
            {
                return null;
            }

            object[] objValues = null;
            if (keyValues.ToString() == "System.Object[]")
            {
                objValues = (object[])keyValues;                
            }
            else
            {
                objValues = new object[] { keyValues };
            }

            bool blnIsFound = true;
            foreach (EntityBase entity in this)
            {
                blnIsFound = true;
                for (int i = 0; i < entity.KeyValues.Length; i++)
                {
                    if (entity.KeyValues[i].ToString() != objValues[i].ToString())
                    {
                        blnIsFound = false;
                        break;
                    }
                }
                if (blnIsFound)
                {
                    return entity;
                }
            }
            return null;
        }

        public EntityBase GetItem(string keys, string values)
        {
            return GetItem(keys.Split(','), values.Split(','));
        }

        public EntityBase GetItem(string keys, object[] values)
        {
            return GetItem(keys.Split(','), values);
        }

        /// <summary>
        /// 通过指定列查找集合中的实体。返回第一个匹配条件的实体。
        /// </summary>
        /// <param name="keys"></param>
        /// <param name="values"></param>
        /// <returns></returns>
        public EntityBase GetItem(string[] keys, object[] values)
        {
            EntityBase found = null;

            foreach (EntityBase entity in this)
            {
                found = entity;
                
                for (int i=0;i<keys.Length;i++)
                {
                    if (entity.GetValue(keys[i]).ToString() != values[i].ToString())
                    {
                        found = null;
                        break;
                    }
                }

                if (found != null)
                {
                    break;
                }
            }

            return found;
        }
        #endregion

        #region 获取各种更改状态的实体集合
        public virtual EntityCollectionBase GetAdded()
        {
            return GetChanges(e_EntityState.Added);
        }

        public virtual EntityCollectionBase GetDeleted()
        {
            return GetChanges(e_EntityState.Deleted);
        }

        public virtual EntityCollectionBase GetModified()
        {
            return GetChanges(e_EntityState.Modified);
        }

        public virtual EntityCollectionBase GetChanges()
        {
            EntityCollectionBase collection = new EntityCollectionBase();

            foreach (EntityBase entity in this)
            {
                if (entity.EntityState == e_EntityState.Added || entity.EntityState == e_EntityState.Modified || entity.EntityState == e_EntityState.Deleted)
                {
                    collection.Add(EntityBase.Clone(entity));
                }
            }

            return collection;
        }

        public virtual EntityCollectionBase GetChanges(e_EntityState state)
        {
            EntityCollectionBase collection = new EntityCollectionBase();

            if (state == e_EntityState.Deleted)
            {
                return m_collectionDeleted;
            }
            else
            {
                foreach (EntityBase entity in this)
                {
                    if (entity.EntityState == state)
                    {
                        collection.Add(EntityBase.Clone(entity));
                    }
                }
            }

            return collection;
        }
        #endregion

        /// <summary>
        /// 将集合中实体的状态设置为未更改状态。
        /// </summary>
        public void AcceptChanges()
        {
            foreach (EntityBase entity in this)
            {
                entity.SetEntityState(e_EntityState.Unchanged);
            }

            if (m_collectionDeleted != null)
            {
                foreach (EntityBase entity in m_collectionDeleted)
                {
                    entity.SetEntityState(e_EntityState.Invalid);
                }
                m_collectionDeleted.Clear();
            }
        }

        /// <summary>
        /// 将dataTable转换为实体集合。
        /// </summary>
        /// <param name="dtb"></param>
        /// <param name="collectionType"></param>
        /// <param name="entityType"></param>
        /// <returns></returns>
        public static EntityCollectionBase DataTableToCollection(DataTable dtb,Type collectionType,Type entityType)
        {
            EntityCollectionBase collection = Activator.CreateInstance(collectionType) as EntityCollectionBase;
            foreach (DataRow drw in dtb.Rows)
            {
                collection.Add(EntityBase.DataRowToEntity(entityType, drw));
            }
            return collection;
        }

        public void Fill(IEnumerable collection)
        {
            int startIndex = List.Count - 1;
            foreach (object item in collection)
            {
                List.Add(item);
            }
            for (int i = startIndex; i >= 0; i--)
            {
                List.RemoveAt(i);
            }
        }

        #region 排序
        public void Sort(string column)
        {
            Sort(column, false);
        }

        public void Sort(string column,bool isDesc)
        {
            System.Collections.IComparer sorter = new SortHelper(column, isDesc);
            InnerList.Sort(sorter);
        }

        private class SortHelper : System.Collections.IComparer
        {
            private const int c_Greater = 1;
            private const int c_Less = -1;
            private const int c_Equal = 0;

            private string m_strColumn = "";
            private bool m_blnIsDesc = false;

            public SortHelper(string column, bool isDesc)
            {
                if (column == null || column.Length == 0)
                {
                    throw new Exception("未指定有效的参数[column]。");
                }

                m_strColumn = column;
                m_blnIsDesc = isDesc;
            }

            public int Compare(object x, object y)
            {   
                EntityBase entityX = (EntityBase)x;
                EntityBase entityY = (EntityBase)y;
                object objValueX = entityX.GetValue(m_strColumn);
                object objValueY = entityY.GetValue(m_strColumn);
                Type typ = x.GetType();
                PropertyInfo p = typ.GetProperty(m_strColumn);
                int intResult = 0;
                
                if (p != null)
                {
                    switch (p.PropertyType.FullName)
                    {
                        case "System.String":
                            intResult = objValueX.ToString().CompareTo(objValueY.ToString());
                            break;

                        case "System.Int32":
                            int intValueX = System.Convert.ToInt32(objValueX);
                            int intValueY = System.Convert.ToInt32(objValueY);
                            if (intValueX > intValueY)
                            {
                                intResult = c_Greater;
                            }
                            else if (intValueX < intValueY)
                            {
                                intResult = c_Less; 
                            }
                            else
                            {
                                intResult = c_Equal;
                            }
                            break;

                        case "System.Decimal":
                            decimal decValueX = System.Convert.ToDecimal(objValueX);
                            decimal decValueY = System.Convert.ToDecimal(objValueY);
                            if (decValueX > decValueY)
                            {
                                intResult = c_Greater; 
                            }
                            else if (decValueX < decValueY)
                            {
                                intResult = c_Less;
                            }
                            else
                            {
                                intResult = c_Equal;
                            }
                            break;

                        case "System.DateTime":                            
                            break;

                        case "System.Boolean":                            
                            break;

                        case "System.Int64":                            
                            break;

                        case "System.Int16":                            
                            break;

                        case "System.Double":                            
                            break;

                        case "System.Single":
                            break;

                        case "System.Char":
                            break;

                        case "System.Byte":
                            break;

                        default:
                            break;
                    }
                }

                // 倒序处理
                if (m_blnIsDesc)
                {
                    if (intResult == c_Greater)
                    {
                        intResult = c_Less;
                    }
                    else if (intResult == c_Less)
                    {
                        intResult = c_Greater;
                    }
                }

                return intResult;
            }
        }
        #endregion

        public string GetValueString(string columns)
        {
            string strValueString = "";

            foreach (EntityBase entity in this)
            {
                if (strValueString.Length > 0)
                {
                    strValueString += ",";
                }

                strValueString += entity.GetValue(columns).ToString();
            }

            return strValueString;
        }

        /// <summary>
        /// 把符合给定条件的实体对象从集合中删除
        /// </summary>
        /// <param name="conditionFilter">列名和值对应，多列用','分开即可。如：custumerID=120</param>
        /// <returns>集合有元素被删除则返回true，无则返回false</returns>
        public bool DeleteItemByCondition(string conditionFilter)
        {
            string[] conditions = conditionFilter.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
            if (conditions.Length == 0)
            {
                return false;
            }

            bool blnDeleted = false;
            string[] columns = new string[conditions.Length];
            string[] values = new string[conditions.Length];

            for (int i = 0; i < conditions.Length; i++)
            {
                columns[i] = conditions[i].Split('=')[0];
                values[i] = conditions[i].Split('=')[1];
            }

            for (int i = List.Count-1; i >=0; i--)
            {
                EntityBase en = List[i] as EntityBase;
                for (int j = 0; j < columns.Length; j++)
                {
                    if (en.GetValue(columns[j]).ToString() != values[j])
                    {
                        break;
                    }
                    else
                    {
                        if (j == columns.Length - 1)
                        {
                            blnDeleted = true;
                            List.Remove(en);

                            if (m_collectionDeleted == null)
                            {
                                m_collectionDeleted = new EntityCollectionBase();
                            }
                            if (en.EntityState != e_EntityState.Added)
                            {
                                m_collectionDeleted.Add(en);
                                en.SetEntityState(e_EntityState.Deleted);
                            }
                        }
                    }
                }
            }

            return blnDeleted;
        }
    }    
}
