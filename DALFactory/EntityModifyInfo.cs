﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;

namespace XiWan.DALFactory
{
    public class EntityModifyInfo
    {
        public EntityModifyInfo(EntityBase entity)
        {
            this.EntityState = entity.EntityState;
            this.ModifiedList = entity.ModifiedList;
        }

        public e_EntityState EntityState { get; set; }
        public Hashtable ModifiedList { get; set; }
    }
}
