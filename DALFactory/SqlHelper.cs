/*----------------------------------------------------------------------------------------------------
 * 代码说明：
 * 
 *       数据操作辅助类
 * 
 * 创建者：
 * 
 * 创建日期：
 * 
 * 修改日志： 
 *  
 * BUG处理： 
 *  
 ---------------------------------------------------------------------------------------------------*/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;  
using System.Text.RegularExpressions;
using System.Data;
using System.Data.Common;
using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.IO;
using System.Reflection;

namespace XiWan.DALFactory
{
    /// <summary>
    /// 数据操作配置类
    /// </summary>
    public class SqlHelper
    {
        #region 枚举
        /// <summary>
        /// 已有的数据库对象
        /// </summary>
        public enum ExistsDataBase
        {
            /// <summary>
            /// 数据库 
            /// </summary>
            XiWanDB = 0,
            /// <summary>
            /// 数据库 
            /// </summary>
            Main = 1

        }


        /// <summary>
        /// 数据库类型
        /// </summary>
        public enum DataBaseType
        {
            /// <summary>
            /// SqlServer数据库
            /// </summary>
            SqlServer = 0,
            /// <summary>
            /// Access数据库
            /// </summary>
            Access = 1

        }
        #endregion

        #region 定义
        const DataBaseType constDbType = DataBaseType.SqlServer; //默认sql数据库
        private List<string> m_lst_SQL = null;  // 存储 SQL 语句
        private Database _DbInstance = null;
        public Database DbInstance
        {
            get
            {
                if (_DbInstance == null)
                {
                    _DbInstance = CreateDatabase();
                    return _DbInstance;
                }
                else
                {
                    return _DbInstance;
                }
            }
            set
            {
                _DbInstance = value;
            }
        }

        //private static SqlHelper _Instance;
        public static SqlHelper Instance
        {
            get
            {
                return new SqlHelper(ExistsDataBase.XiWanDB);
            }
        }
        #endregion 

        #region 类析构函数
        /// <summary>
        /// 类析构函数
        /// </summary>
        public SqlHelper()
        {
            this.m_lst_SQL = new List<string>();
        }

        public SqlHelper(ExistsDataBase Dtype)
        {
            DbInstance = CreateDatabase(Dtype);
        }
        #endregion 
        
        #region 创建数据库对象
        /// <summary>
        /// 创建数据库操作对象 默认返回 系统 数据库
        /// </summary>
        /// <returns>返回数据库对象</returns>      
        public static Database CreateDatabase()
        {
            return CreateDatabase(ExistsDataBase.XiWanDB);
        }

        /// <summary>
        /// 创建数据库操作对象
        /// </summary>
        /// <param name="strConnName">数据库名</param>
        /// <returns>返回数据库对象</returns>      
        public static Database CreateDatabase(string strConnName)
        {
            return DatabaseFactory.CreateDatabase(strConnName);
        }

        /// <summary>
        /// 创建数据库对象
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        public static Database CreateDatabase(ExistsDataBase db)
        {
            switch (db)
            {
                case ExistsDataBase.Main:
                    return DatabaseFactory.CreateDatabase("Main");
                    break;
            }
            return null;
        }
        #endregion


        #region 存储/更新/删除语句执行操作所用的方法
        /// <summary>
        /// 将一个 SQL 语句添加到存储对象中
        /// </summary>
        /// <param name="strSQL">待加入集合的 SQL 语句</param>
        /// <returns>int 在存储集合中的索引</returns>
        public int addSQL(string strSQL)
        {
            m_lst_SQL.Add(strSQL);

            return m_lst_SQL.Count;
        }

        /// <summary>
        /// 清除所有sql
        /// </summary>
        public void clear()
        {
            m_lst_SQL.Clear();
        }

        /// <summary>
        /// 执行存储中的 SQL 语句
        /// </summary>
        /// <returns>空    函数执行成功   其他  返回错误信息</returns>
        public string runSQL()
        {
            try
            {
                //Database DbInstance = SqlHelper.CreateDatabase();
                DbConnection conn = DbInstance.CreateConnection();
                conn.Open();

                DbTransaction trans = conn.BeginTransaction(IsolationLevel.ReadCommitted);

                try
                {
                    foreach (string strSQL in this.m_lst_SQL)
                    {
                        DbCommand m_Command = DbInstance.GetSqlStringCommand(strSQL);
                        DbInstance.ExecuteNonQuery(m_Command, trans);
                    }

                    trans.Commit();
                }
                catch (Exception eT)
                {
                    trans.Rollback();

                    // 记录错误SQL，便于分析处理
                    //DbErrorLog.addErrorLog(m_lst_SQL);
                    throw eT;
                }
                finally
                {
                    conn.Close();
                }

                return "";
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        #endregion


        #region 通用数据库的操作(当前默认数据库记录集操作方法——重写方法)
        /// <summary>
        ///  执行sql语句，返回IDataReader
        /// </summary>
        /// <param name="sql">SQL</param>
        /// <param name="dbType">数据库类型</param>
        /// <returns></returns>
        public IDataReader ExecuteReader(string sql)
        {

            WriteLog(sql);//输出执行语句 以便测试

            //Database db = CreateDatabase();
            return DbInstance.ExecuteReader(CommandType.Text, sql);
        }



        /// <summary>
        /// 执行数据库操作
        /// </summary>
        /// <param name="sql">SQL</param>
        /// <param name="dbType">数据库类型</param>
        public void ExecuteNonQuery(string sql)
        {
            //Database db = CreateDatabase();
            try
            {
               
                WriteLog(sql);//输出执行语句 以便测试

                DbInstance.ExecuteNonQuery(CommandType.Text, sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + sql);
            }
        }
        /// <summary>
        /// 执行数据库操作
        /// </summary>
        /// <param name="sql">SQL</param>
        /// <param name="dbType">数据库类型</param>
        public void ExecuteNonQuery(string sql,params object[] parameterValues)
        {
            //Database db = CreateDatabase();
            try
            {
                
                WriteLog(sql);//输出执行语句 以便测试
                DbInstance.ExecuteNonQuery(sql, parameterValues);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + sql);
            }
        }

        public void WriteLog(string strMsg)
        {
            //输出执行语句 以便测试
            if (CCommon.GetWebConfigValue("WriteExecuteSQLLog") == "1")
            {
                LogHelper.DoOperateLog(strMsg, "ExecuteSQL");
            }
        }

        /// <summary>
        /// 执行sql语句，返回datatable
        /// </summary>
        /// <param name="sql">SQL</param>
        /// <param name="dbType">数据库类型</param>
        /// <returns></returns>
        public DataTable ExecuteDataTable(string sql)
        {
            //Database DbInstance = CreateDatabase();
            try
            {
                return DbInstance.ExecuteDataSet(CommandType.Text, sql).Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + sql);
            }
            finally
            {
                WriteLog(sql);//输出执行语句 以便测试
            }

        }

        /// <summary>
        /// 执行sql语句，返回第一条记录信息
        /// </summary>
        /// <param name="sql">SQL</param>
        /// <param name="dbType">数据库类型</param>
        /// <returns></returns>
        public DataRow ExecuteDataRow(string sql)
        {
            //Database db = CreateDatabase();
            try
            {
                DataTable dt = DbInstance.ExecuteDataSet(CommandType.Text, sql).Tables[0];
                if (dt != null && dt.Rows.Count > 0)
                {
                    return dt.Rows[0];
                }
                return null;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + sql);
            }
            finally
            {
                WriteLog(sql);//输出执行语句 以便测试
            }

        }

        /// <summary>
        /// 执行sql语句，返回DataSet
        /// </summary>
        /// <param name="sql"></param>
        /// <returns>DataSet</returns>
        public DataSet ExecuteDataSet(string sql)
        {
            //Database DbInstance = CreateDatabase();
            try
            {
                return DbInstance.ExecuteDataSet(CommandType.Text, sql);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + sql);
            }
            finally
            {
                WriteLog(sql);//输出执行语句 以便测试
            }
        }

        /// <summary>
        /// 执行sql语句，返回IDataReader 
        /// </summary>
        /// <param name="sql"></param>
        /// <returns></returns>
        public string ExecuteScalar(string sql)
        {
            //Database db = CreateDatabase();
            try
            {
                return Convert.ToString(DbInstance.ExecuteScalar(CommandType.Text, sql));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + sql);
            }
            finally
            {
                WriteLog(sql);//输出执行语句 以便测试
            }
        }

        public object ExecuteScalarObj(string sql)
        {
            //Database db = CreateDatabase();
            return Convert.ToString(DbInstance.ExecuteScalar(CommandType.Text, sql));
            
        }
        #endregion

        #region 

        #region 批量设置某一字段信息
        /// <summary>
        /// 批量设置某一字段信息
        /// </summary>
        /// <param name="ColumnSetSql">设置的字段与值对，需符合Sql更新语句写法</param>
        /// <param name="TableName">表名</param>
        /// <param name="KeyColumnName">主键名</param>
        /// <param name="Keys">KeysId值用","分隔</param>
        /// <returns></returns>
        public bool UpdateColumnByKeyList(string ColumnSetSql, string TableName, string KeyColumnName, string Keys)
        {
            //Database db = CreateDatabase();
            string strSql = string.Format(" UPDATE {0} SET {1} where {2} in({3}) ", TableName, ColumnSetSql, KeyColumnName, Keys);
            DbCommand cmd = DbInstance.GetSqlStringCommand(strSql);
            DbInstance.ExecuteNonQuery(cmd);
            return true;
        }
        #endregion


        #region 批量删除记录信息
        /// <summary>
        /// 批量删除记录信息
        /// </summary> 
        /// <param name="TableName">表名</param>
        /// <param name="KeyColumnName">主键名</param>
        /// <param name="Keys">KeysId值用","分隔</param>
        /// <returns></returns>
        public bool DeleteRecordByKeyList(string TableName, string KeyColumnName, string Keys)
        {
            //Database db = CreateDatabase();
            string strSql = string.Format(" DELETE FROM {0} WHERE {1} in({2}) ", TableName, KeyColumnName, Keys);
            DbCommand cmd = DbInstance.GetSqlStringCommand(strSql);
            DbInstance.ExecuteNonQuery(cmd);
            return true;
        }
        #endregion

        #region 获取相关信息

        #region 根据id获取详细信息
        /// <summary>
        /// 根据主键ID,取单条记录：用于获取详细信息编辑或查看
        /// </summary>
        /// <typeparam name="T">表结构类</typeparam>
        /// <param name="ColumnKeyName">主键名</param>
        /// <param name="KeyValue">主键值</param>
        /// <returns>返回表记录的对象</returns>
        public T GetRecordDetailByID<T>(string ColumnKeyName, string KeyValue) where T : class
        {
            T _t = (T)Activator.CreateInstance(typeof(T));

            //Database db = CreateDatabase();
            string strSql = string.Format(" SELECT top 1 * FROM {0} WHERE {1}={2} ", _t.GetType().Name, ColumnKeyName, KeyValue);
            DbCommand cmd = DbInstance.GetSqlStringCommand(strSql);
            DataSet ds = DbInstance.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return SerializeHelper.DataTableToObject<T>(ds.Tables[0]).First();
            }
            return null;
        }

        /// <summary>
        /// 根据主键ID,取单条记录：用于获取详细信息编辑或查看；返回DataTable的单条记录
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="ColumnKeyName">主键名</param>
        /// <param name="KeyValue">主键值</param>
        /// <returns>返回DataTable的单条记录</returns>
        public DataTable GetRecordDetailByID(string TableName, string ColumnKeyName, string KeyValue)
        {
            //Database db = CreateDatabase();
            string strSql = string.Format(" SELECT top 1 * FROM {0} WHERE {1}={2} ", TableName, ColumnKeyName, KeyValue);
            DbCommand cmd = DbInstance.GetSqlStringCommand(strSql);
            DataSet ds = DbInstance.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return ds.Tables[0];
            }
            return null;
        }


        /// <summary>
        /// 执行SQL 是否有返回的记录，有则返回true,若无——则返回false
        /// </summary>
        /// <param name="Sql">SQL的格式大概为SELECT top 1 * FROM {0} WHERE {1}={2}</param>
        /// <returns></returns>
        public bool IsExistsRecord(string Sql)
        {
            //Database DbInstance = CreateDatabase();
            string strSql = Sql;//string.Format(" SELECT top 1 * FROM {0} WHERE {1}={2} ", TableName, ColumnKeyName, KeyValue);
            DbCommand cmd = DbInstance.GetSqlStringCommand(strSql);
            DataSet ds = DbInstance.ExecuteDataSet(cmd);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            return false;
        }
        #endregion

        #region 根据id获取某一字段的值
        /// <summary>
        /// 根据id获取名称信息
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <param name="ColumnKeyName">主键名</param>
        /// <param name="KeyValue">主键值</param>
        /// <param name="ColumnName">要获取的字段名</param>
        /// <returns>字段信息</returns>
        public string GetColumnValueByKey(string TableName, string ColumnKeyName, string KeyValue, string ColumnName)
        {
            //Database db = CreateDatabase();
            string strSql = string.Format(" SELECT top 1 {0} FROM {1} WHERE {2}={3} ", ColumnName, TableName, ColumnKeyName, KeyValue);
            DbCommand cmd = DbInstance.GetSqlStringCommand(strSql);
            return StringHelper.FilterToDefault(DbInstance.ExecuteScalar(cmd), "");
        }
        #endregion

        #endregion
        




        #endregion 

        #region sql string 辅助方法


        #region 根据表名产生一个在该表的唯一ID 语句版本
        /// <summary>
        /// 根据表名产生一个在该表的唯一ID
        /// </summary>
        /// <param name="TableName">表名</param>
        /// <returns>string</returns>
        public string GetNewID(string TableName)
        {
            if (string.IsNullOrEmpty(TableName)) return "0";

            string strReValue = "0";
            string strSql = @" UPDATE sys_IdPool SET CurID=CurID+1,UpdateTime=@UpdateTime WHERE TableName=@TableName ";
            //Database db = SqlHelper.CreateDatabase();
            DbCommand cmd = DbInstance.GetSqlStringCommand(strSql);
            DbInstance.AddInParameter(cmd, "UpdateTime", DbType.String, DateTime.Now.ToShortDateString());
            DbInstance.AddInParameter(cmd, "TableName", DbType.String, TableName);
            DbInstance.ExecuteNonQuery(cmd);

            strSql = @"SELECT TOP 1 CurID FROM sys_IdPool WHERE TableName=@TableName ";
            cmd = DbInstance.GetSqlStringCommand(strSql);
            DbInstance.AddInParameter(cmd, "TableName", DbType.String, TableName);
            try
            {
                strReValue = Convert.ToString(DbInstance.ExecuteScalar(cmd));
            }
            catch (Exception ex)
            {
                strReValue = "0";
                
            }
            if (strReValue == "0"||string.IsNullOrEmpty(strReValue))
            {
                //插入一条表记录
                strSql = string.Format("INSERT INTO sys_IdPool(TableName,BeginID,CurID,UpdateTime) VALUES('{0}',10000,10001,'{1}') ", TableName, DateTime.Now);
                DbInstance.ExecuteNonQuery(CommandType.Text, strSql);
                strReValue = "10001";
            }
            return strReValue;
            //return SqlHelper.ExecuteScalar(strSql);
        }
        #endregion


        #region 将对象表插入到数据库，并获取对象的主键
        /// <summary>
        /// 由对象直接转换成该对象表的插入Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <param name="PKeyName">表主键字段名</param>
        /// <param name="KeyType">主键获取值 1为中间表产生增加编码，2为数据库字段自动编码</param>
        /// <returns>插入Sql语句</returns>
        public void ObjectInsertToDb<T>(T EntityObj, string ColumnNames,string PKeyName,int KeyType) where T : class
        {
            if (EntityObj == null) return;
            string strSql = "";
            string strSqlS = "";
            string strSqlE = "";
            string strWhere = "";
            string[] ArrC = (ColumnNames + "|").Split('|');
            T _t = (T)Activator.CreateInstance(typeof(T));
            PropertyInfo[] propertys = _t.GetType().GetProperties();
            string strPName = "";
            object pValue = null;
            int iKeyValue = 0;

            string strTableName =  _t.GetType().Name;

            #region //获取编号，并给对象主键赋值
            if (!string.IsNullOrEmpty(PKeyName) && KeyType != 2)
            {
                iKeyValue = CConvert.ObjectToInt(GetNewID(strTableName));  
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.Name == PKeyName)
                    {
                        pi.SetValue(EntityObj, iKeyValue, null);
                        break;
                    }
                }
            }
            #endregion

            #region 组成插入语句
            foreach (PropertyInfo pi in propertys)
            {
                if (!string.IsNullOrEmpty(ColumnNames) && !ArrC.Contains(pi.Name))  //非指定更新字段，则跳过
                {
                    continue;
                }
                if (pi.Name == PKeyName && KeyType == 2)
                {
                    continue;
                }
                strPName = pi.PropertyType.FullName;
                strSqlS += pi.Name + ","; //对象属性转成字段名

                pValue = pi.GetValue(EntityObj, null);
                if (pValue == null && strPName.Contains("Nullable`1"))
                {
                    strSqlE += "NULL,";
                }
                else
                {
                    if (strPName.Contains("System.Int16")
                        || strPName.Contains( "System.Int32")
                        || strPName.Contains( "System.Int64")
                        || strPName.Contains( "System.Decimal")
                        || strPName.Contains( "System.Double")
                        || strPName.Contains("System.Boolean"))
                    {
                        strSqlE += string.Format("{0},", pValue);//对象属性转成字段名
                        strWhere += string.Format(" and {0}={1}", pi.Name, pValue);
                    }
                    else if (strPName.Contains("System.DateTime"))
                    {
                        strSqlE += string.Format("'{0}',", pValue);//对象属性转成字段名
                        strWhere += string.Format(" and {0}=#{1}#", pi.Name, pValue); 
                    }
                    else
                    {
                        strSqlE += string.Format("'{0}',", FilterSqlValue(pValue));//对象属性转成字段名
                        strWhere += string.Format(" and {0}='{1}'", pi.Name, FilterSqlValue(pValue));
                    }
                }
            }
            #endregion 

            strSql = string.Format(" INSERT INTO {0}({1}) VALUES({2})", _t.GetType().Name, strSqlS.TrimEnd(','), strSqlE.TrimEnd(','));
            ExecuteNonQuery(strSql);

            #region //非手动编写的主键，则直接从库中查出
            if (!string.IsNullOrEmpty(PKeyName) && KeyType==2)
            {
                strSql = string.Format("select top 1 {0} from {1} where 1=1 {2} order by {0} desc ", PKeyName, _t.GetType().Name, strWhere);
                object PKeyValue = ExecuteScalarObj(strSql);
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.Name == PKeyName)
                    {
                        strPName = pi.PropertyType.FullName;
                        if (strPName.Contains("System.Int"))
                        {
                            pi.SetValue(EntityObj,CConvert.ObjectToInt(PKeyValue), null);
                        }
                        else
                        {
                            pi.SetValue(EntityObj,CConvert.ObjectToString(PKeyValue), null);
                        }
                        
                        break;
                    }                     
                }
            }
            #endregion  
        }
        #endregion 

        #region 由对象直接转换成该对象表的更新Sql语句
        /// <summary>
        /// 由对象直接转换成该对象表的更新Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <returns>更新语句</returns>
        public void ObjectUpdateToDb<T>(T EntityObj, string ColumnNames, string PKeyName) where T : class
        {
            if (EntityObj == null||string.IsNullOrEmpty(PKeyName) ) return;
            string strSql = "";
            string strSqlS = "";
            string strSqlF = "";
            string[] ArrC = (ColumnNames + "|").Split('|');
            T _t = (T)Activator.CreateInstance(typeof(T));
            PropertyInfo[] propertys = _t.GetType().GetProperties();
            string strPName = "";
            object pValue = null;
            object pKeyValue = null;
            #region //获取编号，并给对象主键赋值
            if (!string.IsNullOrEmpty(PKeyName))
            {
                foreach (PropertyInfo pi in propertys)
                {
                    if (pi.Name == PKeyName)
                    {
                        pKeyValue = pi.GetValue(EntityObj, null);
                        break;
                    }
                }
            }
            if (pKeyValue == null) return;
            #endregion

            #region 更新内容字段信息
            foreach (PropertyInfo pi in propertys)
            {
                if (!string.IsNullOrEmpty(ColumnNames) && !ArrC.Contains(pi.Name))  //非指定更新字段，则跳过
                {
                    continue;
                }
                if (pi.Name == PKeyName) continue;

                strPName = pi.PropertyType.FullName;
                pValue = pi.GetValue(EntityObj, null);
                if (pValue == null && strPName.Contains("Nullable`1"))
                {
                    strSqlS += string.Format("{0}={1},", pi.Name, "NULL");
                }
                else
                {
                    if (strPName.Contains( "System.Int16")
                        || strPName.Contains( "System.Int32")
                        || strPName.Contains( "System.Int64")
                        || strPName.Contains( "System.Decimal")
                        || strPName.Contains("System.Double")
                        || strPName.Contains("System.Boolean") )
                    {
                        strSqlF = "{0}={1},";//FilterSqlValue
                        strSqlS += string.Format(strSqlF, pi.Name, pValue);
                    }
                    else if (strPName.Contains("System.DateTime"))
                    {
                        strSqlF = "{0}='{1}',";//FilterSqlValue
                        strSqlS += string.Format(strSqlF, pi.Name, pValue); 
                    }
                    else
                    {
                        strSqlF = "{0}='{1}',";
                        strSqlS += string.Format(strSqlF, pi.Name,FilterSqlValue(pValue));//对象属性转成字段名
                    }
                   
                }

            }
            #endregion 


            strSql = string.Format(" UPDATE {0} SET {1} where {2}={3} ", _t.GetType().Name, strSqlS.TrimEnd(','), PKeyName, pKeyValue) + "";
            ExecuteNonQuery(strSql);
        }
        #endregion 



        #region 由传的表 更新数据库信息，数据库中必须存在该表名
        /// <summary>
        /// 由对象直接转换成该对象表的更新Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <returns>更新语句</returns>
        public void DtableUpdateToDb(DataTable Dt, string ColumnNames, string PKeyName) 
        {
            if (Dt == null) return;//空则无更新，直接返回
            string strTableName = Dt.TableName;
            //获取该表的数据库架构信息
            string strSql = "";
            strSql = string.Format(" select * From {0} where 1=0 ",strTableName);
            DataTable DtModule = ExecuteDataTable(strSql);
            #region //若传入的主键名称为空，则使用数据库架构的值
            if (string.IsNullOrEmpty(PKeyName))
            {
                if (DtModule.PrimaryKey.Count() > 0)
                {
                    PKeyName = DtModule.PrimaryKey.First().ColumnName;
                }
                else
                {
                    return;
                }
            }
             
            DataColumn[] PColum = new DataColumn[1] { Dt.Columns[PKeyName] };
            Dt.PrimaryKey = PColum;
            #endregion 


            string strSqlS = "";
            string strSqlF = "";
            string[] ArrC = (ColumnNames + "|").Split('|');
            string strPName = "";
            object pValue = null;
            object pKeyValue = null;
            string strColumnName = "";

           

            for (int i = 0; i < Dt.Rows.Count; i++)
            {
                DataRow dr = Dt.Rows[i];

                #region //获取编号，并给对象主键赋值
                if (!string.IsNullOrEmpty(PKeyName))
                {
                    if (dr.Table.Columns.Contains(PKeyName) && DtModule.Columns.Contains(PKeyName))
                    {
                        pKeyValue = dr[PKeyName];
                    }                    
                }
                if (pKeyValue == null) continue;
                #endregion

                #region 更新字段内容信息
                for (int j = 0; j < dr.Table.Columns.Count; j++)
                {                   
                    strColumnName=dr.Table.Columns[j].ColumnName;

                    if (!string.IsNullOrEmpty(ColumnNames) && !ArrC.Contains(strColumnName))  //非指定更新字段，则跳过
                    {
                        continue;
                    }

                    if (DtModule.Columns.Contains(strColumnName))
                    {
                        if (strColumnName == PKeyName) continue; //主键不做更新
                        strPName = DtModule.Columns[strColumnName].DataType.FullName;

                        pValue = dr[j];
                        #region 判断数据类型 并组装Sql 语句段
                        if (pValue == null && strPName.Contains("Nullable`1"))
                        {
                            strSqlS += string.Format("{0}={1},", strColumnName, "NULL");
                        }
                        else
                        {
                            if (strPName.Contains("System.Int16")
                                || strPName.Contains("System.Int32")
                                || strPName.Contains("System.Int64")
                                || strPName.Contains("System.Decimal")
                                || strPName.Contains("System.Double")
                                || strPName.Contains("System.Boolean"))
                            {
                                strSqlF = "{0}={1},";//FilterSqlValue
                                strSqlS += string.Format(strSqlF, strColumnName, pValue);
                            }
                            else if (strPName.Contains("System.DateTime"))
                            {
                                strSqlF = "{0}='{1}',";//FilterSqlValue
                                strSqlS += string.Format(strSqlF, strColumnName, pValue);
                            }
                            else
                            {
                                strSqlF = "{0}='{1}',";
                                strSqlS += string.Format(strSqlF, strColumnName, FilterSqlValue(pValue));//对象属性转成字段名
                            }

                        }
                        #endregion
                    }
                }
                #endregion 

                strSql = string.Format(" UPDATE {0} SET {1} where {2}={3} ", strTableName, strSqlS.TrimEnd(','), PKeyName, pKeyValue) + "";
                ExecuteNonQuery(strSql);

            }           
            
        }
        #endregion 
        #region 将对象表插入到数据库，并获取对象的主键
        /// <summary>
        /// 由对象直接转换成该对象表的插入Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <param name="PKeyName">表主键字段名</param>
        /// <param name="KeyType">主键获取值 1为中间表产生增加编码，2为数据库字段自动编码</param>
        /// <returns>插入Sql语句</returns>
        public void DtableInsertToDb(DataTable Dt, string ColumnNames, string PKeyName, int KeyType)
        {
            if (Dt == null) return;//空则无更新，直接返回
            string strTableName = Dt.TableName;

            string strSql = "";
            string strSqlS = "";
            string strSqlE = "";
            string strWhere = "";  
            string strPName = "";
            object pValue = null;
            int iKeyValue = 0;
            string strColumnName = "";
             string[] ArrC = (ColumnNames + "|").Split('|');

            strSql = string.Format(" select * From {0} where 1=0 ", strTableName);
            DataTable DtModule = ExecuteDataTable(strSql);
            #region //若传入的主键名称为空，则使用数据库架构的值
            if (string.IsNullOrEmpty(PKeyName))
            {
                if (DtModule.PrimaryKey.Count() > 0)
                {
                    PKeyName = DtModule.PrimaryKey.First().ColumnName;
                }
                else
                {
                    return;
                }
            }
            if (!Dt.Columns.Contains(PKeyName))  //增加主键字段
            {
                Dt.Columns.Add(new DataColumn(PKeyName,typeof(System.Int32)) );
                for (int i = 0; i < Dt.Rows.Count; i++)
                {
                    Dt.Rows[i][PKeyName] = i+1;
                }
            }

            DataColumn[] PColum = new DataColumn[1] { Dt.Columns[PKeyName] };
            Dt.PrimaryKey = PColum;
            #endregion 
            
            #region 组成插入语句
            for (int i = 0; i < Dt.Rows.Count; i++)
            {
                DataRow dr = Dt.Rows[i];

                #region //获取编号，并给对象主键赋值
                if (!string.IsNullOrEmpty(PKeyName) && KeyType != 2 && DtModule.Columns.Contains(PKeyName))
                {
                    iKeyValue = CConvert.ObjectToInt(GetNewID(strTableName));
                    dr[PKeyName] = iKeyValue;                     
                }
                #endregion
               
                #region 更新字段内容信息
                for (int j = 0; j < dr.Table.Columns.Count; j++)
                {
                    strColumnName = dr.Table.Columns[j].ColumnName;

                    if (!string.IsNullOrEmpty(ColumnNames) && !ArrC.Contains(strColumnName))  //非指定更新字段，则跳过
                    {
                        continue;
                    }
                    if (KeyType == 2 && strColumnName == PKeyName)  //主键字段 若为自动增长的，则跳过
                    {
                        continue;
                    }

                    if (DtModule.Columns.Contains(strColumnName))
                    {
                        if (strColumnName == PKeyName) continue; //主键不做更新
                        strPName = DtModule.Columns[strColumnName].DataType.FullName;

                        pValue = dr[j];
                        #region 判断数据类型 并组装Sql 语句段

                        strSqlS += strColumnName + ","; //对象属性转成字段名
 
                        if (pValue == null && strPName.Contains("Nullable`1"))
                        {
                            strSqlE += "NULL,";
                        }
                        else
                        {
                            if (strPName.Contains("System.Int16")
                                || strPName.Contains("System.Int32")
                                || strPName.Contains("System.Int64")
                                || strPName.Contains("System.Decimal")
                                || strPName.Contains("System.Double")
                                || strPName.Contains("System.Boolean"))
                            {
                                strSqlE += string.Format("{0},", pValue);//对象属性转成字段名
                                strWhere += string.Format(" and {0}={1}", strColumnName, pValue);
                            }
                            else if (strPName.Contains("System.DateTime"))
                            {
                                strSqlE += string.Format("'{0}',", pValue);//对象属性转成字段名
                                strWhere += string.Format(" and {0}=#{1}#", strColumnName, pValue);
                            }
                            else
                            {
                                strSqlE += string.Format("'{0}',", FilterSqlValue(pValue));//对象属性转成字段名
                                strWhere += string.Format(" and {0}='{1}'", strColumnName, FilterSqlValue(pValue));
                            }
                        }                         
                        #endregion
                    }
                }
                #endregion

                strSql = string.Format(" INSERT INTO {0}({1}) VALUES({2})", strTableName, strSqlS.TrimEnd(','), strSqlE.TrimEnd(','));
                ExecuteNonQuery(strSql);

                #region //非手动编写的主键，则直接从库中查出
                if (!string.IsNullOrEmpty(PKeyName) && KeyType == 2)
                {
                    strSql = string.Format("select top 1 {0} from {1} where 1=1 {2} order by {0} desc ", PKeyName, strTableName, strWhere);
                    object PKeyValue = ExecuteScalarObj(strSql);
                   
                    dr[PKeyName] = CConvert.ObjectToInt(PKeyValue);
                    
                }
                #endregion
            }             
            #endregion            
        }
        #endregion 


        #region 查询一条记录，并返回对象
        public T GetObjectFromFirstRecord<T>(string strSql) where T : class
        {             
            DataSet ds = ExecuteDataSet(strSql);
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                return SerializeHelper.DataTableToObject<T>(ds.Tables[0]).First();
            }
            return null;
        }
        #endregion 

        #region 表对象转换成相应插入或更新语句
        /// <summary>
        /// 由对象直接转换成该对象表的插入Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <returns>插入Sql语句</returns>
        public static string ObjectToInsertSql<T>(T EntityObj, string ColumnNames) where T : class
        {
            if (EntityObj == null) return "";
            string strSql = "";
            string strSqlS = "";
            string strSqlE = "";
            string[] ArrC = (ColumnNames + "|").Split('|');
            T _t = (T)Activator.CreateInstance(typeof(T));
            PropertyInfo[] propertys = _t.GetType().GetProperties();
            string strPName = "";
            object pValue = null;
            foreach (PropertyInfo pi in propertys)
            {
                if (!string.IsNullOrEmpty(ColumnNames) && !ArrC.Contains(pi.Name))  //非指定更新字段，则跳过
                {
                    continue;
                }
                strPName = pi.PropertyType.FullName;
                if (strPName.Contains("EntitySet"))  //集合，非字段
                {
                    continue;
                }

                strSqlS += pi.Name + ","; //对象属性转成字段名

                pValue = pi.GetValue(EntityObj, null);
                if (pValue == null && strPName.Contains("Nullable`1"))
                {
                    strSqlE += "NULL,";
                }
                else
                {
                    if (strPName.Contains( "System.Int16")
                        || strPName.Contains( "System.Int32")
                        || strPName.Contains( "System.Int64")
                        || strPName.Contains( "System.Decimal")
                        || strPName.Contains( "System.Double")
                        || strPName.Contains( "System.Boolean"))
                    {
                        strSqlE += string.Format("{0},", pValue);//对象属性转成字段名 
                    }
                    else 
                    {
                        strSqlE += string.Format("'{0}',", pValue);//对象属性转成字段名
                    }
                }
            }
            strSql = string.Format(" INSERT INTO {0}({1}) VALUES({2})", _t.GetType().Name, strSqlS.TrimEnd(','), strSqlE.TrimEnd(','));
            return strSql;
        }

        /// <summary>
        /// 由对象直接转换成该对象表的更新Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <returns>更新语句</returns>
        public static string ObjectToUpdateSql<T>(T EntityObj, string ColumnNames) where T : class
        {
            if (EntityObj == null) return "";
            string strSql = "";
            string strSqlS = "";
            string strSqlF = "";
            string[] ArrC = (ColumnNames + "|").Split('|');
            T _t = (T)Activator.CreateInstance(typeof(T));
            PropertyInfo[] propertys = _t.GetType().GetProperties();
            string strPName = "";
            object pValue = null;

            foreach (PropertyInfo pi in propertys)
            {
                if (!string.IsNullOrEmpty(ColumnNames) && !ArrC.Contains(pi.Name))  //非指定更新字段，则跳过
                {
                    continue;
                }
                strPName = pi.PropertyType.FullName;
                if (strPName.Contains("EntitySet"))  //集合，非字段
                {
                    continue;
                }

                pValue = pi.GetValue(EntityObj, null);
                if (pValue == null && strPName.Contains("Nullable`1"))
                {
                    strSqlS += string.Format("{0}={1},", pi.Name, "NULL");
                }
                else
                {
                    if (strPName.Contains( "System.Int16")
                        || strPName.Contains( "System.Int32")
                        || strPName.Contains( "System.Int64")
                        || strPName.Contains( "System.Decimal")
                        || strPName.Contains( "System.Double")
                        || strPName.Contains( "System.Boolean"))
                    {
                        strSqlF = "{0}={1},";
                    }
                    else 
                    {
                        strSqlF = "{0}='{1}',";
                    }
                    strSqlS += string.Format(strSqlF, pi.Name, pValue);//对象属性转成字段名
                }
                
            }
            strSql = string.Format(" UPDATE {0} SET {1}", _t.GetType().Name, strSqlS.TrimEnd(','));
            return strSql;
        }
        #endregion

        #region 格式化 sql 字符串为指定规格
        /// <summary>
        /// 格式化 sql 字符串为指定规格
        /// </summary>
        /// <param name="str">字符串</param>
        /// <param name="type">
        /// <para>转换类型</para>
        /// <para>1、 把[1,2,3,4,5,6,7]转换为['1','2','3'] 符合sql的in('a','b','c')格式</para>
        /// </param>
        /// <returns></returns>
        public static string FormatSqlString(string str, int type)
        {
            string s = "";
            if (str != null && str.Trim() != "")
            {
                string[] tmp = str.Split(',');
                for (int i = 0; i < tmp.Length; i++)
                {
                    s += string.Format("'{0}',", tmp[i].Trim());
                }

                if (s.Trim() != "") s = s.Substring(0, s.Length - 1);
            }

            return s;
        }
        #endregion

        #region 过滤SQL的非法参数值  防止注入漏洞
        /// <summary>
        /// 过滤SQL的参数值  防止注入漏洞
        /// </summary>
        /// <param name="sqlValue"></param>
        /// <returns></returns>
        public static string FilterSqlValue(object SValue)
        {

            if (SValue == null || SValue.ToString().Trim() == "")
                return "";
            string sqlValue = SValue.ToString();
            //sqlValue = sqlValue.Replace("'","''");
            //hxw 9.15 
            //过滤sql注入and|exec|insert|select|delete|update|count|*|%|chr|mid|master|truncate|char|declare 
            sqlValue = Regex.Replace(sqlValue, "--", "——", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, " or ", " or_ ", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, " and ", " and_ ", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, "exec ", "exec_", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, "execute ", "execute_ ", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, "insert ", "insert_ ", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, "select ", "select_ ", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, "delete ", "delete_ ", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, "update ", "update_ ", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, "xp_cmdshell", "xp_cmdshell_", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, " count", " count_", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, "truncate ", "truncate_ ", RegexOptions.IgnoreCase);

            //sqlValue = Regex.Replace(sqlValue,@"\*","x",RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, "chr", "ch_r", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, "mid", "mi_d", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, "master ", "master_ ", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, "declare ", "declare_ ", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, "'", "''", RegexOptions.IgnoreCase);
            //sqlValue = Regex.Replace(sqlValue, ";", "；", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, @"\(", "（", RegexOptions.IgnoreCase);
            sqlValue = Regex.Replace(sqlValue, @"\)", "）", RegexOptions.IgnoreCase);


            return sqlValue;
        }
        #endregion

        #endregion




    }
}
