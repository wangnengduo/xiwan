﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Data.OracleClient;

namespace XiWan.DALFactory
{
    [Serializable]
    public class EntityBase
    {
        #region 初始化
        protected EntityBase() : this(e_EntityState.Added)
        {
        }

        protected EntityBase(e_EntityState state)
        {
            SetEntityState(state);
        }

        static EntityBase()
        {
            // 获取数据类型
            LoadDataType();            
        }

        // 获取数据类型
        private static void LoadDataType()
        {
            System.IO.Stream stream = null;
            DataSet dst = new DataSet();
            string strPath = "DALFactory.Entities.DataType.xml";

            try
            {
                stream = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream(strPath);
                dst.ReadXml(stream);
                m_dtbDataTypeStatic = dst.Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception(strPath + "找不到!", ex);
            }
        }

        protected internal void SetEntityState(e_EntityState state)
        {
            m_entityState = state;            
        }
        #endregion

        #region 变量
        private static e_ControllerType m_controllerType = e_ControllerType.Sql;
        protected e_EntityState m_entityState = e_EntityState.Invalid;
        protected Hashtable m_listModified = null;
        protected bool m_blnRecordModified = true;

        [NonSerialized]
        protected Dictionary<string, object> m_listAdded = null;

        protected static DataTable m_dtbDataTypeStatic = null;
        private bool m_blnRecordAdded = false;

        #endregion

        #region 属性
        public static e_ControllerType ControllerType
        {
            get { return m_controllerType; }
            set { m_controllerType = value; }
        }

        /// <summary>
        /// 是否记录添加实体
        /// </summary>
        public bool IsRecordAdded
        {
            get { return m_blnRecordAdded; }
            set { m_blnRecordAdded = value; }
        }
        /// <summary>
        /// 实体添加值
        /// </summary>        
        public Dictionary<string, object> AddedList
        {
            get { return m_listAdded; }
            set { m_listAdded = value; }
        }

        /// <summary>
        /// 实体状态。
        /// </summary>
        public e_EntityState EntityState
        {
            get { return m_entityState; }
            set { m_entityState = value; }
        }

        /// <summary>
        /// 值被更改过的字段集合。
        /// </summary>
        public Hashtable ModifiedList
        {
            get { return m_listModified; }
            set { m_listModified = value; }
        }

        /// <summary>
        /// 值被更改过的字段名称。字段之间用逗号隔开。
        /// </summary>
        public string ModifiedColumnsName
        {
            get
            {
                string strColumns = "";
                if (m_listModified != null)
                {
                    IDictionaryEnumerator enumerator = this.ModifiedList.GetEnumerator();
                    while (enumerator.MoveNext())
                    {
                        strColumns += enumerator.Key.ToString() + ",";
                    }
                    strColumns = strColumns.Remove(strColumns.Length - 1);
                }
                return strColumns;
            }
        }

        /// <summary>
        /// 实体中字段的值是否被更改。任意一个字段被改过，都返回True。
        /// </summary>
        public bool HasChange
        {
            get 
            {
                if (m_entityState == e_EntityState.Added || (m_listModified != null && m_listModified.Count > 0))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        
        /// <summary>
        /// 实体的类型。
        /// </summary>
        public virtual Type Type
        {
            get { return null; }
        }
        
        /// <summary>
        /// 实体的表结构信息。
        /// </summary>
        public virtual DataTable Schema
        {
            get { return null; }
        }

        /// <summary>
        /// 实体的扩展字段集合。
        /// </summary>
        public virtual Hashtable ExtendedList
        {
            get { return null; }
        }

        /// <summary>
        /// 实体的字段对应的参数集合。
        /// </summary>
        public virtual Hashtable Parameters
        {
            get { return null; }
        }

        /// <summary>
        /// 实体的字段名，字段之间用逗号隔开。
        /// </summary>
        public virtual string ColumnsName
        {
            get { return null; }
        }

        /// <summary>
        /// 实体的扩展字段名，字段之间用逗号隔开。
        /// </summary>
        public virtual string ExtendedColumnsName
        {
            get { return null; }
        }

        /// <summary>
        /// 实体主键的字段名，字段之间用逗号隔开。
        /// </summary>
        public virtual string IdentityColumnName
        {
            get { return null; }
        }

        public virtual string Sql_Where
        {
            get { return null; }
        }

        /// <summary>
        /// 实体的主键值。
        /// </summary>
        public virtual object[] KeyValues
        {
            get
            {
                return GetKeyValues();
            }

        }
        #endregion

        #region 参数
        public IDataParameter[] GetAllParameters()
        {
            return DataTableHelper.Instance.GetParameters(this.Parameters, null,null);
        }

        public IDataParameter[] GetKeyParameters()
        {
            return DataTableHelper.Instance.GetParameters(this.Parameters, null, this.Schema.PrimaryKey);
        }

        public IDataParameter[] GetModifiedParameters()
        {
            return DataTableHelper.Instance.GetParameters(this.Parameters, this.ModifiedList.Keys,null);
        }

        public IDataParameter[] GetUpdateParameters()
        {
            return DataTableHelper.Instance.GetParameters(this.Parameters, this.ModifiedList.Keys, this.Schema.PrimaryKey);
        }
        #endregion

        #region 读取/设置属性值
        protected bool CheckInstance()
        {
            if (m_entityState == e_EntityState.Invalid)
            {
                throw (new Exception("Check e_EntityState.Invalid"));
            }
            return true;
        }

        //protected int GetValue(string key, int value)
        //{
        //    return value;
        //}

        /// <summary>
        /// 获取字段值。
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public object GetValue(string column)
        {
            PropertyInfo p = this.Type.GetProperty(column);
            object objValue = null;

            if (p != null)
            {
                objValue = p.GetValue(this, null);
            }

            return objValue;
        }

        /// <summary>
        /// 获取所有字段的值。
        /// </summary>
        /// <returns></returns>
        public object[] GetValues()
        {
            //TODO 不通过反射操作，直接生成代码
            object[] objValues = new object[this.Schema.Columns.Count];
            PropertyInfo p = null;
            DataColumn dcl = null;

            for (int i = 0; i < this.Schema.Columns.Count; i++)
            {
                dcl = this.Schema.Columns[i];
                p = this.Type.GetProperty(dcl.ColumnName);
                objValues[i] = p.GetValue(this, null);
            }

            return objValues;
        }

        /// <summary>
        /// 获取字段更改前的值。返回null表示字段没有更改。
        /// </summary>
        /// <param name="column"></param>
        /// <returns></returns>
        public object GetOriginalValue(string column)
        {
            if (m_listModified == null)
            {
                return null;
            }
            else
            {
                return m_listModified[column];
            }
        }

        // 获取主键值
        protected object[] GetKeyValues()
        {
            object[] objValues = new object[this.Schema.PrimaryKey.Length];
            PropertyInfo p = null;
            DataColumn dcl = null;

            if (HasChange)
            {
                for (int i = 0; i < this.Schema.PrimaryKey.Length; i++)
                {
                    dcl = this.Schema.PrimaryKey[i];
                    if (this.ModifiedList != null && this.ModifiedList.Contains(dcl.ColumnName))
                    {
                        objValues[i] = this.ModifiedList[dcl.ColumnName];
                    }
                    else
                    {
                        p = this.Type.GetProperty(dcl.ColumnName);
                        objValues[i] = p.GetValue(this, null);
                    }
                }
            }
            else
            {
                for (int i = 0; i < this.Schema.PrimaryKey.Length; i++)
                {
                    dcl = this.Schema.PrimaryKey[i];
                    p = this.Type.GetProperty(dcl.ColumnName);
                    objValues[i] = p.GetValue(this, null);
                }
            }

            return objValues;
        }        

        /// <summary>
        /// 设置字段的值。
        /// </summary>
        /// <param name="column"></param>
        /// <param name="value"></param>
        public void SetValue(string column, object value)
        {
            //TODO 默认值 null处理
            //TODO 检查长度
            object objValue = value;
            PropertyInfo p = this.Type.GetProperty(column);

            if (p != null)
            {
                switch (p.PropertyType.FullName)
                {
                    case "System.Int32":
                        if (value != null && value.ToString().Length > 0)
                        {
                            p.SetValue(this, System.Convert.ToInt32(value), null);
                        }
                        break;
                    case "System.Decimal":
                        if (value != null && value.ToString().Length > 0)
                        {
                            p.SetValue(this, System.Convert.ToDecimal(value), null);
                        }                        
                        break;
                    case "System.DateTime":
                        if (value != null && value.ToString().Length > 0)
                        {
                            p.SetValue(this, System.Convert.ToDateTime(value), null);
                        }                                               
                        break;
                    case "System.Boolean":
                        if (objValue==null || objValue.ToString() == string.Empty || objValue.ToString() == "0" )
                        {
                            objValue = "False";
                        }
                        else if (objValue.ToString() == "1")
                        {
                            objValue = "True";
                        }
                        p.SetValue(this, System.Convert.ToBoolean(objValue), null);
                        break;
                    case "System.Int64":
                        if (value != null && value.ToString().Length > 0)
                        {
                            p.SetValue(this, System.Convert.ToInt64(value), null);
                        }                                              
                        break;
                    case "System.Int16":
                        if (value != null && value.ToString().Length > 0)
                        {
                            p.SetValue(this, System.Convert.ToInt16(value), null);
                        }                                              
                        break;
                    case "System.Double":
                        if (value != null && value.ToString().Length > 0)
                        {
                            p.SetValue(this, System.Convert.ToDouble(value), null);
                        }
                        break;
                    case "System.Single":
                        if (value != null && value.ToString().Length > 0)
                        {
                            p.SetValue(this, System.Convert.ToSingle(value), null);
                        }                        
                        break;
                    case "System.Char":
                        p.SetValue(this, System.Convert.ToChar(value), null);
                        break;
                    case "System.Byte":
                        p.SetValue(this, System.Convert.ToByte(value), null);
                        break;                   

                    default:
                        p.SetValue(this, value, null);
                        break;
                }                
            }
        }
        /// <summary>
        /// 新增字段时 ,记录字段值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private void AddNew(string key, object value)
        {
            if (m_entityState == e_EntityState.Added)
            {
                if (m_listAdded == null)
                {
                    m_listAdded = new Dictionary<string, object>();
                }
                m_listAdded.Add(Convert.ToString(key), value);
            }
        }

        /// <summary>
        /// 更改字段时，记录字段最初的值。
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        private void AddOriginal(object key, object value)
        {
            if (!m_blnRecordModified)
            {
                return;
            }
            if (m_entityState != e_EntityState.Modified && m_entityState != e_EntityState.Unchanged)
            {
                return;
            }
            if (m_entityState == e_EntityState.Unchanged)
            {
                m_entityState = e_EntityState.Modified;
            }

            if (m_listModified == null)
            {
                m_listModified = new Hashtable();
            }
            if (!m_listModified.Contains(key))
            {
                m_listModified.Add(key, value);
            }            
        }
        /// <summary>
        /// 更改字段时，记录字段最初的值。
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        protected void AddOriginal(object key, object valueOriginal, object value)
        {
            if (m_entityState == e_EntityState.Added && m_blnRecordAdded)
            {
                string keyField = Convert.ToString(key);
                if (!Renew(key, valueOriginal, value))
                {
                    if (m_listAdded == null)
                    {
                        AddNew(keyField, value);
                    }
                    else
                    {
                        if (m_listAdded.FirstOrDefault(p => p.Key == keyField).Key == null)
                        {
                            AddNew(keyField, value);
                        }
                        else
                        {
                            m_listAdded[keyField] = value;
                        }
                    }
                }
            }
            else if (m_entityState == e_EntityState.Modified || m_entityState == e_EntityState.Unchanged)
            {
                if (m_listModified == null)
                {
                    if (!Renew(key, valueOriginal, value))
                    {
                        AddOriginal(key, valueOriginal);
                    }
                }
                else
                {
                    object original = m_listModified[key];
                    if (original == null)
                    {
                        if (!Renew(key, valueOriginal, value))
                        {
                            AddOriginal(key, valueOriginal);
                        }
                    }
                    else
                    {
                        if (Renew(key, original, value))
                        {
                            m_listModified.Remove(key);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// 是否更新
        /// </summary>
        /// <returns></returns>
        private bool Renew(object key, object valueOriginal, object value)
        {
            PropertyInfo p = this.Type.GetProperty(key.ToString());
            bool result = false;
            switch (p.PropertyType.FullName)
            {
                case "System.String":
                    result = Compare<string>(valueOriginal, value);
                    break;
                case "System.Int32":
                    result = Compare<int>(valueOriginal, value);
                    break;
                case "System.Int64":
                    result = Compare<long>(valueOriginal, value);
                    break;
                case "System.Int16":
                    result = Compare<int>(valueOriginal, value);
                    break;
                case "System.Byte":
                    result = Compare<Byte>(valueOriginal, value);
                    break;
                case "System.Boolean":
                    result = Compare<bool>(valueOriginal, value);
                    break;
                case "System.Decimal":
                    result = Compare<decimal>(valueOriginal, value);
                    break;
                case "System.Single":
                    result = Compare<float>(valueOriginal, value);
                    break;
                case "System.Double":
                    result = Compare<double>(valueOriginal, value);
                    break;
                case "System.DateTime":
                    result = Compare<DateTime>(valueOriginal, value);
                    break;
                case "System.Object":
                    result = Compare<Object>(valueOriginal, value);
                    break;
                case "System.Byte[]":
                    result = Compare<Byte[]>(valueOriginal, value);
                    break;
                default:
                    throw (new Exception("无法识别的数据类型[" + p.PropertyType.FullName + "]。"));
            }
            return result;
        }

        private bool Compare<T>(object valueOriginal, object value)
        {
            if (valueOriginal == DBNull.Value)
            {
                valueOriginal = null;
            }
            if (value == DBNull.Value)
            {
                value = null;
            }
            if (valueOriginal == null && value == null)
            {
                return true;
            }
            else if ((valueOriginal == null && value != null) || (valueOriginal != null && value == null))
            {
                return false;
            }
            T originalValue = (T)valueOriginal;
            T newValue = (T)value;
            return originalValue.Equals(newValue);
        }

        /// <summary>
        /// 设置所有字段的值。不记录更改状态。
        /// </summary>
        /// <param name="values"></param>
        public void SetValues(object[] values)
        {
            PropertyInfo p = null;
            DataColumn dcl = null;

            try
            {
                m_blnRecordModified = false;
                for (int i = 0; i < this.Schema.Columns.Count; i++)
                {
                    dcl = this.Schema.Columns[i];
                    SetValue(dcl.ColumnName, values[i]);
                    //p = this.Type.GetProperty(dcl.ColumnName);
                    //p.SetValue(this, values[i], null);
                }

                // 扩展属性
                if (this.ExtendedColumnsName.Length > 0)
                {
                    string[] arrExt = this.ExtendedColumnsName.Split(',');

                    if (values.Length == this.Schema.Columns.Count + arrExt.Length)
                    {
                        for (int i = 0; i < arrExt.Length; i++)
                        {
                            p = this.Type.GetProperty(arrExt[i]);
                            if (!Convert.IsDBNull(values[this.Schema.Columns.Count + i]))
                            {
                                SetValue(arrExt[i], values[this.Schema.Columns.Count + i]);
                                //p.SetValue(this, values[this.Schema.Columns.Count + i], null);
                            }
                        }
                    }
                }
            }
            finally
            {
                m_blnRecordModified = true;
            }
        }

        /// <summary>
        /// 设置实体的值。
        /// </summary>
        /// <param name="dr"></param>
        public void SetValues(IDataReader dr)
        {
            //TODO 检查字段是否匹配，不匹配的按名称赋值 考虑仅返回部分字段的情况
            PropertyInfo p = null;
            DataColumn dcl = null;

            try
            {
                m_blnRecordModified = false;
                for (int i = 0; i < this.Schema.Columns.Count; i++)
                {
                    dcl = this.Schema.Columns[i];

                    if (!System.Convert.IsDBNull(dr[i]))
                    {
                        SetValue(dcl.ColumnName, dr[i]);
                        //p = this.Type.GetProperty(dcl.ColumnName);
                        //p.SetValue(this, dr[i], null);
                    }
                }

                // 扩展属性
                if (this.ExtendedColumnsName.Length > 0)
                {
                    string[] arrExt = this.ExtendedColumnsName.Split(',');

                    if (dr.FieldCount == this.Schema.Columns.Count + arrExt.Length)
                    {
                        for (int i = 0; i < arrExt.Length; i++)
                        {
                            p = this.Type.GetProperty(arrExt[i]);
                            if (!Convert.IsDBNull(dr[this.Schema.Columns.Count + i]))
                            {
                                SetValue(arrExt[i], dr[this.Schema.Columns.Count + i]);
                                //p.SetValue(this, dr[this.Schema.Columns.Count + i], null);
                            }
                        }
                    }
                }
            }
            finally
            {
                m_blnRecordModified = true;
            }            
        }

        /// <summary>
        /// 设置实体的值。
        /// </summary>
        /// <param name="drw"></param>
        public void SetValues(DataRow drw)
        {
            //TODO 检查字段是否匹配，不匹配的按名称赋值 考虑仅返回部分字段的情况
            PropertyInfo p = null;

            try
            {
                m_blnRecordModified = false;
                foreach (DataColumn dcl in this.Schema.Columns)
                {
                    p = this.Type.GetProperty(dcl.ColumnName);
                    if (drw.Table.Columns.Contains(dcl.ColumnName))
                    {
                        if (!System.Convert.IsDBNull(drw[dcl.ColumnName]))
                        {
                            SetValue(dcl.ColumnName, drw[dcl.ColumnName]);
                            //p.SetValue(this, drw[dcl.ColumnName], null);
                        }
                    }
                }

                // 扩展属性
                if (this.ExtendedColumnsName.Length > 0)
                {
                    string[] arrExt = this.ExtendedColumnsName.Split(',');

                    if (drw.Table.Columns.Count == this.Schema.Columns.Count + arrExt.Length)
                    {
                        for (int i = 0; i < arrExt.Length; i++)
                        {
                            p = this.Type.GetProperty(arrExt[i]);
                            if (!Convert.IsDBNull(drw[this.Schema.Columns.Count + i]))
                            {
                                SetValue(arrExt[i], drw[this.Schema.Columns.Count + i]);
                                //p.SetValue(this, drw[this.Schema.Columns.Count + i], null);
                            }
                        }
                    }
                }
            }
            finally
            {
                m_blnRecordModified = true;
            }             
        }
        #endregion

        #region 数据类型
        internal static Hashtable BuildParameters(DataTable dtb)
        {
            Hashtable list = new Hashtable();

            foreach (DataColumn dcl in dtb.Columns)
            {
                list.Add(dcl.ColumnName,NewParameter(dcl));
            }

            foreach (DataColumn dcl in dtb.PrimaryKey)
            {
                list.Add(dcl.ColumnName + "_Where", NewParameter(dcl.ColumnName + "_Where",dcl.DataType.Name));
            }

            return list;
        }

         internal static IDataParameter NewParameter(DataColumn dcl)
        {
            return NewParameter(dcl.ColumnName, dcl.DataType.Name);
        }

        public static IDataParameter NewParameter(string columnName, string dataTypeName)
        {
            IDataParameter prm = null;

            if (m_controllerType == e_ControllerType.Oracle)
            {
                prm = new System.Data.OracleClient.OracleParameter(columnName, GetOracleDbType(dataTypeName));
            }
            else
            {
                prm = new System.Data.SqlClient.SqlParameter("@" + columnName, GetSqlDbType(dataTypeName));
            }

            return prm;
        }

        private static string SqlDbTypeToDotnetType(string dataType)
        {
            string strDataType = dataType.Substring(0, 1).ToUpper() + dataType.Substring(1);
            DataRow[] adrw = m_dtbDataTypeStatic.Select("SqlDbType=\'" + strDataType + "\'");
            string strValue = "";

            if (adrw.Length > 0)
            {
                strValue = adrw[0]["Type"].ToString();
            }
            else
            {
                throw (new Exception("找不到数据类型[" + dataType + "]"));
            }

            return strValue;
        }

        private static string SqlDbTypeToSqlDbType(string dataType)
        {
            string strDataType = dataType.Substring(0, 1).ToUpper() + dataType.Substring(1);
            DataRow[] adrw = m_dtbDataTypeStatic.Select("SqlDbType=\'" + strDataType + "\'");
            string strValue = "";

            if (adrw.Length > 0)
            {
                strValue = adrw[0]["SqlDbType"].ToString();
            }
            else
            {
                throw (new Exception("找不到数据类型[" + dataType + "]"));
            }

            return strValue;
        }

        private static bool IsNumeric(string typeName)
        {
            if ((typeName == "Decimal") || (typeName == "Int16") || (typeName == "Int32") || (typeName == "Int64") || (typeName == "Single") || (typeName == "Double"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private static string GetSqlDbTypeName(string adonetTypeName)
        {
            DataRow[] adrw = m_dtbDataTypeStatic.Select("Type=\'" + adonetTypeName + "\'");
            if (adrw.Length > 0)
            {
                return adrw[0]["SqlDbType"].ToString();
            }
            else
            {
                return "";
            }
        }

        private static Type GetAdonetType(string sqlDbTypeName)
        {
            string sSqlDbTypeName = sqlDbTypeName.Substring(0, 1).ToUpper() + sqlDbTypeName.Substring(1);
            DataRow[] adrw = m_dtbDataTypeStatic.Select("SqlDbType=\'" + sSqlDbTypeName + "\'");
            if (adrw.Length > 0)
            {
                return Type.GetType(adrw[0]["Type"].ToString());
            }
            else
            {
                return null;
            }
        }

        private static OracleType GetOracleDbType(string typeName)
        {
            OracleType typ = OracleType.NVarChar;
            string sOracleTypeName = "";

            if (typeName.IndexOf("System.") == -1)
            {
                typeName = "System." + typeName;
            }
            DataRow[] adrw = m_dtbDataTypeStatic.Select("Type=\'" + typeName + "\'");

            if (adrw.Length > 0)
            {
                sOracleTypeName = adrw[0]["SqlDbType"].ToString();
                switch (sOracleTypeName)
                {
                    case "NVarChar":
                        typ = OracleType.NVarChar;
                        break;
                    case "VarChar":
                        typ = OracleType.VarChar;
                        break;
                    case "TinyInt":
                        typ = OracleType.Int16;
                        break;
                    case "SmallInt":
                        typ = OracleType.Int16;
                        break;
                    case "Int":
                        typ = OracleType.Int32;
                        break;
                    case "BigInt":
                        typ = OracleType.Int32;
                        break;
                    case "Bit":
                        typ = OracleType.Byte;
                        break;
                    case "Decimal":
                        typ = OracleType.Number;
                        break;
                    case "Float":
                        typ = OracleType.Float;
                        break;
                    case "Real":
                        typ = OracleType.Double;
                        break;
                    case "DateTime":
                        typ = OracleType.DateTime;
                        break;
                    case "Guid":
                        typ = OracleType.VarChar;
                        break;
                    case "Variant":
                        typ = OracleType.VarChar;
                        break;
                    case "Binary":
                        typ = OracleType.Blob;
                        break;
                    case "Image":
                        typ = OracleType.Blob;
                        break;
                    case "Timestamp":
                        typ = OracleType.Timestamp;
                        break;
                }
            }

            return typ;
        }

        private static SqlDbType GetSqlDbType(string typeName)
        {
            SqlDbType typ = SqlDbType.Variant;
            string sSqlDbTypeName = "";

            if (typeName.IndexOf("System.") == -1)
            {
                typeName = "System." + typeName;
            }
            DataRow[] adrw = m_dtbDataTypeStatic.Select("Type=\'" + typeName + "\'");

            if (adrw.Length > 0)
            {
                sSqlDbTypeName = adrw[0]["SqlDbType"].ToString();
                switch (sSqlDbTypeName)
                {
                    case "NVarChar":
                        typ = SqlDbType.NVarChar;
                        break;
                    case "VarChar":
                        typ = SqlDbType.VarChar;
                        break;
                    case "TinyInt":
                        typ = SqlDbType.TinyInt;
                        break;
                    case "SmallInt":
                        typ = SqlDbType.SmallInt;
                        break;
                    case "Int":
                        typ = SqlDbType.Int;
                        break;
                    case "BigInt":
                        typ = SqlDbType.BigInt;
                        break;
                    case "Bit":
                        typ = SqlDbType.Bit;
                        break;
                    case "Decimal":
                        typ = SqlDbType.Decimal;
                        break;
                    case "Float":
                        typ = SqlDbType.Float;
                        break;
                    case "Real":
                        typ = SqlDbType.Real;
                        break;
                    case "DateTime":
                        //typ = SqlDbType.DateTime;
                        typ = SqlDbType.NVarChar;
                        break;
                    case "Guid":
                        typ = SqlDbType.UniqueIdentifier;
                        break;
                    case "Variant":
                        typ = SqlDbType.Variant;
                        break;
                    case "Binary":
                        typ = SqlDbType.Binary;
                        break;
                    case "Image":
                        typ = SqlDbType.Image;
                        break;
                    case "Timestamp":
                        typ = SqlDbType.Timestamp;
                        break;
                }
            }

            return typ;
        }

        private static object GetDataTypeDefaultValue(string datatype)
        {
            object objValue = null;
            switch (datatype)
            {
                case "String":
                    objValue = string.Empty;
                    break;
                case "Int32":
                    objValue = 0;
                    break;
                case "Int64":
                    objValue = 0;
                    break;
                case "Int16":
                    objValue = 0;
                    break;
                case "Byte":
                    objValue = 0;
                    break;
                case "Boolean":
                    objValue = false;
                    break;
                case "Decimal":
                    objValue = 0.0m;
                    break;
                case "Single":
                    objValue = 0.0;
                    break;
                case "Double":
                    objValue = 0.0;
                    break;
                case "DateTime":
                    //todo 取消默认1900-01-01
                    objValue = new DateTime(1900,1,1);
                    break;
                case "Guid":
                    objValue = Guid.Empty;
                    break;
                case "Object":
                    objValue = null;
                    break;
                case "Byte[]":
                    objValue = null;
                    break;
                default:
                    throw (new Exception("无法识别的数据类型[" + datatype + "]。"));
            }
            return objValue;
        }

        private static string GetDataTypeDefaultValueString(string datatype)
        {
            string strValue = null;
            switch (datatype)
            {
                case "String":
                    strValue = "string.Empty";
                    break;
                case "Int32":
                    strValue = "0";
                    break;
                case "Int64":
                    strValue = "0";
                    break;
                case "Int16":
                    strValue = "0";
                    break;
                case "Byte":
                    strValue = "0";
                    break;
                case "Boolean":
                    strValue = "false";
                    break;
                case "Decimal":
                    strValue = "0.0m";
                    break;
                case "Single":
                    strValue = "0.0";
                    break;
                case "Double":
                    strValue = "0.0";
                    break;
                case "DateTime":
                    strValue = "new DateTime(1900, 1, 1)";
                    break;
                case "Guid":
                    strValue = "Guid.Empty";
                    break;
                case "Object":
                    strValue = "null";
                    break;
                case "Byte[]":
                    strValue = null;
                    break;
                default:
                    throw (new Exception("无法识别的数据类型[" + datatype + "]。"));
            }
            return strValue;
        }

        public static void InitializeColumnDefalutValue(DataTable dtb)
        {
            foreach (DataColumn dcl in dtb.Columns)
            {
                dcl.ReadOnly = false;
                switch (dcl.DataType.Name)
                {
                    case "String":
                        dcl.DefaultValue = string.Empty;
                        break;
                    case "Int32":
                        if (!dcl.AutoIncrement)
                        {
                            dcl.DefaultValue = 0;
                        }
                        break;
                    case "Int64":
                        if (!dcl.AutoIncrement)
                        {
                            dcl.DefaultValue = 0;
                        }
                        break;
                    case "Int16":
                        if (!dcl.AutoIncrement)
                        {
                            dcl.DefaultValue = 0;
                        }
                        break;
                    case "Byte":
                        if (!dcl.AutoIncrement)
                        {
                            dcl.DefaultValue = 0;
                        }
                        break;
                    case "Boolean":
                        dcl.DefaultValue = false;
                        break;
                    case "Decimal":
                        if (!dcl.AutoIncrement)
                        {
                            dcl.DefaultValue = 0.0;
                        }
                        break;
                    case "Single":
                        if (!dcl.AutoIncrement)
                        {
                            dcl.DefaultValue = 0.0;
                        }
                        break;
                    case "Double":
                        if (!dcl.AutoIncrement)
                        {
                            dcl.DefaultValue = 0.0;
                        }
                        break;
                    case "DateTime":
                        if (!dcl.AllowDBNull)
                        {
                            dcl.DefaultValue = DateTime.Now;
                        }
                        break;
                    case "Guid":
                        dcl.DefaultValue = Guid.Empty;
                        break;
                    case "Object":
                        dcl.DefaultValue = null;
                        break;
                    case "Byte[]":
                        // Binary、Image、Timestamp类型对应Byte[]，故采用Variant
                        dcl.DefaultValue = null;
                        break;
                    default:
                        throw (new Exception("无法识别的数据类型[" + dcl.DataType.Name + "]。"));
                }
            }
        }
        #endregion

        #region 操作
        /// <summary>
        /// 此方法会将实体设置为Unchanged状体，然后清除字段更改集合。一般在将实体数据保存到数据库后再执行此操作。
        /// </summary>
        public void AcceptChanges()
        {
            m_entityState = e_EntityState.Unchanged;

            if (m_listModified != null)
            {
                m_listModified.Clear();
                m_listModified = null;
            }
        }


        public void RejectChanges()
        {
            //m_entityState = e_EntityState.Unchanged;

            //if (m_listModified != null && this.ModifiedList.Count > 0)
            //{
            //    IDictionaryEnumerator enumerator = this.ModifiedList.GetEnumerator();
            //    while (enumerator.MoveNext())
            //    {
            //        SetValue(enumerator.Key.ToString(), enumerator.Value);                
            //    }

            //    m_listModified.Clear();
            //    m_listModified = null;
            //}
        }

        /// <summary>
        /// 将DataRow转换为实体。转换后的实体状态与DataRow的状态RowState对应。
        /// </summary>
        /// <param name="typ"></param>
        /// <param name="drw"></param>
        /// <returns></returns>
        public static EntityBase DataRowToEntity(Type typ,DataRow drw)
        {
            e_EntityState state = e_EntityState.Invalid;
            switch (drw.RowState)
            {
                case DataRowState.Added:
                    state = e_EntityState.Added;
                    break;
                case DataRowState.Modified:
                    state = e_EntityState.Modified;
                    break;
                case DataRowState.Deleted:
                    state = e_EntityState.Deleted;
                    break;
                case DataRowState.Unchanged:
                    state = e_EntityState.Unchanged;
                    break;
            }

            EntityBase entity = Activator.CreateInstance(typ,  new object[] { state }) as EntityBase;
            entity.SetValues(drw);
            return entity;
        }

        /// <summary>
        /// 克隆一个实体。克隆内容除了字段值，还有已更改的字段值，实体状态等。
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public static EntityBase Clone(EntityBase entity)
        {
            EntityBase entityClone = Activator.CreateInstance(entity.GetType(), new object[] { entity.EntityState }) as EntityBase;
            entityClone.SetValues(entity.GetValues());

            if (entity.ModifiedList != null)
            {
                entityClone.ModifiedList = new Hashtable(entity.ModifiedList);
            }

            return entityClone;

        }
        #endregion
    }
}
