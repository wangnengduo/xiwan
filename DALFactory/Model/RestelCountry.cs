// [实体版本]v2.7
// 2018-08-07 09:29:24
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class RestelCountry : EntityBase
    {

        #region 初始化
        public RestelCountry() : base()
        {
        }
    
        public RestelCountry(e_EntityState state) : base(state)
        {
        }
    
        static RestelCountry()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("RestelCountry");
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 50;
    
            DataColumn dclCountryName = new DataColumn(c_CountryName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryName);
            dclCountryName.Caption = "";
            dclCountryName.AllowDBNull = true;
            dclCountryName.MaxLength = 500;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] {  };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(RestelCountry);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "CountryCode,CountryName";
        private static string m_strEntityIdentityColumnName = "";
        private static string m_strSqlWhere = "";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_CountryCode = "CountryCode";
        public const string c_CountryName = "CountryName";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _countryName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                AddOriginal("CountryName", _countryName, value);
                _countryName = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] {  };
                }
            }
        }
        #endregion

        #region 操作
        public static RestelCountry DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as RestelCountry;
        }
    
        public RestelCountry Clone()
        {
            return EntityBase.Clone(this) as RestelCountry;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class RestelCountryCollection : EntityCollectionBase
    {

        public RestelCountryCollection()
        {
        }

        private static Type m_typ = typeof(RestelCountryCollection);
        private static Type m_typItem = typeof(RestelCountry);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public RestelCountry this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (RestelCountry)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public RestelCountry this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new RestelCountry GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as RestelCountry;
        }

        public int Add(RestelCountry value)
        {
            return List.Add(value);
        }

        public void Insert(int index, RestelCountry value)
        {
            List.Insert(index, value);
        }

        public void Remove(RestelCountry value)
        {
            List.Remove(value);
        }

        public void Delete(RestelCountry value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new RestelCountryCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(RestelCountry value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(RestelCountry value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class RestelCountryQuery : XiWan.DALFactory.EntityQueryBase
    {

        public RestelCountryQuery()
        {
            m_strTableName = "RestelCountry";

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmCountryName.SourceColumn = "CountryName";
            this.Parameters.Add(m_prmCountryName);
        }
        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(RestelCountry.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmCountryName= EntityBase.NewParameter(RestelCountry.m_dtbEntitySchema.Columns["CountryName"]);
        public IDataParameter CountryName
        {
            get { return m_prmCountryName; }
            set { m_prmCountryName = value; }
        }

    }
    #endregion
}