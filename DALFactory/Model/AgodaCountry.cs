// [实体版本]v2.7
// 2018-11-22 11:12:35
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AgodaCountry : EntityBase
    {

        #region 初始化
        public AgodaCountry() : base()
        {
        }
    
        public AgodaCountry(e_EntityState state) : base(state)
        {
        }
    
        static AgodaCountry()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AgodaCountry");
    
            DataColumn dclActiveHotels = new DataColumn(c_ActiveHotels,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclActiveHotels);
            dclActiveHotels.Caption = "";
            dclActiveHotels.AllowDBNull = true;
            dclActiveHotels.MaxLength = 50;
    
            DataColumn dclContinentId = new DataColumn(c_ContinentId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinentId);
            dclContinentId.Caption = "";
            dclContinentId.AllowDBNull = true;
            dclContinentId.MaxLength = 50;
    
            DataColumn dclCountryId = new DataColumn(c_CountryId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryId);
            dclCountryId.Caption = "";
            dclCountryId.AllowDBNull = true;
            dclCountryId.MaxLength = 50;
    
            DataColumn dclCountryIso = new DataColumn(c_CountryIso,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryIso);
            dclCountryIso.Caption = "";
            dclCountryIso.AllowDBNull = true;
            dclCountryIso.MaxLength = 50;
    
            DataColumn dclCountryIso2 = new DataColumn(c_CountryIso2,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryIso2);
            dclCountryIso2.Caption = "";
            dclCountryIso2.AllowDBNull = true;
            dclCountryIso2.MaxLength = 50;
    
            DataColumn dclCountryName = new DataColumn(c_CountryName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryName);
            dclCountryName.Caption = "";
            dclCountryName.AllowDBNull = true;
            dclCountryName.MaxLength = 500;
    
            DataColumn dclCountryTranslated = new DataColumn(c_CountryTranslated,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryTranslated);
            dclCountryTranslated.Caption = "";
            dclCountryTranslated.AllowDBNull = true;
            dclCountryTranslated.MaxLength = 500;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclLatitude = new DataColumn(c_Latitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitude);
            dclLatitude.Caption = "";
            dclLatitude.AllowDBNull = true;
            dclLatitude.MaxLength = 50;
    
            DataColumn dclLongitude = new DataColumn(c_Longitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitude);
            dclLongitude.Caption = "";
            dclLongitude.AllowDBNull = true;
            dclLongitude.MaxLength = 50;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AgodaCountry);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "ActiveHotels,ContinentId,CountryId,CountryIso,CountryIso2,CountryName,CountryTranslated,ID,Latitude,Longitude";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_ActiveHotels = "ActiveHotels";
        public const string c_ContinentId = "ContinentId";
        public const string c_CountryId = "CountryId";
        public const string c_CountryIso = "CountryIso";
        public const string c_CountryIso2 = "CountryIso2";
        public const string c_CountryName = "CountryName";
        public const string c_CountryTranslated = "CountryTranslated";
        public const string c_ID = "ID";
        public const string c_Latitude = "Latitude";
        public const string c_Longitude = "Longitude";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _activeHotels = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ActiveHotels
        {
            get
            {
                return _activeHotels;
            }
            set
            {
                AddOriginal("ActiveHotels", _activeHotels, value);
                _activeHotels = value;
            }
        }
    
        private System.String _continentId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ContinentId
        {
            get
            {
                return _continentId;
            }
            set
            {
                AddOriginal("ContinentId", _continentId, value);
                _continentId = value;
            }
        }
    
        private System.String _countryId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryId
        {
            get
            {
                return _countryId;
            }
            set
            {
                AddOriginal("CountryId", _countryId, value);
                _countryId = value;
            }
        }
    
        private System.String _countryIso = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryIso
        {
            get
            {
                return _countryIso;
            }
            set
            {
                AddOriginal("CountryIso", _countryIso, value);
                _countryIso = value;
            }
        }
    
        private System.String _countryIso2 = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryIso2
        {
            get
            {
                return _countryIso2;
            }
            set
            {
                AddOriginal("CountryIso2", _countryIso2, value);
                _countryIso2 = value;
            }
        }
    
        private System.String _countryName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                AddOriginal("CountryName", _countryName, value);
                _countryName = value;
            }
        }
    
        private System.String _countryTranslated = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryTranslated
        {
            get
            {
                return _countryTranslated;
            }
            set
            {
                AddOriginal("CountryTranslated", _countryTranslated, value);
                _countryTranslated = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _latitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                AddOriginal("Latitude", _latitude, value);
                _latitude = value;
            }
        }
    
        private System.String _longitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                AddOriginal("Longitude", _longitude, value);
                _longitude = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AgodaCountry DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AgodaCountry;
        }
    
        public AgodaCountry Clone()
        {
            return EntityBase.Clone(this) as AgodaCountry;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AgodaCountryCollection : EntityCollectionBase
    {

        public AgodaCountryCollection()
        {
        }

        private static Type m_typ = typeof(AgodaCountryCollection);
        private static Type m_typItem = typeof(AgodaCountry);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AgodaCountry this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AgodaCountry)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AgodaCountry this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AgodaCountry GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AgodaCountry;
        }

        public int Add(AgodaCountry value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AgodaCountry value)
        {
            List.Insert(index, value);
        }

        public void Remove(AgodaCountry value)
        {
            List.Remove(value);
        }

        public void Delete(AgodaCountry value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AgodaCountryCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AgodaCountry value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AgodaCountry value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AgodaCountryQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AgodaCountryQuery()
        {
            m_strTableName = "AgodaCountry";

            m_prmActiveHotels.SourceColumn = "ActiveHotels";
            this.Parameters.Add(m_prmActiveHotels);

            m_prmContinentId.SourceColumn = "ContinentId";
            this.Parameters.Add(m_prmContinentId);

            m_prmCountryId.SourceColumn = "CountryId";
            this.Parameters.Add(m_prmCountryId);

            m_prmCountryIso.SourceColumn = "CountryIso";
            this.Parameters.Add(m_prmCountryIso);

            m_prmCountryIso2.SourceColumn = "CountryIso2";
            this.Parameters.Add(m_prmCountryIso2);

            m_prmCountryName.SourceColumn = "CountryName";
            this.Parameters.Add(m_prmCountryName);

            m_prmCountryTranslated.SourceColumn = "CountryTranslated";
            this.Parameters.Add(m_prmCountryTranslated);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmLatitude.SourceColumn = "Latitude";
            this.Parameters.Add(m_prmLatitude);

            m_prmLongitude.SourceColumn = "Longitude";
            this.Parameters.Add(m_prmLongitude);
        }
        private IDataParameter m_prmActiveHotels= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["ActiveHotels"]);
        public IDataParameter ActiveHotels
        {
            get { return m_prmActiveHotels; }
            set { m_prmActiveHotels = value; }
        }

        private IDataParameter m_prmContinentId= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["ContinentId"]);
        public IDataParameter ContinentId
        {
            get { return m_prmContinentId; }
            set { m_prmContinentId = value; }
        }

        private IDataParameter m_prmCountryId= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["CountryId"]);
        public IDataParameter CountryId
        {
            get { return m_prmCountryId; }
            set { m_prmCountryId = value; }
        }

        private IDataParameter m_prmCountryIso= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["CountryIso"]);
        public IDataParameter CountryIso
        {
            get { return m_prmCountryIso; }
            set { m_prmCountryIso = value; }
        }

        private IDataParameter m_prmCountryIso2= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["CountryIso2"]);
        public IDataParameter CountryIso2
        {
            get { return m_prmCountryIso2; }
            set { m_prmCountryIso2 = value; }
        }

        private IDataParameter m_prmCountryName= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["CountryName"]);
        public IDataParameter CountryName
        {
            get { return m_prmCountryName; }
            set { m_prmCountryName = value; }
        }

        private IDataParameter m_prmCountryTranslated= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["CountryTranslated"]);
        public IDataParameter CountryTranslated
        {
            get { return m_prmCountryTranslated; }
            set { m_prmCountryTranslated = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmLatitude= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["Latitude"]);
        public IDataParameter Latitude
        {
            get { return m_prmLatitude; }
            set { m_prmLatitude = value; }
        }

        private IDataParameter m_prmLongitude= EntityBase.NewParameter(AgodaCountry.m_dtbEntitySchema.Columns["Longitude"]);
        public IDataParameter Longitude
        {
            get { return m_prmLongitude; }
            set { m_prmLongitude = value; }
        }

    }
    #endregion
}