// [实体版本]v2.7
// 2018-11-22 13:43:23
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AgodaRoomType : EntityBase
    {

        #region 初始化
        public AgodaRoomType() : base()
        {
        }
    
        public AgodaRoomType(e_EntityState state) : base(state)
        {
        }
    
        static AgodaRoomType()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AgodaRoomType");
    
            DataColumn dclBedType = new DataColumn(c_BedType,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBedType);
            dclBedType.Caption = "";
            dclBedType.AllowDBNull = true;
            dclBedType.MaxLength = 0;
    
            DataColumn dclCreateTime = new DataColumn(c_CreateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreateTime);
            dclCreateTime.Caption = "";
            dclCreateTime.AllowDBNull = true;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "";
            dclHotelID.AllowDBNull = true;
            dclHotelID.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsBalcony = new DataColumn(c_IsBalcony,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclIsBalcony);
            dclIsBalcony.Caption = "";
            dclIsBalcony.AllowDBNull = true;
    
            DataColumn dclMaximumBabies = new DataColumn(c_MaximumBabies,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclMaximumBabies);
            dclMaximumBabies.Caption = "";
            dclMaximumBabies.AllowDBNull = true;
    
            DataColumn dclMaximumExtraBeds = new DataColumn(c_MaximumExtraBeds,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclMaximumExtraBeds);
            dclMaximumExtraBeds.Caption = "";
            dclMaximumExtraBeds.AllowDBNull = true;
    
            DataColumn dclMaxPerson = new DataColumn(c_MaxPerson,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclMaxPerson);
            dclMaxPerson.Caption = "";
            dclMaxPerson.AllowDBNull = true;
    
            DataColumn dclParentRoomTypeID = new DataColumn(c_ParentRoomTypeID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclParentRoomTypeID);
            dclParentRoomTypeID.Caption = "";
            dclParentRoomTypeID.AllowDBNull = true;
            dclParentRoomTypeID.MaxLength = 50;
    
            DataColumn dclRoomsArea = new DataColumn(c_RoomsArea,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomsArea);
            dclRoomsArea.Caption = "";
            dclRoomsArea.AllowDBNull = true;
            dclRoomsArea.MaxLength = 50;
    
            DataColumn dclRoomsTotal = new DataColumn(c_RoomsTotal,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclRoomsTotal);
            dclRoomsTotal.Caption = "";
            dclRoomsTotal.AllowDBNull = true;
    
            DataColumn dclRoomTypeID = new DataColumn(c_RoomTypeID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomTypeID);
            dclRoomTypeID.Caption = "";
            dclRoomTypeID.AllowDBNull = true;
            dclRoomTypeID.MaxLength = 50;
    
            DataColumn dclRoomTypeNameCN = new DataColumn(c_RoomTypeNameCN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomTypeNameCN);
            dclRoomTypeNameCN.Caption = "";
            dclRoomTypeNameCN.AllowDBNull = true;
            dclRoomTypeNameCN.MaxLength = 0;
    
            DataColumn dclRoomTypeNameEN = new DataColumn(c_RoomTypeNameEN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomTypeNameEN);
            dclRoomTypeNameEN.Caption = "";
            dclRoomTypeNameEN.AllowDBNull = true;
            dclRoomTypeNameEN.MaxLength = 0;
    
            DataColumn dclRoomTypePicture = new DataColumn(c_RoomTypePicture,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomTypePicture);
            dclRoomTypePicture.Caption = "";
            dclRoomTypePicture.AllowDBNull = true;
            dclRoomTypePicture.MaxLength = 0;
    
            DataColumn dclViews = new DataColumn(c_Views,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclViews);
            dclViews.Caption = "";
            dclViews.AllowDBNull = true;
            dclViews.MaxLength = 0;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AgodaRoomType);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "BedType,CreateTime,HotelID,ID,IsBalcony,MaximumBabies,MaximumExtraBeds,MaxPerson,ParentRoomTypeID,RoomsArea,RoomsTotal,RoomTypeID,RoomTypeNameCN,RoomTypeNameEN,RoomTypePicture,Views";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_BedType = "BedType";
        public const string c_CreateTime = "CreateTime";
        public const string c_HotelID = "HotelID";
        public const string c_ID = "ID";
        public const string c_IsBalcony = "IsBalcony";
        public const string c_MaximumBabies = "MaximumBabies";
        public const string c_MaximumExtraBeds = "MaximumExtraBeds";
        public const string c_MaxPerson = "MaxPerson";
        public const string c_ParentRoomTypeID = "ParentRoomTypeID";
        public const string c_RoomsArea = "RoomsArea";
        public const string c_RoomsTotal = "RoomsTotal";
        public const string c_RoomTypeID = "RoomTypeID";
        public const string c_RoomTypeNameCN = "RoomTypeNameCN";
        public const string c_RoomTypeNameEN = "RoomTypeNameEN";
        public const string c_RoomTypePicture = "RoomTypePicture";
        public const string c_Views = "Views";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _bedType = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String BedType
        {
            get
            {
                return _bedType;
            }
            set
            {
                AddOriginal("BedType", _bedType, value);
                _bedType = value;
            }
        }
    
        private System.DateTime _createTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                AddOriginal("CreateTime", _createTime, value);
                _createTime = value;
            }
        }
    
        private System.String _hotelID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Int32 _isBalcony = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 IsBalcony
        {
            get
            {
                return _isBalcony;
            }
            set
            {
                AddOriginal("IsBalcony", _isBalcony, value);
                _isBalcony = value;
            }
        }
    
        private System.Int32 _maximumBabies = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 MaximumBabies
        {
            get
            {
                return _maximumBabies;
            }
            set
            {
                AddOriginal("MaximumBabies", _maximumBabies, value);
                _maximumBabies = value;
            }
        }
    
        private System.Int32 _maximumExtraBeds = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 MaximumExtraBeds
        {
            get
            {
                return _maximumExtraBeds;
            }
            set
            {
                AddOriginal("MaximumExtraBeds", _maximumExtraBeds, value);
                _maximumExtraBeds = value;
            }
        }
    
        private System.Int32 _maxPerson = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 MaxPerson
        {
            get
            {
                return _maxPerson;
            }
            set
            {
                AddOriginal("MaxPerson", _maxPerson, value);
                _maxPerson = value;
            }
        }
    
        private System.String _parentRoomTypeID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ParentRoomTypeID
        {
            get
            {
                return _parentRoomTypeID;
            }
            set
            {
                AddOriginal("ParentRoomTypeID", _parentRoomTypeID, value);
                _parentRoomTypeID = value;
            }
        }
    
        private System.String _roomsArea = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomsArea
        {
            get
            {
                return _roomsArea;
            }
            set
            {
                AddOriginal("RoomsArea", _roomsArea, value);
                _roomsArea = value;
            }
        }
    
        private System.Int32 _roomsTotal = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 RoomsTotal
        {
            get
            {
                return _roomsTotal;
            }
            set
            {
                AddOriginal("RoomsTotal", _roomsTotal, value);
                _roomsTotal = value;
            }
        }
    
        private System.String _roomTypeID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomTypeID
        {
            get
            {
                return _roomTypeID;
            }
            set
            {
                AddOriginal("RoomTypeID", _roomTypeID, value);
                _roomTypeID = value;
            }
        }
    
        private System.String _roomTypeNameCN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomTypeNameCN
        {
            get
            {
                return _roomTypeNameCN;
            }
            set
            {
                AddOriginal("RoomTypeNameCN", _roomTypeNameCN, value);
                _roomTypeNameCN = value;
            }
        }
    
        private System.String _roomTypeNameEN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomTypeNameEN
        {
            get
            {
                return _roomTypeNameEN;
            }
            set
            {
                AddOriginal("RoomTypeNameEN", _roomTypeNameEN, value);
                _roomTypeNameEN = value;
            }
        }
    
        private System.String _roomTypePicture = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomTypePicture
        {
            get
            {
                return _roomTypePicture;
            }
            set
            {
                AddOriginal("RoomTypePicture", _roomTypePicture, value);
                _roomTypePicture = value;
            }
        }
    
        private System.String _views = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Views
        {
            get
            {
                return _views;
            }
            set
            {
                AddOriginal("Views", _views, value);
                _views = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AgodaRoomType DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AgodaRoomType;
        }
    
        public AgodaRoomType Clone()
        {
            return EntityBase.Clone(this) as AgodaRoomType;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AgodaRoomTypeCollection : EntityCollectionBase
    {

        public AgodaRoomTypeCollection()
        {
        }

        private static Type m_typ = typeof(AgodaRoomTypeCollection);
        private static Type m_typItem = typeof(AgodaRoomType);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AgodaRoomType this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AgodaRoomType)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AgodaRoomType this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AgodaRoomType GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AgodaRoomType;
        }

        public int Add(AgodaRoomType value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AgodaRoomType value)
        {
            List.Insert(index, value);
        }

        public void Remove(AgodaRoomType value)
        {
            List.Remove(value);
        }

        public void Delete(AgodaRoomType value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AgodaRoomTypeCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AgodaRoomType value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AgodaRoomType value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AgodaRoomTypeQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AgodaRoomTypeQuery()
        {
            m_strTableName = "AgodaRoomType";

            m_prmBedType.SourceColumn = "BedType";
            this.Parameters.Add(m_prmBedType);

            m_prmCreateTime.SourceColumn = "CreateTime";
            this.Parameters.Add(m_prmCreateTime);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsBalcony.SourceColumn = "IsBalcony";
            this.Parameters.Add(m_prmIsBalcony);

            m_prmMaximumBabies.SourceColumn = "MaximumBabies";
            this.Parameters.Add(m_prmMaximumBabies);

            m_prmMaximumExtraBeds.SourceColumn = "MaximumExtraBeds";
            this.Parameters.Add(m_prmMaximumExtraBeds);

            m_prmMaxPerson.SourceColumn = "MaxPerson";
            this.Parameters.Add(m_prmMaxPerson);

            m_prmParentRoomTypeID.SourceColumn = "ParentRoomTypeID";
            this.Parameters.Add(m_prmParentRoomTypeID);

            m_prmRoomsArea.SourceColumn = "RoomsArea";
            this.Parameters.Add(m_prmRoomsArea);

            m_prmRoomsTotal.SourceColumn = "RoomsTotal";
            this.Parameters.Add(m_prmRoomsTotal);

            m_prmRoomTypeID.SourceColumn = "RoomTypeID";
            this.Parameters.Add(m_prmRoomTypeID);

            m_prmRoomTypeNameCN.SourceColumn = "RoomTypeNameCN";
            this.Parameters.Add(m_prmRoomTypeNameCN);

            m_prmRoomTypeNameEN.SourceColumn = "RoomTypeNameEN";
            this.Parameters.Add(m_prmRoomTypeNameEN);

            m_prmRoomTypePicture.SourceColumn = "RoomTypePicture";
            this.Parameters.Add(m_prmRoomTypePicture);

            m_prmViews.SourceColumn = "Views";
            this.Parameters.Add(m_prmViews);
        }
        private IDataParameter m_prmBedType= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["BedType"]);
        public IDataParameter BedType
        {
            get { return m_prmBedType; }
            set { m_prmBedType = value; }
        }

        private IDataParameter m_prmCreateTime= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["CreateTime"]);
        public IDataParameter CreateTime
        {
            get { return m_prmCreateTime; }
            set { m_prmCreateTime = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsBalcony= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["IsBalcony"]);
        public IDataParameter IsBalcony
        {
            get { return m_prmIsBalcony; }
            set { m_prmIsBalcony = value; }
        }

        private IDataParameter m_prmMaximumBabies= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["MaximumBabies"]);
        public IDataParameter MaximumBabies
        {
            get { return m_prmMaximumBabies; }
            set { m_prmMaximumBabies = value; }
        }

        private IDataParameter m_prmMaximumExtraBeds= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["MaximumExtraBeds"]);
        public IDataParameter MaximumExtraBeds
        {
            get { return m_prmMaximumExtraBeds; }
            set { m_prmMaximumExtraBeds = value; }
        }

        private IDataParameter m_prmMaxPerson= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["MaxPerson"]);
        public IDataParameter MaxPerson
        {
            get { return m_prmMaxPerson; }
            set { m_prmMaxPerson = value; }
        }

        private IDataParameter m_prmParentRoomTypeID= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["ParentRoomTypeID"]);
        public IDataParameter ParentRoomTypeID
        {
            get { return m_prmParentRoomTypeID; }
            set { m_prmParentRoomTypeID = value; }
        }

        private IDataParameter m_prmRoomsArea= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["RoomsArea"]);
        public IDataParameter RoomsArea
        {
            get { return m_prmRoomsArea; }
            set { m_prmRoomsArea = value; }
        }

        private IDataParameter m_prmRoomsTotal= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["RoomsTotal"]);
        public IDataParameter RoomsTotal
        {
            get { return m_prmRoomsTotal; }
            set { m_prmRoomsTotal = value; }
        }

        private IDataParameter m_prmRoomTypeID= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["RoomTypeID"]);
        public IDataParameter RoomTypeID
        {
            get { return m_prmRoomTypeID; }
            set { m_prmRoomTypeID = value; }
        }

        private IDataParameter m_prmRoomTypeNameCN= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["RoomTypeNameCN"]);
        public IDataParameter RoomTypeNameCN
        {
            get { return m_prmRoomTypeNameCN; }
            set { m_prmRoomTypeNameCN = value; }
        }

        private IDataParameter m_prmRoomTypeNameEN= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["RoomTypeNameEN"]);
        public IDataParameter RoomTypeNameEN
        {
            get { return m_prmRoomTypeNameEN; }
            set { m_prmRoomTypeNameEN = value; }
        }

        private IDataParameter m_prmRoomTypePicture= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["RoomTypePicture"]);
        public IDataParameter RoomTypePicture
        {
            get { return m_prmRoomTypePicture; }
            set { m_prmRoomTypePicture = value; }
        }

        private IDataParameter m_prmViews= EntityBase.NewParameter(AgodaRoomType.m_dtbEntitySchema.Columns["Views"]);
        public IDataParameter Views
        {
            get { return m_prmViews; }
            set { m_prmViews = value; }
        }

    }
    #endregion
}