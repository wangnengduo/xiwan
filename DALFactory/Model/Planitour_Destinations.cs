// [实体版本]v2.7
// 2018-07-20 14:48:08
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class Planitour_Destinations : EntityBase
    {

        #region 初始化
        public Planitour_Destinations() : base()
        {
        }
    
        public Planitour_Destinations(e_EntityState state) : base(state)
        {
        }
    
        static Planitour_Destinations()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("Planitour_Destinations");
    
            DataColumn dclcountryId = new DataColumn(c_countryId,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclcountryId);
            dclcountryId.Caption = "";
            dclcountryId.AllowDBNull = true;
    
            DataColumn dclDestinationId = new DataColumn(c_DestinationId,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclDestinationId);
            dclDestinationId.Caption = "";
            dclDestinationId.AllowDBNull = true;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
    
            DataColumn dclName = new DataColumn(c_Name,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclName);
            dclName.Caption = "";
            dclName.AllowDBNull = true;
            dclName.MaxLength = 500;
    
            DataColumn dclParentDestinationId = new DataColumn(c_ParentDestinationId,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclParentDestinationId);
            dclParentDestinationId.Caption = "";
            dclParentDestinationId.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] {  };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(Planitour_Destinations);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "countryId,DestinationId,ID,Name,ParentDestinationId";
        private static string m_strEntityIdentityColumnName = "";
        private static string m_strSqlWhere = "";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_countryId = "countryId";
        public const string c_DestinationId = "DestinationId";
        public const string c_ID = "ID";
        public const string c_Name = "Name";
        public const string c_ParentDestinationId = "ParentDestinationId";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.Int32 _countryId = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 countryId
        {
            get
            {
                return _countryId;
            }
            set
            {
                AddOriginal("countryId", _countryId, value);
                _countryId = value;
            }
        }
    
        private System.Int32 _destinationId = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 DestinationId
        {
            get
            {
                return _destinationId;
            }
            set
            {
                AddOriginal("DestinationId", _destinationId, value);
                _destinationId = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _name = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Name
        {
            get
            {
                return _name;
            }
            set
            {
                AddOriginal("Name", _name, value);
                _name = value;
            }
        }
    
        private System.Int32 _parentDestinationId = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ParentDestinationId
        {
            get
            {
                return _parentDestinationId;
            }
            set
            {
                AddOriginal("ParentDestinationId", _parentDestinationId, value);
                _parentDestinationId = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] {  };
                }
            }
        }
        #endregion

        #region 操作
        public static Planitour_Destinations DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as Planitour_Destinations;
        }
    
        public Planitour_Destinations Clone()
        {
            return EntityBase.Clone(this) as Planitour_Destinations;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class Planitour_DestinationsCollection : EntityCollectionBase
    {

        public Planitour_DestinationsCollection()
        {
        }

        private static Type m_typ = typeof(Planitour_DestinationsCollection);
        private static Type m_typItem = typeof(Planitour_Destinations);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public Planitour_Destinations this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (Planitour_Destinations)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public Planitour_Destinations this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new Planitour_Destinations GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as Planitour_Destinations;
        }

        public int Add(Planitour_Destinations value)
        {
            return List.Add(value);
        }

        public void Insert(int index, Planitour_Destinations value)
        {
            List.Insert(index, value);
        }

        public void Remove(Planitour_Destinations value)
        {
            List.Remove(value);
        }

        public void Delete(Planitour_Destinations value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new Planitour_DestinationsCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(Planitour_Destinations value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(Planitour_Destinations value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class Planitour_DestinationsQuery : XiWan.DALFactory.EntityQueryBase
    {

        public Planitour_DestinationsQuery()
        {
            m_strTableName = "Planitour_Destinations";

            m_prmcountryId.SourceColumn = "countryId";
            this.Parameters.Add(m_prmcountryId);

            m_prmDestinationId.SourceColumn = "DestinationId";
            this.Parameters.Add(m_prmDestinationId);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmName.SourceColumn = "Name";
            this.Parameters.Add(m_prmName);

            m_prmParentDestinationId.SourceColumn = "ParentDestinationId";
            this.Parameters.Add(m_prmParentDestinationId);
        }
        private IDataParameter m_prmcountryId= EntityBase.NewParameter(Planitour_Destinations.m_dtbEntitySchema.Columns["countryId"]);
        public IDataParameter countryId
        {
            get { return m_prmcountryId; }
            set { m_prmcountryId = value; }
        }

        private IDataParameter m_prmDestinationId= EntityBase.NewParameter(Planitour_Destinations.m_dtbEntitySchema.Columns["DestinationId"]);
        public IDataParameter DestinationId
        {
            get { return m_prmDestinationId; }
            set { m_prmDestinationId = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(Planitour_Destinations.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmName= EntityBase.NewParameter(Planitour_Destinations.m_dtbEntitySchema.Columns["Name"]);
        public IDataParameter Name
        {
            get { return m_prmName; }
            set { m_prmName = value; }
        }

        private IDataParameter m_prmParentDestinationId= EntityBase.NewParameter(Planitour_Destinations.m_dtbEntitySchema.Columns["ParentDestinationId"]);
        public IDataParameter ParentDestinationId
        {
            get { return m_prmParentDestinationId; }
            set { m_prmParentDestinationId = value; }
        }

    }
    #endregion
}