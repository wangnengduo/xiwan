// [实体版本]v2.7
// 2018-11-22 11:12:35
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AgodaCity : EntityBase
    {

        #region 初始化
        public AgodaCity() : base()
        {
        }
    
        public AgodaCity(e_EntityState state) : base(state)
        {
        }
    
        static AgodaCity()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AgodaCity");
    
            DataColumn dclActiveHotels = new DataColumn(c_ActiveHotels,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclActiveHotels);
            dclActiveHotels.Caption = "";
            dclActiveHotels.AllowDBNull = true;
            dclActiveHotels.MaxLength = 50;
    
            DataColumn dclCityCame = new DataColumn(c_CityCame,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityCame);
            dclCityCame.Caption = "";
            dclCityCame.AllowDBNull = true;
            dclCityCame.MaxLength = 500;
    
            DataColumn dclCityId = new DataColumn(c_CityId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityId);
            dclCityId.Caption = "";
            dclCityId.AllowDBNull = true;
            dclCityId.MaxLength = 50;
    
            DataColumn dclCityTranslated = new DataColumn(c_CityTranslated,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityTranslated);
            dclCityTranslated.Caption = "";
            dclCityTranslated.AllowDBNull = true;
            dclCityTranslated.MaxLength = 500;
    
            DataColumn dclCountryId = new DataColumn(c_CountryId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryId);
            dclCountryId.Caption = "";
            dclCountryId.AllowDBNull = true;
            dclCountryId.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclLatitude = new DataColumn(c_Latitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitude);
            dclLatitude.Caption = "";
            dclLatitude.AllowDBNull = true;
            dclLatitude.MaxLength = 50;
    
            DataColumn dclLongitude = new DataColumn(c_Longitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitude);
            dclLongitude.Caption = "";
            dclLongitude.AllowDBNull = true;
            dclLongitude.MaxLength = 50;
    
            DataColumn dclNoArea = new DataColumn(c_NoArea,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclNoArea);
            dclNoArea.Caption = "";
            dclNoArea.AllowDBNull = true;
            dclNoArea.MaxLength = 50;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AgodaCity);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "ActiveHotels,CityCame,CityId,CityTranslated,CountryId,ID,Latitude,Longitude,NoArea";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_ActiveHotels = "ActiveHotels";
        public const string c_CityCame = "CityCame";
        public const string c_CityId = "CityId";
        public const string c_CityTranslated = "CityTranslated";
        public const string c_CountryId = "CountryId";
        public const string c_ID = "ID";
        public const string c_Latitude = "Latitude";
        public const string c_Longitude = "Longitude";
        public const string c_NoArea = "NoArea";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _activeHotels = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ActiveHotels
        {
            get
            {
                return _activeHotels;
            }
            set
            {
                AddOriginal("ActiveHotels", _activeHotels, value);
                _activeHotels = value;
            }
        }
    
        private System.String _cityCame = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityCame
        {
            get
            {
                return _cityCame;
            }
            set
            {
                AddOriginal("CityCame", _cityCame, value);
                _cityCame = value;
            }
        }
    
        private System.String _cityId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityId
        {
            get
            {
                return _cityId;
            }
            set
            {
                AddOriginal("CityId", _cityId, value);
                _cityId = value;
            }
        }
    
        private System.String _cityTranslated = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityTranslated
        {
            get
            {
                return _cityTranslated;
            }
            set
            {
                AddOriginal("CityTranslated", _cityTranslated, value);
                _cityTranslated = value;
            }
        }
    
        private System.String _countryId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryId
        {
            get
            {
                return _countryId;
            }
            set
            {
                AddOriginal("CountryId", _countryId, value);
                _countryId = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _latitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                AddOriginal("Latitude", _latitude, value);
                _latitude = value;
            }
        }
    
        private System.String _longitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                AddOriginal("Longitude", _longitude, value);
                _longitude = value;
            }
        }
    
        private System.String _noArea = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String NoArea
        {
            get
            {
                return _noArea;
            }
            set
            {
                AddOriginal("NoArea", _noArea, value);
                _noArea = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AgodaCity DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AgodaCity;
        }
    
        public AgodaCity Clone()
        {
            return EntityBase.Clone(this) as AgodaCity;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AgodaCityCollection : EntityCollectionBase
    {

        public AgodaCityCollection()
        {
        }

        private static Type m_typ = typeof(AgodaCityCollection);
        private static Type m_typItem = typeof(AgodaCity);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AgodaCity this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AgodaCity)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AgodaCity this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AgodaCity GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AgodaCity;
        }

        public int Add(AgodaCity value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AgodaCity value)
        {
            List.Insert(index, value);
        }

        public void Remove(AgodaCity value)
        {
            List.Remove(value);
        }

        public void Delete(AgodaCity value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AgodaCityCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AgodaCity value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AgodaCity value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AgodaCityQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AgodaCityQuery()
        {
            m_strTableName = "AgodaCity";

            m_prmActiveHotels.SourceColumn = "ActiveHotels";
            this.Parameters.Add(m_prmActiveHotels);

            m_prmCityCame.SourceColumn = "CityCame";
            this.Parameters.Add(m_prmCityCame);

            m_prmCityId.SourceColumn = "CityId";
            this.Parameters.Add(m_prmCityId);

            m_prmCityTranslated.SourceColumn = "CityTranslated";
            this.Parameters.Add(m_prmCityTranslated);

            m_prmCountryId.SourceColumn = "CountryId";
            this.Parameters.Add(m_prmCountryId);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmLatitude.SourceColumn = "Latitude";
            this.Parameters.Add(m_prmLatitude);

            m_prmLongitude.SourceColumn = "Longitude";
            this.Parameters.Add(m_prmLongitude);

            m_prmNoArea.SourceColumn = "NoArea";
            this.Parameters.Add(m_prmNoArea);
        }
        private IDataParameter m_prmActiveHotels= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["ActiveHotels"]);
        public IDataParameter ActiveHotels
        {
            get { return m_prmActiveHotels; }
            set { m_prmActiveHotels = value; }
        }

        private IDataParameter m_prmCityCame= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["CityCame"]);
        public IDataParameter CityCame
        {
            get { return m_prmCityCame; }
            set { m_prmCityCame = value; }
        }

        private IDataParameter m_prmCityId= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["CityId"]);
        public IDataParameter CityId
        {
            get { return m_prmCityId; }
            set { m_prmCityId = value; }
        }

        private IDataParameter m_prmCityTranslated= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["CityTranslated"]);
        public IDataParameter CityTranslated
        {
            get { return m_prmCityTranslated; }
            set { m_prmCityTranslated = value; }
        }

        private IDataParameter m_prmCountryId= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["CountryId"]);
        public IDataParameter CountryId
        {
            get { return m_prmCountryId; }
            set { m_prmCountryId = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmLatitude= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["Latitude"]);
        public IDataParameter Latitude
        {
            get { return m_prmLatitude; }
            set { m_prmLatitude = value; }
        }

        private IDataParameter m_prmLongitude= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["Longitude"]);
        public IDataParameter Longitude
        {
            get { return m_prmLongitude; }
            set { m_prmLongitude = value; }
        }

        private IDataParameter m_prmNoArea= EntityBase.NewParameter(AgodaCity.m_dtbEntitySchema.Columns["NoArea"]);
        public IDataParameter NoArea
        {
            get { return m_prmNoArea; }
            set { m_prmNoArea = value; }
        }

    }
    #endregion
}