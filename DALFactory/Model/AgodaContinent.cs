// [实体版本]v2.7
// 2018-11-22 11:12:35
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AgodaContinent : EntityBase
    {

        #region 初始化
        public AgodaContinent() : base()
        {
        }
    
        public AgodaContinent(e_EntityState state) : base(state)
        {
        }
    
        static AgodaContinent()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AgodaContinent");
    
            DataColumn dclActiveHotels = new DataColumn(c_ActiveHotels,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclActiveHotels);
            dclActiveHotels.Caption = "";
            dclActiveHotels.AllowDBNull = true;
            dclActiveHotels.MaxLength = 50;
    
            DataColumn dclContinentId = new DataColumn(c_ContinentId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinentId);
            dclContinentId.Caption = "";
            dclContinentId.AllowDBNull = true;
            dclContinentId.MaxLength = 50;
    
            DataColumn dclContinentName = new DataColumn(c_ContinentName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinentName);
            dclContinentName.Caption = "";
            dclContinentName.AllowDBNull = true;
            dclContinentName.MaxLength = 500;
    
            DataColumn dclContinentTranslated = new DataColumn(c_ContinentTranslated,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinentTranslated);
            dclContinentTranslated.Caption = "";
            dclContinentTranslated.AllowDBNull = true;
            dclContinentTranslated.MaxLength = 500;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AgodaContinent);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "ActiveHotels,ContinentId,ContinentName,ContinentTranslated,ID";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_ActiveHotels = "ActiveHotels";
        public const string c_ContinentId = "ContinentId";
        public const string c_ContinentName = "ContinentName";
        public const string c_ContinentTranslated = "ContinentTranslated";
        public const string c_ID = "ID";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _activeHotels = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ActiveHotels
        {
            get
            {
                return _activeHotels;
            }
            set
            {
                AddOriginal("ActiveHotels", _activeHotels, value);
                _activeHotels = value;
            }
        }
    
        private System.String _continentId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ContinentId
        {
            get
            {
                return _continentId;
            }
            set
            {
                AddOriginal("ContinentId", _continentId, value);
                _continentId = value;
            }
        }
    
        private System.String _continentName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ContinentName
        {
            get
            {
                return _continentName;
            }
            set
            {
                AddOriginal("ContinentName", _continentName, value);
                _continentName = value;
            }
        }
    
        private System.String _continentTranslated = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ContinentTranslated
        {
            get
            {
                return _continentTranslated;
            }
            set
            {
                AddOriginal("ContinentTranslated", _continentTranslated, value);
                _continentTranslated = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AgodaContinent DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AgodaContinent;
        }
    
        public AgodaContinent Clone()
        {
            return EntityBase.Clone(this) as AgodaContinent;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AgodaContinentCollection : EntityCollectionBase
    {

        public AgodaContinentCollection()
        {
        }

        private static Type m_typ = typeof(AgodaContinentCollection);
        private static Type m_typItem = typeof(AgodaContinent);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AgodaContinent this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AgodaContinent)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AgodaContinent this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AgodaContinent GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AgodaContinent;
        }

        public int Add(AgodaContinent value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AgodaContinent value)
        {
            List.Insert(index, value);
        }

        public void Remove(AgodaContinent value)
        {
            List.Remove(value);
        }

        public void Delete(AgodaContinent value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AgodaContinentCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AgodaContinent value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AgodaContinent value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AgodaContinentQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AgodaContinentQuery()
        {
            m_strTableName = "AgodaContinent";

            m_prmActiveHotels.SourceColumn = "ActiveHotels";
            this.Parameters.Add(m_prmActiveHotels);

            m_prmContinentId.SourceColumn = "ContinentId";
            this.Parameters.Add(m_prmContinentId);

            m_prmContinentName.SourceColumn = "ContinentName";
            this.Parameters.Add(m_prmContinentName);

            m_prmContinentTranslated.SourceColumn = "ContinentTranslated";
            this.Parameters.Add(m_prmContinentTranslated);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);
        }
        private IDataParameter m_prmActiveHotels= EntityBase.NewParameter(AgodaContinent.m_dtbEntitySchema.Columns["ActiveHotels"]);
        public IDataParameter ActiveHotels
        {
            get { return m_prmActiveHotels; }
            set { m_prmActiveHotels = value; }
        }

        private IDataParameter m_prmContinentId= EntityBase.NewParameter(AgodaContinent.m_dtbEntitySchema.Columns["ContinentId"]);
        public IDataParameter ContinentId
        {
            get { return m_prmContinentId; }
            set { m_prmContinentId = value; }
        }

        private IDataParameter m_prmContinentName= EntityBase.NewParameter(AgodaContinent.m_dtbEntitySchema.Columns["ContinentName"]);
        public IDataParameter ContinentName
        {
            get { return m_prmContinentName; }
            set { m_prmContinentName = value; }
        }

        private IDataParameter m_prmContinentTranslated= EntityBase.NewParameter(AgodaContinent.m_dtbEntitySchema.Columns["ContinentTranslated"]);
        public IDataParameter ContinentTranslated
        {
            get { return m_prmContinentTranslated; }
            set { m_prmContinentTranslated = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(AgodaContinent.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

    }
    #endregion
}