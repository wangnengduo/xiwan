// [实体版本]v2.7
// 2018-08-23 16:16:45
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class HotelRecheckPrice : EntityBase
    {

        #region 初始化
        public HotelRecheckPrice() : base()
        {
        }
    
        public HotelRecheckPrice(e_EntityState state) : base(state)
        {
        }
    
        static HotelRecheckPrice()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("HotelRecheckPrice");
    
            DataColumn dclAdults = new DataColumn(c_Adults,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclAdults);
            dclAdults.Caption = "";
            dclAdults.AllowDBNull = true;
    
            DataColumn dclAge = new DataColumn(c_Age,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAge);
            dclAge.Caption = "";
            dclAge.AllowDBNull = true;
            dclAge.MaxLength = 50;
    
            DataColumn dclAgreementID = new DataColumn(c_AgreementID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclAgreementID);
            dclAgreementID.Caption = "";
            dclAgreementID.AllowDBNull = true;
    
            DataColumn dclBBCode = new DataColumn(c_BBCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBBCode);
            dclBBCode.Caption = "";
            dclBBCode.AllowDBNull = true;
            dclBBCode.MaxLength = 50;
    
            DataColumn dclChilds = new DataColumn(c_Childs,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclChilds);
            dclChilds.Caption = "";
            dclChilds.AllowDBNull = true;
    
            DataColumn dclCreateTime = new DataColumn(c_CreateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreateTime);
            dclCreateTime.Caption = "";
            dclCreateTime.AllowDBNull = true;
    
            DataColumn dclDailyCurrencyCode = new DataColumn(c_DailyCurrencyCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclDailyCurrencyCode);
            dclDailyCurrencyCode.Caption = "";
            dclDailyCurrencyCode.AllowDBNull = true;
            dclDailyCurrencyCode.MaxLength = 50;
    
            DataColumn dclDailyRate = new DataColumn(c_DailyRate,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclDailyRate);
            dclDailyRate.Caption = "";
            dclDailyRate.AllowDBNull = true;
    
            DataColumn dclDateOfRate = new DataColumn(c_DateOfRate,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclDateOfRate);
            dclDateOfRate.Caption = "";
            dclDateOfRate.AllowDBNull = true;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "";
            dclHotelID.AllowDBNull = true;
            dclHotelID.MaxLength = 50;
    
            DataColumn dclHotelPriceID = new DataColumn(c_HotelPriceID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclHotelPriceID);
            dclHotelPriceID.Caption = "";
            dclHotelPriceID.AllowDBNull = true;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsClose = new DataColumn(c_IsClose,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclIsClose);
            dclIsClose.Caption = "";
            dclIsClose.AllowDBNull = true;
    
            DataColumn dclRoomCurrencyCode = new DataColumn(c_RoomCurrencyCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomCurrencyCode);
            dclRoomCurrencyCode.Caption = "";
            dclRoomCurrencyCode.AllowDBNull = true;
            dclRoomCurrencyCode.MaxLength = 50;
    
            DataColumn dclRoomID = new DataColumn(c_RoomID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomID);
            dclRoomID.Caption = "";
            dclRoomID.AllowDBNull = true;
            dclRoomID.MaxLength = 50;
    
            DataColumn dclRoomName = new DataColumn(c_RoomName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomName);
            dclRoomName.Caption = "";
            dclRoomName.AllowDBNull = true;
            dclRoomName.MaxLength = 500;
    
            DataColumn dclStatus = new DataColumn(c_Status,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStatus);
            dclStatus.Caption = "";
            dclStatus.AllowDBNull = true;
            dclStatus.MaxLength = 50;
    
            DataColumn dclSupplierRefID = new DataColumn(c_SupplierRefID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclSupplierRefID);
            dclSupplierRefID.Caption = "";
            dclSupplierRefID.AllowDBNull = true;
    
            DataColumn dclSupplierRefValue = new DataColumn(c_SupplierRefValue,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclSupplierRefValue);
            dclSupplierRefValue.Caption = "";
            dclSupplierRefValue.AllowDBNull = true;
            dclSupplierRefValue.MaxLength = 5000;
    
            DataColumn dclTotalPrice = new DataColumn(c_TotalPrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclTotalPrice);
            dclTotalPrice.Caption = "";
            dclTotalPrice.AllowDBNull = true;
    
            DataColumn dclUpdateTime = new DataColumn(c_UpdateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclUpdateTime);
            dclUpdateTime.Caption = "";
            dclUpdateTime.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(HotelRecheckPrice);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Adults,Age,AgreementID,BBCode,Childs,CreateTime,DailyCurrencyCode,DailyRate,DateOfRate,HotelID,HotelPriceID,ID,IsClose,RoomCurrencyCode,RoomID,RoomName,Status,SupplierRefID,SupplierRefValue,TotalPrice,UpdateTime";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Adults = "Adults";
        public const string c_Age = "Age";
        public const string c_AgreementID = "AgreementID";
        public const string c_BBCode = "BBCode";
        public const string c_Childs = "Childs";
        public const string c_CreateTime = "CreateTime";
        public const string c_DailyCurrencyCode = "DailyCurrencyCode";
        public const string c_DailyRate = "DailyRate";
        public const string c_DateOfRate = "DateOfRate";
        public const string c_HotelID = "HotelID";
        public const string c_HotelPriceID = "HotelPriceID";
        public const string c_ID = "ID";
        public const string c_IsClose = "IsClose";
        public const string c_RoomCurrencyCode = "RoomCurrencyCode";
        public const string c_RoomID = "RoomID";
        public const string c_RoomName = "RoomName";
        public const string c_Status = "Status";
        public const string c_SupplierRefID = "SupplierRefID";
        public const string c_SupplierRefValue = "SupplierRefValue";
        public const string c_TotalPrice = "TotalPrice";
        public const string c_UpdateTime = "UpdateTime";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.Int32 _adults = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 Adults
        {
            get
            {
                return _adults;
            }
            set
            {
                AddOriginal("Adults", _adults, value);
                _adults = value;
            }
        }
    
        private System.String _age = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Age
        {
            get
            {
                return _age;
            }
            set
            {
                AddOriginal("Age", _age, value);
                _age = value;
            }
        }
    
        private System.Int32 _agreementID = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 AgreementID
        {
            get
            {
                return _agreementID;
            }
            set
            {
                AddOriginal("AgreementID", _agreementID, value);
                _agreementID = value;
            }
        }
    
        private System.String _bBCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String BBCode
        {
            get
            {
                return _bBCode;
            }
            set
            {
                AddOriginal("BBCode", _bBCode, value);
                _bBCode = value;
            }
        }
    
        private System.Int32 _childs = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 Childs
        {
            get
            {
                return _childs;
            }
            set
            {
                AddOriginal("Childs", _childs, value);
                _childs = value;
            }
        }
    
        private System.DateTime _createTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                AddOriginal("CreateTime", _createTime, value);
                _createTime = value;
            }
        }
    
        private System.String _dailyCurrencyCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String DailyCurrencyCode
        {
            get
            {
                return _dailyCurrencyCode;
            }
            set
            {
                AddOriginal("DailyCurrencyCode", _dailyCurrencyCode, value);
                _dailyCurrencyCode = value;
            }
        }
    
        private System.Decimal _dailyRate = 0.0m;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Decimal DailyRate
        {
            get
            {
                return _dailyRate;
            }
            set
            {
                AddOriginal("DailyRate", _dailyRate, value);
                _dailyRate = value;
            }
        }
    
        private System.DateTime _dateOfRate = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime DateOfRate
        {
            get
            {
                return _dateOfRate;
            }
            set
            {
                AddOriginal("DateOfRate", _dateOfRate, value);
                _dateOfRate = value;
            }
        }
    
        private System.String _hotelID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.Int32 _hotelPriceID = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 HotelPriceID
        {
            get
            {
                return _hotelPriceID;
            }
            set
            {
                AddOriginal("HotelPriceID", _hotelPriceID, value);
                _hotelPriceID = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Int32 _isClose = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                AddOriginal("IsClose", _isClose, value);
                _isClose = value;
            }
        }
    
        private System.String _roomCurrencyCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomCurrencyCode
        {
            get
            {
                return _roomCurrencyCode;
            }
            set
            {
                AddOriginal("RoomCurrencyCode", _roomCurrencyCode, value);
                _roomCurrencyCode = value;
            }
        }
    
        private System.String _roomID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomID
        {
            get
            {
                return _roomID;
            }
            set
            {
                AddOriginal("RoomID", _roomID, value);
                _roomID = value;
            }
        }
    
        private System.String _roomName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomName
        {
            get
            {
                return _roomName;
            }
            set
            {
                AddOriginal("RoomName", _roomName, value);
                _roomName = value;
            }
        }
    
        private System.String _status = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Status
        {
            get
            {
                return _status;
            }
            set
            {
                AddOriginal("Status", _status, value);
                _status = value;
            }
        }
    
        private System.Int32 _supplierRefID = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 SupplierRefID
        {
            get
            {
                return _supplierRefID;
            }
            set
            {
                AddOriginal("SupplierRefID", _supplierRefID, value);
                _supplierRefID = value;
            }
        }
    
        private System.String _supplierRefValue = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String SupplierRefValue
        {
            get
            {
                return _supplierRefValue;
            }
            set
            {
                AddOriginal("SupplierRefValue", _supplierRefValue, value);
                _supplierRefValue = value;
            }
        }
    
        private System.Decimal _totalPrice = 0.0m;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Decimal TotalPrice
        {
            get
            {
                return _totalPrice;
            }
            set
            {
                AddOriginal("TotalPrice", _totalPrice, value);
                _totalPrice = value;
            }
        }
    
        private System.DateTime _updateTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime UpdateTime
        {
            get
            {
                return _updateTime;
            }
            set
            {
                AddOriginal("UpdateTime", _updateTime, value);
                _updateTime = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static HotelRecheckPrice DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as HotelRecheckPrice;
        }
    
        public HotelRecheckPrice Clone()
        {
            return EntityBase.Clone(this) as HotelRecheckPrice;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class HotelRecheckPriceCollection : EntityCollectionBase
    {

        public HotelRecheckPriceCollection()
        {
        }

        private static Type m_typ = typeof(HotelRecheckPriceCollection);
        private static Type m_typItem = typeof(HotelRecheckPrice);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public HotelRecheckPrice this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (HotelRecheckPrice)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public HotelRecheckPrice this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new HotelRecheckPrice GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as HotelRecheckPrice;
        }

        public int Add(HotelRecheckPrice value)
        {
            return List.Add(value);
        }

        public void Insert(int index, HotelRecheckPrice value)
        {
            List.Insert(index, value);
        }

        public void Remove(HotelRecheckPrice value)
        {
            List.Remove(value);
        }

        public void Delete(HotelRecheckPrice value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new HotelRecheckPriceCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(HotelRecheckPrice value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(HotelRecheckPrice value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class HotelRecheckPriceQuery : XiWan.DALFactory.EntityQueryBase
    {

        public HotelRecheckPriceQuery()
        {
            m_strTableName = "HotelRecheckPrice";

            m_prmAdults.SourceColumn = "Adults";
            this.Parameters.Add(m_prmAdults);

            m_prmAge.SourceColumn = "Age";
            this.Parameters.Add(m_prmAge);

            m_prmAgreementID.SourceColumn = "AgreementID";
            this.Parameters.Add(m_prmAgreementID);

            m_prmBBCode.SourceColumn = "BBCode";
            this.Parameters.Add(m_prmBBCode);

            m_prmChilds.SourceColumn = "Childs";
            this.Parameters.Add(m_prmChilds);

            m_prmCreateTime.SourceColumn = "CreateTime";
            this.Parameters.Add(m_prmCreateTime);

            m_prmDailyCurrencyCode.SourceColumn = "DailyCurrencyCode";
            this.Parameters.Add(m_prmDailyCurrencyCode);

            m_prmDailyRate.SourceColumn = "DailyRate";
            this.Parameters.Add(m_prmDailyRate);

            m_prmDateOfRate.SourceColumn = "DateOfRate";
            this.Parameters.Add(m_prmDateOfRate);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmHotelPriceID.SourceColumn = "HotelPriceID";
            this.Parameters.Add(m_prmHotelPriceID);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsClose.SourceColumn = "IsClose";
            this.Parameters.Add(m_prmIsClose);

            m_prmRoomCurrencyCode.SourceColumn = "RoomCurrencyCode";
            this.Parameters.Add(m_prmRoomCurrencyCode);

            m_prmRoomID.SourceColumn = "RoomID";
            this.Parameters.Add(m_prmRoomID);

            m_prmRoomName.SourceColumn = "RoomName";
            this.Parameters.Add(m_prmRoomName);

            m_prmStatus.SourceColumn = "Status";
            this.Parameters.Add(m_prmStatus);

            m_prmSupplierRefID.SourceColumn = "SupplierRefID";
            this.Parameters.Add(m_prmSupplierRefID);

            m_prmSupplierRefValue.SourceColumn = "SupplierRefValue";
            this.Parameters.Add(m_prmSupplierRefValue);

            m_prmTotalPrice.SourceColumn = "TotalPrice";
            this.Parameters.Add(m_prmTotalPrice);

            m_prmUpdateTime.SourceColumn = "UpdateTime";
            this.Parameters.Add(m_prmUpdateTime);
        }
        private IDataParameter m_prmAdults= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["Adults"]);
        public IDataParameter Adults
        {
            get { return m_prmAdults; }
            set { m_prmAdults = value; }
        }

        private IDataParameter m_prmAge= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["Age"]);
        public IDataParameter Age
        {
            get { return m_prmAge; }
            set { m_prmAge = value; }
        }

        private IDataParameter m_prmAgreementID= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["AgreementID"]);
        public IDataParameter AgreementID
        {
            get { return m_prmAgreementID; }
            set { m_prmAgreementID = value; }
        }

        private IDataParameter m_prmBBCode= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["BBCode"]);
        public IDataParameter BBCode
        {
            get { return m_prmBBCode; }
            set { m_prmBBCode = value; }
        }

        private IDataParameter m_prmChilds= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["Childs"]);
        public IDataParameter Childs
        {
            get { return m_prmChilds; }
            set { m_prmChilds = value; }
        }

        private IDataParameter m_prmCreateTime= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["CreateTime"]);
        public IDataParameter CreateTime
        {
            get { return m_prmCreateTime; }
            set { m_prmCreateTime = value; }
        }

        private IDataParameter m_prmDailyCurrencyCode= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["DailyCurrencyCode"]);
        public IDataParameter DailyCurrencyCode
        {
            get { return m_prmDailyCurrencyCode; }
            set { m_prmDailyCurrencyCode = value; }
        }

        private IDataParameter m_prmDailyRate= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["DailyRate"]);
        public IDataParameter DailyRate
        {
            get { return m_prmDailyRate; }
            set { m_prmDailyRate = value; }
        }

        private IDataParameter m_prmDateOfRate= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["DateOfRate"]);
        public IDataParameter DateOfRate
        {
            get { return m_prmDateOfRate; }
            set { m_prmDateOfRate = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmHotelPriceID= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["HotelPriceID"]);
        public IDataParameter HotelPriceID
        {
            get { return m_prmHotelPriceID; }
            set { m_prmHotelPriceID = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsClose= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["IsClose"]);
        public IDataParameter IsClose
        {
            get { return m_prmIsClose; }
            set { m_prmIsClose = value; }
        }

        private IDataParameter m_prmRoomCurrencyCode= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["RoomCurrencyCode"]);
        public IDataParameter RoomCurrencyCode
        {
            get { return m_prmRoomCurrencyCode; }
            set { m_prmRoomCurrencyCode = value; }
        }

        private IDataParameter m_prmRoomID= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["RoomID"]);
        public IDataParameter RoomID
        {
            get { return m_prmRoomID; }
            set { m_prmRoomID = value; }
        }

        private IDataParameter m_prmRoomName= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["RoomName"]);
        public IDataParameter RoomName
        {
            get { return m_prmRoomName; }
            set { m_prmRoomName = value; }
        }

        private IDataParameter m_prmStatus= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["Status"]);
        public IDataParameter Status
        {
            get { return m_prmStatus; }
            set { m_prmStatus = value; }
        }

        private IDataParameter m_prmSupplierRefID= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["SupplierRefID"]);
        public IDataParameter SupplierRefID
        {
            get { return m_prmSupplierRefID; }
            set { m_prmSupplierRefID = value; }
        }

        private IDataParameter m_prmSupplierRefValue= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["SupplierRefValue"]);
        public IDataParameter SupplierRefValue
        {
            get { return m_prmSupplierRefValue; }
            set { m_prmSupplierRefValue = value; }
        }

        private IDataParameter m_prmTotalPrice= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["TotalPrice"]);
        public IDataParameter TotalPrice
        {
            get { return m_prmTotalPrice; }
            set { m_prmTotalPrice = value; }
        }

        private IDataParameter m_prmUpdateTime= EntityBase.NewParameter(HotelRecheckPrice.m_dtbEntitySchema.Columns["UpdateTime"]);
        public IDataParameter UpdateTime
        {
            get { return m_prmUpdateTime; }
            set { m_prmUpdateTime = value; }
        }

    }
    #endregion
}