// [实体版本]v2.7
// 2018-09-03 11:22:32
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class SunSeriesCity : EntityBase
    {

        #region 初始化
        public SunSeriesCity() : base()
        {
        }
    
        public SunSeriesCity(e_EntityState state) : base(state)
        {
        }
    
        static SunSeriesCity()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("SunSeriesCity");
    
            DataColumn dclCityID = new DataColumn(c_CityID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityID);
            dclCityID.Caption = "";
            dclCityID.AllowDBNull = true;
            dclCityID.MaxLength = 100;
    
            DataColumn dclCityName = new DataColumn(c_CityName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityName);
            dclCityName.Caption = "";
            dclCityName.AllowDBNull = true;
            dclCityName.MaxLength = 200;
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 50;
    
            DataColumn dclCountryID = new DataColumn(c_CountryID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryID);
            dclCountryID.Caption = "";
            dclCountryID.AllowDBNull = true;
            dclCountryID.MaxLength = 200;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(SunSeriesCity);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "CityID,CityName,CountryCode,CountryID,ID";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_CityID = "CityID";
        public const string c_CityName = "CityName";
        public const string c_CountryCode = "CountryCode";
        public const string c_CountryID = "CountryID";
        public const string c_ID = "ID";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _cityID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityID
        {
            get
            {
                return _cityID;
            }
            set
            {
                AddOriginal("CityID", _cityID, value);
                _cityID = value;
            }
        }
    
        private System.String _cityName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                AddOriginal("CityName", _cityName, value);
                _cityName = value;
            }
        }
    
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _countryID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryID
        {
            get
            {
                return _countryID;
            }
            set
            {
                AddOriginal("CountryID", _countryID, value);
                _countryID = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static SunSeriesCity DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as SunSeriesCity;
        }
    
        public SunSeriesCity Clone()
        {
            return EntityBase.Clone(this) as SunSeriesCity;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class SunSeriesCityCollection : EntityCollectionBase
    {

        public SunSeriesCityCollection()
        {
        }

        private static Type m_typ = typeof(SunSeriesCityCollection);
        private static Type m_typItem = typeof(SunSeriesCity);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public SunSeriesCity this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (SunSeriesCity)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public SunSeriesCity this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new SunSeriesCity GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as SunSeriesCity;
        }

        public int Add(SunSeriesCity value)
        {
            return List.Add(value);
        }

        public void Insert(int index, SunSeriesCity value)
        {
            List.Insert(index, value);
        }

        public void Remove(SunSeriesCity value)
        {
            List.Remove(value);
        }

        public void Delete(SunSeriesCity value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new SunSeriesCityCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(SunSeriesCity value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(SunSeriesCity value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class SunSeriesCityQuery : XiWan.DALFactory.EntityQueryBase
    {

        public SunSeriesCityQuery()
        {
            m_strTableName = "SunSeriesCity";

            m_prmCityID.SourceColumn = "CityID";
            this.Parameters.Add(m_prmCityID);

            m_prmCityName.SourceColumn = "CityName";
            this.Parameters.Add(m_prmCityName);

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmCountryID.SourceColumn = "CountryID";
            this.Parameters.Add(m_prmCountryID);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);
        }
        private IDataParameter m_prmCityID= EntityBase.NewParameter(SunSeriesCity.m_dtbEntitySchema.Columns["CityID"]);
        public IDataParameter CityID
        {
            get { return m_prmCityID; }
            set { m_prmCityID = value; }
        }

        private IDataParameter m_prmCityName= EntityBase.NewParameter(SunSeriesCity.m_dtbEntitySchema.Columns["CityName"]);
        public IDataParameter CityName
        {
            get { return m_prmCityName; }
            set { m_prmCityName = value; }
        }

        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(SunSeriesCity.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmCountryID= EntityBase.NewParameter(SunSeriesCity.m_dtbEntitySchema.Columns["CountryID"]);
        public IDataParameter CountryID
        {
            get { return m_prmCountryID; }
            set { m_prmCountryID = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(SunSeriesCity.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

    }
    #endregion
}