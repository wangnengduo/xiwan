// [实体版本]v2.7
// 2018-08-17 11:57:01
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class RestelTown : EntityBase
    {

        #region 初始化
        public RestelTown() : base()
        {
        }
    
        public RestelTown(e_EntityState state) : base(state)
        {
        }
    
        static RestelTown()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("RestelTown");
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = false;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclProvinceCode = new DataColumn(c_ProvinceCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclProvinceCode);
            dclProvinceCode.Caption = "";
            dclProvinceCode.AllowDBNull = true;
            dclProvinceCode.MaxLength = 50;
    
            DataColumn dclTownCode = new DataColumn(c_TownCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownCode);
            dclTownCode.Caption = "";
            dclTownCode.AllowDBNull = true;
            dclTownCode.MaxLength = 50;
    
            DataColumn dclTownName = new DataColumn(c_TownName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownName);
            dclTownName.Caption = "";
            dclTownName.AllowDBNull = true;
            dclTownName.MaxLength = 500;
    
            DataColumn dclTownNameEn = new DataColumn(c_TownNameEn,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownNameEn);
            dclTownNameEn.Caption = "";
            dclTownNameEn.AllowDBNull = true;
            dclTownNameEn.MaxLength = 500;
    
            DataColumn dclTownNameFr = new DataColumn(c_TownNameFr,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownNameFr);
            dclTownNameFr.Caption = "";
            dclTownNameFr.AllowDBNull = true;
            dclTownNameFr.MaxLength = 500;
    
            DataColumn dclTownNameGe = new DataColumn(c_TownNameGe,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownNameGe);
            dclTownNameGe.Caption = "";
            dclTownNameGe.AllowDBNull = true;
            dclTownNameGe.MaxLength = 500;
    
            DataColumn dclTownNameIt = new DataColumn(c_TownNameIt,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownNameIt);
            dclTownNameIt.Caption = "";
            dclTownNameIt.AllowDBNull = true;
            dclTownNameIt.MaxLength = 500;
    
            DataColumn dclTownNamePo = new DataColumn(c_TownNamePo,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownNamePo);
            dclTownNamePo.Caption = "";
            dclTownNamePo.AllowDBNull = true;
            dclTownNamePo.MaxLength = 500;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(RestelTown);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "ID,ProvinceCode,TownCode,TownName,TownNameEn,TownNameFr,TownNameGe,TownNameIt,TownNamePo";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_ID = "ID";
        public const string c_ProvinceCode = "ProvinceCode";
        public const string c_TownCode = "TownCode";
        public const string c_TownName = "TownName";
        public const string c_TownNameEn = "TownNameEn";
        public const string c_TownNameFr = "TownNameFr";
        public const string c_TownNameGe = "TownNameGe";
        public const string c_TownNameIt = "TownNameIt";
        public const string c_TownNamePo = "TownNamePo";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _provinceCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ProvinceCode
        {
            get
            {
                return _provinceCode;
            }
            set
            {
                AddOriginal("ProvinceCode", _provinceCode, value);
                _provinceCode = value;
            }
        }
    
        private System.String _townCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownCode
        {
            get
            {
                return _townCode;
            }
            set
            {
                AddOriginal("TownCode", _townCode, value);
                _townCode = value;
            }
        }
    
        private System.String _townName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownName
        {
            get
            {
                return _townName;
            }
            set
            {
                AddOriginal("TownName", _townName, value);
                _townName = value;
            }
        }
    
        private System.String _townNameEn = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownNameEn
        {
            get
            {
                return _townNameEn;
            }
            set
            {
                AddOriginal("TownNameEn", _townNameEn, value);
                _townNameEn = value;
            }
        }
    
        private System.String _townNameFr = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownNameFr
        {
            get
            {
                return _townNameFr;
            }
            set
            {
                AddOriginal("TownNameFr", _townNameFr, value);
                _townNameFr = value;
            }
        }
    
        private System.String _townNameGe = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownNameGe
        {
            get
            {
                return _townNameGe;
            }
            set
            {
                AddOriginal("TownNameGe", _townNameGe, value);
                _townNameGe = value;
            }
        }
    
        private System.String _townNameIt = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownNameIt
        {
            get
            {
                return _townNameIt;
            }
            set
            {
                AddOriginal("TownNameIt", _townNameIt, value);
                _townNameIt = value;
            }
        }
    
        private System.String _townNamePo = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownNamePo
        {
            get
            {
                return _townNamePo;
            }
            set
            {
                AddOriginal("TownNamePo", _townNamePo, value);
                _townNamePo = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static RestelTown DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as RestelTown;
        }
    
        public RestelTown Clone()
        {
            return EntityBase.Clone(this) as RestelTown;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class RestelTownCollection : EntityCollectionBase
    {

        public RestelTownCollection()
        {
        }

        private static Type m_typ = typeof(RestelTownCollection);
        private static Type m_typItem = typeof(RestelTown);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public RestelTown this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (RestelTown)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public RestelTown this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new RestelTown GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as RestelTown;
        }

        public int Add(RestelTown value)
        {
            return List.Add(value);
        }

        public void Insert(int index, RestelTown value)
        {
            List.Insert(index, value);
        }

        public void Remove(RestelTown value)
        {
            List.Remove(value);
        }

        public void Delete(RestelTown value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new RestelTownCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(RestelTown value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(RestelTown value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class RestelTownQuery : XiWan.DALFactory.EntityQueryBase
    {

        public RestelTownQuery()
        {
            m_strTableName = "RestelTown";

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmProvinceCode.SourceColumn = "ProvinceCode";
            this.Parameters.Add(m_prmProvinceCode);

            m_prmTownCode.SourceColumn = "TownCode";
            this.Parameters.Add(m_prmTownCode);

            m_prmTownName.SourceColumn = "TownName";
            this.Parameters.Add(m_prmTownName);

            m_prmTownNameEn.SourceColumn = "TownNameEn";
            this.Parameters.Add(m_prmTownNameEn);

            m_prmTownNameFr.SourceColumn = "TownNameFr";
            this.Parameters.Add(m_prmTownNameFr);

            m_prmTownNameGe.SourceColumn = "TownNameGe";
            this.Parameters.Add(m_prmTownNameGe);

            m_prmTownNameIt.SourceColumn = "TownNameIt";
            this.Parameters.Add(m_prmTownNameIt);

            m_prmTownNamePo.SourceColumn = "TownNamePo";
            this.Parameters.Add(m_prmTownNamePo);
        }
        private IDataParameter m_prmID= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmProvinceCode= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["ProvinceCode"]);
        public IDataParameter ProvinceCode
        {
            get { return m_prmProvinceCode; }
            set { m_prmProvinceCode = value; }
        }

        private IDataParameter m_prmTownCode= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["TownCode"]);
        public IDataParameter TownCode
        {
            get { return m_prmTownCode; }
            set { m_prmTownCode = value; }
        }

        private IDataParameter m_prmTownName= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["TownName"]);
        public IDataParameter TownName
        {
            get { return m_prmTownName; }
            set { m_prmTownName = value; }
        }

        private IDataParameter m_prmTownNameEn= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["TownNameEn"]);
        public IDataParameter TownNameEn
        {
            get { return m_prmTownNameEn; }
            set { m_prmTownNameEn = value; }
        }

        private IDataParameter m_prmTownNameFr= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["TownNameFr"]);
        public IDataParameter TownNameFr
        {
            get { return m_prmTownNameFr; }
            set { m_prmTownNameFr = value; }
        }

        private IDataParameter m_prmTownNameGe= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["TownNameGe"]);
        public IDataParameter TownNameGe
        {
            get { return m_prmTownNameGe; }
            set { m_prmTownNameGe = value; }
        }

        private IDataParameter m_prmTownNameIt= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["TownNameIt"]);
        public IDataParameter TownNameIt
        {
            get { return m_prmTownNameIt; }
            set { m_prmTownNameIt = value; }
        }

        private IDataParameter m_prmTownNamePo= EntityBase.NewParameter(RestelTown.m_dtbEntitySchema.Columns["TownNamePo"]);
        public IDataParameter TownNamePo
        {
            get { return m_prmTownNamePo; }
            set { m_prmTownNamePo = value; }
        }

    }
    #endregion
}