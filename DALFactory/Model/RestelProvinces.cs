// [实体版本]v2.7
// 2018-08-07 10:07:08
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class RestelProvinces : EntityBase
    {

        #region 初始化
        public RestelProvinces() : base()
        {
        }
    
        public RestelProvinces(e_EntityState state) : base(state)
        {
        }
    
        static RestelProvinces()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("RestelProvinces");
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 50;
    
            DataColumn dclProvinceCode = new DataColumn(c_ProvinceCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclProvinceCode);
            dclProvinceCode.Caption = "";
            dclProvinceCode.AllowDBNull = true;
            dclProvinceCode.MaxLength = 50;
    
            DataColumn dclProvinceName = new DataColumn(c_ProvinceName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclProvinceName);
            dclProvinceName.Caption = "";
            dclProvinceName.AllowDBNull = true;
            dclProvinceName.MaxLength = 500;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] {  };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(RestelProvinces);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "CountryCode,ProvinceCode,ProvinceName";
        private static string m_strEntityIdentityColumnName = "";
        private static string m_strSqlWhere = "";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_CountryCode = "CountryCode";
        public const string c_ProvinceCode = "ProvinceCode";
        public const string c_ProvinceName = "ProvinceName";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _provinceCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ProvinceCode
        {
            get
            {
                return _provinceCode;
            }
            set
            {
                AddOriginal("ProvinceCode", _provinceCode, value);
                _provinceCode = value;
            }
        }
    
        private System.String _provinceName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ProvinceName
        {
            get
            {
                return _provinceName;
            }
            set
            {
                AddOriginal("ProvinceName", _provinceName, value);
                _provinceName = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] {  };
                }
            }
        }
        #endregion

        #region 操作
        public static RestelProvinces DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as RestelProvinces;
        }
    
        public RestelProvinces Clone()
        {
            return EntityBase.Clone(this) as RestelProvinces;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class RestelProvincesCollection : EntityCollectionBase
    {

        public RestelProvincesCollection()
        {
        }

        private static Type m_typ = typeof(RestelProvincesCollection);
        private static Type m_typItem = typeof(RestelProvinces);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public RestelProvinces this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (RestelProvinces)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public RestelProvinces this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new RestelProvinces GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as RestelProvinces;
        }

        public int Add(RestelProvinces value)
        {
            return List.Add(value);
        }

        public void Insert(int index, RestelProvinces value)
        {
            List.Insert(index, value);
        }

        public void Remove(RestelProvinces value)
        {
            List.Remove(value);
        }

        public void Delete(RestelProvinces value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new RestelProvincesCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(RestelProvinces value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(RestelProvinces value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class RestelProvincesQuery : XiWan.DALFactory.EntityQueryBase
    {

        public RestelProvincesQuery()
        {
            m_strTableName = "RestelProvinces";

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmProvinceCode.SourceColumn = "ProvinceCode";
            this.Parameters.Add(m_prmProvinceCode);

            m_prmProvinceName.SourceColumn = "ProvinceName";
            this.Parameters.Add(m_prmProvinceName);
        }
        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(RestelProvinces.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmProvinceCode= EntityBase.NewParameter(RestelProvinces.m_dtbEntitySchema.Columns["ProvinceCode"]);
        public IDataParameter ProvinceCode
        {
            get { return m_prmProvinceCode; }
            set { m_prmProvinceCode = value; }
        }

        private IDataParameter m_prmProvinceName= EntityBase.NewParameter(RestelProvinces.m_dtbEntitySchema.Columns["ProvinceName"]);
        public IDataParameter ProvinceName
        {
            get { return m_prmProvinceName; }
            set { m_prmProvinceName = value; }
        }

    }
    #endregion
}