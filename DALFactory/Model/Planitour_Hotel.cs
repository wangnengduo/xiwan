// [实体版本]v2.7
// 2018-07-20 14:48:08
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class Planitour_Hotel : EntityBase
    {

        #region 初始化
        public Planitour_Hotel() : base()
        {
        }
    
        public Planitour_Hotel(e_EntityState state) : base(state)
        {
        }
    
        static Planitour_Hotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("Planitour_Hotel");
    
            DataColumn dclCategory = new DataColumn(c_Category,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCategory);
            dclCategory.Caption = "";
            dclCategory.AllowDBNull = true;
            dclCategory.MaxLength = 200;
    
            DataColumn dclDestinationId = new DataColumn(c_DestinationId,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclDestinationId);
            dclDestinationId.Caption = "";
            dclDestinationId.AllowDBNull = true;
    
            DataColumn dclHotelCode = new DataColumn(c_HotelCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelCode);
            dclHotelCode.Caption = "";
            dclHotelCode.AllowDBNull = true;
            dclHotelCode.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclName = new DataColumn(c_Name,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclName);
            dclName.Caption = "";
            dclName.AllowDBNull = true;
            dclName.MaxLength = 50;
    
            DataColumn dclPropertyType = new DataColumn(c_PropertyType,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclPropertyType);
            dclPropertyType.Caption = "";
            dclPropertyType.AllowDBNull = true;
    
            DataColumn dclStarRating = new DataColumn(c_StarRating,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStarRating);
            dclStarRating.Caption = "";
            dclStarRating.AllowDBNull = true;
            dclStarRating.MaxLength = 50;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(Planitour_Hotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Category,DestinationId,HotelCode,ID,Name,PropertyType,StarRating";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Category = "Category";
        public const string c_DestinationId = "DestinationId";
        public const string c_HotelCode = "HotelCode";
        public const string c_ID = "ID";
        public const string c_Name = "Name";
        public const string c_PropertyType = "PropertyType";
        public const string c_StarRating = "StarRating";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _category = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Category
        {
            get
            {
                return _category;
            }
            set
            {
                AddOriginal("Category", _category, value);
                _category = value;
            }
        }
    
        private System.Int32 _destinationId = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 DestinationId
        {
            get
            {
                return _destinationId;
            }
            set
            {
                AddOriginal("DestinationId", _destinationId, value);
                _destinationId = value;
            }
        }
    
        private System.String _hotelCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelCode
        {
            get
            {
                return _hotelCode;
            }
            set
            {
                AddOriginal("HotelCode", _hotelCode, value);
                _hotelCode = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _name = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Name
        {
            get
            {
                return _name;
            }
            set
            {
                AddOriginal("Name", _name, value);
                _name = value;
            }
        }
    
        private System.Int32 _propertyType = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 PropertyType
        {
            get
            {
                return _propertyType;
            }
            set
            {
                AddOriginal("PropertyType", _propertyType, value);
                _propertyType = value;
            }
        }
    
        private System.String _starRating = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String StarRating
        {
            get
            {
                return _starRating;
            }
            set
            {
                AddOriginal("StarRating", _starRating, value);
                _starRating = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static Planitour_Hotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as Planitour_Hotel;
        }
    
        public Planitour_Hotel Clone()
        {
            return EntityBase.Clone(this) as Planitour_Hotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class Planitour_HotelCollection : EntityCollectionBase
    {

        public Planitour_HotelCollection()
        {
        }

        private static Type m_typ = typeof(Planitour_HotelCollection);
        private static Type m_typItem = typeof(Planitour_Hotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public Planitour_Hotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (Planitour_Hotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public Planitour_Hotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new Planitour_Hotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as Planitour_Hotel;
        }

        public int Add(Planitour_Hotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, Planitour_Hotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(Planitour_Hotel value)
        {
            List.Remove(value);
        }

        public void Delete(Planitour_Hotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new Planitour_HotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(Planitour_Hotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(Planitour_Hotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class Planitour_HotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public Planitour_HotelQuery()
        {
            m_strTableName = "Planitour_Hotel";

            m_prmCategory.SourceColumn = "Category";
            this.Parameters.Add(m_prmCategory);

            m_prmDestinationId.SourceColumn = "DestinationId";
            this.Parameters.Add(m_prmDestinationId);

            m_prmHotelCode.SourceColumn = "HotelCode";
            this.Parameters.Add(m_prmHotelCode);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmName.SourceColumn = "Name";
            this.Parameters.Add(m_prmName);

            m_prmPropertyType.SourceColumn = "PropertyType";
            this.Parameters.Add(m_prmPropertyType);

            m_prmStarRating.SourceColumn = "StarRating";
            this.Parameters.Add(m_prmStarRating);
        }
        private IDataParameter m_prmCategory= EntityBase.NewParameter(Planitour_Hotel.m_dtbEntitySchema.Columns["Category"]);
        public IDataParameter Category
        {
            get { return m_prmCategory; }
            set { m_prmCategory = value; }
        }

        private IDataParameter m_prmDestinationId= EntityBase.NewParameter(Planitour_Hotel.m_dtbEntitySchema.Columns["DestinationId"]);
        public IDataParameter DestinationId
        {
            get { return m_prmDestinationId; }
            set { m_prmDestinationId = value; }
        }

        private IDataParameter m_prmHotelCode= EntityBase.NewParameter(Planitour_Hotel.m_dtbEntitySchema.Columns["HotelCode"]);
        public IDataParameter HotelCode
        {
            get { return m_prmHotelCode; }
            set { m_prmHotelCode = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(Planitour_Hotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmName= EntityBase.NewParameter(Planitour_Hotel.m_dtbEntitySchema.Columns["Name"]);
        public IDataParameter Name
        {
            get { return m_prmName; }
            set { m_prmName = value; }
        }

        private IDataParameter m_prmPropertyType= EntityBase.NewParameter(Planitour_Hotel.m_dtbEntitySchema.Columns["PropertyType"]);
        public IDataParameter PropertyType
        {
            get { return m_prmPropertyType; }
            set { m_prmPropertyType = value; }
        }

        private IDataParameter m_prmStarRating= EntityBase.NewParameter(Planitour_Hotel.m_dtbEntitySchema.Columns["StarRating"]);
        public IDataParameter StarRating
        {
            get { return m_prmStarRating; }
            set { m_prmStarRating = value; }
        }

    }
    #endregion
}