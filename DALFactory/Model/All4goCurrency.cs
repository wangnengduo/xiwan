// [实体版本]v2.7
// 2018-09-29 15:48:02
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class All4goCurrency : EntityBase
    {

        #region 初始化
        public All4goCurrency() : base()
        {
        }
    
        public All4goCurrency(e_EntityState state) : base(state)
        {
        }
    
        static All4goCurrency()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("All4goCurrency");
    
            DataColumn dclagency_id = new DataColumn(c_agency_id,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclagency_id);
            dclagency_id.Caption = "";
            dclagency_id.AllowDBNull = true;
            dclagency_id.MaxLength = 50;
    
            DataColumn dclagencycurrency_id = new DataColumn(c_agencycurrency_id,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclagencycurrency_id);
            dclagencycurrency_id.Caption = "";
            dclagencycurrency_id.AllowDBNull = true;
            dclagencycurrency_id.MaxLength = 50;
    
            DataColumn dclcurrency_b2b = new DataColumn(c_currency_b2b,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclcurrency_b2b);
            dclcurrency_b2b.Caption = "";
            dclcurrency_b2b.AllowDBNull = true;
            dclcurrency_b2b.MaxLength = 50;
    
            DataColumn dclcurrency_b2c = new DataColumn(c_currency_b2c,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclcurrency_b2c);
            dclcurrency_b2c.Caption = "";
            dclcurrency_b2c.AllowDBNull = true;
            dclcurrency_b2c.MaxLength = 50;
    
            DataColumn dclcurrency_code = new DataColumn(c_currency_code,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclcurrency_code);
            dclcurrency_code.Caption = "";
            dclcurrency_code.AllowDBNull = true;
            dclcurrency_code.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(All4goCurrency);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "agency_id,agencycurrency_id,currency_b2b,currency_b2c,currency_code,ID";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_agency_id = "agency_id";
        public const string c_agencycurrency_id = "agencycurrency_id";
        public const string c_currency_b2b = "currency_b2b";
        public const string c_currency_b2c = "currency_b2c";
        public const string c_currency_code = "currency_code";
        public const string c_ID = "ID";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _agency_id = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String agency_id
        {
            get
            {
                return _agency_id;
            }
            set
            {
                AddOriginal("agency_id", _agency_id, value);
                _agency_id = value;
            }
        }
    
        private System.String _agencycurrency_id = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String agencycurrency_id
        {
            get
            {
                return _agencycurrency_id;
            }
            set
            {
                AddOriginal("agencycurrency_id", _agencycurrency_id, value);
                _agencycurrency_id = value;
            }
        }
    
        private System.String _currency_b2b = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String currency_b2b
        {
            get
            {
                return _currency_b2b;
            }
            set
            {
                AddOriginal("currency_b2b", _currency_b2b, value);
                _currency_b2b = value;
            }
        }
    
        private System.String _currency_b2c = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String currency_b2c
        {
            get
            {
                return _currency_b2c;
            }
            set
            {
                AddOriginal("currency_b2c", _currency_b2c, value);
                _currency_b2c = value;
            }
        }
    
        private System.String _currency_code = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String currency_code
        {
            get
            {
                return _currency_code;
            }
            set
            {
                AddOriginal("currency_code", _currency_code, value);
                _currency_code = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static All4goCurrency DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as All4goCurrency;
        }
    
        public All4goCurrency Clone()
        {
            return EntityBase.Clone(this) as All4goCurrency;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class All4goCurrencyCollection : EntityCollectionBase
    {

        public All4goCurrencyCollection()
        {
        }

        private static Type m_typ = typeof(All4goCurrencyCollection);
        private static Type m_typItem = typeof(All4goCurrency);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public All4goCurrency this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (All4goCurrency)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public All4goCurrency this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new All4goCurrency GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as All4goCurrency;
        }

        public int Add(All4goCurrency value)
        {
            return List.Add(value);
        }

        public void Insert(int index, All4goCurrency value)
        {
            List.Insert(index, value);
        }

        public void Remove(All4goCurrency value)
        {
            List.Remove(value);
        }

        public void Delete(All4goCurrency value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new All4goCurrencyCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(All4goCurrency value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(All4goCurrency value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class All4goCurrencyQuery : XiWan.DALFactory.EntityQueryBase
    {

        public All4goCurrencyQuery()
        {
            m_strTableName = "All4goCurrency";

            m_prmagency_id.SourceColumn = "agency_id";
            this.Parameters.Add(m_prmagency_id);

            m_prmagencycurrency_id.SourceColumn = "agencycurrency_id";
            this.Parameters.Add(m_prmagencycurrency_id);

            m_prmcurrency_b2b.SourceColumn = "currency_b2b";
            this.Parameters.Add(m_prmcurrency_b2b);

            m_prmcurrency_b2c.SourceColumn = "currency_b2c";
            this.Parameters.Add(m_prmcurrency_b2c);

            m_prmcurrency_code.SourceColumn = "currency_code";
            this.Parameters.Add(m_prmcurrency_code);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);
        }
        private IDataParameter m_prmagency_id= EntityBase.NewParameter(All4goCurrency.m_dtbEntitySchema.Columns["agency_id"]);
        public IDataParameter agency_id
        {
            get { return m_prmagency_id; }
            set { m_prmagency_id = value; }
        }

        private IDataParameter m_prmagencycurrency_id= EntityBase.NewParameter(All4goCurrency.m_dtbEntitySchema.Columns["agencycurrency_id"]);
        public IDataParameter agencycurrency_id
        {
            get { return m_prmagencycurrency_id; }
            set { m_prmagencycurrency_id = value; }
        }

        private IDataParameter m_prmcurrency_b2b= EntityBase.NewParameter(All4goCurrency.m_dtbEntitySchema.Columns["currency_b2b"]);
        public IDataParameter currency_b2b
        {
            get { return m_prmcurrency_b2b; }
            set { m_prmcurrency_b2b = value; }
        }

        private IDataParameter m_prmcurrency_b2c= EntityBase.NewParameter(All4goCurrency.m_dtbEntitySchema.Columns["currency_b2c"]);
        public IDataParameter currency_b2c
        {
            get { return m_prmcurrency_b2c; }
            set { m_prmcurrency_b2c = value; }
        }

        private IDataParameter m_prmcurrency_code= EntityBase.NewParameter(All4goCurrency.m_dtbEntitySchema.Columns["currency_code"]);
        public IDataParameter currency_code
        {
            get { return m_prmcurrency_code; }
            set { m_prmcurrency_code = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(All4goCurrency.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

    }
    #endregion
}