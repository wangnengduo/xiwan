// [实体版本]v2.7
// 2018-08-07 16:08:52
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class RestelHotel : EntityBase
    {

        #region 初始化
        public RestelHotel() : base()
        {
        }
    
        public RestelHotel(e_EntityState state) : base(state)
        {
        }
    
        static RestelHotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("RestelHotel");
    
            DataColumn dclCalidad = new DataColumn(c_Calidad,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCalidad);
            dclCalidad.Caption = "";
            dclCalidad.AllowDBNull = true;
            dclCalidad.MaxLength = 50;
    
            DataColumn dclCategoria2 = new DataColumn(c_Categoria2,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCategoria2);
            dclCategoria2.Caption = "";
            dclCategoria2.AllowDBNull = true;
            dclCategoria2.MaxLength = 50;
    
            DataColumn dclHotelCobol = new DataColumn(c_HotelCobol,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelCobol);
            dclHotelCobol.Caption = "";
            dclHotelCobol.AllowDBNull = true;
            dclHotelCobol.MaxLength = 50;
    
            DataColumn dclHotelCode = new DataColumn(c_HotelCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelCode);
            dclHotelCode.Caption = "";
            dclHotelCode.AllowDBNull = true;
            dclHotelCode.MaxLength = 50;
    
            DataColumn dclHotelDescription = new DataColumn(c_HotelDescription,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelDescription);
            dclHotelDescription.Caption = "";
            dclHotelDescription.AllowDBNull = true;
            dclHotelDescription.MaxLength = 5000;
    
            DataColumn dclHotelName = new DataColumn(c_HotelName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelName);
            dclHotelName.Caption = "";
            dclHotelName.AllowDBNull = true;
            dclHotelName.MaxLength = 200;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclLatitud = new DataColumn(c_Latitud,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitud);
            dclLatitud.Caption = "";
            dclLatitud.AllowDBNull = true;
            dclLatitud.MaxLength = 50;
    
            DataColumn dclLongitud = new DataColumn(c_Longitud,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitud);
            dclLongitud.Caption = "";
            dclLongitud.AllowDBNull = true;
            dclLongitud.MaxLength = 50;
    
            DataColumn dclMarca = new DataColumn(c_Marca,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclMarca);
            dclMarca.Caption = "";
            dclMarca.AllowDBNull = true;
            dclMarca.MaxLength = 50;
    
            DataColumn dclProvinceCode = new DataColumn(c_ProvinceCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclProvinceCode);
            dclProvinceCode.Caption = "";
            dclProvinceCode.AllowDBNull = true;
            dclProvinceCode.MaxLength = 50;
    
            DataColumn dclProvinceName = new DataColumn(c_ProvinceName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclProvinceName);
            dclProvinceName.Caption = "";
            dclProvinceName.AllowDBNull = true;
            dclProvinceName.MaxLength = 50;
    
            DataColumn dclStaring = new DataColumn(c_Staring,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStaring);
            dclStaring.Caption = "";
            dclStaring.AllowDBNull = true;
            dclStaring.MaxLength = 50;
    
            DataColumn dclTownName = new DataColumn(c_TownName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownName);
            dclTownName.Caption = "";
            dclTownName.AllowDBNull = true;
            dclTownName.MaxLength = 50;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(RestelHotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Calidad,Categoria2,HotelCobol,HotelCode,HotelDescription,HotelName,ID,Latitud,Longitud,Marca,ProvinceCode,ProvinceName,Staring,TownName";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Calidad = "Calidad";
        public const string c_Categoria2 = "Categoria2";
        public const string c_HotelCobol = "HotelCobol";
        public const string c_HotelCode = "HotelCode";
        public const string c_HotelDescription = "HotelDescription";
        public const string c_HotelName = "HotelName";
        public const string c_ID = "ID";
        public const string c_Latitud = "Latitud";
        public const string c_Longitud = "Longitud";
        public const string c_Marca = "Marca";
        public const string c_ProvinceCode = "ProvinceCode";
        public const string c_ProvinceName = "ProvinceName";
        public const string c_Staring = "Staring";
        public const string c_TownName = "TownName";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _calidad = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Calidad
        {
            get
            {
                return _calidad;
            }
            set
            {
                AddOriginal("Calidad", _calidad, value);
                _calidad = value;
            }
        }
    
        private System.String _categoria2 = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Categoria2
        {
            get
            {
                return _categoria2;
            }
            set
            {
                AddOriginal("Categoria2", _categoria2, value);
                _categoria2 = value;
            }
        }
    
        private System.String _hotelCobol = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelCobol
        {
            get
            {
                return _hotelCobol;
            }
            set
            {
                AddOriginal("HotelCobol", _hotelCobol, value);
                _hotelCobol = value;
            }
        }
    
        private System.String _hotelCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelCode
        {
            get
            {
                return _hotelCode;
            }
            set
            {
                AddOriginal("HotelCode", _hotelCode, value);
                _hotelCode = value;
            }
        }
    
        private System.String _hotelDescription = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelDescription
        {
            get
            {
                return _hotelDescription;
            }
            set
            {
                AddOriginal("HotelDescription", _hotelDescription, value);
                _hotelDescription = value;
            }
        }
    
        private System.String _hotelName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelName
        {
            get
            {
                return _hotelName;
            }
            set
            {
                AddOriginal("HotelName", _hotelName, value);
                _hotelName = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _latitud = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitud
        {
            get
            {
                return _latitud;
            }
            set
            {
                AddOriginal("Latitud", _latitud, value);
                _latitud = value;
            }
        }
    
        private System.String _longitud = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitud
        {
            get
            {
                return _longitud;
            }
            set
            {
                AddOriginal("Longitud", _longitud, value);
                _longitud = value;
            }
        }
    
        private System.String _marca = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Marca
        {
            get
            {
                return _marca;
            }
            set
            {
                AddOriginal("Marca", _marca, value);
                _marca = value;
            }
        }
    
        private System.String _provinceCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ProvinceCode
        {
            get
            {
                return _provinceCode;
            }
            set
            {
                AddOriginal("ProvinceCode", _provinceCode, value);
                _provinceCode = value;
            }
        }
    
        private System.String _provinceName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ProvinceName
        {
            get
            {
                return _provinceName;
            }
            set
            {
                AddOriginal("ProvinceName", _provinceName, value);
                _provinceName = value;
            }
        }
    
        private System.String _staring = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Staring
        {
            get
            {
                return _staring;
            }
            set
            {
                AddOriginal("Staring", _staring, value);
                _staring = value;
            }
        }
    
        private System.String _townName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownName
        {
            get
            {
                return _townName;
            }
            set
            {
                AddOriginal("TownName", _townName, value);
                _townName = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static RestelHotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as RestelHotel;
        }
    
        public RestelHotel Clone()
        {
            return EntityBase.Clone(this) as RestelHotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class RestelHotelCollection : EntityCollectionBase
    {

        public RestelHotelCollection()
        {
        }

        private static Type m_typ = typeof(RestelHotelCollection);
        private static Type m_typItem = typeof(RestelHotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public RestelHotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (RestelHotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public RestelHotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new RestelHotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as RestelHotel;
        }

        public int Add(RestelHotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, RestelHotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(RestelHotel value)
        {
            List.Remove(value);
        }

        public void Delete(RestelHotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new RestelHotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(RestelHotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(RestelHotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class RestelHotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public RestelHotelQuery()
        {
            m_strTableName = "RestelHotel";

            m_prmCalidad.SourceColumn = "Calidad";
            this.Parameters.Add(m_prmCalidad);

            m_prmCategoria2.SourceColumn = "Categoria2";
            this.Parameters.Add(m_prmCategoria2);

            m_prmHotelCobol.SourceColumn = "HotelCobol";
            this.Parameters.Add(m_prmHotelCobol);

            m_prmHotelCode.SourceColumn = "HotelCode";
            this.Parameters.Add(m_prmHotelCode);

            m_prmHotelDescription.SourceColumn = "HotelDescription";
            this.Parameters.Add(m_prmHotelDescription);

            m_prmHotelName.SourceColumn = "HotelName";
            this.Parameters.Add(m_prmHotelName);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmLatitud.SourceColumn = "Latitud";
            this.Parameters.Add(m_prmLatitud);

            m_prmLongitud.SourceColumn = "Longitud";
            this.Parameters.Add(m_prmLongitud);

            m_prmMarca.SourceColumn = "Marca";
            this.Parameters.Add(m_prmMarca);

            m_prmProvinceCode.SourceColumn = "ProvinceCode";
            this.Parameters.Add(m_prmProvinceCode);

            m_prmProvinceName.SourceColumn = "ProvinceName";
            this.Parameters.Add(m_prmProvinceName);

            m_prmStaring.SourceColumn = "Staring";
            this.Parameters.Add(m_prmStaring);

            m_prmTownName.SourceColumn = "TownName";
            this.Parameters.Add(m_prmTownName);
        }
        private IDataParameter m_prmCalidad= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["Calidad"]);
        public IDataParameter Calidad
        {
            get { return m_prmCalidad; }
            set { m_prmCalidad = value; }
        }

        private IDataParameter m_prmCategoria2= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["Categoria2"]);
        public IDataParameter Categoria2
        {
            get { return m_prmCategoria2; }
            set { m_prmCategoria2 = value; }
        }

        private IDataParameter m_prmHotelCobol= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["HotelCobol"]);
        public IDataParameter HotelCobol
        {
            get { return m_prmHotelCobol; }
            set { m_prmHotelCobol = value; }
        }

        private IDataParameter m_prmHotelCode= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["HotelCode"]);
        public IDataParameter HotelCode
        {
            get { return m_prmHotelCode; }
            set { m_prmHotelCode = value; }
        }

        private IDataParameter m_prmHotelDescription= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["HotelDescription"]);
        public IDataParameter HotelDescription
        {
            get { return m_prmHotelDescription; }
            set { m_prmHotelDescription = value; }
        }

        private IDataParameter m_prmHotelName= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["HotelName"]);
        public IDataParameter HotelName
        {
            get { return m_prmHotelName; }
            set { m_prmHotelName = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmLatitud= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["Latitud"]);
        public IDataParameter Latitud
        {
            get { return m_prmLatitud; }
            set { m_prmLatitud = value; }
        }

        private IDataParameter m_prmLongitud= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["Longitud"]);
        public IDataParameter Longitud
        {
            get { return m_prmLongitud; }
            set { m_prmLongitud = value; }
        }

        private IDataParameter m_prmMarca= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["Marca"]);
        public IDataParameter Marca
        {
            get { return m_prmMarca; }
            set { m_prmMarca = value; }
        }

        private IDataParameter m_prmProvinceCode= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["ProvinceCode"]);
        public IDataParameter ProvinceCode
        {
            get { return m_prmProvinceCode; }
            set { m_prmProvinceCode = value; }
        }

        private IDataParameter m_prmProvinceName= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["ProvinceName"]);
        public IDataParameter ProvinceName
        {
            get { return m_prmProvinceName; }
            set { m_prmProvinceName = value; }
        }

        private IDataParameter m_prmStaring= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["Staring"]);
        public IDataParameter Staring
        {
            get { return m_prmStaring; }
            set { m_prmStaring = value; }
        }

        private IDataParameter m_prmTownName= EntityBase.NewParameter(RestelHotel.m_dtbEntitySchema.Columns["TownName"]);
        public IDataParameter TownName
        {
            get { return m_prmTownName; }
            set { m_prmTownName = value; }
        }

    }
    #endregion
}