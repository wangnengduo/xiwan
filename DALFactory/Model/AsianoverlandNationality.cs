// [实体版本]v2.7
// 2018-09-18 16:39:06
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AsianoverlandNationality : EntityBase
    {

        #region 初始化
        public AsianoverlandNationality() : base()
        {
        }
    
        public AsianoverlandNationality(e_EntityState state) : base(state)
        {
        }
    
        static AsianoverlandNationality()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AsianoverlandNationality");
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = false;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsoCode = new DataColumn(c_IsoCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclIsoCode);
            dclIsoCode.Caption = "";
            dclIsoCode.AllowDBNull = true;
            dclIsoCode.MaxLength = 50;
    
            DataColumn dclNationalityCode = new DataColumn(c_NationalityCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclNationalityCode);
            dclNationalityCode.Caption = "";
            dclNationalityCode.AllowDBNull = true;
            dclNationalityCode.MaxLength = 50;
    
            DataColumn dclNationalityName = new DataColumn(c_NationalityName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclNationalityName);
            dclNationalityName.Caption = "";
            dclNationalityName.AllowDBNull = true;
            dclNationalityName.MaxLength = 500;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AsianoverlandNationality);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "ID,IsoCode,NationalityCode,NationalityName";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_ID = "ID";
        public const string c_IsoCode = "IsoCode";
        public const string c_NationalityCode = "NationalityCode";
        public const string c_NationalityName = "NationalityName";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _isoCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String IsoCode
        {
            get
            {
                return _isoCode;
            }
            set
            {
                AddOriginal("IsoCode", _isoCode, value);
                _isoCode = value;
            }
        }
    
        private System.String _nationalityCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String NationalityCode
        {
            get
            {
                return _nationalityCode;
            }
            set
            {
                AddOriginal("NationalityCode", _nationalityCode, value);
                _nationalityCode = value;
            }
        }
    
        private System.String _nationalityName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String NationalityName
        {
            get
            {
                return _nationalityName;
            }
            set
            {
                AddOriginal("NationalityName", _nationalityName, value);
                _nationalityName = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AsianoverlandNationality DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AsianoverlandNationality;
        }
    
        public AsianoverlandNationality Clone()
        {
            return EntityBase.Clone(this) as AsianoverlandNationality;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AsianoverlandNationalityCollection : EntityCollectionBase
    {

        public AsianoverlandNationalityCollection()
        {
        }

        private static Type m_typ = typeof(AsianoverlandNationalityCollection);
        private static Type m_typItem = typeof(AsianoverlandNationality);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AsianoverlandNationality this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AsianoverlandNationality)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AsianoverlandNationality this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AsianoverlandNationality GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AsianoverlandNationality;
        }

        public int Add(AsianoverlandNationality value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AsianoverlandNationality value)
        {
            List.Insert(index, value);
        }

        public void Remove(AsianoverlandNationality value)
        {
            List.Remove(value);
        }

        public void Delete(AsianoverlandNationality value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AsianoverlandNationalityCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AsianoverlandNationality value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AsianoverlandNationality value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AsianoverlandNationalityQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AsianoverlandNationalityQuery()
        {
            m_strTableName = "AsianoverlandNationality";

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsoCode.SourceColumn = "IsoCode";
            this.Parameters.Add(m_prmIsoCode);

            m_prmNationalityCode.SourceColumn = "NationalityCode";
            this.Parameters.Add(m_prmNationalityCode);

            m_prmNationalityName.SourceColumn = "NationalityName";
            this.Parameters.Add(m_prmNationalityName);
        }
        private IDataParameter m_prmID= EntityBase.NewParameter(AsianoverlandNationality.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsoCode= EntityBase.NewParameter(AsianoverlandNationality.m_dtbEntitySchema.Columns["IsoCode"]);
        public IDataParameter IsoCode
        {
            get { return m_prmIsoCode; }
            set { m_prmIsoCode = value; }
        }

        private IDataParameter m_prmNationalityCode= EntityBase.NewParameter(AsianoverlandNationality.m_dtbEntitySchema.Columns["NationalityCode"]);
        public IDataParameter NationalityCode
        {
            get { return m_prmNationalityCode; }
            set { m_prmNationalityCode = value; }
        }

        private IDataParameter m_prmNationalityName= EntityBase.NewParameter(AsianoverlandNationality.m_dtbEntitySchema.Columns["NationalityName"]);
        public IDataParameter NationalityName
        {
            get { return m_prmNationalityName; }
            set { m_prmNationalityName = value; }
        }

    }
    #endregion
}