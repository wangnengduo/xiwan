// [实体版本]v2.7
// 2018-09-18 17:16:16
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AsianoverlandHotel : EntityBase
    {

        #region 初始化
        public AsianoverlandHotel() : base()
        {
        }
    
        public AsianoverlandHotel(e_EntityState state) : base(state)
        {
        }
    
        static AsianoverlandHotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AsianoverlandHotel");
    
            DataColumn dclAddress = new DataColumn(c_Address,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress);
            dclAddress.Caption = "";
            dclAddress.AllowDBNull = true;
            dclAddress.MaxLength = 500;
    
            DataColumn dclCityCode = new DataColumn(c_CityCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityCode);
            dclCityCode.Caption = "";
            dclCityCode.AllowDBNull = true;
            dclCityCode.MaxLength = 200;
    
            DataColumn dclCityName = new DataColumn(c_CityName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityName);
            dclCityName.Caption = "";
            dclCityName.AllowDBNull = true;
            dclCityName.MaxLength = 500;
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 50;
    
            DataColumn dclCountryName = new DataColumn(c_CountryName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryName);
            dclCountryName.Caption = "";
            dclCountryName.AllowDBNull = true;
            dclCountryName.MaxLength = 500;
    
            DataColumn dclEmail = new DataColumn(c_Email,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclEmail);
            dclEmail.Caption = "";
            dclEmail.AllowDBNull = true;
            dclEmail.MaxLength = 50;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "";
            dclHotelID.AllowDBNull = true;
            dclHotelID.MaxLength = 50;
    
            DataColumn dclHotelName = new DataColumn(c_HotelName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelName);
            dclHotelName.Caption = "";
            dclHotelName.AllowDBNull = true;
            dclHotelName.MaxLength = 500;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclLatitude = new DataColumn(c_Latitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitude);
            dclLatitude.Caption = "";
            dclLatitude.AllowDBNull = true;
            dclLatitude.MaxLength = 100;
    
            DataColumn dclLongitude = new DataColumn(c_Longitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitude);
            dclLongitude.Caption = "";
            dclLongitude.AllowDBNull = true;
            dclLongitude.MaxLength = 100;
    
            DataColumn dclPhone = new DataColumn(c_Phone,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPhone);
            dclPhone.Caption = "";
            dclPhone.AllowDBNull = true;
            dclPhone.MaxLength = 50;
    
            DataColumn dclRating = new DataColumn(c_Rating,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRating);
            dclRating.Caption = "";
            dclRating.AllowDBNull = true;
            dclRating.MaxLength = 50;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AsianoverlandHotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Address,CityCode,CityName,CountryCode,CountryName,Email,HotelID,HotelName,ID,Latitude,Longitude,Phone,Rating";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Address = "Address";
        public const string c_CityCode = "CityCode";
        public const string c_CityName = "CityName";
        public const string c_CountryCode = "CountryCode";
        public const string c_CountryName = "CountryName";
        public const string c_Email = "Email";
        public const string c_HotelID = "HotelID";
        public const string c_HotelName = "HotelName";
        public const string c_ID = "ID";
        public const string c_Latitude = "Latitude";
        public const string c_Longitude = "Longitude";
        public const string c_Phone = "Phone";
        public const string c_Rating = "Rating";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _address = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Address
        {
            get
            {
                return _address;
            }
            set
            {
                AddOriginal("Address", _address, value);
                _address = value;
            }
        }
    
        private System.String _cityCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityCode
        {
            get
            {
                return _cityCode;
            }
            set
            {
                AddOriginal("CityCode", _cityCode, value);
                _cityCode = value;
            }
        }
    
        private System.String _cityName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                AddOriginal("CityName", _cityName, value);
                _cityName = value;
            }
        }
    
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _countryName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                AddOriginal("CountryName", _countryName, value);
                _countryName = value;
            }
        }
    
        private System.String _email = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Email
        {
            get
            {
                return _email;
            }
            set
            {
                AddOriginal("Email", _email, value);
                _email = value;
            }
        }
    
        private System.String _hotelID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.String _hotelName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelName
        {
            get
            {
                return _hotelName;
            }
            set
            {
                AddOriginal("HotelName", _hotelName, value);
                _hotelName = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _latitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                AddOriginal("Latitude", _latitude, value);
                _latitude = value;
            }
        }
    
        private System.String _longitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                AddOriginal("Longitude", _longitude, value);
                _longitude = value;
            }
        }
    
        private System.String _phone = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                AddOriginal("Phone", _phone, value);
                _phone = value;
            }
        }
    
        private System.String _rating = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Rating
        {
            get
            {
                return _rating;
            }
            set
            {
                AddOriginal("Rating", _rating, value);
                _rating = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AsianoverlandHotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AsianoverlandHotel;
        }
    
        public AsianoverlandHotel Clone()
        {
            return EntityBase.Clone(this) as AsianoverlandHotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AsianoverlandHotelCollection : EntityCollectionBase
    {

        public AsianoverlandHotelCollection()
        {
        }

        private static Type m_typ = typeof(AsianoverlandHotelCollection);
        private static Type m_typItem = typeof(AsianoverlandHotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AsianoverlandHotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AsianoverlandHotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AsianoverlandHotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AsianoverlandHotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AsianoverlandHotel;
        }

        public int Add(AsianoverlandHotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AsianoverlandHotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(AsianoverlandHotel value)
        {
            List.Remove(value);
        }

        public void Delete(AsianoverlandHotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AsianoverlandHotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AsianoverlandHotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AsianoverlandHotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AsianoverlandHotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AsianoverlandHotelQuery()
        {
            m_strTableName = "AsianoverlandHotel";

            m_prmAddress.SourceColumn = "Address";
            this.Parameters.Add(m_prmAddress);

            m_prmCityCode.SourceColumn = "CityCode";
            this.Parameters.Add(m_prmCityCode);

            m_prmCityName.SourceColumn = "CityName";
            this.Parameters.Add(m_prmCityName);

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmCountryName.SourceColumn = "CountryName";
            this.Parameters.Add(m_prmCountryName);

            m_prmEmail.SourceColumn = "Email";
            this.Parameters.Add(m_prmEmail);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmHotelName.SourceColumn = "HotelName";
            this.Parameters.Add(m_prmHotelName);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmLatitude.SourceColumn = "Latitude";
            this.Parameters.Add(m_prmLatitude);

            m_prmLongitude.SourceColumn = "Longitude";
            this.Parameters.Add(m_prmLongitude);

            m_prmPhone.SourceColumn = "Phone";
            this.Parameters.Add(m_prmPhone);

            m_prmRating.SourceColumn = "Rating";
            this.Parameters.Add(m_prmRating);
        }
        private IDataParameter m_prmAddress= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["Address"]);
        public IDataParameter Address
        {
            get { return m_prmAddress; }
            set { m_prmAddress = value; }
        }

        private IDataParameter m_prmCityCode= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["CityCode"]);
        public IDataParameter CityCode
        {
            get { return m_prmCityCode; }
            set { m_prmCityCode = value; }
        }

        private IDataParameter m_prmCityName= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["CityName"]);
        public IDataParameter CityName
        {
            get { return m_prmCityName; }
            set { m_prmCityName = value; }
        }

        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmCountryName= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["CountryName"]);
        public IDataParameter CountryName
        {
            get { return m_prmCountryName; }
            set { m_prmCountryName = value; }
        }

        private IDataParameter m_prmEmail= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["Email"]);
        public IDataParameter Email
        {
            get { return m_prmEmail; }
            set { m_prmEmail = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmHotelName= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["HotelName"]);
        public IDataParameter HotelName
        {
            get { return m_prmHotelName; }
            set { m_prmHotelName = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmLatitude= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["Latitude"]);
        public IDataParameter Latitude
        {
            get { return m_prmLatitude; }
            set { m_prmLatitude = value; }
        }

        private IDataParameter m_prmLongitude= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["Longitude"]);
        public IDataParameter Longitude
        {
            get { return m_prmLongitude; }
            set { m_prmLongitude = value; }
        }

        private IDataParameter m_prmPhone= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["Phone"]);
        public IDataParameter Phone
        {
            get { return m_prmPhone; }
            set { m_prmPhone = value; }
        }

        private IDataParameter m_prmRating= EntityBase.NewParameter(AsianoverlandHotel.m_dtbEntitySchema.Columns["Rating"]);
        public IDataParameter Rating
        {
            get { return m_prmRating; }
            set { m_prmRating = value; }
        }

    }
    #endregion
}