// [实体版本]v2.7
// 2018-12-03 17:44:21
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AgodaHotel : EntityBase
    {

        #region 初始化
        public AgodaHotel() : base()
        {
        }
    
        public AgodaHotel(e_EntityState state) : base(state)
        {
        }
    
        static AgodaHotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AgodaHotel");
    
            DataColumn dclAccommodationType = new DataColumn(c_AccommodationType,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAccommodationType);
            dclAccommodationType.Caption = "";
            dclAccommodationType.AllowDBNull = true;
            dclAccommodationType.MaxLength = 50;
    
            DataColumn dclAddressEN = new DataColumn(c_AddressEN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddressEN);
            dclAddressEN.Caption = "";
            dclAddressEN.AllowDBNull = true;
            dclAddressEN.MaxLength = 0;
    
            DataColumn dclAreaId = new DataColumn(c_AreaId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAreaId);
            dclAreaId.Caption = "";
            dclAreaId.AllowDBNull = true;
            dclAreaId.MaxLength = 50;
    
            DataColumn dclChildrenAgeFrom = new DataColumn(c_ChildrenAgeFrom,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclChildrenAgeFrom);
            dclChildrenAgeFrom.Caption = "";
            dclChildrenAgeFrom.AllowDBNull = true;
            dclChildrenAgeFrom.MaxLength = 50;
    
            DataColumn dclChildrenAgeTo = new DataColumn(c_ChildrenAgeTo,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclChildrenAgeTo);
            dclChildrenAgeTo.Caption = "";
            dclChildrenAgeTo.AllowDBNull = true;
            dclChildrenAgeTo.MaxLength = 50;
    
            DataColumn dclChildrenStayFree = new DataColumn(c_ChildrenStayFree,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclChildrenStayFree);
            dclChildrenStayFree.Caption = "";
            dclChildrenStayFree.AllowDBNull = true;
            dclChildrenStayFree.MaxLength = 50;
    
            DataColumn dclCityId = new DataColumn(c_CityId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityId);
            dclCityId.Caption = "";
            dclCityId.AllowDBNull = true;
            dclCityId.MaxLength = 50;
    
            DataColumn dclCityNameCN = new DataColumn(c_CityNameCN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityNameCN);
            dclCityNameCN.Caption = "";
            dclCityNameCN.AllowDBNull = true;
            dclCityNameCN.MaxLength = 500;
    
            DataColumn dclCityNameEN = new DataColumn(c_CityNameEN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityNameEN);
            dclCityNameEN.Caption = "";
            dclCityNameEN.AllowDBNull = true;
            dclCityNameEN.MaxLength = 500;
    
            DataColumn dclContinentId = new DataColumn(c_ContinentId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinentId);
            dclContinentId.Caption = "";
            dclContinentId.AllowDBNull = true;
            dclContinentId.MaxLength = 50;
    
            DataColumn dclContinentNameCN = new DataColumn(c_ContinentNameCN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinentNameCN);
            dclContinentNameCN.Caption = "";
            dclContinentNameCN.AllowDBNull = true;
            dclContinentNameCN.MaxLength = 500;
    
            DataColumn dclContinentNameEN = new DataColumn(c_ContinentNameEN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinentNameEN);
            dclContinentNameEN.Caption = "";
            dclContinentNameEN.AllowDBNull = true;
            dclContinentNameEN.MaxLength = 500;
    
            DataColumn dclCountryId = new DataColumn(c_CountryId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryId);
            dclCountryId.Caption = "";
            dclCountryId.AllowDBNull = true;
            dclCountryId.MaxLength = 50;
    
            DataColumn dclCountryNameCN = new DataColumn(c_CountryNameCN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryNameCN);
            dclCountryNameCN.Caption = "";
            dclCountryNameCN.AllowDBNull = true;
            dclCountryNameCN.MaxLength = 500;
    
            DataColumn dclCountryNameEN = new DataColumn(c_CountryNameEN,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryNameEN);
            dclCountryNameEN.Caption = "";
            dclCountryNameEN.AllowDBNull = true;
            dclCountryNameEN.MaxLength = 500;
    
            DataColumn dclHotelFormerlyName = new DataColumn(c_HotelFormerlyName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelFormerlyName);
            dclHotelFormerlyName.Caption = "";
            dclHotelFormerlyName.AllowDBNull = true;
            dclHotelFormerlyName.MaxLength = 500;
    
            DataColumn dclHotelId = new DataColumn(c_HotelId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelId);
            dclHotelId.Caption = "";
            dclHotelId.AllowDBNull = true;
            dclHotelId.MaxLength = 50;
    
            DataColumn dclHotelName = new DataColumn(c_HotelName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelName);
            dclHotelName.Caption = "";
            dclHotelName.AllowDBNull = true;
            dclHotelName.MaxLength = 0;
    
            DataColumn dclHotelUrl = new DataColumn(c_HotelUrl,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelUrl);
            dclHotelUrl.Caption = "";
            dclHotelUrl.AllowDBNull = true;
            dclHotelUrl.MaxLength = 500;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclImageUrl = new DataColumn(c_ImageUrl,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclImageUrl);
            dclImageUrl.Caption = "";
            dclImageUrl.AllowDBNull = true;
            dclImageUrl.MaxLength = 0;
    
            DataColumn dclInfantAge = new DataColumn(c_InfantAge,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclInfantAge);
            dclInfantAge.Caption = "";
            dclInfantAge.AllowDBNull = true;
            dclInfantAge.MaxLength = 50;
    
            DataColumn dclLatitude = new DataColumn(c_Latitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitude);
            dclLatitude.Caption = "";
            dclLatitude.AllowDBNull = true;
            dclLatitude.MaxLength = 50;
    
            DataColumn dclLongitude = new DataColumn(c_Longitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitude);
            dclLongitude.Caption = "";
            dclLongitude.AllowDBNull = true;
            dclLongitude.MaxLength = 50;
    
            DataColumn dclMinGuestAge = new DataColumn(c_MinGuestAge,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclMinGuestAge);
            dclMinGuestAge.Caption = "";
            dclMinGuestAge.AllowDBNull = true;
            dclMinGuestAge.MaxLength = 50;
    
            DataColumn dclNumberOfReviews = new DataColumn(c_NumberOfReviews,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclNumberOfReviews);
            dclNumberOfReviews.Caption = "";
            dclNumberOfReviews.AllowDBNull = true;
            dclNumberOfReviews.MaxLength = 50;
    
            DataColumn dclPopularityScore = new DataColumn(c_PopularityScore,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPopularityScore);
            dclPopularityScore.Caption = "";
            dclPopularityScore.AllowDBNull = true;
            dclPopularityScore.MaxLength = 50;
    
            DataColumn dclRatingAverage = new DataColumn(c_RatingAverage,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRatingAverage);
            dclRatingAverage.Caption = "";
            dclRatingAverage.AllowDBNull = true;
            dclRatingAverage.MaxLength = 50;
    
            DataColumn dclRemark = new DataColumn(c_Remark,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRemark);
            dclRemark.Caption = "";
            dclRemark.AllowDBNull = true;
            dclRemark.MaxLength = 0;
    
            DataColumn dclStarRating = new DataColumn(c_StarRating,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStarRating);
            dclStarRating.Caption = "";
            dclStarRating.AllowDBNull = true;
            dclStarRating.MaxLength = 50;
    
            DataColumn dclTranslatedName = new DataColumn(c_TranslatedName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTranslatedName);
            dclTranslatedName.Caption = "";
            dclTranslatedName.AllowDBNull = true;
            dclTranslatedName.MaxLength = 0;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AgodaHotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "AccommodationType,AddressEN,AreaId,ChildrenAgeFrom,ChildrenAgeTo,ChildrenStayFree,CityId,CityNameCN,CityNameEN,ContinentId,ContinentNameCN,ContinentNameEN,CountryId,CountryNameCN,CountryNameEN,HotelFormerlyName,HotelId,HotelName,HotelUrl,ID,ImageUrl,InfantAge,Latitude,Longitude,MinGuestAge,NumberOfReviews,PopularityScore,RatingAverage,Remark,StarRating,TranslatedName";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_AccommodationType = "AccommodationType";
        public const string c_AddressEN = "AddressEN";
        public const string c_AreaId = "AreaId";
        public const string c_ChildrenAgeFrom = "ChildrenAgeFrom";
        public const string c_ChildrenAgeTo = "ChildrenAgeTo";
        public const string c_ChildrenStayFree = "ChildrenStayFree";
        public const string c_CityId = "CityId";
        public const string c_CityNameCN = "CityNameCN";
        public const string c_CityNameEN = "CityNameEN";
        public const string c_ContinentId = "ContinentId";
        public const string c_ContinentNameCN = "ContinentNameCN";
        public const string c_ContinentNameEN = "ContinentNameEN";
        public const string c_CountryId = "CountryId";
        public const string c_CountryNameCN = "CountryNameCN";
        public const string c_CountryNameEN = "CountryNameEN";
        public const string c_HotelFormerlyName = "HotelFormerlyName";
        public const string c_HotelId = "HotelId";
        public const string c_HotelName = "HotelName";
        public const string c_HotelUrl = "HotelUrl";
        public const string c_ID = "ID";
        public const string c_ImageUrl = "ImageUrl";
        public const string c_InfantAge = "InfantAge";
        public const string c_Latitude = "Latitude";
        public const string c_Longitude = "Longitude";
        public const string c_MinGuestAge = "MinGuestAge";
        public const string c_NumberOfReviews = "NumberOfReviews";
        public const string c_PopularityScore = "PopularityScore";
        public const string c_RatingAverage = "RatingAverage";
        public const string c_Remark = "Remark";
        public const string c_StarRating = "StarRating";
        public const string c_TranslatedName = "TranslatedName";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _accommodationType = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String AccommodationType
        {
            get
            {
                return _accommodationType;
            }
            set
            {
                AddOriginal("AccommodationType", _accommodationType, value);
                _accommodationType = value;
            }
        }
    
        private System.String _addressEN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String AddressEN
        {
            get
            {
                return _addressEN;
            }
            set
            {
                AddOriginal("AddressEN", _addressEN, value);
                _addressEN = value;
            }
        }
    
        private System.String _areaId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String AreaId
        {
            get
            {
                return _areaId;
            }
            set
            {
                AddOriginal("AreaId", _areaId, value);
                _areaId = value;
            }
        }
    
        private System.String _childrenAgeFrom = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ChildrenAgeFrom
        {
            get
            {
                return _childrenAgeFrom;
            }
            set
            {
                AddOriginal("ChildrenAgeFrom", _childrenAgeFrom, value);
                _childrenAgeFrom = value;
            }
        }
    
        private System.String _childrenAgeTo = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ChildrenAgeTo
        {
            get
            {
                return _childrenAgeTo;
            }
            set
            {
                AddOriginal("ChildrenAgeTo", _childrenAgeTo, value);
                _childrenAgeTo = value;
            }
        }
    
        private System.String _childrenStayFree = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ChildrenStayFree
        {
            get
            {
                return _childrenStayFree;
            }
            set
            {
                AddOriginal("ChildrenStayFree", _childrenStayFree, value);
                _childrenStayFree = value;
            }
        }
    
        private System.String _cityId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityId
        {
            get
            {
                return _cityId;
            }
            set
            {
                AddOriginal("CityId", _cityId, value);
                _cityId = value;
            }
        }
    
        private System.String _cityNameCN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityNameCN
        {
            get
            {
                return _cityNameCN;
            }
            set
            {
                AddOriginal("CityNameCN", _cityNameCN, value);
                _cityNameCN = value;
            }
        }
    
        private System.String _cityNameEN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityNameEN
        {
            get
            {
                return _cityNameEN;
            }
            set
            {
                AddOriginal("CityNameEN", _cityNameEN, value);
                _cityNameEN = value;
            }
        }
    
        private System.String _continentId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ContinentId
        {
            get
            {
                return _continentId;
            }
            set
            {
                AddOriginal("ContinentId", _continentId, value);
                _continentId = value;
            }
        }
    
        private System.String _continentNameCN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ContinentNameCN
        {
            get
            {
                return _continentNameCN;
            }
            set
            {
                AddOriginal("ContinentNameCN", _continentNameCN, value);
                _continentNameCN = value;
            }
        }
    
        private System.String _continentNameEN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ContinentNameEN
        {
            get
            {
                return _continentNameEN;
            }
            set
            {
                AddOriginal("ContinentNameEN", _continentNameEN, value);
                _continentNameEN = value;
            }
        }
    
        private System.String _countryId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryId
        {
            get
            {
                return _countryId;
            }
            set
            {
                AddOriginal("CountryId", _countryId, value);
                _countryId = value;
            }
        }
    
        private System.String _countryNameCN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryNameCN
        {
            get
            {
                return _countryNameCN;
            }
            set
            {
                AddOriginal("CountryNameCN", _countryNameCN, value);
                _countryNameCN = value;
            }
        }
    
        private System.String _countryNameEN = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryNameEN
        {
            get
            {
                return _countryNameEN;
            }
            set
            {
                AddOriginal("CountryNameEN", _countryNameEN, value);
                _countryNameEN = value;
            }
        }
    
        private System.String _hotelFormerlyName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelFormerlyName
        {
            get
            {
                return _hotelFormerlyName;
            }
            set
            {
                AddOriginal("HotelFormerlyName", _hotelFormerlyName, value);
                _hotelFormerlyName = value;
            }
        }
    
        private System.String _hotelId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelId
        {
            get
            {
                return _hotelId;
            }
            set
            {
                AddOriginal("HotelId", _hotelId, value);
                _hotelId = value;
            }
        }
    
        private System.String _hotelName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelName
        {
            get
            {
                return _hotelName;
            }
            set
            {
                AddOriginal("HotelName", _hotelName, value);
                _hotelName = value;
            }
        }
    
        private System.String _hotelUrl = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelUrl
        {
            get
            {
                return _hotelUrl;
            }
            set
            {
                AddOriginal("HotelUrl", _hotelUrl, value);
                _hotelUrl = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _imageUrl = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ImageUrl
        {
            get
            {
                return _imageUrl;
            }
            set
            {
                AddOriginal("ImageUrl", _imageUrl, value);
                _imageUrl = value;
            }
        }
    
        private System.String _infantAge = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String InfantAge
        {
            get
            {
                return _infantAge;
            }
            set
            {
                AddOriginal("InfantAge", _infantAge, value);
                _infantAge = value;
            }
        }
    
        private System.String _latitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                AddOriginal("Latitude", _latitude, value);
                _latitude = value;
            }
        }
    
        private System.String _longitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                AddOriginal("Longitude", _longitude, value);
                _longitude = value;
            }
        }
    
        private System.String _minGuestAge = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String MinGuestAge
        {
            get
            {
                return _minGuestAge;
            }
            set
            {
                AddOriginal("MinGuestAge", _minGuestAge, value);
                _minGuestAge = value;
            }
        }
    
        private System.String _numberOfReviews = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String NumberOfReviews
        {
            get
            {
                return _numberOfReviews;
            }
            set
            {
                AddOriginal("NumberOfReviews", _numberOfReviews, value);
                _numberOfReviews = value;
            }
        }
    
        private System.String _popularityScore = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String PopularityScore
        {
            get
            {
                return _popularityScore;
            }
            set
            {
                AddOriginal("PopularityScore", _popularityScore, value);
                _popularityScore = value;
            }
        }
    
        private System.String _ratingAverage = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RatingAverage
        {
            get
            {
                return _ratingAverage;
            }
            set
            {
                AddOriginal("RatingAverage", _ratingAverage, value);
                _ratingAverage = value;
            }
        }
    
        private System.String _remark = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Remark
        {
            get
            {
                return _remark;
            }
            set
            {
                AddOriginal("Remark", _remark, value);
                _remark = value;
            }
        }
    
        private System.String _starRating = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String StarRating
        {
            get
            {
                return _starRating;
            }
            set
            {
                AddOriginal("StarRating", _starRating, value);
                _starRating = value;
            }
        }
    
        private System.String _translatedName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TranslatedName
        {
            get
            {
                return _translatedName;
            }
            set
            {
                AddOriginal("TranslatedName", _translatedName, value);
                _translatedName = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AgodaHotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AgodaHotel;
        }
    
        public AgodaHotel Clone()
        {
            return EntityBase.Clone(this) as AgodaHotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AgodaHotelCollection : EntityCollectionBase
    {

        public AgodaHotelCollection()
        {
        }

        private static Type m_typ = typeof(AgodaHotelCollection);
        private static Type m_typItem = typeof(AgodaHotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AgodaHotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AgodaHotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AgodaHotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AgodaHotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AgodaHotel;
        }

        public int Add(AgodaHotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AgodaHotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(AgodaHotel value)
        {
            List.Remove(value);
        }

        public void Delete(AgodaHotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AgodaHotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AgodaHotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AgodaHotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AgodaHotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AgodaHotelQuery()
        {
            m_strTableName = "AgodaHotel";

            m_prmAccommodationType.SourceColumn = "AccommodationType";
            this.Parameters.Add(m_prmAccommodationType);

            m_prmAddressEN.SourceColumn = "AddressEN";
            this.Parameters.Add(m_prmAddressEN);

            m_prmAreaId.SourceColumn = "AreaId";
            this.Parameters.Add(m_prmAreaId);

            m_prmChildrenAgeFrom.SourceColumn = "ChildrenAgeFrom";
            this.Parameters.Add(m_prmChildrenAgeFrom);

            m_prmChildrenAgeTo.SourceColumn = "ChildrenAgeTo";
            this.Parameters.Add(m_prmChildrenAgeTo);

            m_prmChildrenStayFree.SourceColumn = "ChildrenStayFree";
            this.Parameters.Add(m_prmChildrenStayFree);

            m_prmCityId.SourceColumn = "CityId";
            this.Parameters.Add(m_prmCityId);

            m_prmCityNameCN.SourceColumn = "CityNameCN";
            this.Parameters.Add(m_prmCityNameCN);

            m_prmCityNameEN.SourceColumn = "CityNameEN";
            this.Parameters.Add(m_prmCityNameEN);

            m_prmContinentId.SourceColumn = "ContinentId";
            this.Parameters.Add(m_prmContinentId);

            m_prmContinentNameCN.SourceColumn = "ContinentNameCN";
            this.Parameters.Add(m_prmContinentNameCN);

            m_prmContinentNameEN.SourceColumn = "ContinentNameEN";
            this.Parameters.Add(m_prmContinentNameEN);

            m_prmCountryId.SourceColumn = "CountryId";
            this.Parameters.Add(m_prmCountryId);

            m_prmCountryNameCN.SourceColumn = "CountryNameCN";
            this.Parameters.Add(m_prmCountryNameCN);

            m_prmCountryNameEN.SourceColumn = "CountryNameEN";
            this.Parameters.Add(m_prmCountryNameEN);

            m_prmHotelFormerlyName.SourceColumn = "HotelFormerlyName";
            this.Parameters.Add(m_prmHotelFormerlyName);

            m_prmHotelId.SourceColumn = "HotelId";
            this.Parameters.Add(m_prmHotelId);

            m_prmHotelName.SourceColumn = "HotelName";
            this.Parameters.Add(m_prmHotelName);

            m_prmHotelUrl.SourceColumn = "HotelUrl";
            this.Parameters.Add(m_prmHotelUrl);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmImageUrl.SourceColumn = "ImageUrl";
            this.Parameters.Add(m_prmImageUrl);

            m_prmInfantAge.SourceColumn = "InfantAge";
            this.Parameters.Add(m_prmInfantAge);

            m_prmLatitude.SourceColumn = "Latitude";
            this.Parameters.Add(m_prmLatitude);

            m_prmLongitude.SourceColumn = "Longitude";
            this.Parameters.Add(m_prmLongitude);

            m_prmMinGuestAge.SourceColumn = "MinGuestAge";
            this.Parameters.Add(m_prmMinGuestAge);

            m_prmNumberOfReviews.SourceColumn = "NumberOfReviews";
            this.Parameters.Add(m_prmNumberOfReviews);

            m_prmPopularityScore.SourceColumn = "PopularityScore";
            this.Parameters.Add(m_prmPopularityScore);

            m_prmRatingAverage.SourceColumn = "RatingAverage";
            this.Parameters.Add(m_prmRatingAverage);

            m_prmRemark.SourceColumn = "Remark";
            this.Parameters.Add(m_prmRemark);

            m_prmStarRating.SourceColumn = "StarRating";
            this.Parameters.Add(m_prmStarRating);

            m_prmTranslatedName.SourceColumn = "TranslatedName";
            this.Parameters.Add(m_prmTranslatedName);
        }
        private IDataParameter m_prmAccommodationType= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["AccommodationType"]);
        public IDataParameter AccommodationType
        {
            get { return m_prmAccommodationType; }
            set { m_prmAccommodationType = value; }
        }

        private IDataParameter m_prmAddressEN= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["AddressEN"]);
        public IDataParameter AddressEN
        {
            get { return m_prmAddressEN; }
            set { m_prmAddressEN = value; }
        }

        private IDataParameter m_prmAreaId= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["AreaId"]);
        public IDataParameter AreaId
        {
            get { return m_prmAreaId; }
            set { m_prmAreaId = value; }
        }

        private IDataParameter m_prmChildrenAgeFrom= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ChildrenAgeFrom"]);
        public IDataParameter ChildrenAgeFrom
        {
            get { return m_prmChildrenAgeFrom; }
            set { m_prmChildrenAgeFrom = value; }
        }

        private IDataParameter m_prmChildrenAgeTo= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ChildrenAgeTo"]);
        public IDataParameter ChildrenAgeTo
        {
            get { return m_prmChildrenAgeTo; }
            set { m_prmChildrenAgeTo = value; }
        }

        private IDataParameter m_prmChildrenStayFree= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ChildrenStayFree"]);
        public IDataParameter ChildrenStayFree
        {
            get { return m_prmChildrenStayFree; }
            set { m_prmChildrenStayFree = value; }
        }

        private IDataParameter m_prmCityId= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["CityId"]);
        public IDataParameter CityId
        {
            get { return m_prmCityId; }
            set { m_prmCityId = value; }
        }

        private IDataParameter m_prmCityNameCN= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["CityNameCN"]);
        public IDataParameter CityNameCN
        {
            get { return m_prmCityNameCN; }
            set { m_prmCityNameCN = value; }
        }

        private IDataParameter m_prmCityNameEN= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["CityNameEN"]);
        public IDataParameter CityNameEN
        {
            get { return m_prmCityNameEN; }
            set { m_prmCityNameEN = value; }
        }

        private IDataParameter m_prmContinentId= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ContinentId"]);
        public IDataParameter ContinentId
        {
            get { return m_prmContinentId; }
            set { m_prmContinentId = value; }
        }

        private IDataParameter m_prmContinentNameCN= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ContinentNameCN"]);
        public IDataParameter ContinentNameCN
        {
            get { return m_prmContinentNameCN; }
            set { m_prmContinentNameCN = value; }
        }

        private IDataParameter m_prmContinentNameEN= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ContinentNameEN"]);
        public IDataParameter ContinentNameEN
        {
            get { return m_prmContinentNameEN; }
            set { m_prmContinentNameEN = value; }
        }

        private IDataParameter m_prmCountryId= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["CountryId"]);
        public IDataParameter CountryId
        {
            get { return m_prmCountryId; }
            set { m_prmCountryId = value; }
        }

        private IDataParameter m_prmCountryNameCN= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["CountryNameCN"]);
        public IDataParameter CountryNameCN
        {
            get { return m_prmCountryNameCN; }
            set { m_prmCountryNameCN = value; }
        }

        private IDataParameter m_prmCountryNameEN= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["CountryNameEN"]);
        public IDataParameter CountryNameEN
        {
            get { return m_prmCountryNameEN; }
            set { m_prmCountryNameEN = value; }
        }

        private IDataParameter m_prmHotelFormerlyName= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["HotelFormerlyName"]);
        public IDataParameter HotelFormerlyName
        {
            get { return m_prmHotelFormerlyName; }
            set { m_prmHotelFormerlyName = value; }
        }

        private IDataParameter m_prmHotelId= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["HotelId"]);
        public IDataParameter HotelId
        {
            get { return m_prmHotelId; }
            set { m_prmHotelId = value; }
        }

        private IDataParameter m_prmHotelName= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["HotelName"]);
        public IDataParameter HotelName
        {
            get { return m_prmHotelName; }
            set { m_prmHotelName = value; }
        }

        private IDataParameter m_prmHotelUrl= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["HotelUrl"]);
        public IDataParameter HotelUrl
        {
            get { return m_prmHotelUrl; }
            set { m_prmHotelUrl = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmImageUrl= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["ImageUrl"]);
        public IDataParameter ImageUrl
        {
            get { return m_prmImageUrl; }
            set { m_prmImageUrl = value; }
        }

        private IDataParameter m_prmInfantAge= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["InfantAge"]);
        public IDataParameter InfantAge
        {
            get { return m_prmInfantAge; }
            set { m_prmInfantAge = value; }
        }

        private IDataParameter m_prmLatitude= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["Latitude"]);
        public IDataParameter Latitude
        {
            get { return m_prmLatitude; }
            set { m_prmLatitude = value; }
        }

        private IDataParameter m_prmLongitude= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["Longitude"]);
        public IDataParameter Longitude
        {
            get { return m_prmLongitude; }
            set { m_prmLongitude = value; }
        }

        private IDataParameter m_prmMinGuestAge= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["MinGuestAge"]);
        public IDataParameter MinGuestAge
        {
            get { return m_prmMinGuestAge; }
            set { m_prmMinGuestAge = value; }
        }

        private IDataParameter m_prmNumberOfReviews= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["NumberOfReviews"]);
        public IDataParameter NumberOfReviews
        {
            get { return m_prmNumberOfReviews; }
            set { m_prmNumberOfReviews = value; }
        }

        private IDataParameter m_prmPopularityScore= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["PopularityScore"]);
        public IDataParameter PopularityScore
        {
            get { return m_prmPopularityScore; }
            set { m_prmPopularityScore = value; }
        }

        private IDataParameter m_prmRatingAverage= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["RatingAverage"]);
        public IDataParameter RatingAverage
        {
            get { return m_prmRatingAverage; }
            set { m_prmRatingAverage = value; }
        }

        private IDataParameter m_prmRemark= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["Remark"]);
        public IDataParameter Remark
        {
            get { return m_prmRemark; }
            set { m_prmRemark = value; }
        }

        private IDataParameter m_prmStarRating= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["StarRating"]);
        public IDataParameter StarRating
        {
            get { return m_prmStarRating; }
            set { m_prmStarRating = value; }
        }

        private IDataParameter m_prmTranslatedName= EntityBase.NewParameter(AgodaHotel.m_dtbEntitySchema.Columns["TranslatedName"]);
        public IDataParameter TranslatedName
        {
            get { return m_prmTranslatedName; }
            set { m_prmTranslatedName = value; }
        }

    }
    #endregion
}