// [实体版本]v2.7
// 2018-10-17 10:00:37
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class MgGroupHotel : EntityBase
    {

        #region 初始化
        public MgGroupHotel() : base()
        {
        }
    
        public MgGroupHotel(e_EntityState state) : base(state)
        {
        }
    
        static MgGroupHotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("MgGroupHotel");
    
            DataColumn dclAddress1 = new DataColumn(c_Address1,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress1);
            dclAddress1.Caption = "";
            dclAddress1.AllowDBNull = true;
            dclAddress1.MaxLength = 50;
    
            DataColumn dclCityCode = new DataColumn(c_CityCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityCode);
            dclCityCode.Caption = "";
            dclCityCode.AllowDBNull = true;
            dclCityCode.MaxLength = 100;
    
            DataColumn dclCityName = new DataColumn(c_CityName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityName);
            dclCityName.Caption = "";
            dclCityName.AllowDBNull = true;
            dclCityName.MaxLength = 100;
    
            DataColumn dclContinent = new DataColumn(c_Continent,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclContinent);
            dclContinent.Caption = "";
            dclContinent.AllowDBNull = true;
            dclContinent.MaxLength = 100;
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 100;
    
            DataColumn dclCountryName = new DataColumn(c_CountryName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryName);
            dclCountryName.Caption = "";
            dclCountryName.AllowDBNull = true;
            dclCountryName.MaxLength = 100;
    
            DataColumn dclEmail = new DataColumn(c_Email,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclEmail);
            dclEmail.Caption = "";
            dclEmail.AllowDBNull = true;
            dclEmail.MaxLength = 50;
    
            DataColumn dclHotelId = new DataColumn(c_HotelId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelId);
            dclHotelId.Caption = "";
            dclHotelId.AllowDBNull = true;
            dclHotelId.MaxLength = 20;
    
            DataColumn dclHotelName = new DataColumn(c_HotelName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelName);
            dclHotelName.Caption = "";
            dclHotelName.AllowDBNull = true;
            dclHotelName.MaxLength = 160;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclLocation = new DataColumn(c_Location,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLocation);
            dclLocation.Caption = "";
            dclLocation.AllowDBNull = true;
            dclLocation.MaxLength = 100;
    
            DataColumn dclRating = new DataColumn(c_Rating,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRating);
            dclRating.Caption = "";
            dclRating.AllowDBNull = true;
            dclRating.MaxLength = 10;
    
            DataColumn dclRegion = new DataColumn(c_Region,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRegion);
            dclRegion.Caption = "";
            dclRegion.AllowDBNull = true;
            dclRegion.MaxLength = 100;
    
            DataColumn dclTelephone = new DataColumn(c_Telephone,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTelephone);
            dclTelephone.Caption = "";
            dclTelephone.AllowDBNull = true;
            dclTelephone.MaxLength = 50;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(MgGroupHotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Address1,CityCode,CityName,Continent,CountryCode,CountryName,Email,HotelId,HotelName,ID,Location,Rating,Region,Telephone";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Address1 = "Address1";
        public const string c_CityCode = "CityCode";
        public const string c_CityName = "CityName";
        public const string c_Continent = "Continent";
        public const string c_CountryCode = "CountryCode";
        public const string c_CountryName = "CountryName";
        public const string c_Email = "Email";
        public const string c_HotelId = "HotelId";
        public const string c_HotelName = "HotelName";
        public const string c_ID = "ID";
        public const string c_Location = "Location";
        public const string c_Rating = "Rating";
        public const string c_Region = "Region";
        public const string c_Telephone = "Telephone";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _address1 = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Address1
        {
            get
            {
                return _address1;
            }
            set
            {
                AddOriginal("Address1", _address1, value);
                _address1 = value;
            }
        }
    
        private System.String _cityCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityCode
        {
            get
            {
                return _cityCode;
            }
            set
            {
                AddOriginal("CityCode", _cityCode, value);
                _cityCode = value;
            }
        }
    
        private System.String _cityName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                AddOriginal("CityName", _cityName, value);
                _cityName = value;
            }
        }
    
        private System.String _continent = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Continent
        {
            get
            {
                return _continent;
            }
            set
            {
                AddOriginal("Continent", _continent, value);
                _continent = value;
            }
        }
    
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _countryName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                AddOriginal("CountryName", _countryName, value);
                _countryName = value;
            }
        }
    
        private System.String _email = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Email
        {
            get
            {
                return _email;
            }
            set
            {
                AddOriginal("Email", _email, value);
                _email = value;
            }
        }
    
        private System.String _hotelId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelId
        {
            get
            {
                return _hotelId;
            }
            set
            {
                AddOriginal("HotelId", _hotelId, value);
                _hotelId = value;
            }
        }
    
        private System.String _hotelName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelName
        {
            get
            {
                return _hotelName;
            }
            set
            {
                AddOriginal("HotelName", _hotelName, value);
                _hotelName = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _location = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Location
        {
            get
            {
                return _location;
            }
            set
            {
                AddOriginal("Location", _location, value);
                _location = value;
            }
        }
    
        private System.String _rating = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Rating
        {
            get
            {
                return _rating;
            }
            set
            {
                AddOriginal("Rating", _rating, value);
                _rating = value;
            }
        }
    
        private System.String _region = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Region
        {
            get
            {
                return _region;
            }
            set
            {
                AddOriginal("Region", _region, value);
                _region = value;
            }
        }
    
        private System.String _telephone = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Telephone
        {
            get
            {
                return _telephone;
            }
            set
            {
                AddOriginal("Telephone", _telephone, value);
                _telephone = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static MgGroupHotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as MgGroupHotel;
        }
    
        public MgGroupHotel Clone()
        {
            return EntityBase.Clone(this) as MgGroupHotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class MgGroupHotelCollection : EntityCollectionBase
    {

        public MgGroupHotelCollection()
        {
        }

        private static Type m_typ = typeof(MgGroupHotelCollection);
        private static Type m_typItem = typeof(MgGroupHotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public MgGroupHotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (MgGroupHotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public MgGroupHotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new MgGroupHotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as MgGroupHotel;
        }

        public int Add(MgGroupHotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, MgGroupHotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(MgGroupHotel value)
        {
            List.Remove(value);
        }

        public void Delete(MgGroupHotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new MgGroupHotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(MgGroupHotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(MgGroupHotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class MgGroupHotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public MgGroupHotelQuery()
        {
            m_strTableName = "MgGroupHotel";

            m_prmAddress1.SourceColumn = "Address1";
            this.Parameters.Add(m_prmAddress1);

            m_prmCityCode.SourceColumn = "CityCode";
            this.Parameters.Add(m_prmCityCode);

            m_prmCityName.SourceColumn = "CityName";
            this.Parameters.Add(m_prmCityName);

            m_prmContinent.SourceColumn = "Continent";
            this.Parameters.Add(m_prmContinent);

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmCountryName.SourceColumn = "CountryName";
            this.Parameters.Add(m_prmCountryName);

            m_prmEmail.SourceColumn = "Email";
            this.Parameters.Add(m_prmEmail);

            m_prmHotelId.SourceColumn = "HotelId";
            this.Parameters.Add(m_prmHotelId);

            m_prmHotelName.SourceColumn = "HotelName";
            this.Parameters.Add(m_prmHotelName);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmLocation.SourceColumn = "Location";
            this.Parameters.Add(m_prmLocation);

            m_prmRating.SourceColumn = "Rating";
            this.Parameters.Add(m_prmRating);

            m_prmRegion.SourceColumn = "Region";
            this.Parameters.Add(m_prmRegion);

            m_prmTelephone.SourceColumn = "Telephone";
            this.Parameters.Add(m_prmTelephone);
        }
        private IDataParameter m_prmAddress1= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["Address1"]);
        public IDataParameter Address1
        {
            get { return m_prmAddress1; }
            set { m_prmAddress1 = value; }
        }

        private IDataParameter m_prmCityCode= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["CityCode"]);
        public IDataParameter CityCode
        {
            get { return m_prmCityCode; }
            set { m_prmCityCode = value; }
        }

        private IDataParameter m_prmCityName= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["CityName"]);
        public IDataParameter CityName
        {
            get { return m_prmCityName; }
            set { m_prmCityName = value; }
        }

        private IDataParameter m_prmContinent= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["Continent"]);
        public IDataParameter Continent
        {
            get { return m_prmContinent; }
            set { m_prmContinent = value; }
        }

        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmCountryName= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["CountryName"]);
        public IDataParameter CountryName
        {
            get { return m_prmCountryName; }
            set { m_prmCountryName = value; }
        }

        private IDataParameter m_prmEmail= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["Email"]);
        public IDataParameter Email
        {
            get { return m_prmEmail; }
            set { m_prmEmail = value; }
        }

        private IDataParameter m_prmHotelId= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["HotelId"]);
        public IDataParameter HotelId
        {
            get { return m_prmHotelId; }
            set { m_prmHotelId = value; }
        }

        private IDataParameter m_prmHotelName= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["HotelName"]);
        public IDataParameter HotelName
        {
            get { return m_prmHotelName; }
            set { m_prmHotelName = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmLocation= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["Location"]);
        public IDataParameter Location
        {
            get { return m_prmLocation; }
            set { m_prmLocation = value; }
        }

        private IDataParameter m_prmRating= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["Rating"]);
        public IDataParameter Rating
        {
            get { return m_prmRating; }
            set { m_prmRating = value; }
        }

        private IDataParameter m_prmRegion= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["Region"]);
        public IDataParameter Region
        {
            get { return m_prmRegion; }
            set { m_prmRegion = value; }
        }

        private IDataParameter m_prmTelephone= EntityBase.NewParameter(MgGroupHotel.m_dtbEntitySchema.Columns["Telephone"]);
        public IDataParameter Telephone
        {
            get { return m_prmTelephone; }
            set { m_prmTelephone = value; }
        }

    }
    #endregion
}