// [实体版本]v2.7
// 2018-07-20 14:53:01
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class Planitour_Languages : EntityBase
    {

        #region 初始化
        public Planitour_Languages() : base()
        {
        }
    
        public Planitour_Languages(e_EntityState state) : base(state)
        {
        }
    
        static Planitour_Languages()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("Planitour_Languages");
    
            DataColumn dclCode = new DataColumn(c_Code,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCode);
            dclCode.Caption = "";
            dclCode.AllowDBNull = true;
            dclCode.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclName = new DataColumn(c_Name,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclName);
            dclName.Caption = "";
            dclName.AllowDBNull = true;
            dclName.MaxLength = 200;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(Planitour_Languages);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Code,ID,Name";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Code = "Code";
        public const string c_ID = "ID";
        public const string c_Name = "Name";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _code = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Code
        {
            get
            {
                return _code;
            }
            set
            {
                AddOriginal("Code", _code, value);
                _code = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _name = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Name
        {
            get
            {
                return _name;
            }
            set
            {
                AddOriginal("Name", _name, value);
                _name = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static Planitour_Languages DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as Planitour_Languages;
        }
    
        public Planitour_Languages Clone()
        {
            return EntityBase.Clone(this) as Planitour_Languages;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class Planitour_LanguagesCollection : EntityCollectionBase
    {

        public Planitour_LanguagesCollection()
        {
        }

        private static Type m_typ = typeof(Planitour_LanguagesCollection);
        private static Type m_typItem = typeof(Planitour_Languages);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public Planitour_Languages this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (Planitour_Languages)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public Planitour_Languages this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new Planitour_Languages GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as Planitour_Languages;
        }

        public int Add(Planitour_Languages value)
        {
            return List.Add(value);
        }

        public void Insert(int index, Planitour_Languages value)
        {
            List.Insert(index, value);
        }

        public void Remove(Planitour_Languages value)
        {
            List.Remove(value);
        }

        public void Delete(Planitour_Languages value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new Planitour_LanguagesCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(Planitour_Languages value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(Planitour_Languages value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class Planitour_LanguagesQuery : XiWan.DALFactory.EntityQueryBase
    {

        public Planitour_LanguagesQuery()
        {
            m_strTableName = "Planitour_Languages";

            m_prmCode.SourceColumn = "Code";
            this.Parameters.Add(m_prmCode);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmName.SourceColumn = "Name";
            this.Parameters.Add(m_prmName);
        }
        private IDataParameter m_prmCode= EntityBase.NewParameter(Planitour_Languages.m_dtbEntitySchema.Columns["Code"]);
        public IDataParameter Code
        {
            get { return m_prmCode; }
            set { m_prmCode = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(Planitour_Languages.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmName= EntityBase.NewParameter(Planitour_Languages.m_dtbEntitySchema.Columns["Name"]);
        public IDataParameter Name
        {
            get { return m_prmName; }
            set { m_prmName = value; }
        }

    }
    #endregion
}