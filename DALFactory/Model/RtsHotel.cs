// [实体版本]v2.7
// 2018-07-15 09:22:36
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class RtsHotel : EntityBase
    {

        #region 初始化
        public RtsHotel() : base()
        {
        }
    
        public RtsHotel(e_EntityState state) : base(state)
        {
        }
    
        static RtsHotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("RtsHotel");
    
            DataColumn dclAddress = new DataColumn(c_Address,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress);
            dclAddress.Caption = "";
            dclAddress.AllowDBNull = true;
            dclAddress.MaxLength = 255;
    
            DataColumn dclCityCode = new DataColumn(c_CityCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityCode);
            dclCityCode.Caption = "";
            dclCityCode.AllowDBNull = true;
            dclCityCode.MaxLength = 255;
    
            DataColumn dclCityEname = new DataColumn(c_CityEname,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityEname);
            dclCityEname.Caption = "";
            dclCityEname.AllowDBNull = true;
            dclCityEname.MaxLength = 255;
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 255;
    
            DataColumn dclCountryEname = new DataColumn(c_CountryEname,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryEname);
            dclCountryEname.Caption = "";
            dclCountryEname.AllowDBNull = true;
            dclCountryEname.MaxLength = 255;
    
            DataColumn dclFaxNo = new DataColumn(c_FaxNo,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclFaxNo);
            dclFaxNo.Caption = "";
            dclFaxNo.AllowDBNull = true;
            dclFaxNo.MaxLength = 255;
    
            DataColumn dclItemCode = new DataColumn(c_ItemCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclItemCode);
            dclItemCode.Caption = "";
            dclItemCode.AllowDBNull = true;
            dclItemCode.MaxLength = 255;
    
            DataColumn dclItemGradeCode = new DataColumn(c_ItemGradeCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclItemGradeCode);
            dclItemGradeCode.Caption = "";
            dclItemGradeCode.AllowDBNull = true;
            dclItemGradeCode.MaxLength = 255;
    
            DataColumn dclItemName = new DataColumn(c_ItemName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclItemName);
            dclItemName.Caption = "";
            dclItemName.AllowDBNull = true;
            dclItemName.MaxLength = 255;
    
            DataColumn dclLatitude = new DataColumn(c_Latitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitude);
            dclLatitude.Caption = "";
            dclLatitude.AllowDBNull = true;
            dclLatitude.MaxLength = 255;
    
            DataColumn dclLongitude = new DataColumn(c_Longitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitude);
            dclLongitude.Caption = "";
            dclLongitude.AllowDBNull = true;
            dclLongitude.MaxLength = 255;
    
            DataColumn dclPhoneNo = new DataColumn(c_PhoneNo,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPhoneNo);
            dclPhoneNo.Caption = "";
            dclPhoneNo.AllowDBNull = true;
            dclPhoneNo.MaxLength = 255;
    
            DataColumn dclStateCode = new DataColumn(c_StateCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStateCode);
            dclStateCode.Caption = "";
            dclStateCode.AllowDBNull = true;
            dclStateCode.MaxLength = 255;
    
            DataColumn dclZipCode = new DataColumn(c_ZipCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclZipCode);
            dclZipCode.Caption = "";
            dclZipCode.AllowDBNull = true;
            dclZipCode.MaxLength = 255;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] {  };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(RtsHotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Address,CityCode,CityEname,CountryCode,CountryEname,FaxNo,ItemCode,ItemGradeCode,ItemName,Latitude,Longitude,PhoneNo,StateCode,ZipCode";
        private static string m_strEntityIdentityColumnName = "";
        private static string m_strSqlWhere = "";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Address = "Address";
        public const string c_CityCode = "CityCode";
        public const string c_CityEname = "CityEname";
        public const string c_CountryCode = "CountryCode";
        public const string c_CountryEname = "CountryEname";
        public const string c_FaxNo = "FaxNo";
        public const string c_ItemCode = "ItemCode";
        public const string c_ItemGradeCode = "ItemGradeCode";
        public const string c_ItemName = "ItemName";
        public const string c_Latitude = "Latitude";
        public const string c_Longitude = "Longitude";
        public const string c_PhoneNo = "PhoneNo";
        public const string c_StateCode = "StateCode";
        public const string c_ZipCode = "ZipCode";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _address = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Address
        {
            get
            {
                return _address;
            }
            set
            {
                AddOriginal("Address", _address, value);
                _address = value;
            }
        }
    
        private System.String _cityCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityCode
        {
            get
            {
                return _cityCode;
            }
            set
            {
                AddOriginal("CityCode", _cityCode, value);
                _cityCode = value;
            }
        }
    
        private System.String _cityEname = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityEname
        {
            get
            {
                return _cityEname;
            }
            set
            {
                AddOriginal("CityEname", _cityEname, value);
                _cityEname = value;
            }
        }
    
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _countryEname = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryEname
        {
            get
            {
                return _countryEname;
            }
            set
            {
                AddOriginal("CountryEname", _countryEname, value);
                _countryEname = value;
            }
        }
    
        private System.String _faxNo = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String FaxNo
        {
            get
            {
                return _faxNo;
            }
            set
            {
                AddOriginal("FaxNo", _faxNo, value);
                _faxNo = value;
            }
        }
    
        private System.String _itemCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ItemCode
        {
            get
            {
                return _itemCode;
            }
            set
            {
                AddOriginal("ItemCode", _itemCode, value);
                _itemCode = value;
            }
        }
    
        private System.String _itemGradeCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ItemGradeCode
        {
            get
            {
                return _itemGradeCode;
            }
            set
            {
                AddOriginal("ItemGradeCode", _itemGradeCode, value);
                _itemGradeCode = value;
            }
        }
    
        private System.String _itemName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ItemName
        {
            get
            {
                return _itemName;
            }
            set
            {
                AddOriginal("ItemName", _itemName, value);
                _itemName = value;
            }
        }
    
        private System.String _latitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                AddOriginal("Latitude", _latitude, value);
                _latitude = value;
            }
        }
    
        private System.String _longitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                AddOriginal("Longitude", _longitude, value);
                _longitude = value;
            }
        }
    
        private System.String _phoneNo = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String PhoneNo
        {
            get
            {
                return _phoneNo;
            }
            set
            {
                AddOriginal("PhoneNo", _phoneNo, value);
                _phoneNo = value;
            }
        }
    
        private System.String _stateCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String StateCode
        {
            get
            {
                return _stateCode;
            }
            set
            {
                AddOriginal("StateCode", _stateCode, value);
                _stateCode = value;
            }
        }
    
        private System.String _zipCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ZipCode
        {
            get
            {
                return _zipCode;
            }
            set
            {
                AddOriginal("ZipCode", _zipCode, value);
                _zipCode = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] {  };
                }
            }
        }
        #endregion

        #region 操作
        public static RtsHotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as RtsHotel;
        }
    
        public RtsHotel Clone()
        {
            return EntityBase.Clone(this) as RtsHotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class RtsHotelCollection : EntityCollectionBase
    {

        public RtsHotelCollection()
        {
        }

        private static Type m_typ = typeof(RtsHotelCollection);
        private static Type m_typItem = typeof(RtsHotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public RtsHotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (RtsHotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public RtsHotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new RtsHotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as RtsHotel;
        }

        public int Add(RtsHotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, RtsHotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(RtsHotel value)
        {
            List.Remove(value);
        }

        public void Delete(RtsHotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new RtsHotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(RtsHotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(RtsHotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class RtsHotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public RtsHotelQuery()
        {
            m_strTableName = "RtsHotel";

            m_prmAddress.SourceColumn = "Address";
            this.Parameters.Add(m_prmAddress);

            m_prmCityCode.SourceColumn = "CityCode";
            this.Parameters.Add(m_prmCityCode);

            m_prmCityEname.SourceColumn = "CityEname";
            this.Parameters.Add(m_prmCityEname);

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmCountryEname.SourceColumn = "CountryEname";
            this.Parameters.Add(m_prmCountryEname);

            m_prmFaxNo.SourceColumn = "FaxNo";
            this.Parameters.Add(m_prmFaxNo);

            m_prmItemCode.SourceColumn = "ItemCode";
            this.Parameters.Add(m_prmItemCode);

            m_prmItemGradeCode.SourceColumn = "ItemGradeCode";
            this.Parameters.Add(m_prmItemGradeCode);

            m_prmItemName.SourceColumn = "ItemName";
            this.Parameters.Add(m_prmItemName);

            m_prmLatitude.SourceColumn = "Latitude";
            this.Parameters.Add(m_prmLatitude);

            m_prmLongitude.SourceColumn = "Longitude";
            this.Parameters.Add(m_prmLongitude);

            m_prmPhoneNo.SourceColumn = "PhoneNo";
            this.Parameters.Add(m_prmPhoneNo);

            m_prmStateCode.SourceColumn = "StateCode";
            this.Parameters.Add(m_prmStateCode);

            m_prmZipCode.SourceColumn = "ZipCode";
            this.Parameters.Add(m_prmZipCode);
        }
        private IDataParameter m_prmAddress= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["Address"]);
        public IDataParameter Address
        {
            get { return m_prmAddress; }
            set { m_prmAddress = value; }
        }

        private IDataParameter m_prmCityCode= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["CityCode"]);
        public IDataParameter CityCode
        {
            get { return m_prmCityCode; }
            set { m_prmCityCode = value; }
        }

        private IDataParameter m_prmCityEname= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["CityEname"]);
        public IDataParameter CityEname
        {
            get { return m_prmCityEname; }
            set { m_prmCityEname = value; }
        }

        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmCountryEname= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["CountryEname"]);
        public IDataParameter CountryEname
        {
            get { return m_prmCountryEname; }
            set { m_prmCountryEname = value; }
        }

        private IDataParameter m_prmFaxNo= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["FaxNo"]);
        public IDataParameter FaxNo
        {
            get { return m_prmFaxNo; }
            set { m_prmFaxNo = value; }
        }

        private IDataParameter m_prmItemCode= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["ItemCode"]);
        public IDataParameter ItemCode
        {
            get { return m_prmItemCode; }
            set { m_prmItemCode = value; }
        }

        private IDataParameter m_prmItemGradeCode= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["ItemGradeCode"]);
        public IDataParameter ItemGradeCode
        {
            get { return m_prmItemGradeCode; }
            set { m_prmItemGradeCode = value; }
        }

        private IDataParameter m_prmItemName= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["ItemName"]);
        public IDataParameter ItemName
        {
            get { return m_prmItemName; }
            set { m_prmItemName = value; }
        }

        private IDataParameter m_prmLatitude= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["Latitude"]);
        public IDataParameter Latitude
        {
            get { return m_prmLatitude; }
            set { m_prmLatitude = value; }
        }

        private IDataParameter m_prmLongitude= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["Longitude"]);
        public IDataParameter Longitude
        {
            get { return m_prmLongitude; }
            set { m_prmLongitude = value; }
        }

        private IDataParameter m_prmPhoneNo= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["PhoneNo"]);
        public IDataParameter PhoneNo
        {
            get { return m_prmPhoneNo; }
            set { m_prmPhoneNo = value; }
        }

        private IDataParameter m_prmStateCode= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["StateCode"]);
        public IDataParameter StateCode
        {
            get { return m_prmStateCode; }
            set { m_prmStateCode = value; }
        }

        private IDataParameter m_prmZipCode= EntityBase.NewParameter(RtsHotel.m_dtbEntitySchema.Columns["ZipCode"]);
        public IDataParameter ZipCode
        {
            get { return m_prmZipCode; }
            set { m_prmZipCode = value; }
        }

    }
    #endregion
}