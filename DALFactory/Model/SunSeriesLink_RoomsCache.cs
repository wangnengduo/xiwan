// [实体版本]v2.7
// 2018-12-05 13:20:17
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class SunSeriesLink_RoomsCache : EntityBase
    {

        #region 初始化
        public SunSeriesLink_RoomsCache() : base()
        {
        }
    
        public SunSeriesLink_RoomsCache(e_EntityState state) : base(state)
        {
        }
    
        static SunSeriesLink_RoomsCache()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("SunSeriesLink_RoomsCache");
    
            DataColumn dclCheckin = new DataColumn(c_Checkin,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckin);
            dclCheckin.Caption = "";
            dclCheckin.AllowDBNull = true;
    
            DataColumn dclCheckOut = new DataColumn(c_CheckOut,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckOut);
            dclCheckOut.Caption = "";
            dclCheckOut.AllowDBNull = true;
    
            DataColumn dclCreatTime = new DataColumn(c_CreatTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreatTime);
            dclCreatTime.Caption = "";
            dclCreatTime.AllowDBNull = true;
    
            DataColumn dclHotelId = new DataColumn(c_HotelId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelId);
            dclHotelId.Caption = "";
            dclHotelId.AllowDBNull = true;
            dclHotelId.MaxLength = 100;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsClose = new DataColumn(c_IsClose,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclIsClose);
            dclIsClose.Caption = "";
            dclIsClose.AllowDBNull = true;
    
            DataColumn dclResponse = new DataColumn(c_Response,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclResponse);
            dclResponse.Caption = "";
            dclResponse.AllowDBNull = true;
            dclResponse.MaxLength = 0;
    
            DataColumn dclStrKey = new DataColumn(c_StrKey,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStrKey);
            dclStrKey.Caption = "";
            dclStrKey.AllowDBNull = true;
            dclStrKey.MaxLength = 100;
    
            DataColumn dclUpdateTime = new DataColumn(c_UpdateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclUpdateTime);
            dclUpdateTime.Caption = "";
            dclUpdateTime.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(SunSeriesLink_RoomsCache);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Checkin,CheckOut,CreatTime,HotelId,ID,IsClose,Response,StrKey,UpdateTime";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Checkin = "Checkin";
        public const string c_CheckOut = "CheckOut";
        public const string c_CreatTime = "CreatTime";
        public const string c_HotelId = "HotelId";
        public const string c_ID = "ID";
        public const string c_IsClose = "IsClose";
        public const string c_Response = "Response";
        public const string c_StrKey = "StrKey";
        public const string c_UpdateTime = "UpdateTime";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.DateTime _checkin = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime Checkin
        {
            get
            {
                return _checkin;
            }
            set
            {
                AddOriginal("Checkin", _checkin, value);
                _checkin = value;
            }
        }
    
        private System.DateTime _checkOut = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime CheckOut
        {
            get
            {
                return _checkOut;
            }
            set
            {
                AddOriginal("CheckOut", _checkOut, value);
                _checkOut = value;
            }
        }
    
        private System.DateTime _creatTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime CreatTime
        {
            get
            {
                return _creatTime;
            }
            set
            {
                AddOriginal("CreatTime", _creatTime, value);
                _creatTime = value;
            }
        }
    
        private System.String _hotelId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelId
        {
            get
            {
                return _hotelId;
            }
            set
            {
                AddOriginal("HotelId", _hotelId, value);
                _hotelId = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Int32 _isClose = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                AddOriginal("IsClose", _isClose, value);
                _isClose = value;
            }
        }
    
        private System.String _response = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Response
        {
            get
            {
                return _response;
            }
            set
            {
                AddOriginal("Response", _response, value);
                _response = value;
            }
        }
    
        private System.String _strKey = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String StrKey
        {
            get
            {
                return _strKey;
            }
            set
            {
                AddOriginal("StrKey", _strKey, value);
                _strKey = value;
            }
        }
    
        private System.DateTime _updateTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime UpdateTime
        {
            get
            {
                return _updateTime;
            }
            set
            {
                AddOriginal("UpdateTime", _updateTime, value);
                _updateTime = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static SunSeriesLink_RoomsCache DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as SunSeriesLink_RoomsCache;
        }
    
        public SunSeriesLink_RoomsCache Clone()
        {
            return EntityBase.Clone(this) as SunSeriesLink_RoomsCache;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class SunSeriesLink_RoomsCacheCollection : EntityCollectionBase
    {

        public SunSeriesLink_RoomsCacheCollection()
        {
        }

        private static Type m_typ = typeof(SunSeriesLink_RoomsCacheCollection);
        private static Type m_typItem = typeof(SunSeriesLink_RoomsCache);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public SunSeriesLink_RoomsCache this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (SunSeriesLink_RoomsCache)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public SunSeriesLink_RoomsCache this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new SunSeriesLink_RoomsCache GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as SunSeriesLink_RoomsCache;
        }

        public int Add(SunSeriesLink_RoomsCache value)
        {
            return List.Add(value);
        }

        public void Insert(int index, SunSeriesLink_RoomsCache value)
        {
            List.Insert(index, value);
        }

        public void Remove(SunSeriesLink_RoomsCache value)
        {
            List.Remove(value);
        }

        public void Delete(SunSeriesLink_RoomsCache value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new SunSeriesLink_RoomsCacheCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(SunSeriesLink_RoomsCache value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(SunSeriesLink_RoomsCache value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class SunSeriesLink_RoomsCacheQuery : XiWan.DALFactory.EntityQueryBase
    {

        public SunSeriesLink_RoomsCacheQuery()
        {
            m_strTableName = "SunSeriesLink_RoomsCache";

            m_prmCheckin.SourceColumn = "Checkin";
            this.Parameters.Add(m_prmCheckin);

            m_prmCheckOut.SourceColumn = "CheckOut";
            this.Parameters.Add(m_prmCheckOut);

            m_prmCreatTime.SourceColumn = "CreatTime";
            this.Parameters.Add(m_prmCreatTime);

            m_prmHotelId.SourceColumn = "HotelId";
            this.Parameters.Add(m_prmHotelId);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsClose.SourceColumn = "IsClose";
            this.Parameters.Add(m_prmIsClose);

            m_prmResponse.SourceColumn = "Response";
            this.Parameters.Add(m_prmResponse);

            m_prmStrKey.SourceColumn = "StrKey";
            this.Parameters.Add(m_prmStrKey);

            m_prmUpdateTime.SourceColumn = "UpdateTime";
            this.Parameters.Add(m_prmUpdateTime);
        }
        private IDataParameter m_prmCheckin= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["Checkin"]);
        public IDataParameter Checkin
        {
            get { return m_prmCheckin; }
            set { m_prmCheckin = value; }
        }

        private IDataParameter m_prmCheckOut= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["CheckOut"]);
        public IDataParameter CheckOut
        {
            get { return m_prmCheckOut; }
            set { m_prmCheckOut = value; }
        }

        private IDataParameter m_prmCreatTime= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["CreatTime"]);
        public IDataParameter CreatTime
        {
            get { return m_prmCreatTime; }
            set { m_prmCreatTime = value; }
        }

        private IDataParameter m_prmHotelId= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["HotelId"]);
        public IDataParameter HotelId
        {
            get { return m_prmHotelId; }
            set { m_prmHotelId = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsClose= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["IsClose"]);
        public IDataParameter IsClose
        {
            get { return m_prmIsClose; }
            set { m_prmIsClose = value; }
        }

        private IDataParameter m_prmResponse= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["Response"]);
        public IDataParameter Response
        {
            get { return m_prmResponse; }
            set { m_prmResponse = value; }
        }

        private IDataParameter m_prmStrKey= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["StrKey"]);
        public IDataParameter StrKey
        {
            get { return m_prmStrKey; }
            set { m_prmStrKey = value; }
        }

        private IDataParameter m_prmUpdateTime= EntityBase.NewParameter(SunSeriesLink_RoomsCache.m_dtbEntitySchema.Columns["UpdateTime"]);
        public IDataParameter UpdateTime
        {
            get { return m_prmUpdateTime; }
            set { m_prmUpdateTime = value; }
        }

    }
    #endregion
}