// [实体版本]v2.7
// 2018-10-13 14:32:44
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class AgodaHotelTest : EntityBase
    {

        #region 初始化
        public AgodaHotelTest() : base()
        {
        }
    
        public AgodaHotelTest(e_EntityState state) : base(state)
        {
        }
    
        static AgodaHotelTest()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("AgodaHotelTest");
    
            DataColumn dclCityID = new DataColumn(c_CityID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityID);
            dclCityID.Caption = "";
            dclCityID.AllowDBNull = true;
            dclCityID.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(AgodaHotelTest);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "CityID,ID";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_CityID = "CityID";
        public const string c_ID = "ID";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _cityID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityID
        {
            get
            {
                return _cityID;
            }
            set
            {
                AddOriginal("CityID", _cityID, value);
                _cityID = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static AgodaHotelTest DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as AgodaHotelTest;
        }
    
        public AgodaHotelTest Clone()
        {
            return EntityBase.Clone(this) as AgodaHotelTest;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class AgodaHotelTestCollection : EntityCollectionBase
    {

        public AgodaHotelTestCollection()
        {
        }

        private static Type m_typ = typeof(AgodaHotelTestCollection);
        private static Type m_typItem = typeof(AgodaHotelTest);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public AgodaHotelTest this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (AgodaHotelTest)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public AgodaHotelTest this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new AgodaHotelTest GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as AgodaHotelTest;
        }

        public int Add(AgodaHotelTest value)
        {
            return List.Add(value);
        }

        public void Insert(int index, AgodaHotelTest value)
        {
            List.Insert(index, value);
        }

        public void Remove(AgodaHotelTest value)
        {
            List.Remove(value);
        }

        public void Delete(AgodaHotelTest value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new AgodaHotelTestCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(AgodaHotelTest value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(AgodaHotelTest value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class AgodaHotelTestQuery : XiWan.DALFactory.EntityQueryBase
    {

        public AgodaHotelTestQuery()
        {
            m_strTableName = "AgodaHotelTest";

            m_prmCityID.SourceColumn = "CityID";
            this.Parameters.Add(m_prmCityID);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);
        }
        private IDataParameter m_prmCityID= EntityBase.NewParameter(AgodaHotelTest.m_dtbEntitySchema.Columns["CityID"]);
        public IDataParameter CityID
        {
            get { return m_prmCityID; }
            set { m_prmCityID = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(AgodaHotelTest.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

    }
    #endregion
}