// [实体版本]v2.7
// 2018-09-30 09:39:01
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class All4goLocationCode : EntityBase
    {

        #region 初始化
        public All4goLocationCode() : base()
        {
        }
    
        public All4goLocationCode(e_EntityState state) : base(state)
        {
        }
    
        static All4goLocationCode()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("All4goLocationCode");
    
            DataColumn dclcountry_id = new DataColumn(c_country_id,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclcountry_id);
            dclcountry_id.Caption = "";
            dclcountry_id.AllowDBNull = true;
            dclcountry_id.MaxLength = 50;
    
            DataColumn dclcountry_name = new DataColumn(c_country_name,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclcountry_name);
            dclcountry_name.Caption = "";
            dclcountry_name.AllowDBNull = true;
            dclcountry_name.MaxLength = 200;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dcllocation_code = new DataColumn(c_location_code,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dcllocation_code);
            dcllocation_code.Caption = "";
            dcllocation_code.AllowDBNull = true;
            dcllocation_code.MaxLength = 50;
    
            DataColumn dcllocation_id = new DataColumn(c_location_id,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dcllocation_id);
            dcllocation_id.Caption = "";
            dcllocation_id.AllowDBNull = true;
            dcllocation_id.MaxLength = 50;
    
            DataColumn dcllocation_name = new DataColumn(c_location_name,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dcllocation_name);
            dcllocation_name.Caption = "";
            dcllocation_name.AllowDBNull = true;
            dcllocation_name.MaxLength = 200;
    
            DataColumn dcllocation_type = new DataColumn(c_location_type,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dcllocation_type);
            dcllocation_type.Caption = "";
            dcllocation_type.AllowDBNull = true;
            dcllocation_type.MaxLength = 50;
    
            DataColumn dclregion_name = new DataColumn(c_region_name,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclregion_name);
            dclregion_name.Caption = "";
            dclregion_name.AllowDBNull = true;
            dclregion_name.MaxLength = 200;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(All4goLocationCode);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "country_id,country_name,ID,location_code,location_id,location_name,location_type,region_name";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_country_id = "country_id";
        public const string c_country_name = "country_name";
        public const string c_ID = "ID";
        public const string c_location_code = "location_code";
        public const string c_location_id = "location_id";
        public const string c_location_name = "location_name";
        public const string c_location_type = "location_type";
        public const string c_region_name = "region_name";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _country_id = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String country_id
        {
            get
            {
                return _country_id;
            }
            set
            {
                AddOriginal("country_id", _country_id, value);
                _country_id = value;
            }
        }
    
        private System.String _country_name = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String country_name
        {
            get
            {
                return _country_name;
            }
            set
            {
                AddOriginal("country_name", _country_name, value);
                _country_name = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _location_code = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String location_code
        {
            get
            {
                return _location_code;
            }
            set
            {
                AddOriginal("location_code", _location_code, value);
                _location_code = value;
            }
        }
    
        private System.String _location_id = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String location_id
        {
            get
            {
                return _location_id;
            }
            set
            {
                AddOriginal("location_id", _location_id, value);
                _location_id = value;
            }
        }
    
        private System.String _location_name = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String location_name
        {
            get
            {
                return _location_name;
            }
            set
            {
                AddOriginal("location_name", _location_name, value);
                _location_name = value;
            }
        }
    
        private System.String _location_type = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String location_type
        {
            get
            {
                return _location_type;
            }
            set
            {
                AddOriginal("location_type", _location_type, value);
                _location_type = value;
            }
        }
    
        private System.String _region_name = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String region_name
        {
            get
            {
                return _region_name;
            }
            set
            {
                AddOriginal("region_name", _region_name, value);
                _region_name = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static All4goLocationCode DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as All4goLocationCode;
        }
    
        public All4goLocationCode Clone()
        {
            return EntityBase.Clone(this) as All4goLocationCode;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class All4goLocationCodeCollection : EntityCollectionBase
    {

        public All4goLocationCodeCollection()
        {
        }

        private static Type m_typ = typeof(All4goLocationCodeCollection);
        private static Type m_typItem = typeof(All4goLocationCode);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public All4goLocationCode this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (All4goLocationCode)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public All4goLocationCode this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new All4goLocationCode GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as All4goLocationCode;
        }

        public int Add(All4goLocationCode value)
        {
            return List.Add(value);
        }

        public void Insert(int index, All4goLocationCode value)
        {
            List.Insert(index, value);
        }

        public void Remove(All4goLocationCode value)
        {
            List.Remove(value);
        }

        public void Delete(All4goLocationCode value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new All4goLocationCodeCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(All4goLocationCode value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(All4goLocationCode value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class All4goLocationCodeQuery : XiWan.DALFactory.EntityQueryBase
    {

        public All4goLocationCodeQuery()
        {
            m_strTableName = "All4goLocationCode";

            m_prmcountry_id.SourceColumn = "country_id";
            this.Parameters.Add(m_prmcountry_id);

            m_prmcountry_name.SourceColumn = "country_name";
            this.Parameters.Add(m_prmcountry_name);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmlocation_code.SourceColumn = "location_code";
            this.Parameters.Add(m_prmlocation_code);

            m_prmlocation_id.SourceColumn = "location_id";
            this.Parameters.Add(m_prmlocation_id);

            m_prmlocation_name.SourceColumn = "location_name";
            this.Parameters.Add(m_prmlocation_name);

            m_prmlocation_type.SourceColumn = "location_type";
            this.Parameters.Add(m_prmlocation_type);

            m_prmregion_name.SourceColumn = "region_name";
            this.Parameters.Add(m_prmregion_name);
        }
        private IDataParameter m_prmcountry_id= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["country_id"]);
        public IDataParameter country_id
        {
            get { return m_prmcountry_id; }
            set { m_prmcountry_id = value; }
        }

        private IDataParameter m_prmcountry_name= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["country_name"]);
        public IDataParameter country_name
        {
            get { return m_prmcountry_name; }
            set { m_prmcountry_name = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmlocation_code= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["location_code"]);
        public IDataParameter location_code
        {
            get { return m_prmlocation_code; }
            set { m_prmlocation_code = value; }
        }

        private IDataParameter m_prmlocation_id= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["location_id"]);
        public IDataParameter location_id
        {
            get { return m_prmlocation_id; }
            set { m_prmlocation_id = value; }
        }

        private IDataParameter m_prmlocation_name= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["location_name"]);
        public IDataParameter location_name
        {
            get { return m_prmlocation_name; }
            set { m_prmlocation_name = value; }
        }

        private IDataParameter m_prmlocation_type= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["location_type"]);
        public IDataParameter location_type
        {
            get { return m_prmlocation_type; }
            set { m_prmlocation_type = value; }
        }

        private IDataParameter m_prmregion_name= EntityBase.NewParameter(All4goLocationCode.m_dtbEntitySchema.Columns["region_name"]);
        public IDataParameter region_name
        {
            get { return m_prmregion_name; }
            set { m_prmregion_name = value; }
        }

    }
    #endregion
}