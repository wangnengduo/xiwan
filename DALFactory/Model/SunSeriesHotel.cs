// [实体版本]v2.7
// 2018-11-20 18:35:45
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class SunSeriesHotel : EntityBase
    {

        #region 初始化
        public SunSeriesHotel() : base()
        {
        }
    
        public SunSeriesHotel(e_EntityState state) : base(state)
        {
        }
    
        static SunSeriesHotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("SunSeriesHotel");
    
            DataColumn dclAddress = new DataColumn(c_Address,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress);
            dclAddress.Caption = "";
            dclAddress.AllowDBNull = true;
            dclAddress.MaxLength = 0;
    
            DataColumn dclAreaId = new DataColumn(c_AreaId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAreaId);
            dclAreaId.Caption = "";
            dclAreaId.AllowDBNull = true;
            dclAreaId.MaxLength = 50;
    
            DataColumn dclCityId = new DataColumn(c_CityId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityId);
            dclCityId.Caption = "";
            dclCityId.AllowDBNull = true;
            dclCityId.MaxLength = 50;
    
            DataColumn dclCityName = new DataColumn(c_CityName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCityName);
            dclCityName.Caption = "";
            dclCityName.AllowDBNull = true;
            dclCityName.MaxLength = 200;
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 50;
    
            DataColumn dclCountryID = new DataColumn(c_CountryID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryID);
            dclCountryID.Caption = "";
            dclCountryID.AllowDBNull = true;
            dclCountryID.MaxLength = 200;
    
            DataColumn dclCountryName = new DataColumn(c_CountryName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryName);
            dclCountryName.Caption = "";
            dclCountryName.AllowDBNull = true;
            dclCountryName.MaxLength = 200;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "";
            dclHotelID.AllowDBNull = true;
            dclHotelID.MaxLength = 50;
    
            DataColumn dclHotelName = new DataColumn(c_HotelName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelName);
            dclHotelName.Caption = "";
            dclHotelName.AllowDBNull = true;
            dclHotelName.MaxLength = 500;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclImagerUrl = new DataColumn(c_ImagerUrl,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclImagerUrl);
            dclImagerUrl.Caption = "";
            dclImagerUrl.AllowDBNull = true;
            dclImagerUrl.MaxLength = 0;
    
            DataColumn dclLatitude = new DataColumn(c_Latitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitude);
            dclLatitude.Caption = "";
            dclLatitude.AllowDBNull = true;
            dclLatitude.MaxLength = 50;
    
            DataColumn dclLongitude = new DataColumn(c_Longitude,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitude);
            dclLongitude.Caption = "";
            dclLongitude.AllowDBNull = true;
            dclLongitude.MaxLength = 50;
    
            DataColumn dclstars = new DataColumn(c_stars,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclstars);
            dclstars.Caption = "";
            dclstars.AllowDBNull = true;
            dclstars.MaxLength = 50;
    
            DataColumn dclTelephone = new DataColumn(c_Telephone,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTelephone);
            dclTelephone.Caption = "";
            dclTelephone.AllowDBNull = true;
            dclTelephone.MaxLength = 200;
    
            DataColumn dclThumbnailUrl = new DataColumn(c_ThumbnailUrl,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclThumbnailUrl);
            dclThumbnailUrl.Caption = "";
            dclThumbnailUrl.AllowDBNull = true;
            dclThumbnailUrl.MaxLength = 0;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(SunSeriesHotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Address,AreaId,CityId,CityName,CountryCode,CountryID,CountryName,HotelID,HotelName,ID,ImagerUrl,Latitude,Longitude,stars,Telephone,ThumbnailUrl";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Address = "Address";
        public const string c_AreaId = "AreaId";
        public const string c_CityId = "CityId";
        public const string c_CityName = "CityName";
        public const string c_CountryCode = "CountryCode";
        public const string c_CountryID = "CountryID";
        public const string c_CountryName = "CountryName";
        public const string c_HotelID = "HotelID";
        public const string c_HotelName = "HotelName";
        public const string c_ID = "ID";
        public const string c_ImagerUrl = "ImagerUrl";
        public const string c_Latitude = "Latitude";
        public const string c_Longitude = "Longitude";
        public const string c_stars = "stars";
        public const string c_Telephone = "Telephone";
        public const string c_ThumbnailUrl = "ThumbnailUrl";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _address = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Address
        {
            get
            {
                return _address;
            }
            set
            {
                AddOriginal("Address", _address, value);
                _address = value;
            }
        }
    
        private System.String _areaId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String AreaId
        {
            get
            {
                return _areaId;
            }
            set
            {
                AddOriginal("AreaId", _areaId, value);
                _areaId = value;
            }
        }
    
        private System.String _cityId = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityId
        {
            get
            {
                return _cityId;
            }
            set
            {
                AddOriginal("CityId", _cityId, value);
                _cityId = value;
            }
        }
    
        private System.String _cityName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CityName
        {
            get
            {
                return _cityName;
            }
            set
            {
                AddOriginal("CityName", _cityName, value);
                _cityName = value;
            }
        }
    
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _countryID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryID
        {
            get
            {
                return _countryID;
            }
            set
            {
                AddOriginal("CountryID", _countryID, value);
                _countryID = value;
            }
        }
    
        private System.String _countryName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                AddOriginal("CountryName", _countryName, value);
                _countryName = value;
            }
        }
    
        private System.String _hotelID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.String _hotelName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelName
        {
            get
            {
                return _hotelName;
            }
            set
            {
                AddOriginal("HotelName", _hotelName, value);
                _hotelName = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _imagerUrl = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ImagerUrl
        {
            get
            {
                return _imagerUrl;
            }
            set
            {
                AddOriginal("ImagerUrl", _imagerUrl, value);
                _imagerUrl = value;
            }
        }
    
        private System.String _latitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                AddOriginal("Latitude", _latitude, value);
                _latitude = value;
            }
        }
    
        private System.String _longitude = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                AddOriginal("Longitude", _longitude, value);
                _longitude = value;
            }
        }
    
        private System.String _stars = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String stars
        {
            get
            {
                return _stars;
            }
            set
            {
                AddOriginal("stars", _stars, value);
                _stars = value;
            }
        }
    
        private System.String _telephone = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Telephone
        {
            get
            {
                return _telephone;
            }
            set
            {
                AddOriginal("Telephone", _telephone, value);
                _telephone = value;
            }
        }
    
        private System.String _thumbnailUrl = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ThumbnailUrl
        {
            get
            {
                return _thumbnailUrl;
            }
            set
            {
                AddOriginal("ThumbnailUrl", _thumbnailUrl, value);
                _thumbnailUrl = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static SunSeriesHotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as SunSeriesHotel;
        }
    
        public SunSeriesHotel Clone()
        {
            return EntityBase.Clone(this) as SunSeriesHotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class SunSeriesHotelCollection : EntityCollectionBase
    {

        public SunSeriesHotelCollection()
        {
        }

        private static Type m_typ = typeof(SunSeriesHotelCollection);
        private static Type m_typItem = typeof(SunSeriesHotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public SunSeriesHotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (SunSeriesHotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public SunSeriesHotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new SunSeriesHotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as SunSeriesHotel;
        }

        public int Add(SunSeriesHotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, SunSeriesHotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(SunSeriesHotel value)
        {
            List.Remove(value);
        }

        public void Delete(SunSeriesHotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new SunSeriesHotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(SunSeriesHotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(SunSeriesHotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class SunSeriesHotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public SunSeriesHotelQuery()
        {
            m_strTableName = "SunSeriesHotel";

            m_prmAddress.SourceColumn = "Address";
            this.Parameters.Add(m_prmAddress);

            m_prmAreaId.SourceColumn = "AreaId";
            this.Parameters.Add(m_prmAreaId);

            m_prmCityId.SourceColumn = "CityId";
            this.Parameters.Add(m_prmCityId);

            m_prmCityName.SourceColumn = "CityName";
            this.Parameters.Add(m_prmCityName);

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmCountryID.SourceColumn = "CountryID";
            this.Parameters.Add(m_prmCountryID);

            m_prmCountryName.SourceColumn = "CountryName";
            this.Parameters.Add(m_prmCountryName);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmHotelName.SourceColumn = "HotelName";
            this.Parameters.Add(m_prmHotelName);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmImagerUrl.SourceColumn = "ImagerUrl";
            this.Parameters.Add(m_prmImagerUrl);

            m_prmLatitude.SourceColumn = "Latitude";
            this.Parameters.Add(m_prmLatitude);

            m_prmLongitude.SourceColumn = "Longitude";
            this.Parameters.Add(m_prmLongitude);

            m_prmstars.SourceColumn = "stars";
            this.Parameters.Add(m_prmstars);

            m_prmTelephone.SourceColumn = "Telephone";
            this.Parameters.Add(m_prmTelephone);

            m_prmThumbnailUrl.SourceColumn = "ThumbnailUrl";
            this.Parameters.Add(m_prmThumbnailUrl);
        }
        private IDataParameter m_prmAddress= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["Address"]);
        public IDataParameter Address
        {
            get { return m_prmAddress; }
            set { m_prmAddress = value; }
        }

        private IDataParameter m_prmAreaId= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["AreaId"]);
        public IDataParameter AreaId
        {
            get { return m_prmAreaId; }
            set { m_prmAreaId = value; }
        }

        private IDataParameter m_prmCityId= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["CityId"]);
        public IDataParameter CityId
        {
            get { return m_prmCityId; }
            set { m_prmCityId = value; }
        }

        private IDataParameter m_prmCityName= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["CityName"]);
        public IDataParameter CityName
        {
            get { return m_prmCityName; }
            set { m_prmCityName = value; }
        }

        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmCountryID= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["CountryID"]);
        public IDataParameter CountryID
        {
            get { return m_prmCountryID; }
            set { m_prmCountryID = value; }
        }

        private IDataParameter m_prmCountryName= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["CountryName"]);
        public IDataParameter CountryName
        {
            get { return m_prmCountryName; }
            set { m_prmCountryName = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmHotelName= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["HotelName"]);
        public IDataParameter HotelName
        {
            get { return m_prmHotelName; }
            set { m_prmHotelName = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmImagerUrl= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["ImagerUrl"]);
        public IDataParameter ImagerUrl
        {
            get { return m_prmImagerUrl; }
            set { m_prmImagerUrl = value; }
        }

        private IDataParameter m_prmLatitude= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["Latitude"]);
        public IDataParameter Latitude
        {
            get { return m_prmLatitude; }
            set { m_prmLatitude = value; }
        }

        private IDataParameter m_prmLongitude= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["Longitude"]);
        public IDataParameter Longitude
        {
            get { return m_prmLongitude; }
            set { m_prmLongitude = value; }
        }

        private IDataParameter m_prmstars= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["stars"]);
        public IDataParameter stars
        {
            get { return m_prmstars; }
            set { m_prmstars = value; }
        }

        private IDataParameter m_prmTelephone= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["Telephone"]);
        public IDataParameter Telephone
        {
            get { return m_prmTelephone; }
            set { m_prmTelephone = value; }
        }

        private IDataParameter m_prmThumbnailUrl= EntityBase.NewParameter(SunSeriesHotel.m_dtbEntitySchema.Columns["ThumbnailUrl"]);
        public IDataParameter ThumbnailUrl
        {
            get { return m_prmThumbnailUrl; }
            set { m_prmThumbnailUrl = value; }
        }

    }
    #endregion
}