// [实体版本]v2.7
// 2018-09-22 09:31:31
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class RestelSaleableHotel : EntityBase
    {

        #region 初始化
        public RestelSaleableHotel() : base()
        {
        }
    
        public RestelSaleableHotel(e_EntityState state) : base(state)
        {
        }
    
        static RestelSaleableHotel()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("RestelSaleableHotel");
    
            DataColumn dclAddress = new DataColumn(c_Address,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress);
            dclAddress.Caption = "";
            dclAddress.AllowDBNull = true;
            dclAddress.MaxLength = 500;
    
            DataColumn dclCountryCode = new DataColumn(c_CountryCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryCode);
            dclCountryCode.Caption = "";
            dclCountryCode.AllowDBNull = true;
            dclCountryCode.MaxLength = 50;
    
            DataColumn dclCountryName = new DataColumn(c_CountryName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCountryName);
            dclCountryName.Caption = "";
            dclCountryName.AllowDBNull = true;
            dclCountryName.MaxLength = 200;
    
            DataColumn dclDuplicityCode = new DataColumn(c_DuplicityCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclDuplicityCode);
            dclDuplicityCode.Caption = "";
            dclDuplicityCode.AllowDBNull = true;
            dclDuplicityCode.MaxLength = 500;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "";
            dclHotelID.AllowDBNull = true;
            dclHotelID.MaxLength = 50;
    
            DataColumn dclHotelName = new DataColumn(c_HotelName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelName);
            dclHotelName.Caption = "";
            dclHotelName.AllowDBNull = true;
            dclHotelName.MaxLength = 200;
    
            DataColumn dclHotelShortID = new DataColumn(c_HotelShortID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelShortID);
            dclHotelShortID.Caption = "";
            dclHotelShortID.AllowDBNull = true;
            dclHotelShortID.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclLatitud = new DataColumn(c_Latitud,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLatitud);
            dclLatitud.Caption = "";
            dclLatitud.AllowDBNull = true;
            dclLatitud.MaxLength = 50;
    
            DataColumn dclLongitud = new DataColumn(c_Longitud,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLongitud);
            dclLongitud.Caption = "";
            dclLongitud.AllowDBNull = true;
            dclLongitud.MaxLength = 50;
    
            DataColumn dclProvinceCode = new DataColumn(c_ProvinceCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclProvinceCode);
            dclProvinceCode.Caption = "";
            dclProvinceCode.AllowDBNull = true;
            dclProvinceCode.MaxLength = 50;
    
            DataColumn dclProvinceName = new DataColumn(c_ProvinceName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclProvinceName);
            dclProvinceName.Caption = "";
            dclProvinceName.AllowDBNull = true;
            dclProvinceName.MaxLength = 50;
    
            DataColumn dclStaring = new DataColumn(c_Staring,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStaring);
            dclStaring.Caption = "";
            dclStaring.AllowDBNull = true;
            dclStaring.MaxLength = 50;
    
            DataColumn dclTownCode = new DataColumn(c_TownCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownCode);
            dclTownCode.Caption = "";
            dclTownCode.AllowDBNull = true;
            dclTownCode.MaxLength = 50;
    
            DataColumn dclTownName = new DataColumn(c_TownName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclTownName);
            dclTownName.Caption = "";
            dclTownName.AllowDBNull = true;
            dclTownName.MaxLength = 50;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(RestelSaleableHotel);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Address,CountryCode,CountryName,DuplicityCode,HotelID,HotelName,HotelShortID,ID,Latitud,Longitud,ProvinceCode,ProvinceName,Staring,TownCode,TownName";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Address = "Address";
        public const string c_CountryCode = "CountryCode";
        public const string c_CountryName = "CountryName";
        public const string c_DuplicityCode = "DuplicityCode";
        public const string c_HotelID = "HotelID";
        public const string c_HotelName = "HotelName";
        public const string c_HotelShortID = "HotelShortID";
        public const string c_ID = "ID";
        public const string c_Latitud = "Latitud";
        public const string c_Longitud = "Longitud";
        public const string c_ProvinceCode = "ProvinceCode";
        public const string c_ProvinceName = "ProvinceName";
        public const string c_Staring = "Staring";
        public const string c_TownCode = "TownCode";
        public const string c_TownName = "TownName";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _address = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Address
        {
            get
            {
                return _address;
            }
            set
            {
                AddOriginal("Address", _address, value);
                _address = value;
            }
        }
    
        private System.String _countryCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryCode
        {
            get
            {
                return _countryCode;
            }
            set
            {
                AddOriginal("CountryCode", _countryCode, value);
                _countryCode = value;
            }
        }
    
        private System.String _countryName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CountryName
        {
            get
            {
                return _countryName;
            }
            set
            {
                AddOriginal("CountryName", _countryName, value);
                _countryName = value;
            }
        }
    
        private System.String _duplicityCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String DuplicityCode
        {
            get
            {
                return _duplicityCode;
            }
            set
            {
                AddOriginal("DuplicityCode", _duplicityCode, value);
                _duplicityCode = value;
            }
        }
    
        private System.String _hotelID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.String _hotelName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelName
        {
            get
            {
                return _hotelName;
            }
            set
            {
                AddOriginal("HotelName", _hotelName, value);
                _hotelName = value;
            }
        }
    
        private System.String _hotelShortID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelShortID
        {
            get
            {
                return _hotelShortID;
            }
            set
            {
                AddOriginal("HotelShortID", _hotelShortID, value);
                _hotelShortID = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _latitud = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Latitud
        {
            get
            {
                return _latitud;
            }
            set
            {
                AddOriginal("Latitud", _latitud, value);
                _latitud = value;
            }
        }
    
        private System.String _longitud = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Longitud
        {
            get
            {
                return _longitud;
            }
            set
            {
                AddOriginal("Longitud", _longitud, value);
                _longitud = value;
            }
        }
    
        private System.String _provinceCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ProvinceCode
        {
            get
            {
                return _provinceCode;
            }
            set
            {
                AddOriginal("ProvinceCode", _provinceCode, value);
                _provinceCode = value;
            }
        }
    
        private System.String _provinceName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ProvinceName
        {
            get
            {
                return _provinceName;
            }
            set
            {
                AddOriginal("ProvinceName", _provinceName, value);
                _provinceName = value;
            }
        }
    
        private System.String _staring = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Staring
        {
            get
            {
                return _staring;
            }
            set
            {
                AddOriginal("Staring", _staring, value);
                _staring = value;
            }
        }
    
        private System.String _townCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownCode
        {
            get
            {
                return _townCode;
            }
            set
            {
                AddOriginal("TownCode", _townCode, value);
                _townCode = value;
            }
        }
    
        private System.String _townName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String TownName
        {
            get
            {
                return _townName;
            }
            set
            {
                AddOriginal("TownName", _townName, value);
                _townName = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static RestelSaleableHotel DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as RestelSaleableHotel;
        }
    
        public RestelSaleableHotel Clone()
        {
            return EntityBase.Clone(this) as RestelSaleableHotel;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class RestelSaleableHotelCollection : EntityCollectionBase
    {

        public RestelSaleableHotelCollection()
        {
        }

        private static Type m_typ = typeof(RestelSaleableHotelCollection);
        private static Type m_typItem = typeof(RestelSaleableHotel);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public RestelSaleableHotel this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (RestelSaleableHotel)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public RestelSaleableHotel this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new RestelSaleableHotel GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as RestelSaleableHotel;
        }

        public int Add(RestelSaleableHotel value)
        {
            return List.Add(value);
        }

        public void Insert(int index, RestelSaleableHotel value)
        {
            List.Insert(index, value);
        }

        public void Remove(RestelSaleableHotel value)
        {
            List.Remove(value);
        }

        public void Delete(RestelSaleableHotel value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new RestelSaleableHotelCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(RestelSaleableHotel value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(RestelSaleableHotel value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class RestelSaleableHotelQuery : XiWan.DALFactory.EntityQueryBase
    {

        public RestelSaleableHotelQuery()
        {
            m_strTableName = "RestelSaleableHotel";

            m_prmAddress.SourceColumn = "Address";
            this.Parameters.Add(m_prmAddress);

            m_prmCountryCode.SourceColumn = "CountryCode";
            this.Parameters.Add(m_prmCountryCode);

            m_prmCountryName.SourceColumn = "CountryName";
            this.Parameters.Add(m_prmCountryName);

            m_prmDuplicityCode.SourceColumn = "DuplicityCode";
            this.Parameters.Add(m_prmDuplicityCode);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmHotelName.SourceColumn = "HotelName";
            this.Parameters.Add(m_prmHotelName);

            m_prmHotelShortID.SourceColumn = "HotelShortID";
            this.Parameters.Add(m_prmHotelShortID);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmLatitud.SourceColumn = "Latitud";
            this.Parameters.Add(m_prmLatitud);

            m_prmLongitud.SourceColumn = "Longitud";
            this.Parameters.Add(m_prmLongitud);

            m_prmProvinceCode.SourceColumn = "ProvinceCode";
            this.Parameters.Add(m_prmProvinceCode);

            m_prmProvinceName.SourceColumn = "ProvinceName";
            this.Parameters.Add(m_prmProvinceName);

            m_prmStaring.SourceColumn = "Staring";
            this.Parameters.Add(m_prmStaring);

            m_prmTownCode.SourceColumn = "TownCode";
            this.Parameters.Add(m_prmTownCode);

            m_prmTownName.SourceColumn = "TownName";
            this.Parameters.Add(m_prmTownName);
        }
        private IDataParameter m_prmAddress= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["Address"]);
        public IDataParameter Address
        {
            get { return m_prmAddress; }
            set { m_prmAddress = value; }
        }

        private IDataParameter m_prmCountryCode= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["CountryCode"]);
        public IDataParameter CountryCode
        {
            get { return m_prmCountryCode; }
            set { m_prmCountryCode = value; }
        }

        private IDataParameter m_prmCountryName= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["CountryName"]);
        public IDataParameter CountryName
        {
            get { return m_prmCountryName; }
            set { m_prmCountryName = value; }
        }

        private IDataParameter m_prmDuplicityCode= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["DuplicityCode"]);
        public IDataParameter DuplicityCode
        {
            get { return m_prmDuplicityCode; }
            set { m_prmDuplicityCode = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmHotelName= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["HotelName"]);
        public IDataParameter HotelName
        {
            get { return m_prmHotelName; }
            set { m_prmHotelName = value; }
        }

        private IDataParameter m_prmHotelShortID= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["HotelShortID"]);
        public IDataParameter HotelShortID
        {
            get { return m_prmHotelShortID; }
            set { m_prmHotelShortID = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmLatitud= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["Latitud"]);
        public IDataParameter Latitud
        {
            get { return m_prmLatitud; }
            set { m_prmLatitud = value; }
        }

        private IDataParameter m_prmLongitud= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["Longitud"]);
        public IDataParameter Longitud
        {
            get { return m_prmLongitud; }
            set { m_prmLongitud = value; }
        }

        private IDataParameter m_prmProvinceCode= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["ProvinceCode"]);
        public IDataParameter ProvinceCode
        {
            get { return m_prmProvinceCode; }
            set { m_prmProvinceCode = value; }
        }

        private IDataParameter m_prmProvinceName= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["ProvinceName"]);
        public IDataParameter ProvinceName
        {
            get { return m_prmProvinceName; }
            set { m_prmProvinceName = value; }
        }

        private IDataParameter m_prmStaring= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["Staring"]);
        public IDataParameter Staring
        {
            get { return m_prmStaring; }
            set { m_prmStaring = value; }
        }

        private IDataParameter m_prmTownCode= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["TownCode"]);
        public IDataParameter TownCode
        {
            get { return m_prmTownCode; }
            set { m_prmTownCode = value; }
        }

        private IDataParameter m_prmTownName= EntityBase.NewParameter(RestelSaleableHotel.m_dtbEntitySchema.Columns["TownName"]);
        public IDataParameter TownName
        {
            get { return m_prmTownName; }
            set { m_prmTownName = value; }
        }

    }
    #endregion
}