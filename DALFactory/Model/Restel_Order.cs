// [实体版本]v2.7
// 2018-11-23 15:50:22
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class Restel_Order : EntityBase
    {

        #region 初始化
        public Restel_Order() : base()
        {
        }
    
        public Restel_Order(e_EntityState state) : base(state)
        {
        }
    
        static Restel_Order()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("Restel_Order");
    
            DataColumn dclAdults = new DataColumn(c_Adults,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclAdults);
            dclAdults.Caption = "预订每房间成年数";
            dclAdults.AllowDBNull = true;
    
            DataColumn dclAgentBookingReference = new DataColumn(c_AgentBookingReference,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAgentBookingReference);
            dclAgentBookingReference.Caption = "预订时生成guid";
            dclAgentBookingReference.AllowDBNull = true;
            dclAgentBookingReference.MaxLength = 100;
    
            DataColumn dclBookingCode = new DataColumn(c_BookingCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBookingCode);
            dclBookingCode.Caption = "供应商返回的订单号";
            dclBookingCode.AllowDBNull = true;
            dclBookingCode.MaxLength = 50;
    
            DataColumn dclBookingConfirmationID = new DataColumn(c_BookingConfirmationID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBookingConfirmationID);
            dclBookingConfirmationID.Caption = "预订确认ID";
            dclBookingConfirmationID.AllowDBNull = true;
            dclBookingConfirmationID.MaxLength = 50;
    
            DataColumn dclCancellationTime = new DataColumn(c_CancellationTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCancellationTime);
            dclCancellationTime.Caption = "取消订单日期";
            dclCancellationTime.AllowDBNull = true;
    
            DataColumn dclCheckInDate = new DataColumn(c_CheckInDate,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckInDate);
            dclCheckInDate.Caption = "";
            dclCheckInDate.AllowDBNull = true;
    
            DataColumn dclCheckOutDate = new DataColumn(c_CheckOutDate,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckOutDate);
            dclCheckOutDate.Caption = "";
            dclCheckOutDate.AllowDBNull = true;
    
            DataColumn dclChilds = new DataColumn(c_Childs,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclChilds);
            dclChilds.Caption = "预订每房间儿童数";
            dclChilds.AllowDBNull = true;
    
            DataColumn dclClientCurrencyCode = new DataColumn(c_ClientCurrencyCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclClientCurrencyCode);
            dclClientCurrencyCode.Caption = "供应商返回的币种";
            dclClientCurrencyCode.AllowDBNull = true;
            dclClientCurrencyCode.MaxLength = 50;
    
            DataColumn dclClientPartnerAmount = new DataColumn(c_ClientPartnerAmount,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclClientPartnerAmount);
            dclClientPartnerAmount.Caption = "供应商返回的原始价格";
            dclClientPartnerAmount.AllowDBNull = true;
    
            DataColumn dclCreatIP = new DataColumn(c_CreatIP,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCreatIP);
            dclCreatIP.Caption = "";
            dclCreatIP.AllowDBNull = true;
            dclCreatIP.MaxLength = 200;
    
            DataColumn dclCreatTime = new DataColumn(c_CreatTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreatTime);
            dclCreatTime.Caption = "创建时间";
            dclCreatTime.AllowDBNull = true;
    
            DataColumn dclGuidePrice = new DataColumn(c_GuidePrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclGuidePrice);
            dclGuidePrice.Caption = "指导价，供应商要求买的最低价（原始价）";
            dclGuidePrice.AllowDBNull = true;
    
            DataColumn dclGuideRMBPrice = new DataColumn(c_GuideRMBPrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclGuideRMBPrice);
            dclGuideRMBPrice.Caption = "指导价，供应商要求买的最低价（人民币）";
            dclGuideRMBPrice.AllowDBNull = true;
    
            DataColumn dclHotelId = new DataColumn(c_HotelId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelId);
            dclHotelId.Caption = "酒店ID";
            dclHotelId.AllowDBNull = true;
            dclHotelId.MaxLength = 100;
    
            DataColumn dclId = new DataColumn(c_Id,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclId);
            dclId.Caption = "";
            dclId.AllowDBNull = true;
            dclId.AutoIncrement = true;
            dclId.AutoIncrementSeed = 1;
            dclId.AutoIncrementStep = 1;
    
            DataColumn dclInOrderNum = new DataColumn(c_InOrderNum,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclInOrderNum);
            dclInOrderNum.Caption = "平台传来订单号";
            dclInOrderNum.AllowDBNull = true;
            dclInOrderNum.MaxLength = 50;
    
            DataColumn dclIsClose = new DataColumn(c_IsClose,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclIsClose);
            dclIsClose.Caption = "";
            dclIsClose.AllowDBNull = true;
    
            DataColumn dclLeg_Status = new DataColumn(c_Leg_Status,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLeg_Status);
            dclLeg_Status.Caption = "";
            dclLeg_Status.AllowDBNull = true;
            dclLeg_Status.MaxLength = 50;
    
            DataColumn dclLegID = new DataColumn(c_LegID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclLegID);
            dclLegID.Caption = "";
            dclLegID.AllowDBNull = true;
    
            DataColumn dclPaymentMoney = new DataColumn(c_PaymentMoney,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclPaymentMoney);
            dclPaymentMoney.Caption = "退单时，返回当时付款金额";
            dclPaymentMoney.AllowDBNull = true;
    
            DataColumn dclPlatform = new DataColumn(c_Platform,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPlatform);
            dclPlatform.Caption = "供应商平台";
            dclPlatform.AllowDBNull = true;
            dclPlatform.MaxLength = 50;
    
            DataColumn dclRatePlanId = new DataColumn(c_RatePlanId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRatePlanId);
            dclRatePlanId.Caption = "房型计划ID";
            dclRatePlanId.AllowDBNull = true;
            dclRatePlanId.MaxLength = 50;
    
            DataColumn dclRefundMoney = new DataColumn(c_RefundMoney,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclRefundMoney);
            dclRefundMoney.Caption = "退单时，返回金额";
            dclRefundMoney.AllowDBNull = true;
    
            DataColumn dclRequest = new DataColumn(c_Request,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRequest);
            dclRequest.Caption = "";
            dclRequest.AllowDBNull = true;
            dclRequest.MaxLength = 0;
    
            DataColumn dclRMBprice = new DataColumn(c_RMBprice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclRMBprice);
            dclRMBprice.Caption = "预订价格(人民币)";
            dclRMBprice.AllowDBNull = true;
    
            DataColumn dclRoomCount = new DataColumn(c_RoomCount,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclRoomCount);
            dclRoomCount.Caption = "";
            dclRoomCount.AllowDBNull = true;
    
            DataColumn dclSellingPrice = new DataColumn(c_SellingPrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclSellingPrice);
            dclSellingPrice.Caption = "";
            dclSellingPrice.AllowDBNull = true;
    
            DataColumn dclServiceID = new DataColumn(c_ServiceID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclServiceID);
            dclServiceID.Caption = "";
            dclServiceID.AllowDBNull = true;
    
            DataColumn dclServiceName = new DataColumn(c_ServiceName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclServiceName);
            dclServiceName.Caption = "";
            dclServiceName.AllowDBNull = true;
            dclServiceName.MaxLength = 50;
    
            DataColumn dclStatus = new DataColumn(c_Status,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclStatus);
            dclStatus.Caption = "状态 0为取消订单，1为已预订，99为其他，3正在取消";
            dclStatus.AllowDBNull = true;
    
            DataColumn dclStatusCode = new DataColumn(c_StatusCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStatusCode);
            dclStatusCode.Caption = "供应商返回的状态";
            dclStatusCode.AllowDBNull = true;
            dclStatusCode.MaxLength = 50;
    
            DataColumn dclStatusName = new DataColumn(c_StatusName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStatusName);
            dclStatusName.Caption = "供应商返回的状态名";
            dclStatusName.AllowDBNull = true;
            dclStatusName.MaxLength = 200;
    
            DataColumn dclSurcharge = new DataColumn(c_Surcharge,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclSurcharge);
            dclSurcharge.Caption = "到店付款的附加费";
            dclSurcharge.AllowDBNull = true;
    
            DataColumn dclUpdateTime = new DataColumn(c_UpdateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclUpdateTime);
            dclUpdateTime.Caption = "修改时间";
            dclUpdateTime.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclId };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(Restel_Order);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Adults,AgentBookingReference,BookingCode,BookingConfirmationID,CancellationTime,CheckInDate,CheckOutDate,Childs,ClientCurrencyCode,ClientPartnerAmount,CreatIP,CreatTime,GuidePrice,GuideRMBPrice,HotelId,Id,InOrderNum,IsClose,Leg_Status,LegID,PaymentMoney,Platform,RatePlanId,RefundMoney,Request,RMBprice,RoomCount,SellingPrice,ServiceID,ServiceName,Status,StatusCode,StatusName,Surcharge,UpdateTime";
        private static string m_strEntityIdentityColumnName = "Id";
        private static string m_strSqlWhere = "Id=@Id_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Adults = "Adults";
        public const string c_AgentBookingReference = "AgentBookingReference";
        public const string c_BookingCode = "BookingCode";
        public const string c_BookingConfirmationID = "BookingConfirmationID";
        public const string c_CancellationTime = "CancellationTime";
        public const string c_CheckInDate = "CheckInDate";
        public const string c_CheckOutDate = "CheckOutDate";
        public const string c_Childs = "Childs";
        public const string c_ClientCurrencyCode = "ClientCurrencyCode";
        public const string c_ClientPartnerAmount = "ClientPartnerAmount";
        public const string c_CreatIP = "CreatIP";
        public const string c_CreatTime = "CreatTime";
        public const string c_GuidePrice = "GuidePrice";
        public const string c_GuideRMBPrice = "GuideRMBPrice";
        public const string c_HotelId = "HotelId";
        public const string c_Id = "Id";
        public const string c_InOrderNum = "InOrderNum";
        public const string c_IsClose = "IsClose";
        public const string c_Leg_Status = "Leg_Status";
        public const string c_LegID = "LegID";
        public const string c_PaymentMoney = "PaymentMoney";
        public const string c_Platform = "Platform";
        public const string c_RatePlanId = "RatePlanId";
        public const string c_RefundMoney = "RefundMoney";
        public const string c_Request = "Request";
        public const string c_RMBprice = "RMBprice";
        public const string c_RoomCount = "RoomCount";
        public const string c_SellingPrice = "SellingPrice";
        public const string c_ServiceID = "ServiceID";
        public const string c_ServiceName = "ServiceName";
        public const string c_Status = "Status";
        public const string c_StatusCode = "StatusCode";
        public const string c_StatusName = "StatusName";
        public const string c_Surcharge = "Surcharge";
        public const string c_UpdateTime = "UpdateTime";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.Int32 _adults = 0;
    
        /// <summary>
        /// 预订每房间成年数[字段]
        /// </summary>
        public System.Int32 Adults
        {
            get
            {
                return _adults;
            }
            set
            {
                AddOriginal("Adults", _adults, value);
                _adults = value;
            }
        }
    
        private System.String _agentBookingReference = string.Empty;
    
        /// <summary>
        /// 预订时生成guid[字段]
        /// </summary>
        public System.String AgentBookingReference
        {
            get
            {
                return _agentBookingReference;
            }
            set
            {
                AddOriginal("AgentBookingReference", _agentBookingReference, value);
                _agentBookingReference = value;
            }
        }
    
        private System.String _bookingCode = string.Empty;
    
        /// <summary>
        /// 供应商返回的订单号[字段]
        /// </summary>
        public System.String BookingCode
        {
            get
            {
                return _bookingCode;
            }
            set
            {
                AddOriginal("BookingCode", _bookingCode, value);
                _bookingCode = value;
            }
        }
    
        private System.String _bookingConfirmationID = string.Empty;
    
        /// <summary>
        /// 预订确认ID[字段]
        /// </summary>
        public System.String BookingConfirmationID
        {
            get
            {
                return _bookingConfirmationID;
            }
            set
            {
                AddOriginal("BookingConfirmationID", _bookingConfirmationID, value);
                _bookingConfirmationID = value;
            }
        }
    
        private System.DateTime _cancellationTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 取消订单日期[字段]
        /// </summary>
        public System.DateTime CancellationTime
        {
            get
            {
                return _cancellationTime;
            }
            set
            {
                AddOriginal("CancellationTime", _cancellationTime, value);
                _cancellationTime = value;
            }
        }
    
        private System.DateTime _checkInDate = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime CheckInDate
        {
            get
            {
                return _checkInDate;
            }
            set
            {
                AddOriginal("CheckInDate", _checkInDate, value);
                _checkInDate = value;
            }
        }
    
        private System.DateTime _checkOutDate = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime CheckOutDate
        {
            get
            {
                return _checkOutDate;
            }
            set
            {
                AddOriginal("CheckOutDate", _checkOutDate, value);
                _checkOutDate = value;
            }
        }
    
        private System.Int32 _childs = 0;
    
        /// <summary>
        /// 预订每房间儿童数[字段]
        /// </summary>
        public System.Int32 Childs
        {
            get
            {
                return _childs;
            }
            set
            {
                AddOriginal("Childs", _childs, value);
                _childs = value;
            }
        }
    
        private System.String _clientCurrencyCode = string.Empty;
    
        /// <summary>
        /// 供应商返回的币种[字段]
        /// </summary>
        public System.String ClientCurrencyCode
        {
            get
            {
                return _clientCurrencyCode;
            }
            set
            {
                AddOriginal("ClientCurrencyCode", _clientCurrencyCode, value);
                _clientCurrencyCode = value;
            }
        }
    
        private System.Decimal _clientPartnerAmount = 0.0m;
    
        /// <summary>
        /// 供应商返回的原始价格[字段]
        /// </summary>
        public System.Decimal ClientPartnerAmount
        {
            get
            {
                return _clientPartnerAmount;
            }
            set
            {
                AddOriginal("ClientPartnerAmount", _clientPartnerAmount, value);
                _clientPartnerAmount = value;
            }
        }
    
        private System.String _creatIP = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String CreatIP
        {
            get
            {
                return _creatIP;
            }
            set
            {
                AddOriginal("CreatIP", _creatIP, value);
                _creatIP = value;
            }
        }
    
        private System.DateTime _creatTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 创建时间[字段]
        /// </summary>
        public System.DateTime CreatTime
        {
            get
            {
                return _creatTime;
            }
            set
            {
                AddOriginal("CreatTime", _creatTime, value);
                _creatTime = value;
            }
        }
    
        private System.Decimal _guidePrice = 0.0m;
    
        /// <summary>
        /// 指导价，供应商要求买的最低价（原始价）[字段]
        /// </summary>
        public System.Decimal GuidePrice
        {
            get
            {
                return _guidePrice;
            }
            set
            {
                AddOriginal("GuidePrice", _guidePrice, value);
                _guidePrice = value;
            }
        }
    
        private System.Decimal _guideRMBPrice = 0.0m;
    
        /// <summary>
        /// 指导价，供应商要求买的最低价（人民币）[字段]
        /// </summary>
        public System.Decimal GuideRMBPrice
        {
            get
            {
                return _guideRMBPrice;
            }
            set
            {
                AddOriginal("GuideRMBPrice", _guideRMBPrice, value);
                _guideRMBPrice = value;
            }
        }
    
        private System.String _hotelId = string.Empty;
    
        /// <summary>
        /// 酒店ID[字段]
        /// </summary>
        public System.String HotelId
        {
            get
            {
                return _hotelId;
            }
            set
            {
                AddOriginal("HotelId", _hotelId, value);
                _hotelId = value;
            }
        }
    
        private System.Int32 _id = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 Id
        {
            get
            {
                return _id;
            }
            set
            {
                AddOriginal("Id", _id, value);
                _id = value;
            }
        }
    
        private System.String _inOrderNum = string.Empty;
    
        /// <summary>
        /// 平台传来订单号[字段]
        /// </summary>
        public System.String InOrderNum
        {
            get
            {
                return _inOrderNum;
            }
            set
            {
                AddOriginal("InOrderNum", _inOrderNum, value);
                _inOrderNum = value;
            }
        }
    
        private System.Int32 _isClose = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                AddOriginal("IsClose", _isClose, value);
                _isClose = value;
            }
        }
    
        private System.String _leg_Status = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Leg_Status
        {
            get
            {
                return _leg_Status;
            }
            set
            {
                AddOriginal("Leg_Status", _leg_Status, value);
                _leg_Status = value;
            }
        }
    
        private System.Int32 _legID = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 LegID
        {
            get
            {
                return _legID;
            }
            set
            {
                AddOriginal("LegID", _legID, value);
                _legID = value;
            }
        }
    
        private System.Decimal _paymentMoney = 0.0m;
    
        /// <summary>
        /// 退单时，返回当时付款金额[字段]
        /// </summary>
        public System.Decimal PaymentMoney
        {
            get
            {
                return _paymentMoney;
            }
            set
            {
                AddOriginal("PaymentMoney", _paymentMoney, value);
                _paymentMoney = value;
            }
        }
    
        private System.String _platform = string.Empty;
    
        /// <summary>
        /// 供应商平台[字段]
        /// </summary>
        public System.String Platform
        {
            get
            {
                return _platform;
            }
            set
            {
                AddOriginal("Platform", _platform, value);
                _platform = value;
            }
        }
    
        private System.String _ratePlanId = string.Empty;
    
        /// <summary>
        /// 房型计划ID[字段]
        /// </summary>
        public System.String RatePlanId
        {
            get
            {
                return _ratePlanId;
            }
            set
            {
                AddOriginal("RatePlanId", _ratePlanId, value);
                _ratePlanId = value;
            }
        }
    
        private System.Decimal _refundMoney = 0.0m;
    
        /// <summary>
        /// 退单时，返回金额[字段]
        /// </summary>
        public System.Decimal RefundMoney
        {
            get
            {
                return _refundMoney;
            }
            set
            {
                AddOriginal("RefundMoney", _refundMoney, value);
                _refundMoney = value;
            }
        }
    
        private System.String _request = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Request
        {
            get
            {
                return _request;
            }
            set
            {
                AddOriginal("Request", _request, value);
                _request = value;
            }
        }
    
        private System.Decimal _rMBprice = 0.0m;
    
        /// <summary>
        /// 预订价格(人民币)[字段]
        /// </summary>
        public System.Decimal RMBprice
        {
            get
            {
                return _rMBprice;
            }
            set
            {
                AddOriginal("RMBprice", _rMBprice, value);
                _rMBprice = value;
            }
        }
    
        private System.Int32 _roomCount = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 RoomCount
        {
            get
            {
                return _roomCount;
            }
            set
            {
                AddOriginal("RoomCount", _roomCount, value);
                _roomCount = value;
            }
        }
    
        private System.Decimal _sellingPrice = 0.0m;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Decimal SellingPrice
        {
            get
            {
                return _sellingPrice;
            }
            set
            {
                AddOriginal("SellingPrice", _sellingPrice, value);
                _sellingPrice = value;
            }
        }
    
        private System.Int32 _serviceID = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ServiceID
        {
            get
            {
                return _serviceID;
            }
            set
            {
                AddOriginal("ServiceID", _serviceID, value);
                _serviceID = value;
            }
        }
    
        private System.String _serviceName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ServiceName
        {
            get
            {
                return _serviceName;
            }
            set
            {
                AddOriginal("ServiceName", _serviceName, value);
                _serviceName = value;
            }
        }
    
        private System.Int32 _status = 0;
    
        /// <summary>
        /// 状态 0为取消订单，1为已预订，99为其他，3正在取消[字段]
        /// </summary>
        public System.Int32 Status
        {
            get
            {
                return _status;
            }
            set
            {
                AddOriginal("Status", _status, value);
                _status = value;
            }
        }
    
        private System.String _statusCode = string.Empty;
    
        /// <summary>
        /// 供应商返回的状态[字段]
        /// </summary>
        public System.String StatusCode
        {
            get
            {
                return _statusCode;
            }
            set
            {
                AddOriginal("StatusCode", _statusCode, value);
                _statusCode = value;
            }
        }
    
        private System.String _statusName = string.Empty;
    
        /// <summary>
        /// 供应商返回的状态名[字段]
        /// </summary>
        public System.String StatusName
        {
            get
            {
                return _statusName;
            }
            set
            {
                AddOriginal("StatusName", _statusName, value);
                _statusName = value;
            }
        }
    
        private System.Decimal _surcharge = 0.0m;
    
        /// <summary>
        /// 到店付款的附加费[字段]
        /// </summary>
        public System.Decimal Surcharge
        {
            get
            {
                return _surcharge;
            }
            set
            {
                AddOriginal("Surcharge", _surcharge, value);
                _surcharge = value;
            }
        }
    
        private System.DateTime _updateTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 修改时间[字段]
        /// </summary>
        public System.DateTime UpdateTime
        {
            get
            {
                return _updateTime;
            }
            set
            {
                AddOriginal("UpdateTime", _updateTime, value);
                _updateTime = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _id };
                }
            }
        }
        #endregion

        #region 操作
        public static Restel_Order DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as Restel_Order;
        }
    
        public Restel_Order Clone()
        {
            return EntityBase.Clone(this) as Restel_Order;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class Restel_OrderCollection : EntityCollectionBase
    {

        public Restel_OrderCollection()
        {
        }

        private static Type m_typ = typeof(Restel_OrderCollection);
        private static Type m_typItem = typeof(Restel_Order);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public Restel_Order this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (Restel_Order)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public Restel_Order this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new Restel_Order GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as Restel_Order;
        }

        public int Add(Restel_Order value)
        {
            return List.Add(value);
        }

        public void Insert(int index, Restel_Order value)
        {
            List.Insert(index, value);
        }

        public void Remove(Restel_Order value)
        {
            List.Remove(value);
        }

        public void Delete(Restel_Order value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new Restel_OrderCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(Restel_Order value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(Restel_Order value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class Restel_OrderQuery : XiWan.DALFactory.EntityQueryBase
    {

        public Restel_OrderQuery()
        {
            m_strTableName = "Restel_Order";

            m_prmAdults.SourceColumn = "Adults";
            this.Parameters.Add(m_prmAdults);

            m_prmAgentBookingReference.SourceColumn = "AgentBookingReference";
            this.Parameters.Add(m_prmAgentBookingReference);

            m_prmBookingCode.SourceColumn = "BookingCode";
            this.Parameters.Add(m_prmBookingCode);

            m_prmBookingConfirmationID.SourceColumn = "BookingConfirmationID";
            this.Parameters.Add(m_prmBookingConfirmationID);

            m_prmCancellationTime.SourceColumn = "CancellationTime";
            this.Parameters.Add(m_prmCancellationTime);

            m_prmCheckInDate.SourceColumn = "CheckInDate";
            this.Parameters.Add(m_prmCheckInDate);

            m_prmCheckOutDate.SourceColumn = "CheckOutDate";
            this.Parameters.Add(m_prmCheckOutDate);

            m_prmChilds.SourceColumn = "Childs";
            this.Parameters.Add(m_prmChilds);

            m_prmClientCurrencyCode.SourceColumn = "ClientCurrencyCode";
            this.Parameters.Add(m_prmClientCurrencyCode);

            m_prmClientPartnerAmount.SourceColumn = "ClientPartnerAmount";
            this.Parameters.Add(m_prmClientPartnerAmount);

            m_prmCreatIP.SourceColumn = "CreatIP";
            this.Parameters.Add(m_prmCreatIP);

            m_prmCreatTime.SourceColumn = "CreatTime";
            this.Parameters.Add(m_prmCreatTime);

            m_prmGuidePrice.SourceColumn = "GuidePrice";
            this.Parameters.Add(m_prmGuidePrice);

            m_prmGuideRMBPrice.SourceColumn = "GuideRMBPrice";
            this.Parameters.Add(m_prmGuideRMBPrice);

            m_prmHotelId.SourceColumn = "HotelId";
            this.Parameters.Add(m_prmHotelId);

            m_prmId.SourceColumn = "Id";
            this.Parameters.Add(m_prmId);

            m_prmInOrderNum.SourceColumn = "InOrderNum";
            this.Parameters.Add(m_prmInOrderNum);

            m_prmIsClose.SourceColumn = "IsClose";
            this.Parameters.Add(m_prmIsClose);

            m_prmLeg_Status.SourceColumn = "Leg_Status";
            this.Parameters.Add(m_prmLeg_Status);

            m_prmLegID.SourceColumn = "LegID";
            this.Parameters.Add(m_prmLegID);

            m_prmPaymentMoney.SourceColumn = "PaymentMoney";
            this.Parameters.Add(m_prmPaymentMoney);

            m_prmPlatform.SourceColumn = "Platform";
            this.Parameters.Add(m_prmPlatform);

            m_prmRatePlanId.SourceColumn = "RatePlanId";
            this.Parameters.Add(m_prmRatePlanId);

            m_prmRefundMoney.SourceColumn = "RefundMoney";
            this.Parameters.Add(m_prmRefundMoney);

            m_prmRequest.SourceColumn = "Request";
            this.Parameters.Add(m_prmRequest);

            m_prmRMBprice.SourceColumn = "RMBprice";
            this.Parameters.Add(m_prmRMBprice);

            m_prmRoomCount.SourceColumn = "RoomCount";
            this.Parameters.Add(m_prmRoomCount);

            m_prmSellingPrice.SourceColumn = "SellingPrice";
            this.Parameters.Add(m_prmSellingPrice);

            m_prmServiceID.SourceColumn = "ServiceID";
            this.Parameters.Add(m_prmServiceID);

            m_prmServiceName.SourceColumn = "ServiceName";
            this.Parameters.Add(m_prmServiceName);

            m_prmStatus.SourceColumn = "Status";
            this.Parameters.Add(m_prmStatus);

            m_prmStatusCode.SourceColumn = "StatusCode";
            this.Parameters.Add(m_prmStatusCode);

            m_prmStatusName.SourceColumn = "StatusName";
            this.Parameters.Add(m_prmStatusName);

            m_prmSurcharge.SourceColumn = "Surcharge";
            this.Parameters.Add(m_prmSurcharge);

            m_prmUpdateTime.SourceColumn = "UpdateTime";
            this.Parameters.Add(m_prmUpdateTime);
        }
        private IDataParameter m_prmAdults= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Adults"]);
        public IDataParameter Adults
        {
            get { return m_prmAdults; }
            set { m_prmAdults = value; }
        }

        private IDataParameter m_prmAgentBookingReference= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["AgentBookingReference"]);
        public IDataParameter AgentBookingReference
        {
            get { return m_prmAgentBookingReference; }
            set { m_prmAgentBookingReference = value; }
        }

        private IDataParameter m_prmBookingCode= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["BookingCode"]);
        public IDataParameter BookingCode
        {
            get { return m_prmBookingCode; }
            set { m_prmBookingCode = value; }
        }

        private IDataParameter m_prmBookingConfirmationID= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["BookingConfirmationID"]);
        public IDataParameter BookingConfirmationID
        {
            get { return m_prmBookingConfirmationID; }
            set { m_prmBookingConfirmationID = value; }
        }

        private IDataParameter m_prmCancellationTime= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["CancellationTime"]);
        public IDataParameter CancellationTime
        {
            get { return m_prmCancellationTime; }
            set { m_prmCancellationTime = value; }
        }

        private IDataParameter m_prmCheckInDate= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["CheckInDate"]);
        public IDataParameter CheckInDate
        {
            get { return m_prmCheckInDate; }
            set { m_prmCheckInDate = value; }
        }

        private IDataParameter m_prmCheckOutDate= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["CheckOutDate"]);
        public IDataParameter CheckOutDate
        {
            get { return m_prmCheckOutDate; }
            set { m_prmCheckOutDate = value; }
        }

        private IDataParameter m_prmChilds= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Childs"]);
        public IDataParameter Childs
        {
            get { return m_prmChilds; }
            set { m_prmChilds = value; }
        }

        private IDataParameter m_prmClientCurrencyCode= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["ClientCurrencyCode"]);
        public IDataParameter ClientCurrencyCode
        {
            get { return m_prmClientCurrencyCode; }
            set { m_prmClientCurrencyCode = value; }
        }

        private IDataParameter m_prmClientPartnerAmount= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["ClientPartnerAmount"]);
        public IDataParameter ClientPartnerAmount
        {
            get { return m_prmClientPartnerAmount; }
            set { m_prmClientPartnerAmount = value; }
        }

        private IDataParameter m_prmCreatIP= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["CreatIP"]);
        public IDataParameter CreatIP
        {
            get { return m_prmCreatIP; }
            set { m_prmCreatIP = value; }
        }

        private IDataParameter m_prmCreatTime= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["CreatTime"]);
        public IDataParameter CreatTime
        {
            get { return m_prmCreatTime; }
            set { m_prmCreatTime = value; }
        }

        private IDataParameter m_prmGuidePrice= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["GuidePrice"]);
        public IDataParameter GuidePrice
        {
            get { return m_prmGuidePrice; }
            set { m_prmGuidePrice = value; }
        }

        private IDataParameter m_prmGuideRMBPrice= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["GuideRMBPrice"]);
        public IDataParameter GuideRMBPrice
        {
            get { return m_prmGuideRMBPrice; }
            set { m_prmGuideRMBPrice = value; }
        }

        private IDataParameter m_prmHotelId= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["HotelId"]);
        public IDataParameter HotelId
        {
            get { return m_prmHotelId; }
            set { m_prmHotelId = value; }
        }

        private IDataParameter m_prmId= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Id"]);
        public IDataParameter Id
        {
            get { return m_prmId; }
            set { m_prmId = value; }
        }

        private IDataParameter m_prmInOrderNum= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["InOrderNum"]);
        public IDataParameter InOrderNum
        {
            get { return m_prmInOrderNum; }
            set { m_prmInOrderNum = value; }
        }

        private IDataParameter m_prmIsClose= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["IsClose"]);
        public IDataParameter IsClose
        {
            get { return m_prmIsClose; }
            set { m_prmIsClose = value; }
        }

        private IDataParameter m_prmLeg_Status= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Leg_Status"]);
        public IDataParameter Leg_Status
        {
            get { return m_prmLeg_Status; }
            set { m_prmLeg_Status = value; }
        }

        private IDataParameter m_prmLegID= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["LegID"]);
        public IDataParameter LegID
        {
            get { return m_prmLegID; }
            set { m_prmLegID = value; }
        }

        private IDataParameter m_prmPaymentMoney= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["PaymentMoney"]);
        public IDataParameter PaymentMoney
        {
            get { return m_prmPaymentMoney; }
            set { m_prmPaymentMoney = value; }
        }

        private IDataParameter m_prmPlatform= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Platform"]);
        public IDataParameter Platform
        {
            get { return m_prmPlatform; }
            set { m_prmPlatform = value; }
        }

        private IDataParameter m_prmRatePlanId= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["RatePlanId"]);
        public IDataParameter RatePlanId
        {
            get { return m_prmRatePlanId; }
            set { m_prmRatePlanId = value; }
        }

        private IDataParameter m_prmRefundMoney= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["RefundMoney"]);
        public IDataParameter RefundMoney
        {
            get { return m_prmRefundMoney; }
            set { m_prmRefundMoney = value; }
        }

        private IDataParameter m_prmRequest= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Request"]);
        public IDataParameter Request
        {
            get { return m_prmRequest; }
            set { m_prmRequest = value; }
        }

        private IDataParameter m_prmRMBprice= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["RMBprice"]);
        public IDataParameter RMBprice
        {
            get { return m_prmRMBprice; }
            set { m_prmRMBprice = value; }
        }

        private IDataParameter m_prmRoomCount= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["RoomCount"]);
        public IDataParameter RoomCount
        {
            get { return m_prmRoomCount; }
            set { m_prmRoomCount = value; }
        }

        private IDataParameter m_prmSellingPrice= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["SellingPrice"]);
        public IDataParameter SellingPrice
        {
            get { return m_prmSellingPrice; }
            set { m_prmSellingPrice = value; }
        }

        private IDataParameter m_prmServiceID= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["ServiceID"]);
        public IDataParameter ServiceID
        {
            get { return m_prmServiceID; }
            set { m_prmServiceID = value; }
        }

        private IDataParameter m_prmServiceName= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["ServiceName"]);
        public IDataParameter ServiceName
        {
            get { return m_prmServiceName; }
            set { m_prmServiceName = value; }
        }

        private IDataParameter m_prmStatus= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Status"]);
        public IDataParameter Status
        {
            get { return m_prmStatus; }
            set { m_prmStatus = value; }
        }

        private IDataParameter m_prmStatusCode= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["StatusCode"]);
        public IDataParameter StatusCode
        {
            get { return m_prmStatusCode; }
            set { m_prmStatusCode = value; }
        }

        private IDataParameter m_prmStatusName= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["StatusName"]);
        public IDataParameter StatusName
        {
            get { return m_prmStatusName; }
            set { m_prmStatusName = value; }
        }

        private IDataParameter m_prmSurcharge= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["Surcharge"]);
        public IDataParameter Surcharge
        {
            get { return m_prmSurcharge; }
            set { m_prmSurcharge = value; }
        }

        private IDataParameter m_prmUpdateTime= EntityBase.NewParameter(Restel_Order.m_dtbEntitySchema.Columns["UpdateTime"]);
        public IDataParameter UpdateTime
        {
            get { return m_prmUpdateTime; }
            set { m_prmUpdateTime = value; }
        }

    }
    #endregion
}