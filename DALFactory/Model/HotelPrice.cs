// [实体版本]v2.7
// 2018-11-13 14:17:26
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class HotelPrice : EntityBase
    {

        #region 初始化
        public HotelPrice() : base()
        {
        }
    
        public HotelPrice(e_EntityState state) : base(state)
        {
        }
    
        static HotelPrice()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("HotelPrice");
    
            DataColumn dclAdults = new DataColumn(c_Adults,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclAdults);
            dclAdults.Caption = "计划预订每房间成人数";
            dclAdults.AllowDBNull = true;
    
            DataColumn dclBasePrice = new DataColumn(c_BasePrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclBasePrice);
            dclBasePrice.Caption = "供应商返回的收取时价格";
            dclBasePrice.AllowDBNull = true;
    
            DataColumn dclBedTypeCode = new DataColumn(c_BedTypeCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBedTypeCode);
            dclBedTypeCode.Caption = "床型";
            dclBedTypeCode.AllowDBNull = true;
            dclBedTypeCode.MaxLength = 50;
    
            DataColumn dclBedTypeName = new DataColumn(c_BedTypeName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBedTypeName);
            dclBedTypeName.Caption = "";
            dclBedTypeName.AllowDBNull = true;
            dclBedTypeName.MaxLength = 200;
    
            DataColumn dclBreakfastName = new DataColumn(c_BreakfastName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBreakfastName);
            dclBreakfastName.Caption = "早餐名称";
            dclBreakfastName.AllowDBNull = true;
            dclBreakfastName.MaxLength = 200;
    
            DataColumn dclCGuID = new DataColumn(c_CGuID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCGuID);
            dclCGuID.Caption = "生成房型ID";
            dclCGuID.AllowDBNull = true;
            dclCGuID.MaxLength = 50;
    
            DataColumn dclCheckID = new DataColumn(c_CheckID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCheckID);
            dclCheckID.Caption = "检查价格供应商返回ID，预订是要";
            dclCheckID.AllowDBNull = true;
            dclCheckID.MaxLength = 50;
    
            DataColumn dclCheckInDate = new DataColumn(c_CheckInDate,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckInDate);
            dclCheckInDate.Caption = "入住时间";
            dclCheckInDate.AllowDBNull = true;
    
            DataColumn dclCheckOutDate = new DataColumn(c_CheckOutDate,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCheckOutDate);
            dclCheckOutDate.Caption = "退房时间";
            dclCheckOutDate.AllowDBNull = true;
    
            DataColumn dclChilds = new DataColumn(c_Childs,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclChilds);
            dclChilds.Caption = "计划预订每房间儿童数";
            dclChilds.AllowDBNull = true;
    
            DataColumn dclChildsAge = new DataColumn(c_ChildsAge,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclChildsAge);
            dclChildsAge.Caption = "计划预订每房间儿童岁数，多个用逗号分开";
            dclChildsAge.AllowDBNull = true;
            dclChildsAge.MaxLength = 100;
    
            DataColumn dclClassName = new DataColumn(c_ClassName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclClassName);
            dclClassName.Caption = "房型名称";
            dclClassName.AllowDBNull = true;
            dclClassName.MaxLength = 500;
    
            DataColumn dclClassNo = new DataColumn(c_ClassNo,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclClassNo);
            dclClassNo.Caption = "";
            dclClassNo.AllowDBNull = true;
            dclClassNo.MaxLength = 200;
    
            DataColumn dclCreateTime = new DataColumn(c_CreateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreateTime);
            dclCreateTime.Caption = "创建时间";
            dclCreateTime.AllowDBNull = true;
    
            DataColumn dclCurrencyCode = new DataColumn(c_CurrencyCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCurrencyCode);
            dclCurrencyCode.Caption = "供应商返回币种";
            dclCurrencyCode.AllowDBNull = true;
            dclCurrencyCode.MaxLength = 50;
    
            DataColumn dclEachRoomAdult = new DataColumn(c_EachRoomAdult,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclEachRoomAdult);
            dclEachRoomAdult.Caption = "每个房间成人数以逗号分开";
            dclEachRoomAdult.AllowDBNull = true;
            dclEachRoomAdult.MaxLength = 50;
    
            DataColumn dclEachRoomChild = new DataColumn(c_EachRoomChild,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclEachRoomChild);
            dclEachRoomChild.Caption = "每个房间儿童人数以逗号分开";
            dclEachRoomChild.AllowDBNull = true;
            dclEachRoomChild.MaxLength = 50;
    
            DataColumn dclExtraBed = new DataColumn(c_ExtraBed,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclExtraBed);
            dclExtraBed.Caption = "加床";
            dclExtraBed.AllowDBNull = true;
    
            DataColumn dclExtraBedPrice = new DataColumn(c_ExtraBedPrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclExtraBedPrice);
            dclExtraBedPrice.Caption = "加床价格";
            dclExtraBedPrice.AllowDBNull = true;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "酒店ID";
            dclHotelID.AllowDBNull = true;
            dclHotelID.MaxLength = 50;
    
            DataColumn dclHotelNo = new DataColumn(c_HotelNo,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelNo);
            dclHotelNo.Caption = "";
            dclHotelNo.AllowDBNull = true;
            dclHotelNo.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclIsBreakfast = new DataColumn(c_IsBreakfast,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclIsBreakfast);
            dclIsBreakfast.Caption = "是否含有早餐1为含有或含单早，0为不含早餐";
            dclIsBreakfast.AllowDBNull = true;
    
            DataColumn dclIsClose = new DataColumn(c_IsClose,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclIsClose);
            dclIsClose.Caption = "是否有效，1有效，0为无效";
            dclIsClose.AllowDBNull = true;
    
            DataColumn dclPenalty = new DataColumn(c_Penalty,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclPenalty);
            dclPenalty.Caption = "退单罚款金额";
            dclPenalty.AllowDBNull = true;
    
            DataColumn dclPlatform = new DataColumn(c_Platform,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPlatform);
            dclPlatform.Caption = "供应商";
            dclPlatform.AllowDBNull = true;
            dclPlatform.MaxLength = 50;
    
            DataColumn dclPrice = new DataColumn(c_Price,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclPrice);
            dclPrice.Caption = "供应商返回原始价格";
            dclPrice.AllowDBNull = true;
    
            DataColumn dclPriceBreakdownDate = new DataColumn(c_PriceBreakdownDate,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPriceBreakdownDate);
            dclPriceBreakdownDate.Caption = "供应商返回入住时间，多天用逗号分开，以便判断计划入住3天而返回2天这种情况";
            dclPriceBreakdownDate.AllowDBNull = true;
            dclPriceBreakdownDate.MaxLength = 500;
    
            DataColumn dclPriceBreakdownPrice = new DataColumn(c_PriceBreakdownPrice,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPriceBreakdownPrice);
            dclPriceBreakdownPrice.Caption = "供应商返回每天价格，多天用逗号分开";
            dclPriceBreakdownPrice.AllowDBNull = true;
            dclPriceBreakdownPrice.MaxLength = 500;
    
            DataColumn dclRateplanId = new DataColumn(c_RateplanId,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRateplanId);
            dclRateplanId.Caption = "价格计划ID";
            dclRateplanId.AllowDBNull = true;
            dclRateplanId.MaxLength = 50;
    
            DataColumn dclReferenceClient = new DataColumn(c_ReferenceClient,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclReferenceClient);
            dclReferenceClient.Caption = "供应商返回房型表示";
            dclReferenceClient.AllowDBNull = true;
            dclReferenceClient.MaxLength = -1;
    
            DataColumn dclRMBbasePrice = new DataColumn(c_RMBbasePrice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclRMBbasePrice);
            dclRMBbasePrice.Caption = "供应商返回的收取时价格(人民币)";
            dclRMBbasePrice.AllowDBNull = true;
    
            DataColumn dclRMBprice = new DataColumn(c_RMBprice,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclRMBprice);
            dclRMBprice.Caption = "房间价格(人民币)";
            dclRMBprice.AllowDBNull = true;
    
            DataColumn dclRoomAdults = new DataColumn(c_RoomAdults,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclRoomAdults);
            dclRoomAdults.Caption = "房间最大可入住成人数";
            dclRoomAdults.AllowDBNull = true;
    
            DataColumn dclRoomChilds = new DataColumn(c_RoomChilds,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclRoomChilds);
            dclRoomChilds.Caption = "房间最大可入住儿童数";
            dclRoomChilds.AllowDBNull = true;
    
            DataColumn dclRoomCount = new DataColumn(c_RoomCount,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclRoomCount);
            dclRoomCount.Caption = "计划预订房间数";
            dclRoomCount.AllowDBNull = true;
    
            DataColumn dclRoomsLeft = new DataColumn(c_RoomsLeft,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclRoomsLeft);
            dclRoomsLeft.Caption = "剩余房间数";
            dclRoomsLeft.AllowDBNull = true;
    
            DataColumn dclRoomText = new DataColumn(c_RoomText,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomText);
            dclRoomText.Caption = "房间相关属性，在预订时需要用";
            dclRoomText.AllowDBNull = true;
            dclRoomText.MaxLength = 5000;
    
            DataColumn dclRoomTypeID = new DataColumn(c_RoomTypeID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomTypeID);
            dclRoomTypeID.Caption = "";
            dclRoomTypeID.AllowDBNull = true;
            dclRoomTypeID.MaxLength = 500;
    
            DataColumn dclSeachID = new DataColumn(c_SeachID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclSeachID);
            dclSeachID.Caption = "报价时,供应商返回ID。有些供应商下单时需要报价时的ID";
            dclSeachID.AllowDBNull = true;
            dclSeachID.MaxLength = 200;
    
            DataColumn dclStatus = new DataColumn(c_Status,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclStatus);
            dclStatus.Caption = "状态";
            dclStatus.AllowDBNull = true;
            dclStatus.MaxLength = 50;
    
            DataColumn dclSurcharge = new DataColumn(c_Surcharge,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclSurcharge);
            dclSurcharge.Caption = "到店付款的附加费";
            dclSurcharge.AllowDBNull = true;
    
            DataColumn dclUpdateTime = new DataColumn(c_UpdateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclUpdateTime);
            dclUpdateTime.Caption = "修改时间";
            dclUpdateTime.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(HotelPrice);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Adults,BasePrice,BedTypeCode,BedTypeName,BreakfastName,CGuID,CheckID,CheckInDate,CheckOutDate,Childs,ChildsAge,ClassName,ClassNo,CreateTime,CurrencyCode,EachRoomAdult,EachRoomChild,ExtraBed,ExtraBedPrice,HotelID,HotelNo,ID,IsBreakfast,IsClose,Penalty,Platform,Price,PriceBreakdownDate,PriceBreakdownPrice,RateplanId,ReferenceClient,RMBbasePrice,RMBprice,RoomAdults,RoomChilds,RoomCount,RoomsLeft,RoomText,RoomTypeID,SeachID,Status,Surcharge,UpdateTime";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Adults = "Adults";
        public const string c_BasePrice = "BasePrice";
        public const string c_BedTypeCode = "BedTypeCode";
        public const string c_BedTypeName = "BedTypeName";
        public const string c_BreakfastName = "BreakfastName";
        public const string c_CGuID = "CGuID";
        public const string c_CheckID = "CheckID";
        public const string c_CheckInDate = "CheckInDate";
        public const string c_CheckOutDate = "CheckOutDate";
        public const string c_Childs = "Childs";
        public const string c_ChildsAge = "ChildsAge";
        public const string c_ClassName = "ClassName";
        public const string c_ClassNo = "ClassNo";
        public const string c_CreateTime = "CreateTime";
        public const string c_CurrencyCode = "CurrencyCode";
        public const string c_EachRoomAdult = "EachRoomAdult";
        public const string c_EachRoomChild = "EachRoomChild";
        public const string c_ExtraBed = "ExtraBed";
        public const string c_ExtraBedPrice = "ExtraBedPrice";
        public const string c_HotelID = "HotelID";
        public const string c_HotelNo = "HotelNo";
        public const string c_ID = "ID";
        public const string c_IsBreakfast = "IsBreakfast";
        public const string c_IsClose = "IsClose";
        public const string c_Penalty = "Penalty";
        public const string c_Platform = "Platform";
        public const string c_Price = "Price";
        public const string c_PriceBreakdownDate = "PriceBreakdownDate";
        public const string c_PriceBreakdownPrice = "PriceBreakdownPrice";
        public const string c_RateplanId = "RateplanId";
        public const string c_ReferenceClient = "ReferenceClient";
        public const string c_RMBbasePrice = "RMBbasePrice";
        public const string c_RMBprice = "RMBprice";
        public const string c_RoomAdults = "RoomAdults";
        public const string c_RoomChilds = "RoomChilds";
        public const string c_RoomCount = "RoomCount";
        public const string c_RoomsLeft = "RoomsLeft";
        public const string c_RoomText = "RoomText";
        public const string c_RoomTypeID = "RoomTypeID";
        public const string c_SeachID = "SeachID";
        public const string c_Status = "Status";
        public const string c_Surcharge = "Surcharge";
        public const string c_UpdateTime = "UpdateTime";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.Int32 _adults = 0;
    
        /// <summary>
        /// 计划预订每房间成人数[字段]
        /// </summary>
        public System.Int32 Adults
        {
            get
            {
                return _adults;
            }
            set
            {
                AddOriginal("Adults", _adults, value);
                _adults = value;
            }
        }
    
        private System.Decimal _basePrice = 0.0m;
    
        /// <summary>
        /// 供应商返回的收取时价格[字段]
        /// </summary>
        public System.Decimal BasePrice
        {
            get
            {
                return _basePrice;
            }
            set
            {
                AddOriginal("BasePrice", _basePrice, value);
                _basePrice = value;
            }
        }
    
        private System.String _bedTypeCode = string.Empty;
    
        /// <summary>
        /// 床型[字段]
        /// </summary>
        public System.String BedTypeCode
        {
            get
            {
                return _bedTypeCode;
            }
            set
            {
                AddOriginal("BedTypeCode", _bedTypeCode, value);
                _bedTypeCode = value;
            }
        }
    
        private System.String _bedTypeName = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String BedTypeName
        {
            get
            {
                return _bedTypeName;
            }
            set
            {
                AddOriginal("BedTypeName", _bedTypeName, value);
                _bedTypeName = value;
            }
        }
    
        private System.String _breakfastName = string.Empty;
    
        /// <summary>
        /// 早餐名称[字段]
        /// </summary>
        public System.String BreakfastName
        {
            get
            {
                return _breakfastName;
            }
            set
            {
                AddOriginal("BreakfastName", _breakfastName, value);
                _breakfastName = value;
            }
        }
    
        private System.String _cGuID = string.Empty;
    
        /// <summary>
        /// 生成房型ID[字段]
        /// </summary>
        public System.String CGuID
        {
            get
            {
                return _cGuID;
            }
            set
            {
                AddOriginal("CGuID", _cGuID, value);
                _cGuID = value;
            }
        }
    
        private System.String _checkID = string.Empty;
    
        /// <summary>
        /// 检查价格供应商返回ID，预订是要[字段]
        /// </summary>
        public System.String CheckID
        {
            get
            {
                return _checkID;
            }
            set
            {
                AddOriginal("CheckID", _checkID, value);
                _checkID = value;
            }
        }
    
        private System.DateTime _checkInDate = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 入住时间[字段]
        /// </summary>
        public System.DateTime CheckInDate
        {
            get
            {
                return _checkInDate;
            }
            set
            {
                AddOriginal("CheckInDate", _checkInDate, value);
                _checkInDate = value;
            }
        }
    
        private System.DateTime _checkOutDate = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 退房时间[字段]
        /// </summary>
        public System.DateTime CheckOutDate
        {
            get
            {
                return _checkOutDate;
            }
            set
            {
                AddOriginal("CheckOutDate", _checkOutDate, value);
                _checkOutDate = value;
            }
        }
    
        private System.Int32 _childs = 0;
    
        /// <summary>
        /// 计划预订每房间儿童数[字段]
        /// </summary>
        public System.Int32 Childs
        {
            get
            {
                return _childs;
            }
            set
            {
                AddOriginal("Childs", _childs, value);
                _childs = value;
            }
        }
    
        private System.String _childsAge = string.Empty;
    
        /// <summary>
        /// 计划预订每房间儿童岁数，多个用逗号分开[字段]
        /// </summary>
        public System.String ChildsAge
        {
            get
            {
                return _childsAge;
            }
            set
            {
                AddOriginal("ChildsAge", _childsAge, value);
                _childsAge = value;
            }
        }
    
        private System.String _className = string.Empty;
    
        /// <summary>
        /// 房型名称[字段]
        /// </summary>
        public System.String ClassName
        {
            get
            {
                return _className;
            }
            set
            {
                AddOriginal("ClassName", _className, value);
                _className = value;
            }
        }
    
        private System.String _classNo = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String ClassNo
        {
            get
            {
                return _classNo;
            }
            set
            {
                AddOriginal("ClassNo", _classNo, value);
                _classNo = value;
            }
        }
    
        private System.DateTime _createTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 创建时间[字段]
        /// </summary>
        public System.DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                AddOriginal("CreateTime", _createTime, value);
                _createTime = value;
            }
        }
    
        private System.String _currencyCode = string.Empty;
    
        /// <summary>
        /// 供应商返回币种[字段]
        /// </summary>
        public System.String CurrencyCode
        {
            get
            {
                return _currencyCode;
            }
            set
            {
                AddOriginal("CurrencyCode", _currencyCode, value);
                _currencyCode = value;
            }
        }
    
        private System.String _eachRoomAdult = string.Empty;
    
        /// <summary>
        /// 每个房间成人数以逗号分开[字段]
        /// </summary>
        public System.String EachRoomAdult
        {
            get
            {
                return _eachRoomAdult;
            }
            set
            {
                AddOriginal("EachRoomAdult", _eachRoomAdult, value);
                _eachRoomAdult = value;
            }
        }
    
        private System.String _eachRoomChild = string.Empty;
    
        /// <summary>
        /// 每个房间儿童人数以逗号分开[字段]
        /// </summary>
        public System.String EachRoomChild
        {
            get
            {
                return _eachRoomChild;
            }
            set
            {
                AddOriginal("EachRoomChild", _eachRoomChild, value);
                _eachRoomChild = value;
            }
        }
    
        private System.Int32 _extraBed = 0;
    
        /// <summary>
        /// 加床[字段]
        /// </summary>
        public System.Int32 ExtraBed
        {
            get
            {
                return _extraBed;
            }
            set
            {
                AddOriginal("ExtraBed", _extraBed, value);
                _extraBed = value;
            }
        }
    
        private System.Decimal _extraBedPrice = 0.0m;
    
        /// <summary>
        /// 加床价格[字段]
        /// </summary>
        public System.Decimal ExtraBedPrice
        {
            get
            {
                return _extraBedPrice;
            }
            set
            {
                AddOriginal("ExtraBedPrice", _extraBedPrice, value);
                _extraBedPrice = value;
            }
        }
    
        private System.String _hotelID = string.Empty;
    
        /// <summary>
        /// 酒店ID[字段]
        /// </summary>
        public System.String HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.String _hotelNo = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelNo
        {
            get
            {
                return _hotelNo;
            }
            set
            {
                AddOriginal("HotelNo", _hotelNo, value);
                _hotelNo = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Int32 _isBreakfast = 0;
    
        /// <summary>
        /// 是否含有早餐1为含有或含单早，0为不含早餐[字段]
        /// </summary>
        public System.Int32 IsBreakfast
        {
            get
            {
                return _isBreakfast;
            }
            set
            {
                AddOriginal("IsBreakfast", _isBreakfast, value);
                _isBreakfast = value;
            }
        }
    
        private System.Int32 _isClose = 0;
    
        /// <summary>
        /// 是否有效，1有效，0为无效[字段]
        /// </summary>
        public System.Int32 IsClose
        {
            get
            {
                return _isClose;
            }
            set
            {
                AddOriginal("IsClose", _isClose, value);
                _isClose = value;
            }
        }
    
        private System.Decimal _penalty = 0.0m;
    
        /// <summary>
        /// 退单罚款金额[字段]
        /// </summary>
        public System.Decimal Penalty
        {
            get
            {
                return _penalty;
            }
            set
            {
                AddOriginal("Penalty", _penalty, value);
                _penalty = value;
            }
        }
    
        private System.String _platform = string.Empty;
    
        /// <summary>
        /// 供应商[字段]
        /// </summary>
        public System.String Platform
        {
            get
            {
                return _platform;
            }
            set
            {
                AddOriginal("Platform", _platform, value);
                _platform = value;
            }
        }
    
        private System.Decimal _price = 0.0m;
    
        /// <summary>
        /// 供应商返回原始价格[字段]
        /// </summary>
        public System.Decimal Price
        {
            get
            {
                return _price;
            }
            set
            {
                AddOriginal("Price", _price, value);
                _price = value;
            }
        }
    
        private System.String _priceBreakdownDate = string.Empty;
    
        /// <summary>
        /// 供应商返回入住时间，多天用逗号分开，以便判断计划入住3天而返回2天这种情况[字段]
        /// </summary>
        public System.String PriceBreakdownDate
        {
            get
            {
                return _priceBreakdownDate;
            }
            set
            {
                AddOriginal("PriceBreakdownDate", _priceBreakdownDate, value);
                _priceBreakdownDate = value;
            }
        }
    
        private System.String _priceBreakdownPrice = string.Empty;
    
        /// <summary>
        /// 供应商返回每天价格，多天用逗号分开[字段]
        /// </summary>
        public System.String PriceBreakdownPrice
        {
            get
            {
                return _priceBreakdownPrice;
            }
            set
            {
                AddOriginal("PriceBreakdownPrice", _priceBreakdownPrice, value);
                _priceBreakdownPrice = value;
            }
        }
    
        private System.String _rateplanId = string.Empty;
    
        /// <summary>
        /// 价格计划ID[字段]
        /// </summary>
        public System.String RateplanId
        {
            get
            {
                return _rateplanId;
            }
            set
            {
                AddOriginal("RateplanId", _rateplanId, value);
                _rateplanId = value;
            }
        }
    
        private System.String _referenceClient = string.Empty;
    
        /// <summary>
        /// 供应商返回房型表示[字段]
        /// </summary>
        public System.String ReferenceClient
        {
            get
            {
                return _referenceClient;
            }
            set
            {
                AddOriginal("ReferenceClient", _referenceClient, value);
                _referenceClient = value;
            }
        }
    
        private System.Decimal _rMBbasePrice = 0.0m;
    
        /// <summary>
        /// 供应商返回的收取时价格(人民币)[字段]
        /// </summary>
        public System.Decimal RMBbasePrice
        {
            get
            {
                return _rMBbasePrice;
            }
            set
            {
                AddOriginal("RMBbasePrice", _rMBbasePrice, value);
                _rMBbasePrice = value;
            }
        }
    
        private System.Decimal _rMBprice = 0.0m;
    
        /// <summary>
        /// 房间价格(人民币)[字段]
        /// </summary>
        public System.Decimal RMBprice
        {
            get
            {
                return _rMBprice;
            }
            set
            {
                AddOriginal("RMBprice", _rMBprice, value);
                _rMBprice = value;
            }
        }
    
        private System.Int32 _roomAdults = 0;
    
        /// <summary>
        /// 房间最大可入住成人数[字段]
        /// </summary>
        public System.Int32 RoomAdults
        {
            get
            {
                return _roomAdults;
            }
            set
            {
                AddOriginal("RoomAdults", _roomAdults, value);
                _roomAdults = value;
            }
        }
    
        private System.Int32 _roomChilds = 0;
    
        /// <summary>
        /// 房间最大可入住儿童数[字段]
        /// </summary>
        public System.Int32 RoomChilds
        {
            get
            {
                return _roomChilds;
            }
            set
            {
                AddOriginal("RoomChilds", _roomChilds, value);
                _roomChilds = value;
            }
        }
    
        private System.Int32 _roomCount = 0;
    
        /// <summary>
        /// 计划预订房间数[字段]
        /// </summary>
        public System.Int32 RoomCount
        {
            get
            {
                return _roomCount;
            }
            set
            {
                AddOriginal("RoomCount", _roomCount, value);
                _roomCount = value;
            }
        }
    
        private System.Int32 _roomsLeft = 0;
    
        /// <summary>
        /// 剩余房间数[字段]
        /// </summary>
        public System.Int32 RoomsLeft
        {
            get
            {
                return _roomsLeft;
            }
            set
            {
                AddOriginal("RoomsLeft", _roomsLeft, value);
                _roomsLeft = value;
            }
        }
    
        private System.String _roomText = string.Empty;
    
        /// <summary>
        /// 房间相关属性，在预订时需要用[字段]
        /// </summary>
        public System.String RoomText
        {
            get
            {
                return _roomText;
            }
            set
            {
                AddOriginal("RoomText", _roomText, value);
                _roomText = value;
            }
        }
    
        private System.String _roomTypeID = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String RoomTypeID
        {
            get
            {
                return _roomTypeID;
            }
            set
            {
                AddOriginal("RoomTypeID", _roomTypeID, value);
                _roomTypeID = value;
            }
        }
    
        private System.String _seachID = string.Empty;
    
        /// <summary>
        /// 报价时,供应商返回ID。有些供应商下单时需要报价时的ID[字段]
        /// </summary>
        public System.String SeachID
        {
            get
            {
                return _seachID;
            }
            set
            {
                AddOriginal("SeachID", _seachID, value);
                _seachID = value;
            }
        }
    
        private System.String _status = string.Empty;
    
        /// <summary>
        /// 状态[字段]
        /// </summary>
        public System.String Status
        {
            get
            {
                return _status;
            }
            set
            {
                AddOriginal("Status", _status, value);
                _status = value;
            }
        }
    
        private System.Decimal _surcharge = 0.0m;
    
        /// <summary>
        /// 到店付款的附加费[字段]
        /// </summary>
        public System.Decimal Surcharge
        {
            get
            {
                return _surcharge;
            }
            set
            {
                AddOriginal("Surcharge", _surcharge, value);
                _surcharge = value;
            }
        }
    
        private System.DateTime _updateTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// 修改时间[字段]
        /// </summary>
        public System.DateTime UpdateTime
        {
            get
            {
                return _updateTime;
            }
            set
            {
                AddOriginal("UpdateTime", _updateTime, value);
                _updateTime = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static HotelPrice DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as HotelPrice;
        }
    
        public HotelPrice Clone()
        {
            return EntityBase.Clone(this) as HotelPrice;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class HotelPriceCollection : EntityCollectionBase
    {

        public HotelPriceCollection()
        {
        }

        private static Type m_typ = typeof(HotelPriceCollection);
        private static Type m_typItem = typeof(HotelPrice);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public HotelPrice this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (HotelPrice)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public HotelPrice this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new HotelPrice GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as HotelPrice;
        }

        public int Add(HotelPrice value)
        {
            return List.Add(value);
        }

        public void Insert(int index, HotelPrice value)
        {
            List.Insert(index, value);
        }

        public void Remove(HotelPrice value)
        {
            List.Remove(value);
        }

        public void Delete(HotelPrice value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new HotelPriceCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(HotelPrice value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(HotelPrice value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class HotelPriceQuery : XiWan.DALFactory.EntityQueryBase
    {

        public HotelPriceQuery()
        {
            m_strTableName = "HotelPrice";

            m_prmAdults.SourceColumn = "Adults";
            this.Parameters.Add(m_prmAdults);

            m_prmBasePrice.SourceColumn = "BasePrice";
            this.Parameters.Add(m_prmBasePrice);

            m_prmBedTypeCode.SourceColumn = "BedTypeCode";
            this.Parameters.Add(m_prmBedTypeCode);

            m_prmBedTypeName.SourceColumn = "BedTypeName";
            this.Parameters.Add(m_prmBedTypeName);

            m_prmBreakfastName.SourceColumn = "BreakfastName";
            this.Parameters.Add(m_prmBreakfastName);

            m_prmCGuID.SourceColumn = "CGuID";
            this.Parameters.Add(m_prmCGuID);

            m_prmCheckID.SourceColumn = "CheckID";
            this.Parameters.Add(m_prmCheckID);

            m_prmCheckInDate.SourceColumn = "CheckInDate";
            this.Parameters.Add(m_prmCheckInDate);

            m_prmCheckOutDate.SourceColumn = "CheckOutDate";
            this.Parameters.Add(m_prmCheckOutDate);

            m_prmChilds.SourceColumn = "Childs";
            this.Parameters.Add(m_prmChilds);

            m_prmChildsAge.SourceColumn = "ChildsAge";
            this.Parameters.Add(m_prmChildsAge);

            m_prmClassName.SourceColumn = "ClassName";
            this.Parameters.Add(m_prmClassName);

            m_prmClassNo.SourceColumn = "ClassNo";
            this.Parameters.Add(m_prmClassNo);

            m_prmCreateTime.SourceColumn = "CreateTime";
            this.Parameters.Add(m_prmCreateTime);

            m_prmCurrencyCode.SourceColumn = "CurrencyCode";
            this.Parameters.Add(m_prmCurrencyCode);

            m_prmEachRoomAdult.SourceColumn = "EachRoomAdult";
            this.Parameters.Add(m_prmEachRoomAdult);

            m_prmEachRoomChild.SourceColumn = "EachRoomChild";
            this.Parameters.Add(m_prmEachRoomChild);

            m_prmExtraBed.SourceColumn = "ExtraBed";
            this.Parameters.Add(m_prmExtraBed);

            m_prmExtraBedPrice.SourceColumn = "ExtraBedPrice";
            this.Parameters.Add(m_prmExtraBedPrice);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmHotelNo.SourceColumn = "HotelNo";
            this.Parameters.Add(m_prmHotelNo);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmIsBreakfast.SourceColumn = "IsBreakfast";
            this.Parameters.Add(m_prmIsBreakfast);

            m_prmIsClose.SourceColumn = "IsClose";
            this.Parameters.Add(m_prmIsClose);

            m_prmPenalty.SourceColumn = "Penalty";
            this.Parameters.Add(m_prmPenalty);

            m_prmPlatform.SourceColumn = "Platform";
            this.Parameters.Add(m_prmPlatform);

            m_prmPrice.SourceColumn = "Price";
            this.Parameters.Add(m_prmPrice);

            m_prmPriceBreakdownDate.SourceColumn = "PriceBreakdownDate";
            this.Parameters.Add(m_prmPriceBreakdownDate);

            m_prmPriceBreakdownPrice.SourceColumn = "PriceBreakdownPrice";
            this.Parameters.Add(m_prmPriceBreakdownPrice);

            m_prmRateplanId.SourceColumn = "RateplanId";
            this.Parameters.Add(m_prmRateplanId);

            m_prmReferenceClient.SourceColumn = "ReferenceClient";
            this.Parameters.Add(m_prmReferenceClient);

            m_prmRMBbasePrice.SourceColumn = "RMBbasePrice";
            this.Parameters.Add(m_prmRMBbasePrice);

            m_prmRMBprice.SourceColumn = "RMBprice";
            this.Parameters.Add(m_prmRMBprice);

            m_prmRoomAdults.SourceColumn = "RoomAdults";
            this.Parameters.Add(m_prmRoomAdults);

            m_prmRoomChilds.SourceColumn = "RoomChilds";
            this.Parameters.Add(m_prmRoomChilds);

            m_prmRoomCount.SourceColumn = "RoomCount";
            this.Parameters.Add(m_prmRoomCount);

            m_prmRoomsLeft.SourceColumn = "RoomsLeft";
            this.Parameters.Add(m_prmRoomsLeft);

            m_prmRoomText.SourceColumn = "RoomText";
            this.Parameters.Add(m_prmRoomText);

            m_prmRoomTypeID.SourceColumn = "RoomTypeID";
            this.Parameters.Add(m_prmRoomTypeID);

            m_prmSeachID.SourceColumn = "SeachID";
            this.Parameters.Add(m_prmSeachID);

            m_prmStatus.SourceColumn = "Status";
            this.Parameters.Add(m_prmStatus);

            m_prmSurcharge.SourceColumn = "Surcharge";
            this.Parameters.Add(m_prmSurcharge);

            m_prmUpdateTime.SourceColumn = "UpdateTime";
            this.Parameters.Add(m_prmUpdateTime);
        }
        private IDataParameter m_prmAdults= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["Adults"]);
        public IDataParameter Adults
        {
            get { return m_prmAdults; }
            set { m_prmAdults = value; }
        }

        private IDataParameter m_prmBasePrice= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["BasePrice"]);
        public IDataParameter BasePrice
        {
            get { return m_prmBasePrice; }
            set { m_prmBasePrice = value; }
        }

        private IDataParameter m_prmBedTypeCode= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["BedTypeCode"]);
        public IDataParameter BedTypeCode
        {
            get { return m_prmBedTypeCode; }
            set { m_prmBedTypeCode = value; }
        }

        private IDataParameter m_prmBedTypeName= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["BedTypeName"]);
        public IDataParameter BedTypeName
        {
            get { return m_prmBedTypeName; }
            set { m_prmBedTypeName = value; }
        }

        private IDataParameter m_prmBreakfastName= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["BreakfastName"]);
        public IDataParameter BreakfastName
        {
            get { return m_prmBreakfastName; }
            set { m_prmBreakfastName = value; }
        }

        private IDataParameter m_prmCGuID= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["CGuID"]);
        public IDataParameter CGuID
        {
            get { return m_prmCGuID; }
            set { m_prmCGuID = value; }
        }

        private IDataParameter m_prmCheckID= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["CheckID"]);
        public IDataParameter CheckID
        {
            get { return m_prmCheckID; }
            set { m_prmCheckID = value; }
        }

        private IDataParameter m_prmCheckInDate= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["CheckInDate"]);
        public IDataParameter CheckInDate
        {
            get { return m_prmCheckInDate; }
            set { m_prmCheckInDate = value; }
        }

        private IDataParameter m_prmCheckOutDate= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["CheckOutDate"]);
        public IDataParameter CheckOutDate
        {
            get { return m_prmCheckOutDate; }
            set { m_prmCheckOutDate = value; }
        }

        private IDataParameter m_prmChilds= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["Childs"]);
        public IDataParameter Childs
        {
            get { return m_prmChilds; }
            set { m_prmChilds = value; }
        }

        private IDataParameter m_prmChildsAge= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["ChildsAge"]);
        public IDataParameter ChildsAge
        {
            get { return m_prmChildsAge; }
            set { m_prmChildsAge = value; }
        }

        private IDataParameter m_prmClassName= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["ClassName"]);
        public IDataParameter ClassName
        {
            get { return m_prmClassName; }
            set { m_prmClassName = value; }
        }

        private IDataParameter m_prmClassNo= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["ClassNo"]);
        public IDataParameter ClassNo
        {
            get { return m_prmClassNo; }
            set { m_prmClassNo = value; }
        }

        private IDataParameter m_prmCreateTime= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["CreateTime"]);
        public IDataParameter CreateTime
        {
            get { return m_prmCreateTime; }
            set { m_prmCreateTime = value; }
        }

        private IDataParameter m_prmCurrencyCode= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["CurrencyCode"]);
        public IDataParameter CurrencyCode
        {
            get { return m_prmCurrencyCode; }
            set { m_prmCurrencyCode = value; }
        }

        private IDataParameter m_prmEachRoomAdult= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["EachRoomAdult"]);
        public IDataParameter EachRoomAdult
        {
            get { return m_prmEachRoomAdult; }
            set { m_prmEachRoomAdult = value; }
        }

        private IDataParameter m_prmEachRoomChild= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["EachRoomChild"]);
        public IDataParameter EachRoomChild
        {
            get { return m_prmEachRoomChild; }
            set { m_prmEachRoomChild = value; }
        }

        private IDataParameter m_prmExtraBed= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["ExtraBed"]);
        public IDataParameter ExtraBed
        {
            get { return m_prmExtraBed; }
            set { m_prmExtraBed = value; }
        }

        private IDataParameter m_prmExtraBedPrice= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["ExtraBedPrice"]);
        public IDataParameter ExtraBedPrice
        {
            get { return m_prmExtraBedPrice; }
            set { m_prmExtraBedPrice = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmHotelNo= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["HotelNo"]);
        public IDataParameter HotelNo
        {
            get { return m_prmHotelNo; }
            set { m_prmHotelNo = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmIsBreakfast= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["IsBreakfast"]);
        public IDataParameter IsBreakfast
        {
            get { return m_prmIsBreakfast; }
            set { m_prmIsBreakfast = value; }
        }

        private IDataParameter m_prmIsClose= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["IsClose"]);
        public IDataParameter IsClose
        {
            get { return m_prmIsClose; }
            set { m_prmIsClose = value; }
        }

        private IDataParameter m_prmPenalty= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["Penalty"]);
        public IDataParameter Penalty
        {
            get { return m_prmPenalty; }
            set { m_prmPenalty = value; }
        }

        private IDataParameter m_prmPlatform= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["Platform"]);
        public IDataParameter Platform
        {
            get { return m_prmPlatform; }
            set { m_prmPlatform = value; }
        }

        private IDataParameter m_prmPrice= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["Price"]);
        public IDataParameter Price
        {
            get { return m_prmPrice; }
            set { m_prmPrice = value; }
        }

        private IDataParameter m_prmPriceBreakdownDate= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["PriceBreakdownDate"]);
        public IDataParameter PriceBreakdownDate
        {
            get { return m_prmPriceBreakdownDate; }
            set { m_prmPriceBreakdownDate = value; }
        }

        private IDataParameter m_prmPriceBreakdownPrice= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["PriceBreakdownPrice"]);
        public IDataParameter PriceBreakdownPrice
        {
            get { return m_prmPriceBreakdownPrice; }
            set { m_prmPriceBreakdownPrice = value; }
        }

        private IDataParameter m_prmRateplanId= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RateplanId"]);
        public IDataParameter RateplanId
        {
            get { return m_prmRateplanId; }
            set { m_prmRateplanId = value; }
        }

        private IDataParameter m_prmReferenceClient= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["ReferenceClient"]);
        public IDataParameter ReferenceClient
        {
            get { return m_prmReferenceClient; }
            set { m_prmReferenceClient = value; }
        }

        private IDataParameter m_prmRMBbasePrice= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RMBbasePrice"]);
        public IDataParameter RMBbasePrice
        {
            get { return m_prmRMBbasePrice; }
            set { m_prmRMBbasePrice = value; }
        }

        private IDataParameter m_prmRMBprice= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RMBprice"]);
        public IDataParameter RMBprice
        {
            get { return m_prmRMBprice; }
            set { m_prmRMBprice = value; }
        }

        private IDataParameter m_prmRoomAdults= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RoomAdults"]);
        public IDataParameter RoomAdults
        {
            get { return m_prmRoomAdults; }
            set { m_prmRoomAdults = value; }
        }

        private IDataParameter m_prmRoomChilds= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RoomChilds"]);
        public IDataParameter RoomChilds
        {
            get { return m_prmRoomChilds; }
            set { m_prmRoomChilds = value; }
        }

        private IDataParameter m_prmRoomCount= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RoomCount"]);
        public IDataParameter RoomCount
        {
            get { return m_prmRoomCount; }
            set { m_prmRoomCount = value; }
        }

        private IDataParameter m_prmRoomsLeft= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RoomsLeft"]);
        public IDataParameter RoomsLeft
        {
            get { return m_prmRoomsLeft; }
            set { m_prmRoomsLeft = value; }
        }

        private IDataParameter m_prmRoomText= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RoomText"]);
        public IDataParameter RoomText
        {
            get { return m_prmRoomText; }
            set { m_prmRoomText = value; }
        }

        private IDataParameter m_prmRoomTypeID= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["RoomTypeID"]);
        public IDataParameter RoomTypeID
        {
            get { return m_prmRoomTypeID; }
            set { m_prmRoomTypeID = value; }
        }

        private IDataParameter m_prmSeachID= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["SeachID"]);
        public IDataParameter SeachID
        {
            get { return m_prmSeachID; }
            set { m_prmSeachID = value; }
        }

        private IDataParameter m_prmStatus= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["Status"]);
        public IDataParameter Status
        {
            get { return m_prmStatus; }
            set { m_prmStatus = value; }
        }

        private IDataParameter m_prmSurcharge= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["Surcharge"]);
        public IDataParameter Surcharge
        {
            get { return m_prmSurcharge; }
            set { m_prmSurcharge = value; }
        }

        private IDataParameter m_prmUpdateTime= EntityBase.NewParameter(HotelPrice.m_dtbEntitySchema.Columns["UpdateTime"]);
        public IDataParameter UpdateTime
        {
            get { return m_prmUpdateTime; }
            set { m_prmUpdateTime = value; }
        }

    }
    #endregion
}