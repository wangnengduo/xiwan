// [实体版本]v2.7
// 2018-11-03 10:01:06
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class SunSeriesRoomType : EntityBase
    {

        #region 初始化
        public SunSeriesRoomType() : base()
        {
        }
    
        public SunSeriesRoomType(e_EntityState state) : base(state)
        {
        }
    
        static SunSeriesRoomType()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("SunSeriesRoomType");
    
            DataColumn dclBedName = new DataColumn(c_BedName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclBedName);
            dclBedName.Caption = "床型";
            dclBedName.AllowDBNull = true;
            dclBedName.MaxLength = 500;
    
            DataColumn dclCreateTime = new DataColumn(c_CreateTime,typeof(System.DateTime));
            m_dtbEntitySchema.Columns.Add(dclCreateTime);
            dclCreateTime.Caption = "";
            dclCreateTime.AllowDBNull = true;
    
            DataColumn dclHotelID = new DataColumn(c_HotelID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelID);
            dclHotelID.Caption = "酒店ID";
            dclHotelID.AllowDBNull = true;
            dclHotelID.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclMaxPerson = new DataColumn(c_MaxPerson,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclMaxPerson);
            dclMaxPerson.Caption = "最多可容人数";
            dclMaxPerson.AllowDBNull = true;
            dclMaxPerson.MaxLength = 50;
    
            DataColumn dclRoomTypeID = new DataColumn(c_RoomTypeID,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomTypeID);
            dclRoomTypeID.Caption = "房型ID";
            dclRoomTypeID.AllowDBNull = true;
            dclRoomTypeID.MaxLength = 50;
    
            DataColumn dclRoomTypeName = new DataColumn(c_RoomTypeName,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclRoomTypeName);
            dclRoomTypeName.Caption = "房型名称";
            dclRoomTypeName.AllowDBNull = true;
            dclRoomTypeName.MaxLength = 500;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(SunSeriesRoomType);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "BedName,CreateTime,HotelID,ID,MaxPerson,RoomTypeID,RoomTypeName";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_BedName = "BedName";
        public const string c_CreateTime = "CreateTime";
        public const string c_HotelID = "HotelID";
        public const string c_ID = "ID";
        public const string c_MaxPerson = "MaxPerson";
        public const string c_RoomTypeID = "RoomTypeID";
        public const string c_RoomTypeName = "RoomTypeName";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _bedName = string.Empty;
    
        /// <summary>
        /// 床型[字段]
        /// </summary>
        public System.String BedName
        {
            get
            {
                return _bedName;
            }
            set
            {
                AddOriginal("BedName", _bedName, value);
                _bedName = value;
            }
        }
    
        private System.DateTime _createTime = new DateTime(1900, 1, 1);
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.DateTime CreateTime
        {
            get
            {
                return _createTime;
            }
            set
            {
                AddOriginal("CreateTime", _createTime, value);
                _createTime = value;
            }
        }
    
        private System.String _hotelID = string.Empty;
    
        /// <summary>
        /// 酒店ID[字段]
        /// </summary>
        public System.String HotelID
        {
            get
            {
                return _hotelID;
            }
            set
            {
                AddOriginal("HotelID", _hotelID, value);
                _hotelID = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.String _maxPerson = string.Empty;
    
        /// <summary>
        /// 最多可容人数[字段]
        /// </summary>
        public System.String MaxPerson
        {
            get
            {
                return _maxPerson;
            }
            set
            {
                AddOriginal("MaxPerson", _maxPerson, value);
                _maxPerson = value;
            }
        }
    
        private System.String _roomTypeID = string.Empty;
    
        /// <summary>
        /// 房型ID[字段]
        /// </summary>
        public System.String RoomTypeID
        {
            get
            {
                return _roomTypeID;
            }
            set
            {
                AddOriginal("RoomTypeID", _roomTypeID, value);
                _roomTypeID = value;
            }
        }
    
        private System.String _roomTypeName = string.Empty;
    
        /// <summary>
        /// 房型名称[字段]
        /// </summary>
        public System.String RoomTypeName
        {
            get
            {
                return _roomTypeName;
            }
            set
            {
                AddOriginal("RoomTypeName", _roomTypeName, value);
                _roomTypeName = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static SunSeriesRoomType DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as SunSeriesRoomType;
        }
    
        public SunSeriesRoomType Clone()
        {
            return EntityBase.Clone(this) as SunSeriesRoomType;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class SunSeriesRoomTypeCollection : EntityCollectionBase
    {

        public SunSeriesRoomTypeCollection()
        {
        }

        private static Type m_typ = typeof(SunSeriesRoomTypeCollection);
        private static Type m_typItem = typeof(SunSeriesRoomType);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public SunSeriesRoomType this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (SunSeriesRoomType)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public SunSeriesRoomType this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new SunSeriesRoomType GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as SunSeriesRoomType;
        }

        public int Add(SunSeriesRoomType value)
        {
            return List.Add(value);
        }

        public void Insert(int index, SunSeriesRoomType value)
        {
            List.Insert(index, value);
        }

        public void Remove(SunSeriesRoomType value)
        {
            List.Remove(value);
        }

        public void Delete(SunSeriesRoomType value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new SunSeriesRoomTypeCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(SunSeriesRoomType value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(SunSeriesRoomType value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class SunSeriesRoomTypeQuery : XiWan.DALFactory.EntityQueryBase
    {

        public SunSeriesRoomTypeQuery()
        {
            m_strTableName = "SunSeriesRoomType";

            m_prmBedName.SourceColumn = "BedName";
            this.Parameters.Add(m_prmBedName);

            m_prmCreateTime.SourceColumn = "CreateTime";
            this.Parameters.Add(m_prmCreateTime);

            m_prmHotelID.SourceColumn = "HotelID";
            this.Parameters.Add(m_prmHotelID);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmMaxPerson.SourceColumn = "MaxPerson";
            this.Parameters.Add(m_prmMaxPerson);

            m_prmRoomTypeID.SourceColumn = "RoomTypeID";
            this.Parameters.Add(m_prmRoomTypeID);

            m_prmRoomTypeName.SourceColumn = "RoomTypeName";
            this.Parameters.Add(m_prmRoomTypeName);
        }
        private IDataParameter m_prmBedName= EntityBase.NewParameter(SunSeriesRoomType.m_dtbEntitySchema.Columns["BedName"]);
        public IDataParameter BedName
        {
            get { return m_prmBedName; }
            set { m_prmBedName = value; }
        }

        private IDataParameter m_prmCreateTime= EntityBase.NewParameter(SunSeriesRoomType.m_dtbEntitySchema.Columns["CreateTime"]);
        public IDataParameter CreateTime
        {
            get { return m_prmCreateTime; }
            set { m_prmCreateTime = value; }
        }

        private IDataParameter m_prmHotelID= EntityBase.NewParameter(SunSeriesRoomType.m_dtbEntitySchema.Columns["HotelID"]);
        public IDataParameter HotelID
        {
            get { return m_prmHotelID; }
            set { m_prmHotelID = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(SunSeriesRoomType.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmMaxPerson= EntityBase.NewParameter(SunSeriesRoomType.m_dtbEntitySchema.Columns["MaxPerson"]);
        public IDataParameter MaxPerson
        {
            get { return m_prmMaxPerson; }
            set { m_prmMaxPerson = value; }
        }

        private IDataParameter m_prmRoomTypeID= EntityBase.NewParameter(SunSeriesRoomType.m_dtbEntitySchema.Columns["RoomTypeID"]);
        public IDataParameter RoomTypeID
        {
            get { return m_prmRoomTypeID; }
            set { m_prmRoomTypeID = value; }
        }

        private IDataParameter m_prmRoomTypeName= EntityBase.NewParameter(SunSeriesRoomType.m_dtbEntitySchema.Columns["RoomTypeName"]);
        public IDataParameter RoomTypeName
        {
            get { return m_prmRoomTypeName; }
            set { m_prmRoomTypeName = value; }
        }

    }
    #endregion
}