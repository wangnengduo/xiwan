// [实体版本]v2.7
// 2018-07-20 14:48:08
using System;
using System.Data;
using System.Collections;
using System.Collections.Specialized;
using XiWan.DALFactory;

namespace XiWan.DALFactory.Model
{
    [Serializable]
    public class Planitour_HotelDetail : EntityBase
    {

        #region 初始化
        public Planitour_HotelDetail() : base()
        {
        }
    
        public Planitour_HotelDetail(e_EntityState state) : base(state)
        {
        }
    
        static Planitour_HotelDetail()
        {
            InitializeSchema();
        }
    
        private static void InitializeSchema()
        {
            m_dtbEntitySchema = new DataTable("Planitour_HotelDetail");
    
            DataColumn dclAddress = new DataColumn(c_Address,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclAddress);
            dclAddress.Caption = "";
            dclAddress.AllowDBNull = true;
            dclAddress.MaxLength = 1000;
    
            DataColumn dclCategory = new DataColumn(c_Category,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclCategory);
            dclCategory.Caption = "";
            dclCategory.AllowDBNull = true;
            dclCategory.MaxLength = 200;
    
            DataColumn dclDescription = new DataColumn(c_Description,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclDescription);
            dclDescription.Caption = "";
            dclDescription.AllowDBNull = true;
            dclDescription.MaxLength = 500;
    
            DataColumn dclEmail = new DataColumn(c_Email,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclEmail);
            dclEmail.Caption = "";
            dclEmail.AllowDBNull = true;
            dclEmail.MaxLength = 100;
    
            DataColumn dclHotelCode = new DataColumn(c_HotelCode,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclHotelCode);
            dclHotelCode.Caption = "";
            dclHotelCode.AllowDBNull = true;
            dclHotelCode.MaxLength = 50;
    
            DataColumn dclID = new DataColumn(c_ID,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclID);
            dclID.Caption = "";
            dclID.AllowDBNull = true;
            dclID.AutoIncrement = true;
            dclID.AutoIncrementSeed = 1;
            dclID.AutoIncrementStep = 1;
    
            DataColumn dclLatitude = new DataColumn(c_Latitude,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclLatitude);
            dclLatitude.Caption = "";
            dclLatitude.AllowDBNull = true;
    
            DataColumn dclLocation = new DataColumn(c_Location,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclLocation);
            dclLocation.Caption = "";
            dclLocation.AllowDBNull = true;
            dclLocation.MaxLength = 1000;
    
            DataColumn dclLongitude = new DataColumn(c_Longitude,typeof(System.Decimal));
            m_dtbEntitySchema.Columns.Add(dclLongitude);
            dclLongitude.Caption = "";
            dclLongitude.AllowDBNull = true;
    
            DataColumn dclName = new DataColumn(c_Name,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclName);
            dclName.Caption = "";
            dclName.AllowDBNull = true;
            dclName.MaxLength = 500;
    
            DataColumn dclPhone = new DataColumn(c_Phone,typeof(System.String));
            m_dtbEntitySchema.Columns.Add(dclPhone);
            dclPhone.Caption = "";
            dclPhone.AllowDBNull = true;
            dclPhone.MaxLength = 50;
    
            DataColumn dclStarRating = new DataColumn(c_StarRating,typeof(System.Int32));
            m_dtbEntitySchema.Columns.Add(dclStarRating);
            dclStarRating.Caption = "";
            dclStarRating.AllowDBNull = true;
    
            EntityBase.InitializeColumnDefalutValue(m_dtbEntitySchema);
            m_dtbEntitySchema.PrimaryKey = new DataColumn[] { dclID };
            m_dtbEntitySchema.AcceptChanges();
            m_listEntityParameters = EntityBase.BuildParameters(m_dtbEntitySchema);
    
        }
        #endregion

        #region 结构
        private static Type m_typ = typeof(Planitour_HotelDetail);
        internal static DataTable m_dtbEntitySchema = null;
        private static Hashtable m_listEntityExtended = null;
        private static string m_strEntityExtendedColumnsName = "";
        private static string m_strEntityColumnsName = "Address,Category,Description,Email,HotelCode,ID,Latitude,Location,Longitude,Name,Phone,StarRating";
        private static string m_strEntityIdentityColumnName = "ID";
        private static string m_strSqlWhere = "ID=@ID_Where";
        private static Hashtable m_listEntityParameters = null;
    
        public const string c_Address = "Address";
        public const string c_Category = "Category";
        public const string c_Description = "Description";
        public const string c_Email = "Email";
        public const string c_HotelCode = "HotelCode";
        public const string c_ID = "ID";
        public const string c_Latitude = "Latitude";
        public const string c_Location = "Location";
        public const string c_Longitude = "Longitude";
        public const string c_Name = "Name";
        public const string c_Phone = "Phone";
        public const string c_StarRating = "StarRating";
    
        public static Type EntityType
        {
            get { return m_typ; }
        }
    
        public static DataTable EntitySchema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public static Hashtable EntityExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public static Hashtable EntityParameters
        {
            get { return m_listEntityParameters; }
        }
    
        public static string EntityColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public static string EntityIdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public static string EntitySql_Where
        {
            get { return m_strSqlWhere; }
        }
    
        public override Type Type
        {
            get { return m_typ; }
        }
    
        public override DataTable Schema
        {
            get { return m_dtbEntitySchema; }
        }
    
        public override Hashtable ExtendedList
        {
            get { return m_listEntityExtended; }
        }
    
        public override Hashtable Parameters
        {
            get { return m_listEntityParameters; }
        }
    
        public override string ColumnsName
        {
            get { return m_strEntityColumnsName; }
        }
    
        public override string ExtendedColumnsName
        {
            get { return m_strEntityExtendedColumnsName; }
        }
    
        public override string IdentityColumnName
        {
            get { return m_strEntityIdentityColumnName; }
        }
    
        public override string Sql_Where
        {
            get { return m_strSqlWhere; }
        }
        #endregion

        #region 字段
        private System.String _address = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Address
        {
            get
            {
                return _address;
            }
            set
            {
                AddOriginal("Address", _address, value);
                _address = value;
            }
        }
    
        private System.String _category = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Category
        {
            get
            {
                return _category;
            }
            set
            {
                AddOriginal("Category", _category, value);
                _category = value;
            }
        }
    
        private System.String _description = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Description
        {
            get
            {
                return _description;
            }
            set
            {
                AddOriginal("Description", _description, value);
                _description = value;
            }
        }
    
        private System.String _email = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Email
        {
            get
            {
                return _email;
            }
            set
            {
                AddOriginal("Email", _email, value);
                _email = value;
            }
        }
    
        private System.String _hotelCode = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String HotelCode
        {
            get
            {
                return _hotelCode;
            }
            set
            {
                AddOriginal("HotelCode", _hotelCode, value);
                _hotelCode = value;
            }
        }
    
        private System.Int32 _iD = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 ID
        {
            get
            {
                return _iD;
            }
            set
            {
                AddOriginal("ID", _iD, value);
                _iD = value;
            }
        }
    
        private System.Decimal _latitude = 0.0m;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Decimal Latitude
        {
            get
            {
                return _latitude;
            }
            set
            {
                AddOriginal("Latitude", _latitude, value);
                _latitude = value;
            }
        }
    
        private System.String _location = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Location
        {
            get
            {
                return _location;
            }
            set
            {
                AddOriginal("Location", _location, value);
                _location = value;
            }
        }
    
        private System.Decimal _longitude = 0.0m;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Decimal Longitude
        {
            get
            {
                return _longitude;
            }
            set
            {
                AddOriginal("Longitude", _longitude, value);
                _longitude = value;
            }
        }
    
        private System.String _name = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Name
        {
            get
            {
                return _name;
            }
            set
            {
                AddOriginal("Name", _name, value);
                _name = value;
            }
        }
    
        private System.String _phone = string.Empty;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.String Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                AddOriginal("Phone", _phone, value);
                _phone = value;
            }
        }
    
        private System.Int32 _starRating = 0;
    
        /// <summary>
        /// [字段]
        /// </summary>
        public System.Int32 StarRating
        {
            get
            {
                return _starRating;
            }
            set
            {
                AddOriginal("StarRating", _starRating, value);
                _starRating = value;
            }
        }
        #endregion
    
        #region 扩展字段
        #endregion
    
        #region 属性
        public override object[] KeyValues
        {
            get
            {
                if (HasChange)
                {
                    return GetKeyValues();
                }
                else
                {
                    return new object[] { _iD };
                }
            }
        }
        #endregion

        #region 操作
        public static Planitour_HotelDetail DataRowToEntity(DataRow drw)
        {
            return EntityBase.DataRowToEntity(m_typ, drw) as Planitour_HotelDetail;
        }
    
        public Planitour_HotelDetail Clone()
        {
            return EntityBase.Clone(this) as Planitour_HotelDetail;
        }
        #endregion
    

    }

    #region 实体集合类
    [Serializable]
    public class Planitour_HotelDetailCollection : EntityCollectionBase
    {

        public Planitour_HotelDetailCollection()
        {
        }

        private static Type m_typ = typeof(Planitour_HotelDetailCollection);
        private static Type m_typItem = typeof(Planitour_HotelDetail);

        public override Type Type
        {
            get
            {
                return m_typ;
            }
        }

        public override Type ItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public static Type CollectionType
        {
            get
            {
                return m_typ;
            }
        }

        public static Type CollectionItemType
        {
            get
            {
                return m_typItem;
            }
        }

        public Planitour_HotelDetail this[int index]
        {
            get
            {
                if (index < 0)
                {
                    return null;
                }
                return (Planitour_HotelDetail)List[index];
            }
            set
            {
                if (index < 0)
                {
                    return;
                }
                List[index] = value;
            }
        }

        public Planitour_HotelDetail this[object keyValues]
        {
            get { return this[IndexOf(GetItem(keyValues))]; }
            set { this[IndexOf(GetItem(keyValues))] = value; }
        }

        public new Planitour_HotelDetail GetItem(object keyValues)
        {
            return base.GetItem(keyValues) as Planitour_HotelDetail;
        }

        public int Add(Planitour_HotelDetail value)
        {
            return List.Add(value);
        }

        public void Insert(int index, Planitour_HotelDetail value)
        {
            List.Insert(index, value);
        }

        public void Remove(Planitour_HotelDetail value)
        {
            List.Remove(value);
        }

        public void Delete(Planitour_HotelDetail value)
        {
            List.Remove(value);
            if (m_collectionDeleted == null)
            {
                m_collectionDeleted = new Planitour_HotelDetailCollection();
            }
            if (value.EntityState != e_EntityState.Added)
            {
                m_collectionDeleted.Add(value);
                value.SetEntityState(e_EntityState.Deleted);
            }
        }

        public void DeleteAll()
        {
            if (this.Count == 0)
            {
                return;
            }
            
            for (int i = this.Count - 1; i >= 0; i--)
            {
                Delete(this[i]);
            }
        }

        public int IndexOf(Planitour_HotelDetail value)
        {
            return List.IndexOf(value);
        }

        public bool Contains(Planitour_HotelDetail value)
        {
            return List.Contains(value);
        }
    }
    #endregion

    #region 实体查询类
    public class Planitour_HotelDetailQuery : XiWan.DALFactory.EntityQueryBase
    {

        public Planitour_HotelDetailQuery()
        {
            m_strTableName = "Planitour_HotelDetail";

            m_prmAddress.SourceColumn = "Address";
            this.Parameters.Add(m_prmAddress);

            m_prmCategory.SourceColumn = "Category";
            this.Parameters.Add(m_prmCategory);

            m_prmDescription.SourceColumn = "Description";
            this.Parameters.Add(m_prmDescription);

            m_prmEmail.SourceColumn = "Email";
            this.Parameters.Add(m_prmEmail);

            m_prmHotelCode.SourceColumn = "HotelCode";
            this.Parameters.Add(m_prmHotelCode);

            m_prmID.SourceColumn = "ID";
            this.Parameters.Add(m_prmID);

            m_prmLatitude.SourceColumn = "Latitude";
            this.Parameters.Add(m_prmLatitude);

            m_prmLocation.SourceColumn = "Location";
            this.Parameters.Add(m_prmLocation);

            m_prmLongitude.SourceColumn = "Longitude";
            this.Parameters.Add(m_prmLongitude);

            m_prmName.SourceColumn = "Name";
            this.Parameters.Add(m_prmName);

            m_prmPhone.SourceColumn = "Phone";
            this.Parameters.Add(m_prmPhone);

            m_prmStarRating.SourceColumn = "StarRating";
            this.Parameters.Add(m_prmStarRating);
        }
        private IDataParameter m_prmAddress= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Address"]);
        public IDataParameter Address
        {
            get { return m_prmAddress; }
            set { m_prmAddress = value; }
        }

        private IDataParameter m_prmCategory= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Category"]);
        public IDataParameter Category
        {
            get { return m_prmCategory; }
            set { m_prmCategory = value; }
        }

        private IDataParameter m_prmDescription= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Description"]);
        public IDataParameter Description
        {
            get { return m_prmDescription; }
            set { m_prmDescription = value; }
        }

        private IDataParameter m_prmEmail= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Email"]);
        public IDataParameter Email
        {
            get { return m_prmEmail; }
            set { m_prmEmail = value; }
        }

        private IDataParameter m_prmHotelCode= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["HotelCode"]);
        public IDataParameter HotelCode
        {
            get { return m_prmHotelCode; }
            set { m_prmHotelCode = value; }
        }

        private IDataParameter m_prmID= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["ID"]);
        public IDataParameter ID
        {
            get { return m_prmID; }
            set { m_prmID = value; }
        }

        private IDataParameter m_prmLatitude= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Latitude"]);
        public IDataParameter Latitude
        {
            get { return m_prmLatitude; }
            set { m_prmLatitude = value; }
        }

        private IDataParameter m_prmLocation= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Location"]);
        public IDataParameter Location
        {
            get { return m_prmLocation; }
            set { m_prmLocation = value; }
        }

        private IDataParameter m_prmLongitude= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Longitude"]);
        public IDataParameter Longitude
        {
            get { return m_prmLongitude; }
            set { m_prmLongitude = value; }
        }

        private IDataParameter m_prmName= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Name"]);
        public IDataParameter Name
        {
            get { return m_prmName; }
            set { m_prmName = value; }
        }

        private IDataParameter m_prmPhone= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["Phone"]);
        public IDataParameter Phone
        {
            get { return m_prmPhone; }
            set { m_prmPhone = value; }
        }

        private IDataParameter m_prmStarRating= EntityBase.NewParameter(Planitour_HotelDetail.m_dtbEntitySchema.Columns["StarRating"]);
        public IDataParameter StarRating
        {
            get { return m_prmStarRating; }
            set { m_prmStarRating = value; }
        }

    }
    #endregion
}