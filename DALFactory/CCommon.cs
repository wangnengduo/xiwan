﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Web;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Net;

namespace XiWan.DALFactory
{
    /// <summary>
    /// 常用静态方法集合类
    /// </summary>
    /// 


    public class CCommon
    {

        /// <summary>
        /// 只读默认连接字符串
        /// </summary>
        public readonly string DefaultConnectionString = GetConnectionString();
        /// <summary>
        /// 将相对路径转换为绝对路径
        /// </summary>
        /// <param name="httpPath">相对路径地址,如Doc/XmlDocs.xml</param>
        /// <returns>返回转换后的绝对路径E:\Project\CallCenter2008\Doc\XmlDocs.xml</returns>
        static public string MapPath(string httpPath)
        {
            return HttpContext.Current.Server.MapPath(httpPath);
        }
        /// <summary>
        /// 通过Reponse对象在客户端设置Cookie
        /// </summary>
        /// <param name="key">cookie的键</param>
        /// <param name="value">cookie对应的值</param>
        public static void SetCookies(string key, string value)
        {
            if (key != null && key != "")
            {
                HttpContext.Current.Response.Cookies[key].Value = value;
                HttpContext.Current.Response.Cookies[key].Expires = System.DateTime.Now.AddDays(1);
            }
        }

        /// <summary>
        /// 通过Reponse对象在客户端设置Session
        /// </summary>
        /// <param name="key">Session的键</param>
        /// <param name="value">Session对应的值</param>
        public static void SetSession(string key, object value)
        {
            if (key != null)
            {
                HttpContext.Current.Session[key] = value;
            }
        }



     


        /// <summary>
        /// 通过Request对象从客户端获取Cookie
        /// </summary>
        /// <param name="key">cookie键</param>
        /// <returns></returns>
        public static string GetCookies(string key)
        {
            if (HttpContext.Current.Request.Cookies[key] != null && HttpContext.Current.Request.Cookies[key].Value != null)
                return HttpContext.Current.Request.Cookies[key].Value;
            else
                return "";
        }

        /// <summary>
        /// 通过Request对象从客户端获取Session
        /// </summary>
        /// <param name="key">Session键</param>
        /// <returns></returns>
        public static string GetSession(string key)
        {
            if (HttpContext.Current.Session!=null&&HttpContext.Current.Session[key] != null && HttpContext.Current.Session[key].ToString() != "")
                return HttpContext.Current.Session[key].ToString();
            else
                return string.Empty;
        }

        /// <summary>
        /// 通过Request对象从客户端获取Session
        /// </summary>
        /// <param name="key">Session键</param>
        /// <returns>返回Session对象</returns>
        public static object GetSessionObj(string key)
        {
            if (HttpContext.Current.Session[key] != null)
                return HttpContext.Current.Session[key];
            else
                return null;
        }

        /// <summary>
        /// 得到当前用户Code
        /// </summary>
        /// <returns>获取成功返回UserCode，失败返回""</returns>
        public static string GetUserCode()
        {
            if (GetCookies("UserCode") != "")
                return GetCookies("UserCode");
            else
                return "";
        }
        /// <summary>
        /// 得到当前用户Name
        /// </summary>
        /// <returns>获取成功返回UserName，失败返回""</returns>
        public static string GetUserName()
        {
            if (GetCookies("UserName") != "")
                return GetCookies("UserName");
            else
                return "";
        }



        /// <summary>
        /// 得到默认的数据库连接字符串，以打开事务
        /// </summary>
        /// <returns></returns>
        public static string GetConnectionString()
        {
            return System.Web.Configuration.WebConfigurationManager.ConnectionStrings["Lib.DataAccess.ConnectionString"].ConnectionString;
        }
        /// <summary>
        /// 是否是调试
        /// </summary>
        /// <returns></returns>
        public static bool Isdebuger()
        {
            bool bl = false;
            
            //if (System.Web.Configuration.WebConfigurationManager.AppSettings["IsDebugMode"] == "1")
            //    bl = true;

            return bl;
            
        }


        /// <summary>
        /// 得到默认的数据库连接字符串，以打开事务
        /// </summary>
        /// <param name="connStringName">Web.Config中定义的数据库连接字符串的名称</param>
        /// <returns>返回连接字符串</returns>
        public static string GetConnectionString(string connStringName)
        {
            return System.Web.Configuration.WebConfigurationManager.ConnectionStrings[connStringName].ConnectionString;
        }

        /// <summary>
        /// 对文本进行加密，返回密文
        /// </summary>
        /// <param name="str">需要加密的字串</param>
        /// <returns>返回加密后的字串，如果传入的字串为"",则返回""</returns>
        static public string GetPasswordText(string str)
        {
            if (str != null && str != "")
            {
                return System.Web.Security.FormsAuthentication.HashPasswordForStoringInConfigFile(str, System.Web.Configuration.FormsAuthPasswordFormat.SHA1.ToString());
            }
            else
                return "";
        }

        /// <summary>
        /// 得到Web.Config文件的配置值appSettings
        /// </summary>
        /// <param name="key">appSettings键</param>
        /// <returns>获取成功返回字串，失败返回""</returns>
        static public string GetWebConfigValue(string key)
        {
            if (System.Web.Configuration.WebConfigurationManager.AppSettings[key] != null && System.Web.Configuration.WebConfigurationManager.AppSettings[key] != "")
                return System.Web.Configuration.WebConfigurationManager.AppSettings[key];
            else
                return "";
        }
        static public string GetWebConfigValue(string key,string strDefault)
        {
            if (System.Web.Configuration.WebConfigurationManager.AppSettings[key] != null && System.Web.Configuration.WebConfigurationManager.AppSettings[key] != "")
                return System.Web.Configuration.WebConfigurationManager.AppSettings[key];
            else
                return strDefault;
        }

        /// <summary>
        /// 判断字符串是否为数字，是则返回数字，否则范围0
        /// </summary>
        /// <param name="Str"></param>
        /// <returns></returns>
        static public int ToInt(string Str)
        {
            int Result = 0;
            try
            {
                Result = int.Parse(Str);
            }
            catch
            {

            }
            finally
            {
               
            }
            return Result;
        }

        /// <summary>
        /// 客户端弹出框
        /// </summary>
        /// <param name="msg">弹出框显示的内容</param>
        static public void MessageBox(string msg)
        {
            HttpContext.Current.Response.Write("<script> alert('" + msg + "');</script>");
        }

        /// <summary>
        /// 对输入的字符串进行替换，过滤掉特殊字符
        /// </summary>
        /// <param name="text">输入文本</param>
        /// <param name="maxLength">返回的最大长度</param>
        /// <returns>返回替换后的字符串</returns>
        public static string InputText(string text, int maxLength)
        {
            text = text.Trim();
            if (string.IsNullOrEmpty(text))
                return string.Empty;
            if (text.Length > maxLength)
                text = text.Substring(0, maxLength);
            text = Regex.Replace(text, "[\\s]{2,}", " ");	//two or more spaces
            text = Regex.Replace(text, "(<[b|B][r|R]/*>)+|(<[p|P](.|\\n)*?>)", "\n");	//<br>
            text = Regex.Replace(text, "(\\s*&[n|N][b|B][s|S][p|P];\\s*)+", " ");	//&nbsp;
            text = Regex.Replace(text, "<(.|\\n)*?>", string.Empty);	//any other tags
            text = text.Replace("'", "''");
            return text;
        }

        /// <summary>
        /// 过滤Excel的信息
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string FilterExcelText(string text)
        {
            text = text.Replace("\r\n", ",").Replace("	", ",").Replace(" ", "").Replace(",,", ",").Replace(",,", ",");
             
            return text;
        }

        #region//URL路径,网址目录等信息

        /// <summary>
        /// 获取指定页的URL路径，不含文件名 
        /// </summary>
        /// <example>
        /// http://www.xxx.com/manage/admin.aspx  --> http://www.xxx.com/manage/
        /// </example>
        /// <param name="page"></param>
        /// <returns></returns>
        public static string GetPagePath(System.Web.UI.Page page)
        {
            string url = page.Request.Url.AbsoluteUri;
            return url.Substring(0, url.LastIndexOf("/") + 1);
        }

        /// <summary>
        /// 网站的虚拟目录路径    / or /manage/
        /// </summary>
        public static string ApplicationPath
        {
            get
            {
                string url = HttpContext.Current.Request.ApplicationPath;
                return url == "/" ? url : url + "/";
            }
        }

        /// <summary>
        /// 网站的绝对地址
        /// </summary>
        public static string ApplicationAbsoluteUri
        {
            get
            {
                return string.Format(
                    "http://{0}:{1}",
                    HttpContext.Current.Request.Url.Host,
                    HttpContext.Current.Request.Url.Port + ApplicationPath);
            }
        }


  
        /// <summary>
        /// 网址目录
        /// </summary>
        /// <param name="strHtmlPagePath"></param>
        /// <returns></returns>
        public static string[] GetUrlFolerName(string strHtmlPagePath)
        {
            //抓取网址字符串中的文件目录

            int at = 0;
            int start = 0;
            int notei = 0;
            int endi = 0;
            int[] myIntArray = new int[10];
            string[] ArrayFolderName = null;
            string NewFolderName;
            while ((start < strHtmlPagePath.Length) && (at > -1))
            {
                at = strHtmlPagePath.IndexOf('/', start);
                if (at == -1) break;
                myIntArray[notei] = at;
                start = at + 1;
                notei = notei + 1;
                endi = at;
            }
            ArrayFolderName = new string[notei - 1];
            for (int i = 0; i < notei; i++)
            {
                if (myIntArray[i] > 0)
                {
                    if (myIntArray[i + 1] > 0)
                    {
                        NewFolderName = strHtmlPagePath.Substring(myIntArray[i] + 1, myIntArray[i + 1] - myIntArray[i] - 1);
                        ArrayFolderName.SetValue(NewFolderName, i);
                    }

                }
            }
            return ArrayFolderName;
        }
        #endregion

        #region 客户端信息

        /// <summary>
        /// 客户端IP地址
        /// </summary>
        public static string ClientIP
        {
            get
            {
                string ip;
                try
                {
                    HttpRequest request = HttpContext.Current.Request;
                    if (request.ServerVariables["HTTP_VIA"] != null)
                    {
                        ip = request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString().Split(',')[0].Trim();
                    }
                    else
                    {
                        ip = request.UserHostAddress;
                    }
                    //若为本地ip，则调用其他方法获取
                    if (ip == "127.0.0.1")
                    {
                        ip = GetClientIPv4();
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                return ip;
            }
        }

        //判断客户端有没有使用代理使用代理返回true,没有使用代理返回false
        public static bool IsProxy()
        {
            bool flag = false;
            if (HttpContext.Current.Request.ServerVariables["HTTP_VIA"] != null) // 使用了代理
            {
                flag = true;
            }
            else //没有使用代理
            {
                flag = false;
            }
            return flag;
        }


        //获取服务端的IPv4
        public static string GetClientIPv4()
        {
            string ipv4 = String.Empty;
            //foreach (IPAddress IPA in Dns.GetHostAddresses(HttpContext.Current.Request.UserHostAddress))
            //{
            //    if (IPA.AddressFamily.ToString() == "InterNetwork")
            //    {
            //        ipv4 = IPA.ToString();
            //        break;
            //    }
            //}
            //if (ipv4 != String.Empty)
            //{
            //    return ipv4;
            //}
            foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
            {
                if (IPA.AddressFamily.ToString() == "InterNetwork")
                {
                    ipv4 = IPA.ToString();
                    break;
                }
            }
            return ipv4;
        }


        /// <summary>
        /// 客户端代理IP
        /// </summary>
        /// <returns></returns>
        public static string ClientViaIP
        {
            get
            {
                string viaIp = null;
                try
                {
                    HttpRequest request = HttpContext.Current.Request;
                    if (request.ServerVariables["HTTP_VIA"] != null)
                    {
                        viaIp = request.UserHostAddress;
                    }
                }
                catch (Exception e)
                {
                    throw e;
                }
                return viaIp;
            }
        }
        #endregion

        #region 文本框信息转为html网页显示格式
        public static string TextToHTML(string newContent)
        {
            newContent = newContent.Replace("\r", "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace(" ", "&nbsp;");
            newContent = newContent.Replace("\n", "");
            return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + newContent;
        }
        #endregion 


        #region 将某一值加上日期后加密返回
        public static string GetEncryptDateVal(string strText)
        {
            // 加上时间
            string strReParam = string.Empty;
            try
            {
                strReParam = strText + ":" + DateTime.Now.ToString("yyyyMMddHHmmss");
                string strKey = CCommon.GetWebConfigValue("TokenEncryptKey");
                strKey = strKey == "" ? "cashchina3321" : strKey;
                strReParam = SecurityHelper.DESCommonEncrypt(strReParam, strKey);
                strReParam = System.Web.HttpUtility.UrlEncode(strReParam);
            }
            catch
            {
                strReParam = "";
            }
            return strReParam;
        }
        #endregion 


        #region 带分类ID的加密
        public static string GetEncryptDateVal(string strText,string typeId)
        {
            // 加上时间
            string strReParam = string.Empty;
            try
            {
                strReParam = strText + ":" + DateTime.Now.ToString("yyyyMMddHHmmss") + ":" + typeId;
                string strKey = CCommon.GetWebConfigValue("TokenEncryptKey");
                strKey = strKey == "" ? "cashchina3321" : strKey;
                strReParam = SecurityHelper.DESCommonEncrypt(strReParam, strKey);
                strReParam = System.Web.HttpUtility.UrlEncode(strReParam);
            }
            catch
            {
                strReParam = "";
            }
            return strReParam;
        }
        #endregion 


        #region 解析加密对：获取主键信息并检查是否过期，若过期，则返回0，否则返回键值
        public static string GetDecKey(string strIdKey)
        {
            int typeId = 0;
            return GetDecKey(strIdKey, out typeId); 
        }
        #endregion 


        #region 格式化审批意见
        /// <summary>
        /// 格式化审批意见
        /// </summary>
        /// <param name="Obj"></param>
        /// <returns></returns>
        public static string TextToHTML2(string Obj)
        {
            string newContent = Obj;
            newContent = newContent.Replace("；", "；\r");
            newContent = newContent.Replace("\r\r", "\r");
            newContent = newContent.Replace("\r", "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;").Replace(" ", "&nbsp;");
            newContent = newContent.Replace("\n", "");
            //newContent = newContent.Replace("；", "；<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");

            return "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + newContent; //&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        }
        #endregion 

        #region 组装查询条件语句
        /// <summary>
        /// 组装常用的查询条件；过滤空的值
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="ColumnValue"></param>
        /// <param name="FormatType">Like:字符like组装 Numer等于任何数据 Number+大于-1时才组装 ，空表示组装=字符型语句</param>
        /// <returns></returns>
        public static string GetWhere(string ColumnName, string ColumnValue, string FormatType)
        {
            string strWhere = "";
            ColumnValue = ColumnValue.Trim();
            switch (FormatType)
            {
                case "Like":
                    if (!string.IsNullOrEmpty(ColumnValue))
                    {
                        strWhere = string.Format(" AND {0} like '%{1}%' ", ColumnName, ColumnValue);
                    }
                    break;
                case "Number":
                    if (!string.IsNullOrEmpty(ColumnValue))
                    {
                        strWhere = string.Format(" AND {0}={1} ", ColumnName, ColumnValue);
                    }
                    break;
                case "Number+":
                    if (CConvert.ObjectToInt(ColumnValue) > -1)
                    {
                        strWhere = string.Format(" AND {0}={1} ", ColumnName, ColumnValue);
                    }
                    break;
                case "Number1+":
                    if (CConvert.ObjectToInt(ColumnValue) > 0)
                    {
                        strWhere = string.Format(" AND {0}={1} ", ColumnName, ColumnValue);
                    }
                    break;
                case "StringIn":
                    if (!string.IsNullOrEmpty(ColumnValue))
                    {
                        strWhere = string.Format(" AND {0} in ('{1}') ", ColumnName, ColumnValue.Replace(",", "','"));
                    }
                    break;
                case "NumberIn":
                    if (!string.IsNullOrEmpty(ColumnValue))
                    {
                        strWhere = string.Format(" AND {0} in ({1}) ", ColumnName, ColumnValue);
                    }
                    break;
                default:
                    if (!string.IsNullOrEmpty(ColumnValue))
                    {
                        strWhere = string.Format(" AND {0}='{1}' ", ColumnName, ColumnValue);
                    }
                    break;
            }
            return strWhere;
        }

        /// <summary>
        /// 组装日期的查询条件
        /// </summary>
        /// <param name="ColumnName"></param>
        /// <param name="BeginDateValue"></param>
        /// <param name="EndDateValue"></param>
        /// <returns></returns>
        public static string GetDateWhere(string ColumnName, string BeginDateValue, string EndDateValue)
        {
            string strWhere = "";
            //BeginDateValue = BeginDateValue.Trim();
            //EndDateValue = EndDateValue.Trim();

            if (!string.IsNullOrEmpty(BeginDateValue) && !string.IsNullOrEmpty(EndDateValue))
            {
                strWhere = string.Format(" AND {0} BETWEEN '{1}' AND '{2}' ", ColumnName, BeginDateValue, CConvert.StringToDateTime(EndDateValue, DateTime.Now).AddDays(1).AddSeconds(-1));
            }
            else if (!string.IsNullOrEmpty(BeginDateValue))
            {
                strWhere = string.Format(" AND {0}>='{1}' ", ColumnName, BeginDateValue);
            }
            else if (!string.IsNullOrEmpty(EndDateValue))
            {
                strWhere = string.Format(" AND {0}<='{1}' ", ColumnName, CConvert.StringToDateTime(EndDateValue, DateTime.Now).AddDays(1).AddSeconds(-1));
            }

            return strWhere;
        }
        #endregion


        //public static string GetIdKeyCiphertext(string str)
        //{
        //    // 加上时间
        //    string strReParam = string.Empty;
        //    try
        //    {
        //        strReParam = strText + ":" + DateTime.Now.ToString("yyyyMMddHHmmss");
        //        string strKey = CCommon.GetWebConfigValue("TokenEncryptKey");
        //        strKey = strKey == "" ? "cashchina3321" : strKey;
        //        strReParam = SecurityHelper.DESCommonEncrypt(strReParam, strKey);
        //        strReParam = System.Web.HttpUtility.UrlEncode(strReParam);
        //    }
        //    catch
        //    {
        //        strReParam = "";
        //    }
        //    return strReParam;
        //}        

        //#region 将某一值加上日期后加密返回
        ///// <summary>
        ///// 
        ///// </summary>
        ///// <param name="strText">需要加密的明文</param>
        ///// <param name="typename">分类</param>
        ///// <returns></returns>
        //public static string GetIdKeyCiphertext(string strText,string typename)
        //{
        //    // 加上时间
        //    string strReParam = string.Empty;
        //    try
        //    {
        //        strReParam = strText + ":" + DateTime.Now.ToString("yyyyMMddHHmmss");
        //        strReParam = SecurityHelper.DESCommonEncrypt(strReParam, typename);
        //        strReParam = System.Web.HttpUtility.UrlEncode(strReParam);
        //    }
        //    catch
        //    {
        //        strReParam = "";
        //    }
        //    return strReParam;
        //}
        //#endregion


        #region 解析加密对：获取主键信息及分类信息并检查是否过期，若过期，则返回0，否则返回键值        
        public static string GetDecKey(string strIdKey,out int typeId)
        {
            typeId = 0;
            if (string.IsNullOrEmpty(strIdKey)) return "0";

            //解析token信息            
            string strTokenEncryptKey = CCommon.GetWebConfigValue("TokenEncryptKey");
            strTokenEncryptKey = strTokenEncryptKey == "" ? "cashchina3321" : strTokenEncryptKey;

            strIdKey = SecurityHelper.DESCommonDecrypt(strIdKey, strTokenEncryptKey);
            string[] ArrToken = (strIdKey + ":").Split(':');

            if (ArrToken[0].Trim() == "" || ArrToken[1].Trim() == "")
            {
                return "0";
            }

            //在此需要实现一个超时检测时间，若超过1分钟的token,认为无效 yyyyMMddHHmmss
            if (ArrToken[1].Trim() != "")
            {
                string strDate = ArrToken[1].Trim() + "19990101010101";
                strDate = strDate.Substring(0, 4)
                    + "-" + strDate.Substring(4, 2)
                    + "-" + strDate.Substring(6, 2)
                    + " " + strDate.Substring(8, 2)
                    + ":" + strDate.Substring(10, 2)
                    + ":" + strDate.Substring(12, 2);
                DateTime d = CConvert.StringToDateTime(strDate);
                if (d.AddMinutes(1) < DateTime.Now)
                {
                    return "0";
                }
            }
            if (ArrToken.Length >= 3)
                typeId = CConvert.StringToInt(ArrToken[2]);
            return ArrToken[0];
        }
        #endregion 

        /// <summary>  
        /// 根据GUID获取19位的唯一数字序列  
        /// </summary>  
        /// <returns></returns>  
        public static string GuidToLongID()
        {
            byte[] buffer = Guid.NewGuid().ToByteArray();
            return BitConverter.ToInt64(buffer, 0).AsTargetType<string>("0");
        }

    }
}
