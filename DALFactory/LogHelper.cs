﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 * 日记类
 */
using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Web;
using System.Data;
using System.IO;
using System.Threading;
using System.Runtime.Remoting.Messaging;

namespace XiWan.DALFactory
{
    public class LogHelper
    {
        public delegate void AsyncFactorWriter(ref string primefactor1, ref string primefactor2);
        ManualResetEvent waiter;
        #region 属性
        /// <summary>
        /// 日志目录获取
        /// </summary>
        public static string LogFolderRoot
        {
            get
            {
                string strPath;
                strPath = StringHelper.FilterToDefault(System.Configuration.ConfigurationSettings.AppSettings["LogFolderRoot"], "~/Log/");
                if (strPath.Contains(":"))
                {
                    return strPath;
                }
                else
                {
                    if (System.Web.HttpContext.Current != null)
                    {
                        return System.Web.HttpContext.Current.Server.MapPath(strPath);
                    }
                    else
                    {
                        return System.AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\Log\\";
                    }
                }
            }
        }
        #endregion

        #region 异常错误处理
        /// <summary>
        ///实现功能：写异常错误 
        /// </summary>
        /// <param name="sException">表示在应用程序执行期间发生的错误描述字段</param>
        public static void DoWriteLog(string errSource, string sException)
        {
            string sFilePath = LogFolderRoot + "Err\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";
            try
            {
                WriteErrorText(sFilePath, "错误来源：" + errSource + "；错误信息" + sException);
            }
            catch (System.Exception oErr)
            {
                //return oErr.Message.ToString();
            }
            // return sException;
        }

        public static void DoException(Exception e, string err)
        {
            DoWriteLog(err, e.Message);
        }

        public static void DoException(string errSource, Exception e)
        {
            DoWriteLog(errSource, e.Message);
        }

        public static void DoException(string strError)
        {
            DoWriteLog(" 未知 ", strError);
        }

        public static void DoException(string errSource, string strError)
        {
            DoWriteLog(errSource, strError);
        }

        /// <summary>
        ///实现功能：写错误信息
        ///传递参数说明：sPathFileName 日志文件的路径和文件
        ///sErrInfo 描述当前异常的消息
        ///返回结果说明：无
        /// </summary>
        /// <param name="sPathFileName"></param>
        /// <param name="sErrInfo"></param>
        private static void WriteErrorText(string sPathFileName, string sErrInfo)
        {
            if (CCommon.GetWebConfigValue("IsWriteLog") == "0")
                return;
            IOHelper.CreateNotExistsFile(sPathFileName);
            System.IO.StreamWriter newStreamWriter = System.IO.File.AppendText(sPathFileName);
            newStreamWriter.WriteLine("--------");
            newStreamWriter.WriteLine("异常信息：" + sErrInfo + " ");
            newStreamWriter.WriteLine("发生时间：" + DateTime.Now.ToString() + "　");
            //			newStreamWriter.WriteLine(" 【触发ＩＰ】："+this.Request.Url. .UserHostName.ToString()  + "");
            //newStreamWriter.WriteLine("--------");
            newStreamWriter.Close();
        }
        #endregion

        #region 一般的日志记录
        /// <summary>
        /// 使用异步保存日志存取
        /// </summary>
        /// <param name="strLogText">操作信息</param>
        /// <param name="strTypeName">所要保存的自定义文件夹名称，可空，默认在OperateLog的public里</param>
        public static void DoOperateLog(string strLogText, string strTypeName)
        {
            try
            {
                //waiter = new ManualResetEvent(false);
                AsyncFactorWriter factorDelegate = new AsyncFactorWriter(LogHelper.WriteOperateText);

                if (strTypeName == null || strTypeName == "")
                    strTypeName = "public";
                string sFilePath = LogFolderRoot + "OperateLog\\" + strTypeName + "\\" + DateTime.Now.ToString("yyyyMM") + "\\" + DateTime.Now.ToString("yyyyMMdd") + ".txt";

                IAsyncResult result = factorDelegate.BeginInvoke(
                                  ref sFilePath,
                                  ref strLogText,
                                  null,
                                  null);

            }
            catch (Exception)
            {

                throw;
            }
            //waiter.Set();
            //factorDelegate.EndInvoke(ref sFilePath, ref strLogText, result);

        }
        /// <summary>
        /// 读取josn.txt
        /// </summary>
        /// <param name="queryCredNum">路径</param>
        /// <returns></returns>
        public static string ReadJson(string sFilePath)
        {
            string returnStr = string.Empty;

            //string sFilePath = LogFolderRoot + "OperateLog\\CreditJosnData\\" + queryCredNum + ".txt";
            try
            {

                var file = File.OpenText(sFilePath);
                returnStr = file.ReadToEnd();
                file.Close();

            }
            catch (Exception ex)
            {
                DoWriteLog("ReadJson", ex.Message);
            }

            return returnStr;
        }
        /// <summary>
        /// 写操作信息
        /// </summary>
        /// <param name="sPathFileName"></param>
        /// <param name="strLogText"></param>
        private static void WriteText(ref string sPathFileName, ref string strLogText)
        {
            try
            {
                System.IO.StreamWriter newStreamWriter;
                newStreamWriter = System.IO.File.CreateText(sPathFileName);
                newStreamWriter.Write(strLogText);
                newStreamWriter.Close();
            }
            catch (Exception ex)
            {
                DoWriteLog("WriteText", ex.Message);
            }
        }

        /// <summary>
        /// 使用异步保存日志存取
        /// </summary>
        /// <param name="strLogText">操作信息</param>
        /// <param name="strTypeName">所要保存的自定义文件夹名称，可空，默认在OperateLog的public里</param>
        public static void DoOperateLog2(string strLogText, string strTypeName, string FileName)
        {
            //waiter = new ManualResetEvent(false);
            AsyncFactorWriter factorDelegate = new AsyncFactorWriter(LogHelper.WriteOperateText);

            if (strTypeName == null || strTypeName == "")
                strTypeName = "public";
            string sFilePath = LogFolderRoot + "OperateLog\\" + strTypeName + "\\" + DateTime.Now.ToString("yyyyMMdd") + "\\" + FileName + ".txt";

            IAsyncResult result = factorDelegate.BeginInvoke(
                              ref sFilePath,
                              ref strLogText,
                              null,
                              null);
            //waiter.Set();
            //factorDelegate.EndInvoke(ref sFilePath, ref strLogText, result);

        }

        /// <summary>
        /// 写操作信息
        /// </summary>
        /// <param name="sPathFileName"></param>
        /// <param name="strLogText"></param>
        private static void WriteOperateText(ref string sPathFileName, ref string strLogText)
        {
            try
            {
                if (CCommon.GetWebConfigValue("IsWriteLog") == "0")
                    return;
                IOHelper.CreateNotExistsFile(sPathFileName);
                System.IO.StreamWriter newStreamWriter;
                newStreamWriter = System.IO.File.AppendText(sPathFileName);
                newStreamWriter.WriteLine("---------");
                newStreamWriter.WriteLine("日志内容：" + strLogText);
                newStreamWriter.WriteLine("发生时间：" + DateTime.Now.ToString());
                newStreamWriter.Close();
            }
            catch (Exception ex)
            {
            }
        }
        #endregion

    }
}
