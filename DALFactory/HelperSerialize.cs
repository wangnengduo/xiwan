﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Reflection;
using System.Data;

using System.Xml.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;


namespace XiWan.DALFactory
{
    public class HelperSerialize
    {

        #region 系列化与反系列化
        public string Serialize<T>(T obj)
        {
            IFormatter formatter = new BinaryFormatter();
            MemoryStream stream = new MemoryStream();
            try
            {
                formatter.Serialize(stream, obj);
                stream.Position = 0;
                byte[] buffer = new byte[stream.Length];
                stream.Read(buffer, 0, buffer.Length);
                stream.Flush();
                stream.Close();
                return Convert.ToBase64String(buffer);
            }
            catch (Exception ex)
            {
                throw new Exception("序列化失败,原因:" + ex.Message);
            }
        }

        public T Desrialize<T>(string str)
        {
            T obj = default(T);
            IFormatter formatter = new BinaryFormatter();
            byte[] buffer = Convert.FromBase64String(str);
            MemoryStream stream = new MemoryStream(buffer);
            try
            {
                obj = (T)formatter.Deserialize(stream);
                stream.Flush();
                stream.Close();
            }
            catch (Exception ex)
            {
                throw new Exception("反序列化失败,原因:" + ex.Message);
            }
            return obj;
        }
        #endregion 

        #region 将 XmlDocument 类型数据转换为 String 数据
        /// <summary>
        /// 将 XmlDocument 类型数据转换为 String 数据
        /// </summary>
        /// <param name="xmldoc">xmldocument 参数</param>
        /// <returns>返回xml字符串</returns>
        public static string XmlDocToXmlString(XmlDocument xmldoc)
        {
            try
            {
                if (xmldoc == null) return null;

                return xmldoc.InnerXml;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 将xml格式的string数据转换成xmldocument
        /// <summary>
        /// 将xml格式的string数据转换成xmldocument
        /// </summary>
        /// <param name="xmlstring">xml格式string</param>
        /// <returns>xmldocument</returns>
        public static XmlDocument XmlStringToXmlDoc(string xmlstring)
        {
            try
            {
                // 如果传入的字符为空，返回null
                if (xmlstring == null || xmlstring.Length == 0) return null;

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(xmlstring);

                return xmldoc;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 将xml格式的string串转换为特定类型的实体对象
        /// <summary>
        /// 将xml格式的string串转换为特定类型的实体对象
        /// </summary>
        /// <typeparam name="T">泛型类</typeparam>
        /// <param name="xmlstring">xml格式的string串</param>
        /// <returns>返回泛型类型的实体对象</returns>
        public static T XmlStringToObject<T>(string xmlstring) where T : class
        {
            try
            {
                // 如果传入的字符为空，返回null
                if (xmlstring == null || xmlstring.Length == 0) return null;

                MemoryStream ms;
                ms = new MemoryStream(System.Text.Encoding.GetEncoding("GB2312").GetBytes(xmlstring));

                XmlSerializer xs = new XmlSerializer(typeof(T));
                T t = (T)xs.Deserialize(ms);

                ms.Close();

                return t;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 反序列化
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Object UnSerialize(string str)
        {
            try
            {
                //反序列化
                IFormatter formatter = new BinaryFormatter();


                byte[] b = Convert.FromBase64String(str);

                MemoryStream ms = new MemoryStream(b);

                Object obj = formatter.Deserialize(ms);

                return obj;
            }
            catch
            {
                throw new Exception("对象反序列化失败");
            }
        }
        #endregion

        #region  将xml格式文档转换特定类型的实体对象
        /// <summary>
        /// 将xml格式文档转换特定类型的实体对象
        /// </summary>
        /// <typeparam name="T">泛型类型</typeparam>
        /// <param name="xmldoc"></param>
        /// <returns>返回泛型类型的实体对象</returns>
        public static T XmlDocToObject<T>(XmlDocument xmldoc) where T : class
        {
            try
            {
                // 先调用xml to string方法，再调用 string to object方法
                return XmlStringToObject<T>(XmlDocToXmlString(xmldoc));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region 将特定实体对象序列化成xml格式的字符串
        /// <summary>
        /// 将特定实体对象序列化成xml格式的字符串
        /// </summary>
        /// <typeparam name="T">泛型类型</typeparam>
        /// <param name="t">实体对象</param>
        /// <returns>返回xml格式的string</returns>
        public static string ObjectToXmlString<T>(T t) where T : class
        {
            try
            {
                if (t == null) return null;

                MemoryStream ms = new MemoryStream();
                XmlSerializer xs = new XmlSerializer(typeof(T));
                xs.Serialize(ms, t);

                // 内存流转成字符串
                StringBuilder sb = new StringBuilder();
                foreach (byte b in ms.ToArray())
                {
                    sb.Append((char)b);
                }

                ms.Close();

                return sb.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 将对象序例化
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string Serialize(Object obj)
        {
            string ret = "";

            try
            {
                //序列化
                IFormatter formatter = new BinaryFormatter();
                MemoryStream ms = new MemoryStream();
                formatter.Serialize(ms, obj);

                byte[] byte_obj = ms.GetBuffer();


                ret = Convert.ToBase64String(byte_obj);
            }
            catch
            {
                throw new Exception("对象序列化失败");
            }

            return ret;
        }

        #endregion

        #region 将特定实体对象序列化成xml格式的文档
        /// <summary>
        /// 将特定实体对象序列化成xml格式的文档
        /// </summary>
        /// <typeparam name="T">泛型类型</typeparam>
        /// <param name="t">实体对象</param>
        /// <returns>返回xml格式的文档</returns>
        public static XmlDocument ObjectToXmlDoc<T>(T t) where T : class
        {
            try
            {
                return XmlStringToXmlDoc(ObjectToXmlString<T>(t));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region XML序列化与反序列化
        /// <summary>
        /// 将类序列化成XML
        /// </summary>
        /// <param name="obj">要序列化成XML的对像</param>
        /// <returns>XML</returns>
        public static string Model2Xml(object obj)
        {
            string str = null;
            XmlSerializer serializer = new XmlSerializer(obj.GetType());
            using (TextWriter writer = new StringWriter())
            {
                serializer.Serialize(writer, obj);
                str = writer.ToString();
                writer.Close();
            }
            return str;
        }
        /// <summary>
        /// 根据XML反序列成实体类
        /// </summary>
        /// <param name="xml">XML字符串</param>
        /// <param name="t">要反序列化的对象类型</param>
        /// <returns>对象</returns>
        public static object GetModelByXml(string xml, Type t)
        {
            object obj2 = null;
            if (!string.IsNullOrEmpty(xml) && (t != null))
            {
                if (t == null)
                {
                    return obj2;
                }
                obj2 = Activator.CreateInstance(t);
                XmlSerializer serializer = new XmlSerializer(t);
                try
                {
                    using (TextReader reader = new StringReader(xml))
                    {
                        obj2 = serializer.Deserialize(reader);
                        reader.Close();
                    }
                }
                catch (Exception)
                {
                }
            }
            return obj2;
        }
        #endregion

        #region DataTable转换成对像
        /// <summary>
        /// DataSet装换为泛型集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="p_DataSet">DataSet</param>
        /// <param name="p_TableIndex">待转换数据表索引</param>
        /// <returns></returns>
        public static List<T> DataTableToObject<T>(DataTable dtSource) where T : class
        {
            if (dtSource == null) return null;

            // 返回值初始化
            List<T> result = null;

            for (int j = 0; j < dtSource.Rows.Count; j++)
            {
                T _t = (T)Activator.CreateInstance(typeof(T));
                PropertyInfo[] propertys = _t.GetType().GetProperties();

                foreach (PropertyInfo pi in propertys)
                {
                    if (dtSource.Columns.Contains(pi.Name))
                    {
                        // 数据库NULL值单独处理
                        if (dtSource.Rows[j][pi.Name] != DBNull.Value)
                            pi.SetValue(_t, dtSource.Rows[j][pi.Name], null);
                        else
                            pi.SetValue(_t, null, null);
                    }
                    /*
                    for (int i = 0; i < dtSource.Columns.Count; i++)
                    {
                        // 属性与字段名称一致的进行赋值
                        if (pi.Name.Equals(dtSource.Columns[i].ColumnName))
                        {
                            // 数据库NULL值单独处理
                            if (dtSource.Rows[j][i] != DBNull.Value)
                                pi.SetValue(_t, dtSource.Rows[j][i], null);
                            else
                                pi.SetValue(_t, null, null);
                            break;
                        }
                    }
                    */
                }
                result.Add(_t);
            }
            return result;
        }
        #endregion

        #region 表对象转换成相应插入或更新语句
        /// <summary>
        /// 由对象直接转换成该对象表的插入Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <returns>插入Sql语句</returns>
        public static string ObjectToInsertSql<T>(T EntityObj, string ColumnNames) where T : class
        {
            if (EntityObj == null) return "";

            string strSql = "";
            string strSqlS = "";
            string strSqlE = "";

            string[] ArrC = (ColumnNames + "|").Split('|');
            T _t = (T)Activator.CreateInstance(typeof(T));
            PropertyInfo[] propertys = _t.GetType().GetProperties();
            string strPName = "";
            foreach (PropertyInfo pi in propertys)
            {
                if (ArrC.Length > 2 && !ArrC.Contains(pi.Name))  //非指定更新字段，则跳过
                {
                    continue;
                }
                strPName = pi.PropertyType.FullName;
                strSqlS += pi.Name + ","; //对象属性转成字段名
                if (strPName == "System.Int16"
                    || strPName == "System.Int32"
                    || strPName == "System.Int64"
                    || strPName == "System.Decimal"
                    || strPName == "System.Double"
                    || strPName == "System.Boolean")
                {
                    strSqlE += pi.GetValue(EntityObj, null) + ",";//对象属性转成字段名
                }
                else
                {
                    strSqlE += "'" + pi.GetValue(EntityObj, null) + "',"; //对象属性值转成字段值
                }
            }
            strSql = string.Format(" INSERT INTO {0}(1) VALUES({2})", _t.GetType().Name, strSqlS.TrimEnd(','), strSqlE.TrimEnd(','));
            return strSql;
        }


        /// <summary>
        /// 由对象直接转换成该对象表的更新Sql语句
        /// </summary>
        /// <typeparam name="T">表对象类名</typeparam>
        /// <param name="EntityObj">表对象</param>
        /// <param name="ColumnNames">更新的字段，多个用“|”隔开，空则全部字段</param>
        /// <returns>更新语句</returns>
        public static string ObjectToUpdateSql<T>(T EntityObj, string ColumnNames) where T : class
        {
            if (EntityObj == null) return "";

            string strSql = "";
            string strSqlS = "";
            string strSqlF = "";

            string[] ArrC = (ColumnNames + "|").Split('|');
            T _t = (T)Activator.CreateInstance(typeof(T));
            PropertyInfo[] propertys = _t.GetType().GetProperties();
            string strPName = "";

            foreach (PropertyInfo pi in propertys)
            {
                if (ArrC.Length > 2 && !ArrC.Contains(pi.Name))  //非指定更新字段，则跳过
                {
                    continue;
                }

                strPName = pi.PropertyType.FullName;
                if (strPName == "System.Int16"
                    || strPName == "System.Int32"
                    || strPName == "System.Int64"
                    || strPName == "System.Decimal"
                    || strPName == "System.Double"
                    || strPName == "System.Boolean")
                {
                    strSqlF = "{0}={1},";
                }
                else
                {
                    strSqlF = "{0}='{1}',";
                }
                strSqlS += string.Format(strSqlF, pi.Name, pi.GetValue(EntityObj, null));//对象属性转成字段名
            }

            strSql = string.Format(" UPDATE {0} SET {1}", _t.GetType().Name, strSqlS.TrimEnd(','));
            return strSql;
        }
        #endregion 
        
        #region Xml方法..

        #region 获取xml结点名字串..

        public static string GetNodeNameList(XElement xmlNode)
        {
            return GetNodeNameList(xmlNode, "|");
        }

        /// <summary>
        /// 只能是二级结点的xml结点
        /// </summary>
        /// <param name="xmlNode"></param>
        /// <param name="strSplit"></param>
        /// <returns></returns>
        public static string GetNodeNameList(XElement xmlNode, string strSplit)
        {
            try
            {
                if (xmlNode == null) return "";

                StringBuilder sb = new StringBuilder();

                foreach (XElement item in xmlNode.Elements())
                {
                    if (sb.Length > 0) sb.Append(strSplit);
                    sb.Append(item.Name);
                }

                return sb.ToString();
            }
            catch
            {
                return "";
            }
        }

        #endregion

        /// <summary>
        /// 通过结点名和值获取结点信息
        /// </summary>
        /// <param name="XName"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static XElement GetXElement(string XName, object value)
        {
            try
            {
                return new XElement(XName.Trim(), value);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 将xml字符串
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>
        public static XElement GetXElementByString(string xml)
        {
            try
            {
                // 把xml字符串转换成结点
                Stream stream = new MemoryStream(Encoding.UTF8.GetBytes(xml));
                XmlReader reader = XmlReader.Create(stream);
                XElement xmlroot = XElement.Load(reader);

                return xmlroot;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static XElement GetXElementByFile(string path)
        {
            try
            {
                if (!File.Exists(path)) return null;

                // 把xml字符串转换成结点
                Stream stream = new FileStream(path, FileMode.Open);
                XmlReader reader = XmlReader.Create(stream);
                XElement xml = XElement.Load(reader);

                return xml;
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 通过DataTable生成xml结点
        /// </summary>
        /// <param name="dt">datatable生成xml结点</param>
        /// <param name="rowName">行的名称</param>
        /// <returns></returns>
        public static XElement GetXElementByDataTable(DataTable dt, string rowName)
        {
            try
            {
                return GetXElementByDataTable(dt, rowName, "Root");
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 通过DataTable生成xml结点
        /// </summary>
        /// <param name="dt">datatable生成xml结点</param>
        /// <param name="rowName">行的名称</param>
        /// <returns></returns>
        public static XElement GetXElementByDataTable(DataTable dt, string rowName, string rootName)
        {
            try
            {
                if (dt == null) return null;
                if (dt.Rows.Count == 0) return null;

                if (rowName.Trim().Length == 0) rowName = "DataRow";
                if (rootName.Trim().Length == 0) rootName = "Root";

                XElement xmlRoot = new XElement(rootName);
                XElement xmlRow;
                foreach (DataRow row in dt.Rows)
                {
                    xmlRow = new XElement(rowName);

                    foreach (DataColumn col in dt.Columns)
                    {
                        xmlRow.Add(new XElement(col.ColumnName, row[col.ColumnName].ToString()));
                    }

                    xmlRoot.Add(xmlRow);
                }

                return xmlRoot;
            }
            catch
            {
                return null;
            }
        }

        public static string SetXElementToFile(string path, XElement xmlNode)
        {
            try
            {
                if (!File.Exists(path)) return null;

                // 把xml字符串转换成结点
                StreamWriter sw = File.CreateText(path);
                sw.Write(xmlNode.ToString());

                sw.Close();

                return "";
            }
            catch (Exception ex)
            {
                return "99|" + ex.Message;
            }
        }

        #region 加上编码头
        public static string GetXmlString(XElement xmlNode)
        {
            try
            {
                if (xmlNode == null) return "";

                return "<?xml version=\"1.0\" encoding=\"GB2312\" ?> " + xmlNode.ToString();
            }
            catch
            {
                return "";
            }
        }

        /// <summary>
        /// 增加编码头
        /// </summary>
        /// <param name="xml"></param>
        /// <param name="encoding"></param>
        /// <returns></returns>
        public static string GetXmlString(string xml, Encoding encoding)
        {
            try
            {
                return "<?xml version=\"1.0\" encoding=\"" + encoding.HeaderName + "\" ?> " + xml;
            }
            catch
            {
                return "";
            }
        }
        #endregion

        #region XML串的生成..

        /// <summary>
        /// 返回实体对应列的xml串
        /// </summary>
        /// <param name="strFieldList">变更列名，列名为空，保存所有的</param>
        /// <returns>返回xml结点</returns>
        public static XElement GetXml<T>(T t, string NodeName, string[] strFields) where T : class
        {
            try
            {
                return GetXml<T>(t, NodeName, strFields, false);
            }
            catch
            {
                return null;
            }
        }

        /// <summary>
        /// 获取实体对应列的xml，实体列与xml结点列可不同
        /// </summary>
        /// <typeparam name="T">实体类型</typeparam>
        /// <param name="t">实体实例</param>
        /// <param name="NodeName">结点名</param>
        /// <param name="strFields">实体列</param>
        /// <param name="isReplace">实体列与xml结点是否一致；isReplace为true时，strFidlds保存列表和结点名，用“,”分隔开，如"FieldName,NodeName"</param>
        /// <returns></returns>
        public static XElement GetXml<T>(T t, string NodeName, string[] strFields, bool isReplace) where T : class
        {
            try
            {
                if (t == null) return null;

                // 只有一个并为空时
                if (strFields.Length == 0 || (strFields.Length == 1 && strFields[0].Trim().Length == 0))
                {
                    // 保存所有资料
                    strFields = GetTableFieldList(t.GetType()).Split('|');
                }

                XElement xmlInfo = new XElement(NodeName);

                if (!isReplace)
                {
                    for (int i = 0; i < strFields.Length; i++)
                    {
                        if (strFields[i].Trim().Length == 0) continue;

                        object obj = GetValue<T>(t, strFields[i]);

                        xmlInfo.Add(new XElement(strFields[i], obj == null ? "" : obj.ToString()));
                    }
                }
                else
                {
                    for (int i = 0; i < strFields.Length; i++)
                    {
                        if (strFields[i].Trim().Length == 0) continue;

                        object obj = GetValue<T>(t, strFields[i].Trim().Split(',')[0]);
                        if (strFields[i].Trim().Split(',').Length == 1)
                        { xmlInfo.Add(new XElement(strFields[i], obj == null ? "" : obj.ToString())); }
                        else
                        { xmlInfo.Add(new XElement(strFields[i].Trim().Split(',')[1], obj == null ? "" : obj.ToString())); }
                    }
                }

                return xmlInfo;
            }
            catch
            {
                return null;
            }
        }

        public static XElement GetXml(DataTable dt, string NodeName, string[] strFields, bool isReplace)
        {
            try
            {
                if (dt == null) return null;

                // 只有一个并为空时
                if (strFields.Length == 0 || (strFields.Length == 1 && strFields[0].Trim().Length == 0))
                {
                    StringBuilder sb = new StringBuilder();

                    foreach (DataColumn column in dt.Columns)
                    {
                        if (sb.Length > 0) sb.Append("|");
                        sb.Append(column.ColumnName);
                    }

                    strFields = sb.ToString().Split('|');
                }

                XElement xmlRoot = new XElement("Root");

                XElement xmlNode;
                foreach (DataRow dr in dt.Rows)
                {
                    xmlNode = GetXml(dr, NodeName, strFields, isReplace);

                    if (xmlNode != null) xmlRoot.Add(xmlNode);
                }

                return xmlRoot;
            }
            catch
            {
                return null;
            }
        }

        public static XElement GetXml(DataRow dr, string NodeName, string[] strFields, bool isReplace)
        {
            try
            {
                if (dr == null) return null;

                // 只有一个并为空时
                if (strFields.Length == 0 || (strFields.Length == 1 && strFields[0].Trim().Length == 0))
                { return null; }

                XElement xmlNode = new XElement(NodeName);

                if (!isReplace)
                {
                    for (int i = 0; i < strFields.Length; i++)
                    {
                        if (strFields[i].Trim().Length == 0) continue;

                        object obj = dr[strFields[i]];

                        xmlNode.Add(new XElement(strFields[i], obj == null ? "" : obj.ToString()));
                    }
                }
                else
                {
                    for (int i = 0; i < strFields.Length; i++)
                    {
                        if (strFields[i].Trim().Length == 0) continue;

                        object obj = dr[strFields[i]];
                        if (strFields[i].Trim().Split(',').Length == 1)
                        { xmlNode.Add(new XElement(strFields[i], obj == null ? "" : obj.ToString())); }
                        else
                        { xmlNode.Add(new XElement(strFields[i].Trim().Split(',')[1], obj == null ? "" : obj.ToString())); }
                    }
                }

                return xmlNode;
            }
            catch
            {
                return null;
            }
        }

        public static XElement GetXml(DataRow[] drs, string NodeName, string[] strFields, bool isReplace)
        {
            try
            {
                if (drs == null) return null;


                XElement xmlRoot = new XElement("Root");

                XElement xmlNode;
                foreach (DataRow dr in drs)
                {
                    xmlNode = GetXml(dr, NodeName, strFields, isReplace);

                    if (xmlNode != null) xmlRoot.Add(xmlNode);
                }

                return xmlRoot;
            }
            catch
            {
                return null;
            }
        }

        #endregion

        #region Data Linq Method ..

        /// <summary>
        /// 通过属性名，获得实例中对应属性名的值
        /// </summary>
        /// <typeparam name="T">实例类型，必须为类</typeparam>
        /// <param name="t">实例</param>
        /// <param name="fieldName">属性名</param>
        /// <returns></returns>
        public static object GetValue<T>(T t, string fieldName) where T : class
        {
            try
            {
                if (fieldName.Trim().Length < 1) return string.Empty;

                Type type = t.GetType();

                fieldName = fieldName.Trim().ToLower();

                foreach (PropertyInfo pi in type.GetProperties())
                {
                    if (pi.Name.Trim().ToLower() != fieldName)
                    { continue; }

                    return pi.GetValue(t, null);
                }

                // 没有对应属性
                return null;
            }
            catch
            { return null; }
        }

        /// <summary>
        /// 通过属性名，获得ORM实例中对应属性名的值
        /// </summary>
        /// <typeparam name="T">实例类型，必须为类</typeparam>
        /// <param name="t">实例</param>
        /// <param name="fieldName">属性名</param>
        /// <returns></returns>
        public static object GetTableValue<T>(T t, string fieldName) where T : class
        {
            try
            {
                if (fieldName.Trim().Length < 1) return string.Empty;

                Type type = t.GetType();

                fieldName = fieldName.Trim().ToLower();
                foreach (PropertyInfo pi in type.GetProperties())
                {
                    if (pi.Name.Trim().ToLower() != fieldName)
                    { continue; }

                    object[] attrs = pi.GetCustomAttributes(false);
                    foreach (object attr in attrs)
                    {
                        if (attr is System.Data.Linq.Mapping.ColumnAttribute)
                        {
                            System.Data.Linq.Mapping.ColumnAttribute column = (System.Data.Linq.Mapping.ColumnAttribute)attr;

                            if (column.Storage.Trim().ToLower() == "_" + fieldName)
                            { return pi.GetValue(t, null); }
                        }
                    }
                }

                // 没有任何条件
                return null;
            }
            catch
            { return null; }
        }

        public static string GetTableFieldList(Type t)
        {
            return GetTableFieldList(t, new string[0]);
        }

        /// <summary>
        /// 获取一个对象的属性集合'|'分隔
        /// </summary>
        /// <param name="t"></param>
        /// <param name="AddField">尾部追加列名</param>
        /// <returns></returns>
        public static string GetTableFieldList(Type t, string[] AddField)
        {
            try
            {
                if (t == null) return string.Empty;

                StringBuilder sb = new StringBuilder();

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    object[] attrs = pi.GetCustomAttributes(false);
                    foreach (object attr in attrs)
                    {
                        if (attr is System.Data.Linq.Mapping.ColumnAttribute)
                        {
                            if (sb.Length > 0)
                            { sb.Append("|"); }

                            // 追加属性名
                            sb.Append(pi.Name);
                        }
                    }
                }
                foreach (string Item in AddField)
                {
                    // 追加属性名
                    sb.Append("|" + Item);
                }

                return sb.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// 获取一个对象的属性集合'|'分隔(Ibatis对象专用)
        /// </summary>
        /// <param name="t"></param>
        /// <returns></returns>
        public static string GetObjectPropertyList(Type t)
        {
            return GetObjectPropertyList(t, new string[0]);
        }
        /// <summary>
        /// 获取一个对象的属性集合'|'分隔(Ibatis对象专用)
        /// </summary>
        /// <param name="t"></param>
        /// <param name="AddField"></param>
        /// <returns></returns>
        public static string GetObjectPropertyList(Type t, string[] AddField)
        {
            try
            {
                if (t == null) return string.Empty;

                StringBuilder sb = new StringBuilder();

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    if (sb.Length > 0)
                    { sb.Append("|"); }

                    // 追加属性名
                    sb.Append(pi.Name);
                }
                foreach (string Item in AddField)
                {
                    // 追加属性名
                    sb.Append("|" + Item);
                }

                return sb.ToString();
            }
            catch
            {
                return string.Empty;
            }
        }

        #endregion


        #endregion

        #region Clone..

        /// <summary>
        /// 返回clone的实体给调用方
        /// </summary>
        /// <typeparam name="T">泛型类</typeparam>
        /// <param name="obj">T类型</param>
        /// <returns>返回T类型的新实体</returns>        
        public static T Clone<T>(T obj) where T : class
        {
            try
            {
                Type t = obj.GetType();
                T newObj = (T)Activator.CreateInstance(t);

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        pi.SetValue(newObj, pi.GetValue(obj, null), null);
                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        fi.SetValue(newObj, fi.GetValue(obj));
                    }
                    catch { }
                }

                return newObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        /// <summary>
        /// 返回clone的实体给调用方
        /// </summary>
        /// <typeparam name="T">泛型类</typeparam>
        /// <param name="obj">T类型</param>
        /// <returns>返回T类型的新实体</returns>        
        public static T CloneValue<T>(T obj) where T : class
        {
            try
            {
                Type t = obj.GetType();
                T newObj = (T)Activator.CreateInstance(t);

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        if (pi.PropertyType.ToString().IndexOf("EntitySet") >= 0)
                        {
                            continue; //不copy集合
                        }
                        pi.SetValue(newObj, pi.GetValue(obj, null), null);
                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        fi.SetValue(newObj, fi.GetValue(obj));
                    }
                    catch { }
                }

                return newObj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public static void Clone<T>(T source, T target) where T : class
        {
            try
            {
                Type t = source.GetType();

                if (target == null)
                { return; }

                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        pi.SetValue(target, pi.GetValue(source, null), null);
                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        fi.SetValue(target, fi.GetValue(source));
                    }
                    catch { }
                }

                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static void Clone<T>(T source, T target, string[] propertyNames) where T : class
        {
            try
            {
                Type t = source.GetType();

                if (target == null)
                { return; }


                foreach (PropertyInfo pi in t.GetProperties())
                {
                    try
                    {
                        //if (propertyNames.Select(o => o.Trim().ToLower() == pi.Name.Trim().ToLower()).Count() > 0)                        
                        if (propertyNames.Contains(pi.Name.Trim()))
                        {
                            pi.SetValue(target, pi.GetValue(source, null), null);
                        }

                    }
                    catch { }
                }
                foreach (FieldInfo fi in t.GetFields())
                {
                    try
                    {
                        //if (propertyNames.Select(o => o.Trim().ToLower() == fi.Name.Trim().ToLower()).Count() > 0)
                        if (propertyNames.Contains(fi.Name.Trim()))
                        {
                            fi.SetValue(target, fi.GetValue(source));
                        }
                    }
                    catch { }
                }

                return;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 两个不同类型的对象数据复制
        /// </summary>
        /// <typeparam name="TSource">数据对象类</typeparam>
        /// <typeparam name="TTarget">要赋值的对象类</typeparam>
        /// <param name="source">数据对象</param>
        /// <returns></returns>
        public static TTarget Clone<TSource, TTarget>(TSource source)
            where TSource : class
            where TTarget : class
        {
            if (source == null)
            {
                return null;
            }

            try
            {
                Type tSource = source.GetType();
                Type tTarget = typeof(TTarget);

                // 催化剂实例对象
                TTarget target = (TTarget)Activator.CreateInstance(tTarget);

                PropertyInfo sProperty;
                foreach (PropertyInfo pi in tTarget.GetProperties())
                {
                    try
                    {
                        sProperty = tSource.GetProperty(pi.Name);

                        pi.SetValue(target, sProperty.GetValue(source, null), null);
                    }
                    catch { }
                }
                FieldInfo sfield;
                foreach (FieldInfo fi in tTarget.GetFields())
                {
                    try
                    {
                        sfield = tSource.GetField(fi.Name);

                        fi.SetValue(target, sfield.GetValue(source));
                    }
                    catch { }
                }
                return target;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 两个不同类型同属性的对象数组复制
        /// </summary>
        /// <param name="arrSourceObj">数据源数组对象</param>
        /// <returns>返回的指定类型的数组对象</returns>
        public static TTarget[] CloneArrayObject<TSource, TTarget>(TSource[] arrSourceObj)
            where TSource : class
            where TTarget : class
        {
            if (arrSourceObj == null)
            {
                return null;
            }
            //复制返回的数组TTarget Clone<TSource, TTarget>(TSource source) where TSource : class where TTarget :class
            List<TTarget> iList = new List<TTarget>();

            for (int i = 0; i < arrSourceObj.Length; i++)
            {
                TTarget newObj;
                newObj = Clone<TSource, TTarget>(arrSourceObj[i]);
                iList.Add(newObj);
            }

            return iList.ToArray<TTarget>();
        }

        #endregion
        
        #region xml/实体操作（替换）..

        /// <summary>
        /// 用xml结点的内容替换实例，返回当前实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlNode"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static void Replace<T>(ref T obj, XElement xmlNode) where T : class
        {
            if (obj == null) return;

            Type type = obj.GetType();

            XElement item;
            foreach (PropertyInfo pi in type.GetProperties())
            {
                try
                {
                    // 是否有对应属性的值，有就赋值
                    item = xmlNode.Element(pi.Name);
                    if (item != null)
                    {
                        pi.SetValue(obj, (object)ConvertByType(pi.PropertyType, item.Value), null);
                    }
                }
                catch { }
            }
            foreach (FieldInfo fi in type.GetFields())
            {
                try
                {
                    item = xmlNode.Element(fi.Name);
                    if (item != null)
                    {
                        fi.SetValue(obj, (object)ConvertByType(fi.FieldType, item.Value));
                    }
                }
                catch { }
            }

            return;
        }

        /// <summary>
        /// 用xml结点的内容替换实例
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="xmlNode"></param>
        /// <param name="t"></param>
        /// <returns></returns>
        public static T Replace<T>(T oldObj, XElement xmlNode) where T : class
        {
            if (oldObj == null) return null;

            Type type = oldObj.GetType();

            // 可对所有属性做复制
            T newObj = Clone<T>(oldObj);

            XElement item;
            foreach (PropertyInfo pi in type.GetProperties())
            {
                try
                {
                    // 是否有对应属性的值，有就赋值
                    item = xmlNode.Element(pi.Name);
                    if (item != null)
                    {
                        pi.SetValue(newObj, (object)ConvertByType(pi.PropertyType, item.Value), null);
                    }
                }
                catch { }
            }
            foreach (FieldInfo fi in type.GetFields())
            {
                try
                {
                    item = xmlNode.Element(fi.Name);
                    if (item != null)
                    {
                        fi.SetValue(newObj, (object)ConvertByType(fi.FieldType, item.Value));
                    }
                }
                catch { }
            }

            return newObj;
        }

        public static T Replace<T>(T oldObj, XElement xmlNode, string[] fileds) where T : class
        {
            if (oldObj == null) return null;

            Type type = oldObj.GetType();

            // 可对所有属性做复制
            T newObj = oldObj;

            XElement item;
            PropertyInfo pi;
            FieldInfo fi;
            for (int i = 0; i < fileds.Length; i++)
            {
                try
                {
                    item = xmlNode.Element(fileds[i]);
                    if (item != null)
                    {
                        pi = type.GetProperty(fileds[i]);
                        if (pi != null)
                        {
                            pi.SetValue(newObj, (object)ConvertByType(pi.PropertyType, item.Value), null);
                        }
                        else
                        {
                            fi = type.GetField(fileds[i]);
                            if (fi == null) continue;

                            fi.SetValue(newObj, (object)ConvertByType(fi.FieldType, item.Value));
                        }
                    }

                }
                catch { }
            }

            return newObj;
        }


        #region Convert By Type..

        public static object ConvertByType(Type t, string value)
        {
            try
            {
                if (value == null) return null;

                if (t.FullName == typeof(String).FullName)
                { return value; }
                if (t.FullName == typeof(Boolean).FullName || t == typeof(Nullable<Boolean>))
                { return Boolean.Parse(value); }
                if (t.FullName == typeof(Byte).FullName || t == typeof(Nullable<Byte>))
                { return Byte.Parse(value); }
                if (t.FullName == typeof(SByte).FullName || t == typeof(Nullable<SByte>))
                { return SByte.Parse(value); }
                if (t.FullName == typeof(Decimal).FullName || t == typeof(Nullable<Decimal>))
                { return Decimal.Parse(value); }
                if (t.FullName == typeof(Single).FullName || t == typeof(Nullable<Single>))
                { return Single.Parse(value); }
                if (t.FullName == typeof(Double).FullName || t == typeof(Nullable<Double>))
                { return Double.Parse(value); }
                if (t.FullName == typeof(Int16).FullName || t == typeof(Nullable<Int16>))
                { return Int16.Parse(value); }
                if (t.FullName == typeof(UInt16).FullName || t == typeof(Nullable<UInt16>))
                { return UInt16.Parse(value); }
                if (t.FullName == typeof(int).FullName || t == typeof(Nullable<int>))
                { return int.Parse(value); }
                if (t.FullName == typeof(Int32).FullName || t == typeof(Nullable<Int32>))
                { return Int32.Parse(value); }
                if (t.FullName == typeof(UInt32).FullName || t == typeof(Nullable<UInt32>))
                { return UInt32.Parse(value); }
                if (t.FullName == typeof(Int64).FullName || t == typeof(Nullable<Int64>))
                { return Int64.Parse(value); }
                if (t.FullName == typeof(UInt64).FullName || t == typeof(Nullable<UInt64>))
                { return UInt64.Parse(value); }
                if (t.FullName == typeof(DateTime).FullName || t == typeof(Nullable<DateTime>))
                { return DateTime.Parse(value); }

                return null;
            }
            catch
            {
                return null;
            }
        }

        #endregion
        #endregion

        #region Xml转DataSet
        /// <summary>
        /// 将xml对象内容字符串转换为DataSet
        /// </summary>
        /// <param name="xmlData">xml数据集合</param>
        /// <returns>转换成功返回DataSet，失败返回new DataSet</returns>
        public static DataSet ConvertXMLToDataSet(string xmlData)
        {
            StringReader stream = null;
            XmlTextReader reader = null;
            try
            {
                DataSet xmlDS = new DataSet();
                stream = new StringReader(xmlData);
                //从stream装载到XmlTextReader
                reader = new XmlTextReader(stream);
                xmlDS.ReadXml(reader);
                return xmlDS;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// 将xml文件转换为DataSet
        /// </summary>
        /// <param name="xmlFile">xml文件路径</param>
        /// <returns>转换成功返回DataSet,失败返回new DataSet</returns>
        public static DataSet ConvertXMLFileToDataSet(string xmlFile)
        {
            StringReader stream = null;
            XmlTextReader reader = null;
            try
            {
                XmlDocument xmld = new XmlDocument();
                xmld.Load(xmlFile);

                DataSet xmlDS = new DataSet();
                stream = new StringReader(xmld.InnerXml);
                //从stream装载到XmlTextReader
                reader = new XmlTextReader(stream);
                xmlDS.ReadXml(reader);
                //xmlDS.ReadXml(xmlFile);
                return xmlDS;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        /// <summary>
        /// 读取XML文件档案类别
        /// </summary>
        /// <param name="xmlFile">xml文件路径</param>
        /// <param name="xPath">XPath搜索路径</param>
        /// <returns>转换成功返回XmlNodeList,失败返回new XmlNodeList</returns>
        public static XmlNodeList ConvertXMLFileToNodeList(string xmlFile, string xPath)
        {
            try
            {
                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.Load(xmlFile);
                XmlNodeList nodeList = xmlDoc.SelectNodes(xPath);

                return nodeList;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// 将DataSet转换为xml对象字符串
        /// </summary>
        /// <param name="xmlDS">要转换的DataSet</param>
        /// <returns>转换后的string</returns>
        public static string ConvertDataSetToXML(DataSet xmlDS)
        {
            MemoryStream stream = null;
            XmlTextWriter writer = null;

            try
            {
                stream = new MemoryStream();
                //从stream装载到XmlTextReader
                writer = new XmlTextWriter(stream, Encoding.Unicode);

                //用WriteXml方法写入文件.
                xmlDS.WriteXml(writer);
                int count = (int)stream.Length;
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arr, 0, count);

                UnicodeEncoding utf = new UnicodeEncoding();
                return utf.GetString(arr).Trim();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        /// <summary>
        /// 将DataSet转换为xml文件
        /// </summary>
        /// <param name="xmlDS">传入的DataSet集合</param>
        /// <param name="xmlFile">要写入的xml文件路径</param>
        public static void ConvertDataSetToXMLFile(DataSet xmlDS, string xmlFile)
        {
            MemoryStream stream = null;
            XmlTextWriter writer = null;

            try
            {
                stream = new MemoryStream();
                //从stream装载到XmlTextReader
                writer = new XmlTextWriter(stream, Encoding.Unicode);

                //用WriteXml方法写入文件.
                xmlDS.WriteXml(writer);
                int count = (int)stream.Length;
                byte[] arr = new byte[count];
                stream.Seek(0, SeekOrigin.Begin);
                stream.Read(arr, 0, count);

                //返回Unicode编码的文本
                UnicodeEncoding utf = new UnicodeEncoding();
                StreamWriter sw = new StreamWriter(xmlFile);
                sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\"?>");
                sw.WriteLine(utf.GetString(arr).Trim());
                sw.Close();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }
        #endregion 

    }

    public static class Extensions
    {
        #region DateTime操作..

        public static long DateDiff(this DateTime dt, DateInterval Interval, System.DateTime StartDate)
        {
            try
            {
                long lngDateDiffValue = 0;
                System.TimeSpan TS = new System.TimeSpan(dt.Ticks - StartDate.Ticks);
                switch (Interval)
                {
                    case DateInterval.Millisecond:
                        lngDateDiffValue = (long)TS.TotalMilliseconds;
                        break;
                    case DateInterval.Second:
                        lngDateDiffValue = (long)TS.TotalSeconds;
                        break;
                    case DateInterval.Minute:
                        lngDateDiffValue = (long)TS.TotalMinutes;
                        break;
                    case DateInterval.Hour:
                        lngDateDiffValue = (long)TS.TotalHours;
                        break;
                    case DateInterval.Day:
                        lngDateDiffValue = (long)TS.Days;
                        break;
                    case DateInterval.Week:
                        lngDateDiffValue = (long)(TS.Days / 7);
                        break;
                    case DateInterval.Month:
                        lngDateDiffValue = (long)(TS.Days / 30);
                        if (StartDate.AddMonths(CConvert.ObjectToInt(lngDateDiffValue)) > dt) //校正
                        {
                            lngDateDiffValue -= 1;
                        }
                        else if (TS.Days % 30 > 27)
                        {
                            lngDateDiffValue += 1;
                        }
                        break;
                    case DateInterval.Quarter:
                        lngDateDiffValue = (long)((TS.Days / 30) / 3);
                        break;
                    case DateInterval.Year:
                        lngDateDiffValue = (long)(TS.Days / 365);
                        break;
                }
                return (lngDateDiffValue);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion          
    }

    public enum DateInterval
    {
        Millisecond, Second, Minute, Hour, Day, Week, Month, Quarter, Year
    }
}
