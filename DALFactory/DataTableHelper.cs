﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Data.OleDb;

namespace XiWan.DALFactory
{
    public class DataTableHelper
    {
        #region 构造函数
        private static DataTableHelper m_instance = null;
        private static object syncRoot = new Object();

        private DataTableHelper()
        {
        }

        public static DataTableHelper Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (m_instance == null) m_instance = new DataTableHelper();
                    }
                }
                return m_instance;
            }
        }
        #endregion

        public decimal TotalItemPrice(DataTable dtb,string columnName)
        {
            return TotalItemPrice(dtb,columnName, "SUM");
        }
        // luobing 9-7
        public decimal TotalItemPriceByWhere(DataTable dtb, string columnName,string strWhere)
        {
            object objValue = dtb.Compute("Sum(["+columnName+"])",strWhere);
            if (objValue == DBNull.Value)
                return 0.0m;
            return Convert.ToDecimal(objValue);
        }

        public decimal TotalItemPrice(DataTable dtb, string columnName, string function)
        {
            object objValue = dtb.Compute(function + "([" + columnName + "])", "");
            if (objValue == DBNull.Value)
            {
                objValue = 0.0m;
            }
            return (decimal)objValue;
        }

        public DataColumn CopyColumn(DataColumn dcl)
        {
            DataColumn dclNew = new DataColumn(dcl.ColumnName, dcl.DataType);
            dclNew.Caption = dcl.Caption;
            dclNew.AllowDBNull = dcl.AllowDBNull;
            dclNew.AutoIncrement = dcl.AutoIncrement;
            dclNew.AutoIncrementSeed = dcl.AutoIncrementSeed;
            dclNew.AutoIncrementStep = dcl.AutoIncrementStep;
            dclNew.DefaultValue = dcl.DefaultValue;
            dclNew.MaxLength = dcl.MaxLength;
            return dclNew;
        }

        /// <summary>
        /// 复制dtb指定的列，返回一个新的表结构，如果指定的列包含完整的主键，返回的新表也将包含主键。
        /// </summary>
        /// <param name="dtb"></param>
        /// <param name="fieldList"></param>
        /// <returns></returns>
        public DataTable CopyTableSchema(DataTable dtbSchema, string fieldList)
        {
            DataTable dtb = new DataTable();
            dtb.TableName = dtbSchema.TableName;

            string[] arrField = fieldList.Split(',');
            foreach (string strField in arrField)
            {
                if (dtbSchema.Columns.Contains(strField))
                {
                    dtb.Columns.Add(CopyColumn(dtbSchema.Columns[strField]));
                }
            }

            bool blnHasKey = true;
            DataColumn[] arrKeyColumn = new DataColumn[dtbSchema.PrimaryKey.Length];
            DataColumn dclKey = null;
            for (int i = 0; i < dtbSchema.PrimaryKey.Length; i++)
            {
                dclKey = dtbSchema.PrimaryKey[i];
                if (dtb.Columns.Contains(dclKey.ColumnName))
                {
                    arrKeyColumn[i] = dtb.Columns[dclKey.ColumnName];
                }
                else
                {
                    blnHasKey = false;
                    break;
                }
            }
            if (blnHasKey) dtb.PrimaryKey = arrKeyColumn;

            return dtb;
        }

        public IDataParameter[] ExcludeParameter(IDataParameter[] paras, string key)
        {
            ArrayList list = new ArrayList();

            for (int i = 0; i < paras.Length; i++)
            {
                if (paras[i].ParameterName != key && paras[i].ParameterName.Substring(1) != key)
                {
                    list.Add(paras[i]);
                }
            }
            return list.ToArray(typeof(IDataParameter)) as IDataParameter[];
        }

        public IDataParameter[] GetParameters(Hashtable list, ICollection keys, DataColumn[] columns)
        {
            IDataParameter[] paras = null;
            int intCount = 0;
            if (keys != null) intCount += keys.Count;
            if (columns != null) intCount += columns.Length;
            if (intCount == 0)
            {
                paras = new IDataParameter[list.Count];
                int intIndex = 0;
                IDictionaryEnumerator enumerator = list.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    paras[intIndex] = (IDataParameter)((ICloneable)list[enumerator.Key]).Clone();
                    intIndex += 1;
                }
            }
            else
            {
                paras = new IDataParameter[intCount];
                int intIndexKeys = 0;
                if (keys != null)
                {
                    foreach (object key in keys)
                    {
                        paras[intIndexKeys] = (IDataParameter)((ICloneable)list[key]).Clone();
                        intIndexKeys += 1;
                    }
                }

                int intIndexColumns = 0;
                if (columns != null)
                {
                    foreach (DataColumn column in columns)
                    {
                        paras[intIndexKeys + intIndexColumns] = (IDataParameter)((ICloneable)list[column.ColumnName + "_Where"]).Clone();
                        intIndexColumns += 1;
                    }
                }
            }

            return paras;
        }


    }
}
