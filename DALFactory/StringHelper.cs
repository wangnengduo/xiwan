/*
 * 作者: Wong
 * 日期: 2018-6-13
 * 处理对象序列化和反序列化
 */
using System.Diagnostics;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Collections;
using System.Text.RegularExpressions;
using System.Text;


namespace XiWan.DALFactory
{
    public class StringHelper
    {
        #region 构造函数
        private static StringHelper m_instance = null;
        private static object syncRoot = new Object();

        private StringHelper()
        {
        }

        public static StringHelper Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (syncRoot)
                    {
                        if (m_instance == null) m_instance = new StringHelper();
                    }
                }
                return m_instance;
            }
        }
        #endregion

        public const string c_Comma_Flag = ",";

        public static readonly string c_CrLf = ((char)13).ToString() + ((char)10).ToString();

        #region 字段相关方法
        public static string ConcatWithComma(string field1, string field2)
        {
            return string.Concat(field1, c_Comma_Flag, field2);
        }

        public static string ConcatWithComma(string field1, string field2, string field3)
        {
            return string.Concat(new string[] { field1, c_Comma_Flag, field2, c_Comma_Flag, field3 });
        }

        public static string ConcatWithComma(string[] fields)
        {
            string strComma = ",";
            string sField;
            System.Text.StringBuilder sb = new System.Text.StringBuilder(100);

            foreach (string tempLoopVar_sField in fields)
            {
                sField = tempLoopVar_sField;
                sb.Append(sField);
                sb.Append(strComma);
            }

            if (sb.Length > 0)
            {
                sb.Remove(sb.Length - 1, 1);
            }

            return sb.ToString();
        }
        #endregion

        // 检查系统对象命名
        public static bool IsValidSystemObjectName(string str)
        {
            // 命名不能为空
            if ((str == null) || (str.Length == 0))
            {
                return false;
            }

            const string FirstLetter = "[^_a-zA-Z]";
            const string OtherLetter = "[^-_a-zA-Z0-9]";
            string sFirstChar;
            sFirstChar = str.Substring(0, 1);

            // 命名首字母必须是字母,下划线
            if (Regex.IsMatch(sFirstChar, FirstLetter))
            {
                return false;
            }

            // 其它字符必须是字母,下划线,数字
            if (Regex.IsMatch(str, OtherLetter))
            {
                return false;
            }
            return true;
        }

        // 输出由若干个指定字符组成的字符串
        public static string Repeat(string value, int count)
        {
            while (count > 0)
            {
                value += value;
            }
            return value;
        }


        public static bool IsLetter(string str)
        {
            bool bResutl = true;
            int iIndex;

            for (iIndex = 0; iIndex <= str.Length - 1; iIndex++)
            {
                if (!char.IsLetter(str.ToCharArray()[iIndex]))
                {
                    bResutl = false;
                    break;
                }
            }

            return bResutl;
        }

        // 说明:判断字符串是否全部为字母
        // - str - 待检查的字符串
        public static bool IsLetterString(string valuse)
        {
            bool pass = true;

            if ((valuse == null) || (valuse.Length == 0))
            {
                pass = false;
            }
            else
            {
                int intAsc;
                char chr;
                foreach (char tempLoopVar_chr in valuse)
                {
                    chr = tempLoopVar_chr;
                    if (chr.ToString() != "_")
                    {
                        intAsc = Strings.Asc(chr);
                        if (!(intAsc > 64 && intAsc < 91) && !(intAsc > 96 && intAsc < 123))
                        {
                            pass = false;
                            break;
                        }
                    }
                }
            }

            return pass;
        }

        // 说明:判断字符串是否全部为数字
        // - str - 待检查的字符串
        public static bool IsDigitalString(string value)
        {
            bool pass = true;

            if ((value == null) || (value.Length == 0))
            {
                pass = false;
            }
            else
            {
                foreach (char chr in value)
                {
                    if ((!char.IsDigit(chr)) && (chr.ToString() != ".") && (chr.ToString() != "-"))
                    {
                        pass = false;
                        break;
                    }
                }
            }

            return pass;
        }

        public static string GetSystemObjectName()
        {
            return "_" + Guid.NewGuid().ToString().Replace("-", "");
        }


        public string Exclude(string old, string excludeString)
        {
            string strNew = "";
            string strOld = "," + old + ",";
            if (strOld.IndexOf("," + excludeString + ",") >= 0)
            {
                strNew = strOld.Replace("," + excludeString + ",", ",");
                strNew = strNew.Substring(1, strNew.Length - 2);
            }
            else
            {
                strNew = old;
            }
            return strNew;
        }
        
        public string[] CopyToArray(string[] source, int index, int length)
        {
            string[] arr = new string[length];
            for (int i = 0; i < source.Length - index - 1; i++)
            {
                arr[i] = source[index + i];
            }
            return arr;
        }

        public string ArrayToString(string[] arr, string separator)
        {
            string strValue = "";
            foreach (string str in arr)
            {
                if (strValue.Length > 0) strValue += separator;
                strValue += str;
            }
            return strValue;
        }

        public string BuildString(DataRow row)
        {
            if (row == null) return "";

            string strOperator = "";

            for (int i = 0; i < row.ItemArray.Length; i++)
            {
                string temp = row.ItemArray[i].ToString();
                if (strOperator.IndexOf(temp + ",") < 0)
                {
                    strOperator += temp + ",";
                }
            }

            if (strOperator.Length > 0)
            {
                strOperator = strOperator.Substring(0, strOperator.Length - 1);
            }

            return strOperator;
        }
        /// <summary>
        /// 移除字符串前面空格
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        public static string ForeTrim(string value)
        {
            int i = 0;
            for (; i < value.Length; i++)
            {
                if (value[i] != ' ')
                    break;
            }
            return value.Remove(0, i);
        }
        /// <summary>
        /// 移除字符串前面 匹配字符的字符
        /// </summary>
        /// <param name="value"></param>
        /// <param name="c"></param>
        /// <returns></returns>
        public static string ForeTrim(string value, char c)
        {
            int i = 0;
            for (; i < value.Length; i++)
            {
                if (value[i] != c)
                    break;
            }
            return value.Remove(0, i);
        }

        /// <summary>
        /// 过滤可对象，并返回要默认值
        /// </summary>
        /// <param name="objQuest">object</param>
        /// <param name="strDefaultVal">string</param>
        /// <returns>过滤后或设置为默认值的字符串</returns>
        public static string FilterToDefault(object objQuest, string strDefaultVal)
        {
            string strTempQuest = "";
            if (objQuest == null || objQuest.ToString().Trim() == "")
            {
                strTempQuest = strDefaultVal;
            }
            else
            {
                strTempQuest = objQuest.ToString();
            }

            return strTempQuest;
        }
        /// <summary>
        /// 正常转换为字节
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static Byte[] NormalToByte(string str)
        {
            return Encoding.Default.GetBytes(str);
        }

        /// <summary>
        /// 字节转化为字符串
        /// </summary>
        /// <param name="Byt"></param>
        /// <returns></returns>
        public static string ToStr(Byte[] Byt)
        {
            return System.Convert.ToBase64String(Byt);
        }

        /// <summary>
        /// 正常专号为字符串
        /// </summary>
        /// <param name="Byt"></param>
        /// <returns></returns>
        public static string NormalToStr(byte[] Byt)
        {
            return Encoding.Default.GetString(Byt);
        }

        // <summary>
        /// 字符串转化为字节
        /// </summary>
        /// <param name="Str"></param>
        /// <returns></returns>
        public static byte[] ToByte(string Str)
        {
            return System.Convert.FromBase64String(Str);
        }
    }
}