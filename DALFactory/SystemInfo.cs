/*
 * 作者: Wong
 * 日期: 2018-6-13
 * 处理对象序列化和反序列化
 */
using System.Diagnostics;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Collections;

namespace XiWan.DALFactory
{
	public class SystemInfo
	{		
		#region 变量
		// 日期

        public const string c_Format_Date = "yyyy-MM-dd";
        public const string c_Format_DateMinute = "yyyy-MM-dd HH:mm";
        public const string c_Format_DateSecond = "yyyy-MM-dd HH:mm:ss";
        public const string c_Sql_DateTime_B = " 00:00:00.000";
        public const string c_Sql_DateTime_E = " 23:59:59.997";
        public const string c_strShort = "yy-MM-dd";

        
		private static string m_strDateFormat = c_Format_DateSecond;
        //private static string m_strDateMinute = c_Format_DateMinute;
        private static string m_strShortDateFormat = c_Format_Date;
		private static string m_strStartTime = c_Sql_DateTime_B;
        private static string m_strEndTime = c_Sql_DateTime_E;
        //private static string m_strShort = c_strShort;
		
		// 数值
		private static string m_decDecimalFormat = "f2";
		#endregion
		
		#region 属性
		public static string AppPath
		{
			get
			{
				return System.Windows.Forms.Application.StartupPath + "\\";
			}
		}

        public static string ReportsPath
        {
            get
            {
                return AppPath + "Reports\\";
            }
        }
				
		public static string DateFormat
		{
			get
			{
				return m_strDateFormat;
			}
		}
		
		public static string DecimalFormat
		{
			get
			{
				return m_decDecimalFormat;
			}
		}
		
		public static string ShortDateFormat
		{
			get
			{
				return m_strShortDateFormat;
			}
		}
		
		public static string StartTime
		{
			get
			{
				return m_strStartTime;
			}
		}
		
		public static string EndTime
		{
			get
			{
				return m_strEndTime;
			}
		}
		
		public static string GetStartDate(DateTime value)
		{
			return value.ToString(m_strShortDateFormat) + m_strStartTime;
		}
		
		public static string GetEndDate(DateTime value)
		{
			return value.ToString(m_strShortDateFormat) + m_strEndTime;
		}
		#endregion			

        public static string checkStr(string html)
        {
            System.Text.RegularExpressions.Regex regex1 = new System.Text.RegularExpressions.Regex(@"<script[\s\S]+</script *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex2 = new System.Text.RegularExpressions.Regex(@" href *= *[\s\S]*script *:", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex3 = new System.Text.RegularExpressions.Regex(@" no[\s\S]*=", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex4 = new System.Text.RegularExpressions.Regex(@"<iframe[\s\S]+</iframe *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex5 = new System.Text.RegularExpressions.Regex(@"<frameset[\s\S]+</frameset *>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex6 = new System.Text.RegularExpressions.Regex(@"\<img[^\>]+\>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex7 = new System.Text.RegularExpressions.Regex(@"</p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex8 = new System.Text.RegularExpressions.Regex(@"<p>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            System.Text.RegularExpressions.Regex regex9 = new System.Text.RegularExpressions.Regex(@"<[^>]*>", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
            html = regex1.Replace(html, ""); //过滤<script></script>标记 
            html = regex2.Replace(html, ""); //过滤href=javascript: (<A>) 属性 
            html = regex3.Replace(html, " _disibledevent="); //过滤其它控件的on...事件 
            html = regex4.Replace(html, ""); //过滤iframe 
            html = regex5.Replace(html, ""); //过滤frameset 
            html = regex6.Replace(html, ""); //过滤frameset 
            html = regex7.Replace(html, ""); //过滤frameset 
            html = regex8.Replace(html, ""); //过滤frameset 
            html = regex9.Replace(html, "");
            //html = html.Replace(" ", "");
            html = html.Replace("</strong>", "");
            html = html.Replace("<strong>", "");
            return html;
        } 
	}	
}
