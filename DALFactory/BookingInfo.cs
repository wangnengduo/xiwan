﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace XiWan.DALFactory
{
    public class BookingInfo
    {
        public int adults { get; set; }
        public string childAge { get; set; }//岁数 以,分开
        public string name { get; set; }//姓名 以姓/名分开,多个人以，分开
    }
}