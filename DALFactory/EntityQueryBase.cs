﻿/*
 * 作者: Wong
 * 日期: 2018-6-13 
 */
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XiWan.DALFactory
{
    public class EntityQueryBase
    {
        protected string m_strTableName = "";
        private string m_strSql = "";
        private string m_strCondition = "";

        protected string m_strAlias = "";
        private string m_strRelationColumnMain = "";
        private string m_strRelationColumnSelf = "";
        private EntityQueryBase m_parentQuery = null;
        private List<EntityQueryBase> m_listRelationQuery = new List<EntityQueryBase>();
        private List<IDataParameter> m_listParameters = new List<IDataParameter>();
        private List<IDataParameter> m_listParametersUsed = new List<IDataParameter>();

        public string QueryTableName
        {
            get { return m_strTableName; }
            set { m_strTableName = value; }
        }

        public string Sql
        {
            get { return m_strSql; }
            set { m_strSql = value; }
        }

        public string Condition
        {
            get { return m_strCondition; }
            set { m_strCondition = value; }
        }

        public string Alias
        {
            get { return m_strAlias; }
            set { m_strAlias = value; }
        }

        public string RelationColumnMain
        {
            get { return m_strRelationColumnMain; }
            set { m_strRelationColumnMain = value; }
        }

        public string RelationColumnSelf
        {
            get { return m_strRelationColumnSelf; }
            set { m_strRelationColumnSelf = value; }
        }

        public List<EntityQueryBase> RelationQuery
        {
            get { return m_listRelationQuery; }
            set { m_listRelationQuery = value; }
        }

        public EntityQueryBase ParentQuery
        {
            get { return m_parentQuery; }
            set { m_parentQuery = value; }
        }

        public List<IDataParameter> Parameters
        {
            get { return m_listParameters; }
            set { m_listParameters = value; }
        }

        public List<IDataParameter> ParametersUsed
        {
            get { return m_listParametersUsed; }
            set { m_listParametersUsed = value; }
        }

        public void BuildQuery()
        {
            // 查询表
            if (m_strSql.Length == 0)
            {
                m_strSql = "select * from {0} {1}";     // 0主表 1主表别名
                m_strSql = string.Format("select * from {0} {1}", m_strTableName, m_strAlias);
            }

            // 查询条件      
            BuildCondition();
            if (m_strCondition.Length > 0)
            {
                m_strSql += " where " + m_strCondition;
            }
        }

        public void BuildCondition()
        {
            ConditionBuilder cb = new ConditionBuilder(Parameters.ToArray());
            string strExists = "";

            foreach (EntityQueryBase query in RelationQuery)
            {
                query.BuildCondition();
                strExists = BuildExistsCondition(query);
                if (strExists.Length > 0)
                {
                    cb.Add(strExists);
                    m_listParametersUsed.AddRange(query.ParametersUsed.ToArray());
                }
            }
            m_strCondition = cb.BuildCondition();
            m_listParametersUsed.AddRange(cb.Parameters.ToArray());
        }

        private string BuildExistsCondition(EntityQueryBase query)
        {
            if (query.Condition.Length == 0) return "";
            string strExists = "exists(select * from {1} {2} where ({3}) and ({2}.{{4}={0}.{5}) )";   //0主表别名 1子表名 2子表别名 3子表条件 4子表关联字段 5主表关联字段
            strExists = "exists(select * from " + query.QueryTableName + " " + query.Alias + " where " + query.Condition + " and (" + query.Alias + "." + query.RelationColumnSelf + " = " + m_strAlias + "." + query.m_strRelationColumnMain + "))";
            return strExists;
        }
    }
}